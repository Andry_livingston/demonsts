package com.orange5.scanner

interface OnBarcodeScannedListener {
    fun onBarcodeScanned(data: String)
}
