package com.orange5.scanner

class ScannerStateException(message: String) : Exception(message)