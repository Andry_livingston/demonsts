package com.orange5.scanner

import android.content.Context
import com.symbol.emdk.EMDKManager
import com.symbol.emdk.EMDKManager.FEATURE_TYPE
import com.symbol.emdk.barcode.BarcodeManager
import com.symbol.emdk.barcode.BarcodeManager.DeviceIdentifier.DEFAULT
import com.symbol.emdk.barcode.ScanDataCollection
import com.symbol.emdk.barcode.ScanDataCollection.ScanData
import com.symbol.emdk.barcode.Scanner
import com.symbol.emdk.barcode.Scanner.TriggerType.HARD
import com.symbol.emdk.barcode.Scanner.TriggerType.SOFT_ONCE
import com.symbol.emdk.barcode.ScannerException
import com.symbol.emdk.barcode.ScannerResults.SUCCESS
import com.symbol.emdk.barcode.StatusData
import com.symbol.emdk.barcode.StatusData.ScannerStates.IDLE
import timber.log.Timber

class ScannerServiceImpl(private val context: Context) : ScannerService, EMDKManager.EMDKListener, Scanner.DataListener, Scanner.StatusListener  {

    private val TAG = "ScannerService"
    private var scanner: Scanner? = null
    private var onBarcodeScannedListener: OnBarcodeScannedListener? = null
    private var stateListener: ScannerService.StateListener? = null
    private var emdkManager: EMDKManager? = null
    private var barcodeManager: BarcodeManager? = null
    private var isSoftTypeEnable = false
    private var isAlreadyScanned = false

    init {
        try {
            EMDKManager.getEMDKManager(context, this)
        } catch (t: Throwable) {
            Timber.tag(TAG).e(t)
        }
    }

    override fun setOnBarcodeScanListener(onBarcodeScannedListener: OnBarcodeScannedListener) {
        this.onBarcodeScannedListener = onBarcodeScannedListener
    }

    override fun enableScanner() {
        if (scanner != null) {
            try {
                scanner!!.enable()
                onAttached()
            } catch (e: ScannerException) {
                Timber.tag(TAG).e(e)
            }
        } else {
            initScanner()
        }
    }

    override fun disableScanner() {
        scanner?.let {
            try {
                it.disable()
                onDetached()
            } catch (e: ScannerException) {
                Timber.tag(TAG).e(e)
            }
        }
    }

    override fun connect() {
        initBarcodeManager()
        initScanner()
    }

    override fun reconnect() {
        release()
        EMDKManager.getEMDKManager(context, this)
    }

    override fun releaseScanner() {
        scanner?.let {
            try {
                it.disable()
                it.release()
                onDetached()
            } catch (e: ScannerException) {
                Timber.tag(TAG).e(e)
            }
            scanner = null
        }
    }

    override fun setSoftMode() {
        isSoftTypeEnable = true
        cancelRead()
    }

    override fun isScannerActive() = emdkManager != null

    override fun setScannerStateListener(stateListener: ScannerService.StateListener) {
        this.stateListener = stateListener
    }

    override fun onOpened(manager: EMDKManager?) {
        emdkManager = manager
        initBarcodeManager()
    }

    override fun onClosed() {
        emdkManager?.let {
            it.release()
            emdkManager = null
        }
    }

    override fun onData(data: ScanDataCollection?) {
        if (isAlreadyScanned || data == null || data.result != SUCCESS) return
        data.scanData
                .forEach { scanData: ScanData ->
                    isAlreadyScanned = true
                    onBarcodeScannedListener?.onBarcodeScanned(scanData.data.trim())
                }
    }

    override fun onStatus(statusData: StatusData) {
        if (statusData.state == IDLE) {
            if (scanner == null) {
                initScanner()
                return
            }
            if (isSoftTypeEnable) {
                scanner?.triggerType = SOFT_ONCE
                isSoftTypeEnable = false
            } else {
                scanner?.triggerType = HARD
            }
            read()
        }
    }

    private fun initScanner() {
        if (barcodeManager == null) {
            onError("Scanner Fails : BarcodeManager == null")
            return
        }
        scanner = barcodeManager!!.getDevice(DEFAULT)
        if (scanner != null) {
            scanner?.addStatusListener(this)
            scanner?.addDataListener(this)
            try {
                scanner?.enable()
                onAttached()
            } catch (e: ScannerException) {
                Timber.tag(TAG).e(e)
                releaseScanner()
                onError("Scanner Fails : Can't enable scan   " + e.message)
            }
        } else {
            onError("Scanner Fails : Scanner == null")
        }
    }

    private fun release() {
        releaseScanner()
        if (emdkManager != null) {
            emdkManager!!.release()
            emdkManager = null
        }
    }


    private fun initBarcodeManager() {
        emdkManager?.let {
            barcodeManager = it.getInstance(FEATURE_TYPE.BARCODE) as BarcodeManager
        }
    }

    private fun releaseBarcodeManager() {
        emdkManager?.release(FEATURE_TYPE.BARCODE)
    }

    private fun read() {
        scanner?.let { scanner ->
            isAlreadyScanned = false
            if (!scanner.isReadPending) {
                try {
                    scanner.read()
                } catch (e: ScannerException) {
                    Timber.tag(TAG).e(e)
                }
            }
        }
    }

    private fun cancelRead() {
        scanner?.let { scanner ->
            if (scanner.isReadPending) {
                try {
                    scanner.cancelRead()
                } catch (e: ScannerException) {
                    Timber.tag(TAG).e(e)
                }
            }
        }
    }

    private fun onDetached() {
        stateListener?.onDetached()
    }

    private fun onAttached() {
        stateListener?.onAttached()
    }

    private fun onError(cause: String) {
        val error = ScannerStateException(cause)
        Timber.tag(TAG).e(error)
        stateListener?.onError(error)
    }
}