package com.orange5.scanner

interface ScannerService {

    fun setOnBarcodeScanListener(onBarcodeScannedListener: OnBarcodeScannedListener)

    fun setScannerStateListener(stateListener: StateListener)

    fun enableScanner()

    fun disableScanner()

    fun connect()

    fun reconnect()

    fun releaseScanner()

    fun setSoftMode()

    fun isScannerActive(): Boolean

    interface StateListener {
        fun onAttached()

        fun onDetached()

        fun onError(e: ScannerStateException) = Unit
    }
}