package com.orange5.scanner

import android.os.Build
import android.os.VibrationEffect.EFFECT_CLICK
import android.os.VibrationEffect.EFFECT_HEAVY_CLICK
import android.os.VibrationEffect.createPredefined
import android.os.Vibrator
import androidx.annotation.RequiresApi

class VibratorHandler(private val vibrator: Vibrator) {

    fun vibrate(type: Type) {
        if (type == Type.ERROR) errorVibration() else successVibration()
    }

    private fun successVibration() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) vibrate(EFFECT_CLICK) else vibrate(SHORT_EFFECT)
    }

    private fun errorVibration() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) vibrate(EFFECT_HEAVY_CLICK) else vibrate(LONG_EFFECT)
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun vibrate(effectId: Int) {
        if (vibrator.hasVibrator()) {
            vibrator.vibrate(createPredefined(effectId))
        }
    }

    @Suppress("DEPRECATION")
    private fun vibrate(duration: Long) {
        if (vibrator.hasVibrator()) {
            vibrator.vibrate(duration)
        }
    }

    companion object {
        const val SHORT_EFFECT = 50L
        const val LONG_EFFECT = 200L
    }

    enum class Type {
        SUCCESS, ERROR
    }
}

