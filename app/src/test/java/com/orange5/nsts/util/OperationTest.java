package com.orange5.nsts.util;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;


public class OperationTest {

    @Test
    public void start() {
        Object value =
                Operation.with("32")
                        .add(12)
                        .subtract(1)
                        .subtract(1)
                        .divide(2)
                        .multiply(2)
                        .add("58")
                        .add("100.000000000000000000234567")
                        .percentage(50)
                        .addPercentage(30.00f)
                        .toDouble().intValue();
        assertEquals(value, 130);
    }

    @Test
    public void start2() {
        Operation sale = Operation.start()//init new operation and add items with quantity and discount
                .add(Operation.with(20.99).multiply(3).subtractPercentage(4.99))
                .add(Operation.with(1.99).multiply(2).subtractPercentage(4.99))
                .add(Operation.with(16.49).multiply(12).subtractPercentage(2.99))
                .add(Operation.with(12.99).multiply(2).subtractPercentage(.5))
                .add(Operation.with(2.99).multiply(7).subtractPercentage(1.1))
                .addPercentage(21.21);//VAT

        Operation received = Operation.with(400);
        Operation change = received.subtract(sale);
        assertEquals(change.toDouble(), 33.8);
    }


    @Test
    public void multiply() {
        assertEquals(
                Operation.with("12.12")
                        .multiply("12.12")
                        .multiply(12.12)
                        .toDouble(), 1780.31);
    }

}