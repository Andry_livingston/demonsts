package com.orange5.nsts.receivers

import android.content.BroadcastReceiver
import android.content.IntentFilter

abstract class BaseBroadcastReceiver : BroadcastReceiver() {
    abstract val intentFilter: IntentFilter
}