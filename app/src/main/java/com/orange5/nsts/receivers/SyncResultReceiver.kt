package com.orange5.nsts.receivers

import android.content.Intent
import android.content.IntentFilter
import com.orange5.nsts.receivers.BroadcastService.Companion.SYNC_RESULT_ACTION

abstract class SyncResultReceiver : BaseBroadcastReceiver() {
    override val intentFilter: IntentFilter = IntentFilter(SYNC_RESULT_ACTION)

    companion object {
        const val SYNC_RESULT_KEY = "sync_result_key"
        const val SYNC_FAIL_KEY = "sync_fail_key"
        const val EMPTY_SERVER_QUERIES_KEY = "empty_server_queries_key"

        @JvmStatic
        fun newSyncResultIntent(errorMessage: String?, areServerQueriesEmpty: Boolean = true) = Intent().also {
            it.action = SYNC_RESULT_ACTION
            if (errorMessage != null) {
                it.putExtra(SYNC_FAIL_KEY, true)
                it.putExtra(EMPTY_SERVER_QUERIES_KEY, areServerQueriesEmpty)
                it.putExtra(SYNC_RESULT_KEY, errorMessage)
            } else {
                it.putExtra(SYNC_FAIL_KEY, false)
                it.putExtra(EMPTY_SERVER_QUERIES_KEY, areServerQueriesEmpty)
            }
        }
    }
}