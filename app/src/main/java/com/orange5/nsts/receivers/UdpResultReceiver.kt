package com.orange5.nsts.receivers

import android.content.Intent
import android.content.IntentFilter
import com.orange5.nsts.receivers.BroadcastService.Companion.UDP_RESULT_ACTION

abstract class UdpResultReceiver : BaseBroadcastReceiver() {
    override val intentFilter: IntentFilter = IntentFilter(UDP_RESULT_ACTION)

    companion object {
        const val UDP_SENDER = "udp_sender_ip"
        const val UDP_DATA_PACKET = "udp_data_packet"
        const val UDP_ERROR_MESSAGE = "udp_error_message_key"
        const val UDP_FAIL_KEY = "udp_data_fail_key"

        @JvmStatic
        fun newUdpResultIntent(errorMessage: String?, senderIp: String, data: String) = Intent().also {
            it.action = UDP_RESULT_ACTION
            if (errorMessage != null) {
                it.putExtra(UDP_FAIL_KEY, true)
                it.putExtra(UDP_ERROR_MESSAGE, errorMessage)
            } else {
                it.putExtra(UDP_FAIL_KEY, false)
                it.putExtra(UDP_SENDER, senderIp)
                it.putExtra(UDP_DATA_PACKET, data)
            }
        }
    }
}