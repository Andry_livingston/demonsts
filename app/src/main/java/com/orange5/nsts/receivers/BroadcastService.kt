package com.orange5.nsts.receivers

import android.content.Context
import android.content.Intent
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import javax.inject.Inject


open class BroadcastService (private val context: Context) {

    fun sendLocalBroadcast(intent: Intent) {
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
    }

    fun sendBroadcast(intent: Intent) {
        context.sendBroadcast(intent)
    }

    fun registerLocalBroadcast(receiver: BaseBroadcastReceiver) {
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, receiver.intentFilter)
    }

    fun unregisterLocalBroadcast(receiver: BaseBroadcastReceiver) {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver)
    }

    fun registerBroadcast(receiver: BaseBroadcastReceiver) {
        context.registerReceiver(receiver, receiver.intentFilter)
    }

    fun unregisterBroadcast(receiver: BaseBroadcastReceiver) {
        context.unregisterReceiver(receiver)
    }

    companion object {
        const val SYNC_RESULT_ACTION = "android.net.wifi.SYNC_RESULT_ACTION"
        const val UDP_RESULT_ACTION = "android.net.wifi.UDP_RESULT_ACTION"
        const val INIT_LOAD_RESULT_ACTION = "android.net.wifi.INIT_LOAD_RESULT_ACTION"
    }
}
