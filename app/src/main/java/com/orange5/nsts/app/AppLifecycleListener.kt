package com.orange5.nsts.app

import android.app.Activity
import android.app.Application.ActivityLifecycleCallbacks
import android.os.Bundle
import com.orange5.nsts.sync.udp.UdpClientService
import com.orange5.nsts.ui.base.BaseActivity

class AppLifecycleListener(private val udpService: UdpClientService) : ActivityLifecycleCallbacks {

    var activity: BaseActivity? = null

    override fun onActivityStarted(activity: Activity) {
        this.activity = activity as BaseActivity
        udpService.onStart()
    }

    override fun onActivityStopped(activity: Activity) = Unit

    override fun onActivityDestroyed(activity: Activity) = Unit

    override fun onActivityPaused(activity: Activity) = Unit

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) = Unit

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) = Unit

    override fun onActivityResumed(activity: Activity) = Unit
}