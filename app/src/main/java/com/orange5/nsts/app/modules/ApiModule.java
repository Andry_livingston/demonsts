package com.orange5.nsts.app.modules;

import com.google.gson.Gson;
import com.orange5.nsts.api.Api;
import com.orange5.nsts.api.load.InitLoadClient;
import com.orange5.nsts.api.load.InitLoadClientImpl;
import com.orange5.nsts.api.sync.SyncClient;
import com.orange5.nsts.api.sync.SyncClientImpl;
import com.orange5.nsts.app.App;
import com.orange5.nsts.data.preferencies.RawSharedPreferences;
import com.orange5.nsts.ui.settings.models.ApiSettings;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.orange5.nsts.ui.settings.models.ApiSettings.INIT_LOAD_PORT;
import static com.orange5.nsts.ui.settings.models.ApiSettings.SYNC_PORT;

public class ApiModule {

    private final Gson gson;
    private final RawSharedPreferences preferences;
    private final OkHttpClient okHttpClient;
    private final ApiSettings settings;

    private Retrofit retrofit;

    public ApiModule(Gson gson, RawSharedPreferences preferences, OkHttpClient okHttpClient) {
        this.gson = gson;
        this.preferences = preferences;
        this.okHttpClient = okHttpClient;
        if (preferences.getApiSettings() == null) {
            settings = new ApiSettings();
        } else {
            settings = preferences.getApiSettings();
        }

        retrofit = provideRetrofit();
    }

    public SyncClient syncClient() {
        setPort(SYNC_PORT);
        if (retrofit == null) {
            retrofit = provideRetrofit();
        } else {
            retrofit = buildRetrofit(retrofit.newBuilder());
        }
        return new SyncClientImpl(retrofit.create(Api.class));
    }

    public InitLoadClient initLoadClient() {
        setPort(INIT_LOAD_PORT);
        if (retrofit == null) {
            retrofit = provideRetrofit();
        } else {
            retrofit = buildRetrofit(retrofit.newBuilder());
        }
        return new InitLoadClientImpl(retrofit.create(Api.class));
    }

    public SyncClient syncClient(String host) {
        settings.setHost(host);
        setPort(SYNC_PORT);
        preferences.setApiSettings(settings);
        if (retrofit == null) {
            retrofit = provideRetrofit();
        } else {
            retrofit = buildRetrofit(retrofit.newBuilder());
        }
        return new SyncClientImpl(retrofit.create(Api.class));
    }

    private Retrofit provideRetrofit() {
        if (retrofit == null) {
            Class var2 = App.class;
            synchronized (App.class) {
                if (retrofit == null) {
                    retrofit = buildRetrofit(new Retrofit.Builder());
                }
            }
        }
        return retrofit;
    }

    private Retrofit buildRetrofit(Retrofit.Builder builder) {
        return builder
                .baseUrl(settings.getBaseUrl())
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    private void setPort(String syncPort) {
        if (!settings.getPort().equals(syncPort)) settings.setPort(syncPort);
        preferences.setApiSettings(settings);
    }
}
