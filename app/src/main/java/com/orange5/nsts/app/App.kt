package com.orange5.nsts.app

import android.app.Application
import android.database.sqlite.SQLiteDatabase
import com.orange5.nsts.BuildConfig.DEBUG
import com.orange5.nsts.data.db.dao.DaoSession
import com.orange5.nsts.di.DaggerAppComponent
import com.orange5.nsts.sync.SyncByWifiStateHandler
import com.orange5.scanner.ScannerService
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import io.reactivex.exceptions.UndeliverableException
import io.reactivex.plugins.RxJavaPlugins
import timber.log.Timber
import javax.inject.Inject

class App : Application(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>
    @Inject
    lateinit var appLifecycleListener: AppLifecycleListener
    @Inject
    lateinit var daoSession: DaoSession
    @Inject
    lateinit var sqLiteDatabase: SQLiteDatabase
    @Inject
    lateinit var scannerService: ScannerService

    override fun androidInjector() = androidInjector

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent.builder().application(this).build().inject(this)
        instance = this
        SyncByWifiStateHandler.registerSyncByWifiHandler(this, appLifecycleListener)

        RxJavaPlugins.setErrorHandler { throwable ->
            if (throwable is UndeliverableException) {
                Timber.e(throwable)
            }
        }
    }


    fun getRawDb(): SQLiteDatabase {
        return sqLiteDatabase
    }

    companion object {
        @JvmField
        var instance: App? = null

        @JvmStatic
        fun getInstance(): App? = instance
    }

}