package com.orange5.nsts.scan

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseActivity
import com.orange5.nsts.util.view.CustomToast
import com.orange5.scanner.VibratorHandler.Type
import com.orange5.scanner.VibratorHandler.Type.ERROR
import com.orange5.scanner.VibratorHandler.Type.SUCCESS
import javax.inject.Inject

abstract class ScanningActivity : BaseActivity() {

    protected val scanningViewModel: ScanningViewModel by viewModels { vmFactory }
    private var toast: Toast? = null

    @Inject
    lateinit var vibrator: ScanVibrator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        scanningViewModel.vibration.observe(this, Observer(::vibrate))
        scanningViewModel.liveScannerAttached.observe(this, Observer { invalidateOptionsMenu() })
        scanningViewModel.showError.observe(this, Observer(::showErrorToast))
    }

    abstract fun notFoundMessage(): Int

    override fun onStart() {
        super.onStart()
        scanningViewModel.setUpScanService()
    }

    override fun getMenuResourceId() = R.menu.barcode_scan_menu

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        menu.findItem(R.id.scanner_on).isVisible = scanningViewModel.isScannerServiceActive() && scanningViewModel.isScannerActive
        menu.findItem(R.id.scanner_off).isVisible = scanningViewModel.isScannerServiceActive() && !scanningViewModel.isScannerActive
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
            when (item.itemId) {
                R.id.scanner_on -> {
                    scanningViewModel.disableScanner()
                    true
                }
                R.id.scanner_off -> {
                    scanningViewModel.enableScanner()
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    override fun onStop() {
        scanningViewModel.releaseScanner()
        super.onStop()
    }

    private fun showErrorToast(shouldShow: Boolean) {
        toast?.cancel()
        if (shouldShow) {
            errorVibration()
            toast = CustomToast.show(this, getString(notFoundMessage()))
        }
    }

    private fun vibrate(type: Type) {
        vibrator.vibrate(type)
    }

    protected fun errorVibration() = vibrate(ERROR)

    protected fun successVibration() = vibrate(SUCCESS)
}