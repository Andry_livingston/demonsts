package com.orange5.nsts.scan

import android.os.Bundle
import android.view.View
import android.view.View.VISIBLE
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.view.postDelayed
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseDialogFragment
import com.orange5.nsts.ui.extensions.errorToast
import com.orange5.nsts.util.view.CustomToast
import com.orange5.scanner.VibratorHandler.Type
import com.orange5.scanner.VibratorHandler.Type.ERROR
import com.orange5.scanner.VibratorHandler.Type.SUCCESS
import javax.inject.Inject

abstract class ScanningDialog : BaseDialogFragment() {

    private var toast: Toast? = null
    private var scannerToggle: ImageView? = null
    protected val scanningViewModel: ScanningViewModel by viewModels { factory }

    @get:StringRes
    abstract val notFoundMessage: Int

    @Inject
    lateinit var vibrator: ScanVibrator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        scanningViewModel.vibration.observe(this, Observer(::vibrate))
        scanningViewModel.liveScannerAttached.observe(this, Observer { setScannerIcon() })
        scanningViewModel.showError.observe(this, Observer(this::showErrorToast))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        scannerToggle = view.findViewById(R.id.scannerToggle)
        scannerToggle?.setOnClickListener { toggleScanner() }
    }

    override fun onStart() {
        super.onStart()
        requireView().postDelayed(200) {
            scanningViewModel.setUpScanService()
            scanningViewModel.setUpScanService()
        }
    }

    protected open fun setScannerIcon() {
        scannerToggle?.let {
            it.visibility = VISIBLE
            val icon =
                    if (scanningViewModel.isScannerServiceActive() && scanningViewModel.isScannerActive) R.drawable.barcode_scan_on_ripple
                    else R.drawable.barcode_scan_off_ripple
            it.setImageResource(icon)
        }
    }

    private fun toggleScanner() {
        if (scanningViewModel.isScannerServiceActive() && scanningViewModel.isScannerActive) scanningViewModel.disableScanner()
        else scanningViewModel.enableScanner()
    }

    override fun onStop() {
        scanningViewModel.releaseScanner()
        super.onStop()
    }

    protected fun showErrorToast(shouldShow: Boolean) {
        errorVibration()
        toast?.cancel()
        if (shouldShow) toast = CustomToast.show(requireActivity(), getString(notFoundMessage))
    }

    protected fun showErrorToast(@StringRes errorRes: Int) {
        toast?.cancel()
        toast = requireActivity().errorToast(errorRes)
        errorVibration()
    }

    private fun vibrate(type: Type) {
        vibrator.vibrate(type)
    }

    protected fun errorVibration() = vibrate(ERROR)

    protected fun successVibration() = vibrate(SUCCESS)
}