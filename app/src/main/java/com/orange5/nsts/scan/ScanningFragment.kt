package com.orange5.nsts.scan

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.annotation.CallSuper
import androidx.annotation.StringRes
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseFragment
import com.orange5.nsts.ui.base.BaseListFragment
import com.orange5.nsts.ui.extensions.errorToast
import com.orange5.scanner.VibratorHandler.Type
import com.orange5.scanner.VibratorHandler.Type.ERROR
import com.orange5.scanner.VibratorHandler.Type.SUCCESS
import javax.inject.Inject

abstract class ScanningFragment : BaseListFragment() {

    private var toast: Toast? = null
    protected val scanningViewModel: ScanningViewModel by viewModels { vmFactory }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        subscribe()
    }

    @Inject
    lateinit var vibrator: ScanVibrator

    @CallSuper
    protected open fun subscribe() {
        scanningViewModel.vibration.observe(viewLifecycleOwner, Observer(::vibrate))
        scanningViewModel.liveScannerAttached.observe(viewLifecycleOwner, Observer { activity?.invalidateOptionsMenu() })
        scanningViewModel.showError.observe(viewLifecycleOwner, Observer(this::showErrorToast))
    }

    @get:StringRes
    abstract val notFoundMessage: Int

    override fun onStart() {
        super.onStart()
        scanningViewModel.setUpScanService()
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.findItem(R.id.scanner_off)?.isVisible = scanningViewModel.isScannerServiceActive() && !scanningViewModel.isScannerActive
        menu.findItem(R.id.scanner_on)?.isVisible = scanningViewModel.isScannerServiceActive() && scanningViewModel.isScannerActive
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
            when (item.itemId) {
                R.id.scanner_on -> {
                    scanningViewModel.disableScanner()
                    true
                }
                R.id.scanner_off -> {
                    scanningViewModel.enableScanner()
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    override fun onStop() {
        scanningViewModel.releaseScanner()
        super.onStop()
    }

    protected fun clearSubscriptions() {
        scanningViewModel.clearSubscriptions(viewLifecycleOwner)
    }

    protected fun showErrorToast(shouldShow: Boolean) {
        toast?.cancel()
        if (shouldShow) {
            toast = requireActivity().errorToast(notFoundMessage)
            errorVibration()
        }
    }

    protected fun showErrorToast(@StringRes errorRes: Int) {
        toast?.cancel()
        toast = requireActivity().errorToast(errorRes)
        errorVibration()
    }

    protected fun errorVibration() = vibrate(ERROR)

    protected fun successVibration() = vibrate(SUCCESS)

    private fun vibrate(type: Type) {
        vibrator.vibrate(type)
    }
}