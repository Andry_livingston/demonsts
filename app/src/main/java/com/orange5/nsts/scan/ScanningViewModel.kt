package com.orange5.nsts.scan

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.orange5.nsts.data.db.entities.BinNumber
import com.orange5.nsts.data.db.entities.ConsumableProduct
import com.orange5.nsts.data.db.entities.Customer
import com.orange5.nsts.data.db.entities.RentalProduct
import com.orange5.nsts.data.db.entities.RentalProductItem
import com.orange5.nsts.service.bin.BinService
import com.orange5.nsts.service.consumable.ConsumableProductService
import com.orange5.nsts.service.customer.CustomerService
import com.orange5.nsts.service.rental.RentalProductItemService
import com.orange5.nsts.service.rental.RentalProductService
import com.orange5.nsts.ui.base.ActionLiveData
import com.orange5.nsts.ui.base.BaseViewModel
import com.orange5.scanner.OnBarcodeScannedListener
import com.orange5.scanner.ScannerService
import javax.inject.Inject

class ScanningViewModel @Inject constructor(private val scannerService: ScannerService,
                                            private val rentalItemService: RentalProductItemService,
                                            private val rentalService: RentalProductService,
                                            private val consumableService: ConsumableProductService,
                                            private val binService: BinService,
                                            private val customerService: CustomerService) : BaseViewModel(),
        OnBarcodeScannedListener, ScannerService.StateListener {

    var isScannerActive = true

    @JvmField
    val barcodeValue = ActionLiveData<String>()

    @JvmField
    val liveRentalItemScanned = MutableLiveData<RentalProductItem>()

    @JvmField
    val liveRentalScanned = MutableLiveData<RentalProduct>()

    @JvmField
    val liveScannerAttached = ActionLiveData<Unit>()

    @JvmField
    val liveConsumableScanned = MutableLiveData<ConsumableProduct>()

    @JvmField
    val liveBinScanned = MutableLiveData<BinNumber>()

    @JvmField
    val liveCustomerScanned = MutableLiveData<Customer>()

    @JvmField
    val showError = ActionLiveData<Boolean>()

    override fun onBarcodeScanned(data: String) {
        if (barcodeValue.hasActiveObservers()) {
            clearShownError()
            barcodeValue.postValue(data)
            return
        }
        if (liveRentalItemScanned.hasActiveObservers()) {
            val product = searchRentalItem(data)
            product?.let {
                clearShownError()
                liveRentalItemScanned.postValue(it)
                return
            }
        }
        if (liveRentalScanned.hasActiveObservers()) {
            val product = searchRental(data)
            product?.let {
                liveRentalScanned.postValue(it)
                return
            }
        }
        if (liveConsumableScanned.hasActiveObservers()) {
            val product = searchConsumable(data)
            product?.let {
                liveConsumableScanned.postValue(it)
                return
            }
        }
        if (liveBinScanned.hasActiveObservers()) {
            val bin = searchBin(data)
            bin?.let {
                liveBinScanned.postValue(it)
                return
            }
        }
        if (liveCustomerScanned.hasActiveObservers()) {
            val customer = searchCustomer(data)
            customer?.let {
                liveCustomerScanned.postValue(it)
                return
            }
        }
        if (showError.hasActiveObservers()) {
            showError.postValue(true)
        }
    }

    override fun onAttached() {
        isScannerActive = true
        liveScannerAttached.postValue(Unit)
    }

    override fun onDetached() {
        isScannerActive = false
        liveScannerAttached.postValue(Unit)
    }

    fun disableScanner() {
        scannerService.disableScanner()
    }

    fun releaseScanner() {
        scannerService.releaseScanner()
    }

    fun setUpScanService() {
        scannerService.connect()
        scannerService.setOnBarcodeScanListener(this)
        scannerService.setScannerStateListener(this)
    }

    fun enableScanner() {
        scannerService.enableScanner()
    }

    fun isScannerServiceActive() = scannerService.isScannerActive()

    fun clearSubscriptions(owner: LifecycleOwner) {
        if (liveBinScanned.hasActiveObservers()) liveBinScanned.removeObservers(owner)
        if (barcodeValue.hasActiveObservers()) barcodeValue.removeObservers(owner)
        if (liveConsumableScanned.hasActiveObservers()) liveConsumableScanned.removeObservers(owner)
        if (liveCustomerScanned.hasActiveObservers()) liveCustomerScanned.removeObservers(owner)
        if (liveRentalItemScanned.hasActiveObservers()) liveRentalItemScanned.removeObservers(owner)
        if (liveRentalScanned.hasActiveObservers()) liveRentalScanned.removeObservers(owner)
        if (liveScannerAttached.hasActiveObservers()) liveScannerAttached.removeObservers(owner)
        if (showError.hasActiveObservers()) showError.removeObservers(owner)
    }

    private fun searchRentalItem(barcode: String): RentalProductItem? = rentalItemService.loadItemByUUN(barcode)

    private fun searchRental(barcode: String): RentalProduct? = rentalService.byProductNumber(barcode)

    private fun searchConsumable(barcode: String): ConsumableProduct? = consumableService.byProductNumber(barcode)

    private fun searchBin(barcode: String): BinNumber? = binService.byBinNum(barcode)

    private fun searchCustomer(barcode: String): Customer? = customerService.byBadgeNumber(barcode)

    private fun clearShownError() {
        showError.postValue(false)
    }

}