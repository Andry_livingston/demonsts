package com.orange5.nsts.scan

import com.orange5.nsts.data.preferencies.RawSharedPreferences
import com.orange5.scanner.VibratorHandler
import com.orange5.scanner.VibratorHandler.Type
import javax.inject.Inject

class ScanVibrator @Inject constructor(private val vibrator: VibratorHandler,
                                       private val preferences: RawSharedPreferences) {

    fun vibrate(type: Type) {
        if (preferences.isScanVibrationEnabled) vibrator.vibrate(type)
    }
}