package com.orange5.nsts.ui.home

import android.content.Context
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseBottomSheetDialogFragment
import com.orange5.nsts.ui.extensions.compoundDrawable
import com.orange5.nsts.ui.home.BottomMenuDialog.BottomType.*
import kotlinx.android.synthetic.main.dialog_bottom_menu.*


class BottomMenuDialog : BaseBottomSheetDialogFragment() {

    private var listener: Listener? = null

    override val layoutResId = R.layout.dialog_bottom_menu

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener =
                if (targetFragment != null) targetFragment as Listener
                else context as Listener

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val type = arguments?.getSerializable(KEY_TYPE) as? BottomType ?: MENU_ORDER
        val params = bottomMenuParams(type)
        val colorSet = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.primary_dark))

        topBtn.isVisible = params.hasSaveForLater
        saveForLaterDivider.isVisible = params.hasSaveForLater
        topBtn.compoundDrawable(params.saveForLaterIcon)
        topBtn.compoundDrawableTintList = colorSet
        topBtn.text = getString(params.saveForLaterTitle)
        middleBtn.compoundDrawable(params.continueIcon)
        middleBtn.text = getString(params.continueTitle)
        middleBtn.compoundDrawableTintList = colorSet
        bottomBtn.compoundDrawable(params.deleteIcon)
        bottomBtn.text = getString(params.deleteTitle)
        bottomBtn.compoundDrawableTintList = colorSet

        topBtn.onMenuButtonClick { listener?.onTopButtonClick() }
        middleBtn.onMenuButtonClick { listener?.onMiddleButtonClick() }
        bottomBtn.onMenuButtonClick { listener?.onBottomButtonClick() }
        if (type != MENU_DISCREPANCY) {
            dialog?.setOnKeyListener { _, code, _ -> onBackPress(code) }
        }
    }

    private fun bottomMenuParams(type: BottomType): BottomMenuParams {
        return when (type) {
            MENU_ORDER -> BottomMenuParams.order()
            MENU_RETURN -> BottomMenuParams.rentalReturn()
            MENU_SHIPPING -> BottomMenuParams.shipping()
            MENU_DISCREPANCY -> BottomMenuParams.discrepancy()
        }
    }

    private fun onBackPress(code: Int): Boolean {
        return if (code == KeyEvent.KEYCODE_BACK) {
            requireActivity().finish()
            listener?.onBackButtonClick()
            dismiss()
            true
        } else false
    }

    private fun View.onMenuButtonClick(action: () -> Unit) = setOnClickListener {
        action()
        dismiss()
    }

    companion object {
        private const val KEY_TYPE = "bottom_menu_type"

        @JvmOverloads
        @JvmStatic
        fun show(fragmentManager: FragmentManager, type: BottomType = MENU_ORDER) =
                BottomMenuDialog().apply {
                    arguments = bundleOf(KEY_TYPE to type)
                    show(fragmentManager, this::class.java.simpleName)
                }

        @JvmOverloads
        @JvmStatic
        fun show(fragment: Fragment, fragmentManager: FragmentManager, type: BottomType = MENU_ORDER) =
                BottomMenuDialog().apply {
                    arguments = bundleOf(KEY_TYPE to type)
                    setTargetFragment(fragment, 991)
                    show(fragmentManager, this::class.java.simpleName)
                }
    }

    interface Listener {
        fun onBottomButtonClick()
        fun onTopButtonClick() = Unit
        @JvmDefault
        fun onMiddleButtonClick() = Unit
        @JvmDefault
        fun onBackButtonClick() = Unit
    }

    data class BottomMenuParams(val hasSaveForLater: Boolean = true,
                                @DrawableRes val saveForLaterIcon: Int = R.drawable.ic_content_save,
                                @StringRes val saveForLaterTitle: Int = R.string.common_save_for_later,
                                @DrawableRes val continueIcon: Int = R.drawable.ic_playlist_edit,
                                @StringRes val continueTitle: Int = R.string.common_continue_editing,
                                @DrawableRes val deleteIcon: Int = R.drawable.ic_delete,
                                @StringRes val deleteTitle: Int = R.string.order_creation_step_activity_delete_order) {
        companion object {
            fun order() = BottomMenuParams()
            fun rentalReturn() = BottomMenuParams(hasSaveForLater = false, deleteIcon = R.drawable.ic_close, deleteTitle = R.string.return_activity_cancel_return)
            fun shipping() = BottomMenuParams(hasSaveForLater = false, deleteIcon = R.drawable.ic_close, deleteTitle = R.string.shipping_notice_cancel_shipping_notice)
            fun discrepancy() = BottomMenuParams(
                    saveForLaterIcon = R.drawable.ic_remove_circle_black_24dp,
                    saveForLaterTitle = R.string.discrepancy_add_shipment_shortage,
                    continueIcon = R.drawable.ic_add_circle_black_24dp,
                    continueTitle = R.string.discrepancy_add_existing_extras,
                    deleteIcon = R.drawable.ic_help_black_24dp,
                    deleteTitle = R.string.discrepancy_add_unknown_extras
            )
        }
    }

    enum class BottomType { MENU_ORDER, MENU_RETURN, MENU_SHIPPING, MENU_DISCREPANCY }
}
