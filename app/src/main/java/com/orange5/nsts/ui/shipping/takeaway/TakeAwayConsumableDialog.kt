package com.orange5.nsts.ui.shipping.takeaway

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.core.os.bundleOf
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.observe
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.BinNumber
import com.orange5.nsts.scan.ScanningDialog
import com.orange5.nsts.ui.base.OnClosedDialogListener
import com.orange5.nsts.ui.extensions.setOnClickListener
import com.orange5.nsts.ui.shipping.discrepancy.widget.BinNumberPickerView
import com.orange5.nsts.ui.shipping.discrepancy.widget.ShippingDiscrepancyView
import com.orange5.nsts.ui.shipping.receiving.ShippingPagerAdapter
import com.orange5.nsts.ui.shipping.repository.ReceivingNotice
import kotlinx.android.synthetic.main.dialog_shipping_container.*
import kotlinx.android.synthetic.main.view_bin_number_picker.view.*

class TakeAwayConsumableDialog : ScanningDialog() {

    private val viewModel: TakeAwayViewModel by activityViewModels { factory }
    private val adapter = ShippingPagerAdapter()

    override fun getLayoutResourceId() = R.layout.dialog_shipping_container

    override val notFoundMessage = R.string.shipping_notice_scan_bin

    private var selectedQuantity: Int = 0

    private lateinit var shortageView: ShippingDiscrepancyView
    private lateinit var extrasView: ShippingDiscrepancyView
    private lateinit var takeAwayView: BinNumberPickerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        closeCallback = targetFragment as? OnClosedDialogListener
        val noticeId = arguments?.getString(NOTICE_ID_KEY) ?: ""
        selectedQuantity = arguments?.getInt(POSITION_KEY) ?: 0
        viewModel.loadReceivingNotice(noticeId)
        viewModel.onDiscrepancyAdded.observe(this) { dismiss() }
        viewModel.onBinsLoaded.observe(this, ::submitBins)
        viewModel.onNoticeLoaded.observe(this, ::submitProduct)
        viewModel.binPosition.observe(this, ::scrollPickerToBinPosition)
        scanningViewModel.liveBinScanned.observe(this, ::setScannedBin)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cancel.setOnClickListener(::dismiss)
        viewPager.updateLayoutParams<LinearLayout.LayoutParams> { height = resources.getDimensionPixelOffset(R.dimen.take_away_consumable_pager_height) }
        takeAwayView = BinNumberPickerView(requireContext())
        shortageView = ShippingDiscrepancyView(requireContext())
        extrasView = ShippingDiscrepancyView(requireContext())
    }

    override fun setScannerIcon() {
        takeAwayView.scannerToggle?.let {
            it.visibility = View.VISIBLE
            val icon =
                    if (scanningViewModel.isScannerServiceActive() && scanningViewModel.isScannerActive) R.drawable.barcode_scan_on_ripple
                    else R.drawable.barcode_scan_off_ripple
            it.setImageResource(icon)
        }
    }

    private fun submitProduct(pair: Pair<ReceivingNotice?, Int>) {
        val notice = pair.first ?: return
        product.setValueText(notice.productNumber)
        setUpPager(notice)
        takeAwayView.selectBinPosition(pair.second)
    }

    private fun setUpPager(notice: ReceivingNotice) {
        viewPager.adapter = adapter
        adapter.setItems(getPagerItems(notice))

        tabLayout.setupWithViewPager(viewPager)
    }

    private fun getPagerItems(notice: ReceivingNotice): List<ShippingPagerAdapter.Item> {
        val count = notice.quantity.imported - notice.quantity.discrepancy
        takeAwayView.submitNumbers(count)
        takeAwayView.selectNumberPosition(if (selectedQuantity == 0) count else selectedQuantity)
        takeAwayView.setOnQueryListener(viewModel::searchBin)
        takeAwayView.setOnActionClick { bin, index ->
            viewModel.takeAwayConsumable(bin, index)
            dismiss()
        }

        shortageView.setDiscrepancyText(R.string.discrepancy_shortage_quantity_text)
        shortageView.isRental(notice.isRental())
        if (notice.quantity.remaining < 1) shortageView.notDiscrepancyAvailable()
        else shortageView.setOnActionClick { quantity, notes ->
            if (quantity > notice.quantity.remaining) shortageView.showCountError(notice.quantity.remaining)
            else viewModel.addShortages(notice, quantity, notes)
        }

        val items = mutableListOf(
                ShippingPagerAdapter.Item(takeAwayView, getString(R.string.receiving_init_item_title)),
                ShippingPagerAdapter.Item(shortageView, getString(R.string.discrepancy_shortage_title))
        )

        if (!notice.isRental()) {
            extrasView.setDiscrepancyText(R.string.discrepancy_extras_quantity_text)
            extrasView.isRental(notice.isRental())
            extrasView.setOnActionClick { quantity, notes -> viewModel.addExtras(notice, quantity, notes) }
            items.add(ShippingPagerAdapter.Item(extrasView, getString(R.string.discrepancy_extras_title)))
        }

        return items
    }

    private fun setScannedBin(bin: BinNumber) {
        viewModel.searchScannedBin(bin.binId)
    }

    private fun submitBins(bins: List<BinNumber>) {
        takeAwayView.submitBins(bins)
    }

    private fun scrollPickerToBinPosition(position: Int) {
        takeAwayView.selectBinPosition(position)
    }

    companion object {
        private const val NOTICE_ID_KEY = "notice_id_key"
        private const val WAS_SCANNED_ID = "was_scanned_key"
        private const val POSITION_KEY = "position_key"

        fun show(fragment: Fragment, noticeId: String, position: Int, wasScanned: Boolean = false) =
                TakeAwayConsumableDialog().apply {
                    arguments = bundleOf(NOTICE_ID_KEY to noticeId,
                            WAS_SCANNED_ID to wasScanned, POSITION_KEY to position)
                    setTargetFragment(fragment, 999)
                    show(fragment.parentFragmentManager, this::class.java.simpleName)
                }
    }


}