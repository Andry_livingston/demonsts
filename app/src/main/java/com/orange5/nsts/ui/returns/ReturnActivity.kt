package com.orange5.nsts.ui.returns

import android.graphics.Bitmap
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseActivity
import com.orange5.nsts.ui.base.TwoPageAdapter
import com.orange5.nsts.ui.extensions.toast
import com.orange5.nsts.ui.home.BottomMenuDialog
import com.orange5.nsts.ui.home.BottomMenuDialog.BottomType.MENU_RETURN
import com.orange5.nsts.ui.returns.signature.FinishReturnDialog
import com.orange5.nsts.ui.returns.signature.OnFinishListener
import com.orange5.nsts.util.SignatureManager
import kotlinx.android.synthetic.main.activity_return.*

abstract class ReturnActivity : BaseActivity(),
        BottomNavigationView.OnNavigationItemSelectedListener,
        FinishReturnDialog.Listener,
        BottomMenuDialog.Listener,
        OnFinishListener {

    protected var dialog: AlertDialog? = null

    private var bottomMenuDialog: BottomMenuDialog? = null

    protected abstract val viewModel: ReturnViewModel

    protected abstract val adapter: TwoPageAdapter

    override fun getLayoutResourceId(): Int = R.layout.activity_return

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.itemCount.observe(this, Observer(::onQuantityChanged))
        viewModel.badgeCount.observe(this, Observer(::setBadge))
        viewModel.liveRunSync.observe(this, Observer { runSync() })
        bottomNavigation.setOnNavigationItemSelectedListener(this)
        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) = Unit
            override fun onPageScrolled(position: Int, positionOffset: Float,
                                        positionOffsetPixels: Int) = Unit

            override fun onPageSelected(position: Int) {
                bottomNavigation.selectedItemId = if (position == 0) R.id.initItem else R.id.intermediateItem
                viewModel.handleTabSelection(TwoPageAdapter.TabType.values()[position])
            }
        })
    }

    override fun onGetSignature(signature: Bitmap) {
        viewModel.createReturn(SignatureManager.bitmapToByteArray(signature))
    }

    override fun onFinishProcessClick() {
        if (viewModel.allReturnItems.value.isNullOrEmpty()) showEmptyReturnDialog()
        else showSignatureDialog()
    }

    private fun showSignatureDialog() {
        val dialog = FinishReturnDialog.show(viewModel.rentalReturn.returnNumber,
                supportFragmentManager)
        dialog.setOnGetSignatureListener(this)
    }

    fun showEmptyReturnDialog() {
        if (dialog != null && dialog!!.isShowing) {
            dialog!!.dismiss()
        }
        dialog = AlertDialog.Builder(this)
                .setTitle(R.string.rental_return_activity_nothing_to_return)
                .setMessage(R.string.rental_return_activity_nothing_to_return_message)
                .setPositiveButton(android.R.string.ok) { d, _ -> d.dismiss() }
                .show()
    }

    private fun onQuantityChanged(quantity: Int?) {
        rentedItemCount.text = getString(R.string.common_n_items, quantity ?: 0)
    }

    private fun setBadge(count: Int?) {
        if (count != null && count > 0) bottomNavigation.getOrCreateBadge(R.id.intermediateItem).number = count
        else bottomNavigation.removeBadge(R.id.intermediateItem)
    }

    override fun onBackPressed() {
        if (viewModel.allReturnItems.value.isNullOrEmpty()) {
            finish()
            return
        }

        if (bottomMenuDialog == null || bottomMenuDialog?.isVisible == false) {
            bottomMenuDialog = BottomMenuDialog.show(supportFragmentManager, MENU_RETURN)
            return
        }
        super.onBackPressed()
    }

    override fun onBottomButtonClick() {
        finishReturn()
    }

    override fun onBackButtonClick() {
        this.toast(R.string.rental_return_activity_has_been_canceled)
    }

    protected fun finishReturn() {
        finish()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.initItem -> viewPager.setCurrentItem(TwoPageAdapter.TabType.INIT.id, true)
            R.id.intermediateItem -> viewPager.setCurrentItem(TwoPageAdapter.TabType.RESULT.id, true)
        }
        return true
    }
}

