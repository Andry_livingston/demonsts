package com.orange5.nsts.ui.shipping.discrepancy.list

import androidx.lifecycle.MutableLiveData
import com.orange5.nsts.ui.base.BaseViewModel
import com.orange5.nsts.ui.shipping.discrepancy.repository.Discrepancy
import com.orange5.nsts.ui.shipping.discrepancy.repository.DiscrepancyRepository
import io.reactivex.Flowable
import io.reactivex.Observable
import javax.inject.Inject

class DiscrepancyListViewModel @Inject constructor(private val discrepancyRepository: DiscrepancyRepository) : BaseViewModel() {

    val discrepancies = MutableLiveData<List<Discrepancy>>()

    fun loadDiscrepancies() {
        callJobComputation(
                { discrepancyRepository.loadDiscrepancies() },
                { discrepancies.postValue(it) })
    }
}