package com.orange5.nsts.ui.order;

import android.app.AlertDialog;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.orange5.nsts.R;
import com.orange5.nsts.ui.base.BaseActivity;
import com.orange5.nsts.ui.base.picker.Item;
import com.orange5.nsts.ui.base.picker.NumberPickerAdapter;
import com.orange5.nsts.ui.order.search.ProductWrapper;
import com.orange5.nsts.util.CollectionUtils;
import com.orange5.nsts.util.UI;
import com.orange5.nsts.util.view.dialogs.BaseDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import butterknife.BindView;
import butterknife.OnTextChanged;
import com.orange5.nsts.util.view.picker.PickerView;
import com.orange5.nsts.util.view.picker.ValuePickerAdapter;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class EditQuantityDialog extends BaseDialog {

    private static final int MAX_OVERSELL = 10001;
    @BindView(R.id.actionButton)
    TextView actionButton;
    @BindView(R.id.customQuantity)
    ImageButton customQuantity;
    @BindView(R.id.editQuantity)
    EditText editQuantity;
    @BindView(R.id.quantityPicker)
    PickerView picker;
    @BindView(R.id.totalQuantity)
    TextView totalQuantity;
    @BindView(R.id.productNumber)
    TextView productNumber;
    @BindView(R.id.isBackorder)
    CheckBox isBackorder;

    @OnTextChanged(R.id.editQuantity)
    void onQuantityChanged(CharSequence s, int start, int count, int after) {
        String value = s.toString();
        Integer number = parseToIntOrNull(value);
        if (number == null || number > maxQuantity + getOverflow(allowOverflow)) {
            number = handleMaxEnteredValue(value);
            editQuantity.setText(String.valueOf(number));
        } else if (start == 0 && after == 1 && s.length() > 1) {
            editQuantity.setText(String.valueOf(number.toString().charAt(0)));
            number = parseToIntOrNull(String.valueOf(s.charAt(0)));
            editQuantity.setSelection(editQuantity.getText().length());
        }
        setActionButtonTitle(number);
    }

    private final int maxQuantity;
    private final int defaultSelection;
    private final boolean allowOverflow;
    private final boolean hasBackorder;
    private final String number;
    private final BiConsumer<Integer, Boolean> selectionConsumer;

    private List<Integer> items = new ArrayList<>();

    public static EditQuantityDialog create(ProductWrapper wrapper,
                                            BiConsumer<Integer, Boolean> selectionConsumer) {
        return new EditQuantityDialog(wrapper, selectionConsumer);
    }

    private EditQuantityDialog(ProductWrapper wrapper, BiConsumer<Integer, Boolean> selectionConsumer) {
        super(BaseActivity.getActive());
        this.maxQuantity = wrapper.getAvailableQuantity();
        this.defaultSelection = wrapper.getCurrentSelected() == 0 ? 1 : wrapper.getCurrentSelected();
        this.allowOverflow = wrapper.isConsumable();
        this.selectionConsumer = selectionConsumer;
        this.number = wrapper.getProduct().getProductNumber();
        this.hasBackorder = wrapper.isConsumable() && wrapper.isBackorder();
        init();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.dialog_quantity;
    }

    private void init() {
        dialog = new AlertDialog.Builder(activity)
                .setView(view)
                .create();
        setUpPicker();
        setListeners();
        setBackorder();
        totalQuantity.setText(activity.getString(R.string.edit_quantity_dialog_total_quantity, maxQuantity));
        if (number != null) {
            productNumber.setVisibility(VISIBLE);
            productNumber.setText(activity.getString(R.string.edit_quantity_dialog_product_number, number));
        } else {
            productNumber.setVisibility(GONE);
        }
        show();
    }

    private void setBackorder() {
        int visibility = picker.getSelectedItemPosition() > maxQuantity ? VISIBLE : GONE;
        isBackorder.setVisibility(visibility);
        isBackorder.setChecked(hasBackorder);
    }

    private void setListeners() {
        customQuantity.setOnClickListener(v -> {
            if (editQuantity.getVisibility() == GONE) {
                editQuantity.requestFocus();
                UI.openKeyboard(editQuantity);
                editQuantity.setHint(String.valueOf(items.get(picker.getSelectedItemPosition())));
                editQuantity.setVisibility(VISIBLE);
                picker.setVisibility(GONE);
            } else {
                UI.closeKeyboard(editQuantity);
                editQuantity.setVisibility(GONE);
                picker.setVisibility(VISIBLE);
                int position = handleMaxEnteredValue(editQuantity.getText().toString()) - 1;
                setPickerDefaultValue(position);
            }
        });
        actionButton.setOnClickListener(v -> {
            UI.closeKeyboard(editQuantity);
            int currentValue;
            if (editQuantity.getVisibility() == VISIBLE) {
                currentValue = handleMaxEnteredValue(editQuantity.getText().toString());
            } else {
                currentValue = items.get(picker.getSelectedItemPosition());
            }
            selectionConsumer.accept(currentValue, isBackorder.isChecked());
            dismiss();
        });
    }

    private void setUpPicker() {
        items = CollectionUtils.rangeClosedList(1, maxQuantity + getOverflow(allowOverflow));
        NumberPickerAdapter adapter = new NumberPickerAdapter();
        picker.setAdapter(adapter);
        adapter.submitItems(items);
        adapter.setConsumer(count -> {
            setActionButtonTitle(count.getValue());
            setIsBackorderVisibility(count.getValue());
        });

        if (allowOverflow) {
            picker.setOverflowPosition(maxQuantity);
        }
        setPickerDefaultValue(items.indexOf(defaultSelection));
        setActionButtonTitle(defaultSelection);

    }

    private void setIsBackorderVisibility(int count) {
        int visibility = allowOverflow && count > maxQuantity ? VISIBLE : GONE;
        if (allowOverflow && count <= maxQuantity) isBackorder.setChecked(false);
        isBackorder.setVisibility(visibility);
    }

    private void setActionButtonTitle(int count) {
        actionButton.setText(BaseActivity.getActive().getResources().getQuantityString(R.plurals.add_n_items, count, count));
    }

    private void setPickerDefaultValue(int value) {
        picker.setSelectedItemPosition(value);
    }

    private int handleMaxEnteredValue(String value) {
        int maxValue = maxQuantity + getOverflow(allowOverflow);
        Integer number = parseToIntOrNull(value);
        if (number != null) {
            if (number > maxValue) {
                number = maxValue;
            }
        } else {
            number = 0;
        }
        return number;
    }

    private Integer parseToIntOrNull(String value) {
        Integer number;
        try {
            number = Integer.valueOf(value);
        } catch (NumberFormatException e) {
            number = null;
        }
        return number;
    }

    private int getOverflow(boolean allowed) {
        return allowed ? MAX_OVERSELL : 0;
    }


}
