package com.orange5.nsts.ui.report.closed

import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.orange5.nsts.R
import com.orange5.nsts.data.db.base.OrderItem
import com.orange5.nsts.ui.base.BaseFragment
import com.orange5.nsts.ui.order.full.pickup.OrderDetailsDialog
import com.orange5.nsts.ui.report.ReportViewModel
import kotlinx.android.synthetic.main.fragment_return_report.*

class ClosedOrderReportFragment : BaseFragment() {

    private val viewModel: ReportViewModel by activityViewModels { vmFactory }
    private val adapter = OrderItemsAdapter()

    override fun getLayoutResourceId(): Int = R.layout.fragment_closed_report

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.orderItems.observe(this, Observer(::updateList))
        viewModel.itemCount.observe(this, Observer(::onQuantityChanged))
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        itemsRecycler.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) = inflater.inflate(R.menu.info, menu)

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
            when (item.itemId) {
                R.id.orderInfo -> {
                    showInformDialog(requireContext())
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    private fun onQuantityChanged(quantity: Int?) {
        itemCount.text = getString(R.string.common_n_items, quantity ?: 0)
    }

    private fun updateList(items: List<OrderItem>?) {
        if (userVisibleHint) viewModel.itemCount.value = (items?.size ?: 0)
        if (items != null) adapter.submitList(items)
    }


    private fun showInformDialog(context: Context) {
        OrderDetailsDialog.show(context, viewModel.reportOrder) {}
    }
}