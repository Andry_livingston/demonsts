package com.orange5.nsts.ui.base

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.R.id.design_bottom_sheet
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

abstract class BaseBottomSheetDialogFragment : BottomSheetDialogFragment(), HasAndroidInjector {

    @Inject
    lateinit var injector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @get:LayoutRes
    abstract val layoutResId: Int

    var fullscreen: Boolean = false

    var draggable: Boolean = true
        set(value) {
            field = value
            behavior()?.applyDraggableBehavior()
        }

    override fun androidInjector(): AndroidInjector<Any> = injector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(layoutResId, container, false)

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setOnShowListener {
            val bottomSheet = bottomSheet(it) ?: return@setOnShowListener
            val behavior = BottomSheetBehavior.from(bottomSheet)

            bottomSheet.addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
                behavior?.applyDraggableBehavior(bottomSheet)
            }

            behavior?.skipCollapsed = true
            behavior?.state = BottomSheetBehavior.STATE_EXPANDED

            if (fullscreen) setupFullHeight(bottomSheet)
        }
        return dialog
    }

    private fun BottomSheetBehavior<View>.applyDraggableBehavior(bottomSheet: View? = bottomSheet()) {
        if (bottomSheet == null) return
        isHideable = draggable
        peekHeight = if (draggable) 0 else bottomSheet.height
    }

    fun blockDialog(isBlocked: Boolean) {
        isCancelable = !isBlocked
        dialog?.setCanceledOnTouchOutside(!isBlocked)
        draggable = !isBlocked
    }

    private fun bottomSheet(dialogInterface: DialogInterface? = dialog) =
        (dialogInterface as? BottomSheetDialog)?.findViewById<View>(design_bottom_sheet)

    private fun behavior(): BottomSheetBehavior<View>? {
        val bottomSheet = bottomSheet() ?: return null
        return BottomSheetBehavior.from(bottomSheet)
    }

    private fun setupFullHeight(bottomSheet: View) {
        val behavior = BottomSheetBehavior.from(bottomSheet)
        val layoutParams = bottomSheet.layoutParams

        val windowHeight = getWindowHeight()
        if (layoutParams != null) {
            layoutParams.height = windowHeight
        }
        bottomSheet.layoutParams = layoutParams
        behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    private fun getWindowHeight(): Int {
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics.heightPixels
    }
}