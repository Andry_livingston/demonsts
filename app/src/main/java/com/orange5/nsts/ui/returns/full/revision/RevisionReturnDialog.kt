package com.orange5.nsts.ui.returns.full.revision

import android.os.Bundle
import androidx.fragment.app.activityViewModels
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.ui.base.OnClosedDialogListener
import com.orange5.nsts.ui.returns.BaseReturnDialog
import com.orange5.nsts.ui.base.TwoPageAdapter.TabType

class RevisionReturnDialog : BaseReturnDialog() {

    override val viewModel: RevisionViewModel by activityViewModels { factory }

    override val args: Bundle? by lazy { arguments }

    override val notFoundMessage = R.string.rental_return_activity_scan_bin

    override fun getLayoutResourceId(): Int = R.layout.dialog_rental_return

    companion object {
        fun newInstance(item: OrderRentalItem,
                        type: TabType,
                        wasScanned: Boolean = false,
                        closeCallback: OnClosedDialogListener) = RevisionReturnDialog()
                .apply {
                    arguments = Bundle().also {
                        it.putString(ORDER_ITEM_ID, item.orderItemId)
                        it.putBoolean(WAS_SCANNED_ID, wasScanned)
                        it.putSerializable(TYPE_ID, type)
                    }
                    this.closeCallback = closeCallback
                }
    }
}
