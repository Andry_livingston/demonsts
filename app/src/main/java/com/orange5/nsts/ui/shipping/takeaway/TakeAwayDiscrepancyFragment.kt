package com.orange5.nsts.ui.shipping.takeaway

import androidx.fragment.app.activityViewModels
import com.orange5.nsts.ui.order.search.ProductWrapper
import com.orange5.nsts.ui.shipping.discrepancy.DiscrepancyFragment
import com.orange5.nsts.ui.shipping.discrepancy.add.AddDiscrepancyDetailsDialog
import com.orange5.nsts.ui.shipping.discrepancy.add.AddDiscrepancyDialog
import com.orange5.nsts.ui.shipping.discrepancy.add.AddExistingExtrasDetailsDialog
import com.orange5.nsts.ui.shipping.discrepancy.add.AddUnknownExtrasDialog
import com.orange5.nsts.ui.shipping.discrepancy.add.EditDiscrepancyDetailsDialog
import com.orange5.nsts.ui.shipping.discrepancy.add.EditUnknownExtrasDialog
import com.orange5.nsts.ui.shipping.discrepancy.repository.Discrepancy
import com.orange5.nsts.ui.shipping.repository.ReceivingNotice

class TakeAwayDiscrepancyFragment : DiscrepancyFragment() {
    override val viewModel by activityViewModels<TakeAwayViewModel> { vmFactory }

    override fun onTopButtonClick() {
        AddDiscrepancyDialog.fromTakeAway(this, Discrepancy.Type.SHIPMENT_SHORTAGE)
    }

    override fun onItemClick(item: Discrepancy) {
        if (item.type == Discrepancy.Type.NON_EXISTING_PRODUCT) EditUnknownExtrasDialog.fromTakeAway(parentFragmentManager, item.id)
        else EditDiscrepancyDetailsDialog.fromTakeAway(parentFragmentManager, item.id)
    }

    override fun onAddDiscrepancyClick(item: ReceivingNotice, type: Discrepancy.Type) {
        AddDiscrepancyDetailsDialog.fromTakeAway(parentFragmentManager, item.id, type)
    }

    override fun showExistingExtrasDetailsDialog(product: ProductWrapper) {
        val discrepancy = Discrepancy.from(product.product, viewModel.transferNo)
        AddExistingExtrasDetailsDialog.fromTakeAway(parentFragmentManager, discrepancy)
    }

    override fun onBottomButtonClick() {
        AddUnknownExtrasDialog.fromTakeAway(parentFragmentManager)
    }
}