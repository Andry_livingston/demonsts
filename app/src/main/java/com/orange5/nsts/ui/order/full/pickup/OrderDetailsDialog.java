package com.orange5.nsts.ui.order.full.pickup;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;

import com.orange5.nsts.R;
import com.orange5.nsts.service.order.OrderType;
import com.orange5.nsts.ui.order.quick.dialogs.FinishOrderView;
import com.orange5.nsts.data.db.entities.Order;
import com.orange5.nsts.service.order.OrderStatus;

public class OrderDetailsDialog {

    public interface FinishOrderListener {
        void finishOrder(Bitmap signatureBitmap);
    }

    public static void show(Context context, Order order, FinishOrderListener callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        FinishOrderView view = new FinishOrderView(context);
        view.initWithOrder(OrderType.FULL_ORDER, order);
        builder.setView(view);

        if (OrderStatus.open.matches(order)) {
            builder.setPositiveButton(R.string.common_close, ((dialog, which) -> dialog.dismiss()));
            builder.show();
            return;
        }

        if (OrderStatus.closed.matches(order)
                || OrderStatus.readyNotShipped.matches(order)
                || OrderStatus.active.matches(order)) {
            builder.setPositiveButton(R.string.common_close, ((dialog, which) -> dialog.dismiss()));
            builder.setCancelable(false);
            builder.show();
            return;
        }

        if (OrderStatus.pickUp.matches(order)) {
            builder.setPositiveButton(R.string.common_finish_order, (dialog, which) -> {
                callback.finishOrder(view.getSignatureBitmap());
                builder.setOnCancelListener(dialog1 -> { });
                dialog.dismiss();
            });
            builder.setNegativeButton(R.string.order_details_dialog_back_to_order, (dialog, which) -> dialog.dismiss());
            builder.show();
        }
    }
}
