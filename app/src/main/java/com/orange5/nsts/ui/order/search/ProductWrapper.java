package com.orange5.nsts.ui.order.search;

import androidx.annotation.StringRes;

import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.data.db.base.OrderProduct;
import com.orange5.nsts.data.db.entities.ConsumableProduct;

public final class ProductWrapper {

    private final OrderProduct product;
    private @StringRes int message = 0;
    private boolean isBackorder = false;
    private int availableQuantity;
    private int currentSelected;

    public static ProductWrapper from(OrderItem item) {
        return new ProductWrapper(item);
    }

    public ProductWrapper(OrderProduct product) {
        this.product = product;
        availableQuantity = getTotalQuantity();
    }

    public ProductWrapper(OrderItem item) {
        if (item.isConsumable()) {
            this.product = item.asConsumable().getConsumableProduct();
            this.isBackorder = item.asConsumable().isBackorder();
        } else {
            this.product = item.asRental().getRentalProduct();
        }
        currentSelected = item.getQuantity();
        availableQuantity = getTotalQuantity();
    }

    public boolean isBackorder() {
        return isBackorder;
    }

    public void setBackorder(boolean backorder) {
        isBackorder = backorder;
    }

    public int getAvailableQuantity() {
        return availableQuantity < 0 ? 0 : availableQuantity;
    }

    public void setAvailableQuantity(int selectedQuantity) {
        this.availableQuantity = getTotalQuantity() - selectedQuantity;
    }

    public @StringRes
    int getMessage() {
        return message;
    }

    public void setError(@StringRes int message) {
        this.message = message;
    }

    public boolean hasError() {
        return message != 0;
    }

    public OrderProduct getProduct() {
        return product;
    }

    public boolean isConsumable() {
        return product instanceof ConsumableProduct;
    }

    public int getTotalQuantity() {
        return product.getBinInformation().getTotalQuantity();
    }

    public int getCurrentSelected() {
        return currentSelected;
    }
}