package com.orange5.nsts.ui.shipping

import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.Customer
import com.orange5.nsts.ui.base.ActionLiveData
import com.orange5.nsts.ui.base.BaseViewModel
import com.orange5.nsts.ui.extensions.refresh
import com.orange5.nsts.ui.shipping.discrepancy.repository.Discrepancy
import com.orange5.nsts.ui.shipping.discrepancy.repository.Discrepancy.Type.SHIPMENT_EXTRAS
import com.orange5.nsts.ui.shipping.discrepancy.repository.Discrepancy.Type.SHIPMENT_SHORTAGE
import com.orange5.nsts.ui.shipping.discrepancy.repository.DiscrepancyRepository
import com.orange5.nsts.ui.shipping.repository.ReceivingNotice
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

abstract class ShippingProcessViewModel(private val discrepancyRepository: DiscrepancyRepository) : BaseViewModel() {

    protected var customer: Customer? = null

    val discrepancyItems = MutableLiveData<MutableList<Discrepancy>>()
    val receivingItems = MutableLiveData<List<ReceivingNotice>>()

    val receivingNotice = MutableLiveData<ReceivingNotice>()
    val discrepancyItem = ActionLiveData<Discrepancy>()
    val onEmptyProcessFinished = ActionLiveData<Unit>()
    val invalidDiscrepanciesCount = ActionLiveData<Int>()

    val searchedItems = MutableLiveData<List<ReceivingNotice>>()

    val onDiscrepancyAdded = ActionLiveData<Unit>()

    val scannedItemNotFound = ActionLiveData<Int>()

    abstract val receivingBadges: LiveData<Int>

    val discrepancyBadges = discrepancyItems.map { it.sumBy { discrepancy -> discrepancy.quantity } }
    val title = MutableLiveData<String>()

    var transferNo: String = ""

    abstract fun finishReceivingProcess()

    open fun loadReceivingNotice(noticeId: String) {
        val item = receivingItems.value?.find { it.id == noticeId } ?: return
        receivingNotice.value = item
    }

    open fun loadDiscrepancyItem(id: String) {
        discrepancyItem.value = discrepancyItems.value?.find { it.id == id }
    }

    protected open fun refresh() = receivingItems.refresh()

    protected open fun onScanFails(@StringRes errorRes: Int) {
        scannedItemNotFound.value = errorRes
    }

    fun addShortages(uun: String, itemCount: Int, notes: String) {
        val notice = receivingItems.value?.find { it.uniqueUnitNumber == uun } ?: return
        addShortages(notice, itemCount, notes)
    }

    fun addShortages(item: ReceivingNotice, itemCount: Int, notes: String) {
        val discrepancies = discrepancyItems.value ?: mutableListOf()
        if (isValid(itemCount, item, itemCount)) {
            val discrepancy = Discrepancy.from(item, SHIPMENT_SHORTAGE)
            discrepancy.quantity = itemCount
            discrepancy.comment = notes
            discrepancies.add(discrepancy)
            item.addDiscrepancy(itemCount)
            refresh()
            discrepancyItems.value = discrepancies
            onDiscrepancyAdded.value = Unit
        }
    }

    fun addExtras(item: ReceivingNotice, itemCount: Int, notes: String) {
        val discrepancies = discrepancyItems.value ?: mutableListOf()
        val discrepancy = Discrepancy.from(item, SHIPMENT_EXTRAS)
        discrepancy.quantity = itemCount
        discrepancy.comment = notes
        discrepancies.add(discrepancy)
        discrepancyItems.value = discrepancies
        onDiscrepancyAdded.value = Unit
    }

    fun addSearchObservable(emitted: Observable<String>) {
        addDisposable(emitted
                .debounce(300, TimeUnit.MILLISECONDS)
                .subscribe(::searchInShortages, ::onError)
        )
    }

    private fun searchInShortages(query: String) {
        val receiving = receivingItems.value ?: return
        searchedItems.postValue(receiving.filter {
            it.productNumber.contains(query, ignoreCase = true)
                    || it.productDescription.contains(query, ignoreCase = true)
                    || it.uniqueUnitNumber.contains(query, ignoreCase = true)
        })
    }

    fun onFinishProcessClick() {
        finishReceivingProcess()
    }

    fun removeDiscrepancy(item: Discrepancy) = if (item.type == SHIPMENT_SHORTAGE) removeShortages(item) else removeExtras(item)

    fun onEditDiscrepancy(item: Discrepancy) = if (item.type == SHIPMENT_SHORTAGE) editShortages(item) else editExtras(item)

    private fun removeExtras(item: Discrepancy) {
        val discrepancies = discrepancyItems.value ?: return
        discrepancies.remove(item)
        discrepancyItems.value = discrepancyItems.value
    }

    private fun editExtras(item: Discrepancy) {
        if (item.quantity == 0) removeExtras(item)
        else discrepancyItems.value = discrepancyItems.value
        onDiscrepancyAdded.value = Unit
    }

    private fun editShortages(item: Discrepancy) =
            if (item.quantity == 0) {
                removeShortages(item)
                onDiscrepancyAdded.value = Unit
            } else updateShortages(item)

    private fun updateShortages(item: Discrepancy) {
        val discrepancies = discrepancyItems.value ?: return
        val receiving = receivingItems.value ?: return
        val receivingItem = receiving.find { it.productId == item.productId } ?: return
        val discrepanciesCount = discrepancies.filter { it.productId == item.productId }.sumBy { it.quantity }
        if (isValid(discrepanciesCount, receivingItem, item.quantity)) {
            receivingItem.editDiscrepancy(discrepanciesCount)
            refresh()
            discrepancyItems.value = discrepancies
            onDiscrepancyAdded.value = Unit
        }
    }

    private fun removeShortages(item: Discrepancy) {
        val discrepancies = discrepancyItems.value ?: return
        val receiving = receivingItems.value ?: return
        val receivingItem = receiving.find { it.productId == item.productId } ?: return

        val discrepanciesCount = discrepancies.filter { it.productId == item.productId }.sumBy { it.quantity }
        receivingItem.editDiscrepancy(discrepanciesCount)
        discrepancies.remove(item)
        discrepancyItems.value = discrepancies
        refresh()
    }

    private fun isValid(discrepanciesCount: Int, receivingItem: ReceivingNotice, currentCount: Int): Boolean {
        val delta = receivingItem.quantity.imported - receivingItem.quantity.processed - discrepanciesCount
        if (delta < 0) {
            invalidDiscrepanciesCount.value = currentCount + delta
            return false
        }
        return true
    }

    fun addUnknownExtras(count: Int, description: String, notes: String) {
        val discrepancy = Discrepancy.unknown(count, description, notes, transferNo)
        addExistingExtras(discrepancy)
    }

    fun addExistingExtras(discrepancy: Discrepancy) {
        val discrepancies = discrepancyItems.value ?: mutableListOf()
        discrepancies.add(discrepancy)
        discrepancyItems.value = discrepancies
    }

    fun onDiscrepancyScanned(barcode: String, type: Discrepancy.Type) {
        val item = receivingItems.value?.find { it.productNumber == barcode || it.uniqueUnitNumber == barcode }
        when {
            item == null -> scannedItemNotFound.value = R.string.discrepancy_scanned_not_found
            type == SHIPMENT_SHORTAGE -> addShortages(item, 1, "")
            else -> addExtras(item, 1, "")
        }
    }
}
