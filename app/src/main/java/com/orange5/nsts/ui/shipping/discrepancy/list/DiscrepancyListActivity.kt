package com.orange5.nsts.ui.shipping.discrepancy.list

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseActivity
import com.orange5.nsts.ui.extensions.addFragment

class DiscrepancyListActivity : BaseActivity() {

    private val viewModel: DiscrepancyListViewModel by viewModels { vmFactory }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.loadDiscrepancies()
        title = getString(R.string.discrepancy)
        if (savedInstanceState == null) {
            supportFragmentManager.addFragment(DiscrepancyListFragment(), shouldAddToBackStack = false)
        }
    }

    override fun getLayoutResourceId(): Int = R.layout.activity_discrepancies

    companion object {
        @JvmStatic
        fun newIntent(context: Context) = Intent(context, DiscrepancyListActivity::class.java)
    }
}