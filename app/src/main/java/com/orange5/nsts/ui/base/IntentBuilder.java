package com.orange5.nsts.ui.base;

import android.app.Activity;
import android.content.Intent;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;

public class IntentBuilder {
    private Intent intent;
    private Activity activity;

    public IntentBuilder(Activity context, Class<? extends BaseActivity> activityClass) {
        this.activity = context;
        intent = new Intent(context, activityClass);
    }

    public IntentBuilder add(String name, Integer object) {
        intent.putExtra(name, object);
        return this;
    }

    public IntentBuilder add(String name, String object) {
        intent.putExtra(name, object);
        return this;
    }

    public IntentBuilder addFlags(int flags) {
        intent.addFlags(flags);
        return this;
    }

    public IntentBuilder clearTask() {
        intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK);
        return this;
    }

    public void start() {
        activity.startActivity(intent);
    }

    public void startForResult(int requestCode) {
        activity.startActivityForResult(intent, requestCode);
    }
}