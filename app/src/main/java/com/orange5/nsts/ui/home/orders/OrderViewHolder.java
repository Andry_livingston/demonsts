package com.orange5.nsts.ui.home.orders;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.text.SpannableString;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.entities.Order;
import com.orange5.nsts.data.db.entities.OrderConsumableItem;
import com.orange5.nsts.service.order.OrderStatus;
import com.orange5.nsts.ui.base.BaseViewHolder;
import com.orange5.nsts.util.CollectionUtils;
import com.orange5.nsts.util.format.DateFormatter;

import java.util.LinkedHashMap;
import java.util.Map;

public class OrderViewHolder extends BaseViewHolder {

    private Order currentOrder;
    private final Context context;

    public OrderViewHolder(Context context) {
        this.context = context;
    }

    public void setCurrentOrder(Order currentOrder) {
        this.currentOrder = currentOrder;
    }

    public String getFormattedOrderDate() {
        String dateString = currentOrder.getOrderCompleteDate();
        if (dateString == null) {
            dateString = currentOrder.getOrderDate();
        }
        return DateFormatter.convertDbDateToUI(dateString);
    }

    public SpannableString getTypeAndNumber() {
        return getTypeAndNumber("");
    }

    public SpannableString getTypeAndNumber(String highlight) {
        String result = currentOrder.getOrderNumber();//+ " Sales";
        return highlightBackground(result, highlight);
    }

    public String getStatusText() {
        return context.getString(OrderStatus.values()[currentOrder.getOrderStatusId()].getName());
    }


    private Map<OrderStatus, GradientDrawable> cachedDrawables = new LinkedHashMap<>();

    public Drawable getStatusBackground() {
        OrderStatus orderStatus = OrderStatus.of(currentOrder);
        GradientDrawable result = cachedDrawables.get(orderStatus);
        if (result == null) {
            result = new GradientDrawable();
            result.setCornerRadius(context.getResources().getDimensionPixelSize(R.dimen.common_3dp));
            result.setColor(context.getColor(orderStatus.getHighlightColor()));
            cachedDrawables.put(orderStatus, result);
        }
        return result;
    }

    public boolean hasBackorder() {
        boolean hasBackorder = false;
        if (currentOrder != null &&
                currentOrder.getOrderStatusId() != OrderStatus.closed.getId() &&
                CollectionUtils.isNotEmpty(currentOrder.getConsumableItems())) {
            hasBackorder = currentOrder.getConsumableItems().stream().anyMatch(OrderConsumableItem::isBackorder);
        }
        return hasBackorder;
    }

    public boolean hasConsumable() {
        return currentOrder != null && CollectionUtils.isNotEmpty(currentOrder.getConsumableItems());
    }

    public boolean hasRental() {
        return currentOrder != null && CollectionUtils.isNotEmpty(currentOrder.getRentalItems());
    }

    public SpannableString getCustomerFullName(String highlight) {
        String result = currentOrder.getCustomer().getLName() + " " + currentOrder.getCustomer().getFName();
        return highlightBackground(result, highlight);
    }
}
