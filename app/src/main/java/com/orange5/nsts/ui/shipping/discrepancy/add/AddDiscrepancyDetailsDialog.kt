package com.orange5.nsts.ui.shipping.discrepancy.add

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.observe
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseDialogFragment
import com.orange5.nsts.ui.extensions.setOnClickListener
import com.orange5.nsts.ui.shipping.ShippingProcessViewModel
import com.orange5.nsts.ui.shipping.discrepancy.repository.Discrepancy
import com.orange5.nsts.ui.shipping.receiving.ReceivingViewModel
import com.orange5.nsts.ui.shipping.repository.ReceivingNotice
import com.orange5.nsts.ui.shipping.takeaway.TakeAwayViewModel
import kotlinx.android.synthetic.main.dialog_edit_discrepancy.*

abstract class AddDiscrepancyDetailsDialog : BaseDialogFragment() {

    abstract val viewModel: ShippingProcessViewModel

    override fun getLayoutResourceId(): Int = R.layout.dialog_edit_discrepancy

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val id = arguments?.getString(NOTICE_ID_KEY) ?: ""
        viewModel.loadReceivingNotice(id)
        viewModel.receivingNotice.observe(this, ::setUpView)
        viewModel.invalidDiscrepanciesCount.observe(this, ::onInvalidDiscrepancyCount)
        viewModel.onDiscrepancyAdded.observe(this) { dismiss() }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cancel.setOnClickListener(::dismiss)
    }

    protected fun setUpView(item: ReceivingNotice) {
        val isShortage = arguments?.getSerializable(DISCREPANCY_TYPE_KEY) == Discrepancy.Type.SHIPMENT_SHORTAGE
        productNumber.setValueText(item.productNumber)
        discrepancyView.setQuantity(1)
        discrepancyView.isRental(item.isRental())
        discrepancyView.setDiscrepancyText(
                if (isShortage) R.string.discrepancy_shortage_quantity_text
                else R.string.discrepancy_extras_quantity_text
        )
        discrepancyView.setOnActionClick { count, notes ->
            if (isShortage) viewModel.addShortages(item, count, notes)
            else viewModel.addExtras(item, count, notes)
        }
    }

    private fun onInvalidDiscrepancyCount(count: Int) {
        discrepancyView.showCountError(count)
    }

    companion object {
        @JvmStatic
        fun fromReceiving(fragmentManager: FragmentManager,
                          noticeId: String,
                          type: Discrepancy.Type) = show(ReceivingAddDiscrepancyDetailsDialog(), fragmentManager, noticeId, type)

        @JvmStatic
        fun fromTakeAway(fragmentManager: FragmentManager,
                         noticeId: String,
                         type: Discrepancy.Type) = show(TakeAwayAddDiscrepancyDetailsDialog(), fragmentManager, noticeId, type)

        private fun show(dialog: AddDiscrepancyDetailsDialog,
                         fragmentManager: FragmentManager,
                         noticeId: String,
                         type: Discrepancy.Type) =
                dialog.apply {
                    arguments = bundleOf(NOTICE_ID_KEY to noticeId, DISCREPANCY_TYPE_KEY to type)
                    show(fragmentManager, this::class.java.simpleName)
                }

        private const val NOTICE_ID_KEY = "notice_id_key"
        private const val DISCREPANCY_TYPE_KEY = "discrepancy_type_key"
    }
}

class ReceivingAddDiscrepancyDetailsDialog : AddDiscrepancyDetailsDialog() {
    override val viewModel by activityViewModels<ReceivingViewModel> { factory }
}

class TakeAwayAddDiscrepancyDetailsDialog : AddDiscrepancyDetailsDialog() {
    override val viewModel by activityViewModels<TakeAwayViewModel> { factory }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.onNoticeLoaded.observe { pair -> pair.first?.let { setUpView(it) } }
    }
}