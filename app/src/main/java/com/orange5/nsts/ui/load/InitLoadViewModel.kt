package com.orange5.nsts.ui.load

import androidx.lifecycle.MutableLiveData
import com.orange5.nsts.api.load.InitLoadClient
import com.orange5.nsts.app.modules.ApiModule
import com.orange5.nsts.service.base.InitInsertFactory
import com.orange5.nsts.service.base.SyncObjectService
import com.orange5.nsts.sync.InitLoadQueries
import com.orange5.nsts.ui.base.ActionLiveData
import com.orange5.nsts.ui.base.BaseViewModel
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class InitLoadViewModel @Inject constructor(private val apiModule: ApiModule,
                                            private val factory: InitInsertFactory,
                                            private val syncObjectService: SyncObjectService) : BaseViewModel() {

    private val syncClient: InitLoadClient by lazy { apiModule.initLoadClient() }

    private var counter = 0

    private val errorList: MutableList<String> = mutableListOf()
    val entityLoaded = ActionLiveData<String>()
    val progress = ActionLiveData<Boolean>()
    val report = MutableLiveData<List<String>>()

    fun startLoading() {
        progress.value = true
        InitLoadQueries.values().forEach(::loadAndSaveData)
    }

    fun cancelLoading() {
        clearDisposable()
    }

    private fun loadAndSaveData(query: InitLoadQueries) {
        addDisposable(rxCompletableCreate { factory.uploadData(query, syncClient.initLoad(query.entity)) }
                .subscribeOn(Schedulers.io())
                .doOnComplete { syncObjectService.deleteAll() }
                .subscribe({ onSuccess(query.entity) }, { error -> onError(query.entity, error) }))
    }

    private fun onSuccess(currentTable: String) {
        val message = "$currentTable was successfully loaded"
        Timber.d(message)
        entityLoaded.postValue(message)
        countQueries()
    }

    private fun onError(table: String, throwable: Throwable) {
        val errorMessage = "Init load fails during loading $table table. Cause ${throwable.message} See logs for details"
        errorList.add(errorMessage)
        Timber.d(throwable, errorMessage)
        countQueries()
    }

    private fun countQueries() {
        counter++
        if (counter >= InitLoadQueries.values().size){
            report.postValue(errorList)
            progress.postValue(false)
        }
    }

}