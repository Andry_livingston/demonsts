package com.orange5.nsts.ui.returns.quick

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.orange5.nsts.ui.base.TwoPageAdapter
import com.orange5.nsts.ui.base.TwoPageAdapter.TabType.INIT
import com.orange5.nsts.ui.base.TwoPageAdapter.TabType.RESULT
import com.orange5.nsts.ui.returns.ReturnActivity

class QuickReturnActivity : ReturnActivity() {

    override val adapter by lazy {
        val map = mapOf(INIT to QuickRentalItemsFragment(), RESULT to QuickReturnedItemsFragment())
        TwoPageAdapter(map, supportFragmentManager)
    }

    override val viewModel: QuickReturnViewModel by viewModels { vmFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.returnFinished.observe(this, Observer { finishReturn() })
        viewModel.title.observe(this, Observer { title = it })
    }

    companion object {
        @JvmStatic
        fun newIntent(context: Context): Intent = Intent(context, QuickReturnActivity::class.java)
    }
}

