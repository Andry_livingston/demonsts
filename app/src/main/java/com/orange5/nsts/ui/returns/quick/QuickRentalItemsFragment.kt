package com.orange5.nsts.ui.returns.quick

import androidx.fragment.app.activityViewModels
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.service.rental.RentalStatus
import com.orange5.nsts.ui.base.TwoPageAdapter
import com.orange5.nsts.ui.returns.RentalItemsFragment

class QuickRentalItemsFragment : RentalItemsFragment() {

    override val viewModel: QuickReturnViewModel by activityViewModels { vmFactory }

    override val noDataMessage = R.string.rental_rented_is_empty_message

    override fun onItemClick(item: OrderRentalItem) {
        clearSubscriptions()
        scanningViewModel.releaseScanner()
        QuickRentalItemReturnDialog.show(this, item, TwoPageAdapter.TabType.INIT)
    }

    override fun onItemScanned(item: OrderRentalItem?) {
        item?.let {
            val defaultBin = viewModel.getDefaultBin(it)
            if (defaultBin != null) {
                successVibration()
                viewModel.onItemReturned(ReturnInformation(
                        defaultBin,
                        if (it.wasDamaged) RentalStatus.InRepair else RentalStatus.Available,
                        it.rentalReturnNotes ?: ""
                ))
            } else showScanFailToast(R.string.rental_return_error_inspection_bin)
        }
    }

    override fun updateRentedList(items: List<OrderRentalItem>?) {
        super.updateRentedList(items)
        viewModel.setTitle(getString(R.string.rental_return_activity_quick))
    }

    override fun onResume() {
        super.onResume()
        viewModel.setTitle(getString(R.string.rental_return_activity_quick))
    }

    override fun onDialogClosed() {
        scanningViewModel.setUpScanService()
        subscribe()
    }
}
