package com.orange5.nsts.ui.report.returns

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.data.db.entities.RentalReturn
import com.orange5.nsts.ui.base.BaseFragment
import com.orange5.nsts.ui.report.ReportViewModel
import kotlinx.android.synthetic.main.fragment_return_report.*

class ReturnReportFragment: BaseFragment() {

    private val viewModel : ReportViewModel by activityViewModels { vmFactory }
    private val adapter = ReturnReportItemsAdapter()

    override fun getLayoutResourceId(): Int = R.layout.fragment_return_report

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.returnItems.observe(this, Observer(::updateRentedList))
        viewModel.itemCount.observe(this, Observer(::onQuantityChanged))
        viewModel.customerName.observe(this, Observer(::onShowCustomerName))
        viewModel.updateTitle(resources.getString(R.string.rental_return_activity_title_rental_return_s,
                        viewModel.returnOrder.returnNumber))
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) = inflater.inflate(R.menu.info, menu)

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
            when (item.itemId) {
                R.id.orderInfo-> {
                     showReturnInfo(requireContext(),viewModel.returnOrder)
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        itemsRecycler.adapter = adapter
    }

    private fun showReturnInfo(context: Context, rentalReturn: RentalReturn) {
        val builder = AlertDialog.Builder(context)
        val view = ReturnInfoView(context)
        view.fillView(rentalReturn)
        builder.setView(view)
        builder.setTitle(context.getString(R.string.rental_return_activity_title_rental_return_s,
                rentalReturn.returnNumber))
        builder.setPositiveButton(R.string.common_close) { dialog, _ -> dialog.dismiss() }
        builder.show()
    }

    private fun updateRentedList(items: List<OrderRentalItem>?) {
        viewModel.itemCount.value = (items?.size ?: 0)
        if (items != null) adapter.submitList(items)
    }

    private fun onQuantityChanged(quantity: Int?) {
        itemCount.text = getString(R.string.common_n_items, quantity ?: 0)
    }

    private fun onShowCustomerName(s: String?) {
        customerNameText.text = s
    }

}

