package com.orange5.nsts.ui.shipping.discrepancy.list

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.observe
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseListFragment
import com.orange5.nsts.ui.shipping.discrepancy.repository.Discrepancy
import kotlinx.android.synthetic.main.fragment_refreshed_list.*

class DiscrepancyListFragment : BaseListFragment() {

    private val adapter = DiscrepancyListAdapter()
    private val viewModel: DiscrepancyListViewModel by activityViewModels { vmFactory }

    override val noDataMessage = R.string.shipping_notice_empty_data_message

    override fun onRefresh() = viewModel.loadDiscrepancies()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.discrepancies.observe(this, ::submitList)
        viewModel.liveProgress.observe(this, ::showLoading)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        noData.isVisible = false
        recyclerView.adapter = adapter
    }

    private fun submitList(items: List<Discrepancy>) {
        noData.isVisible = items.isEmpty()
        adapter.submitList(items)
    }
}