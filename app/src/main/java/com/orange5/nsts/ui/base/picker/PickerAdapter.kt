package com.orange5.nsts.ui.base.picker

import com.orange5.nsts.util.view.picker.PickerView
import java.util.function.Consumer

open class PickerAdapter<T> : PickerView.Adapter<Item<T>>() {

    private var items = mutableListOf<Item<T>>()

    var consumer = Consumer<Item<T>> {}
    var action: (Item<T>) -> Unit = {}

    override fun getItemCount() = items.size

    override fun getItem(index: Int) = items[index]

    open fun submitItems(list: List<T>) {
        items.clear()
        items.addAll(list.map { Item(it) })
        notifyDataSetChanged()
    }

    override fun onSelectedItemChanged(pickerView: PickerView?, previousPosition: Int, selectedItemPosition: Int) {
        val item = getItem(selectedItemPosition)
        item.setPosition(selectedItemPosition)
        action(item)
        consumer.accept(item)
    }
}

class Item<T>(val value: T) : PickerView.PickerItem {

    var index: Int = 0

    override fun setPosition(position: Int) {
        this.index = position
    }

    override fun getText(): String = value.toString()
}


