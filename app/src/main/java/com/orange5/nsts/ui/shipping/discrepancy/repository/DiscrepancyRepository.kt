package com.orange5.nsts.ui.shipping.discrepancy.repository

import com.orange5.nsts.data.db.entities.Discrepancies
import com.orange5.nsts.service.bin.BinService
import com.orange5.nsts.service.bin.ConsumableBinService
import com.orange5.nsts.service.consumable.ConsumableProductService
import com.orange5.nsts.service.discrepancy.DiscrepanciesService
import com.orange5.nsts.service.rental.RentalProductItemService
import com.orange5.nsts.service.rental.RentalProductService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DiscrepancyRepository @Inject constructor(private val discrepanciesService: DiscrepanciesService,
                                                private val consumableService: ConsumableProductService,
                                                private val rentalService: RentalProductService,
                                                private val consumableBinService: ConsumableBinService,
                                                private val rentalProductItem: RentalProductItemService,
                                                binService: BinService) {

    private val receivingBin = binService.receivingBin()

    fun loadDiscrepancies() = discrepanciesService.loadNonResolved().map { mapDiscrepancy(it) }

    fun updateProducts(items: List<Discrepancy>) {
        items.filter { it.type == Discrepancy.Type.SHIPMENT_SHORTAGE }
                .forEach {
                    if (it.isRental()) updateRentalItem(it)
                    else updateConsumableBin(it)
                }
    }

    fun saveDiscrepancy(items: List<Discrepancy>) {
        discrepanciesService.insertAll(items.map { Discrepancy.to(it) })
    }

    private fun updateConsumableBin(discrepancy: Discrepancy) {
        val receiveBin = consumableBinService.byProductIdAndBin(discrepancy.productId, receivingBin.binId)
        if (receiveBin != null) {
            if (receiveBin.quantity > discrepancy.quantity) {
                receiveBin.quantity -= discrepancy.quantity
                consumableBinService.update(receiveBin)
            } else consumableBinService.delete(receiveBin)
        }
    }

    private fun updateRentalItem(discrepancy: Discrepancy) {
        val item = rentalProductItem.loadItemByUUN(discrepancy.uun)
        rentalProductItem.delete(item)
    }

    private fun mapDiscrepancy(item: Discrepancies): Discrepancy {
        val product =
                if (item.rentalUniqueUnitNumber.isNullOrEmpty()) consumableService.byProductId(item.consumableProductId ?: "")
                else rentalService.byProductId(item.rentalProductId ?: "")
        return Discrepancy.from(item, product?.productDescription ?: "", product?.productNumber ?: "")
    }
}