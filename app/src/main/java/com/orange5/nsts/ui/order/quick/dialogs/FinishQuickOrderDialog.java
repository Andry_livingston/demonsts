package com.orange5.nsts.ui.order.quick.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.entities.Order;
import com.orange5.nsts.service.order.OrderType;

import java.util.function.Consumer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FinishQuickOrderDialog {
    private final AlertDialog dialog;

    @BindView(R.id.finishOrderView)
    protected FinishOrderView finishOrderView;
    private Consumer<Bitmap> finishOrder;
    private Consumer<Bitmap> finishOrderAndStartNew;

    public static FinishQuickOrderDialog with(Context context, Order order) {
        return new FinishQuickOrderDialog(context, order);
    }

    public FinishQuickOrderDialog finishOrder(Consumer<Bitmap> finishOrder) {
        this.finishOrder = finishOrder;
        return this;
    }

    public FinishQuickOrderDialog finishOrderAndStartNew(Consumer<Bitmap> finishOrder) {
        this.finishOrderAndStartNew = finishOrder;
        return this;
    }

    private FinishQuickOrderDialog(Context context, Order order) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = View.inflate(context, R.layout.order_finish_tab,null);
        ButterKnife.bind(this, view);
        finishOrderView.initWithOrder(OrderType.QUICK_ORDER,order);
        builder.setView(view);
        dialog = builder.create();
        dialog.setOnShowListener(d -> {
            Window window = dialog.getWindow();
            assert window != null;
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        });
        show();
    }

    @OnClick(R.id.finishOrderButton)
    void finishCurrentOrder() {
        finishOrder.accept(finishOrderView.getSignatureBitmap());
        dialog.dismiss();
    }

    @OnClick(R.id.finishAndCreateNewOrder)
    void finishAndCreateAnotherOrder() {
        finishOrderAndStartNew.accept(finishOrderView.getSignatureBitmap());
        dialog.dismiss();
    }

    public void show() {
        dialog.show();
    }

}
