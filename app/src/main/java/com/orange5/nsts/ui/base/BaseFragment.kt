package com.orange5.nsts.ui.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import com.orange5.nsts.R
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

abstract class BaseFragment : Fragment(), HasAndroidInjector, OnClosedDialogListener {

    @Inject
    lateinit var injector: DispatchingAndroidInjector<Any>
    @Inject
    lateinit var vmFactory: ViewModelProvider.Factory

    override fun onDialogClosed()  = Unit

    override fun androidInjector(): AndroidInjector<Any> = injector

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    protected abstract fun getLayoutResourceId(): Int

    protected fun <VM : ViewModel> getVMFromFragment(clazz: Class<VM>): VM {
        return ViewModelProviders.of(this, vmFactory).get(clazz)
    }

    protected fun <VM : ViewModel> getVMFromActivity(clazz: Class<VM>): VM {
        return ViewModelProviders.of(requireActivity(), vmFactory).get(clazz)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val layoutResourceId = if (getLayoutResourceId() == 0) R.layout.layout_dummy else getLayoutResourceId()
        val result = inflater.inflate(layoutResourceId, container, false)
        ButterKnife.bind(this, result)
        return result
    }

    protected fun <T> LiveData<T>.observe(consumer: (T) -> Unit) =
            observe(viewLifecycleOwner, Observer { consumer(it) })

    protected fun <T> LiveData<T?>.safeObserve(consumer: (T) -> Unit) =
            observe(viewLifecycleOwner, Observer { it?.let(consumer) })
}