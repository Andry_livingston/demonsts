package com.orange5.nsts.ui.home.orders;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.entities.Order;
import com.orange5.nsts.service.order.OrderStatus;
import com.orange5.nsts.ui.base.BaseFragment;
import com.orange5.nsts.ui.home.HomeViewModel;
import com.orange5.nsts.util.CollectionUtils;
import com.orange5.nsts.util.DateSections;
import com.orange5.nsts.util.adapters.recycler.RecyclerAdapterBuilder;

import java.util.List;

import butterknife.BindView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class OrdersFragment extends BaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView ordersList;
    @BindView(R.id.noData)
    TextView noData;
    @BindView(R.id.refresh)
    SwipeRefreshLayout refresh;

    private RecyclerAdapterBuilder<OrderView, Order> adapter;
    private HomeViewModel viewModel;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_refreshed_list;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = getVMFromActivity(HomeViewModel.class);
        viewModel.liveOrders.observe(this, this::submitList);
        viewModel.ordersProgress.observe(this, this::showLoading);
        viewModel.liveCantDeleteOrder.observe(this, this::showAlertDialog);
        viewModel.finishDeleteOrder.observe(this, this::finishDeleteOrderAndUpdateView);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        noData.setVisibility(GONE);
        noData.setText(R.string.no_orders_was_found);
        refresh.setOnRefreshListener(() -> viewModel.refreshOrders());
    }

    public void submitList(Pair<List<Order>, String> ordersPair) {
        if (CollectionUtils.isEmpty(ordersPair.first)) {
            noData.setVisibility(VISIBLE);
            ordersList.setVisibility(GONE);
            return;
        } else {
            ordersList.setVisibility(VISIBLE);
            noData.setVisibility(GONE);
        }

        SparseArray<String> sections = DateSections.generateSections(ordersPair.first, false,
                OrderStatus.closed.getId(), OrderStatus.active.getId());
        adapter = new RecyclerAdapterBuilder<OrderView, Order>(ordersList)
                .data(() -> ordersPair.first)
                .viewFactory(() -> new OrderView(requireContext()))
                .bind((view, model) -> view.fill(model, ordersPair.second))
                .onClick(R.id.contents, this::openOrderDetails)
                .onLongClick(R.id.contents, this::deleteOrder)
                .mutate((view, model, position) -> {
                    String sectionTitle = sections.get(position);
                    if (sectionTitle != null) {
                        view.showSectionHeader(sectionTitle);
                        return;
                    }
                    view.hideSectionHeader();
                })
                .vertical();
    }

    private void openOrderDetails(int position, Order item) {
        OrderDetailsHandler.showDetailsFor(getActivity(), item);
    }

    private boolean deleteOrder(int position, Order item) {
        if (item.getOrderStatusId() == OrderStatus.closed.getId()) {
            return true;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.common_remove_from_order);
        builder.setMessage(getString(R.string.common_confirm_order_removal, item.getOrderNumber()));
        builder.setPositiveButton(R.string.common_remove, (dialog, which) -> {
            viewModel.deleteOrder(item, position);
            dialog.dismiss();
        });
        builder.setNegativeButton(R.string.common_cancel, (dialog, which) -> dialog.dismiss());
        builder.show();
        return false;
    }

    private void finishDeleteOrderAndUpdateView(int position) {
        adapter.getItems().remove(position);
        adapter.notifyItemRemoved(position);
    }

    private void showLoading(Boolean isShown) {
        refresh.setRefreshing(isShown);
    }

    private void showAlertDialog(Boolean cantDelete) {
        new AlertDialog.Builder(getContext())
                .setTitle(R.string.common_warning)
                .setMessage(getString(R.string.pick_up_activity_cant_delete_order))
                .setPositiveButton(getString(R.string.common_ok), (dialog, which) -> {
                    dialog.dismiss();
                })
                .show();
    }
}
