package com.orange5.nsts.ui.base

enum class ActivityType {
    QUICK_ORDER,
    FULL_ORDER,
    QUICK_RETURN,
    FULL_RETURN
}