package com.orange5.nsts.ui.shipping.transfer

import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.lifecycle.observe
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseActivity
import com.orange5.nsts.ui.extensions.addFragment
import com.orange5.nsts.ui.shipping.takeaway.TakeAwayActivity
import kotlinx.android.synthetic.main.activity_shipping_notice.*

class ShippingNoticeActivity : BaseActivity() {

    val viewModel: ShippingNoticeViewModel by viewModels { vmFactory }

    override fun getLayoutResourceId() = R.layout.activity_shipping_notice

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.takeAwayQuantity.observe(this, ::onTakeAwayNoticesLoaded)
        setTitle(R.string.shipping_notice_title)
        if (savedInstanceState == null) {
            supportFragmentManager.addFragment(ShippingNoticeFragment(), shouldAddToBackStack = false)
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.loadShippingNotices()
        viewModel.loadTakeAwayNotices()
    }

    private fun onTakeAwayNoticesLoaded(count: Int) {
        takeAwayContainer.isVisible = count != 0
        receivingBinQty.text = getString(R.string.shipping_notice_take_away_count, count)
        takeAwayContainer.setOnClickListener { startActivity(TakeAwayActivity.intent(this)) }
    }
}
