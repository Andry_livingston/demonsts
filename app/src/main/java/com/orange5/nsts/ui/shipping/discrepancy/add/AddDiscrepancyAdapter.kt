package com.orange5.nsts.ui.shipping.discrepancy.add

import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseViewHolder
import com.orange5.nsts.ui.extensions.inflate
import com.orange5.nsts.ui.shipping.repository.ReceivingNotice
import kotlinx.android.synthetic.main.item_search_discrepancy.view.*

class AddDiscrepancyAdapter(private val listener: Listener) :
    ListAdapter<ReceivingNotice, AddDiscrepancyAdapter.SearchItemVH>(ListUtils()) {

    private var query = ""

    fun setList(notices: List<ReceivingNotice>, query: String = "") {
        submitList(notices)
        this.query = query
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchItemVH =
        SearchItemVH(listener, parent.inflate(R.layout.item_search_discrepancy))

    override fun onBindViewHolder(holder: SearchItemVH, position: Int) {
        holder.bind(getItem(position), query)
    }

    class SearchItemVH(private val listener: Listener, itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: ReceivingNotice, query: String) {
            itemView.run {
                productDescription.text = BaseViewHolder.highlightBackground(item.productDescription, query)
                productNumber.text = BaseViewHolder.highlightBackground(item.productNumber, query)
                quantity.isVisible = !item.isRental()
                uun.isVisible = item.isRental()
                uun.text = context.getString(
                    R.string.shipping_notice_uun,
                    BaseViewHolder.highlightBackground(item.uniqueUnitNumber, query)
                )
                productIcon.setImageResource(if (item.isRental()) R.drawable.ic_rental else R.drawable.ic_consumable)
                date.text = item.shippingDate()
                quantity.text = context.getString(R.string.shipping_notice_rental_quantity, item.quantity.remaining)
                setOnClickListener {listener.onItemClick(item)}
            }
        }
    }

    interface Listener {
        fun onItemClick(item: ReceivingNotice)
    }

    private class ListUtils : DiffUtil.ItemCallback<ReceivingNotice>() {
        override fun areItemsTheSame(oldItem: ReceivingNotice, newItem: ReceivingNotice) = oldItem == newItem
        override fun areContentsTheSame(oldItem: ReceivingNotice, newItem: ReceivingNotice) = oldItem.id == newItem.id
    }
}