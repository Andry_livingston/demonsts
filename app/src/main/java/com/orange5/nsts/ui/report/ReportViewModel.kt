package com.orange5.nsts.ui.report

import androidx.lifecycle.MutableLiveData
import com.orange5.nsts.data.db.base.OrderItem
import com.orange5.nsts.data.db.entities.Order
import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.data.db.entities.RentalReturn
import com.orange5.nsts.service.order.OrderService
import com.orange5.nsts.service.rental.RentalProductItemService
import com.orange5.nsts.service.rentalReturn.RentalReturnService
import com.orange5.nsts.ui.base.BaseViewModel
import com.orange5.nsts.util.format.DateFormatter
import javax.inject.Inject

class ReportViewModel @Inject constructor(
        private val rentalReturnService: RentalReturnService,
        private val rentalProductItemService: RentalProductItemService,
        private val orderService: OrderService) : BaseViewModel() {

    val returnItems = MutableLiveData<List<OrderRentalItem>>()
    val orderItems = MutableLiveData<List<OrderItem>>()

    val itemCount = MutableLiveData<Int>()
    val orderTitle = MutableLiveData<String>()
    val customerName = MutableLiveData<String>()
    var returnOrder = RentalReturn()
    var reportOrder = Order()

    fun getCount(): Int {
        return returnOrder.items.size
    }

    fun updateOrderInformation(id: String?, type: ReportType) {
        if (type == ReportType.RETURN) {
            initReturnOrder(id)
        } else {
            initReportOrder(id)
        }
    }

    fun updateTitle(name: String) {
        orderTitle.value = name
    }

    private fun initReportOrder(id: String?) {
        reportOrder = orderService.getOrderById(id)
        if (reportOrder.orderNumber.isNotEmpty()) {
            updateTitle(reportOrder.orderNumber)
            orderItems.value = reportOrder.allItems
        }
    }

    private fun initReturnOrder(id: String?) {
        returnOrder = rentalReturnService.getRentalById(id)
        if (returnOrder.returnNumber.isNotEmpty()) {
            setCustomerName(configNameAndDate())
        }
        val items = returnOrder.items
                .filterNotNull()
                .map {
                    it.scannedBin = rentalProductItemService.loadItemByUUN(it.uniqueUnitNumber)?.containerBin
                    it
                }
        returnItems.value = items
    }

    private fun configNameAndDate(): String {
        val date = DateFormatter.convertDbDateToUI(returnOrder.startDate)
        val customerInfo = returnOrder.customerInformText
        return String.format("%s %s", customerInfo, date)
    }

    private fun setCustomerName(customer: String) {
        customerName.value = customer
    }
}