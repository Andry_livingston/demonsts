package com.orange5.nsts.ui.base

import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import com.orange5.nsts.R
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.fragment_refreshed_list.*

abstract class BaseListFragment : BaseFragment(), HasAndroidInjector, OnClosedDialogListener {

    override fun getLayoutResourceId() = R.layout.fragment_refreshed_list

    @get:StringRes abstract val noDataMessage: Int

    protected open fun onRefresh() = Unit

    protected var isRefreshEnabled = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        refresh.setOnRefreshListener(::onRefresh)
        noData.text = getString(noDataMessage)
        refresh.isEnabled = isRefreshEnabled
    }

    protected fun showLoading(isShown: Boolean) {
        refresh.isRefreshing = isShown
    }

}