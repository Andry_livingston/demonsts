package com.orange5.nsts.ui.shipping.discrepancy

import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.observe
import com.orange5.nsts.R
import com.orange5.nsts.data.preferencies.RawSharedPreferences
import com.orange5.nsts.service.order.OrderType
import com.orange5.nsts.service.product.ExtrasProductSearchFilter
import com.orange5.nsts.service.product.ProductSearchFilter
import com.orange5.nsts.ui.base.BaseActivity
import com.orange5.nsts.ui.base.BaseFragment
import com.orange5.nsts.ui.extensions.hideOnScroll
import com.orange5.nsts.ui.extensions.setOnClickListener
import com.orange5.nsts.ui.extensions.showDialog
import com.orange5.nsts.ui.home.BottomMenuDialog
import com.orange5.nsts.ui.home.BottomMenuDialog.BottomType.MENU_DISCREPANCY
import com.orange5.nsts.ui.order.search.ProductWrapper
import com.orange5.nsts.ui.order.search.SearchProductDialog
import com.orange5.nsts.ui.returns.signature.OnFinishListener
import com.orange5.nsts.ui.shipping.ShippingProcessViewModel
import com.orange5.nsts.ui.shipping.discrepancy.repository.Discrepancy
import com.orange5.nsts.ui.shipping.repository.ReceivingNotice
import kotlinx.android.synthetic.main.fragment_discrepancy.*
import kotlinx.android.synthetic.main.layout_no_data.*
import javax.inject.Inject

abstract class DiscrepancyFragment : BaseFragment(),
        DiscrepancyAdapter.Listener,
        BottomMenuDialog.Listener,
        OnAddDiscrepancyClickListener {

    @Inject
    lateinit var rawSharedPreferences: RawSharedPreferences

    abstract val viewModel: ShippingProcessViewModel
    private val adapter = DiscrepancyAdapter(this)
    private var onFinishListener: OnFinishListener? = null

    override fun getLayoutResourceId() = R.layout.fragment_discrepancy

    abstract fun showExistingExtrasDetailsDialog(product: ProductWrapper)

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onFinishListener = (context as? OnFinishListener)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        viewModel.discrepancyItems.observe(this, ::submitList)
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as? BaseActivity)?.setTitle(R.string.discrepancy)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) = inflater.inflate(R.menu.menu_finish, menu)

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
            when (item.itemId) {
                R.id.finish -> {
                    onFinishListener?.onFinishProcessClick()
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.adapter = adapter
        noData.text = getString(R.string.discrepancy_empty_data_text)
        fab.setOnClickListener(::onAddShortageDialogShow)
        fab.hideOnScroll(recyclerView)
    }

    override fun onMiddleButtonClick() {
        SearchProductDialog.showSearchExtras(listOf(), ::showExistingExtrasDetailsDialog)
    }

    override fun onItemLongClick(item: Discrepancy) {
        requireContext().showDialog(
                message = getString(R.string.discrepancy_do_you_want_delete_discrepancy),
                positive = getString(R.string.discrepancy_btn_delete),
                negative = getString(R.string.common_cancel),
                positiveClick = {
                    item.quantity = 0
                    viewModel.removeDiscrepancy(item)
                })
    }

    private fun submitList(items: List<Discrepancy>) {
        adapter.submitList(items)
        noData.isVisible = items.isEmpty()
    }

    private fun onAddShortageDialogShow() {
        BottomMenuDialog.show(this, parentFragmentManager, MENU_DISCREPANCY)
    }
}

interface OnAddDiscrepancyClickListener {
    fun onAddDiscrepancyClick(item: ReceivingNotice, type: Discrepancy.Type)
}
