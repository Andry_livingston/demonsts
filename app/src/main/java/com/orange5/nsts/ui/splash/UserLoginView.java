package com.orange5.nsts.ui.splash;

import android.content.Context;
import android.util.AttributeSet;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.orange5.nsts.BuildConfig;
import com.orange5.nsts.R;
import com.orange5.nsts.data.db.entities.User;
import com.orange5.nsts.util.StringUtils;
import com.orange5.nsts.util.view.LoadingButton;

import java.util.function.Consumer;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserLoginView extends FrameLayout {

    @BindView(R.id.userInput)
    EditText userInput;

    @BindView(R.id.passwordInput)
    EditText passwordInput;

    @BindView(R.id.rememberCheck)
    CheckBox rememberMe;

    @BindView(R.id.loginButton)
    LoadingButton loginButton;

    private Consumer<UserData> loginConsumer = userLoginStatus -> {
    };

    public void setLoginConsumer(Consumer<UserData> statusConsumer) {
        this.loginConsumer = statusConsumer;
    }

    public UserLoginView(@NonNull Context context) {
        super(context);
        init();
    }

    public UserLoginView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public UserLoginView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public UserLoginView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.view_login_form, this);
        ButterKnife.bind(this);
        loginButton.setOnClickListener(view -> {
            login();
            return null;
        });
        passwordInput.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                login();
                return true;
            }
            return false;
        });
    }

    private void login() {
        showLoading(true);
        loginConsumer.accept(new UserData(
                getUserName(),
                getPassword(),
                rememberMe.isChecked()));
    }

    public void showLoading(boolean isEnable) {
        loginButton.setLoading(isEnable);
    }

    @NonNull
    public String getPassword() {
        return passwordInput.getText().toString();
    }

    @NonNull
    public String getUserName() {
        return userInput.getText().toString();
    }

    public void setUserData(User user) {
        if (user != null) {
            userInput.setText(user.getUserName());
            if (BuildConfig.DEBUG) passwordInput.setText(user.getPassword());
            rememberMe.setChecked(StringUtils.isNotEmpty(user.getUserName()) && StringUtils.isNotEmpty(user.getPassword()));
        }
    }

    public static class UserData {
        private final String userName;
        private final String password;
        private final boolean shouldRemember;


        public UserData(String userName, String password, boolean shouldRemembr) {
            this.userName = userName;
            this.password = password;
            this.shouldRemember = shouldRemembr;
        }

        public String getUserName() {
            return userName;
        }

        public String getPassword() {
            return password;
        }

        public boolean isShouldRemember() {
            return shouldRemember;
        }
    }
}
