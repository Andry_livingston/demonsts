package com.orange5.nsts.ui.shipping.discrepancy.widget

import android.content.Context
import android.text.SpannableStringBuilder
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.widget.SearchView
import androidx.core.text.bold
import androidx.core.text.color
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.BinNumber
import com.orange5.nsts.ui.base.picker.BinPickerAdapter
import com.orange5.nsts.ui.base.picker.Item
import com.orange5.nsts.ui.base.picker.NumberPickerAdapter
import com.orange5.nsts.ui.extensions.color
import kotlinx.android.synthetic.main.view_bin_number_picker.view.*

class BinNumberPickerView @JvmOverloads constructor(context: Context, attrsSet: AttributeSet? = null,
                                                    defStyleAttr: Int = 0, defStyleRes: Int = 0)
    : LinearLayout(context, attrsSet, defStyleAttr, defStyleRes), SearchView.OnQueryTextListener {

    private val numberAdapter = NumberPickerAdapter()
    private val binAdapter = BinPickerAdapter()
    private var selectedBin: BinNumber? = null
    private var selectedQuantity: Int = 0
    private var binPosition: Int = 0
    private var searchBinListener: ((String?, Int) -> Unit)? = null

    init {
        View.inflate(context, R.layout.view_bin_number_picker, this)
        rightPicker.setAdapter(binAdapter)
        leftPicker.setAdapter(numberAdapter)
        this.searchBin.setOnQueryTextListener(this)
        numberAdapter.action = ::onQuantitySelected
        binAdapter.action = ::onBinSelected
    }

    override fun onQueryTextSubmit(query: String?): Boolean = false

    override fun onQueryTextChange(newText: String?): Boolean {
        searchBinListener?.invoke(newText, binPosition)
        return false
    }

    fun submitNumbers(count: Int) {
        val list = mutableListOf<Int>()
        repeat(count + 1) { list.add((it)) }
        numberAdapter.submitItems(list)
    }

    fun submitBins(bins: List<BinNumber>) {
        binAdapter.submitItems(bins)
        rightPicker.selectedItemPosition = 0
        updateActionButtonText()
    }

    fun selectNumberPosition(position: Int) {
        leftPicker.selectedItemPosition = position
    }

    fun selectBinPosition(position: Int) {
        binPosition = position
        rightPicker.selectedItemPosition = position
    }

    fun setOnActionClick(action: (BinNumber?, Int) -> Unit) {
        actionButton.setOnClickListener {
            action(selectedBin, selectedQuantity)
        }
    }

    fun setOnQueryListener(listener: (String?, Int) -> Unit) {
        searchBinListener = listener
    }

    private fun onQuantitySelected(item: Item<Int>) {
        selectedQuantity = item.value
        updateActionButtonText()
    }

    private fun onBinSelected(item: Item<BinNumber>) {
        selectedBin = item.value
        updateActionButtonText()
    }

    private fun updateActionButtonText() {
        val text = SpannableStringBuilder(resources.getString(R.string.take_away_action_put))
                .append(NumberPickerView.SPACE)
                .bold { color(context.color(R.color.blue)) { append(selectedQuantity.toString()) } }
                .append(NumberPickerView.SPACE).append(resources.getString(R.string.take_away_action_in)).append(SPACE)
                .bold { color(context.color(R.color.blue)) { append(selectedBin?.binNum ?: "") } }
        actionButton.text = text
    }

    companion object {
        const val SPACE = " "
    }
}