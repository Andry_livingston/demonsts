package com.orange5.nsts.ui.shipping.discrepancy.widget

import android.content.Context
import android.text.SpannableStringBuilder
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.core.text.bold
import androidx.core.text.color
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.picker.Item
import com.orange5.nsts.ui.base.picker.NumberPickerAdapter
import com.orange5.nsts.ui.extensions.color
import kotlinx.android.synthetic.main.view_number_picker.view.*

class NumberPickerView @JvmOverloads constructor(context: Context, attrsSet: AttributeSet? = null,
                                                 defStyleAttr: Int = 0, defStyleRes: Int = 0)
    : LinearLayout(context, attrsSet, defStyleAttr, defStyleRes) {

    private val adapter: NumberPickerAdapter = NumberPickerAdapter()
    private var selectedQuantity: Int = 1

    var actionButtonText: Int = R.string.receiving_receive
        set(value) {
            field = value
            updateActionButtonText()
        }

    init {
        View.inflate(context, R.layout.view_number_picker, this)
        picker.setAdapter(adapter)
        adapter.action = ::onQuantitySelected
    }

    fun submitItems(count: Int) {
        val list = mutableListOf<Int>()
        repeat(count) { list.add((it + 1)) }
        adapter.submitItems(list)
    }

    fun selectedPosition(position: Int) {
        picker.selectedItemPosition = position
    }

    fun setOnActionClick(action: (Int) -> Unit) {
        actionButton.setOnClickListener {
            action(selectedQuantity)
        }
    }

    private fun onQuantitySelected(item: Item<Int>) {
        selectedQuantity = item.value
        updateActionButtonText()
    }

    private fun updateActionButtonText() {
        val text = SpannableStringBuilder(resources.getString(actionButtonText))
                .append(SPACE)
                .bold { color(context.color(R.color.blue)) { append(selectedQuantity.toString()) } }
                .append(SPACE).append(resources.getString(R.string.receiving_items))
        actionButton.text = text
    }

    companion object {
        const val SPACE = " "
    }
}