package com.orange5.nsts.ui.settings.models

data class ApiSettings @JvmOverloads constructor(var host: String = "localhost",
                                                 val protocol: String = "http://",
                                                 var port: String = SYNC_PORT) {

    fun getBaseUrl() = "$protocol$host$port"

    companion object {
        const val INIT_LOAD_PORT = ":8003/"
        const val SYNC_PORT = ":8005/"
    }
}