package com.orange5.nsts.ui.settings

import com.orange5.nsts.R

enum class Setting(val settingType: SettingType,
                   val title: Int,
                   val description: Int
) {
    CHANGE_IP(SettingType.CUSTOM,
            R.string.setting_ip_settings,
            R.string.setting_description_change_update_ip),

    EXPORT_DB_FILE(SettingType.CUSTOM,
            R.string.settings_export_db_file,
            R.string.settings_export_db_file_on_device),

    RUN_INIT_SYNC(SettingType.CUSTOM,
            R.string.settings_run_init_sync,
            R.string.settings_run_init_sync_description),

    CLEAR_SYNC_DATA(SettingType.CUSTOM,
            R.string.settings_run_clear_sync_data,
            R.string.settings_run_clear_sync_data_description),

    VIBRATION_ENABLED(SettingType.BOOLEAN,
            R.string.settings_enable_vibration,
            R.string.settings_give_haptic_feedback),

    ACCOUNTING_CODE_REQUIRED(SettingType.BOOLEAN,
            R.string.settings_accounting_code_required,
            R.string.settings_choose_your_option),

    HIDE_QUICK_ORDER(SettingType.BOOLEAN,
            R.string.settings_hide_quick_order,
            R.string.settings_choose_your_option),

    HIDE_QUICK_RETURN(SettingType.BOOLEAN,
            R.string.settings_hide_quick_return,
            R.string.settings_choose_your_option),

    DISCREPANCIES(SettingType.CUSTOM,
            R.string.discrepancy,
            R.string.discrepancy),

    STRESS_TES_ENABLED(SettingType.BOOLEAN,
            R.string.settings_stress_test_enable,
            R.string.settings_choose_your_option),

    SET_UP_STRESS_TEST(SettingType.CUSTOM,
            R.string.settings_set_up_stress_test,
            R.string.settings_choose_your_option);
}

enum class SettingType {
    INTEGER, BOOLEAN, CUSTOM
}