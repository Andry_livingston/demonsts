package com.orange5.nsts.ui.order.search;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Switch;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.service.order.OrderType;
import com.orange5.nsts.service.product.ExtrasProductSearchFilter;
import com.orange5.nsts.service.product.ProductSearchFilter;
import com.orange5.nsts.service.product.ProductService;
import com.orange5.nsts.service.product.SearchFilter;
import com.orange5.nsts.ui.base.BaseActivity;
import com.orange5.nsts.util.RealTimeSearch;
import com.orange5.nsts.util.UI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.GONE;

public class SearchProductDialog implements SearchRVAdapter.OnProductClickListener {

    private final Consumer<ProductWrapper> selectionConsumer;
    private final List<OrderItem> existingItems;
    private final OrderType type;
    private final boolean showFilter;
    private SearchFilter searchFilter;
    private SearchRVAdapter adapter;
    private AlertDialog dialog;

    @BindView(R.id.filterContainer)
    View filterContainer;
    @BindView(R.id.contents)
    RelativeLayout contents;
    @BindView(R.id.searchResultsList)
    RecyclerView searchResultsList;
    @BindView(R.id.searchInput)
    EditText searchInput;
    @BindView(R.id.showRentals)
    Switch showRentals;
    @BindView(R.id.showConsumables)
    Switch showConsumables;
    @BindView(R.id.showZeroQuantity)
    Switch showZeroQuantity;

    @OnClick(R.id.clearSearch)
    void onClearSearch() {
        if (searchInput.getText().toString().length() == 0) {
            UI.closeKeyboard(searchInput);
            dismiss();
        } else {
            searchInput.setText("");
        }
    }

    @Override
    public void onProductClick(ProductWrapper product) {
        UI.closeKeyboard(searchInput);
        selectionConsumer.accept(product);
        dismiss();
    }

    private SearchProductDialog(Consumer<ProductWrapper> selectionConsumer,
                                List<OrderItem> existingItems,
                                SearchFilter searchFilter,
                                OrderType type,
                                boolean showFilter) {
        this.selectionConsumer = selectionConsumer;
        this.existingItems = existingItems == null ? new ArrayList<>() : existingItems;
        this.searchFilter = searchFilter;
        this.type = type;
        this.showFilter = showFilter;
        create();
    }

    public static void show(List<OrderItem> existingItems,
                            Consumer<ProductWrapper> selectionConsumer,
                            ProductSearchFilter searchFilter,
                            OrderType type) {
        new SearchProductDialog(
                selectionConsumer,
                existingItems,
                searchFilter,
                type, true);
    }

    public static void showSearchExtras(List<OrderItem> existingItems,
                                        Consumer<ProductWrapper> selectionConsumer) {
        new SearchProductDialog(
                selectionConsumer,
                existingItems,
                new ExtrasProductSearchFilter(),
                OrderType.FULL_ORDER, false);
    }

    private void create() {
        Context context = BaseActivity.getActive();
        View container = LayoutInflater.from(context).inflate(R.layout.search_item_dialog, null);
        ButterKnife.bind(this, container);
        initFilter();
        filterContainer.setVisibility(showFilter ? View.VISIBLE : GONE);
        RealTimeSearch.with(searchInput)
                .searchHandler(this::search);
        searchResultsList.setLayoutManager(new LinearLayoutManager(container.getContext(),
                LinearLayoutManager.VERTICAL, false));
        adapter = new SearchRVAdapter(this, getExistingItems(existingItems), type);
        searchResultsList.setAdapter(adapter);
        searchInput.requestFocus();
        UI.openKeyboard(searchInput);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(container);
        dialog = builder.create();
        dialog.setOnShowListener(d -> {
            Window window = dialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        });
        search("");
        show();
    }

    private Map<String, Integer> getExistingItems(List<OrderItem> items) {
        Map<String, Integer> map = new HashMap<>();
        items.forEach(item -> map.put(item.getProductId(), item.getQuantity()));
        return map;
    }

    private void initFilter() {
        showRentals.setChecked(searchFilter.getShouldShowRentals());
        showConsumables.setChecked(searchFilter.getShouldShowConsumables());
        showZeroQuantity.setChecked(needShowZeroQuantity(searchFilter.getShouldShowZeroQuantity()));


        CompoundButton.OnCheckedChangeListener listener = (buttonView, isChecked) -> {
            searchFilter.setShouldShowConsumables(showConsumables.isChecked());
            searchFilter.setShouldShowRentals(showRentals.isChecked());
            needShowZeroQuantity(showZeroQuantity.isChecked());
            search(searchFilter.getQuery());
        };
        showRentals.setOnCheckedChangeListener(listener);
        showConsumables.setOnCheckedChangeListener(listener);
        showZeroQuantity.setOnCheckedChangeListener(listener);
    }

    private boolean needShowZeroQuantity(boolean checked) {
        if (!searchFilter.getShouldShowConsumables()) {
            showZeroQuantity.setVisibility(GONE);
            searchFilter.setShouldShowZeroQuantity(false);
            return false;
        } else {
            showZeroQuantity.setVisibility(View.VISIBLE);
            searchFilter.setShouldShowZeroQuantity(checked);
            return checked;
        }
    }

    private void show() {
        dialog.show();
    }

    private void search(String query) {
        searchFilter.setQuery(query);
        if (type.equals(OrderType.FULL_ORDER)) {
            ProductService.get().searchProductsFullOrder(searchFilter, it -> adapter.setItems(it, query));
        } else {
            ProductService.get().searchProductsQuickOrder(searchFilter, it -> adapter.setItems(it, query));
        }
    }

    private void dismiss() {
        dialog.dismiss();
    }
}
