package com.orange5.nsts.ui.shipping.takeaway

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.core.os.bundleOf
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.observe
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.BinNumber
import com.orange5.nsts.scan.ScanningDialog
import com.orange5.nsts.ui.base.OnClosedDialogListener
import com.orange5.nsts.ui.extensions.setOnClickListener
import com.orange5.nsts.ui.shipping.discrepancy.widget.BinPickerView
import com.orange5.nsts.ui.shipping.discrepancy.widget.ShippingDiscrepancyView
import com.orange5.nsts.ui.shipping.receiving.ShippingPagerAdapter
import kotlinx.android.synthetic.main.dialog_shipping_container.*
import kotlinx.android.synthetic.main.view_bin_number_picker.view.*

class TakeAwayRentalDialog : ScanningDialog() {

    private val viewModel: TakeAwayViewModel by activityViewModels { factory }
    private val adapter = ShippingPagerAdapter()

    override fun getLayoutResourceId() = R.layout.dialog_shipping_container

    override val notFoundMessage = R.string.shipping_notice_scan_bin

    private lateinit var shortageView: ShippingDiscrepancyView
    private lateinit var takeAwayView: BinPickerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        closeCallback = targetFragment as? OnClosedDialogListener
        viewModel.onDiscrepancyAdded.observe(this) { dismiss() }
        viewModel.onBinsLoaded.observe(this, ::setUpPager)
        viewModel.binPosition.observe(this, ::scrollPickerToBinPosition)
        scanningViewModel.liveBinScanned.observe(this, ::setScannedBin)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val uunValue = arguments?.getString(KEY_UUN)
        viewModel.loadRentalProduct(uunValue)
        product.setValueText(uunValue)
        cancel.setOnClickListener(::dismiss)
        viewPager.updateLayoutParams<LinearLayout.LayoutParams> { height = resources.getDimensionPixelOffset(R.dimen.take_away_rental_pager_height) }
        takeAwayView = BinPickerView(requireContext())
        shortageView = ShippingDiscrepancyView(requireContext())
    }

    override fun setScannerIcon() {
        takeAwayView.scannerToggle?.let {
            it.visibility = View.VISIBLE
            val icon =
                    if (scanningViewModel.isScannerServiceActive() && scanningViewModel.isScannerActive) R.drawable.barcode_scan_on_ripple
                    else R.drawable.barcode_scan_off_ripple
            it.setImageResource(icon)
        }
    }

    private fun setUpPager(bins: List<BinNumber>) {
        viewPager.adapter = adapter
        adapter.setItems(getPagerItems(bins))

        tabLayout.setupWithViewPager(viewPager)
    }

    private fun getPagerItems(bins: List<BinNumber>): List<ShippingPagerAdapter.Item> {
        takeAwayView.submitList(bins)
        takeAwayView.setOnQueryListener(viewModel::searchBin)
        takeAwayView.setOnActionClick { bin ->
            viewModel.takeAwayRental(bin, product.getValueText())
            dismiss()
        }

        shortageView.setDiscrepancyText(R.string.discrepancy_shortage_quantity_text)
        shortageView.isRental(true)
        shortageView.setOnActionClick { quantity, notes ->
            viewModel.addShortages(product.getValueText(), quantity, notes)
        }

        return mutableListOf(
                ShippingPagerAdapter.Item(takeAwayView, getString(R.string.receiving_init_item_title)),
                ShippingPagerAdapter.Item(shortageView, getString(R.string.discrepancy_shortage_title))
        )
    }

    private fun setScannedBin(bin: BinNumber) {
        viewModel.searchScannedBin(bin.binId)
    }

    private fun scrollPickerToBinPosition(position: Int) {
        takeAwayView.selectedPosition(position)
    }

    companion object {
        fun show(fragment: Fragment, uun: String, wasScanned: Boolean = false) =
                TakeAwayRentalDialog().apply {
                    arguments = bundleOf(WAS_SCANNED_ID to wasScanned, KEY_UUN to uun)
                    setTargetFragment(fragment, 999)
                    show(fragment.parentFragmentManager, this::class.java.simpleName)
                }

        private const val WAS_SCANNED_ID = "was_scanned_key"
        private const val KEY_UUN = "key_uun"
    }
}