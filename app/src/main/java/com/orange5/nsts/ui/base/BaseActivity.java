package com.orange5.nsts.ui.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;

import androidx.annotation.CallSuper;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.orange5.nsts.R;
import com.orange5.nsts.app.App;
import com.orange5.nsts.receivers.BaseBroadcastReceiver;
import com.orange5.nsts.receivers.BroadcastService;
import com.orange5.nsts.receivers.SyncResultReceiver;
import com.orange5.nsts.receivers.UdpResultReceiver;
import com.orange5.nsts.sync.service.SyncService;
import com.orange5.nsts.ui.extensions.ContextExtensionsKt;

import org.jetbrains.annotations.Nullable;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;
import timber.log.Timber;

import static com.orange5.nsts.ui.base.BaseService.ServiceRunner.StartState.ALREADY_RUNNING;

public abstract class BaseActivity extends AppCompatActivity implements HasAndroidInjector {

    @Inject
    public DispatchingAndroidInjector<Object> injector;
    @Inject
    public ViewModelProvider.Factory vmFactory;
    @Inject
    BroadcastService broadcastService;

    protected FrameLayout mainContent;
    protected View contentView;
    private BaseBroadcastReceiver syncResultReceiver;
    private BaseBroadcastReceiver udpResultReceiver;
    private Snackbar snackbar;

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    @Override
    public AndroidInjector<Object> androidInjector() {
        return injector;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setWindowFlags();
        overrideTransitions();
        createContentView();
        attachViewTreeObserver();
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerSyncResultReceiver();
        registerUdpResultReceiver();
    }

    public static BaseActivity getActive() {
        return App.getInstance().getAppLifecycleListener().getActivity();
    }

    protected <VM extends ViewModel> VM getVMFromFragment(BaseFragment fragment, Class<VM> clazz) {
        return new ViewModelProvider(fragment, vmFactory).get(clazz);
    }

    protected <VM extends ViewModel> VM getVMFromActivity(Class<VM> clazz) {
        return new ViewModelProvider(this, vmFactory).get(clazz);
    }

    private void registerSyncResultReceiver() {
        syncResultReceiver = new SyncResultReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getBooleanExtra(SYNC_FAIL_KEY, false)) {
                    onSyncError(intent.getStringExtra(SYNC_RESULT_KEY));
                } else {
                    boolean areServerQueriesEmpty = intent.getBooleanExtra(EMPTY_SERVER_QUERIES_KEY, true);
                    onSyncSuccess(areServerQueriesEmpty);
                }
            }
        };
        broadcastService.registerLocalBroadcast(syncResultReceiver);
    }

    private void registerUdpResultReceiver() {
        udpResultReceiver = new UdpResultReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getBooleanExtra(UdpResultReceiver.UDP_FAIL_KEY, false)) {
                    String error = intent.getStringExtra(UDP_ERROR_MESSAGE);
                    ContextExtensionsKt.toast(context,"UDP fails with " + error);
                } else {
                    String sender = intent.getStringExtra(UDP_SENDER);
                    String data = intent.getStringExtra(UDP_DATA_PACKET);
                    ContextExtensionsKt.toast(context,"UDP success: from " + sender + " with data: " + data);
                    runSync();
                }
            }
        };
        broadcastService.registerLocalBroadcast(udpResultReceiver);
    }

    @Override
    protected void onStop() {
        broadcastService.unregisterLocalBroadcast(syncResultReceiver);
        broadcastService.unregisterLocalBroadcast(udpResultReceiver);
        super.onStop();
    }

    protected abstract int getLayoutResourceId();

    protected int getMenuResourceId() {
        return 0;
    }

    protected void setWindowFlags() {
    }

    protected void afterMeasure() {
    }

    @CallSuper
    protected void onSyncError(String errorMessage) {
        Timber.e(errorMessage);
    }

    @CallSuper
    protected void onSyncSuccess(boolean areServerQueriesEmpty) {
        Timber.d("sync success");
    }

    protected void runSync(boolean forcibly) {
        switch (SyncService.ServiceRunner.getServiceStartState(this)) {
            case ALREADY_RUNNING:
                if (forcibly) errorSnack(R.string.error_start_sync_already_running);
                onSyncError(getString(R.string.error_start_sync_already_running));
                break;
            case NO_WIFI_CONNECTION:
                if (forcibly) errorSnack(R.string.error_start_sync_no_wifi_connection);
                onSyncError(getString(R.string.error_start_sync_no_wifi_connection));
                break;
            case SUCCESS:
                SyncService.start(this);
                break;
        }
    }

    protected boolean isSyncInProgress() {
        return SyncService.ServiceRunner.getServiceStartState(this) == ALREADY_RUNNING;
    }

    protected void runSync() {
        runSync(false);
    }

    protected String getActivityTitle() {
        return getString(R.string.app_name);
    }

    private void attachViewTreeObserver() {
        final ViewTreeObserver vto = mainContent.getViewTreeObserver();
        if (vto.isAlive()) {
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    afterMeasure();
                    if (vto.isAlive()) {
                        vto.removeOnGlobalLayoutListener(this);
                    }
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (getMenuResourceId() == 0) {
            return false;
        }
        getMenuInflater().inflate(getMenuResourceId(), menu);
        return true;
    }

    private void createContentView() {
        contentView = View.inflate(this, R.layout.activity_base, null);
        mainContent = contentView.findViewById(R.id.mainContent);
        View.inflate(this, getLayoutResourceId(), mainContent);
        setContentView(contentView);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setTitle(getActivityTitle());
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        reverseOverrideTransitions();
    }

    @Override
    public void finish() {
        super.finish();
        reverseOverrideTransitions();
    }

    protected void overrideTransitions() {
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    protected void reverseOverrideTransitions() {
        overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
    }

    public IntentBuilder prepareActivity(Class<? extends BaseActivity> activityClass) {
        overrideTransitions();
        return new IntentBuilder(this, activityClass);
    }

    public void hideToolbar() {
        toolbar.setVisibility(View.GONE);
    }

    public void syncSnack(BaseTransientBottomBar.BaseCallback callback, Runnable cancelClick) {
        Snackbar.make(contentView, R.string.sync_new_data_from_server_available, Snackbar.LENGTH_LONG)
                .setAction(R.string.sync_snack_action, view -> cancelClick.run())
                .setActionTextColor(getResources().getColor(R.color.orange, null))
                .addCallback(callback)
                .show();
    }

    protected void errorSnack(@StringRes int errorMessage) {
        snackbar = Snackbar.make(contentView, errorMessage, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    protected void errorSnack(String message) {
        snackbar = Snackbar.make(contentView, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    protected void showMessage(String message) {
        if (snackbar != null && snackbar.isShown()) {
            snackbar.dismiss();
        }
        errorSnack(message);
    }
}
