package com.orange5.nsts.ui.home.orders;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.entities.Order;
import com.orange5.nsts.util.format.CurrencyFormatter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderView extends LinearLayout {

    @BindView(R.id.customerName)
    TextView customerName;

    @BindView(R.id.orderTotalSum)
    TextView orderSum;

    @BindView(R.id.orderTypeAndNumber)
    TextView orderTypeAndNumber;

    @BindView(R.id.orderStatus)
    TextView orderStatus;

    @BindView(R.id.orderDate)
    TextView orderDate;

    @BindView(R.id.sectionHeader)
    TextView sectionHeader;

    @BindView(R.id.backorder)
    TextView backorder;

    @BindView(R.id.iconHasConsumable)
    ImageView hasConsumable;

    @BindView(R.id.iconHasRental)
    ImageView hasRental;

    public OrderView(@NonNull Context context) {
        this(context, null);
    }

    public OrderView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public OrderView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public OrderView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setOrientation(VERTICAL);
        inflate(context, R.layout.item_order, this);
        ButterKnife.bind(this, this);
        sectionHeader.setVisibility(GONE);
    }


    public void fill(Order order, String highlight) {
        OrderViewHolder orderViewModel = new OrderViewHolder(getContext());
        orderViewModel.setCurrentOrder(order);
        orderTypeAndNumber.setText(orderViewModel.getTypeAndNumber(highlight));
        customerName.setText(orderViewModel.getCustomerFullName(highlight));

        orderSum.setText(CurrencyFormatter.formatPrice(order.getOrderSum()));
        orderDate.setText(orderViewModel.getFormattedOrderDate());
        orderStatus.setText(orderViewModel.getStatusText());
        orderStatus.setBackground(orderViewModel.getStatusBackground());
        backorder.setVisibility(orderViewModel.hasBackorder() ? VISIBLE : GONE);
        hasConsumable.setVisibility(orderViewModel.hasConsumable() ? VISIBLE : GONE);
        hasRental.setVisibility(orderViewModel.hasRental() ? VISIBLE : GONE);
    }


    public void showSectionHeader(String sectionTitle) {
        sectionHeader.setText(sectionTitle);
        sectionHeader.setVisibility(VISIBLE);
    }

    public void hideSectionHeader() {
        sectionHeader.setVisibility(GONE);
    }
}
