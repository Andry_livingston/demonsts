package com.orange5.nsts.ui.shipping.discrepancy.add

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseDialogFragment
import com.orange5.nsts.ui.extensions.setOnClickListener
import com.orange5.nsts.ui.shipping.ShippingProcessViewModel
import com.orange5.nsts.ui.shipping.discrepancy.repository.Discrepancy
import com.orange5.nsts.ui.shipping.receiving.ReceivingViewModel
import com.orange5.nsts.ui.shipping.takeaway.TakeAwayViewModel
import com.orange5.nsts.util.view.CustomToast
import kotlinx.android.synthetic.main.dialog_edit_discrepancy.*

abstract class AddExistingExtrasDetailsDialog : BaseDialogFragment() {

    abstract val viewModel: ShippingProcessViewModel

    override fun getLayoutResourceId(): Int = R.layout.dialog_edit_discrepancy

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cancel.setOnClickListener(::dismiss)
        setUpView()
    }

    private fun setUpView() {
        val discrepancy = arguments?.getParcelable(DISCREPANCY_KEY) as? Discrepancy
        discrepancy?.let {
            productNumber.setValueText(it.productNumber)
            discrepancyView.setQuantity(1)
            discrepancyView.isRental(it.isRental())
            discrepancyView.setDiscrepancyText(R.string.discrepancy_extras_quantity_text)
            discrepancyView.setOnActionClick { count, notes -> onUpdateDiscrepancyDetails(discrepancy, count, notes) }
        } ?: run {
            CustomToast.show(requireActivity(), getString(R.string.common_generic_error))
            dismiss()
        }
    }

    private fun onUpdateDiscrepancyDetails(discrepancy: Discrepancy, count: Int, notes: String) {
        discrepancy.quantity = count
        discrepancy.comment = notes
        viewModel.addExistingExtras(discrepancy)
        dismiss()
    }

    companion object {
        @JvmStatic
        fun fromReceiving(fragmentManager: FragmentManager,
                          discrepancy: Discrepancy) = show(ReceivingAddExistingExtrasDetailsDialog(), fragmentManager, discrepancy)

        @JvmStatic
        fun fromTakeAway(fragmentManager: FragmentManager,
                         discrepancy: Discrepancy) = show(TakeAwayAddExistingExtrasDetailsDialog(), fragmentManager, discrepancy)

        fun show(dialog: AddExistingExtrasDetailsDialog,
                 fragmentManager: FragmentManager,
                 discrepancy: Discrepancy) =
                dialog.apply {
                    arguments = bundleOf(DISCREPANCY_KEY to discrepancy)
                    show(fragmentManager, this::class.java.simpleName)
                }

        private const val DISCREPANCY_KEY = "discrepancy_key"
    }
}


class ReceivingAddExistingExtrasDetailsDialog : AddExistingExtrasDetailsDialog() {
    override val viewModel by activityViewModels<ReceivingViewModel> { factory }
}

class TakeAwayAddExistingExtrasDetailsDialog : AddExistingExtrasDetailsDialog() {
    override val viewModel by activityViewModels<TakeAwayViewModel> { factory }
}