package com.orange5.nsts.ui.shipping.receiving

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.CallSuper
import androidx.core.os.bundleOf
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.observe
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseDialogFragment
import com.orange5.nsts.ui.extensions.setOnClickListener
import com.orange5.nsts.ui.shipping.discrepancy.widget.NumberPickerView
import com.orange5.nsts.ui.shipping.discrepancy.widget.ShippingDiscrepancyView
import com.orange5.nsts.ui.shipping.receiving.ShippingPagerAdapter.Item
import com.orange5.nsts.ui.shipping.repository.ReceivingNotice
import kotlinx.android.synthetic.main.dialog_shipping_container.*

abstract class ReceivingConsumableDialog : BaseDialogFragment() {

    protected val viewModel: ReceivingViewModel by activityViewModels { factory }
    private val adapter = ShippingPagerAdapter()
    private lateinit var shortageView: ShippingDiscrepancyView
    private lateinit var extrasView: ShippingDiscrepancyView

    override fun getLayoutResourceId(): Int = R.layout.dialog_shipping_container

    abstract val actionBtnText: Int

    abstract fun itemCount(item: ReceivingNotice): Int

    @CallSuper
    open fun onProcessBtnClick(count: Int) {
        dismiss()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val noticeId = arguments?.getString(NOTICE_ID_KEY) ?: ""
        viewModel.loadReceivingNotice(noticeId)
        viewModel.receivingNotice.observe(this, ::setUpView)
        viewModel.onDiscrepancyAdded.observe(this) { dismiss() }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cancel.setOnClickListener(::dismiss)
        viewPager.updateLayoutParams<LinearLayout.LayoutParams> { height = resources.getDimensionPixelOffset(R.dimen.receiving_pager_height) }
        shortageView = ShippingDiscrepancyView(requireContext())
        extrasView = ShippingDiscrepancyView(requireContext())
    }

    private fun setUpView(item: ReceivingNotice) {
        product.setValueText(item.productNumber)
        setUpPager(item)
    }

    private fun setUpPager(notice: ReceivingNotice) {
        viewPager.adapter = adapter
        adapter.setItems(getPagerItems(notice))

        tabLayout.setupWithViewPager(viewPager)
    }

    private fun getPagerItems(notice: ReceivingNotice): List<Item> {
        val receiveView = NumberPickerView(requireContext())
        val count = itemCount(notice)
        receiveView.submitItems(count)
        receiveView.selectedPosition(arguments?.getInt(POSITION_KEY, count) ?: count)
        receiveView.setOnActionClick(::onProcessBtnClick)
        receiveView.actionButtonText = actionBtnText

        shortageView.setDiscrepancyText(R.string.discrepancy_shortage_quantity_text)
        shortageView.isRental(notice.isRental())
        if (notice.quantity.remaining < 1) shortageView.notDiscrepancyAvailable()
        else shortageView.setOnActionClick { quantity, notes ->
            if (quantity > notice.quantity.remaining) shortageView.showCountError(notice.quantity.remaining)
            else viewModel.addShortages(notice, quantity, notes)
        }

        val items = mutableListOf(
                Item(receiveView, getString(R.string.receiving_init_item_title)),
                Item(shortageView, getString(R.string.discrepancy_shortage_title))
        )

        if (!notice.isRental()) {
            extrasView.setDiscrepancyText(R.string.discrepancy_extras_quantity_text)
            extrasView.isRental(notice.isRental())
            extrasView.setOnActionClick { quantity, notes -> viewModel.addExtras(notice, quantity, notes) }
            items.add(Item(extrasView, getString(R.string.discrepancy_extras_title)))
        }

        return items
    }


    companion object {
        fun consumeASN(fragmentManager: FragmentManager, noticeId: String) =
                show(ConsumeASNConsumableDialog(), fragmentManager, noticeId)

        fun consumeReceiving(fragmentManager: FragmentManager, noticeId: String, position: Int) =
                show(ConsumeReceivingConsumableDialog(), fragmentManager, noticeId, position)


        private fun show(dialog: ReceivingConsumableDialog,
                         fragmentManager: FragmentManager,
                         noticeId: String,
                         position: Int? = null) =
                dialog.apply {
                    arguments = bundleOf(NOTICE_ID_KEY to noticeId, POSITION_KEY to position)
                    show(fragmentManager, this::class.java.simpleName)
                }

        private const val NOTICE_ID_KEY = "notice_id_key"
        private const val POSITION_KEY = "position_key"
    }
}

class ConsumeASNConsumableDialog : ReceivingConsumableDialog() {
    override val actionBtnText: Int = R.string.receiving_receive

    override fun itemCount(item: ReceivingNotice) = item.quantity.remaining

    override fun onProcessBtnClick(count: Int) {
        super.onProcessBtnClick(count)
        viewModel.receiveItem(count)
    }
}

class ConsumeReceivingConsumableDialog : ReceivingConsumableDialog() {
    override val actionBtnText: Int = R.string.receiving_return

    override fun itemCount(item: ReceivingNotice) = item.quantity.processed

    override fun onProcessBtnClick(count: Int) {
        super.onProcessBtnClick(count)
        viewModel.returnItem(count)
    }
}