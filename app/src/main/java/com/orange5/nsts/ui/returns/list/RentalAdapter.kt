package com.orange5.nsts.ui.returns.list

import android.text.SpannableString
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.ui.base.BaseViewHolder
import com.orange5.nsts.util.Colors
import com.orange5.nsts.util.format.DateFormatter
import kotlinx.android.synthetic.main.item_rental_return.view.*
import com.orange5.nsts.util.Spannable

class RentalAdapter(private val listener: Listener)
    : ListAdapter<OrderRentalItem, RentalAdapter.RentalVH>(ListUtils()) {

    var isInspection: Boolean = false
    var query = ""

    fun submitList(list: MutableList<OrderRentalItem>?, query: String = "") {
        this.query = query
        super.submitList(list)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RentalVH =
            RentalVH(LayoutInflater.from(parent.context).inflate(R.layout.item_rental_return, parent, false))

    override fun onBindViewHolder(holder: RentalVH, position: Int) = holder.bind(getItem(position), query)

    inner class RentalVH(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: OrderRentalItem, query: String) {
            with(itemView) {
                productDescription.text = BaseViewHolder.highlightBackground(item.rentalProduct.productDescription, query)
                productNumber.text = BaseViewHolder.highlightBackground(item.productNumber, query)
                manufacturerNumber.text = item.rentalProduct.manufacturerNumber
                uun.text = resources.getString(R.string.common_uun_nr, item.uniqueUnitNumber)
                date.text = resources.getString(R.string.rental_return_activity_rented_start_date, DateFormatter
                        .convertDbDateToUI(item.startDate))
                isDamaged.setOnCheckedChangeListener { _, b -> item.wasDamaged = b }
                isDamaged.isChecked = item.wasDamaged
                if(!isInspection) {
                    dueDate.text = getDueDateText(item)
                } else {
                    dueDate.visibility = GONE
                }
                setOnClickListener {
                    item.wasDamaged = isDamaged.isChecked
                    listener.onItemClick(item)
                }
            }
        }

        private fun getDueDateText(item: OrderRentalItem): SpannableString {
            val dateTime = DateFormatter.convertToDateTime(item.dueDate)
            val color = when {
                dateTime.isBeforeNow -> Colors.RED
                dateTime.minusDays(1).isBeforeNow -> Colors.ORANGE
                else -> Colors.GREEN
            }
            val formatted = DateFormatter.convertDbDateToUI(dateTime)
            return Spannable.highlightTextColor("Due on: $formatted", color, "Due on: $formatted")
        }
    }

    private class ListUtils : DiffUtil.ItemCallback<OrderRentalItem>() {
        override fun areItemsTheSame(oldItem: OrderRentalItem, newItem: OrderRentalItem) = oldItem == newItem
        override fun areContentsTheSame(oldItem: OrderRentalItem, newItem: OrderRentalItem) = oldItem.orderItemId == newItem.orderItemId
    }

    interface Listener {
        fun onItemClick(item: OrderRentalItem)
    }
}