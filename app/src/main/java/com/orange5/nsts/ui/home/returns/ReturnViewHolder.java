package com.orange5.nsts.ui.home.returns;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.entities.Customer;
import com.orange5.nsts.data.db.entities.OrderRentalItem;
import com.orange5.nsts.data.db.entities.RentalReturn;
import com.orange5.nsts.service.rentalReturn.RentalReturnStatus;
import com.orange5.nsts.ui.base.BaseViewHolder;
import com.orange5.nsts.util.format.DateFormatter;

import java.util.List;

import com.orange5.nsts.util.StringUtils;

public class ReturnViewHolder extends BaseViewHolder {

    private GradientDrawable openBackground;
    private GradientDrawable processedBackground;

    private final RentalReturn currentRentalReturn;
    private final Context context;

    public ReturnViewHolder(Context context, RentalReturn rentalReturn) {
        this.context = context;
        this.currentRentalReturn = rentalReturn;
        openBackground = new GradientDrawable();
        openBackground.setColor(RentalReturnStatus.open.getColor());
        openBackground.setCornerRadius(context.getResources().getDimensionPixelSize(R.dimen.common_3dp));

        processedBackground = new GradientDrawable();
        processedBackground.setColor(RentalReturnStatus.processed.getColor());
        processedBackground.setCornerRadius(context.getResources().getDimensionPixelSize(R.dimen.common_3dp));
    }

    public String getCreationDate() {
        String dateString = currentRentalReturn.getStartDate();
        if (StringUtils.isEmpty(dateString)) {
            return "";
        }
        return DateFormatter.convertDbDateToUI(dateString);
    }

    public String getClosedDate() {
        String dateString = currentRentalReturn.getCompleteDate();
        if (dateString == null) {
            return "";
        }
        return DateFormatter.convertDbDateToUI(dateString);
    }

    public String getCustomer() {
        Customer customer = currentRentalReturn.getCustomerOrNull();
        if (customer == null) {
            return "Multiple Customers";
        }
        return customer.toString();
    }

    public String getItemCount() {
        int count = -1;
        List<OrderRentalItem> items = currentRentalReturn.getItems();
        if (items != null && items.size() > 0) {
            count = items.size();
        }
        return context.getResources().getQuantityString(R.plurals.rental_return_activity_n_items, count, count);
    }

    public String getStatus() {
        return RentalReturnStatus.of(currentRentalReturn).getName();
    }

    public Drawable getStatusBackground() {
        return RentalReturnStatus.of(currentRentalReturn).equals(RentalReturnStatus.open) ?
                openBackground : processedBackground;
    }
}
