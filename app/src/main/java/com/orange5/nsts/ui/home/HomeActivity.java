package com.orange5.nsts.ui.home;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.ColorRes;
import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;
import androidx.core.util.Pair;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.orange5.nsts.R;
import com.orange5.nsts.ui.base.ActivityType;
import com.orange5.nsts.ui.base.BaseActivity;
import com.orange5.nsts.ui.customer.SelectCustomerActivity;
import com.orange5.nsts.ui.extensions.ContextExtensionsKt;
import com.orange5.nsts.ui.home.bottommenu.HomeNavigationView;
import com.orange5.nsts.ui.home.bottommenu.OnBottomMenuClickListener;
import com.orange5.nsts.ui.returns.full.revision.RevisionActivity;
import com.orange5.nsts.ui.returns.quick.QuickReturnActivity;
import com.orange5.nsts.ui.settings.SettingsActivity;
import com.orange5.nsts.ui.shipping.transfer.ShippingNoticeActivity;
import com.orange5.nsts.util.UI;
import com.orange5.nsts.util.adapters.pager.PagerAdapterBuilder;
import com.orange5.nsts.util.view.ApiSettingsDialog;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import butterknife.BindView;

public class HomeActivity extends BaseActivity implements OnBottomMenuClickListener {

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.homeNavigationView)
    HomeNavigationView homeNavigationView;
    @BindView(R.id.progress)
    View progress;

    private ImageView syncIcon;
    private SearchView searchView;

    private int lastTab = 0;
    private boolean shouldAnimate = false;
    private boolean isSyncFailed = false;
    private long lastBack = System.currentTimeMillis() - 3000;
    private HomeViewModel homeViewModel;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_main;
    }

    @Override
    protected int getMenuResourceId() {
        return R.menu.search_sync_settings;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(getMenuResourceId(), menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        initSearch(searchItem);
        setSyncMenuIcon(menu);
        return true;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeViewModel = getVMFromActivity(HomeViewModel.class);
        homeViewModel.fetchData();
        homeViewModel.liveChangeIp.observe(this, this::requestChangeIp);
        homeViewModel.livePingServer.observe(this, Void -> onPingSuccess());
        homeViewModel.inspectionBadge.observe(this, this::updateInspectionBadge);
        homeViewModel.receivingBadge.observe(this, this::updateReceivingBadge);
        homeViewModel.isHideQuickOrder.observe(this, this::actionNewOrder);
        homeViewModel.isHideQuickReturn.observe(this, this::actionNewReturn);
        initNavigation();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isSyncInProgress()) animateSync();
        homeViewModel.fetchData();
    }

    private void onPingSuccess() {
        if (syncIcon != null) animateSync();
        runSync();
    }

    private void setSyncMenuIcon(Menu menu) {
        syncIcon = (ImageView) menu.findItem(R.id.sync).getActionView();
        syncIcon.setImageResource(R.drawable.ic_sync_white_24dp);
        syncIcon.setOnClickListener(v -> onSyncIconClick());
    }

    private void onSyncIconClick() {
        animateSync();
        runSync(true);
    }

    private void animateSync() {
        setSyncIconColor(R.color.white);
        shouldAnimate = true;
        isSyncFailed = false;
        animateSyncIcon();
    }

    private void animateSyncIcon() {
        if (syncIcon != null) {
            syncIcon.animate().rotationBy(360).setDuration(800).setInterpolator(new LinearInterpolator()).withEndAction(() -> {
                if (shouldAnimate) {
                    animateSyncIcon();
                } else if (isSyncFailed) {
                    setSyncIconColor(R.color.errorRed);
                }
            }).start();
        }
    }

    private void setSyncIconColor(@ColorRes int color) {
        syncIcon.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(this, color)));
    }

    private void actionNewOrder(Boolean isHideQuickOrder) {
        if (!isHideQuickOrder) {
            ChooseOptionDialog.createNewOrder(this,
                    () -> startActivity(SelectCustomerActivity.newIntent(this, ActivityType.QUICK_ORDER)),
                    () -> startActivity(SelectCustomerActivity.newIntent(this, ActivityType.FULL_ORDER)));

        } else {
            startActivity(SelectCustomerActivity.newIntent(this, ActivityType.FULL_ORDER));
        }
    }

    private void actionNewReturn(Boolean isHideQuickReturn) {
        if (!isHideQuickReturn) {
            ChooseOptionDialog.createNewReturn(this,
                    () -> startActivity(QuickReturnActivity.newIntent(this)),
                    () -> startActivity(SelectCustomerActivity.newIntent(this, ActivityType.FULL_RETURN)));
        } else {
            startActivity(SelectCustomerActivity.newIntent(this, ActivityType.FULL_RETURN));
        }
    }

    private void updateInspectionBadge(int count) {
        homeNavigationView.setInspectionBadge(count);
    }

    private void updateReceivingBadge(int count) {
        homeNavigationView.setReceivingBadge(count);
    }

    private void requestChangeIp(Pair<Boolean, String> pair) {
        ApiSettingsDialog dialog = new ApiSettingsDialog(this, pair.first, pair.second,
                this::actionRebuildSyncServiceAndSync);
        dialog.show();
    }

    private void actionRebuildSyncServiceAndSync(String s) {
        homeViewModel.rebuildSyncService(s);
        homeViewModel.pingServer();
    }

    @Override
    protected void afterMeasure() {
        homeNavigationView.initialize();
        homeNavigationView.setOnBottomManuClickListener(this);
    }

    private void initSearch(MenuItem searchItem) {
        SearchManager searchManager = (SearchManager) HomeActivity.this.getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) searchItem.getActionView();
        final Runnable runnable = () -> homeViewModel.fetchOrders(searchView.getQuery().toString());
        final Handler handler = new Handler();

        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(HomeActivity.this.getComponentName()));
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                @Override
                public boolean onQueryTextSubmit(String query) {
                    UI.closeKeyboard(searchView);
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    handler.removeCallbacksAndMessages(null);
                    handler.postDelayed(runnable, 300);
                    return true;
                }
            });
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initNavigation() {
        PagerAdapterBuilder
                .from(viewPager)
                .into(viewPager)
                .withTabLayout(tabLayout)
                .onPageSelected(selectedPage -> lastTab = selectedPage);
        viewPager.setCurrentItem(lastTab);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                prepareActivity(SettingsActivity.class).start();
                return true;
            case R.id.sync:
                animateSync();
                runSync(true);
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSyncError(String errorMessage) {
        super.onSyncError(errorMessage);
        isSyncFailed = true;
        shouldAnimate = false;
        ContextExtensionsKt.toast(this, errorMessage);
    }

    @Override
    protected void onSyncSuccess(boolean areServerQueriesEmpty) {
        super.onSyncSuccess(areServerQueriesEmpty);
        isSyncFailed = false;
        shouldAnimate = false;
        if (!areServerQueriesEmpty) {
            homeViewModel.fetchData();
        }
    }

    @Override
    public void onBackPressed() {
        /*super.onBackPressed();*/
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        if (System.currentTimeMillis() - lastBack < 3000) {
            finish();
            // Misc.quitApplication(this);
            // System.exit(0);
        } else {
            Toast.makeText(this, R.string.home_activity_toast_press_back_to_quit, Toast.LENGTH_SHORT).show();
            lastBack = System.currentTimeMillis();
        }
    }

    @Override
    public void onBottomItemClick(@NotNull com.orange5.nsts.ui.home.bottommenu.Menu menu) {
        switch (menu) {
            case newOrder:
                homeViewModel.checkConditionForCreatingNewOrder();
                break;
            case newReturn:
                homeViewModel.checkConditionForCreatingNewReturn();
                break;
            case inspectionBin:
                startActivity(RevisionActivity.newIntent(this, null));
                break;
            case receiving:
                startActivity(new Intent(this, ShippingNoticeActivity.class));
                break;
        }
    }
}
