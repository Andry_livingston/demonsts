package com.orange5.nsts.ui.splash;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.entities.User;
import com.orange5.nsts.ui.base.BaseActivity;
import com.orange5.nsts.ui.extensions.ContextExtensionsKt;
import com.orange5.nsts.ui.home.HomeActivity;
import com.orange5.nsts.util.UI;

import org.jetbrains.annotations.Nullable;

import butterknife.BindView;
import butterknife.OnClick;

import static com.orange5.nsts.BuildConfig.BUILD_TYPE;
import static com.orange5.nsts.BuildConfig.VERSION_CODE;
import static com.orange5.nsts.BuildConfig.VERSION_NAME;


public class SplashActivity extends BaseActivity {

    @BindView(R.id.userLoginView)
    UserLoginView userLoginView;

    @BindView(R.id.pleaseWait)
    View pleaseWaitText;

    @BindView(R.id.versionTextView)
    TextView versionTextView;

    @OnClick(R.id.orange_logo)
    void onClicklink() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.orange5.com"));
        startActivity(browserIntent);
    }

    private SplashViewModel viewModel;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_splash;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = getVMFromActivity(SplashViewModel.class);
        viewModel.liveDbLoadSucceed.observe(this, Void -> showLoginView());
        viewModel.liveLogInFail.observe(this, message -> showAlert(getString(message)));
        viewModel.liveLogInSucceed.observe(this, Void -> startHomeScreen());
        viewModel.liveProgress.observe(this, loading -> userLoginView.showLoading(loading));
        viewModel.userData.observe(this, this::setUserData);
        versionTextView.setText(String.format("%s %s (%s)", VERSION_NAME, VERSION_CODE, BUILD_TYPE));
        hideToolbar();
        viewModel.retrieveUser();
        userLoginView.setLoginConsumer(this::startLogin);
        viewModel.loadDb();
    }

    private void startLogin(UserLoginView.UserData user) {
        ContextExtensionsKt.hideKeyboard(this, userLoginView);
        viewModel.logIn(user);
    }

    private void setUserData(User user) {
        userLoginView.setUserData(user);
    }

    private void showLoginView() {
        pleaseWaitText.setVisibility(View.GONE);
        userLoginView.setVisibility(View.VISIBLE);
        userLoginView.bringToFront();
    }

    private void showAlert(String message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP, 0, 200);
        toast.show();
    }

    @Override
    protected void setWindowFlags() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
    }

    private void startHomeScreen() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void afterMeasure() {
        initDefaultUIValues();
    }

    private void initDefaultUIValues() {
        UI.HEIGHT = getWindow().getDecorView().getMeasuredHeight();
        UI.WIDTH = getWindow().getDecorView().getMeasuredWidth();
    }
}
