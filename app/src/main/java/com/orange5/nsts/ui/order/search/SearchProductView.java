package com.orange5.nsts.ui.order.search;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.base.OrderProduct;
import com.orange5.nsts.service.product.ProductViewModel;
import com.orange5.nsts.ui.extensions.OrderExtensionsKt;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchProductView extends FrameLayout {

    @BindView(R.id.productDescription)
    TextView itemName;

    @BindView(R.id.productNumber)
    TextView itemNumber;

    @BindView(R.id.itemType)
    ImageView itemType;

    @BindView(R.id.itemPrice)
    TextView itemPrice;

    @BindView(R.id.availability)
    TextView itemAvailability;

    public SearchProductView(@NonNull Context context) {
        this(context, null);
    }

    public SearchProductView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SearchProductView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public SearchProductView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        inflate(context, R.layout.search_item, this);
        ButterKnife.bind(this, this);
    }

    public void fill(OrderProduct item, String highlight) {
        ProductViewModel viewModel = new ProductViewModel(getContext(), item);
        itemName.setText(viewModel.getProductDescription(highlight));
        itemNumber.setText(viewModel.getProductNumber(highlight));
        itemType.setBackgroundResource(OrderExtensionsKt.orderIcon(item));
        itemPrice.setText(viewModel.getFormattedPrice());
        itemAvailability.setText(viewModel.getAvailability());
    }


}