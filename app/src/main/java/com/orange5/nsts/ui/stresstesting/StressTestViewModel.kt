package com.orange5.nsts.ui.stresstesting

import androidx.lifecycle.MutableLiveData
import com.orange5.nsts.R
import com.orange5.nsts.data.db.base.OrderProduct
import com.orange5.nsts.data.db.entities.Customer
import com.orange5.nsts.data.db.entities.Order
import com.orange5.nsts.data.local.OrderWrapper
import com.orange5.nsts.data.preferencies.RawSharedPreferences
import com.orange5.nsts.service.customer.CustomerService
import com.orange5.nsts.service.order.OrderService
import com.orange5.nsts.service.order.OrderStatus
import com.orange5.nsts.service.order.items.OrderConsumableItemService
import com.orange5.nsts.service.order.items.OrderRentalItemService
import com.orange5.nsts.service.product.ProductService
import com.orange5.nsts.ui.base.ActionLiveData
import com.orange5.nsts.ui.base.BaseViewModel
import com.orange5.nsts.ui.extensions.getRandomIndex
import com.orange5.nsts.ui.extensions.getRandomList
import com.orange5.nsts.util.db.OrderNumberGenerator
import com.orange5.nsts.util.format.DateFormatter
import javax.inject.Inject

class StressTestViewModel @Inject constructor(
        val customerService: CustomerService,
        val orderService: OrderService,
        val rentalService: OrderRentalItemService,
        val consumableService: OrderConsumableItemService,
        val preferences: RawSharedPreferences) : BaseViewModel() {

    val liveProgressCreateOrders = ActionLiveData<Int>()
    val onCreateOrdersSucceed = MutableLiveData<Int>()
    val onCreateOrdersFails = ActionLiveData<Int>()

    fun createDummyOrders(qtyAddOrder: Int) {
        liveProgress.value = false
        runJobIO({
            val customers = customerService.loadAll()
            repeat(qtyAddOrder) { index ->
                val customer = customers[getRandomIndex(customers.size)]
                createDummyOrder(customer)
                liveProgressCreateOrders.postValue(index)
            }
        }, { onCreateOrdersSucceed.postValue(R.string.stress_test_orders_creation_succeed) })
    }

    override fun onError(throwable: Throwable?) {
        super.onError(throwable)
        onCreateOrdersFails.postValue(R.string.stress_test_orders_creation_fails)
    }

    private fun createDummyOrder(selectedCustomer: Customer) {
        val order = createOrder(selectedCustomer)
        orderService.insert(order)
        addProductsToOrder(order)
    }

    private fun createOrder(selectedCustomer: Customer) = Order().apply {
        createdByUserId = preferences.user?.userId
        orderDate = DateFormatter.currentTimeDb()
        deliveryDate = DateFormatter.currentTimeDb()
        creatorCustomerId = selectedCustomer.customerId
        orderStatusId = OrderStatus.open.id
        orderNumber = OrderNumberGenerator.getNext(orderService.getLastOrder("DUMMY"), "DUMMY")
    }

    private fun addProductsToOrder(order: Order) {
        val consumables = getRandomList(ProductService.get().loadAllConsumablesLazy(), 5)
        val rentals = getRandomList(ProductService.get().loadAllRentalsLazy(), 5)
        val products = consumables + rentals
        val orderWrapper = OrderWrapper(order, mutableListOf(), mutableListOf())
        products.forEach {
            addProduct(it, orderWrapper)
        }
        rentalService.saveAll(orderWrapper.rentalItems)
        consumableService.saveAll(orderWrapper.consumableItems)
        order.setOrderSum(orderWrapper.orderSum)
        update(order, orderWrapper)
        orderService.update(order)
    }

    private fun update(order: Order, orderWrapper: OrderWrapper) {
        val (toInsert, toUpdate) = orderWrapper.getConsumableDiff(order.consumableItems)
        consumableService.insertAll(toInsert)
        consumableService.updateAll(toUpdate)
        order.resetRentalItems()
        val (toInsert1, toDelete) = orderWrapper.getRentalDiff(order.rentalItems)
        rentalService.deleteAll(toDelete)
        rentalService.insertAll(toInsert1)
    }

    private fun addProduct(product: OrderProduct, orderWrapper: OrderWrapper, quantity: Int = 1) {
        product.ifRental { orderWrapper.setRentalItems(it, quantity) }
        product.ifConsumable { orderWrapper.setConsumableItems(it, quantity, false) }
    }
}