package com.orange5.nsts.ui.returns.full.inspect

import androidx.lifecycle.MutableLiveData
import com.orange5.nsts.data.db.entities.Customer
import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.data.db.entities.RentalProductItem
import com.orange5.nsts.service.bin.BinService
import com.orange5.nsts.service.customer.CustomerService
import com.orange5.nsts.service.order.items.OrderRentalItemService
import com.orange5.nsts.service.rental.RentalProductItemService
import com.orange5.nsts.service.rental.RentalStatus
import com.orange5.nsts.service.rentalReturn.RentalReturnService
import com.orange5.nsts.ui.returns.ReturnViewModel
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class FullReturnViewModel @Inject constructor(private val returnService: RentalReturnService,
                                              private val orderRentalItemService: OrderRentalItemService,
                                              private val rentalProductService: RentalProductItemService,
                                              private val customerService: CustomerService,
                                              binService: BinService) : ReturnViewModel(orderRentalItemService, binService) {

    val liveCustomer = MutableLiveData<Customer>()

    init {
        fetchReturnItems()
        loadBins()
    }

    override fun createReturn(signature: ByteArray) {
        rentalReturn.apply {
            signImage = signature
            runDbTransaction {
                returnService.insert(this)

                mappedReturns.value?.forEach { item, product ->
                    item.rentalReturnId = rentalReturnId
                    item.returnCustomerId = super.customer!!.customerId
                    product.status = RentalStatus.Inspection.id
                    orderRentalItemService.update(item)
                    rentalProductService.update(product)
                }
            }
        }
    }

    override fun loadRentalReturnItem(items: List<OrderRentalItem>, showAll: Boolean): MutableMap<OrderRentalItem, RentalProductItem> {
        val map = mutableMapOf<OrderRentalItem, RentalProductItem>()
        items.filter {
                    when {
                        showAll -> showAll
                        customer != null -> it.order.creatorCustomerId == customer!!.customerId
                        else -> showAll
                    }
                }
                .forEach {
                    rentalProductService.loadItemByUUN(it.uniqueUnitNumber)?.let { rental -> map[it] = rental }
                }
        return map
    }

    override fun runDbTransaction(action: () -> Unit) {
        addDisposable(rxCompletableCreate { action() }
                .subscribeOn(Schedulers.computation())
                .subscribe({
                    liveRunSync.postValue(null)
                    returnFinished.postValue(customer)
                }, ::onError))
    }

    fun onItemReturned(info: ReturnInformation) {
        val item = orderItem.value
        if (item != null) {
            val product = mappedRentals.value?.get(item)
            product?.let {
                it.containerBin = info.bin

                item.scannedBin = info.bin
                item.wasDamaged = info.wasDamaged
                item.returnCustomerId = item.order.creatorCustomerId
                item.rentalReturnNotes = info.comments
                item.rentalReturnId = rentalReturn.rentalReturnId

                insertReturn(item, it)
                deleteRental(item)
                onSuccessAdded.value = item.uniqueUnitNumber
            }
        }
    }

    fun onUpdateReturnedItem(info: ReturnInformation) {
        val item = orderItem.value
        if (item != null) {
            val product = mappedReturns.value?.get(item)
            product?.let {
                it.containerBin = info.bin
                item.scannedBin = info.bin
                item.wasDamaged = info.wasDamaged
                item.rentalReturnNotes = info.comments
                item.rentalReturnId = rentalReturn.rentalReturnId
                updateReturn(item, it)
            }
        }
    }

    fun setCustomer(customerId: String) {
        customer = customerService.loadById(customerId)
        liveCustomer.value = customer
    }
}

