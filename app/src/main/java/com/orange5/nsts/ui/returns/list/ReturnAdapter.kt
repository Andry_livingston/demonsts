package com.orange5.nsts.ui.returns.list

import android.text.SpannableString
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.ui.base.BaseViewHolder
import com.orange5.nsts.util.Spannable
import com.orange5.nsts.util.format.DateFormatter
import kotlinx.android.synthetic.main.item_returning.view.*

class ReturnAdapter(private val listener: Listener)
    : RecyclerView.Adapter<ReturnAdapter.ReturnVH>() {

    private var items = mutableListOf<OrderRentalItem>()
    private var query = ""

    override fun getItemCount() = items.size

    fun submitList(items: List<OrderRentalItem>, query: String = "") {
        this.query = query
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReturnVH =
            ReturnVH(LayoutInflater.from(parent.context).inflate(R.layout.item_returning, parent, false))

    override fun onBindViewHolder(holder: ReturnVH, position: Int) = holder.bind(items[position], query)

    inner class ReturnVH(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: OrderRentalItem, query: String) {
            with(itemView) {
                productDescription.text = BaseViewHolder.highlightBackground(item.rentalProduct.productDescription, query)
                productNumber.text = BaseViewHolder.highlightBackground(item.productNumber, query)
                manufacturerNumber.text = item.rentalProduct.manufacturerNumber
                uun.text = resources.getString(R.string.common_uun_nr, item.uniqueUnitNumber)
                date.text = resources.getString(R.string.rental_return_activity_rented_start_date, DateFormatter
                        .convertDbDateToUI(item.startDate))
                statusIcon.background = getDrawable(item)
                itemView.setOnLongClickListener{ listener.onItemDelete(item)
                    true }
                setOnClickListener {listener.onItemClick(item)}

                if (item.rentalReturnNotes.isNullOrEmpty()) {
                    notes.visibility = View.GONE
                } else {
                    notes.visibility = View.VISIBLE
                    notes.text = getBinAndNotesText(item)
                }
            }
        }

        private fun getDrawable(item: OrderRentalItem) =
                if (item.wasDamaged) ContextCompat.getDrawable(itemView.context, R.drawable.alert_circle_outline)
                else ContextCompat.getDrawable(itemView.context, R.drawable.check_circle_outline)


        private fun getBinAndNotesText(item: OrderRentalItem): SpannableString {
            val notes = item.rentalReturnNotes
            val color = if (item.wasDamaged) R.color.errorRed else R.color.colorPrimary

            return Spannable.highlightTextColor(notes, color, notes)
        }
    }


    interface Listener {
        fun onItemClick(item: OrderRentalItem)
        fun onItemDelete(item: OrderRentalItem)
    }
}