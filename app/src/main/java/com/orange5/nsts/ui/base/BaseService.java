package com.orange5.nsts.ui.base;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;

import com.orange5.nsts.api.NetworkUtils;
import com.orange5.nsts.receivers.BroadcastService;
import com.orange5.nsts.sync.service.SyncService;

import java.util.concurrent.Callable;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import timber.log.Timber;

abstract public class BaseService extends Service implements HasAndroidInjector {

    protected static boolean isRunning = false;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    DispatchingAndroidInjector<Object> injector;

    @Inject
    public BroadcastService broadcastService;

    @Override
    public AndroidInjector<Object> androidInjector() {
        return injector;
    }

    @Override
    public void onCreate() {
        AndroidInjection.inject(this);
        super.onCreate();
        isRunning = true;
    }

    protected void addDisposable(Disposable disposable) {
        compositeDisposable.add(disposable);
    }

    protected <T> Single<T> rxSingleCreate(Callable<T> call) {
        return Single.create(emitter -> {
            try {
                T result = call.call();
                if (!emitter.isDisposed()) emitter.onSuccess(result);
            } catch (Throwable t) {
                if (!emitter.isDisposed()) emitter.onError(t);
            }
        });
    }

    protected Completable rxCompletableCreate(Action runnable) {
        return Completable.create(emitter -> {
            try {
                runnable.run();
                if (!emitter.isDisposed()) emitter.onComplete();
            } catch (Throwable t) {
                if (!emitter.isDisposed()) emitter.onError(t);
            }
        });
    }

    public final static class ServiceRunner {
        public enum StartState {
            SUCCESS,
            NO_WIFI_CONNECTION,
            ALREADY_RUNNING
        }

        public static StartState getServiceStartState(Context context) {
            StartState state = StartState.SUCCESS;
            if (isServiceRunning(context)) {
                state = StartState.ALREADY_RUNNING;
            } else if (!NetworkUtils.Network.isWifiConnected(context)) {
                state = StartState.NO_WIFI_CONNECTION;
            }
            return state;
        }

        private static boolean isServiceRunning(Context context) {
            boolean isRunning = false;
            ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            if (manager != null) {
                for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                    isRunning = SyncService.class.getName().equals(service.service.getClassName());
                }
            }
            return isRunning;
        }
    }

    @Override
    public void onDestroy() {
        isRunning = false;
        compositeDisposable.clear();
        Timber.tag(this.getClass().getSimpleName()).d("Service destroyed");
        super.onDestroy();
    }
}
