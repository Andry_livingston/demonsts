package com.orange5.nsts.ui.home.bottommenu;

import com.orange5.nsts.R;

public enum Menu {
    newOrder(R.string.common_new_order, R.drawable.ic_orders_icon),
    newReturn(R.string.home_bottom_menu_create_new_return, R.drawable.ic_returns_icon),
    receiving(R.string.menu_receiving, R.drawable.ic_receiving_icon),
    inspectionBin(R.string.menu_inspection, R.drawable.ic_inspection_icon);

    private int textResourceId;
    private int drawableResourceId;

    public int getTextResourceId() {
        return textResourceId;
    }

    public int getDrawableResourceId() {
        return drawableResourceId;
    }

    Menu(int textResourceId, int drawableResourceId) {
        this.textResourceId = textResourceId;
        this.drawableResourceId = drawableResourceId;
    }
}
