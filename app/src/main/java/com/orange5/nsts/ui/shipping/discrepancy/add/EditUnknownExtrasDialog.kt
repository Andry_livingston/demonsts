package com.orange5.nsts.ui.shipping.discrepancy.add

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.observe
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseDialogFragment
import com.orange5.nsts.ui.extensions.setOnClickListener
import com.orange5.nsts.ui.shipping.ShippingProcessViewModel
import com.orange5.nsts.ui.shipping.discrepancy.repository.Discrepancy
import com.orange5.nsts.ui.shipping.receiving.ReceivingViewModel
import com.orange5.nsts.ui.shipping.takeaway.TakeAwayViewModel
import kotlinx.android.synthetic.main.dialog_add_unknown_extras.*

abstract class EditUnknownExtrasDialog : BaseDialogFragment() {

    abstract val viewModel: ShippingProcessViewModel

    override fun getLayoutResourceId(): Int = R.layout.dialog_add_unknown_extras

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val id = arguments?.getString(UNKNOWN_ID_KEY) ?: ""
        viewModel.loadDiscrepancyItem(id)
        viewModel.discrepancyItem.observe(this, ::setView)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        title.text = getString(R.string.discrepancy_edit_unknown_product)
        addDiscrepancy.text = getString(R.string.discrepancy_edit_unknown)
        increment.setOnClickListener { updateCount(1) }
        decrement.setOnClickListener { updateCount(-1) }
        cancel.setOnClickListener(::dismiss)
        description.doOnTextChanged { _, _, _, _ -> clearError() }
    }

    private fun setView(item: Discrepancy) {
        quantity.setText(item.quantity.toString())
        description.setText(item.description)
        notes.setText(item.comment)
        addDiscrepancy.setOnClickListener { onAddDiscrepancyClick(item) }
    }

    private fun updateCount(count: Int) {
        val updated = quantity.text.toString().toInt() + count
        quantity.setText((if (updated < 1) 1 else updated).toString())
    }

    private fun onAddDiscrepancyClick(item: Discrepancy) {
        val description = description.text.toString()
        if (description.isEmpty()) showError()
        else {
            item.description = description
            item.quantity = quantity.text.toString().toInt()
            item.comment = notes.text.toString()
            viewModel.onEditDiscrepancy(item)
            dismiss()
        }
    }

    private fun showError() {
        descriptionLayout.error = getString(R.string.discrepancy_description_is_required)
    }

    private fun clearError() {
        descriptionLayout.error = null
    }

    companion object {
        private const val UNKNOWN_ID_KEY = "unknown_id_key"

        @JvmStatic
        fun fromReceiving(fragmentManager: FragmentManager, id: String) = show(ReceivingEditUnknownExtrasDialog(), fragmentManager, id)

        @JvmStatic
        fun fromTakeAway(fragmentManager: FragmentManager, id: String) = show(TakeAwayEditUnknownExtrasDialog(), fragmentManager, id)

        private fun show(dialog: EditUnknownExtrasDialog, fragmentManager: FragmentManager, id: String) =
                dialog.apply {
                    arguments = bundleOf(UNKNOWN_ID_KEY to id)
                    show(fragmentManager, this::class.java.simpleName)
                }
    }

}

class ReceivingEditUnknownExtrasDialog : EditUnknownExtrasDialog() {
    override val viewModel by activityViewModels<ReceivingViewModel> { factory }
}

class TakeAwayEditUnknownExtrasDialog : EditUnknownExtrasDialog() {
    override val viewModel by activityViewModels<TakeAwayViewModel> { factory }
}