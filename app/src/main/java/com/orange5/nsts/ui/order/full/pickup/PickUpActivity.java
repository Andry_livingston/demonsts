package com.orange5.nsts.ui.order.full.pickup;

import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.SparseIntArray;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.orange5.nsts.R;
import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.data.db.entities.BinNumber;
import com.orange5.nsts.data.db.entities.ConsumableBin;
import com.orange5.nsts.data.db.entities.ConsumableProduct;
import com.orange5.nsts.data.db.entities.OrderConsumableItem;
import com.orange5.nsts.data.db.entities.OrderRentalItem;
import com.orange5.nsts.data.db.entities.RentalProductItem;
import com.orange5.nsts.scan.ScanningActivity;
import com.orange5.nsts.service.rental.AggregatedRentalItem;
import com.orange5.nsts.ui.home.BottomMenuDialog;
import com.orange5.nsts.ui.order.full.pickup.adapter.bins.BinsItemView;
import com.orange5.nsts.ui.order.full.pickup.adapter.pickup.PickupItemsAdapter;
import com.orange5.nsts.ui.order.full.pickup.pick.consumables.ConsumablePickDialog;
import com.orange5.nsts.ui.order.full.pickup.pick.consumables.ConsumableReturnDialog;
import com.orange5.nsts.ui.order.full.pickup.pick.rentals.RentalPickDialog;
import com.orange5.nsts.ui.order.full.pickup.pick.rentals.RentalReturnDialog;
import com.orange5.nsts.ui.order.full.pickup.ship.ShipOutItemView;
import com.orange5.nsts.util.CollectionUtils;
import com.orange5.nsts.util.UI;
import com.orange5.nsts.util.adapters.pager.PagerAdapterBuilder;
import com.orange5.nsts.util.adapters.recycler.RecyclerAdapterBuilder;
import com.orange5.nsts.util.format.CurrencyFormatter;
import com.orange5.nsts.util.view.dialogs.Dialogs;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import timber.log.Timber;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.orange5.nsts.ui.order.full.pickup.pick.consumables.ConsumablePickDialog.OVERSELL;

public class PickUpActivity extends ScanningActivity implements BottomMenuDialog.Listener,
        PickupItemsAdapter.PickItemListener {

public static final String ORDER_ID = "ORDER_ID";

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.pickUpList)
    RecyclerView pickUpList;
    @BindView(R.id.binsList)
    RecyclerView binsList;
    @BindView(R.id.shipOutList)
    RecyclerView shipOutList;
    @BindView(R.id.orderTotalText)
    TextView orderTotalText;
    @BindView(R.id.pickedProgressText)
    TextView pickedProgressText;
    @BindView(R.id.pickedProgressBar)
    ProgressBar pickedProgressBar;
    @BindView(R.id.bottomNavigation)
    BottomNavigationView bottomNavigation;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.selectedBin)
    TextView selectedBin;
    @BindView(R.id.binsContainer)
    FrameLayout binsContainer;
    @BindView(R.id.syncNotification)
    LinearLayout syncNotification;
    @BindView(R.id.apply)
    TextView apply;
    @BindView(R.id.frameLayout)
    FrameLayout progressContainer;


    private PickupItemsAdapter pickupItemsAdapter;
    private BinNumber scannedBin;
    private PickUpViewModel viewModel;
    private boolean containerVisible = false;
    private boolean isBinSelected = false;
    private boolean isShipOutShown = false;

    public static final String RENTAL = "rental_action";
    public static final String CONSUMABLE = "consumable_action";
    public static final String EMPTY_TYPE = "empty_action";

    private boolean isRentalScanWorks = false;
    private boolean isConsumableScanWorks = false;

    public static Intent newIntent(Context context, String orderId) {
        Intent intent = new Intent(context, PickUpActivity.class);
        intent.putExtra(ORDER_ID, orderId);
        return intent;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_pick_up;
    }

    @Override
    protected int getMenuResourceId() {
        return R.menu.finish_order_menu;
    }

    @Override
    public void onItemClick(OrderItem item) {
        showPickUpDialog(item);
    }

    @Override
    public void onAddItemsClick(OrderItem item) {
        item.ifConsumable(it -> {
            ConsumableBin bin = null;
            List<ConsumableBin> bins;
            bins = viewModel.getBinsForPickUp(it.getConsumableProduct());
            for (ConsumableBin c : bins) {
                if (c.getBinNumber().equals(it.getScannedBin())) {
                    bin = c;
                    break;
                }
            }
            pickupConsumable(it, bin, it.getQuantity());
        });
        item.ifRental(it -> pickupRental(it, it.getScannedBin(), it.getUniqueUnitNumber()));
    }

    @Override
    public void onBackorderChecked(OrderItem item, boolean isChecked) {
        viewModel.setBackorderFlag(item, isChecked);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = getVMFromActivity(PickUpViewModel.class);
        viewModel.liveFailLoadOrder.observe(this, (id -> forceCloseIfError(getString(R.string.logging_error_messages_order_from_db_is_null, id))));
        viewModel.liveRunSync.observe(this, Void -> runSync());
        viewModel.liveBinsListUpdate.observe(this, this::refreshSelectedBinItems);
        viewModel.liveProgress.observe(this, this::handleProgressDialog);
        viewModel.liveOrderFinished.observe(this, (Void) -> finish());
        viewModel.liveUpdateUi.observe(this, this::updateUi);
        viewModel.liveCantDeleteOrder.observe(this, this::showAlertDialog);
        viewModel.liveDeleteOrder.observe(this, shouldFinish -> finish());
        viewModel.isServerDeleteOrder.observe(this, this::onSyncLoaded);
        getScanningViewModel().liveBinScanned.observe(this, this::onBinScanned);
        getScanningViewModel().liveConsumableScanned.observe(this, this::onConsumableScanned);
        getScanningViewModel().liveRentalItemScanned.observe(this, this::onRentalScanned);
        setOrder();
        initNavigation();
        setTitle(viewModel.getOrder().getCustomer().toString());
        viewModel.setExistingBins();
        pickupItemsAdapter = new PickupItemsAdapter(this);
        pickUpList.setAdapter(pickupItemsAdapter);
        recalculateViews();
        setListeners();
    }

    private void onSyncLoaded(Boolean isServerDeleteOrder) {
        if (isServerDeleteOrder) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.common_invalid_state)
                    .setMessage(getString(R.string.pickup_order_was_delete))
                    .setPositiveButton(getString(R.string.common_ok), (dialog, which) -> dialog.dismiss())
                    .setOnDismissListener(d -> finish())
                    .show();
        } else {
            handleSyncNotification(true);
        }
    }

    private void showAlertDialog(Boolean aBoolean) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.common_warning)
                .setMessage(getString(R.string.pick_up_activity_cant_delete_order))
                .setPositiveButton(getString(R.string.common_ok), (dialog, which) -> dialog.dismiss())
                .show();
    }

    private void setListeners() {
        apply.setOnClickListener(v -> {
            handleSyncNotification(false);
            recalculateViews();
        });
        selectedBin.setOnClickListener((v) -> {
            if (containerVisible) {
                binsContainer.setVisibility(GONE);
                containerVisible = false;
            } else {
                binsContainer.setVisibility(VISIBLE);
                containerVisible = true;
            }
        });
    }

    @Override
    protected void onSyncSuccess(boolean areServerQueriesEmpty) {
        super.onSyncSuccess(areServerQueriesEmpty);
        if (!areServerQueriesEmpty) viewModel.verifyOrderStatus();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.finish) {
            if (canFinishOrder()) {
                showFinishOrderDialog();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public int notFoundMessage() {
        return R.string.pickup_scan_rental_uun_or_consumable_product_number;
    }

    public void onBinScanned(BinNumber binNumber) {
        if (binNumber != null) {
            Map<BinNumber, List<OrderItem>> binNumbers = viewModel.getBinNumbers();
            if (binNumbers.keySet().contains(binNumber)) {
                scannedBin = binNumber;
                selectedBin.setText(getString(R.string.pickup_selected_bin, binNumber.getBinNum()));
                List<OrderItem> items = binNumbers.get(binNumber);
                if (CollectionUtils.isNotEmpty(items)) {
                    items.forEach(item -> item.setScannedBin(binNumber));
                }
                viewModel.refreshSelectedBinItems(scannedBin);
                isBinSelected = true;
                UI.setVisibility(selectedBin, !isShipOutShown);
            }
        } else {
            isRentalScanWorks = false;
            showMessage(getString(R.string.pickup_warning_bin_dosent_exists));
        }
    }


    public void onConsumableScanned(ConsumableProduct product) {
        if (isConsumableScanWorks) return;

        isConsumableScanWorks = true;
        OrderItem item = viewModel.getItem(product);
        if (item != null) {
            ConsumableBin consumableBin;
            int quantity = item.asConsumable().getQuantity() - item.asConsumable().getPicked();
            if (quantity <= 0) {
                isConsumableScanWorks = false;
                showMessage(getString(R.string.pick_up_activity_already_picked));
                return;
            }
            List<ConsumableBin> bins = viewModel.getBinsForPickUp(product);
            if (CollectionUtils.isEmpty(bins)) {
                consumableBin = null;
            } else {
                consumableBin = bins.get(0);
                if (scannedBin != null) {
                    for (ConsumableBin bin : bins) {
                        if (bin.getBinNumber().equals(scannedBin)) {
                            consumableBin = bin;
                            break;
                        }
                    }
                }
                if (quantity >= consumableBin.getQuantity()) {
                    quantity = consumableBin.getQuantity();
                }
            }
            pickupConsumable(item.asConsumable(), consumableBin, quantity);
        } else {
            isConsumableScanWorks = false;
            showMessage(getString(R.string.warning_no_item_in_order, product.getProductId()));
        }
    }

    public void onRentalScanned(RentalProductItem product) {
        if (isRentalScanWorks) return;

        isRentalScanWorks = true;
        OrderItem item = viewModel.getItem(product.getRentalProduct());
        if (item != null) {
            BinNumber bin = product.getRentalProduct().getDefaultBin();
            List<BinNumber> bins = viewModel.getBinsForPickUp(product.getRentalProduct());
            if (bins.contains(scannedBin)) {
                bin = scannedBin;
            }
            pickupRental(item.asRental(), bin, product.getUniqueUnitNumber());
        } else {
            showMessage(getString(R.string.warning_no_item_in_order, product.getProductId()));
        }
    }

    private void setBinsList(List<OrderItem> data) {
        new RecyclerAdapterBuilder<BinsItemView, OrderItem>(binsList)
                .viewFactory(() -> new BinsItemView(this))
                .data(() -> data)
                .bind(BinsItemView::fill)
                .onClick(R.id.addAll, (position, item) -> {
                    item.ifRental(it ->
                            pickupRental(it, it.getScannedBin(), it.getUniqueUnitNumber()));
                    item.ifConsumable(this::handleAddAllConsumableClick);
                    viewModel.refreshSelectedBinItems(scannedBin);
                })
                .vertical();
    }

    private void refreshSelectedBinItems(List<OrderItem> items) {
        if (CollectionUtils.isNotEmpty(items)) {
            setBinsList(items);
        } else {
            selectedBin.setVisibility(GONE);
            binsList.setVisibility(GONE);
        }
    }

    private void handleProgressDialog(boolean enable) {
        UI.setVisibility(progress, enable);
    }

    private void handleAddAllConsumableClick(OrderConsumableItem item) {
        int quantity = item.asConsumable().getQuantity() - item.asConsumable().getPicked();
        if (quantity <= 0) {
            showMessage(getString(R.string.pick_up_activity_already_picked));
            return;
        }
        ConsumableBin consumableBin = item.getConsumableProduct().getConsumableBin(item.getScannedBin());
        if (consumableBin != null) {
            if (quantity >= consumableBin.getQuantity()) {
                quantity = consumableBin.getQuantity();
            }
            pickupConsumable(item, consumableBin, quantity);
        }
    }

    private void showFinishOrderDialog() {
        OrderDetailsDialog.show(this, viewModel.getOrder(), PickUpActivity.this::finishOrder);
    }

    private void initNavigation() {
        SparseIntArray map = new SparseIntArray();
        map.put(R.id.orderProducts, 0);
        map.put(R.id.shipOutBin, 1);

        PagerAdapterBuilder
                .from(viewPager)
                .into(viewPager)
                .onPageSelected(position ->
                        ((ViewGroup) bottomNavigation
                                .getChildAt(0))
                                .getChildAt(position)
                                .performClick());

        bottomNavigation.setOnNavigationItemSelectedListener(item -> {
            int index = map.get(item.getItemId());
            isShipOutShown = index == 1;
            UI.setVisibility(selectedBin, !isShipOutShown && isBinSelected);
            UI.setVisibility(binsContainer, !isBinSelected && containerVisible);
            viewPager.setCurrentItem(index);
            return true;
        });
    }

    private void recalculateAndUpdateView() {
        int pickedItems = viewModel.recalculateAndGetProductCount();
        int totalItems = viewModel.getTotalItemsCount();
        pickedProgressBar.setMax(totalItems);
        ObjectAnimator progressAnimator =
                ObjectAnimator.ofInt(pickedProgressBar, "progress", pickedProgressBar.getProgress(), pickedItems);
        progressAnimator.setDuration(400);
        progressAnimator.start();

        String itemsText = getString(R.string.common_picked_d_d, pickedItems, totalItems);
        pickedProgressText.setText(itemsText);

        String sum = CurrencyFormatter.formatPrice(viewModel.getOrder().getOrderSum());
        orderTotalText.setText(getString(R.string.pick_up_activity_total_s, sum));
    }

    private void reloadShipOutBinItems() {
        new RecyclerAdapterBuilder<ShipOutItemView, OrderItem>(shipOutList)
                .viewFactory(() -> new ShipOutItemView(this))
                .data(() -> viewModel.getShipOutBinItems())
                .bind(ShipOutItemView::fill)
                .onClick(R.id.returnButton, this::showReturnDialog)
                .vertical();

        shipOutList.smoothScrollToPosition(viewModel.getShipOutBinItems().size());
    }

    private void reloadOrderItems() {
        List<AggregatedRentalItem> aggregatedRentalsList = viewModel.getAggregatedRentalsList();
        List<OrderItem> allItems = new ArrayList<>(aggregatedRentalsList);
        allItems.addAll(viewModel.getConsumableItems());
        pickupItemsAdapter.submitList(allItems);
    }

    private void showPickUpDialog(OrderItem item) {
        item.ifConsumable(it -> {
            if (it.allPicked()) {
                showMessage(getString(R.string.pick_up_activity_already_picked));
                return;
            }

            List<ConsumableBin> bins = viewModel.getBinsForPickUp(it.getConsumableProduct());
            ConsumablePickDialog.create(this, bins, it, (bin, quantity) ->
                    pickupConsumable(it, bin, quantity)
            );
        });

        item.ifAggregatedRental(aggregatedRentalItem -> {
            OrderRentalItem firstUnpicked = aggregatedRentalItem.getFirstUnpicked();
            if (firstUnpicked == null) {
                showMessage(getString(R.string.pick_up_activity_no_items_to_pick));
                return;
            }

            if (firstUnpicked.getBinInformation().getAvailableQuantity() < 1) {
                showMessage(getString(R.string.warning_pick_up_rental_zero_quantity_availability));
                return;
            }

            RentalPickDialog.with(this)
                    .resultConsumer((bin, quantity) -> pickupRental(firstUnpicked, bin, quantity))
                    .pick(firstUnpicked);
        });
    }

    private void pickupRental(OrderRentalItem firstUnpicked,
                              BinNumber bin,
                              String quantity) {
        viewModel.pickUp(firstUnpicked, bin, quantity);
    }

    private void pickupConsumable(OrderConsumableItem item,
                                  @Nullable ConsumableBin bin,
                                  Integer quantity) {
        viewModel.pickUp(item,
                (bin == null || bin.getBinId().equalsIgnoreCase(OVERSELL)) ? null : bin,
                quantity);
    }

    private void showReturnDialog(int position, OrderItem item) {
        item.ifRental(rentalItem -> RentalReturnDialog.with(this)
                .resultConsumer((binNumber, s) -> {
                    viewModel.returnItem(rentalItem, binNumber);
                    viewModel.refreshSelectedBinItems(scannedBin);
                }).showForItem(rentalItem)
        );

        item.ifConsumable(consumableItem -> {
            if (consumableItem.getPicked() == 0 && consumableItem.isOversell()) {
                Toast.makeText(this, R.string.pick_up_activity_nothing_to_return_oversell, Toast.LENGTH_SHORT).show();
                return;
            }

            ConsumableReturnDialog.with(this).resultConsumer((consumableBin, quantity) -> {
                viewModel.returnItem(consumableItem, consumableBin, quantity);
                viewModel.refreshSelectedBinItems(scannedBin);
            }).showForItem(item.asConsumable());
        });
    }

    private void setOrder() {
        String orderId = getIntent().getStringExtra(ORDER_ID);
        if (orderId == null) {
            forceCloseIfError(getString(R.string.logging_error_messages_order_id_is_null, this.getClass().getSimpleName()));
            return;
        }
        viewModel.loadExisting(orderId);
    }

    private boolean canFinishOrder() {
        if (!viewModel.hasPickedItems()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.pick_up_activity_pick_at_least_one);
            builder.setTitle(R.string.pick_up_activity_unpicked_items);
            builder.setPositiveButton(R.string.pick_up_activity_pick, (dialog, which) -> {
                viewPager.setCurrentItem(0, true);
                dialog.dismiss();
            });
            builder.show();
            return false;
        }

        if (!viewModel.areAllPicked()) {
            String message = getString(R.string.pick_up_activity_you_have_unpicked_items );

            if (viewModel.hasBackorderItems()) {
                message += getString(R.string.pick_up_activity_has_backorder);
            }
            Dialogs.newDialog()
                    .setTitle(R.string.warning)
                    .setMessage(message)
                    .no(null)
                    .yes(this::onFinishDialogClicked)
                    .show();
            return false;

        }
        return true;
    }

    private void finishOrder(Bitmap signatureBitmap) {
        viewModel.finishOrder(signatureBitmap);
    }

    private void forceCloseIfError(String error) {
        finish();
        showMessage(error);
        Timber.e(error);
    }

    private void updateUi(String productTypePicked) {
        updateScanWorkFlag(productTypePicked);
        recalculateViews();
    }

    private void recalculateViews() {
        reloadOrderItems();
        reloadShipOutBinItems();
        recalculateAndUpdateView();
    }

    private void handleSyncNotification(boolean isVisible) {
        progressContainer.setElevation(isVisible ? 0 : getResources().getDimensionPixelSize(R.dimen.common_5dp));
        syncNotification.setVisibility(isVisible ? VISIBLE : GONE);
    }

    private void updateScanWorkFlag(String productTypePicked) {
        switch (productTypePicked) {
            case CONSUMABLE: {
                isConsumableScanWorks = false;
                break;
            }
            case RENTAL: {
                isRentalScanWorks = false;
                break;
            }
            default:
                break;
        }
    }

    private void onFinishDialogClicked() {
        showFinishOrderDialog();
        viewModel.shouldCreateNewOrder = true;
    }

    @Override
    public void onBottomButtonClick() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.common_remove_order)
                .setMessage(getString(R.string.common_confirm_order_removal, viewModel.getOrder().getOrderNumber()))
                .setPositiveButton(getString(R.string.common_remove), (dialog, which) -> {
                    viewModel.deleteOrder();
                    dialog.dismiss();
                })
                .setNegativeButton(getString(R.string.common_cancel), (dialog, which) -> {
                    dialog.dismiss();
                })
                .show();
    }

    @Override
    public void onTopButtonClick() {
        finish();
    }

    @Override
    public void onBackPressed() {
        BottomMenuDialog.show(getSupportFragmentManager());
    }
}