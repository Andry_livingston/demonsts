package com.orange5.nsts.ui.extensions

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.orange5.nsts.R
import com.orange5.nsts.util.view.CustomToast

fun Context?.hideKeyboard(view: View?) {
    if (this != null && view != null) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}

@JvmOverloads
fun Context?.toast(resId: Int?, duration: Int = Toast.LENGTH_SHORT) {
    if (this != null && resId != null)
        Toast.makeText(this, resId, duration).show()
}

@JvmOverloads
fun Context?.toast(textMessage: String?, duration: Int = Toast.LENGTH_SHORT) {
    if (this != null && textMessage != null)
        Toast.makeText(this, textMessage, duration).show()
}

fun Activity.errorToast(resId: Int, duration: Int = Toast.LENGTH_SHORT) = CustomToast.show(this, getString(resId))

fun Context.color(@ColorRes resId: Int) = ContextCompat.getColor(this, resId)

fun Context.drawable(@DrawableRes resId: Int) = ContextCompat.getDrawable(this, resId)

@JvmOverloads
fun Context.showDialog(title: String? = null,
                       message: String? = null,
                       positive: String? = null,
                       negative: String? = null,
                       neutral: String? = null,
                       positiveClick: () -> Unit = {},
                       negativeClick: () -> Unit = {},
                       neutralClick: () -> Unit = {},
                       cancelable: Boolean = true) =
        AlertDialog.Builder(this).apply {
            setCancelable(cancelable)
            if (title != null) setTitle(title)
            if (message != null) setMessage(message)
            if (positive != null) setPositiveButton(positive) { d, _ ->
                d.dismiss()
                positiveClick()
            }
            if (negative != null) setNegativeButton(negative) { d, _ ->
                d.dismiss()
                negativeClick()
            }
            if (neutral != null) setNeutralButton(neutral) { d, _ ->
                d.dismiss()
                neutralClick()
            }
            show()
        }


@JvmOverloads
fun Context.showErrorDialog(message: String,
                            title: String = getString(R.string.app_name),
                            positive: String = getString(R.string.common_ok),
                            positiveClick: () -> Unit = {}) = showDialog(
        title = title,
        message = message,
        positive = positive,
        positiveClick = positiveClick)

@JvmOverloads
fun Context.showErrorDialog(@StringRes message: Int, positiveClick: () -> Unit = {}) =
        showErrorDialog(message = getString(message), positiveClick = positiveClick)

@JvmOverloads
fun Context.showErrorDialog(@StringRes title: Int, @StringRes message: Int, positiveClick: () -> Unit = {}) =
        showErrorDialog(title = getString(title), message = getString(message), positiveClick = positiveClick)
