package com.orange5.nsts.ui.shipping.takeaway

import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.observe
import com.orange5.nsts.R
import com.orange5.nsts.scan.ScanningFragment
import com.orange5.nsts.ui.returns.signature.OnFinishListener
import com.orange5.nsts.ui.shipping.ShippingProcessAdapter
import com.orange5.nsts.ui.shipping.list.ShippingInitAdapter
import com.orange5.nsts.ui.shipping.repository.ReceivingNotice
import kotlinx.android.synthetic.main.fragment_refreshed_list.*

class TakeAwayInitFragment : ScanningFragment(), ShippingProcessAdapter.Listener {

    private var onFinishListener: OnFinishListener? = null
    private val viewModel: TakeAwayViewModel by activityViewModels { vmFactory }
    private val adapter = ShippingInitAdapter(this)
    override val notFoundMessage = R.string.shipping_notice_scanned_item_do_not_exist
    override val noDataMessage = R.string.receiving_bin_empty_data_message

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onFinishListener = (context as OnFinishListener)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.liveProgress.observe(this, ::showLoading)
        isRefreshEnabled = false
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.receivingItems.safeObserve(::submitList)
        recyclerView.adapter = adapter
    }

    override fun subscribe() {
        super.subscribe()
        viewModel.scannedItemNotFound.observe(::showErrorToast)
        viewModel.onConsumableScanned.observe { onConsumableProcessed(it, true) }
        viewModel.onRentalScanned.observe { onRentalProcessed(it, true) }
        scanningViewModel.barcodeValue.safeObserve(viewModel::onBarcodeScanned)
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as? TakeAwayActivity)?.setTitle(R.string.take_away_init_item_title)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) = inflater.inflate(R.menu.menu_scan_finish, menu)

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
            when (item.itemId) {
                R.id.finish -> {
                    onFinishListener?.onFinishProcessClick()
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    override fun onItemClick(item: ReceivingNotice) {
        if (item.isRental()) onRentalProcessed(item) else onConsumableProcessed(item)
    }

    override fun onDialogClosed() {
        scanningViewModel.setUpScanService()
        subscribe()
    }

    private fun onConsumableProcessed(item: ReceivingNotice, wasScanned: Boolean = false) {
        TakeAwayConsumableDialog.show(this, item.id, item.quantity.processed, wasScanned)
    }

    private fun onRentalProcessed(item: ReceivingNotice, wasScanned: Boolean = false) {
        clearSubscriptions()
        scanningViewModel.releaseScanner()
        TakeAwayRentalDialog.show(this, item.uniqueUnitNumber, wasScanned)
    }

    private fun submitList(items: List<ReceivingNotice>) {
        val filtered = items.filter { it.quantity.remaining != 0 }
        noData.isVisible = filtered.isEmpty()
        adapter.submitList(filtered)
    }
}