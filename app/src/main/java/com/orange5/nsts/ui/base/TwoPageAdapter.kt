package com.orange5.nsts.ui.base

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class TwoPageAdapter(private val map: Map<TabType, Fragment>,
                     fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment = map[TabType.values()[position]]
            ?: Fragment()

    override fun getCount(): Int = TabType.values().size

    enum class TabType(val id: Int) {
        INIT(id = 0),
        RESULT(id = 1)
    }
}