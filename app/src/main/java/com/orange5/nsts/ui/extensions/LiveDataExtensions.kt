package com.orange5.nsts.ui.extensions

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

fun <T> LiveData<T>.refresh() = (this as MutableLiveData).postValue(this.value)
