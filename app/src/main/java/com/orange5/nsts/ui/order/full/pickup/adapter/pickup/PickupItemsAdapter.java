package com.orange5.nsts.ui.order.full.pickup.adapter.pickup;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.data.db.entities.BinNumber;

import java.util.ArrayList;
import java.util.List;

public class PickupItemsAdapter extends RecyclerView.Adapter<PickupItemsAdapter.PickupItemViewHolder> {

    private List<OrderItem> items = new ArrayList<>();
    private final PickItemListener listener;
    private int currentScannedPosition;

    public PickupItemsAdapter(PickItemListener listener) {
        this.listener = listener;
    }

    public void setScannedBin(String productId, BinNumber bin) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getProductId().equals(productId)) {
                items.get(i).setScannedBin(bin);
                if (currentScannedPosition < items.size()) {
                    items.get(currentScannedPosition).setScannedBin(null);
                    notifyItemChanged(currentScannedPosition);
                }
                currentScannedPosition = i;
                notifyItemChanged(currentScannedPosition);
                break;
            }
        }
    }

    public boolean isInList(OrderItem item) {
        boolean contains = false;
        for (int i = 0; i < getItemCount(); i++) {
            if (items.get(i).getProductId().equals(item.getProductId())) {
                contains = true;
                break;
            }
        }
        return contains;
    }

    public void submitList(List<OrderItem> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PickupItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        PickUpItemView view = new PickUpItemView(parent.getContext());
        view.addPickupListener(listener);
        return new PickupItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PickupItemViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class PickupItemViewHolder extends RecyclerView.ViewHolder {


        PickupItemViewHolder(View itemView) {
            super(itemView);
        }

        public void bind(OrderItem item) {
            ((PickUpItemView) itemView).fill(item);
        }
    }

    public interface PickItemListener {
        void onItemClick(OrderItem item);

        void onAddItemsClick(OrderItem item);

        void onBackorderChecked(OrderItem item, boolean isChecked);

    }
}
