package com.orange5.nsts.ui.returns.full.inspect

import androidx.fragment.app.activityViewModels
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.ui.base.TwoPageAdapter
import com.orange5.nsts.ui.returns.RentalItemsFragment

class FullRentalItemsFragment : RentalItemsFragment() {

    override val viewModel: FullReturnViewModel by activityViewModels { vmFactory }

    override fun onItemScanned(item: OrderRentalItem?) {
        item?.let {
            val bins = viewModel.liveBins.value
            val inspectionBin = bins?.find { it.binId == FullRentalItemReturnDialog.INSPECTION_BIN }
            if (inspectionBin != null) {
                successVibration()
                viewModel.onItemReturned(ReturnInformation(
                        inspectionBin,
                        it.wasDamaged,
                        it.rentalReturnNotes ?: ""
                ))
            } else showScanFailToast(R.string.rental_return_error_inspection_bin)
        }
    }

    override val noDataMessage = R.string.full_rental_rented_is_empty_message

    override fun onItemClick(item: OrderRentalItem) {
        FullRentalItemReturnDialog.newInstance(item, TwoPageAdapter.TabType.INIT).show(requireFragmentManager(),
                FullRentalItemReturnDialog::class.java.simpleName)
    }

}
