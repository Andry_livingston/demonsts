package com.orange5.nsts.ui.returns

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.*
import com.orange5.nsts.service.bin.BinService
import com.orange5.nsts.service.order.items.OrderRentalItemService
import com.orange5.nsts.service.rental.RentalStatus
import com.orange5.nsts.ui.base.ActionLiveData
import com.orange5.nsts.ui.base.BaseViewModel
import com.orange5.nsts.ui.base.TwoPageAdapter
import com.orange5.nsts.ui.returns.quick.ReturnInformation
import com.orange5.nsts.util.format.DateFormatter
import io.reactivex.schedulers.Schedulers

abstract class ReturnViewModel(private val orderRentalItemService: OrderRentalItemService,
                               private val binService: BinService) : BaseViewModel() {

    protected var customer: Customer? = null
    val orderItem = MutableLiveData<OrderRentalItem>()
    val rentalReturn: RentalReturn = RentalReturn().apply { startDate = DateFormatter.currentTimeDb() }

    protected val mappedRentals = MutableLiveData<MutableMap<OrderRentalItem, RentalProductItem>>()
    protected val mappedReturns = MutableLiveData<MutableMap<OrderRentalItem, RentalProductItem>>()

    val allRentalItems: LiveData<List<OrderRentalItem>> = mappedRentals.map { map -> (map.keys.toList()).sortedBy { it.uniqueUnitNumber } }
    val allReturnItems: LiveData<List<OrderRentalItem>> = mappedReturns.map { map -> (map.keys.toList()).sortedBy { it.uniqueUnitNumber } }
    val scannedItem = MutableLiveData<OrderRentalItem>()
    val scannedItemNotFound = MutableLiveData<Int>()

    val itemCount = MutableLiveData<Int>()
    val badgeCount = MutableLiveData<Int>()
    val title = MutableLiveData<String>()
    val searchItems = MutableLiveData<List<OrderRentalItem>>()
    val liveBins = MutableLiveData<List<BinNumber>>()
    val onSuccessAdded = ActionLiveData<String>()
    val returnFinished = MutableLiveData<Customer>()
    val binPosition = MutableLiveData<Int>()

    abstract fun createReturn(signature: ByteArray)
    protected open fun loadRentalReturnItem(items: List<OrderRentalItem>, showAll: Boolean): MutableMap<OrderRentalItem, RentalProductItem> = mutableMapOf()

    open fun handleTabSelection(tabType: TwoPageAdapter.TabType) = setCount(
            when (tabType) {
                TwoPageAdapter.TabType.INIT -> allRentalItems.value?.size
                TwoPageAdapter.TabType.RESULT -> {
                    setBadge(allReturnItems.value?.size ?: 0)
                    allReturnItems.value?.size
                }
            } ?: 0)

    fun setCount(count: Int) {
        itemCount.value = count
    }

    fun setBadge(count: Int) {
        badgeCount.value = count
    }

    fun loadOrderItem(id: String, type: TwoPageAdapter.TabType = TwoPageAdapter.TabType.INIT) {
        orderItem.value =
                if (type == TwoPageAdapter.TabType.INIT) mappedRentals.value?.keys?.find { it.orderItemId == id }
                else mappedReturns.value?.keys?.find { it.orderItemId == id }
    }

    open fun onItemReturned(info: ReturnInformation) {
        val item = orderItem.value
        if (item != null) {
            val product = mappedRentals.value?.get(item)
            product?.let {
                it.status = info.status.id
                if (info.returnBin != null) {
                    it.containerBin = info.returnBin
                    item.scannedBin = info.returnBin
                }

                item.returnCustomerId = item.order.creatorCustomerId
                item.rentalReturnNotes = info.comments
                insertReturn(item, it)
                deleteRental(item)
                onSuccessAdded.value = item.uniqueUnitNumber
            }
        }
    }

    open fun onRemoveItemFromReturned(item: OrderRentalItem) {
        val product = mappedReturns.value?.get(item)
        product?.let {
            it.status = RentalStatus.CheckedOut.id
            it.containerBin = null

            item.rentalReturnId = null

            insertRental(item, it)
            deleteReturn(item)
        }
    }

    fun onUpdateReturnedItem(info: ReturnInformation) {
        val item = orderItem.value
        if (item != null) {
            val product = mappedReturns.value?.get(item)
            product?.let {
                it.status = info.status.id
                if (info.returnBin != null) {
                    it.containerBin = info.returnBin

                    item.scannedBin = info.returnBin
                    item.wasDamaged = info.returnBin.binNum == BinService.REPAIR
                }

                item.rentalReturnNotes = info.comments
                updateReturn(item, it)
            }
        }
    }

    fun performSearch(query: String) {
        allRentalItems.value?.let { rentals ->
            searchItems.value = rentals.filter {
                it.uniqueUnitNumber.contains(query, true) ||
                        it.productNumber.contains(query, true) ||
                        it.rentalProduct.productDescription.contains(query, true)
            }
        }
    }

    fun fetchReturnItems(showAll: Boolean = false) {
        liveProgress.postValue(true)
        addDisposable(rxSingleCreate { orderRentalItemService.loadByStatus(RentalStatus.CheckedOut) }
                .subscribeOn(Schedulers.io())
                .map { loadRentalReturnItem(it, showAll) }
                .doFinally {liveProgress.postValue(false)}
                .subscribe({ mappedRentals.postValue(it) }, ::onError))
    }

    fun getDefaultBin(item: OrderRentalItem): BinNumber? =
            item.scannedBin ?: if (liveBins.value == null) null else liveBins.value!![0]


    fun scanByOrderItemId(item: RentalProductItem?) {
        if (item != null) {
            val orderItem = allRentalItems.value?.find {
                it.productId == item.productId && it.uniqueUnitNumber == item.uniqueUnitNumber
            }
            if (orderItem != null) {
                this.orderItem.value = orderItem
                scannedItem.value = orderItem
                this.orderItem.value = orderItem
            } else onScanFails()
        } else onScanFails()
    }

    fun scanByProductId(product: RentalProduct?) {
        if (product != null) {
            val item = allRentalItems.value?.find { it.productId == product.productId }
            if (item != null) {
                scannedItem.value = item
                this.orderItem.value = item
            } else onScanFails()
        } else onScanFails()
    }

    fun setTitle(string: String) {
        title.value = string
    }

    fun searchScannedBin(binId: String) {
        val bins = liveBins.value ?: return
        val position = bins.indexOfFirst { it.binId == binId }
        if (position != -1) {
            binPosition.value = position
        }
    }

    fun searchBin(query: String?, initialPosition: Int) {
        var foundPosition = initialPosition
        val bins = liveBins.value ?: return
        if (query != null && query.isNotEmpty()) {
            val position = bins.indexOfFirst { it.binNum.contains(query, ignoreCase = true) }
            if (position != -1) {
                foundPosition = position
            }
        }
        binPosition.value = foundPosition
    }

    protected open fun runDbTransaction(action: () -> Unit) {
        addDisposable(rxCompletableCreate { action() }
                .subscribeOn(Schedulers.computation())
                .subscribe({
                    liveRunSync.postValue(null)
                    returnFinished.postValue(null)
                }, ::onError))
    }

    protected fun loadBins() {
        addDisposable(rxSingleCreate { binService.loadAll() }
                .subscribeOn(Schedulers.computation())
                .subscribe({ liveBins.postValue(it) }, ::onError))
    }

    protected fun deleteRental(item: OrderRentalItem) {
        val map = mappedRentals.value
        if (!map.isNullOrEmpty()) {
            map.remove(item)
        }
        mappedRentals.value = map
    }

    protected fun insertRental(item: OrderRentalItem, product: RentalProductItem) {
        val map = mappedRentals.value ?: mutableMapOf()
        map[item] = product
        mappedRentals.value = map
    }

    protected fun insertReturn(item: OrderRentalItem, product: RentalProductItem) {
        val map = mappedReturns.value ?: mutableMapOf()
        map[item] = product
        mappedReturns.value = map
    }

    protected fun updateReturn(item: OrderRentalItem, product: RentalProductItem) {
        val map = mappedReturns.value ?: mutableMapOf()
        map.remove(item)
        map[item] = product
        mappedReturns.value = map
    }

    protected fun deleteReturn(item: OrderRentalItem) {
        val map = mappedReturns.value
        if (!map.isNullOrEmpty()) {
            map.remove(item)
        }
        mappedReturns.value = map
    }

    protected open fun onScanFails() {
        scannedItemNotFound.value = R.string.rental_return_activity_message_item_not_found
    }

}