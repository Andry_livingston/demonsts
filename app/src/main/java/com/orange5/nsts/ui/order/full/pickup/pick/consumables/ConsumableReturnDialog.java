package com.orange5.nsts.ui.order.full.pickup.pick.consumables;

import android.app.AlertDialog;
import android.content.Context;
import android.text.SpannableString;
import android.view.View;
import android.widget.Button;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.entities.BinNumber;
import com.orange5.nsts.data.db.entities.OrderConsumableItem;
import com.orange5.nsts.service.bin.BinService;
import com.orange5.nsts.util.CollectionUtils;
import com.orange5.nsts.util.Colors;

import java.util.List;
import java.util.function.BiConsumer;

import com.orange5.nsts.util.Spannable;
import com.orange5.nsts.util.view.picker.PickerView;
import com.orange5.nsts.util.view.picker.ValuePickerAdapter;

public class ConsumableReturnDialog {
    private final Context context;
    private BiConsumer<BinNumber, Integer> resultConsumer;
    private BinNumber selectedBin;
    private Integer selectedQuantity = 1;

    private PickerView picker2;

    private Button pickButton;

    public static ConsumableReturnDialog with(Context context) {
        return new ConsumableReturnDialog(context);
    }

    private ConsumableReturnDialog(Context context) {
        this.context = context;
    }

    public ConsumableReturnDialog resultConsumer(BiConsumer<BinNumber, Integer> resultConsumer) {
        this.resultConsumer = resultConsumer;
        return this;
    }

    public void showForItem(OrderConsumableItem orderItem) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.return_consumable_title, orderItem.getProductNumber()));
        View contentView = View.inflate(context, R.layout.dialog_consumable_return, null);
        builder.setView(contentView);
        PickerView picker1 = contentView.findViewById(R.id.picker1);
        picker2 = contentView.findViewById(R.id.picker2);
        pickButton = contentView.findViewById(R.id.actionButton);

        BinNumber defaultBin = BinService.getById(orderItem.asConsumable().getConsumableProduct().getDefaultBinId());

        List<BinNumber> bins = BinService.getBinsForReturn();
        if (CollectionUtils.isEmpty(bins)) {
            return;
        }
        int selectedPosition = 0;

        if (defaultBin != null) {
            for (int i = 0; i < bins.size(); i++) {
                BinNumber bin = bins.get(i);
                if (bin.getBinId() != null && bin.getBinId().equals(defaultBin.getBinId())) {
                        selectedPosition = i;
                }
            }
        }

        updateQuantities(orderItem.getPicked());

        new ValuePickerAdapter<>(bins)
                .selectedItemConsumer(item -> {
                    selectedBin = item;
                    updateButton();
                })
                .attachTo(picker1);

        picker1.setSelectedItemPosition(selectedPosition);
        selectedBin = bins.get(selectedPosition);
        updateButton();
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        pickButton.setOnClickListener(v -> {
            resultConsumer.accept(selectedBin, selectedQuantity);
            alertDialog.dismiss();
        });
    }

    private void updateQuantities(int quantity) {
        List<Integer> quantities = CollectionUtils.rangeClosedList(1, quantity);
        new ValuePickerAdapter<>(quantities)
                .selectedItemConsumer(selected -> {
                    selectedQuantity = selected;
                    updateButton();
                })
                .attachTo(picker2);
    }

    private void updateButton() {
        String quantityString = selectedQuantity.toString();
        String text = context.getResources().getQuantityString(R.plurals.return_n_items_to_bin, selectedQuantity, selectedQuantity, selectedBin.toString());
        SpannableString spannableString = Spannable
                .highlightTextColor(text, Colors.HIGHLIGHT_BLUE, quantityString, selectedBin.toString());
        pickButton.setText(spannableString);
    }
}
