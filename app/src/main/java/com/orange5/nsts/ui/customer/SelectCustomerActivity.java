package com.orange5.nsts.ui.customer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.entities.AccountingCode;
import com.orange5.nsts.data.db.entities.Customer;
import com.orange5.nsts.data.db.entities.Order;
import com.orange5.nsts.scan.ScanningActivity;
import com.orange5.nsts.ui.base.ActivityType;
import com.orange5.nsts.ui.order.full.select.OrderItemsActivity;
import com.orange5.nsts.ui.order.quick.QuickOrderItemsActivity;
import com.orange5.nsts.ui.returns.full.inspect.FullReturnActivity;
import com.orange5.nsts.util.adapters.spinner.SearchableSpinner;
import com.orange5.nsts.util.view.LoadingButton;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import butterknife.BindView;

import static android.view.View.GONE;
import static com.orange5.nsts.ui.order.full.pickup.PickUpActivity.ORDER_ID;

public class SelectCustomerActivity extends ScanningActivity {

    private static final String ACTIVITY_TYPE = "order_type";

    @BindView(R.id.accountingCode)
    TextView accountingCodeSpinner;
    @BindView(R.id.webOrderCode)
    EditText webOrderNumber;
    @BindView(R.id.customer)
    TextView customerSpinner;
    @BindView(R.id.btnAddItems)
    LoadingButton addItems;

    private SelectCustomerViewModel viewModel;

    private Customer selectedCustomer;
    private ActivityType type;
    private AccountingCode selectedAccountingCode;

    @NotNull
    public static Intent newIntent(Context context, ActivityType type) {
        return new Intent(context, SelectCustomerActivity.class).putExtra(ACTIVITY_TYPE, type);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = getVMFromActivity(SelectCustomerViewModel.class);
        viewModel.liveRunSync.observe(this, Void -> runSync());
        viewModel.liveOrderCreated.observe(this, this::startOrderItemActivity);
        viewModel.liveLoadAccountingCode.observe(this, this::initAccountingCodeAdapter);
        viewModel.liveLoadCustomers.observe(this, this::initCustomerAdapter);
        viewModel.liveValidationError.observe(this, this::errorSnack);
        viewModel.liveProgress.observe(this, this::showLoading);
        viewModel.loadCustomers();
        viewModel.loadAccountingCode();
        getScanningViewModel().liveCustomerScanned.observe(this, this::onCustomerScanned);
        type = (ActivityType) getIntent().getSerializableExtra(ACTIVITY_TYPE);
        int title = R.string.common_new_order;
        if (type == ActivityType.FULL_RETURN) {
            title = R.string.rental_return_activity_full;
            hideAccountWebCode();
        } else if (viewModel.isAccountingCodeRequired()) {
            accountingCodeSpinner.setCompoundDrawablesWithIntrinsicBounds(R.drawable.asterisk_mandatory_shifted, 0, 0, 0);
        }
        setTitle(title);
        addItems.setOnClickListener(this::handleCreateActionByType);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_new_order;
    }

    @Override
    public void onBackPressed() {
        super.finish();
    }

    @Override
    public int notFoundMessage() {
        return R.string.selected_customer_activity_scan_customer_number;
    }

    private void hideAccountWebCode() {
        accountingCodeSpinner.setVisibility(GONE);
        webOrderNumber.setVisibility(GONE);
    }

    private void initAccountingCodeAdapter(List<AccountingCode> data) {
        SearchableSpinner.createForTextView(accountingCodeSpinner,
                data,
                () -> selectedAccountingCode,
                accountingCode -> selectedAccountingCode = accountingCode);
    }

    private void initCustomerAdapter(List<Customer> data) {
        SearchableSpinner.createForTextView(customerSpinner,
                data,
                () -> selectedCustomer,
                customer -> selectedCustomer = customer);
    }

    public void onCustomerScanned(Customer customer) {
        selectedCustomer = customer;
        handleCreateActionByType();
    }

    private void showLoading(boolean isLoading) {
        addItems.setLoading(isLoading);
    }


    private void handleCreateActionByType() {
        switch (type) {
            case FULL_ORDER:
            case QUICK_ORDER:
                createOrder();
                break;
            case FULL_RETURN:
                createReturn();
                break;
            default:
                break;
        }
    }

    private void createReturn() {
        if (selectedCustomer == null) {
            errorSnack(R.string.selected_customer_activity_message_fill_all_mandatory_fields);
        } else {
            startActivity(FullReturnActivity.newIntent(this, selectedCustomer.getCustomerId()));
            finish();
        }
    }

    private void createOrder() {
        showLoading(true);
        viewModel.createNewOrder(selectedAccountingCode,
                webOrderNumber.getText().toString(),
                selectedCustomer);
    }

    private void startOrderItemActivity(Order order) {
        prepareActivity(type == ActivityType.FULL_ORDER
                ? OrderItemsActivity.class
                : QuickOrderItemsActivity.class)
                .add(ORDER_ID, order.getOrderId())
                .start();
        finish();
    }
}