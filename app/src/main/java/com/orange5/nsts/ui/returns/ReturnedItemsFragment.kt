package com.orange5.nsts.ui.returns

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.core.view.isVisible
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.ui.base.BaseListFragment
import com.orange5.nsts.ui.returns.list.ReturnAdapter
import com.orange5.nsts.ui.returns.signature.OnFinishListener
import kotlinx.android.synthetic.main.fragment_refreshed_list.*

abstract class ReturnedItemsFragment : BaseListFragment(), ReturnAdapter.Listener {

    private var onFinishReturnListener: OnFinishListener? = null
    protected abstract val viewModel: ReturnViewModel

    private val adapter = ReturnAdapter(this)

    override val noDataMessage = R.string.rental_return_is_empty_message

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onFinishReturnListener = (context as OnFinishListener)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isRefreshEnabled = false
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.allReturnItems.observe(::updateRentedList)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.adapter = adapter
        showOrHideEmptyListMessage(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) = inflater.inflate(R
            .menu.menu_finish, menu)

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
            when (item.itemId) {
                R.id.finish -> {
                    onFinishReturnListener?.onFinishProcessClick()
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    protected open fun updateRentedList(items: List<OrderRentalItem>) {
        if (userVisibleHint) {
            viewModel.setCount(items.size)
            viewModel.setBadge(items.size)
        }
        showOrHideEmptyListMessage(items.isEmpty())
        adapter.submitList(items)
    }

    private fun showOrHideEmptyListMessage(showOrHide: Boolean) {
        noData.isVisible = showOrHide
    }

    private fun showConfirmDeleteDialog(item: OrderRentalItem) {
        AlertDialog.Builder(context)
                .setTitle(getString(R.string.common_remove_rental_n, item.productNumber))
                .setPositiveButton(R.string.common_remove) { d, _ ->
                    viewModel.onRemoveItemFromReturned(item)
                    d.dismiss()
                }
                .setNegativeButton(R.string.common_cancel) { d, _ -> d.dismiss() }
                .show()
    }

    override fun onItemDelete(item: OrderRentalItem) {
        showConfirmDeleteDialog(item)
    }
}
