package com.orange5.nsts.ui.returns.signature

import android.graphics.Bitmap
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import android.view.View
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseDialogFragment
import kotlinx.android.synthetic.main.dialog_finish_return.*
import kotlinx.android.synthetic.main.view_signature.*

class FinishReturnDialog : BaseDialogFragment() {

    private var listener: Listener? = null

    override fun getLayoutResourceId() = R.layout.dialog_finish_return

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val returnNum = arguments?.getString(RETURN_NUM_KEY) ?: ""
        title.text = getString(R.string.finish_return_dialog_rental_return_n, returnNum)
        cancel.setOnClickListener { dismiss() }
        clearSignature.setOnClickListener { signaturePad.clear() }
        finish.setOnClickListener {
            listener?.onGetSignature(signaturePad.signatureBitmap)
            dismiss()
        }
    }

    fun setOnGetSignatureListener(listener: Listener) {
        this.listener = listener
    }

    companion object {
        @JvmStatic
        fun show(returnNumber: String, fm: FragmentManager) = FinishReturnDialog().apply {
            arguments = Bundle().apply {
                putString(RETURN_NUM_KEY, returnNumber)
            }
            show(fm, this::class.java.simpleName)
        }

        private const val RETURN_NUM_KEY = "return_number_key"
    }

    interface Listener {
        fun onGetSignature(signature: Bitmap)
    }

}
