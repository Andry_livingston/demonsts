package com.orange5.nsts.ui.shipping.takeaway

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.ThreePageAdapter
import com.orange5.nsts.ui.base.ThreePageAdapter.TabType.INIT
import com.orange5.nsts.ui.base.ThreePageAdapter.TabType.INTERMEDIATE
import com.orange5.nsts.ui.base.ThreePageAdapter.TabType.RESULT
import com.orange5.nsts.ui.shipping.ShippingProcessActivity
import kotlinx.android.synthetic.main.activity_return.*

class TakeAwayActivity : ShippingProcessActivity() {

    override val viewModel by viewModels<TakeAwayViewModel> { vmFactory }

    override val adapter: ThreePageAdapter by lazy {
        val init: Fragment = TakeAwayInitFragment()
        val intermediate: Fragment = TakeAwayDiscrepancyFragment()
        val result: Fragment = TakeAwayResultFragment()
        ThreePageAdapter(mapOf(INIT to init, INTERMEDIATE to intermediate, RESULT to result), supportFragmentManager)
    }

    override fun setBottomNavItemsTitle() {
        bottomNavigation.menu.findItem(R.id.initItem)?.title = getString(R.string.take_away_init_item_title)
        bottomNavigation.menu.findItem(R.id.intermediateItem)?.title = getString(R.string.discrepancy)
        bottomNavigation.menu.findItem(R.id.resultItem)?.title = getString(R.string.take_away_result_item_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.loadItems()
        viewModel.loadBins()
        viewModel.onFinishReceivingNotice.observe(this) { onFinishReceiving() }
    }

    override fun nothingToProcessMessage() = R.string.take_away_nothing_to_process

    companion object {
        @JvmStatic
        fun intent(context: Context) = Intent(context, TakeAwayActivity::class.java)
    }
}