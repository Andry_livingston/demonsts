package com.orange5.nsts.ui.order.full.pickup.ship;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.orange5.nsts.R;
import com.orange5.nsts.data.db.entities.OrderConsumableItem;
import com.orange5.nsts.data.db.entities.OrderRentalItem;
import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.ui.extensions.OrderExtensionsKt;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ShipOutItemView extends FrameLayout {

    @BindView(R.id.productDescription)
    TextView itemName;
    @BindView(R.id.productNumber)
    TextView itemNumber;
    @BindView(R.id.itemType)
    ImageView itemType;
    @BindView(R.id.itemPrice)
    TextView itemPrice;
    @BindView(R.id.sectionHeader)
    TextView sectionHeader;
    @BindView(R.id.pickedItemsCount)
    TextView itemQuantity;

    public ShipOutItemView(@NonNull Context context) {
        this(context, null);
    }

    public ShipOutItemView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ShipOutItemView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ShipOutItemView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        inflate(context, R.layout.order_ship_out_item, this);
        ButterKnife.bind(this, this);
    }

    public void fill(OrderItem item) {
        fill(item, "");
    }

    public void fill(OrderItem item, String highlight) {
        ShipOutViewHolder viewModel = new ShipOutViewHolder(item, getContext());
        itemName.setText(viewModel.getProductDescription(highlight));
        itemNumber.setText(viewModel.getProductNumber(highlight));
        itemType.setBackgroundResource(OrderExtensionsKt.orderIcon(item));
        itemPrice.setText(viewModel.getFormattedPrice());

        if (item instanceof OrderConsumableItem) {
            itemQuantity.setVisibility(VISIBLE);
            itemQuantity.setText(viewModel.getPickedItemsCount());
            return;
        }
        OrderRentalItem rentalItem = (OrderRentalItem) item;
        itemQuantity.setTextColor(viewModel.getGreenColor());
        String uniqueUnitNumber = getContext().getString(R.string.common_uun_nr, rentalItem.getUniqueUnitNumber());
        itemQuantity.setText(uniqueUnitNumber);
    }

    public void showSectionHeader(String sectionTitle) {
        sectionHeader.setText(sectionTitle);
        sectionHeader.setVisibility(VISIBLE);
    }

    public void hideSectionHeader() {
        sectionHeader.setVisibility(GONE);
    }

}