package com.orange5.nsts.ui.shipping.receiving

import androidx.fragment.app.activityViewModels
import com.orange5.nsts.ui.order.search.ProductWrapper
import com.orange5.nsts.ui.shipping.discrepancy.DiscrepancyFragment
import com.orange5.nsts.ui.shipping.discrepancy.add.AddDiscrepancyDetailsDialog
import com.orange5.nsts.ui.shipping.discrepancy.add.AddDiscrepancyDialog
import com.orange5.nsts.ui.shipping.discrepancy.add.AddExistingExtrasDetailsDialog
import com.orange5.nsts.ui.shipping.discrepancy.add.AddUnknownExtrasDialog
import com.orange5.nsts.ui.shipping.discrepancy.add.EditDiscrepancyDetailsDialog
import com.orange5.nsts.ui.shipping.discrepancy.add.EditUnknownExtrasDialog
import com.orange5.nsts.ui.shipping.discrepancy.repository.Discrepancy
import com.orange5.nsts.ui.shipping.discrepancy.repository.Discrepancy.Type.NON_EXISTING_PRODUCT
import com.orange5.nsts.ui.shipping.repository.ReceivingNotice

class ReceivingDiscrepancyFragment : DiscrepancyFragment() {
    override val viewModel by activityViewModels<ReceivingViewModel> { vmFactory }

    override fun onTopButtonClick() {
        AddDiscrepancyDialog.fromReceiving(this, Discrepancy.Type.SHIPMENT_SHORTAGE)
    }

    override fun onItemClick(item: Discrepancy) {
        if (item.type == NON_EXISTING_PRODUCT) EditUnknownExtrasDialog.fromReceiving(parentFragmentManager, item.id)
        else EditDiscrepancyDetailsDialog.fromReceiving(parentFragmentManager, item.id)
    }

    override fun onAddDiscrepancyClick(item: ReceivingNotice, type: Discrepancy.Type) {
        AddDiscrepancyDetailsDialog.fromReceiving(parentFragmentManager, item.id, type)
    }

    override fun showExistingExtrasDetailsDialog(product: ProductWrapper) {
        val discrepancy = Discrepancy.from(product.product, viewModel.transferNo)
        AddExistingExtrasDetailsDialog.fromReceiving(parentFragmentManager, discrepancy)
    }

    override fun onBottomButtonClick() {
        AddUnknownExtrasDialog.fromReceiving(parentFragmentManager)
    }
}