package com.orange5.nsts.ui.shipping.discrepancy.widget

import android.content.Context
import android.text.SpannableStringBuilder
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.widget.SearchView
import androidx.core.text.bold
import androidx.core.text.color
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.BinNumber
import com.orange5.nsts.ui.base.picker.BinPickerAdapter
import com.orange5.nsts.ui.base.picker.Item
import com.orange5.nsts.ui.extensions.color
import kotlinx.android.synthetic.main.view_number_picker.view.*

class BinPickerView @JvmOverloads constructor(context: Context, attrsSet: AttributeSet? = null,
                                              defStyleAttr: Int = 0, defStyleRes: Int = 0)
    : LinearLayout(context, attrsSet, defStyleAttr, defStyleRes), SearchView.OnQueryTextListener {

    private val adapter = BinPickerAdapter()
    private var selectedBin: BinNumber? = null
    private var binPosition: Int = 0
    private var searchBinListener: ((String?, Int) -> Unit)? = null

    init {
        View.inflate(context, R.layout.view_bin_picker, this)
        picker.setAdapter(adapter)
        adapter.action = ::onBinSelected
    }

    override fun onQueryTextSubmit(query: String?): Boolean = false

    override fun onQueryTextChange(newText: String?): Boolean {
        searchBinListener?.invoke(newText, binPosition)
        return false
    }

     fun submitList(bins: List<BinNumber>) {
        adapter.submitItems(bins)

        picker.selectedItemPosition = 0
        updateActionButtonText()
    }

    fun selectedPosition(position: Int) {
        picker.selectedItemPosition = position
    }

    fun setOnActionClick(action: (BinNumber?) -> Unit) {
        actionButton.setOnClickListener {
            action(selectedBin)
        }
    }

    fun setOnQueryListener(listener: (String?, Int) -> Unit) {
        searchBinListener = listener
    }

    private fun onBinSelected(item: Item<BinNumber>) {
        selectedBin = item.value
        updateActionButtonText()
    }


    private fun updateActionButtonText() {
        val text = SpannableStringBuilder(resources.getString(R.string.take_away_action_put))
                .append(SPACE)
                .append(resources.getString(R.string.take_away_action_in))
                .append(SPACE)
                .bold { color(context.color(R.color.blue)) { append(selectedBin?.binNum ?: "") } }
        actionButton.text = text
    }

    companion object {
        const val SPACE = " "
    }
}