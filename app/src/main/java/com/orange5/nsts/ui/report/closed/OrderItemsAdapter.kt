package com.orange5.nsts.ui.report.closed

import android.text.SpannableString
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.orange5.nsts.R
import com.orange5.nsts.data.db.base.OrderItem
import com.orange5.nsts.data.db.entities.OrderConsumableItem
import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.ui.extensions.orderIcon
import com.orange5.nsts.util.Spannable
import com.orange5.nsts.util.format.CurrencyFormatter
import kotlinx.android.synthetic.main.item_finished_order.view.*

class OrderItemsAdapter : RecyclerView.Adapter<OrderItemsAdapter.ReportVH>() {

    private var items = mutableListOf<OrderItem>()

    override fun getItemCount() = items.size

    fun submitList(items: List<OrderItem>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportVH =
            ReportVH(LayoutInflater.from(parent.context).inflate(R.layout.item_finished_order,
                    parent, false))

    override fun onBindViewHolder(holder: ReportVH, position: Int)
            = holder.bind(items[position])

    inner class ReportVH(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: OrderItem) {
            with(itemView) {
                productDescription.text = getProduct(item)
                productNumber.text = item.productNumber
                itemPrice.text = CurrencyFormatter.formatPrice(item.price)
                itemType.setBackgroundResource(item.orderIcon())

                if (item is OrderConsumableItem) {
                    pickedItemsCount.visibility = View.VISIBLE
                    pickedItemsCount.text = (getPickedItemsCount(item))
                    return
                }
                pickedItemsCount.text = getUUNText(item)
            }
        }

        private fun getProduct(item: OrderItem): String? {
            if (item is OrderConsumableItem) {
                val currentItem = item as? OrderConsumableItem
                return currentItem?.consumableProduct?.productDescription
            }

            if (item is OrderRentalItem) {
                val currentItem = item as? OrderRentalItem
                return currentItem?.rentalProduct?.productDescription
            }
            throw IllegalStateException()
        }

        private fun getType(item: OrderItem): Int {
            return if (item is OrderConsumableItem) R.string.common_consumable else R.string.common_rental
        }

        private fun getCountColor(item: OrderItem): Int {
            var color: Int = R.color.black
            if (item is OrderConsumableItem) {
                val consumableItem = item as? OrderConsumableItem
                color = if (consumableItem?.allPicked()!!) R.color.okGreen else R.color.errorRed
            }
            return color
        }

        private fun getPickedItemsCount(item: OrderItem): SpannableString? {
            val pickedCountFormatString = itemView.resources
                    .getString(R.string.common_picked_d_d)

            val result:String
            if (item is OrderConsumableItem) {
                val consumableItem = item as? OrderConsumableItem
                var picked = consumableItem?.picked
                if (consumableItem?.isOversell!! && consumableItem.oversellAllowed()) {
                    picked = picked?.plus(consumableItem.oversell)
                }
                 result = String.format(pickedCountFormatString, picked, consumableItem.quantity)
            } else {
                val orderRentalItem = item as OrderRentalItem
                val pickedCount = orderRentalItem.picked!!.toLong()
                result = String.format(pickedCountFormatString, pickedCount, orderRentalItem.quantity)
            }

            return Spannable.highlightTextColor(result,
                    ContextCompat.getColor(itemView.context, getCountColor(item)), result)
        }

        private fun getUUNText(item: OrderItem): SpannableString {
            val uniqueUnitNumber = item.asRental().uniqueUnitNumber

            val color = if (uniqueUnitNumber == null) R.color.errorRed else R.color.okGreen
            val result = if (uniqueUnitNumber == null)
                itemView.resources.getString(R.string.report_closed_order_fragment_unpicked) else
                itemView.resources.getString(R.string.common_uun_nr, uniqueUnitNumber)

            return Spannable.highlightTextColor(result,
                    ContextCompat.getColor(itemView.context,color), result)
        }
    }

}