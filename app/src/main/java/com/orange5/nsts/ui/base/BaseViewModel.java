package com.orange5.nsts.ui.base;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.orange5.nsts.scan.ScanVibrator;
import com.orange5.scanner.VibratorHandler;
import com.orange5.scanner.VibratorHandler.Type;

import java.util.concurrent.Callable;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public abstract class BaseViewModel extends ViewModel {

    public ActionLiveData<String> liveError = new ActionLiveData<>();
    public MutableLiveData<Boolean> liveProgress = new MutableLiveData<>();
    public MutableLiveData<Void> liveRunSync = new MutableLiveData<>();
    public ActionLiveData<Type> vibration = new ActionLiveData<>();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    protected void addDisposable(Disposable disposable) {
        compositeDisposable.add(disposable);
    }

    protected void clearDisposable() {
        compositeDisposable.clear();
    }

    protected void runJobComputation(Action job, Action onSuccess) {
        addDisposable(rxCompletableCreate(job)
                .subscribeOn(Schedulers.computation())
                .doFinally(() -> liveProgress.postValue(false))
                .subscribe(onSuccess, this::onError));
    }

    protected void runJobIO(Action job, Action onSuccess) {
        addDisposable(rxCompletableCreate(job)
                .subscribeOn(Schedulers.io())
                .doFinally(() -> liveProgress.postValue(false))
                .subscribe(onSuccess, this::onError));
    }

    protected void runJobIO(Action job, Action onSuccess, Consumer<Throwable> onError) {
        addDisposable(rxCompletableCreate(job)
                .subscribeOn(Schedulers.io())
                .doFinally(() -> liveProgress.postValue(false))
                .subscribe(onSuccess, onError));
    }

    protected <T> void callJobComputation(Callable<T> call, Consumer<T> onSuccess) {
        addDisposable(rxSingleCreate(call)
                .subscribeOn(Schedulers.computation())
                .doFinally(() -> liveProgress.postValue(false))
                .subscribe(onSuccess, this::onError));
    }

    protected <T> void callJobComputation(Callable<T> call, Consumer<T> onSuccess, Runnable onComplete) {
        addDisposable(rxSingleCreate(call)
                .subscribeOn(Schedulers.computation())
                .doFinally(onComplete::run)
                .subscribe(onSuccess, this::onError));
    }

    protected <T> void callJobIo(Callable<T> call, Consumer<T> onSuccess) {
        addDisposable(rxSingleCreate(call)
                .subscribeOn(Schedulers.io())
                .doFinally(() -> liveProgress.postValue(false))
                .subscribe(onSuccess, this::onError));
    }

    protected <T> void callJobIo(Callable<T> call, Consumer<T> onSuccess, Consumer<Throwable> onError) {
        addDisposable(rxSingleCreate(call)
                .subscribeOn(Schedulers.io())
                .doFinally(() -> liveProgress.postValue(false))
                .subscribe(onSuccess, onError));
    }

    protected <T> Single<T> rxSingleCreate(Callable<T> call) {
        return Single.create(emitter -> {
            try {
                T result = call.call();
                if (!emitter.isDisposed()) emitter.onSuccess(result);
            } catch (Throwable t) {
                if (!emitter.isDisposed()) emitter.onError(t);
            }
        });
    }

    protected Completable rxCompletableCreate(Action action) {
        return Completable.create(emitter -> {
            try {
                action.run();
                if (!emitter.isDisposed()) emitter.onComplete();
            } catch (Throwable t) {
                if (!emitter.isDisposed()) emitter.onError(t);
            }
        });
    }

    protected void onError(Throwable throwable) {
        Timber.e(throwable);
        liveProgress.postValue(false);
        liveError.postValue(throwable.getMessage());
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }
}
