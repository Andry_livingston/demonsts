package com.orange5.nsts.ui.load

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.TypedValue
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.orange5.nsts.R
import com.orange5.nsts.sync.InitLoadQueries
import com.orange5.nsts.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_init_load.*

class InitLoadActivity : BaseActivity() {

    private val viewModel: InitLoadViewModel by viewModels { vmFactory }

    override fun getLayoutResourceId() = R.layout.activity_init_load

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.entityLoaded.observe(this, Observer(::showCurrentLoadedEntity))
        viewModel.progress.observe(this, Observer(::showLoading))
        viewModel.report.observe(this, Observer(::showReport))
        report.movementMethod = ScrollingMovementMethod()
        title = getString(R.string.init_load_loading_activity_title)
        viewModel.startLoading()
        loadingButton.setOnClickListener {
            clearProgress()
            when (loadingButton.text) {
                getString(R.string.init_load_cancel_loading) -> {
                    viewModel.cancelLoading()
                    loadingButton.text = getString(R.string.settings_run_init_sync)
                }
                getString(R.string.init_load_cancel_done) -> onBackPressed()
                else -> {
                    viewModel.startLoading()
                    loadingButton.text = getString(R.string.init_load_cancel_loading)
                }
            }
        }
    }

    private fun clearProgress() {
        report.text = ""
    }

    private fun showCurrentLoadedEntity(entity: String?) {
        this.report.text = entity
    }

    private fun showLoading(isLoading: Boolean) {
        progress.isVisible = isLoading
    }

    private fun showReport(report: List<String>?) {
        this.report.apply {
            setTextColor(ContextCompat.getColor(this@InitLoadActivity, R.color.primary_dark))
            setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.common_18sp))
        }
        var loadReport = getString(R.string.init_load_init_loading_succeed)
        var buttonTitle = getString(R.string.init_load_cancel_done)
        if (!report.isNullOrEmpty()) {
            this.report.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.common_14sp))
            loadReport = report.joinToString(separator = "\n", prefix = "ERROR: ")
            buttonTitle = getString(R.string.init_load_init_retry)
        }
        this.report.text = loadReport
        loadingButton.text = buttonTitle
    }

    override fun onBackPressed() {
        if (loadingButton.text == getString(R.string.init_load_cancel_loading)) showCancelLoadingDialog()
        else super.onBackPressed()
    }

    private fun showCancelLoadingDialog() =
            AlertDialog.Builder(this)
                    .setTitle(R.string.init_load_ask_cancel_loading)
                    .setMessage(R.string.init_load_cancel_loading_explanation)
                    .setPositiveButton(R.string.common_ok) { d, _ ->
                        d.dismiss()
                        finish()
                    }
                    .setNegativeButton(R.string.common_no) { d, _ -> d.dismiss() }
                    .show()

    companion object {
        fun newIntent(context: Context) = Intent(context, InitLoadActivity::class.java)
    }
}