package com.orange5.nsts.ui.order.quick;

import android.app.AlertDialog;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.orange5.nsts.R;
import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.data.db.entities.ConsumableProduct;
import com.orange5.nsts.data.db.entities.OrderConsumableItem;
import com.orange5.nsts.data.db.entities.OrderRentalItem;
import com.orange5.nsts.data.db.entities.RentalProduct;
import com.orange5.nsts.data.db.entities.RentalProductItem;
import com.orange5.nsts.data.preferencies.RawSharedPreferences;
import com.orange5.nsts.scan.ScanningActivity;
import com.orange5.nsts.service.bin.BinService;
import com.orange5.nsts.service.order.BinQuantitySelection;
import com.orange5.nsts.service.order.OrderType;
import com.orange5.nsts.service.product.ProductSearchFilter;
import com.orange5.nsts.ui.base.ActivityType;
import com.orange5.nsts.ui.base.BaseActivity;
import com.orange5.nsts.ui.customer.SelectCustomerActivity;
import com.orange5.nsts.ui.extensions.ContextExtensionsKt;
import com.orange5.nsts.ui.extensions.ViewExtensionsKt;
import com.orange5.nsts.ui.home.BottomMenuDialog;
import com.orange5.nsts.ui.order.OrderItemView;
import com.orange5.nsts.ui.order.quick.dialogs.FinishQuickOrderDialog;
import com.orange5.nsts.ui.order.quick.dialogs.QuickConsumableAddDialog;
import com.orange5.nsts.ui.order.quick.dialogs.QuickRentalAddDialog;
import com.orange5.nsts.ui.order.search.ProductWrapper;
import com.orange5.nsts.ui.order.search.SearchProductDialog;
import com.orange5.nsts.util.Colors;
import com.orange5.nsts.util.Spannable;
import com.orange5.nsts.util.UI;
import com.orange5.nsts.util.adapters.recycler.RecyclerAdapterBuilder;
import com.orange5.nsts.util.format.CurrencyFormatter;
import com.orange5.nsts.util.view.CustomToast;
import com.orange5.nsts.util.view.dialogs.Dialogs;

import org.jetbrains.annotations.Nullable;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;

import static com.orange5.nsts.ui.order.full.pickup.PickUpActivity.ORDER_ID;

public class QuickOrderItemsActivity extends ScanningActivity implements BottomMenuDialog.Listener {

    @Inject
    RawSharedPreferences preferences;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.orderTotals)
    TextView orderTotals;
    @BindView(R.id.customerName)
    TextView customerName;
    @BindView(R.id.fab)
    protected FloatingActionButton fab;
    @BindView(R.id.buttonAddTenProduct)
    Button addTenProduct;

    private QuickOrderViewModel viewModel;
    private QuickConsumableAddDialog addConsumableDialog;
    private QuickRentalAddDialog addRentalDialog;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_order;
    }

    @Override
    protected int getMenuResourceId() {
        return R.menu.finish_order_menu;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = getVMFromActivity(QuickOrderViewModel.class);
        setOrder();
        viewModel.liveError.observe(this, this::errorSnack);
        viewModel.liveRunSync.observe(this, Void -> runSync());
        viewModel.liveCompleteOrder.observe(this, this::completeOrder);
        viewModel.liveDeleteOrder.observe(this, shouldFinish -> finish());
        viewModel.orderIsEmpty.observe(this, it -> showEmptyOrderAlert());
        viewModel.orderHasBackorder.observe(this, it -> showHasBackorderDialog());
        viewModel.showSigningDialog.observe(this, it -> showFinishDialog());
        viewModel.isStressButtonEnabled.observe(this,this::enableStressBtn);
        viewModel.isNeedToUpdateView.observe(this,it -> updateView());
        viewModel.isCheckedOut.observe(this,it ->
                CustomToast.show(this, getString(R.string.warning_rental_uun_is_checked_out)));
        getScanningViewModel().liveConsumableScanned.observe(this, this::onConsumableScanned);
        getScanningViewModel().liveRentalItemScanned.observe(this, this::onRentalScanned);
        setTitle(getString(R.string.quick_order_activity_order_number,
                viewModel.getCurrentOrder().getOrderNumber()));
        customerName.setText(viewModel.getCurrentOrder().getCustomer().toString());
        enableFloatingActionButton( v -> showProductDialog());
        ViewExtensionsKt.hideOnScroll(fab, recyclerView);
        updateView();
    }

    @Override
    public int notFoundMessage() {
        return R.string.quick_order_items_scan_rental_or_consumable_product_number;
    }

    protected void enableFloatingActionButton(View.OnClickListener clickListener) {
        fab.setOnClickListener(clickListener);
        fab.setVisibility(View.VISIBLE);
        fab.post(() -> fab.animate().y(contentView.getMeasuredHeight() - 2 * fab.getMeasuredHeight())
                .setDuration(300).start());
    }

    private void enableStressBtn(boolean isShow) {
        if (isShow) {
            addTenProduct.setVisibility(View.VISIBLE);
            addTenProduct.setOnClickListener(view -> viewModel.addTenProduct());
        }
    }

    private void showProductDialog() {
        SearchProductDialog.show(viewModel.getSelectedItems(), this::showAddDialog,
                new ProductSearchFilter(preferences), OrderType.QUICK_ORDER);
    }

    private void showAddDialog(ProductWrapper productWrapper) {
        if (productWrapper.isConsumable()) {
            addConsumable(productWrapper);
        } else if (productWrapper.getProduct() instanceof RentalProductItem) {
            viewModel.actionAddingRentalProduct((RentalProductItem) productWrapper.getProduct());
        }
    }

    private void setOrder() {
        String orderId = getIntent().getStringExtra(ORDER_ID);
        if (orderId == null) {
            finish();
            Timber.e(getString(R.string.warning_id_quick_order_null, this.getClass().getSimpleName()));
            return;
        }
        viewModel.loadExisting(orderId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.finish) {
            viewModel.createOrder();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onConsumableScanned(ConsumableProduct product) {
        addConsumable(new ProductWrapper(product));
    }

    public void onRentalScanned(RentalProductItem product) {
        viewModel.addRentalProduct(product.getRentalProduct(), product.getUniqueUnitNumber());
        updateView();
    }

    private boolean showRemoveDialog(int position, OrderItem item) {
        item.ifConsumable(this::showRemoveConsumableDialog);
        item.ifRental(this::showRemoveRentalDialog);
        return false;
    }

    private void showRemoveConsumableDialog(OrderConsumableItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.common_remove_rental_n, item.getProductNumber()));
        List<BinQuantitySelection> binQuantitySelections =
                viewModel.getSelectedItemQuantity(item.getConsumableProduct());
        builder.setMessage(updateMessage(binQuantitySelections, item.getQuantity()));
        builder.setPositiveButton(R.string.common_return, (dialogInterface, i) -> {
            viewModel.removeConsumableItem(item);
            updateView();
        });
        builder.setNegativeButton(R.string.common_cancel, null);
        builder.show();
    }

    private void showRemoveRentalDialog(OrderRentalItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.common_remove_rental_n, item.getProductNumber()));
        builder.setMessage(configTextMessage(item.getUniqueUnitNumber(),
                BinService.getById(item.getRentalProduct().getDefaultBinId()).getBinNum()));
        builder.setPositiveButton(R.string.common_return, (dialogInterface, i) -> {
            viewModel.removeRentalItem(item,
                    BinService.getById(item.getRentalProduct().getDefaultBinId()));
            updateView();
        });
        builder.setNegativeButton(R.string.common_cancel, null);
        builder.show();
    }

    private SpannableString configTextMessage(String returningName,
                                              String returnToBin) {
        String string = getString(R.string.common_return_s_to_s, returningName,
                returnToBin);
        return Spannable.highlightTextColor(string, Colors.HIGHLIGHT_BLUE, returningName,
                returnToBin);
    }

    private CharSequence updateMessage(List<BinQuantitySelection> binQuantitySelections,
                                       Integer quantity) {
        SpannableString spannableString = null;
        CharSequence result = "";
        for (BinQuantitySelection bin : binQuantitySelections) {
            if (bin.getSelectedQuantity() > 0) {
                if (spannableString == null) {
                    SpannableString nextString = configTextMessage(
                            String.valueOf(bin.getSelectedQuantity()),
                            bin.getBinNumber());
                    spannableString = nextString;
                    result = nextString;
                } else {
                    result = TextUtils.concat(result, "\n",
                            configTextMessage(
                                    String.valueOf(bin.getSelectedQuantity()),
                                    bin.getBinNumber()));
                }
                quantity -= bin.getSelectedQuantity();
            }
        }
        return result;
    }

    private void showChangeQuantityDialog(int i, OrderItem item) {
        item.ifConsumable(consumableItem ->
                addConsumable(ProductWrapper.from(item)));
    }

    private void addConsumable(ProductWrapper productWrapper) {
        if (addConsumableDialog != null) {
            addConsumableDialog.dismiss();
        }
        addConsumableDialog = QuickConsumableAddDialog
                .with(this, productWrapper, viewModel, false)
                .selectionConsumer((bins, isBackorder) -> {
                    viewModel.addConsumableProduct((ConsumableProduct) productWrapper.getProduct(),
                            bins, isBackorder, false);
                    updateView();
                });
    }

    private void addRental(RentalProduct rentalProduct) {
        if (rentalProduct.getBinInformation().getTotalQuantity() == 0) {
            Toast.makeText(this, R.string.quick_order_activity_no_available, Toast.LENGTH_SHORT).show();
            return;
        }
        if (addRentalDialog != null) {
            addRentalDialog.dismiss();
        }
        addRentalDialog = QuickRentalAddDialog.with(this);
        addRentalDialog.resultConsumer((binNumber, uun) -> {
            viewModel.addRentalProduct(rentalProduct, uun);
            updateView();
        }).pick(rentalProduct);
    }

    private void updateView() {
        reloadSelectedItems();
        UI.setVisibility(findViewById(R.id.emptyOrderText), viewModel.isEmptyItems());
        String sum = CurrencyFormatter.Companion.formatPrice(viewModel.getCurrentOrder().getOrderSum());
        int count = viewModel.getSelectedItemCount();
        orderTotals.setText(getResources().getQuantityString(R.plurals.sum_n_items, count, sum, count));
    }

    private void reloadSelectedItems() {
        RecyclerAdapterBuilder<OrderItemView, OrderItem> selectedItemsAdapter
                = new RecyclerAdapterBuilder<>(recyclerView);
        selectedItemsAdapter
                .viewFactory(() -> new OrderItemView(this, OrderType.QUICK_ORDER))
                .data(viewModel::getSelectedItems)
                .bind(OrderItemView::fill)
                .onCheckedChange(R.id.isBackorder, (this::setBackorder))
                .onLongClick(R.id.contents, this::showRemoveDialog)
                .onClick(R.id.contents, this::showChangeQuantityDialog)
                .vertical();
        recyclerView.smoothScrollToPosition(viewModel.getSelectedItems().size());
    }

    private void showEmptyOrderAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.getActive());
        builder.setTitle(R.string.quick_order_activity_order_is_empty);
        builder.setMessage(R.string.quick_order_activity_no_items_added_to_order);
        builder.setPositiveButton(R.string.common_ok, null);
        builder.show();
    }

    private void setBackorder(int position, OrderItem item, boolean isBackorder) {
        viewModel.setBackorderFlag(item, isBackorder);
        updateView();
    }

    private void showFinishDialog() {
        if (viewModel.checkIfHasOnlyBackorder()) {
            FinishQuickOrderDialog
                    .with(this, viewModel.getCurrentOrder())
                    .finishOrder(signature -> viewModel.finishOrder(signature,false))
                    .finishOrderAndStartNew(signature -> viewModel.finishOrder(signature, true))
                    .show();
        } else {
            ContextExtensionsKt.showDialog(this,
                    getString(R.string.quick_order_activity_alert_title_has_only_backorder),
                    getString(R.string.quick_order_activity_alert_message_has_only_backorder),
                    getString(R.string.common_ok) );
        }
    }


    private void showHasBackorderDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.quick_order_activity_back_order_confirmation_title)
                .setMessage(R.string.quick_order_activity_back_order_confirmation_message)
                .setPositiveButton(R.string.common_ok, (d, w) -> {
                    viewModel.shouldCreateNewOrder = true;
                    showFinishDialog();
                    d.dismiss();
                })
                .setNegativeButton(android.R.string.cancel, (d, w) -> {
                    viewModel.shouldCreateNewOrder = false;
                    d.dismiss();
                })
                .show();
    }

    private void completeOrder(boolean shouldStartNextOrder) {
        if (shouldStartNextOrder) {
            startActivity(SelectCustomerActivity.newIntent(this, ActivityType.QUICK_ORDER));
        }
        finish();
    }

    @Override
    public void onBottomButtonClick() {
        Dialogs.newDialog()
                .setTitle(R.string.common_remove_from_order)
                .setMessage(getString(R.string.common_confirm_order_removal, viewModel.getCurrentOrder().getOrderNumber()))
                .action(getString(R.string.common_remove), () -> viewModel.deleteOrder())
                .no(getString(R.string.common_cancel), null)
                .show();
    }

    @Override
    public void onTopButtonClick() {
        viewModel.saveForLater();
        finish();
    }

    @Override
    public void onBackButtonClick() {
        ContextExtensionsKt.toast(this, R.string.common_order_was_saved, Toast.LENGTH_SHORT);

    }

    @Override
    public void onBackPressed() {
        BottomMenuDialog.show(getSupportFragmentManager());
    }
}
