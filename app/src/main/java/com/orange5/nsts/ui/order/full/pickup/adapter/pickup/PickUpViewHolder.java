package com.orange5.nsts.ui.order.full.pickup.adapter.pickup;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.SpannableString;
import android.view.View;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.data.db.base.OrderProduct;
import com.orange5.nsts.data.db.entities.OrderConsumableItem;
import com.orange5.nsts.service.rental.AggregatedRentalItem;
import com.orange5.nsts.ui.base.BaseViewHolder;
import com.orange5.nsts.util.format.CurrencyFormatter;

import java.util.Locale;

public class PickUpViewHolder extends BaseViewHolder {

    private int redColor;
    private int greenColor;

    private final OrderItem currentItem;
    private final OrderProduct currentProduct;
    private final Context context;

    public PickUpViewHolder(Context context, OrderItem item) {
        this.context = context;
        this.currentItem = item;
        this.currentProduct = getProduct();
        redColor = context.getColor(R.color.errorRed);
        greenColor = context.getColor(R.color.okGreen);
    }

    private OrderProduct getProduct() {
        if (currentItem instanceof OrderConsumableItem) {
            OrderConsumableItem currentItem = (OrderConsumableItem) this.currentItem;
            return currentItem.getConsumableProduct();
        }

        if (currentItem instanceof AggregatedRentalItem) {
            AggregatedRentalItem currentItem = (AggregatedRentalItem) this.currentItem;
            return currentItem.getRentalProduct();
        }
        throw new IllegalStateException();
    }

    public String getFormattedPrice() {
        return CurrencyFormatter.Companion.formatPrice(currentProduct.getPrice());
    }

    public SpannableString getFormattedQuantity() {
        if (currentItem instanceof OrderConsumableItem) {
//            int totalQuantity = ((OrderConsumableItem) currentItem).getConsumableProduct().getBinInformation().getTotalQuantity();
            if (currentItem.isOversell()) {
                int oversell = ((OrderConsumableItem) currentItem).getOversell();
                String formattedString = String.format(Locale.getDefault(), "Quantity: %d (+%d)", currentItem.getQuantity(), oversell);
                return highlightTextColor(formattedString, String.format(Locale.getDefault(), "(+%d)", oversell), redColor);
            }
            return SpannableString.valueOf(String.format(Locale.getDefault(), "Quantity: %d", currentItem.getQuantity()));
        }
        return SpannableString.valueOf(String.format(Locale.getDefault(), "Quantity: %d", currentItem.getQuantity()));
    }

    public SpannableString getProductDescription(String highlight) {
        return highlightBackground(currentProduct.getProductDescription(), highlight);
    }

    public SpannableString getProductNumber(String highlight) {
        return highlightBackground(currentProduct.getProductNumber(), highlight);
    }

    public String getBinQuantities() {
        return currentProduct.getBinInformation().getAvailableText();
    }

    @SuppressLint("DefaultLocale")
    public String getPickedItemsCount() {
        String pickedCountFormatString = "Picked: %d/%d";
        if (currentItem instanceof OrderConsumableItem) {
            OrderConsumableItem consumableItem = (OrderConsumableItem) currentItem;
            int picked = consumableItem.getPicked();
            if (consumableItem.isOversell() && consumableItem.oversellAllowed()) {
                picked += consumableItem.getOversell();
            }
            return String.format(pickedCountFormatString, picked, consumableItem.getQuantity());
        }
        AggregatedRentalItem aggregatedRentalItem = (AggregatedRentalItem) currentItem;
        long pickedCount = aggregatedRentalItem.getPicked();

        return String.format(pickedCountFormatString, pickedCount, aggregatedRentalItem.getQuantity());
    }

    public int getCountColor() {
        if (currentItem instanceof OrderConsumableItem) {
            OrderConsumableItem consumableItem = (OrderConsumableItem) currentItem;
            return consumableItem.allPicked() ? greenColor : redColor;
        }
        AggregatedRentalItem aggregatedRentalItem = (AggregatedRentalItem) currentItem;
        return aggregatedRentalItem.allPicked() ? greenColor : redColor;
    }

    public int getSecondaryProgress() {
        if (currentItem instanceof AggregatedRentalItem) {
            return 0;
        }
        OrderConsumableItem consumableItem = (OrderConsumableItem) currentItem;
        if (consumableItem.isOversell() && consumableItem.oversellAllowed()) {
            return consumableItem.getPicked() + consumableItem.getOversell();
        }
        return 0;
    }

    public int getOversellSwitchVisibility() {
        if (!(currentItem instanceof OrderConsumableItem)) {
            return View.GONE;
        }
        OrderConsumableItem consumableItem = (OrderConsumableItem) currentItem;
        if (!consumableItem.isOversell()) {
            return View.GONE;
        }
        return View.VISIBLE;
    }

    public String getOversellText() {
        if (!(currentItem instanceof OrderConsumableItem)) {
            return "";
        }
        OrderConsumableItem consumableItem = (OrderConsumableItem) currentItem;
        if (!consumableItem.isOversell()) {
            return "";
        }
        int oversell = consumableItem.getOversell();
        return context.getResources().getQuantityString(R.plurals.allow_oversell_of, oversell, oversell);
    }

    public boolean isOversellAllowed() {
        if (!(currentItem instanceof OrderConsumableItem)) {
            return false;
        }
        OrderConsumableItem consumableItem = (OrderConsumableItem) currentItem;
        return consumableItem.oversellAllowed();
    }
}
