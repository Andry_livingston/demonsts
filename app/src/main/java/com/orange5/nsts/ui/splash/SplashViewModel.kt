package com.orange5.nsts.ui.splash

import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.orange5.nsts.R
import com.orange5.nsts.data.db.DbLoader
import com.orange5.nsts.data.db.entities.User
import com.orange5.nsts.data.preferencies.RawSharedPreferences
import com.orange5.nsts.service.bin.BinService
import com.orange5.nsts.service.user.UserService
import com.orange5.nsts.ui.base.ActionLiveData
import com.orange5.nsts.ui.base.BaseViewModel
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SplashViewModel @Inject constructor(
        private val dbLoader: DbLoader,
        private val preferences: RawSharedPreferences,
        private val userService: UserService,
        private val binService: BinService) : BaseViewModel() {

    @JvmField
    val liveLogInSucceed = ActionLiveData<Unit>()
    @JvmField
    val liveLogInFail = MutableLiveData<@StringRes Int>()
    @JvmField
    val liveDbLoadSucceed = ActionLiveData<Unit>()
    @JvmField
    val userData = ActionLiveData<User>()

    var user: User?
        get() = preferences.user
        set(value) {
            preferences.user = value
        }

    fun retrieveUser() {
        if(preferences.isShouldRemember && user != null) {
            userData.value = user
        }
    }

    fun logIn(userData: UserLoginView.UserData) {
        addDisposable(rxCompletableCreate {
            val user: User? = userService.getUserByUserName(userData.userName)
            user?.let {
                if (it.password == userData.password) {
                    preferences.isShouldRemember = userData.isShouldRemember
                    this.user = it
                    liveLogInSucceed.postValue(Unit)
                } else liveLogInFail.postValue(R.string.splash_activity_error_login_wrong_password)
            } ?: liveLogInFail.postValue(R.string.splash_activity_error_login_user_does_not_exists)
        }
                .doOnEvent { liveProgress.postValue(false) }
                .subscribeOn(Schedulers.computation())
                .subscribe({ }, ::onError))
    }

    fun loadDb() {
        addDisposable(rxCompletableCreate { dbLoader.loadDbIfNeeded() }
                .subscribeOn(Schedulers.io())
                .subscribe({ liveDbLoadSucceed.postValue(Unit) }, ::onError))
    }

}