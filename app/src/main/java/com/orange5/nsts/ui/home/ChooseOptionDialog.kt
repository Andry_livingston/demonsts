package com.orange5.nsts.ui.home

import android.app.AlertDialog
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseActivity
import com.orange5.nsts.util.view.dialogs.BaseDialog
import kotlinx.android.synthetic.main.dialog_choose_order.view.*

class ChooseOptionDialog(activity: BaseActivity) : BaseDialog(activity) {
    override fun getLayoutRes(): Int = R.layout.dialog_choose_order

    private fun create(@StringRes dialogTitle: Int, @StringRes btnTopTitle: Int,
                       @StringRes btnBottomTitle: Int, btnTopClick: Runnable,
                       btnBottomClick: Runnable,
                       badgeCount: Int?) {
        dialog = AlertDialog.Builder(activity).setView(view).create()
        view.run {
            title.text = resources.getString(dialogTitle)
            btnTop.text = resources.getString(btnTopTitle)
            btnBottom.text = resources.getString(btnBottomTitle)
            btnTop.setOnClickListener { onTopButtonClick(btnTopClick) }
            btnBottom.setOnClickListener { onBottomButtonClick(btnBottomClick) }
            if (badgeCount != null) {
                badge.isVisible = badgeCount != 0
                badge.text = if (badgeCount > MAX_LIMIT) MAX_LIMIT_TEXT else badgeCount.toString()
            }
        }
        show()
    }

    private fun onTopButtonClick(action: Runnable) {
        action.run()
        dismiss()
    }

    private fun onBottomButtonClick(action: Runnable) {
        action.run()
        dismiss()
    }

    companion object {
        private const val MAX_LIMIT = 99
        private const val MAX_LIMIT_TEXT = "99+"

        @JvmOverloads
        @JvmStatic
        fun createNewOrder(activity: BaseActivity, btnTopClick: Runnable = Runnable { }, btnBottomClick: Runnable = Runnable { }) =
                create(activity, R.string.choose_order_dialog_select_order, R.string.choose_order_dialog_create_quick_order, R.string.choose_order_dialog_create_full_order, btnTopClick, btnBottomClick)

        @JvmOverloads
        @JvmStatic
        fun createNewReturn(activity: BaseActivity, btnTopClick: Runnable = Runnable { }, btnBottomClick: Runnable = Runnable { }) =
                create(activity, R.string.select_return_type, R.string.rental_return_activity_quick, R.string.rental_return_activity_full, btnTopClick, btnBottomClick)

        @JvmOverloads
        @JvmStatic
        fun createDiscrepancies(activity: BaseActivity, btnTopClick: Runnable = Runnable { }, btnBottomClick: Runnable = Runnable { }) =
                create(activity, R.string.discrepancy_select_type, R.string.discrepancy_move_to_shipment_shortage, R.string
                        .discrepancy_create_shipment_extras, btnTopClick, btnBottomClick)

        private fun create(activity: BaseActivity,
                           @StringRes dialogTitle: Int,
                           @StringRes btnTop: Int,
                           @StringRes btnBottom: Int,
                           btnTopClick: Runnable = Runnable { },
                           btnBottomClick: Runnable = Runnable { },
                           badge: Int? = 0) = ChooseOptionDialog(activity).apply {
            create(dialogTitle, btnTop, btnBottom, btnTopClick, btnBottomClick, badge)
        }
    }
}