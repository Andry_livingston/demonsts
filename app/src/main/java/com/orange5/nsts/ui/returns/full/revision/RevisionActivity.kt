package com.orange5.nsts.ui.returns.full.revision

import androidx.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.view.isVisible
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.Customer
import com.orange5.nsts.ui.returns.ReturnActivity
import com.orange5.nsts.ui.base.TwoPageAdapter
import com.orange5.nsts.ui.base.TwoPageAdapter.TabType.RESULT
import com.orange5.nsts.ui.base.TwoPageAdapter.TabType.INIT
import kotlinx.android.synthetic.main.activity_return.*

class RevisionActivity : ReturnActivity() {

    override val viewModel: RevisionViewModel by viewModels { vmFactory }

    override val adapter: TwoPageAdapter by lazy {
        val map =
                mapOf(INIT to RevisionRentalItemsFragment(), RESULT to RevisionReturnedItemsFragment())
        TwoPageAdapter(map, supportFragmentManager)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.returnFinished.observe(this, Observer { finishReturn() })
        viewModel.liveCustomer.observe(this, Observer(::setCustomer))
        val customerId = intent.getStringExtra(CUSTOMER_KEY)
        viewModel.setCustomer(customerId)
    }

    override fun getActivityTitle(): String = getString(R.string.revision_activity_title)

    override fun onFinishProcessClick() {
        if (viewModel.allReturnItems.value.isNullOrEmpty()) showEmptyReturnDialog()
        else viewModel.createReturn(ByteArray(0))
    }

    private fun setCustomer(customer: Customer?) {
        customerName.isVisible = customer != null
        customerName.text = "${customer?.fName} ${customer?.lName}"
    }

    companion object {
        @JvmStatic
        fun newIntent(context: Context, customerId: String?): Intent =
                Intent(context, RevisionActivity::class.java).apply {
                    putExtra(CUSTOMER_KEY, customerId)
                }

        private const val CUSTOMER_KEY = "customer_key"
    }
}