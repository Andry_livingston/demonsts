package com.orange5.nsts.ui.home;

import androidx.core.util.Pair;
import androidx.lifecycle.MutableLiveData;

import com.orange5.nsts.R;
import com.orange5.nsts.api.sync.SyncClient;
import com.orange5.nsts.app.modules.ApiModule;
import com.orange5.nsts.data.db.entities.BinNumber;
import com.orange5.nsts.data.db.entities.Order;
import com.orange5.nsts.data.db.entities.OrderConsumableItem;
import com.orange5.nsts.data.db.entities.OrderRentalItem;
import com.orange5.nsts.data.db.entities.RentalProductItem;
import com.orange5.nsts.data.db.entities.RentalReturn;
import com.orange5.nsts.data.preferencies.RawSharedPreferences;
import com.orange5.nsts.service.bin.BinService;
import com.orange5.nsts.service.order.OrderService;
import com.orange5.nsts.service.order.OrderStatus;
import com.orange5.nsts.service.rental.RentalProductItemService;
import com.orange5.nsts.service.rentalReturn.RentalReturnService;
import com.orange5.nsts.ui.base.ActionLiveData;
import com.orange5.nsts.ui.base.BaseViewModel;
import com.orange5.nsts.ui.settings.models.ApiSettings;
import com.orange5.nsts.ui.shipping.repository.ReceivingRepository;
import com.orange5.nsts.util.CollectionUtils;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class HomeViewModel extends BaseViewModel {

    private final ApiModule apiModule;
    private final OrderService orderService;
    private final RentalProductItemService rentalItemService;
    private final RentalReturnService rentalService;
    private final RawSharedPreferences preferences;
    private final BinService binService;
    private final ReceivingRepository receivingRepository;
    private SyncClient syncClient;
    private BinNumber inspectionBin;

    @Inject
    public HomeViewModel(ApiModule apiModule,
                         OrderService orderService,
                         RentalProductItemService rentalItemService,
                         RentalReturnService rentalService,
                         BinService binService,
                         RawSharedPreferences preferences,
                         ReceivingRepository receivingRepository) {
        this.apiModule = apiModule;
        this.syncClient = apiModule.syncClient();
        this.orderService = orderService;
        this.rentalItemService = rentalItemService;
        this.rentalService = rentalService;
        this.preferences = preferences;
        this.binService = binService;
        this.receivingRepository = receivingRepository;
        pingServer();
    }

    public ActionLiveData<Pair<Boolean, String>> liveChangeIp = new ActionLiveData<>();
    public MutableLiveData<Void> livePingServer = new MutableLiveData<>();
    public MutableLiveData<Integer> inspectionBadge = new MutableLiveData<>();
    public MutableLiveData<Integer> receivingBadge = new MutableLiveData<>();
    public MutableLiveData<Pair<List<Order>, String>> liveOrders = new MutableLiveData<>();
    public MutableLiveData<Pair<List<RentalReturn>, String>> liveReturns = new MutableLiveData<>();
    public MutableLiveData<Boolean> liveCantDeleteOrder = new MutableLiveData<>();
    public MutableLiveData<Boolean> isHideQuickOrder = new MutableLiveData<>();
    public MutableLiveData<Boolean> isHideQuickReturn = new MutableLiveData<>();
    public MutableLiveData<Integer> finishDeleteOrder = new MutableLiveData<>();
    public MutableLiveData<Boolean> ordersProgress = new MutableLiveData<>();
    public MutableLiveData<Boolean> rentalsProgress = new MutableLiveData<>();

    void pingServer() {
        callJobIo(
                () -> syncClient.confirmDataS(new String[0]),
                version -> livePingServer.postValue(null),
                this::handlePingException);
    }

    public void refreshOrders() {
        String query = "";
        if (liveOrders.getValue() != null && liveOrders.getValue().second != null) {
            query = liveOrders.getValue().second;
        }
        fetchOrders(query);
    }

    public void refreshReturns() {
        String query = "";
        if (liveReturns.getValue() != null && liveReturns.getValue().second != null) {
            query = liveReturns.getValue().second;
        }
        fetchReturns(query);
    }

    public void fetchData() {
        fetchOrders("");
        fetchReturns("");
        fetchReceivingBadge();
        fetchInspectionBadge();
    }

    public void rebuildSyncService(String ip) {
        saveIpToPreferences(ip);
        syncClient = apiModule.syncClient(ip);
    }

    public void fetchOrders(String query) {
        ordersProgress.setValue(true);
        addDisposable(rxSingleCreate(() -> orderService.getOrders(query))
                .subscribeOn(Schedulers.io())
                .map(this::configOrdersList)
                .map(list -> new Pair<>(list, query))
                .doFinally(() -> ordersProgress.postValue(false))
                .subscribe(ordersPair -> liveOrders.postValue(ordersPair), this::onError));
    }

    public void fetchReturns(String query) {
        rentalsProgress.setValue(true);
        addDisposable(rxSingleCreate(() -> rentalService.getRentals(query))
                .subscribeOn(Schedulers.io())
                .map(list -> new Pair<>(list, query))
                .doFinally(() -> rentalsProgress.postValue(false))
                .subscribe(rentalsPair -> liveReturns.postValue(rentalsPair), this::onError));
    }

    public List<OrderRentalItem> getPickedRentals(Order order) {
        return order.getRentalItems()
                .stream()
                .filter(it -> it.getUniqueUnitNumber() != null)
                .collect(Collectors.toList());
    }

    public List<OrderConsumableItem> getPickedConsumables(Order order) {
        return order.getConsumableItems()
                .stream()
                .filter(orderConsumableItem -> (orderConsumableItem.getPicked() > 0) || (orderConsumableItem.isOversell() && orderConsumableItem.oversellAllowed()))
                .collect(Collectors.toList());
    }

    public void deleteOrder(Order order, int position) {
        if (isOrderHasShipOutBinItems(order)) {
            liveCantDeleteOrder.postValue(true);
        } else {
            finishDeleteOrder.postValue(position);
            orderService.delete(order);
        }
    }

    public void checkConditionForCreatingNewOrder() {
        isHideQuickOrder.setValue(preferences.isHideQuickOrder());
    }

    public void checkConditionForCreatingNewReturn() {
        isHideQuickReturn.setValue(preferences.isHideQuickReturn());
    }

    private boolean isOrderHasShipOutBinItems(Order order) {
        return !getPickedRentals(order).isEmpty() || !getPickedConsumables(order).isEmpty();
    }

    private void saveIpToPreferences(String ip) {
        ApiSettings apiSettings = new ApiSettings(ip);
        preferences.setApiSettings(apiSettings);
    }

    private boolean isClosed(Order order) {
        return OrderStatus.values()[order.getOrderStatusId() == null ? 0 : order.getOrderStatusId()].getName() == R.string.order_status_order_closed;
    }

    private void fetchInspectionBadge() {
        callJobIo(() -> {
            if (inspectionBin == null) inspectionBin = binService.byBinNumber(BinService.INSPECTION);
            List<RentalProductItem> items = rentalItemService.loadRevisionItems(inspectionBin.getBinId());
            return CollectionUtils.isNotEmpty(items) ? items.size() : 0;
        }, inspectionBadge::postValue);
    }

    private void fetchReceivingBadge() {
        callJobIo(receivingRepository::getReceivingItemsCount, receivingBadge::postValue);
    }

    private List<Order> configOrdersList(List<Order> orders) {
        Collections.sort(orders, this::compare);
        return new ArrayList<>(new LinkedHashSet<>(orders));
    }

    private int compare(Order o1, Order o2) {
        if (isClosed(o1) && isClosed(o2) || !isClosed(o1) && !isClosed(o2)) {
            return o1.getDateTime().isBefore(o2.getDateTime()) ? 1 : -1;
        } else return isClosed(o1) ? 1 : -1;
    }

    private void handlePingException(Throwable throwable) {
        Timber.e(throwable);
        if (throwable instanceof ConnectException && throwable.getMessage().contains("Failed to connect to") ||
                throwable instanceof SocketTimeoutException && throwable.getMessage().contains("failed to connect to")) {
            liveChangeIp.postValue(Pair.create(true,
                    preferences.getApiSettings().getHost()));
        }
    }

}
