package com.orange5.nsts.ui.shipping.discrepancy.repository

import android.os.Parcelable
import androidx.annotation.StringRes
import com.orange5.nsts.R
import com.orange5.nsts.data.db.base.OrderProduct
import com.orange5.nsts.data.db.entities.Discrepancies
import com.orange5.nsts.ui.shipping.repository.ReceivingNotice
import com.orange5.nsts.util.db.GUIDFactory
import com.orange5.nsts.util.format.DateFormatter
import kotlinx.android.parcel.Parcelize

@Parcelize
class Discrepancy(val id: String,
                  val title: String,
                  val type: Type,
                  var description: String,
                  val uun: String = "",
                  var quantity: Int = 0,
                  val productId: String,
                  val productDescription: String,
                  val productNumber: String,
                  val date: String = "",
                  var comment: String = "",
                  private var isResolved: Byte = 0) : Parcelable {

    fun isRental() = uun.isNotEmpty()
    fun isResolved() = isResolved == 1.toByte()
    fun setResolved(isResolved: Boolean) {
        this.isResolved = if (isResolved) 1 else 0
    }

    fun copy(id: String = this.id,
             title: String = this.title,
             type: Type = this.type,
             description: String = this.description,
             uun: String = this.uun,
             quantity: Int = this.quantity,
             productId: String = this.productId,
             productDescription: String = this.productDescription,
             productNumber: String = this.productNumber,
             date: String = this.date,
             comment: String = this.comment,
             isResolved: Byte = this.isResolved) =
            Discrepancy(id,
                    title,
                    type,
                    description,
                    uun,
                    quantity,
                    productId,
                    productDescription,
                    productNumber,
                    date,
                    comment,
                    isResolved)

    companion object {

        private const val SHIPPING_DISCREPANCY_TITLE = "Shipping Notice №"
        private const val SHIPPING_UNKNOWN_DISCREPANCY = "Shipping Notice Import"

        @JvmStatic
        fun from(discrepancies: Discrepancies, productDescription: String, productNumber: String) = Discrepancy(
                discrepancies.discrepanciesID,
                discrepancies.descripancyDescription,
                mapType(discrepancies.discrepancyTypeId),
                discrepancies.upDescription ?: "",
                discrepancies.rentalUniqueUnitNumber ?: "",
                discrepancies.consumableQuantity ?: 0,
                productId(discrepancies) ?: "",
                productDescription,
                productNumber,
                discrepancies.addingDate,
                discrepancies.comment ?: "",
                discrepancies.isResolved
        )

        @JvmStatic
        fun to(discrepancy: Discrepancy) = Discrepancies(
                discrepancy.id,
                discrepancy.title,
                discrepancy.type.typeId,
                discrepancy.date,
                if (discrepancy.isRental() || discrepancy.productId.isEmpty()) null else discrepancy.productId,
                discrepancy.quantity,
                if (discrepancy.isRental() && discrepancy.productId.isNotEmpty()) discrepancy.productId else null,
                if (discrepancy.isRental() && discrepancy.uun.isNotEmpty()) discrepancy.uun else null,
                discrepancy.comment,
                discrepancy.isResolved,
                null,
                null,
                null,
                discrepancy.description,
                null,
                null)

        @JvmStatic
        fun from(notice: ReceivingNotice, type: Type = Type.SHIPMENT_SHORTAGE) = Discrepancy(
                GUIDFactory.newUUID(),
                "$SHIPPING_DISCREPANCY_TITLE${notice.transferNumber}",
                type,
                mapDescription(notice),
                notice.uniqueUnitNumber,
                1,
                notice.productId,
                notice.productDescription,
                notice.productNumber,
                DateFormatter.currentTimeDb(),
                "",
                0
        )

        @JvmStatic
        fun from(product: OrderProduct, transferNumber: String, type: Type = Type.SHIPMENT_EXTRAS) = Discrepancy(
                GUIDFactory.newUUID(),
                "$SHIPPING_DISCREPANCY_TITLE$transferNumber",
                type,
                mapDescription(product, transferNumber),
                product.uun,
                product.quantity,
                product.productId,
                product.productDescription,
                product.productNumber,
                DateFormatter.currentTimeDb(),
                "",
                0
        )

        fun unknown(count: Int, description: String, notes: String, transferNumber: String) = Discrepancy(
                GUIDFactory.newUUID(),
                SHIPPING_UNKNOWN_DISCREPANCY,
                Type.NON_EXISTING_PRODUCT,
                "Unknown${if (transferNumber.isNotEmpty()) " from Transfer №$transferNumber " else ""}- $description",
                "",
                count,
                "",
                "",
                "",
                DateFormatter.currentTimeDb(),
                notes,
                0
        )

        private fun productId(discrepancies: Discrepancies) =
                if (discrepancies.rentalUniqueUnitNumber.isNullOrEmpty()) discrepancies.consumableProductId
                else discrepancies.rentalProductId

        private fun mapDescription(notice: ReceivingNotice): String {
            val type = if (notice.isRental()) "Rental" else "Consumable"
            val uun = if (notice.isRental()) " (${notice.uniqueUnitNumber}" else ""
            val date = if (notice.shippingDate().isNotEmpty()) "(${notice.shippingDate()})" else ""
            return "$type product №${notice.productNumber}$uun from Transfer №${notice.transferNumber}$date"
        }

        private fun mapDescription(product: OrderProduct, transferNumber: String): String {
            val type = if (product.isRental) "Rental" else "Consumable"
            val uun = if (product.isRental) " ${product.uun}" else ""
            val transferNo = if (transferNumber.isNotEmpty()) " from Transfer №$transferNumber" else ""
            return "$type product №${product.productNumber}$uun$transferNo"
        }

        private fun mapType(id: Int): Type = Type.values().find { it.typeId == id } ?: Type.NON_EXISTING_PRODUCT
    }

    enum class Type(val typeId: Int, @StringRes val titleResId: Int) {
        SHIPMENT_SHORTAGE(1, R.string.discrepancy_shipment_shortage),
        SHIPMENT_EXTRAS(2, R.string.discrepancy_shipment_extras),
        INVENTORY_DISCREPANCY(3, R.string.discrepancy_inventory_discrepancy),
        RENTAL_RETURN_BY_OTHER(4, R.string.discrepancy_rental_return_by_other),
        INVENTORY_CHANGE(5, R.string.discrepancy_inventory_change),
        SCANNED_NOT_FOUND(6, R.string.discrepancy_scanned_not_found),
        QNTY_CHANGED_ON_PICKUP(7, R.string.discrepancy_qnty_changed_on_pickup),
        NON_EXISTING_PRODUCT(8, R.string.discrepancy_non_existingProduct),
        PRODUCT_DELETED(9, R.string.discrepancy_product_deleted);
    }
}


fun Discrepancy.Type.isUnknown() = this == Discrepancy.Type.NON_EXISTING_PRODUCT

fun Discrepancy.Type.isShortage() = this == Discrepancy.Type.SHIPMENT_SHORTAGE

fun Discrepancy.Type.isExtras() = this == Discrepancy.Type.SHIPMENT_EXTRAS