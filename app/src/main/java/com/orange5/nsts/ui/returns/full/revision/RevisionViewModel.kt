package com.orange5.nsts.ui.returns.full.revision

import androidx.lifecycle.MutableLiveData
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.*
import com.orange5.nsts.service.bin.BinService
import com.orange5.nsts.service.bin.BinService.INSPECTION
import com.orange5.nsts.service.customer.CustomerService
import com.orange5.nsts.service.order.items.OrderRentalItemService
import com.orange5.nsts.service.rental.RentalProductItemService
import com.orange5.nsts.service.rental.RentalStatus
import com.orange5.nsts.service.rentalReturn.RentalReturnService
import com.orange5.nsts.ui.returns.ReturnViewModel
import com.orange5.nsts.util.format.DateFormatter
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

open class RevisionViewModel @Inject constructor(private val returnService: RentalReturnService,
                                                 private val orderRentalItemService: OrderRentalItemService,
                                                 private val rentalProductService: RentalProductItemService,
                                                 private val customerService: CustomerService,
                                                 private val binService: BinService)
    : ReturnViewModel(orderRentalItemService, binService) {

    private var inspectionBin: BinNumber? = null
    val liveCustomer = MutableLiveData<Customer>()

    init {
        loadBins()
    }

    override fun createReturn(signature: ByteArray) {
        runDbTransaction {
            mappedReturns.value?.forEach { item, product ->
                product.status = RentalStatus.Available.id
                rentalProductService.update(product)
                val rentalReturn = returnService.getRentalById(item.rentalReturnId)
                if (mappedRentals.value.isNullOrEmpty()) {
                    updateRentalReturn(rentalReturn)
                    item.returnDate = rentalReturn.completeDate
                } else {
                    val check = mappedRentals.value!!.keys.find { it.rentalReturnId == item.rentalReturnId }
                    if (check == null) {
                        updateRentalReturn(rentalReturn)
                        item.returnDate = rentalReturn.completeDate
                    }
                }
            }
        }
    }

    override fun onRemoveItemFromReturned(item: OrderRentalItem) {
        val product = mappedReturns.value?.get(item)
        product?.let {
            it.status = RentalStatus.CheckedOut.id
            it.containerBin = null
            insertRental(item, it)
            deleteReturn(item)
        }
    }


    private fun fetchItemsInInspectionBin() {
        addDisposable(rxSingleCreate {
            if (inspectionBin == null) inspectionBin = binService.byBinNumber(INSPECTION)
            orderRentalItemService.loadByStatusAndBin(RentalStatus.Inspection, inspectionBin!!.binId)
        }
                .map { createMapWithRentals(it) }
                .subscribeOn(Schedulers.computation())
                .subscribe({ mappedRentals.postValue(it) }, ::onError))
    }

    private fun createMapWithRentals(items: List<OrderRentalItem>): MutableMap<OrderRentalItem,
            RentalProductItem> {
        val map = mutableMapOf<OrderRentalItem, RentalProductItem>()
        items.forEach {
            rentalProductService.loadItemByUUN(it.uniqueUnitNumber)?.let { product ->
                it.scannedBin = it.rentalProduct.defaultBin
                if (customer == null) map[it] = product
                else if (it.returnCustomerId == customer!!.customerId) {
                    map[it] = product
                }
            }
        }
        return map
    }

    private fun updateRentalReturn(rentalReturn: RentalReturn) {
        rentalReturn.completeDate = DateFormatter.currentTimeDb()
        returnService.update(rentalReturn)
    }

    fun setCustomer(customerId: String?) {
        if (customerId == null) fetchItemsInInspectionBin()
        else {
            customer = customerService.loadById(customerId)
            liveCustomer.value = customer
            fetchItemsInInspectionBin()
        }
    }

    override fun onScanFails() {
        scannedItemNotFound.value = R.string.revision_return_scan_fail_error_message
    }
}

