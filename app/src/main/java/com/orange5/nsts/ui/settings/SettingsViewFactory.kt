package com.orange5.nsts.ui.settings

import android.content.Context
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import com.orange5.nsts.R
import com.orange5.nsts.data.preferencies.RawSharedPreferences
import kotlinx.android.synthetic.main.item_settings_checkbox.view.*

class SettingsViewFactory constructor(
        private val context: Context,
        private val preferences: RawSharedPreferences) {

    var listener: OnSettingClickListener? = null

    fun createSetting(setting: Setting) =
            when (setting.settingType) {
                SettingType.CUSTOM -> createCustomSettings(setting)
                else -> createCheckBoxSettingView(setting)
            }

    fun showOrHideSetting(settingView: View, isShow: Boolean) {
        if (isShow) {
            settingView.visibility =  VISIBLE
        }  else {
            settingView.visibility = GONE
        }
    }

    private fun createCustomSettings(setting: Setting): View {
        val container = View.inflate(context, R.layout.item_settings_custom, null)
        with(container) {
            title.text = context.getString(setting.title)
            description.text = context.getText(setting.description)
        }
        container.setOnClickListener { listener?.onSettingClick(setting, false) }
        if (setting == Setting.SET_UP_STRESS_TEST) {
            showOrHideSetting(container ,preferences.isStressTestEnabled)
        }
        return container
    }

    private fun createCheckBoxSettingView(setting: Setting): View {
        val container = View.inflate(context, R.layout.item_settings_checkbox, null)
        with(container) {
            title.text = context.getString(setting.title)
            description.text = context.getText(setting.description)
            image.isChecked = preferences.isSettingChecked(setting)
            image.setOnCheckedChangeListener { _, isChecked ->
                listener?.onSettingClick(setting, isChecked)
            }
        }

        return container
    }

    interface OnSettingClickListener {
        fun onSettingClick(setting: Setting, checked: Boolean)
    }
}


