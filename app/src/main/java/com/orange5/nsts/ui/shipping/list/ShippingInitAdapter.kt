package com.orange5.nsts.ui.shipping.list

import android.view.View
import android.view.ViewGroup
import com.orange5.nsts.ui.shipping.ShippingProcessAdapter
import com.orange5.nsts.ui.shipping.repository.ReceivingNotice

class ShippingInitAdapter(private val listener: Listener) : ShippingProcessAdapter() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShippingProcessVH =
            ShippingReceivingVH(inflate(parent), listener)

    class ShippingReceivingVH(view: View, listener: Listener) : ShippingProcessVH(view, listener) {
        override fun count(item: ReceivingNotice): Int = item.quantity.remaining
    }
}