package com.orange5.nsts.ui.order.full.pickup.pick.rentals;

import android.app.AlertDialog;
import android.content.Context;
import android.text.SpannableString;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.entities.BinNumber;
import com.orange5.nsts.data.db.entities.OrderRentalItem;
import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.service.bin.BinService;
import com.orange5.nsts.util.Colors;
import com.orange5.nsts.service.rental.RentalItemService;

import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import com.orange5.nsts.util.Spannable;
import com.orange5.nsts.util.view.picker.PickerView;
import com.orange5.nsts.util.view.picker.ValuePickerAdapter;

public class RentalPickDialog {
    private final Context context;
    private OrderItem orderItem;
    private BiConsumer<BinNumber, String> resultConsumer;
    private BinNumber selectedBinNumber;
    private String selectedUniqueUnitNumber;
    private Map<String, Integer> binQuantityMap;

    private PickerView picker1, picker2;
    private TextView text1, text2;
    private Button pickButton;
    private Map<BinNumber, List<String>> allUniqueNumbers;

    public static RentalPickDialog with(Context context) {
        return new RentalPickDialog(context);
    }

    private RentalPickDialog(Context context) {
        this.context = context;
    }


    public RentalPickDialog resultConsumer(BiConsumer<BinNumber, String> resultConsumer) {
        this.resultConsumer = resultConsumer;
        return this;
    }


    public void pick(OrderRentalItem orderItem) {
        this.orderItem = orderItem;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.common_add_rental_n_to_order, orderItem.getProductNumber()));
        View contentView = View.inflate(context, R.layout.rental_pick_dialog, null);
        builder.setView(contentView);
        picker1 = contentView.findViewById(R.id.picker1);
        picker2 = contentView.findViewById(R.id.picker2);
        text1 = contentView.findViewById(R.id.left);
        text2 = contentView.findViewById(R.id.right);
        pickButton = contentView.findViewById(R.id.actionButton);


        List<BinNumber> bins = BinService.getBinsForPickUp(orderItem);

        allUniqueNumbers = RentalItemService.getAllUniqueNumbers(orderItem);

        selectedBinNumber = bins.get(0);
        updateUniqueUnitNumbers(allUniqueNumbers.get(selectedBinNumber));
        new ValuePickerAdapter<>(bins)
                .selectedItemConsumer(item -> {
                    selectedBinNumber = item;
                    updateUniqueUnitNumbers(allUniqueNumbers.get(selectedBinNumber));
                })
                .attachTo(picker1);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        pickButton.setOnClickListener(v -> {
            resultConsumer.accept(selectedBinNumber, selectedUniqueUnitNumber);
            alertDialog.dismiss();
        });
    }

    private void updateUniqueUnitNumbers(List<String> binUniqueUnitNumbers) {
        if (binUniqueUnitNumbers.size() > 0) {
            selectedUniqueUnitNumber = binUniqueUnitNumbers.get(0);
            updateButton();
        }
        new ValuePickerAdapter<>(binUniqueUnitNumbers)
                .selectedItemConsumer(selected -> {
                    selectedUniqueUnitNumber = selected;
                    updateButton();
                })
                .attachTo(picker2);
    }

    private void updateButton() {
        String string = context.getString(R.string.common_pick_uun_from_bin, selectedUniqueUnitNumber, selectedBinNumber.toString());
        SpannableString spannableString = Spannable
                .highlightTextColor(string, Colors.HIGHLIGHT_BLUE, selectedUniqueUnitNumber, selectedBinNumber.getBinNum());
        pickButton.setText(spannableString);
    }


}
