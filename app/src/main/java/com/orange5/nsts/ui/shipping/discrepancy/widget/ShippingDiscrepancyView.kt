package com.orange5.nsts.ui.shipping.discrepancy.widget

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import com.orange5.nsts.R
import com.orange5.nsts.ui.extensions.color
import kotlinx.android.synthetic.main.view_shipping_discrepancy.view.*

class ShippingDiscrepancyView @JvmOverloads constructor(context: Context, attrsSet: AttributeSet? = null,
                                                        defStyleAttr: Int = 0, defStyleRes: Int = 0)
    : LinearLayout(context, attrsSet, defStyleAttr, defStyleRes) {

    init {
        View.inflate(context, R.layout.view_shipping_discrepancy, this)
        quantity.doOnTextChanged { _, _, _, _ -> clearError() }
    }

    private fun clearError() {
        error.isVisible = false
        quantity.backgroundTintList = ColorStateList.valueOf(context.color(R.color.primary_dark))
    }

    fun setDiscrepancyText(@StringRes resId: Int) {
        discrepancyQty.text = resources.getString(resId)
    }

    fun setOnActionClick(action: (count: Int, notes: String) -> Unit) {
        actionButton.setOnClickListener {
            val value = quantity.text.toString()
            action(if (value.isEmpty()) 0 else value.toInt(), notes.text.toString())
        }
    }

    fun showCountError(validCount: Int) {
        quantity.backgroundTintList = ColorStateList.valueOf(context.color(R.color.errorRed))
        error.isVisible = true
        error.text = resources.getString(R.string.receiving_shortage_error, validCount)
    }

    fun notDiscrepancyAvailable() {
        emptyText.isVisible = true
        actionButton.isEnabled = false
        quantityContainer.isVisible = false
        notes.isVisible = false
    }

    fun setQuantity(quantity: Int) {
        this.quantity.setText(quantity.toString())
    }

    fun setNotes(notes: String) {
        this.notes.setText(notes)
    }

    fun isRental(rental: Boolean) {
        quantityContainer.isVisible = !rental
    }

    fun setActionButtonTitle(@StringRes title: Int) {
        actionButton.text = resources.getString(title)
    }
}