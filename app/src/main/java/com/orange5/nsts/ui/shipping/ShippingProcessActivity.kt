package com.orange5.nsts.ui.shipping

import android.graphics.Bitmap
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.observe
import androidx.viewpager.widget.ViewPager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseActivity
import com.orange5.nsts.ui.base.ThreePageAdapter.TabType.*
import com.orange5.nsts.ui.extensions.showDialog
import com.orange5.nsts.ui.extensions.toast
import com.orange5.nsts.ui.home.BottomMenuDialog
import com.orange5.nsts.ui.home.BottomMenuDialog.BottomType.MENU_SHIPPING
import com.orange5.nsts.ui.returns.signature.FinishReturnDialog
import com.orange5.nsts.ui.returns.signature.OnFinishListener
import kotlinx.android.synthetic.main.activity_return.*

abstract class ShippingProcessActivity : BaseActivity(),
    BottomNavigationView.OnNavigationItemSelectedListener,
    FinishReturnDialog.Listener,
    BottomMenuDialog.Listener,
    OnFinishListener {

    protected var dialog: AlertDialog? = null

    var bottomMenuDialog: BottomMenuDialog? = null

    protected abstract val viewModel: ShippingProcessViewModel

    protected abstract val adapter: FragmentStatePagerAdapter

    override fun getLayoutResourceId(): Int = R.layout.activity_shipping_process

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.receivingBadges.observe(this, ::setResultBadge)
        viewModel.discrepancyBadges.observe(this, ::setDiscrepancyBadge)
        viewModel.liveRunSync.observe(this) { runSync() }
        viewModel.onEmptyProcessFinished.observe(this) { finishReturn() }
        bottomNavigation.setOnNavigationItemSelectedListener(this)
        setBottomNavItemsTitle()
        setUpViewPager()
    }

    private fun setUpViewPager() {
        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) = Unit
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) = Unit

            override fun onPageSelected(position: Int) {
                bottomNavigation.selectedItemId =
                    when (position) {
                        0 -> R.id.initItem
                        1 -> R.id.intermediateItem
                        else -> R.id.resultItem
                    }
            }
        })
    }

    protected abstract fun setBottomNavItemsTitle()

    protected fun onFinishReceiving() {
        finish()
        runSync()
    }

    override fun onGetSignature(signature: Bitmap) {
        viewModel.finishReceivingProcess()
    }

    override fun onFinishProcessClick() {
        viewModel.onFinishProcessClick()
    }

    protected open fun nothingToProcessMessage() = R.string.shipping_notice_finish

    private fun setResultBadge(count: Int?) {
        if (count != null && count > 0) bottomNavigation.getOrCreateBadge(R.id.resultItem).number = count
        else bottomNavigation.removeBadge(R.id.resultItem)
    }

    private fun setDiscrepancyBadge(count: Int?) {
        if (count != null && count > 0) bottomNavigation.getOrCreateBadge(R.id.intermediateItem).number = count
        else bottomNavigation.removeBadge(R.id.intermediateItem)
    }

    override fun onBackPressed() {
        if (viewModel.receivingItems.value.isNullOrEmpty()) {
            finish()
            return
        }

        if (bottomMenuDialog == null || bottomMenuDialog?.isVisible == false) {
            bottomMenuDialog = BottomMenuDialog.show(supportFragmentManager, MENU_SHIPPING)
            return
        }
        super.onBackPressed()
    }

    override fun onBottomButtonClick() {
        finishReturn()
    }

    override fun onBackButtonClick() {
        this.toast(R.string.shipping_notice_has_been_canceled)
    }

    protected fun finishReturn() {
        finish()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.initItem -> viewPager.setCurrentItem(INIT.id, true)
            R.id.intermediateItem -> viewPager.setCurrentItem(INTERMEDIATE.id, true)
            R.id.resultItem -> viewPager.setCurrentItem(RESULT.id, true)
        }
        return true
    }
}
