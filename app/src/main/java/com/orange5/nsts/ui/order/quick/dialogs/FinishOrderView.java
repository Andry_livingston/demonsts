package com.orange5.nsts.ui.order.quick.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Pair;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.orange5.nsts.R;
import com.orange5.nsts.app.App;
import com.orange5.nsts.data.db.dao.CustomerDao;
import com.orange5.nsts.data.db.entities.Customer;
import com.orange5.nsts.data.db.entities.Order;
import com.orange5.nsts.service.order.OrderStatus;
import com.orange5.nsts.service.order.OrderTotalsUtils;
import com.orange5.nsts.service.order.OrderType;
import com.orange5.nsts.util.Bitmaps;
import com.orange5.nsts.util.format.CurrencyFormatter;
import com.orange5.nsts.util.format.DateFormatter;
import com.orange5.nsts.util.view.keyvalue.KeyValueComplexView;
import com.orange5.nsts.util.view.keyvalue.KeyValueComplexView.PickedDetails;
import com.orange5.nsts.util.view.keyvalue.KeyValueRowView;

import java.util.List;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.orange5.nsts.util.SignatureManager.getConfigSizeBitmap;

public class FinishOrderView extends FrameLayout {

    private Order order;
    private OrderTotalsUtils totalsUtils;

    @BindView(R.id.customerNameText)
    TextView customerNameText;

    @BindView(R.id.changeCustomerText)
    TextView changeCustomerText;

    @BindView(R.id.detailsRowsContainer)
    LinearLayout detailsRowsContainer;

    @BindView(R.id.signaturePad)
    SignaturePad signaturePad;

    @OnClick(R.id.clearSignature)
    void clearSignature() {
        signaturePad.clear();
    }

    @OnClick(R.id.changeCustomerText)
    void selectNewCustomer() {
        CustomerDao customerDao = App.getInstance().getDaoSession().getCustomerDao();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle(R.string.finish_order_view_select_pick_customer);


        List<Customer> customers = customerDao.loadAll();
        List<String> customerNames = customers.stream().map(Customer::toString).collect(Collectors.toList());

        alertDialogBuilder.setItems(customerNames.toArray(new String[]{}), (dialog, which) -> {
            Customer customer = customers.get(which);
            order.setPickCustomer(customer);
            order.update();
            updateCustomerName();
        });
        alertDialogBuilder.show();
    }

    private boolean initialized = false;

    public FinishOrderView(@NonNull Context context) {
        super(context);
    }

    public FinishOrderView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public FinishOrderView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public FinishOrderView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void initWithOrder(OrderType type, Order order) {
        if (initialized) {
            throw new IllegalStateException("Already initialized");
        }
        initialized = true;
        this.order = order;
        inflate(getContext(), R.layout.view_order_finish, this);
        ButterKnife.bind(this);
        totalsUtils = new OrderTotalsUtils(order);

        if (OrderStatus.closed.matches(order) || OrderStatus.readyNotShipped.matches(order)) {
            initWithClosedOrder(order);
        } else if (type == OrderType.QUICK_ORDER) {
            initWithOpenOrder(order, totalsUtils.getRentalSize(), totalsUtils.getConsumableSize());
        } else {
            initWithOpenOrder(order, totalsUtils.getRentalPicked(), totalsUtils.getConsumablePicked());
        }
    }

    private void initWithOpenOrder(Order order, int rentals, int consumables) {
        addDetail(R.string.finish_order_view_order_nr, order.getOrderNumber());
        addDetail(R.string.common_rentals, String.valueOf(rentals));
        addDetail(R.string.common_consumables, String.valueOf(consumables));
        addDetail(R.string.finish_order_view_total, CurrencyFormatter.Companion.formatPrice(order.getOrderSum()));
        addDetail(R.string.selected_customer_activity_accounting_code, order.getAccountingCode());
        addDetail(R.string.selected_customer_activity_web_order_number, order.getWebOrderNumber());
        updateCustomerName();

        signaturePad.setVisibility(
                OrderStatus.open.matches(order)
                        || OrderStatus.pickUp.matches(order) ? VISIBLE : GONE);
    }

    public void initWithClosedOrder(Order order) {
        Bitmap signature = Bitmaps.fromBytes(order.getSignImage());
        if (signature != null) {
            signaturePad.setSignatureBitmap(signature);
        }
        signaturePad.setEnabled(false);

        addDetail(R.string.finish_order_view_created_at, DateFormatter.convertDbDateToUI(order.getOrderDate()));
        addDetail(R.string.finish_order_view_closed_at, DateFormatter.convertDbDateToUI(order.getOrderCompleteDate()));

        Customer pickCustomer = order.getPickCustomer();
        if (pickCustomer == null) {
            pickCustomer = order.getCustomer();
        }

        fillCreatedAndPicketCustomer(order, pickCustomer);

        addDetail(R.string.finish_order_view_total, CurrencyFormatter.Companion.formatPrice(order.getOrderSum()));

        addProductPickedInfo(new PickedDetails(getContext().getString(R.string.finish_order_view_products),
                totalsUtils.getTotalItemsSize(),
                totalsUtils.getPickedSize(),
                totalsUtils.getUnpickedSize()));

        addProductPickedInfo(new PickedDetails(getContext().getString(R.string.common_rentals),
                totalsUtils.getRentalSize(),
                totalsUtils.getRentalPicked(),
                totalsUtils.getUnpickedRental()));
        addProductPickedInfo(new PickedDetails(getContext().getString(R.string.common_consumables),
                totalsUtils.getConsumableSize(),
                totalsUtils.getConsumablePicked(),
                totalsUtils.getUnpickedConsumable()));

        findViewById(R.id.changeCustomerText).setVisibility(GONE);
        findViewById(R.id.clearSignature).setVisibility(GONE);

    }

    private void fillCreatedAndPicketCustomer(Order order, Customer pickCustomer) {
        if ( pickCustomer.getCustomerId().equals(order.getCreatorCustomerId())) {
            addDetail(R.string.finish_order_view_created_picked_by, order.getCustomer().toString());
        } else {
            addDetail(R.string.finish_order_view_created_by, order.getCustomer().toString());
            addDetail(R.string.finish_order_view_picked_by, pickCustomer.toString());
        }
    }

    private void addDetail(int keyResourceId, String value) {
        if (TextUtils.isEmpty(value)) {
            return;
        }
        String key = getContext().getString(keyResourceId);
        KeyValueRowView rowView = new KeyValueRowView(getContext());
        rowView.fill(new Pair<>(key, value));
        rowView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        detailsRowsContainer.addView(rowView);
    }

    private void addProductPickedInfo(PickedDetails pickedDetails) {
        if (pickedDetails.getTotal() <= 0) return;
        KeyValueComplexView view = new KeyValueComplexView(getContext());
        view.fill(pickedDetails);
        view.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        detailsRowsContainer.addView(view);
    }

    public Bitmap getSignatureBitmap() {
        return getConfigSizeBitmap(signaturePad.getSignatureBitmap());
    }

    private void updateCustomerName() {
        StringBuilder text = new StringBuilder();
        if (order.getPickCustomer() != null) {
            text.append(order.getCustomer().toString())
                    .append("\n")
                    .append(order.getPickCustomer().toString());
        } else {
            text.append(order.getCustomer().toString());
        }
        customerNameText.setText(text.toString());
    }

}
