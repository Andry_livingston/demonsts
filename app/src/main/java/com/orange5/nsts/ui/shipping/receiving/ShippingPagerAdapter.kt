package com.orange5.nsts.ui.shipping.receiving

import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.viewpager.widget.PagerAdapter

class ShippingPagerAdapter : PagerAdapter() {

    private var items = listOf<Item>()

    fun setItems(items: List<Item>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return items[position].tabTitle
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = items[position].view
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object` as View

    override fun getCount(): Int = items.size

    data class Item(val view: View,
                    val tabTitle: String = "")
}