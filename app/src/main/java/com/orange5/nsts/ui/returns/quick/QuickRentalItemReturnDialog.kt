package com.orange5.nsts.ui.returns.quick

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.BinNumber
import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.service.rental.RentalStatus
import com.orange5.nsts.ui.base.OnClosedDialogListener
import com.orange5.nsts.ui.base.TwoPageAdapter.TabType
import com.orange5.nsts.ui.returns.BaseReturnDialog

class QuickRentalItemReturnDialog : BaseReturnDialog() {

    override val viewModel: QuickReturnViewModel by activityViewModels { factory }

    override val args: Bundle? by lazy { arguments }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        closeCallback = targetFragment as? OnClosedDialogListener
    }

    override fun getLayoutResourceId(): Int = R.layout.dialog_rental_return

    override val notFoundMessage = R.string.rental_return_activity_scan_bin

    companion object {
        fun show(fragment: Fragment, item: OrderRentalItem, type: TabType, wasScanned: Boolean = false) =
                QuickRentalItemReturnDialog()
                        .apply {
                            arguments = Bundle().also {
                                it.putString(ORDER_ITEM_ID, item.orderItemId)
                                it.putBoolean(WAS_SCANNED_ID, wasScanned)
                                it.putSerializable(TYPE_ID, type)
                            }
                            setTargetFragment(fragment, 999)
                            show(fragment.parentFragmentManager, this::class.java.simpleName)
                        }
    }
}

data class ReturnInformation(val returnBin: BinNumber?,
                             val status: RentalStatus,
                             val comments: String)