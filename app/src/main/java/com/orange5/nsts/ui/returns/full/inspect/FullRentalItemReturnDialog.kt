package com.orange5.nsts.ui.returns.full.inspect

import androidx.lifecycle.Observer
import android.os.Bundle
import androidx.annotation.StringRes
import android.view.View
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.BinNumber
import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.ui.base.BaseDialogFragment
import com.orange5.nsts.ui.base.TwoPageAdapter.TabType
import com.orange5.nsts.ui.base.TwoPageAdapter.TabType.INIT
import kotlinx.android.synthetic.main.dialog_full_return.*

class FullRentalItemReturnDialog : BaseDialogFragment() {

    val viewModel: FullReturnViewModel by activityViewModels { factory }

    private var type = INIT
    private var inspectionBin: BinNumber? = null

    override fun getLayoutResourceId(): Int = R.layout.dialog_full_return

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.orderItem.observe(this, Observer(::setUpView))
        viewModel.liveBins.observe(this, Observer(::setInspectionBin))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val itemId = arguments?.getString(ORDER_ITEM_ID) ?: "layout_dummy"
        type = arguments?.getSerializable(TYPE_ID) as? TabType ?: INIT
        viewModel.loadOrderItem(itemId, type)
        handleType(type)
        setListeners()
    }

    private fun handleType(type: TabType) =
            when (type) {
                INIT -> ok.text = getString(R.string.common_return)
                TabType.RESULT -> ok.text = getString(R.string.update)
            }

    private fun setListeners() {
        cancel.setOnClickListener { dismiss() }
        ok.setOnClickListener {
            if (inspectionBin == null) errorOccur(R.string.rental_return_error_inspection_bin)
            else {
                val info = ReturnInformation(inspectionBin!!, wasDamaged.isChecked, remark.text
                        .toString())
                if (type == INIT) viewModel.onItemReturned(info)
                else viewModel.onUpdateReturnedItem(info)
            }
            dismiss()
        }
    }

    private fun setUpView(item: OrderRentalItem?) {
        item?.let {
            productNumber.setValueText(it.productNumber)
            uun.setValueText(it.uniqueUnitNumber)
            remark.setText(it.rentalReturnNotes)
            wasDamaged.isChecked = it.wasDamaged
        } ?: errorOccur(R.string.rental_return_order_item_does_not_exists)
    }

    private fun errorOccur(@StringRes errorMessage: Int) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
        dismiss()
    }

    private fun setInspectionBin(bins: List<BinNumber>?) {
        if (!bins.isNullOrEmpty()) {
            inspectionBin = bins.find { it.binId == INSPECTION_BIN }
        }
    }

    companion object {
        fun newInstance(item: OrderRentalItem, type: TabType, wasScanned: Boolean = false) = FullRentalItemReturnDialog()
                .apply {
                    arguments = Bundle().also {
                        it.putString(ORDER_ITEM_ID, item.orderItemId)
                        it.putBoolean(WAS_SCANNED_ID, wasScanned)
                        it.putSerializable(TYPE_ID, type)
                    }
                }

        private const val ORDER_ITEM_ID = "order_item_id_key"
        private const val WAS_SCANNED_ID = "was_scanned_key"
        private const val TYPE_ID = "return_tab_type_key"
        const val INSPECTION_BIN = "aa45668b-69fa-4979-a931-be6cf39a0a37"
    }
}

data class ReturnInformation(val bin: BinNumber, val wasDamaged: Boolean, val comments: String)