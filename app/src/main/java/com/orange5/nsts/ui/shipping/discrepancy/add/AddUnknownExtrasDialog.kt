package com.orange5.nsts.ui.shipping.discrepancy.add

import android.os.Bundle
import android.view.View
import android.view.WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseDialogFragment
import com.orange5.nsts.ui.extensions.setOnClickListener
import com.orange5.nsts.ui.shipping.ShippingProcessViewModel
import com.orange5.nsts.ui.shipping.receiving.ReceivingViewModel
import com.orange5.nsts.ui.shipping.takeaway.TakeAwayViewModel
import kotlinx.android.synthetic.main.dialog_add_unknown_extras.*

abstract class AddUnknownExtrasDialog : BaseDialogFragment() {

    abstract val viewModel: ShippingProcessViewModel

    override fun getLayoutResourceId(): Int = R.layout.dialog_add_unknown_extras

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.window?.setSoftInputMode(SOFT_INPUT_STATE_VISIBLE);
        description.requestFocus()
        increment.setOnClickListener { updateCount(1) }
        decrement.setOnClickListener { updateCount(-1) }
        cancel.setOnClickListener(::dismiss)
        addDiscrepancy.setOnClickListener(::onAddDiscrepancyClick)
        description.doOnTextChanged { _, _, _, _ -> clearError() }
    }

    private fun updateCount(count: Int) {
        val updated = quantity.text.toString().toInt() + count
        quantity.setText((if (updated < 1) 1 else updated).toString())
    }

    private fun onAddDiscrepancyClick() {
        val description = description.text.toString()
        if (description.isEmpty()) showError()
        else {
            val count = quantity.text.toString().toInt()
            val notes = notes.text.toString()
            viewModel.addUnknownExtras(count, description, notes)
            dismiss()
        }
    }

    private fun showError() {
        descriptionLayout.error = getString(R.string.discrepancy_description_is_required)
    }

    private fun clearError() {
        descriptionLayout.error = null
    }

    companion object {
        @JvmStatic
        fun fromReceiving(fragmentManager: FragmentManager) = show(ReceivingAddUnknownExtrasDialog(), fragmentManager)

        @JvmStatic
        fun fromTakeAway(fragmentManager: FragmentManager) = show(TakeAwayAddUnknownExtrasDialog(), fragmentManager)

        private fun show(dialog: AddUnknownExtrasDialog, fragmentManager: FragmentManager) =
                dialog.show(fragmentManager, this::class.java.simpleName)
    }

}

class ReceivingAddUnknownExtrasDialog : AddUnknownExtrasDialog() {
    override val viewModel by activityViewModels<ReceivingViewModel> { factory }
}

class TakeAwayAddUnknownExtrasDialog : AddUnknownExtrasDialog() {
    override val viewModel by activityViewModels<TakeAwayViewModel> { factory }
}