package com.orange5.nsts.ui.settings

import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.lifecycle.observe
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseActivity
import com.orange5.nsts.ui.shipping.discrepancy.list.DiscrepancyListActivity
import com.orange5.nsts.ui.extensions.showDialog
import com.orange5.nsts.ui.load.InitLoadActivity
import com.orange5.nsts.ui.settings.Setting.*
import com.orange5.nsts.ui.stresstesting.StressTestActivity
import com.orange5.nsts.util.view.ApiSettingsDialog
import kotlinx.android.synthetic.main.activity_settings.*
import java.util.function.Consumer
import javax.inject.Inject

class SettingsActivity : BaseActivity(), SettingsViewFactory.OnSettingClickListener {

    @Inject
    lateinit var factory: SettingsViewFactory

    private val viewModel: SettingsViewModel by viewModels { vmFactory }

    override fun getLayoutResourceId(): Int = R.layout.activity_settings

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.ipChangeData.observe(this, ::requestChangeIp)
        viewModel.liveError.observe(this, ::errorSnack)
        viewModel.liveProgress.observe(this, ::loading)
        viewModel.onDataBaseExported.observe(this) { errorSnack(R.string.setting_db_successfully_exported) }
        viewModel.syncDataCleared.observe(this, ::errorSnack)
        viewModel.existDataForSync.observe(this) { showNotSyncedDataExists() }
        viewModel.enabledStressTest.observe(this, ::showSetUpStressTest)

        factory.listener = this
        setSettingsFields(factory)
    }

    private fun setSettingsFields(factory: SettingsViewFactory) {
        if (viewModel.isUserAdministrator()) values().forEach {
            settingsContainer.addView(factory.createSetting(it))
        } else settingsContainer.addView(factory.createSetting(VIBRATION_ENABLED))
    }

    override fun onSettingClick(setting: Setting, checked: Boolean) {
        when (setting) {
            CHANGE_IP -> viewModel.onChangeIPClick()
            EXPORT_DB_FILE -> viewModel.exportDB()
            RUN_INIT_SYNC -> onRunInitLoad()
            CLEAR_SYNC_DATA -> viewModel.onClearSyncData()
            ACCOUNTING_CODE_REQUIRED -> viewModel.changeRequiredAccountingCode(checked)
            HIDE_QUICK_ORDER -> viewModel.hideQuickOrder(checked)
            HIDE_QUICK_RETURN -> viewModel.hideQuickReturn(checked)
            VIBRATION_ENABLED -> viewModel.enableVibration(checked)
            DISCREPANCIES -> startActivity(DiscrepancyListActivity.newIntent(this))
            STRESS_TES_ENABLED -> viewModel.enableStressTest(checked)
            SET_UP_STRESS_TEST -> startActivity(StressTestActivity.newIntent(this))
        }
    }

    private fun onRunInitLoad() {
        this.showDialog(
                title = getString(R.string.app_name),
                message = getString(R.string.settings_init_load_warning),
                positive = getString(android.R.string.ok),
                negative = getString(android.R.string.cancel),
                positiveClick = { startActivity(InitLoadActivity.newIntent(this)) }
        )
    }

    private fun showNotSyncedDataExists() {
        this.showDialog(
                title = getString(R.string.app_name),
                message = getString(R.string.settings_clear_sync_warning),
                positive = getString(android.R.string.ok),
                negative = getString(android.R.string.cancel),
                positiveClick = { viewModel.clearSyncData() }
        )
    }

    private fun requestChangeIp(host: String) {
        val dialog = ApiSettingsDialog(this, false, host, Consumer { viewModel.rebuildSyncService(it) })
        dialog.show()
    }

    private fun showSetUpStressTest(isShow: Boolean) {
        factory.showOrHideSetting(settingsContainer.getChildAt(values().lastIndex),isShow)
    }

    private fun loading(show: Boolean?) {
        progress.isVisible = show ?: false
    }
}