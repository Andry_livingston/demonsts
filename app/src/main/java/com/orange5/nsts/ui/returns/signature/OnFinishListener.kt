package com.orange5.nsts.ui.returns.signature

interface OnFinishListener {
    fun onFinishProcessClick()
}