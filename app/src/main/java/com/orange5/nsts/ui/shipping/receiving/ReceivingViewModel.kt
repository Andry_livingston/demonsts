package com.orange5.nsts.ui.shipping.receiving

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.orange5.nsts.R
import com.orange5.nsts.service.bin.BinService
import com.orange5.nsts.ui.base.ActionLiveData
import com.orange5.nsts.ui.shipping.ShippingProcessViewModel
import com.orange5.nsts.ui.shipping.discrepancy.repository.DiscrepancyRepository
import com.orange5.nsts.ui.shipping.repository.ReceivingNotice
import com.orange5.nsts.ui.shipping.repository.ReceivingRepository
import javax.inject.Inject

class ReceivingViewModel @Inject constructor(
        private val receivingRepository: ReceivingRepository,
        private val binService: BinService,
        private val discrepancyRepository: DiscrepancyRepository) : ShippingProcessViewModel(discrepancyRepository) {

    override val receivingBadges: LiveData<Int> = receivingItems.map { it.sumBy { notice -> notice.quantity.processed } }
    val onFinishReceivingNotice: MutableLiveData<Unit> = ActionLiveData()

    override fun finishReceivingProcess() {
        liveProgress.value = true
        runJobIO({
            receivingItems.value?.let { receivingRepository.processProductsToReceiving(it) }
            processDiscrepancy()
        }, { onFinishReceivingNotice.postValue(Unit) })
    }

    private fun processDiscrepancy() {
        val items = discrepancyItems.value ?: return
        if (items.isNotEmpty()) discrepancyRepository.saveDiscrepancy(items)
    }

    fun loadASNItems(ASNNumber: String) {
        transferNo = ASNNumber
        liveProgress.value = true
        callJobComputation(
                { receivingRepository.byTransferNumber(ASNNumber) },
                { receivingItems.postValue(it) })
    }

    fun receiveItem(count: Int) {
        receivingNotice.value?.processItems(count)
        refresh()
    }

    fun returnItem(count: Int) {
        receivingNotice.value?.returnItems(count)
        refresh()
    }

    private fun processedNotice(item: ReceivingNotice) {
        item.processItems(item.quantity.imported)
        item.setBin(binService.receivingBin())
        refresh()
    }

    fun returnRental(item: ReceivingNotice) {
        item.returned(1)
        item.isProcessed(false)
        item.setBin(null)
        refresh()
    }

    fun onBarcodeScanned(code: String) {
        val scanned = mutableListOf<ReceivingNotice>()
        receivingItems.value?.forEach { notice ->
            if ((notice.uniqueUnitNumber == code || notice.productNumber == code)
                    && notice.quantity.remaining > 0) scanned.add(notice)
        }
        if (scanned.isEmpty()) onScanFails(R.string.receiving_scanning_consumable_item_not_found)
        else scanned.forEach(::processedNotice)
    }
}