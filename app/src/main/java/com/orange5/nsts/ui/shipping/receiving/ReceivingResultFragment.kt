package com.orange5.nsts.ui.shipping.receiving

import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseListFragment
import com.orange5.nsts.ui.returns.signature.OnFinishListener
import com.orange5.nsts.ui.shipping.ShippingProcessAdapter
import com.orange5.nsts.ui.shipping.list.ShippingResultAdapter
import com.orange5.nsts.ui.shipping.repository.ReceivingNotice
import kotlinx.android.synthetic.main.fragment_refreshed_list.*

class ReceivingResultFragment : BaseListFragment(), ShippingProcessAdapter.Listener {

    private var onFinishListener: OnFinishListener? = null
    private val adapter = ShippingResultAdapter(this)
    private val viewModel: ReceivingViewModel by activityViewModels { vmFactory }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onFinishListener = (context as OnFinishListener)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override val noDataMessage = R.string.receiving_bin_empty_data_message

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.receivingItems.safeObserve(::submitList)
        recyclerView.adapter = adapter
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as? ReceivingActivity)?.setTitle(R.string.receiving_result_item_title)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) = inflater.inflate(R.menu.menu_finish, menu)

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
            when (item.itemId) {
                R.id.finish -> {
                    onFinishListener?.onFinishProcessClick()
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    override fun onItemClick(item: ReceivingNotice) {
        if (item.isRental()) viewModel.returnRental(item)
        else ReceivingConsumableDialog.consumeReceiving(parentFragmentManager, item.id, item.quantity.processed)
    }

    private fun submitList(items: List<ReceivingNotice>) {
        val filtered = items.filter { it.quantity.processed != 0 }
        noData.isVisible = filtered.isEmpty()
        adapter.submitList(filtered)
    }
}