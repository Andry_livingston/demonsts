package com.orange5.nsts.ui.shipping.discrepancy.add

import android.os.Bundle
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.observe
import com.orange5.nsts.R
import com.orange5.nsts.scan.ScanningDialog
import com.orange5.nsts.ui.extensions.setOnClickListener
import com.orange5.nsts.ui.shipping.ShippingProcessViewModel
import com.orange5.nsts.ui.shipping.discrepancy.OnAddDiscrepancyClickListener
import com.orange5.nsts.ui.shipping.discrepancy.repository.Discrepancy.Type
import com.orange5.nsts.ui.shipping.receiving.ReceivingViewModel
import com.orange5.nsts.ui.shipping.repository.ReceivingNotice
import com.orange5.nsts.ui.shipping.takeaway.TakeAwayViewModel
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_choose_item.*

abstract class AddDiscrepancyDialog : ScanningDialog(), SearchView.OnQueryTextListener, AddDiscrepancyAdapter.Listener {

    abstract val viewModel: ShippingProcessViewModel
    private val searchSubject = PublishSubject.create<String>()
    private var query: String = ""

    override fun getLayoutResourceId() = R.layout.fragment_choose_item

    override val notFoundMessage: Int = R.string.shipping_notice_scanned_item_do_not_exist

    private val adapter = AddDiscrepancyAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.addSearchObservable(searchSubject)
        viewModel.receivingItems.observe(this, ::submitList)
        viewModel.searchedItems.observe(this, ::submitList)
        viewModel.scannedItemNotFound.observe(this, ::showErrorToast)
        scanningViewModel.barcodeValue.observe(this, ::onBarcodeScanned)
    }

    override fun onStart() {
        super.onStart()
        dialog?.let { it.window?.setLayout(MATCH_PARENT, MATCH_PARENT) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cancel.setOnClickListener(::dismiss)
        emptyText.text = getString(R.string.discrepancy_no_shortages_available_search_view)
        search.setOnQueryTextListener(this)
        list.adapter = adapter
    }

    override fun onQueryTextSubmit(query: String): Boolean = false

    override fun onQueryTextChange(newText: String): Boolean {
        query = newText
        searchSubject.onNext(query)
        return false
    }

    private fun submitList(items: List<ReceivingNotice>) {
        val receiving = items.filter { it.quantity.remaining > 0 }
        val isEmptyList = receiving.isEmpty()
        emptyText.isVisible = isEmptyList
        list.isVisible = !isEmptyList
        if (!isEmptyList) adapter.setList(receiving, query)
    }

    override fun onItemClick(item: ReceivingNotice) {
        val type = arguments?.getSerializable(DISCREPANCY_TYPE_KEY) as? Type ?: Type.SHIPMENT_SHORTAGE
        (targetFragment as? OnAddDiscrepancyClickListener)?.onAddDiscrepancyClick(item, type)
        dismiss()
    }

    private fun onBarcodeScanned(barcode: String) {
        val type = arguments?.getSerializable(DISCREPANCY_TYPE_KEY) as? Type ?: Type.SHIPMENT_SHORTAGE
        viewModel.onDiscrepancyScanned(barcode, type)
    }

    companion object {
        @JvmStatic
        fun fromReceiving(fragment: Fragment, type: Type) = show(ReceivingAddDiscrepancyDialog(), fragment, type)

        @JvmStatic
        fun fromTakeAway(fragment: Fragment, type: Type) = show(TakeAwayAddDiscrepancyDialog(), fragment, type)

        private fun show(dialog: AddDiscrepancyDialog, fragment: Fragment, type: Type): AddDiscrepancyDialog {
            dialog.arguments = bundleOf(DISCREPANCY_TYPE_KEY to type)
            dialog.setTargetFragment(fragment, 900)
            dialog.show(fragment.parentFragmentManager, this::class.java.simpleName)
            return dialog
        }

        const val DISCREPANCY_TYPE_KEY = "discrepancy_type_key"
    }
}

class ReceivingAddDiscrepancyDialog : AddDiscrepancyDialog() {
    override val viewModel by activityViewModels<ReceivingViewModel> { factory }
}

class TakeAwayAddDiscrepancyDialog : AddDiscrepancyDialog() {
    override val viewModel by activityViewModels<TakeAwayViewModel> { factory }
}