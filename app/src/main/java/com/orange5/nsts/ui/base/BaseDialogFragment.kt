package com.orange5.nsts.ui.base

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import butterknife.ButterKnife
import com.orange5.nsts.R
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

abstract class BaseDialogFragment : DialogFragment(), HasAndroidInjector {

    @Inject
    lateinit var injector: DispatchingAndroidInjector<Any>
    @Inject
    lateinit var factory: ViewModelProvider.Factory

    protected var closeCallback: OnClosedDialogListener? = null

    override fun androidInjector(): AndroidInjector<Any> = injector

    protected abstract fun getLayoutResourceId(): Int

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.Theme_AppCompat_Light_Dialog_Alert)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val layoutResourceId = if (getLayoutResourceId() == 0) R.layout.layout_dummy else getLayoutResourceId()
        val result = inflater.inflate(layoutResourceId, container, false)
        ButterKnife.bind(this, result)
        return result
    }

    protected fun <T> LiveData<T>.observe(consumer: (T) -> Unit) =
            observe(viewLifecycleOwner, Observer { consumer(it) })

    protected fun <T> LiveData<T?>.safeObserve(consumer: (T) -> Unit) =
            observe(viewLifecycleOwner, Observer { it?.let(consumer) })

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        closeCallback?.onDialogClosed()
    }
}

interface OnClosedDialogListener {
    fun onDialogClosed()
}