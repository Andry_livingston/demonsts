package com.orange5.nsts.ui.shipping.takeaway

import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.View
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.os.bundleOf
import androidx.core.text.bold
import androidx.core.text.color
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.observe
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseDialogFragment
import com.orange5.nsts.ui.base.picker.Item
import com.orange5.nsts.ui.base.picker.NumberPickerAdapter
import com.orange5.nsts.ui.extensions.color
import com.orange5.nsts.ui.extensions.setOnClickListener
import com.orange5.nsts.ui.shipping.repository.ReceivingNotice
import kotlinx.android.synthetic.main.dialog_take_away_return_consumable.*
import javax.inject.Inject

class TakeAwayReturnConsumableDialog : BaseDialogFragment() {

    val viewModel: TakeAwayViewModel by activityViewModels { factory }

    @Inject
    lateinit var noticeAdapter: NumberPickerAdapter
    private var selectedQuantity: Int = 0

    override fun getLayoutResourceId(): Int = R.layout.dialog_take_away_return_consumable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.processedProduct.observe(this, ::submitList)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cancel.setOnClickListener(::dismiss)
        noticeAdapter.action = ::onQuantitySelected
        picker.setAdapter(noticeAdapter)
        val id = arguments?.getString(KEY_ID) ?: ""
        viewModel.loadProcessedProduct(id)
        actionButton.setOnClickListener(::onActionButtonClick)
    }

    private fun onQuantitySelected(position: Item<Int>) {
        selectedQuantity = position.value
        updateActionButtonText()
    }

    private fun onActionButtonClick() {
        viewModel.onReturnConsumable(selectedQuantity)
        dismiss()
    }

    private fun submitList(notice: ReceivingNotice) {
        productNumber.setValueText(notice.productNumber)

        val list = mutableListOf<Int>()
        repeat(notice.quantity.imported) { list.add((it + 1)) }
        noticeAdapter.submitItems(list)

        picker.selectedItemPosition = 0
        updateActionButtonText()
    }

    private fun updateActionButtonText() {
        val text = SpannableStringBuilder(getString(R.string.take_away_action_return))
                .append(SPACE)
                .bold { color(requireContext().color(R.color.blue)) { append(selectedQuantity.toString()) } }
                .append(SPACE).append(getString(R.string.take_away_action_in_receiving_bin))
        actionButton.text = text
    }

    private fun errorOccur(@StringRes errorMessage: Int) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
        dismiss()
    }

    companion object {
        fun show(fragment: Fragment, id: String, wasScanned: Boolean = false) =
                TakeAwayReturnConsumableDialog().apply {
                    arguments = bundleOf(KEY_ID to id,
                            WAS_SCANNED_ID to wasScanned)
                    show(fragment.parentFragmentManager, this::class.java.simpleName)
                }

        private const val KEY_ID = "id_key"
        private const val WAS_SCANNED_ID = "was_scanned_key"
        private const val SPACE = " "

    }
}