package com.orange5.nsts.ui.returns.full.inspect

import androidx.fragment.app.activityViewModels
import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.ui.base.TwoPageAdapter
import com.orange5.nsts.ui.returns.ReturnedItemsFragment

class FullReturnedItemsFragment : ReturnedItemsFragment() {

    override val viewModel: FullReturnViewModel by activityViewModels { vmFactory }

    override fun onItemClick(item: OrderRentalItem) {
        FullRentalItemReturnDialog.newInstance(item, TwoPageAdapter.TabType.RESULT).show(requireFragmentManager(),
                FullRentalItemReturnDialog::class.java.simpleName)
    }
}