package com.orange5.nsts.ui.shipping.repository

import com.orange5.nsts.data.db.entities.ConsumableBin
import com.orange5.nsts.data.db.entities.ConsumableProduct
import com.orange5.nsts.data.db.entities.ProductReceivingLog
import com.orange5.nsts.data.db.entities.RentalProduct
import com.orange5.nsts.data.db.entities.RentalProductItem
import com.orange5.nsts.data.db.entities.ShippingNotice
import com.orange5.nsts.service.bin.BinService
import com.orange5.nsts.service.bin.ConsumableBinService
import com.orange5.nsts.service.consumable.ConsumableProductService
import com.orange5.nsts.service.rental.RentalProductItemService
import com.orange5.nsts.service.rental.RentalProductService
import com.orange5.nsts.service.rental.RentalStatus
import com.orange5.nsts.service.shipping.ReceivingLogService
import com.orange5.nsts.service.shipping.ShippingNoticeService
import com.orange5.nsts.ui.shipping.discrepancy.repository.Discrepancy
import com.orange5.nsts.util.db.GUIDFactory
import com.orange5.nsts.util.format.DateFormatter
import org.greenrobot.greendao.DaoException
import org.joda.time.DateTime
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReceivingRepository @Inject constructor(private val shippingNoticeService: ShippingNoticeService,
                                              private val consumableService: ConsumableProductService,
                                              private val rentalService: RentalProductService,
                                              private val consumableBinService: ConsumableBinService,
                                              private val rentalItemService: RentalProductItemService,
                                              private val receivingLogService: ReceivingLogService,
                                              binService: BinService) {

    private val receivingBin = binService.receivingBin()

    fun byTransferNumber(number: String): List<ReceivingNotice> {
        val items = shippingNoticeService.byTransferNumber(number)
        return items
                .map { mapNotice(it) }
                .sortedBy { it.isRental() }
    }

    fun loadReceivingItems(): List<ReceivingNotice> {
        val consumables = getConsumableItems()
        val rentals = getRentalsItems()
        return consumables + rentals
    }

    fun getReceivingItemsCount() = loadReceivingItems().sumBy { it.quantity.imported }

    fun processProductsToReceiving(received: List<ReceivingNotice>) {
        received.forEach {
            if (it.quantity.processed > 0) {
                shippingNoticeService.byId(it.id).update(it)
                val product =
                        if (it.isRental()) rentalService.byProductNumber(it.productNumber)
                        else consumableService.byProductNumber(it.productNumber)
                if (product != null) {
                    if (product is ConsumableProduct) processConsumable(product, it)
                    else processRental(product as RentalProduct, it)
                }
            } else if (it.quantity.discrepancy > 0) {
                shippingNoticeService.byId(it.id).update(it)
            }
        }
    }

    fun processTakeAwayItems(processed: List<ReceivingNotice>) {
        processed.forEach { product ->
            if (product.isRental()) processRental(product)
            else processConsumable(product)
        }
    }

    private fun processConsumable(notice: ReceivingNotice) {
        val product = consumableService.byProductNumber(notice.productNumber) ?: return
        try {
            val nonReceiveBin = consumableBinService.byProductIdAndBin(product.productId, notice.bin?.binId ?: "")
            val receiveBin = consumableBinService.byProductIdAndBin(product.productId, receivingBin.binId)
            if (nonReceiveBin == null) {
                if (receiveBin.quantity > notice.quantity.processed) {
                    receiveBin.quantity -= notice.quantity.processed
                    consumableBinService.insert(mapConsumableBin(product, notice))
                } else receiveBin.binId = notice.bin?.binId ?: return

                consumableBinService.update(receiveBin)
            } else {
                receiveBin.quantity -= notice.quantity.processed
                if (receiveBin.quantity > 0) consumableBinService.update(receiveBin)
                else consumableBinService.delete(receiveBin)
                nonReceiveBin.quantity += notice.quantity.processed
                consumableBinService.update(nonReceiveBin)
            }
        } catch (e: DaoException) {
            Timber.e(IllegalArgumentException("More then 1 consumableBin with the same productId and BinId - " +
                    "productNumber : ${notice.productNumber}," +
                    "productId : ${product.productId}," +
                    "binId : ${notice.bin?.binId}"))
        }
    }

    private fun processRental(product: ReceivingNotice) {
        val item = rentalItemService.loadItemByUUN(product.uniqueUnitNumber)
        val bin = product.bin
        if (bin != null) {
            item.binId = bin.binId
            rentalItemService.update(item)
        }
    }

    private fun getConsumableItems(): List<ReceivingNotice> {
        val consumableBins = consumableBinService.byBinId(receivingBin.binId)
        return consumableBins.mapNotNull {
            val product = consumableService.byProductId(it.productId)
            if (product == null) null
            else {
                val notice = ReceivingNotice.from(it, product)
                notice.setBin(receivingBin)
                notice
            }
        }
    }

    private fun getRentalsItems(): List<ReceivingNotice> {
        val products = rentalItemService.loadByBinId(receivingBin.binId)
        return products.mapNotNull {
            val notice = ReceivingNotice.from(it)
            notice.setBin(receivingBin)
            notice
        }
    }

    private fun processConsumable(product: ConsumableProduct, notice: ReceivingNotice) {
        val consumableBin = consumableBinService.byProductIdAndBin(product.productId, receivingBin.binId)
        if (consumableBin == null) consumableBinService.insert(mapConsumableBin(product, notice))
        else {
            consumableBin.quantity += notice.quantity.processed
            consumableBinService.update(consumableBin)
        }
        setReceivingLog(notice, product.productId)
    }

    private fun processRental(product: RentalProduct, notice: ReceivingNotice) {
        val item = rentalItemService.loadItemByUUN(notice.uniqueUnitNumber)
        if (item != null) Timber.e(IllegalArgumentException("The same UUN - ShippingNotice - " +
                "transferNumber : ${notice.transferNumber}," +
                "noticeId : ${notice.id}," +
                "productNumber : ${notice.productNumber}," +
                "rentalUUN : ${notice.uniqueUnitNumber}"))
        else {
            val newItem = RentalProductItem()
            newItem.productId = product.productId
            newItem.status = RentalStatus.Available.id
            newItem.binId = receivingBin.binId
            newItem.uniqueUnitNumber = notice.uniqueUnitNumber
            rentalItemService.insert(newItem)
            setReceivingLog(notice)
        }
    }

    private fun setReceivingLog(notice: ReceivingNotice, productId: String = "") {
        val log = ProductReceivingLog()
        log.addDate = DateTime.now().toDate()
        log.historyId = GUIDFactory.newUUID()
        log.quantity = notice.quantity.processed
        log.toBinId = receivingBin.binId
        if (notice.isRental()) log.rentalUUN = notice.uniqueUnitNumber
        else log.consumableProductId = productId
    }

    private fun ShippingNotice.update(notice: ReceivingNotice) {
        if (!notice.isRental()) consumableQuantity -= (notice.quantity.processed + notice.quantity.discrepancy)
        notice.isProcessed(notice.quantity.imported == notice.quantity.processed + notice.quantity.discrepancy)
        setIsProcessed(notice.isProcessed)
        shippingNoticeService.update(this)
    }

    private fun mapNotice(notice: ShippingNotice): ReceivingNotice {
        val receivingNotice = ReceivingNotice.from(notice)
        val product =
                if (receivingNotice.isRental()) getRental(receivingNotice.productNumber)
                else getConsumable(receivingNotice.productNumber)
        if (product != null) {
            receivingNotice.setDescription(product.productDescription ?: "No description")
            receivingNotice.setProductId(product.productId ?: "No productId")
        }
        return receivingNotice
    }

    private fun mapConsumableBin(product: ConsumableProduct, notice: ReceivingNotice): ConsumableBin {
        val newBin = ConsumableBin()
        newBin.binId = notice.bin?.binId ?: receivingBin.binId
        newBin.productId = product.productId
        newBin.quantity = notice.quantity.processed
        newBin.addingDate = DateFormatter.currentTimeDb()
        return newBin
    }

    private fun getRental(number: String) = rentalService.byProductNumber(number)

    private fun getConsumable(number: String) = consumableService.byProductNumber(number)

}