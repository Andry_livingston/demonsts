package com.orange5.nsts.ui.order.full.pickup.pick.rentals;

import android.app.AlertDialog;
import android.content.Context;
import android.text.SpannableString;
import android.view.View;
import android.widget.Button;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.entities.BinNumber;
import com.orange5.nsts.data.db.entities.OrderRentalItem;
import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.service.bin.BinService;
import com.orange5.nsts.util.Colors;

import java.util.List;
import java.util.function.BiConsumer;

import com.orange5.nsts.util.Spannable;
import com.orange5.nsts.util.view.picker.PickerView;
import com.orange5.nsts.util.view.picker.ValuePickerAdapter;

public class RentalReturnDialog {
    private final Context context;
    private OrderItem orderItem;
    private BiConsumer<BinNumber, String> resultConsumer;
    private BinNumber selectedBinNumber;
    private String selectedUniqueUnitNumber;

    private PickerView picker1;
    private Button returnButton;
    private BinNumber defaultBin;

    public static RentalReturnDialog with(Context context) {
        return new RentalReturnDialog(context);
    }

    private RentalReturnDialog(Context context) {
        this.context = context;
    }


    public RentalReturnDialog resultConsumer(BiConsumer<BinNumber, String> resultConsumer) {
        this.resultConsumer = resultConsumer;
        return this;
    }


    public void showForItem(OrderRentalItem orderItem) {
        this.orderItem = orderItem;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.common_remove_rental_n, orderItem.getUniqueUnitNumber()));
        View contentView = View.inflate(context, R.layout.rental_unpick_dialog, null);
        builder.setView(contentView);
        picker1 = contentView.findViewById(R.id.picker1);
        returnButton = contentView.findViewById(R.id.actionButton);

        selectedUniqueUnitNumber = orderItem.getUniqueUnitNumber();
        List<BinNumber> bins = BinService.getBinsForReturn();

        defaultBin = BinService.getById(orderItem.asRental().getRentalProduct().getDefaultBinId());
        int selectedPosition = 0;

        for (int i = 0; i < bins.size(); i++) {
            BinNumber bin = bins.get(i);
            if (bin.equals(defaultBin)) {
                selectedPosition = i;
            }
        }

        selectedBinNumber = bins.get(selectedPosition);
        updateButton();
        new ValuePickerAdapter<>(bins)
                .selectedItemConsumer(item -> {
                    selectedBinNumber = item;
                    updateButton();
                })
                .attachTo(picker1);
        picker1.setSelectedItemPosition(selectedPosition);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        returnButton.setOnClickListener(v -> {
            resultConsumer.accept(selectedBinNumber, selectedUniqueUnitNumber);
            alertDialog.dismiss();
        });
    }

    private void updateButton() {
        String string = context.getString(R.string.common_return_s_to_s, selectedUniqueUnitNumber, selectedBinNumber.toString());
        SpannableString spannableString = Spannable
                .highlightTextColor(string, Colors.HIGHLIGHT_BLUE, selectedUniqueUnitNumber, selectedBinNumber.getBinNum());
        returnButton.setText(spannableString);
    }


}
