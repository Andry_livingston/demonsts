package com.orange5.nsts.ui.shipping.discrepancy.add

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.observe
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseDialogFragment
import com.orange5.nsts.ui.extensions.setOnClickListener
import com.orange5.nsts.ui.shipping.ShippingProcessViewModel
import com.orange5.nsts.ui.shipping.discrepancy.repository.Discrepancy
import com.orange5.nsts.ui.shipping.receiving.ReceivingViewModel
import com.orange5.nsts.ui.shipping.takeaway.TakeAwayViewModel
import kotlinx.android.synthetic.main.dialog_edit_discrepancy.*

abstract class EditDiscrepancyDetailsDialog : BaseDialogFragment() {

    abstract val viewModel: ShippingProcessViewModel

    override fun getLayoutResourceId(): Int = R.layout.dialog_edit_discrepancy

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val id = arguments?.getString(DISCREPANCY_ID_KEY) ?: ""
        viewModel.loadDiscrepancyItem(id)
        viewModel.discrepancyItem.observe(this, ::setUpView)
        viewModel.invalidDiscrepanciesCount.observe(this, ::onInvalidDiscrepancyCount)
        viewModel.onDiscrepancyAdded.observe(this) { dismiss() }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        discrepancyView.setActionButtonTitle(R.string.discrepancy_edit_action_button_title)
        cancel.setOnClickListener(::dismiss)
    }

    private fun setUpView(item: Discrepancy) {
        productNumber.setValueText(item.productNumber)
        discrepancyView.setQuantity(item.quantity)
        discrepancyView.setNotes(item.comment)
        discrepancyView.setDiscrepancyText(
                if (item.type == Discrepancy.Type.SHIPMENT_SHORTAGE) R.string.discrepancy_shortage_quantity_text
                else R.string.discrepancy_extras_quantity_text)
        discrepancyView.setOnActionClick { count, notes ->
            item.quantity = count
            item.comment = notes
            viewModel.onEditDiscrepancy(item)
        }
    }

    private fun onInvalidDiscrepancyCount(count: Int) {
        discrepancyView.showCountError(count)
    }

    companion object {
        @JvmStatic
        fun fromReceiving(fragmentManager: FragmentManager,
                          discrepancyId: String) = show(ReceivingEditDiscrepancyDetailsDialog(), fragmentManager, discrepancyId)

        @JvmStatic
        fun fromTakeAway(fragmentManager: FragmentManager,
                         discrepancyId: String) = show(TakeAwayEditDiscrepancyDetailsDialog(), fragmentManager, discrepancyId)

        fun show(dialog: EditDiscrepancyDetailsDialog,
                 fragmentManager: FragmentManager,
                 discrepancyId: String) =
                dialog.apply {
                    arguments = bundleOf(DISCREPANCY_ID_KEY to discrepancyId)
                    show(fragmentManager, this::class.java.simpleName)
                }

        private const val DISCREPANCY_ID_KEY = "discrepancy_id_key"
    }
}

class ReceivingEditDiscrepancyDetailsDialog : EditDiscrepancyDetailsDialog() {
    override val viewModel by activityViewModels<ReceivingViewModel> { factory }
}

class TakeAwayEditDiscrepancyDetailsDialog : EditDiscrepancyDetailsDialog() {
    override val viewModel by activityViewModels<TakeAwayViewModel> { factory }
}