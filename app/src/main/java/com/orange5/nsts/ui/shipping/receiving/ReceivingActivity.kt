package com.orange5.nsts.ui.shipping.receiving

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.ThreePageAdapter
import com.orange5.nsts.ui.base.ThreePageAdapter.TabType.*
import com.orange5.nsts.ui.shipping.ShippingProcessActivity
import kotlinx.android.synthetic.main.activity_shipping_process.*

class ReceivingActivity : ShippingProcessActivity() {

    override val viewModel by viewModels<ReceivingViewModel> { vmFactory }

    override val adapter: ThreePageAdapter by lazy {
        val init: Fragment = ReceivingInitFragment()
        val intermediate: Fragment = ReceivingDiscrepancyFragment()
        val result: Fragment = ReceivingResultFragment()
        val map = mapOf(INIT to init, INTERMEDIATE to intermediate, RESULT to result)
        ThreePageAdapter(map, supportFragmentManager)
    }

    override fun setBottomNavItemsTitle() {
        bottomNavigation.menu.findItem(R.id.initItem)?.title = getString(R.string.receiving_init_item_title)
        bottomNavigation.menu.findItem(R.id.intermediateItem)?.title = getString(R.string.discrepancy)
        bottomNavigation.menu.findItem(R.id.resultItem)?.title = getString(R.string.receiving_result_item_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val transferNo = intent.getStringExtra(KEY_TRANSFER_NUMBER) ?: ""
        viewModel.loadASNItems(transferNo)
        viewModel.onFinishReceivingNotice.observe(this) { onFinishReceiving() }
    }

    override fun nothingToProcessMessage() = R.string.receiving_bin_nothing_to_process

    companion object {
        private const val KEY_TRANSFER_NUMBER = "key_transfer_number"
        fun intent(context: Context, transferNumber: String) =
                Intent(context, ReceivingActivity::class.java).apply { putExtra(KEY_TRANSFER_NUMBER, transferNumber) }
    }
}