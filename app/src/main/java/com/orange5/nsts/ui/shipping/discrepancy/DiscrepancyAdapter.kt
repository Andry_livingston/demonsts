package com.orange5.nsts.ui.shipping.discrepancy

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.orange5.nsts.R
import com.orange5.nsts.ui.extensions.color
import com.orange5.nsts.ui.shipping.discrepancy.repository.Discrepancy
import com.orange5.nsts.ui.shipping.discrepancy.repository.isShortage
import com.orange5.nsts.ui.shipping.discrepancy.repository.isUnknown
import kotlinx.android.synthetic.main.item_discrepancy.view.*

class DiscrepancyAdapter(private val listener: Listener) : RecyclerView.Adapter<DiscrepancyAdapter.ShortagesVH>() {

    private var items: List<Discrepancy> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShortagesVH =
            ShortagesVH(LayoutInflater.from(parent.context).inflate(R.layout.item_discrepancy, parent, false), listener)

    override fun onBindViewHolder(holder: ShortagesVH, position: Int) = holder.bind(items[position])

    override fun getItemCount() = items.size

    fun submitList(items: List<Discrepancy>) {
        this.items = items
        notifyDataSetChanged()
    }

    class ShortagesVH(view: View, private val listener: Listener) : RecyclerView.ViewHolder(view) {
        fun bind(item: Discrepancy) {
            with(itemView) {
                uun.isVisible = item.isRental()
                uun.text = context.getString(R.string.shipping_notice_uun, item.uun)
                setDescription(item)
                quantity(item)
                setIcon(item)
                notes.text = item.comment
                setOnClickListener { listener.onItemClick(item) }
                setOnLongClickListener {
                    listener.onItemLongClick(item)
                    true
                }
            }
        }

        private fun View.setDescription(item: Discrepancy) {
            productDescription.text = if (item.type.isUnknown()) item.description else item.productDescription
        }

        private fun View.quantity(item: Discrepancy) {
            val isShortage = item.type.isShortage()
            quantity.text = if (isShortage) {
                quantity.setTextColor(context.color(R.color.errorRed))
                resources.getString(R.string.discrepancy_add_shortage_qty, item.quantity)
            } else {
                quantity.setTextColor(context.color(R.color.okGreen))
                resources.getString(R.string.discrepancy_add_extras_qty, item.quantity)
            }
        }

        private fun View.setIcon(item: Discrepancy) {
            val icon = when {
                item.type.isUnknown() -> R.drawable.ic_discrepancy_unknown
                item.isRental() -> R.drawable.ic_rental
                else -> R.drawable.ic_consumable
            }
            productIcon.setImageResource(icon)
        }
    }

    interface Listener {
        fun onItemClick(item: Discrepancy)
        fun onItemLongClick(item: Discrepancy)
    }
}