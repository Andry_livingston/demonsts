package com.orange5.nsts.ui.order.full.pickup.ship;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.data.db.base.OrderProduct;
import com.orange5.nsts.data.db.entities.OrderConsumableItem;
import com.orange5.nsts.data.db.entities.OrderRentalItem;
import com.orange5.nsts.ui.base.BaseViewHolder;
import com.orange5.nsts.util.format.CurrencyFormatter;

public class ShipOutViewHolder extends BaseViewHolder {

    private final OrderItem currentItem;
    private final OrderProduct currentProduct;
    private final Context context;

    private int redColor;
    private int greenColor;

    public ShipOutViewHolder(OrderItem currentItem, Context context) {
        this.currentItem = currentItem;
        this.currentProduct = getProduct();
        this.context = context;
        redColor = context.getColor(R.color.errorRed);
        greenColor = context.getColor(R.color.okGreen);
    }

    public int getRedColor() {
        return redColor;
    }

    public int getGreenColor() {
        return greenColor;
    }

    private OrderProduct getProduct() {
        if (currentItem instanceof OrderConsumableItem) {
            OrderConsumableItem currentItem = (OrderConsumableItem) this.currentItem;
            return currentItem.getConsumableProduct();
        }

        if (currentItem instanceof OrderRentalItem) {
            OrderRentalItem currentItem = (OrderRentalItem) this.currentItem;
            return currentItem.getRentalProduct();
        }
        throw new IllegalStateException();
    }

    public String getFormattedPrice() {
        return CurrencyFormatter.Companion.formatPrice(currentProduct.getPrice());
    }

    public SpannableString getProductDescription(String highlight) {
        return highlightBackground(currentProduct.getProductDescription(), highlight);
    }

    public SpannableString getProductNumber(String highlight) {
        return highlightBackground(currentProduct.getProductNumber(), highlight);
    }

    @SuppressLint("DefaultLocale")
    public String getPickedItemsCount() {
        return context.getString(R.string.quantity_n, currentItem.getPicked());
    }

    public int getCountColor() {
        if (currentItem instanceof OrderConsumableItem) {
            OrderConsumableItem consumableItem = (OrderConsumableItem) currentItem;
            return consumableItem.allPicked() ? greenColor : redColor;
        }
        return Color.BLACK;
    }

}
