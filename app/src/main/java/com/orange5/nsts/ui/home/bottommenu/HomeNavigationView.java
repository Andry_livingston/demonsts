package com.orange5.nsts.ui.home.bottommenu;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;

import com.orange5.nsts.R;
import com.orange5.nsts.util.CollectionUtils;

import java.util.LinkedHashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeNavigationView extends FrameLayout {


    @BindView(R.id.menuContainer)
    FrameLayout menuContainer;
    @BindView(R.id.dimmer)
    View dimmer;

    private boolean initialized = false;
    private Map<Menu, MenuItem> items;
    private static final int ANIM_DURATION = 300;
    private final FastOutSlowInInterpolator interpolator = new FastOutSlowInInterpolator();
    private int menuItemHeight;
    private OnBottomMenuClickListener listener;

    public HomeNavigationView(@NonNull Context context) {
        super(context);
        prepare();
    }

    public HomeNavigationView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        prepare();
    }

    public HomeNavigationView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        prepare();
    }

    public void setOnBottomManuClickListener(OnBottomMenuClickListener listener) {
        this.listener = listener;
    }

    private void prepare() {
        inflate(getContext(), R.layout.home_navigation_view, this);
        ButterKnife.bind(this, this);
    }

    public ViewPropertyAnimator animate() {
        return menuContainer.animate().setDuration(ANIM_DURATION)
                .setInterpolator(interpolator);
    }

    @SuppressLint("ClickableViewAccessibility")
    public void initialize() {
        if (initialized) {
            return;
        }
        initialized = true;
        menuItemHeight = getResources().getDimensionPixelSize(R.dimen.common_64dp);

        addMenuItems();
    }

    private void addMenuItems() {
        int width = getMeasuredWidth() / 4;
        items = new LinkedHashMap<>();

        Menu[] values = Menu.values();
        for (int i = 0; i < values.length; i++) {
            Menu menu = values[i];
            MenuItem item = new MenuItem(getContext(), menu);
            items.put(menu, item);

            FrameLayout itemView = item.getView();
            menuContainer.addView(itemView);

            item.setAction((name) -> {
                if (listener != null) listener.onBottomItemClick(name);
            });

            LayoutParams params = (LayoutParams) itemView.getLayoutParams();
            params.height = menuItemHeight;
            params.width = width;
            params.leftMargin = (i % 4) * width;
            params.topMargin = (i / 4) * menuItemHeight;
            itemView.setLayoutParams(params);
        }
    }

    public void setInspectionBadge(int value) {
        setBadge(value, items.get(Menu.inspectionBin));
    }

    public void setReceivingBadge(int value) {
        setBadge(value, items.get(Menu.receiving));
    }

    private void setBadge(int value, MenuItem item) {
        if (CollectionUtils.isEmpty(items)) {
            addMenuItems();
        }
        if (item != null) item.setBadgeValue(value);
    }

}
