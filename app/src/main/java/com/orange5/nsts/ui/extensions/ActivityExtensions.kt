package com.orange5.nsts.ui.extensions

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

fun AppCompatActivity.hasFragment(containerId: Int) =
        supportFragmentManager.findFragmentById(containerId) != null

/**
 * Runs a FragmentTransaction, then calls commit().
 */

inline fun FragmentManager.transact(transaction: FragmentTransaction.() -> Unit) {
    beginTransaction().apply { transaction() }.commit()
}

