package com.orange5.nsts.ui.stresstesting

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.observe
import android.view.View.VISIBLE
import androidx.activity.viewModels
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseActivity
import com.orange5.nsts.ui.extensions.errorToast
import kotlinx.android.synthetic.main.activity_stress_test.*

class StressTestActivity : BaseActivity() {

    private val viewModel: StressTestViewModel by viewModels { vmFactory }

    private var countOrders: Int = 0

    override fun getLayoutResourceId(): Int = R.layout.activity_stress_test

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.liveProgressCreateOrders.observe(this, ::updateProgressInformation)
        viewModel.liveProgress.observe(this, ::isProcessRunning)
        viewModel.onCreateOrdersFails.observe(this, ::onCreateProcessFinished)
        viewModel.onCreateOrdersSucceed.observe(this, ::onCreateProcessFinished)
        title = getString(R.string.stress_test_title)

        btnAdd10Orders.setOnClickListener { createOrders(CREATE_TEN_ORDERS) }
        btnAdd250Orders.setOnClickListener { createOrders(CREATE_250_ORDERS) }
    }

    private fun isProcessRunning(isRunning: Boolean) {
        progressBarHorizontal.isVisible = isRunning
        btnAdd10Orders.isEnabled = !isRunning
        btnAdd250Orders.isEnabled = !isRunning
    }

    private fun onCreateProcessFinished(@StringRes errorRes: Int) {
        textViewHorizontalProgress.text = getString(errorRes)
    }

    private fun createOrders(qtyOrders: Int) {
        viewModel.createDummyOrders(qtyOrders)
        countOrders = qtyOrders
        setProgressBar(qtyOrders)
    }

    private fun setProgressBar(qtyOrders: Int) {
        progressBarHorizontal.max = qtyOrders
        progressBarHorizontal.isVisible = true
        textViewHorizontalProgress.visibility = VISIBLE
        updateProgressInformation(0)
    }

    private fun updateProgressInformation(progressQtyStatus: Int) {
        progressBarHorizontal.progress = progressQtyStatus
        textViewHorizontalProgress.text = "${progressQtyStatus}/${countOrders}"
    }

    companion object {
        private const val CREATE_TEN_ORDERS = 10
        private const val CREATE_250_ORDERS = 250

        @JvmStatic
        fun newIntent(context: Context) = Intent(context, StressTestActivity::class.java)
    }

    override fun onBackPressed() {
        if (viewModel.liveProgress.value == true) {
            errorToast(R.string.stress_test_cant_back)
        } else {
            super.onBackPressed()
        }
    }
}