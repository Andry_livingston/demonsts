package com.orange5.nsts.ui.order.full.pickup.adapter.bins;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.base.OrderItem;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class BinsItemView extends LinearLayout {

    @BindView(R.id.bin)
    TextView bin;
    @BindView(R.id.addAll)
    TextView addAll;


    public BinsItemView(@NonNull Context context) {
        this(context, null);
    }

    public BinsItemView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BinsItemView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public BinsItemView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        inflate(context, R.layout.view_bins_item, this);
        ButterKnife.bind(this, this);
        setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT));
    }

    public void fill(OrderItem item) {
        setVisibility(VISIBLE);
        item.ifRental(it -> {
            if (it.getScannedBin() == null) setVisibility(GONE);
            addAll.setText(R.string.pickup_button_add_rental_item);
            bin.setText(getResources().getString(R.string.pickup_rental_item_in_bin_dropdown, it.getRentalProduct().getProductNumber(), it.getUniqueUnitNumber()));
        });
        item.ifConsumable(it -> {
            addAll.setText(R.string.pickup_button_add_all_consumable);
            int quantity = it.getQuantity() - it.getPicked();
            if (quantity <= 0) this.setVisibility(GONE);
            bin.setText(getResources().getString(R.string.pickup_consumable_item_in_bin_dropdown, it.getConsumableProduct().getProductNumber(),
                    quantity));
        });
    }
}