package com.orange5.nsts.ui.shipping.repository

import com.orange5.nsts.data.db.entities.BinNumber
import com.orange5.nsts.data.db.entities.ConsumableBin
import com.orange5.nsts.data.db.entities.ConsumableProduct
import com.orange5.nsts.data.db.entities.RentalProductItem
import com.orange5.nsts.data.db.entities.ShippingNotice
import com.orange5.nsts.util.db.GUIDFactory
import com.orange5.nsts.util.format.DateFormatter

class ReceivingNotice(val id: String = "",
                      val transferNumber: String = "",
                      val productNumber: String = "",
                      val uniqueUnitNumber: String = "",
                      var quantity: Quantity = Quantity(),
                      var isProcessed: Byte = 0,
                      private val shippingDate: String = "") {

    var productDescription: String = ""
        private set

    var productId: String = ""
        private set

    var bin: BinNumber? = BinNumber()
        private set

    fun isProcessed() = isProcessed == 1.toByte()

    fun isProcessed(isProcessed: Boolean) {
        this.isProcessed = if (isProcessed) 1 else 0
    }

    fun processItems(count: Int) {
        val processed = quantity.processed + count
        val remaining = quantity.imported - processed - quantity.discrepancy
        quantity = quantity.copy(processed = processed, remaining = remaining)
    }

    fun returnItems(count: Int) {
        val remaining = quantity.remaining + count
        val processed = quantity.imported - remaining - quantity.discrepancy
        quantity = quantity.copy(processed = processed, remaining = remaining)
    }

    fun returned(returned: Int) {
        val imported = quantity.imported - returned
        quantity = Quantity(imported, imported, 0)
    }

    fun addDiscrepancy(added: Int) {
        val discrepancyCount = quantity.discrepancy + added
        applyDiscrepancy(discrepancyCount)
    }

    fun editDiscrepancy(quantity: Int) {
        applyDiscrepancy(quantity)
    }

    private fun applyDiscrepancy(discrepancyCount: Int) {
        val remaining = quantity.imported - discrepancyCount - quantity.processed
        quantity = quantity.copy(discrepancy = discrepancyCount, remaining = remaining)
    }

    fun isRental() = uniqueUnitNumber.isNotEmpty()

    fun shippingDate() = DateFormatter.toUIDate(shippingDate)

    fun setDescription(description: String) {
        this.productDescription = description
    }

    fun setProductId(productId: String) {
        this.productId = productId
    }

    fun setBin(bin: BinNumber?) {
        this.bin = bin
    }

    fun takeAwayProcessed(processed: Int) {
        val newImported = quantity.imported - processed
        quantity = quantity.copy(imported = newImported, processed = 0, remaining = newImported)
    }

    fun takeAwayReturn(processed: Int) {
        val newImported = quantity.imported + processed
        quantity = quantity.copy(imported = newImported, processed = 0, remaining = newImported)
    }

    companion object {
        fun from(notice: ShippingNotice) = ReceivingNotice(
                notice.shippingNoticeId,
                notice.transferNumber,
                setProductNumber(notice),
                notice.rentalUniqueUnitNumber ?: "",
                setQuantity(notice),
                isProcessed = notice.getIsProcessed(),
                shippingDate = notice.shippingDate)

        fun from(bin: ConsumableBin, product: ConsumableProduct) = ReceivingNotice(
                "TA${GUIDFactory.newUUID()}",
                "N/A",
                product.productNumber,
                shippingDate = bin.addingDate,
                isProcessed = 1,
                quantity = Quantity(bin.quantity, 0, 0, bin.quantity)).apply {
            setDescription(product.productDescription)
            setProductId(product.productId)
        }

        fun from(product: RentalProductItem) = ReceivingNotice(
                "TA${GUIDFactory.newUUID()}",
                "N/A",
                uniqueUnitNumber = product.uniqueUnitNumber,
                productNumber = product.productNumber,
                isProcessed = 1,
                shippingDate = DateFormatter.currentTimeDb()).apply {
            setDescription(product.productDescription)
            setProductId(product.productId)
        }

        fun from(notice: ReceivingNotice) = ReceivingNotice(
                GUIDFactory.newUUID(),
                notice.transferNumber,
                notice.productNumber,
                notice.uniqueUnitNumber,
                notice.quantity,
                0,
                notice.shippingDate).apply {
            setDescription(notice.productDescription)
            setProductId(productId)
        }

        private fun setProductNumber(notice: ShippingNotice) =
                if (notice.rentalUniqueUnitNumber != null) notice.rentalProductNumber else notice.consumableProductNumber

        private fun setQuantity(notice: ShippingNotice) =
                if (notice.rentalUniqueUnitNumber != null) Quantity()
                else Quantity(notice.consumableQuantity,
                        processed = 0,
                        discrepancy = 0,
                        remaining = notice.consumableQuantity)
    }
}

data class Quantity(val imported: Int = 1,
                    val processed: Int = 0,
                    val discrepancy: Int = 0,
                    val remaining: Int = 1)

