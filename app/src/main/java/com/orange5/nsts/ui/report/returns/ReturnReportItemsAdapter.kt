package com.orange5.nsts.ui.report.returns

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.util.format.DateFormatter
import kotlinx.android.synthetic.main.item_return.view.*

class ReturnReportItemsAdapter
    : RecyclerView.Adapter<ReturnReportItemsAdapter.ReportReturnVH>() {

    private var items = mutableListOf<OrderRentalItem>()

    override fun getItemCount() = items.size

    fun submitList(items: List<OrderRentalItem>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportReturnVH =
            ReportReturnVH(LayoutInflater.from(parent.context).inflate(R.layout.item_return,
                    parent, false))

    override fun onBindViewHolder(holder: ReportReturnVH, position: Int) = holder.bind(items[position])

    inner class ReportReturnVH(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: OrderRentalItem) {
            with(itemView) {
                date.text = resources.getString(R.string.rental_return_activity_rented_start_date, DateFormatter
                        .convertDbDateToUI(item.startDate))
                productNumber.text = item.productNumber
                productDescription.text = item.rentalProduct.productDescription
                uun.text = resources.getString(R.string.common_uun_nr, item.uniqueUnitNumber)
                manufacturerNumber.text = item.rentalProduct.manufacturerNumber
                productDueDate.isVisible = item.dueDate != null
                state.isVisible = item.wasDamaged

                if (item.dueDate != null)
                    productDueDate.text = resources.getString(R.string.return_report_fragment_due_date,
                            DateFormatter.convertDbDateToUI(item.dueDate))
                if (item.wasDamaged) state.text = resources.getString(R.string.return_report_fragment_damaged)
            }
        }
    }
}
