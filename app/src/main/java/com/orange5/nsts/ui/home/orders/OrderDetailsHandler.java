package com.orange5.nsts.ui.home.orders;

import android.content.Context;
import android.widget.Toast;

import com.orange5.nsts.data.db.entities.Order;
import com.orange5.nsts.service.order.OrderStatus;
import com.orange5.nsts.ui.base.BaseActivity;
import com.orange5.nsts.ui.order.full.pickup.PickUpActivity;
import com.orange5.nsts.ui.order.full.select.OrderItemsActivity;
import com.orange5.nsts.ui.report.ReportActivity;
import com.orange5.nsts.ui.report.ReportType;

import static com.orange5.nsts.ui.order.full.pickup.PickUpActivity.ORDER_ID;

class OrderDetailsHandler {
    static void showDetailsFor(Context context, Order order) {
        OrderStatus status = OrderStatus.byIndex(order.getOrderStatusId());
        switch (status) {
            case open:
                BaseActivity.getActive()
                        .prepareActivity(OrderItemsActivity.class)
                        .add(ORDER_ID, order.getOrderId())
                        .clearTask()
                        .start();
                break;
            case confirmed:
            case closed:
                context.startActivity(ReportActivity.newIntent(context, ReportType.ORDER,
                        order.getOrderId()));
                break;
            case readyNotShipped:
                context.startActivity(ReportActivity.newIntent(context, ReportType.ORDER,
                        order.getOrderId()));
                break;
            case pickUp:
                BaseActivity.getActive()
                        .prepareActivity(PickUpActivity.class)
                        .add(ORDER_ID, order.getOrderId())
                        .clearTask()
                        .start();
                break;

            case active:
                context.startActivity(ReportActivity.newIntent(context, ReportType.ORDER,
                        order.getOrderId()));
                break;
            default:
                Toast.makeText(context, "Currently not handling status: " + status.name(), Toast.LENGTH_SHORT).show();
        }
    }
}
