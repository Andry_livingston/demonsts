package com.orange5.nsts.ui.order.full.pickup.adapter.pickup;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.ui.extensions.OrderExtensionsKt;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PickUpItemView extends FrameLayout {

    @BindView(R.id.productDescription)
    TextView itemName;
    @BindView(R.id.productNumber)
    TextView itemNumber;
    @BindView(R.id.itemQuantity)
    TextView itemQuantity;
    @BindView(R.id.itemType)
    ImageView itemType;
    @BindView(R.id.itemPrice)
    TextView itemPrice;
    @BindView(R.id.availability)
    TextView itemAvailability;
    @BindView(R.id.pickedItemsCount)
    TextView pickedItemsCount;
    @BindView(R.id.pickProgress)
    ProgressBar pickProgress;
    @BindView(R.id.quantityForPickup)
    TextView quantityForPickup;
    @BindView(R.id.currentBin)
    TextView currentBin;
    @BindView(R.id.scanContainer)
    LinearLayout scanContainer;
    @BindView(R.id.container)
    RelativeLayout container;
    @BindView(R.id.isBackorder)
    CheckBox isBackorder;

    private PickupItemsAdapter.PickItemListener listener;

    public PickUpItemView(@NonNull Context context) {
        this(context, null);
    }

    public PickUpItemView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PickUpItemView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public PickUpItemView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        inflate(context, R.layout.item_order_pickup, this);
        ButterKnife.bind(this, this);
    }

    public void fill(OrderItem item) {
        isBackorder.setVisibility(item.isConsumable() && item.isOversell() ? VISIBLE : GONE);
        item.ifConsumable(it -> this.isBackorder.setChecked(it.isBackorder()));
        setListeners(item);
        scanContainer.setVisibility(GONE);
        PickUpViewHolder viewModel = new PickUpViewHolder(getContext(), item);
        itemName.setText(viewModel.getProductDescription(""));
        itemNumber.setText(viewModel.getProductNumber(""));
        itemType.setBackgroundResource(OrderExtensionsKt.orderIcon(item));
        itemPrice.setText(viewModel.getFormattedPrice());
        itemQuantity.setText(viewModel.getFormattedQuantity());
        itemAvailability.setText(viewModel.getBinQuantities());

        pickProgress.setMax(item.getQuantity());
        pickProgress.setProgress(item.getPicked());
        pickProgress.setSecondaryProgress(viewModel.getSecondaryProgress());
        pickedItemsCount.setTextColor(viewModel.getCountColor());
        pickedItemsCount.setText(viewModel.getPickedItemsCount());
    }

    private void bindScannedBin(OrderItem item) {
        item.ifConsumable(consumable -> {
            if (consumable.getScannedBin() != null) {
                scanContainer.setVisibility(VISIBLE);
                currentBin.setText(getContext().getString(R.string.pickup_scanned_bin, consumable.getScannedBin().toString()));
                quantityForPickup.setText(getContext().getString(R.string.pickup_quantity_for_consumable_pickup, item.getQuantity()));
            }
        });
        item.ifRental(rental -> {
            if (rental.getScannedBin() != null) {
                scanContainer.setVisibility(VISIBLE);
                currentBin.setText(getContext().getString(R.string.pickup_scanned_bin, rental.getScannedBin().toString()));
                quantityForPickup.setText(getContext().getString(R.string.common_uun_nr, rental.getUniqueUnitNumber()));
            }
        });
    }

    private void setListeners(OrderItem item) {
        container.setOnClickListener(v -> {
            if (listener != null) listener.onItemClick(item);
        });
        quantityForPickup.setOnClickListener((v) -> {
            if (listener != null) listener.onAddItemsClick(item);
        });
        isBackorder.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (listener != null) listener.onBackorderChecked(item, isChecked);
        });
    }

    public void addPickupListener(PickupItemsAdapter.PickItemListener listener) {
        this.listener = listener;
    }
}