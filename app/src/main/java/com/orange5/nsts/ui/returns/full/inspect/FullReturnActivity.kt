package com.orange5.nsts.ui.returns.full.inspect

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.Customer
import com.orange5.nsts.ui.base.TwoPageAdapter
import com.orange5.nsts.ui.base.TwoPageAdapter.TabType.INIT
import com.orange5.nsts.ui.base.TwoPageAdapter.TabType.RESULT
import com.orange5.nsts.ui.returns.ReturnActivity
import com.orange5.nsts.ui.returns.full.revision.RevisionActivity
import kotlinx.android.synthetic.main.activity_return.*

class FullReturnActivity : ReturnActivity() {

    override val viewModel: FullReturnViewModel by viewModels { vmFactory }

    override val adapter: TwoPageAdapter by lazy {
        val map =
                mapOf(INIT to FullRentalItemsFragment(), RESULT to FullReturnedItemsFragment())
        TwoPageAdapter(map, supportFragmentManager)
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.returnFinished.observe(this, Observer { showGoToRevisionDialog(it) })
        viewModel.liveCustomer.observe(this, Observer(::setCustomer))
        setTitle(R.string.rental_return_activity_full)
        val customerId = intent.getStringExtra(CUSTOMER_KEY) ?: ""
        viewModel.setCustomer(customerId)
        showAll.isVisible = true
        showAll.setOnCheckedChangeListener { _, isChecked -> viewModel.fetchReturnItems(isChecked) }
    }

    private fun showGoToRevisionDialog(customer: Customer?) {
        if (dialog != null && dialog!!.isShowing) {
            dialog!!.dismiss()
        }
        dialog = AlertDialog.Builder(this)
                .setMessage(R.string.rental_return_activity_open_inspection_bin)
                .setPositiveButton(android.R.string.ok) { d, _ ->
                    startActivity(RevisionActivity.newIntent(this, customer?.customerId))
                    finishRevision(d)
                }
                .setNegativeButton(R.string.dialog_negative_later) { d, _ -> finishRevision(d) }
                .show()
    }

    private fun finishRevision(dialog: DialogInterface) {
        dialog.dismiss()
        finish()
    }

    private fun setCustomer(customer: Customer?) {
        customerName.isVisible = customer != null
        customerName.text = "${customer?.fName} ${customer?.lName}"
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.initItem -> {
                viewPager.setCurrentItem(INIT.id, true)
                showAll.isVisible = true
            }
            R.id.intermediateItem -> {
                viewPager.setCurrentItem(RESULT.id, true)
                showAll.isVisible = false
            }
        }
        return true
    }

    companion object {
        @JvmStatic
        fun newIntent(context: Context, customerId: String): Intent =
                Intent(context, FullReturnActivity::class.java).apply { putExtra(CUSTOMER_KEY, customerId) }

        private const val CUSTOMER_KEY = "customer_key"
    }
}