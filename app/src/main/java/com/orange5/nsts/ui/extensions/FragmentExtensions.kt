package com.orange5.nsts.ui.extensions

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.orange5.nsts.R

fun FragmentManager.replaceFragment(fragment: Fragment,
                                    tag: String = fragment::class.simpleName!!,
                                    frameId: Int = R.id.frameLayout,
                                    shouldAddToBackStack: Boolean = true) {
    transact {
        replace(frameId, fragment, tag)
        if (shouldAddToBackStack)
            addToBackStack(tag)
    }
}

fun FragmentManager.addFragment(fragment: Fragment,
                                    tag: String = fragment::class.simpleName!!,
                                    frameId: Int = R.id.frameLayout,
                                    shouldAddToBackStack: Boolean = true) {
    transact {
        add(frameId, fragment, tag)
        if (shouldAddToBackStack)
            addToBackStack(tag)
    }
}