package com.orange5.nsts.ui.customer;

import androidx.lifecycle.MutableLiveData;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.entities.AccountingCode;
import com.orange5.nsts.data.db.entities.Customer;
import com.orange5.nsts.data.db.entities.Order;
import com.orange5.nsts.data.preferencies.RawSharedPreferences;
import com.orange5.nsts.service.accountingcode.AccountingCodeService;
import com.orange5.nsts.service.customer.CustomerService;
import com.orange5.nsts.service.order.OrderService;
import com.orange5.nsts.service.order.OrderStatus;
import com.orange5.nsts.service.parameters.ParametersService;
import com.orange5.nsts.ui.base.ActionLiveData;
import com.orange5.nsts.ui.base.BaseViewModel;
import com.orange5.nsts.util.CollectionUtils;
import com.orange5.nsts.util.db.OrderNumberGenerator;
import com.orange5.nsts.util.format.DateFormatter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SelectCustomerViewModel extends BaseViewModel {

    private final OrderService orderService;
    private final AccountingCodeService accountingService;
    private final CustomerService customerService;
    private final ParametersService parametersService;
    private final RawSharedPreferences preferences;
    private boolean isOrderCreating = false;
    public ActionLiveData<Order> liveOrderCreated = new ActionLiveData<>();
    public MutableLiveData<List<Customer>> liveLoadCustomers = new MutableLiveData<>();
    public MutableLiveData<List<AccountingCode>> liveLoadAccountingCode = new MutableLiveData<>();
    public MutableLiveData<Integer> liveValidationError = new MutableLiveData<>();

    @Inject
    public SelectCustomerViewModel(OrderService orderService,
                                   AccountingCodeService accountingService,
                                   CustomerService customerService,
                                   ParametersService parametersService,
                                   RawSharedPreferences preferences) {
        this.orderService = orderService;
        this.accountingService = accountingService;
        this.customerService = customerService;
        this.parametersService = parametersService;
        this.preferences = preferences;
    }

    public void createNewOrder(AccountingCode selectedAccountingCode,
                               String webOrderNumber,
                               Customer selectedCustomer) {
        if (selectedCustomer == null) {
            liveProgress.setValue(false);
            liveValidationError.setValue(R.string.selected_customer_activity_warning_message_customer_is_required);
        } else if (isAccountingCodeRequired() && selectedAccountingCode == null) {
            liveProgress.setValue(false);
            liveValidationError.setValue(R.string.selected_customer_activity_warning_message_accounting_code_is_required);
        } else {
            if (isOrderCreating) return;
            isOrderCreating = true;
            createOrder(selectedAccountingCode, webOrderNumber, selectedCustomer);
        }
    }
    public void createNewStressOrder(Customer selectedCustomer) {
        createOrder(null, null, selectedCustomer);
    }

    private void createOrder(AccountingCode selectedAccountingCode, String webOrderNumber, Customer selectedCustomer) {
        addDisposable(rxSingleCreate(() -> mapOrder(selectedAccountingCode, webOrderNumber, selectedCustomer))
                .doOnEvent((o, e) -> orderService.insert(o))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> isOrderCreating = false)
                .subscribe(o -> {
                    liveOrderCreated.setValue(o);
                    liveRunSync.setValue(null);
                    liveProgress.setValue(false);
                }, this::onError));
    }

    public void loadCustomers() {
        List<Customer> customers = customerService.loadAll();
        if (CollectionUtils.isEmpty(customers)) {
            customers = new ArrayList<>();
        }
        liveLoadCustomers.setValue(customers);
    }

    public void loadAccountingCode() {
        List<AccountingCode> accountingCodes = accountingService.loadAll();
        if (CollectionUtils.isEmpty(accountingCodes)) {
            accountingCodes = new ArrayList<>();
        }
        liveLoadAccountingCode.setValue(accountingCodes);
    }

    public boolean isAccountingCodeRequired() {
        return preferences.isAccountingCodeRequired();
    }

    private Order mapOrder(AccountingCode selectedAccountingCode,
                           String webOrderNumber,
                           Customer selectedCustomer) {
        Order order = new Order();
        order.setCreatedByUserId(preferences.getUser().getUserId());
        order.setOrderDate(DateFormatter.currentTimeDb());
        order.setDeliveryDate(DateFormatter.currentTimeDb());
        order.setCustomer(selectedCustomer);
        if (selectedAccountingCode != null) {
            order.setAccountingCode(selectedAccountingCode.getAccountingCodeName());
        }
        order.setWebOrderNumber(webOrderNumber);
        order.setOrderStatusId(OrderStatus.open.getId());
        String prefix = parametersService.getOrderPrefix();
        order.setOrderNumber(OrderNumberGenerator.getNext(orderService.getLastOrder(prefix), prefix));
        return order;
    }
}
