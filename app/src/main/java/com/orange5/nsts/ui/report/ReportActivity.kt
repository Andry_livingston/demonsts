package com.orange5.nsts.ui.report

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseActivity
import com.orange5.nsts.ui.extensions.replaceFragment
import com.orange5.nsts.ui.report.closed.ClosedOrderReportFragment
import com.orange5.nsts.ui.report.returns.ReturnReportFragment

class ReportActivity : BaseActivity() {

    private val viewModel by viewModels<ReportViewModel> { vmFactory }

    override fun getLayoutResourceId() = R.layout.activity_report

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val reportType: ReportType = intent.getSerializableExtra(TYPE_KEY) as ReportType
        viewModel.updateOrderInformation( intent.getStringExtra(ORDER_ID) ?: null, reportType)
        viewModel.orderTitle.observe(this, Observer(::onShowTitle))

        val fragment = when (reportType) {
            ReportType.RETURN -> ReturnReportFragment()
            ReportType.ORDER -> ClosedOrderReportFragment()
        }
        supportFragmentManager.replaceFragment(fragment, shouldAddToBackStack = false)
    }

    private fun onShowTitle(title: String?) {
        this.title = title
    }

    companion object {
        @JvmStatic
        fun newIntent(context: Context, type: ReportType, id: String): Intent {
            return Intent(context, ReportActivity::class.java)
                    .putExtra(ORDER_ID, id)
                    .putExtra(TYPE_KEY, type)
        }
        private const val ORDER_ID = "ORDER_ID"
        private const val TYPE_KEY = "type_key"
    }
}
enum class ReportType {
    ORDER,
    RETURN
}
