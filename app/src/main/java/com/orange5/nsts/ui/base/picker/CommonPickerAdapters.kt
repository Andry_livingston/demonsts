package com.orange5.nsts.ui.base.picker

import com.orange5.nsts.data.db.entities.BinNumber
import javax.inject.Inject

class BinPickerAdapter @Inject constructor() : PickerAdapter<BinNumber>()

class NumberPickerAdapter @Inject constructor() : PickerAdapter<Int>()

