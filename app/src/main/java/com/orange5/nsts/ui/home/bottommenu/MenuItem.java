package com.orange5.nsts.ui.home.bottommenu;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.orange5.nsts.R;

import java.util.function.Consumer;


class MenuItem {

    private FrameLayout rootView;
    private TextView textView;
    private TextView badge;
    private final Menu menu;

    public MenuItem(Context context, Menu menu) {
        this.menu = menu;
        initView(context);
    }

    private void initView(Context context) {
        rootView = (FrameLayout) View.inflate(context, R.layout.home_menu_item, null);
        textView = rootView.findViewById(R.id.text);
        badge = rootView.findViewById(R.id.badge);
        setText(menu.getTextResourceId());
        setBadgeValue(0);
        setDrawable(menu.getDrawableResourceId());
    }

    public void setBadgeValue(int count) {
        badge.setVisibility(count == 0 ? View.GONE : View.VISIBLE);
        badge.setText(count > 99 ? "99+" : String.valueOf(count));
    }

    public FrameLayout getView() {
        return rootView;
    }

    public void setText(int textResourceId) {
        textView.setText(textResourceId);
    }

    public void setDrawable(int drawable) {
        textView.setCompoundDrawablesWithIntrinsicBounds(0, drawable, 0, 0);
    }

    public void setAction(Consumer<Menu> consumer) {
        textView.setOnClickListener((v) -> consumer.accept(menu));
    }
}
