package com.orange5.nsts.ui.order.full.pickup;

import android.graphics.Bitmap;

import androidx.lifecycle.MutableLiveData;

import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.data.db.base.OrderProduct;
import com.orange5.nsts.data.db.entities.BinNumber;
import com.orange5.nsts.data.db.entities.ConsumableBin;
import com.orange5.nsts.data.db.entities.ConsumableProduct;
import com.orange5.nsts.data.db.entities.Order;
import com.orange5.nsts.data.db.entities.OrderConsumableItem;
import com.orange5.nsts.data.db.entities.OrderRentalItem;
import com.orange5.nsts.data.db.entities.RentalProduct;
import com.orange5.nsts.data.db.entities.RentalProductItem;
import com.orange5.nsts.service.bin.ConsumableBinService;
import com.orange5.nsts.service.order.OrderService;
import com.orange5.nsts.service.order.OrderStatus;
import com.orange5.nsts.service.order.items.OrderConsumableItemService;
import com.orange5.nsts.service.order.items.OrderRentalItemService;
import com.orange5.nsts.service.parameters.ParametersService;
import com.orange5.nsts.service.rental.AggregatedRentalItem;
import com.orange5.nsts.service.rental.RentalItemService;
import com.orange5.nsts.service.rental.RentalProductItemService;
import com.orange5.nsts.service.rental.RentalStatus;
import com.orange5.nsts.ui.base.ActionLiveData;
import com.orange5.nsts.ui.base.BaseViewModel;
import com.orange5.nsts.util.CollectionUtils;
import com.orange5.nsts.util.Operation;
import com.orange5.nsts.util.db.OrderNumberGenerator;
import com.orange5.nsts.util.format.DateFormatter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.inject.Inject;

import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

import static com.orange5.nsts.service.bin.BinService.RECEIVING;
import static com.orange5.nsts.ui.order.full.pickup.PickUpActivity.CONSUMABLE;
import static com.orange5.nsts.ui.order.full.pickup.PickUpActivity.EMPTY_TYPE;
import static com.orange5.nsts.ui.order.full.pickup.PickUpActivity.RENTAL;
import static com.orange5.nsts.util.SignatureManager.bitmapToByteArray;

public class PickUpViewModel extends BaseViewModel {

    private final OrderRentalItemService rentalItemService;
    private final OrderConsumableItemService consumableItemService;
    private final ConsumableBinService consumableBinService;
    private final RentalProductItemService rentalProductService;
    private final OrderService orderService;
    private final ParametersService parametersService;
    public boolean shouldCreateNewOrder = false;

    private Map<BinNumber, List<OrderItem>> binNumbers = new HashMap<>();
    public ActionLiveData<String> liveFailLoadOrder = new ActionLiveData<>();
    public MutableLiveData<Void> liveOrderFinished = new MutableLiveData<>();
    public MutableLiveData<String> liveUpdateUi = new MutableLiveData<>();
    public MutableLiveData<List<OrderItem>> liveBinsListUpdate = new MutableLiveData<>();
    public MutableLiveData<Boolean> liveCantDeleteOrder = new MutableLiveData<>();
    public ActionLiveData<Boolean> liveDeleteOrder = new ActionLiveData<>();
    public ActionLiveData<Boolean> isServerDeleteOrder = new ActionLiveData<>();

    private Order order;

    @Inject
    public PickUpViewModel(OrderRentalItemService rentalItemService,
                           OrderConsumableItemService consumableItemService,
                           ConsumableBinService consumableBinService,
                           RentalProductItemService rentalProductService,
                           ParametersService parametersService,
                           OrderService orderService) {
        this.rentalItemService = rentalItemService;
        this.consumableItemService = consumableItemService;
        this.consumableBinService = consumableBinService;
        this.rentalProductService = rentalProductService;
        this.parametersService = parametersService;
        this.orderService = orderService;
    }

    public Map<BinNumber, List<OrderItem>> getBinNumbers() {
        return binNumbers;
    }

    public void loadExisting(String orderId) {
        order = orderService.getOrderById(orderId);
        if (order == null) {
            liveFailLoadOrder.setValue(orderId);
        } else {
            liveUpdateUi.setValue(EMPTY_TYPE);
        }
    }

    public void setBackorderFlag(@NotNull OrderItem item, boolean isBackorder) {
        item.ifConsumable(it -> {
            it.isBackorder(isBackorder);
            consumableItemService.update(it);
            orderService.update(order);
        });
    }

    public void finishOrder(Bitmap signatureBitmap) {
        addDisposable(rxCompletableCreate(() -> {
            if (shouldCreateNewOrder) createBackorder();
            if (order.getPickCustomer() == null) {
                order.setPickCustomer(order.getCustomer());
            }
            order.getRentalItems().forEach(this::startRentPeriod);
            order.setOrderCompleteDate(DateFormatter.currentTimeDb());
            order.setOrderStatusId(OrderStatus.closed.getId());
            order.setSignImage(bitmapToByteArray(signatureBitmap));
            orderService.update(order);
        }).subscribeOn(Schedulers.computation())
                .subscribe(() -> {
                    liveOrderFinished.postValue(null);
                    liveRunSync.postValue(null);
                }));
    }

    public boolean areAllPicked() {
        boolean allRentalsPicked = order.getRentalItems().stream()
                .allMatch(rentalItem -> rentalItem.getUniqueUnitNumber() != null);

        boolean allConsumablesPicked = order.getConsumableItems().stream()
                .allMatch(OrderConsumableItem::allPicked);

        return allRentalsPicked && allConsumablesPicked;
    }

    public boolean hasPickedItems() {

        boolean areRentalsPicked = order.getRentalItems()
                .stream()
                .anyMatch(rentalItem -> rentalItem.getUniqueUnitNumber() != null);

        boolean areConsumablesPicked = order.getConsumableItems()
                .stream()
                .anyMatch(consumableItem -> consumableItem.getPicked() > 0 || consumableItem.oversellAllowed());

        return (areRentalsPicked || areConsumablesPicked);
    }


    public Integer recalculateAndGetProductCount() {
        Operation totalSum = Operation.with(0);
        Operation totalItems = Operation.with(0);
        order.getRentalItems()
                .stream()
                .filter(rentalItem -> rentalItem.getUniqueUnitNumber() != null)
                .forEach(rentalItem -> {
                    totalItems.add(1);
                    totalSum.add(rentalItem.getPrice());
                });
        order.getConsumableItems()
                .stream()
                .filter(consumableItem -> consumableItem.getPicked() > 0 || consumableItem.oversellAllowed())
                .forEach(consumableItem -> {
                    totalItems.add(consumableItem.getPicked());
                    if (consumableItem.oversellAllowed()) {
                        totalItems.add(consumableItem.getOversell());
                    }
                    Operation itemTotal = Operation.with(consumableItem.getPrice())
                            .multiply(consumableItem.getPicked());
                    if (consumableItem.isOversell() && consumableItem.oversellAllowed()) {
                        Operation oversellTotal = Operation
                                .with(consumableItem.getOversell())
                                .multiply(consumableItem.getPrice());
                        itemTotal.add(oversellTotal);
                    }
                    totalSum.add(itemTotal);
                });
        order.setOrderSum(totalSum.toDouble());
        return totalItems.toInteger();
    }

    public void setExistingBins() {
        liveProgress.setValue(true);
        addDisposable(rxCompletableCreate(this::getExistingBinsFromOrder)
                .subscribeOn(Schedulers.computation())
                .subscribe(() -> liveProgress.postValue(false), this::onError));
    }

    public void refreshSelectedBinItems(BinNumber scannedBin) {
        if (scannedBin == null) return;
        List<OrderItem> items = new ArrayList<>();
        List<AggregatedRentalItem> aggregatedRentalsList = getAggregatedRentalsList();
        List<OrderItem> allItems = new ArrayList<>(aggregatedRentalsList);
        allItems.addAll(getConsumableItems());
        for (OrderItem item : allItems) {
            binNumbers.get(scannedBin).forEach(it -> {
                if (it.getProductId().equals(item.getProductId())) {
                    int quantity = item.getQuantity() - item.getPicked();
                    if (quantity > 0) {
                        items.add(item);
                    }
                }
            });
        }
        liveBinsListUpdate.setValue(items);
    }

    public List<OrderRentalItem> getPickedRentals() {
        return order.getRentalItems()
                .stream()
                .filter(it -> it.getUniqueUnitNumber() != null)
                .collect(Collectors.toList());
    }

    public List<OrderConsumableItem> getPickedConsumables() {
        return order.getConsumableItems()
                .stream()
                .filter(orderConsumableItem -> (orderConsumableItem.getPicked() > 0) || (orderConsumableItem.isOversell() && orderConsumableItem.oversellAllowed()))
                .collect(Collectors.toList());
    }

    public List<ConsumableBin> getBinsForPickUp(ConsumableProduct consumableProduct) {
        return consumableBinService.getBinsForPickUp(consumableProduct);
    }

    public List<BinNumber> getBinsForPickUp(RentalProduct product) {
        List<BinNumber> result = new ArrayList<>();
        for (RentalProductItem rentalProductItem : product.getProductItems()) {
            BinNumber containerBin = rentalProductItem.getContainerBin();
            if (containerBin != null
                    && rentalProductItem.getUniqueUnitNumber() != null
                    && !result.contains(containerBin)
                    && !containerBin.getBinNum().equals(RECEIVING)) {
                result.add(containerBin);
            }
        }
        return result;
    }

    public void pickUp(OrderConsumableItem item, ConsumableBin consumableBin, int count) {
        doBeforeSync(CONSUMABLE, () -> {
            item.setPicked(item.getPicked() + count);
            if (item.allPicked()) item.isBackorder(false);
            consumableItemService.update(item);
            if (consumableBin != null) {
                consumableBin.setQuantity(consumableBin.getQuantity() - count);
                consumableBinService.update(consumableBin);
            }
            orderService.update(item.getOrder());
        });
    }

    public void pickUp(OrderRentalItem firstUnpicked, BinNumber binNumber, String uun) {
        doBeforeSync(RENTAL, () -> {
            firstUnpicked.setUniqueUnitNumber(uun);
            rentalItemService.update(firstUnpicked);

            List<RentalProductItem> items = firstUnpicked
                    .getRentalProduct()
                    .getProductItems()
                    .stream()
                    .filter(rentalProductItem -> rentalProductItem.getBinId() != null)
                    .filter(rentalProductItem -> rentalProductItem.getUniqueUnitNumber().equals(uun)
                            && rentalProductItem.getBinId().equals(binNumber.getBinId()))
                    .collect(Collectors.toList());

            if (CollectionUtils.isNotEmpty(items)) {
                RentalProductItem productItem = items.get(0);
                productItem.setBinId(null);
                productItem.setStatus(RentalStatus.CheckedOut.getId());
                rentalProductService.update(productItem);
                orderService.update(firstUnpicked.getOrder());
            } else {
                liveError.postValue("This product is already Scanned and Picked\nPlease scan another");
            }
        });
    }

    public void returnItem(OrderConsumableItem item, BinNumber binNumber, int count) {
        doBeforeSync(EMPTY_TYPE, () -> {
            List<ConsumableBin> consumableBins = consumableBinService.byProductIdAndBin(item.getProductId(), binNumber);
            ConsumableBin consumableBin;

            if (CollectionUtils.isEmpty(consumableBins)) {
                consumableBin = new ConsumableBin();
                consumableBin.setBinNumber(binNumber);
                consumableBin.setQuantity(count);
                consumableBin.setAddingDate(DateFormatter.currentTimeDb());
                consumableBin.setProductId(item.getProductId());
                consumableBinService.insert(consumableBin);
            } else {
                consumableBin = consumableBins.get(0);
                consumableBin.setQuantity(consumableBin.getQuantity() + count);
                consumableBinService.update(consumableBin);
            }

            item.setPicked(item.getPicked() - count);
            consumableItemService.update(item);
            orderService.update(item.getOrder());
        });
    }

    public void returnItem(OrderRentalItem rentalItem, BinNumber binNumber) {
        doBeforeSync(EMPTY_TYPE, () -> {
            String uun = rentalItem.getUniqueUnitNumber();
            rentalItem.setUniqueUnitNumber(null);
            rentalItemService.update(rentalItem);

            RentalProductItem productItem = rentalItem
                    .getRentalProduct()
                    .getProductItems()
                    .stream()
                    .filter(rentalProductItem -> rentalProductItem.getStatus().equals(RentalStatus.CheckedOut.getId())
                            && rentalProductItem.getUniqueUnitNumber().equals(uun))
                    .collect(Collectors.toList()).get(0);

            productItem.setBinId(binNumber.getBinId());
            productItem.setStatus(RentalStatus.Available.getId());
            rentalProductService.update(productItem);
            orderService.update(rentalItem.getOrder());
        });
    }

    public int getTotalItemsCount() {
        AtomicInteger result = new AtomicInteger();
        order.getConsumableItems().forEach(consumableItem -> result.addAndGet(consumableItem.getQuantity()));
        order.getRentalItems().forEach(rentalItem -> result.addAndGet(1));
        return result.get();
    }

    public List<AggregatedRentalItem> getAggregatedRentalsList() {
        List<OrderRentalItem> items = order.getRentalItems();
        items.forEach(OrderRentalItem::refresh);
        return RentalItemService.getAggregatedRentalsList(items);
    }

    public List<OrderConsumableItem> getConsumableItems() {
        List<OrderConsumableItem> items = order.getConsumableItems();
        items.forEach(OrderConsumableItem::refresh);
        return items;
    }

    public Order getOrder() {
        return order;
    }

    public OrderItem getItem(OrderProduct product) {
        OrderItem item = null;
        if (order != null && CollectionUtils.isNotEmpty(order.getAllItems())) {
            for (OrderItem i : order.getAllItems()) {
                if (i.getProductId().equals(product.getProductId())) {
                    item = i;
                    break;
                }
            }
        }
        return item;
    }

    protected void deleteOrder() {
        if (isOrderHasShipOutBinItems()) {
            liveCantDeleteOrder.postValue(true);
        } else {
            addDisposable(rxCompletableCreate(() -> {
                if (order != null) {
                    orderService.delete(order);
                    order.getAllItems().forEach(it -> {
                        it.ifConsumable(consumableItemService::delete);
                        it.ifRental(rentalItemService::delete);
                    });
                }
            })
                    .subscribeOn(Schedulers.computation())
                    .subscribe(() -> {
                        liveRunSync.postValue(null);
                        liveDeleteOrder.postValue(true);
                    }, this::onError));
        }
    }

    protected List<OrderItem> getShipOutBinItems() {
        List<OrderItem> shipOutBinItems = new ArrayList<>();
        shipOutBinItems.addAll(getPickedRentals());
        shipOutBinItems.addAll(getPickedConsumables());
        return shipOutBinItems;
    }

    private boolean isOrderHasShipOutBinItems() {
        return !getPickedRentals().isEmpty() || !getPickedConsumables().isEmpty();
    }

    private void startRentPeriod(OrderRentalItem orderRentalItem) {
        if (orderRentalItem.getUniqueUnitNumber() != null) {
            orderRentalItem.setStartDate(DateFormatter.currentTimeDb());
            orderRentalItem.setDueDate(DateFormatter.endOfRentalPeriod());
            rentalItemService.update(orderRentalItem);
        }
    }

    private void doBeforeSync(String productType, Action action) {
        addDisposable(rxCompletableCreate(action)
                .subscribeOn(Schedulers.computation())
                .subscribe(() -> {
                            liveUpdateUi.postValue(productType);
                            liveRunSync.postValue(null);
                        },
                        this::onError));
    }

    private void getExistingBinsFromOrder() {
        if (order == null || CollectionUtils.isEmpty(order.getAllItems())) {
            return;
        }
        order.getAllItems().forEach(it -> {
            it.ifConsumable(consumable -> {
                List<ConsumableBin> bins = getBinsForPickUp(consumable.getConsumableProduct());
                bins.forEach(bin -> {
                    if (binNumbers.containsKey(bin.getBinNumber())) {
                        binNumbers.get(bin.getBinNumber()).add(it);
                    } else {
                        List<OrderItem> list = new ArrayList<>();
                        list.add(it);
                        binNumbers.put(bin.getBinNumber(), list);
                    }
                });
            });
            it.ifRental(rental -> {
                List<BinNumber> bins = getBinsForPickUp(rental.getRentalProduct());
                bins.forEach(bin -> {
                    if (binNumbers.containsKey(bin)) {
                        binNumbers.get(bin).add(it);
                    } else {
                        List<OrderItem> list = new ArrayList<>();
                        list.add(it);
                        binNumbers.put(bin, list);
                    }
                });
            });
        });
    }

    public void createBackorder() {
        shouldCreateNewOrder = false;
        List<OrderConsumableItem> items = order.getConsumableItems()
                .stream()
                .filter(OrderConsumableItem::isBackorder)
                .collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(items)) {
            Order backorder = new Order();
            int sum = calculateOrderSum(items);
            backorder.setOrderSum(sum);
            backorder.setCreatedByUserId(order.getCreatedByUserId());
            backorder.setOrderDate(DateFormatter.currentTimeDb());
            backorder.setDeliveryDate(DateFormatter.currentTimeDb());
            backorder.setCustomer(order.getCustomer());
            if (order.getAccountingCode() != null) {
                backorder.setAccountingCode(order.getAccountingCode());
            }
            backorder.setWebOrderNumber(order.getWebOrderNumber());
            backorder.setOrderStatusId(OrderStatus.pickUp.getId());
            String prefix = parametersService.getOrderPrefix();
            backorder.setOrderNumber(OrderNumberGenerator.getNext(orderService.getLastOrder(prefix), prefix));
            orderService.insert(backorder);
            items.forEach(it -> {
                if (it.getPicked() > 0) {
                    OrderConsumableItem newItem = new OrderConsumableItem();
                    newItem.setQuantity(it.getQuantity() - it.getPicked());
                    newItem.setOrderId(backorder.getOrderId());
                    newItem.isBackorder(true);
                    newItem.setPrice(it.getPrice());
                    newItem.setProductId(it.getProductId());
                    it.isBackorder(false);
                    consumableItemService.update(it);
                    consumableItemService.insert(newItem);
                } else {
                    it.setOrderId(backorder.getOrderId());
                    consumableItemService.update(it);
                }
            });
            order.resetConsumableItems();
            orderService.update(order);
            orderService.update(backorder);
        }
    }

    private int calculateOrderSum(List<OrderConsumableItem> items) {
        int sum = 0;
        for (OrderConsumableItem it : items) {
            if (it.getPicked() > 0) {
                int delta = it.getQuantity() - it.getPicked();
                sum += delta * it.getPrice();
            } else sum += it.getQuantity() * it.getPrice();
        }
        return sum;
    }

    public boolean hasBackorderItems() {
        return order.getConsumableItems()
                .stream()
                .anyMatch(OrderConsumableItem::isBackorder);
    }

    public void verifyOrderStatus() {
        Order order = orderService.getOrderById(this.order.getOrderId());
        isServerDeleteOrder.setValue(order.getOrderStatusId() == OrderStatus.closed.getId());
    }
}
