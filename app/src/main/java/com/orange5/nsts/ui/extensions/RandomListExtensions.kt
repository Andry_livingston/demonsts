package com.orange5.nsts.ui.extensions

import java.util.*

fun <T> getRandomList(products: List<T>, count: Int): List<T> {
    val newList: MutableList<T> = ArrayList()
    for (i in 1..count) {
        val randomIndex = getRandomIndex(products.size)
        newList.add(products[randomIndex])
    }
    return newList
}

fun getRandomIndex(qty: Int): Int {
    val rand = Random()
    return rand.nextInt(qty)
}