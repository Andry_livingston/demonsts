package com.orange5.nsts.ui.shipping

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.orange5.nsts.R
import com.orange5.nsts.service.bin.BinService
import com.orange5.nsts.ui.base.BaseViewHolder
import com.orange5.nsts.ui.shipping.repository.ReceivingNotice
import kotlinx.android.synthetic.main.item_shipping_receive.view.*

abstract class ShippingProcessAdapter : RecyclerView.Adapter<ShippingProcessAdapter.ShippingProcessVH>() {

    private var items: List<ReceivingNotice> = listOf()
    var query = ""

    fun submitList(items: List<ReceivingNotice>, query: String = "") {
        this.query = query
        this.items = items
        notifyDataSetChanged()
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ShippingProcessVH, position: Int) = holder.bind(items[position], query)

    protected fun inflate(parent: ViewGroup): View = LayoutInflater.from(parent.context).inflate(R.layout.item_shipping_receive, parent, false)

    abstract class ShippingProcessVH(view: View, private val listener: Listener) : RecyclerView.ViewHolder(view) {
        fun bind(item: ReceivingNotice, query: String) {
            with(itemView) {
                productDescription.text = BaseViewHolder.highlightBackground(item.productDescription, query)
                productNumber.text = BaseViewHolder.highlightBackground(item.productNumber, query)
                uun.isVisible = item.isRental()
                uun.text = context.getString(R.string.shipping_notice_uun, item.uniqueUnitNumber)
                productIcon.setImageResource(if (item.isRental()) R.drawable.ic_rental else R.drawable.ic_consumable)
                date.text = item.shippingDate()
                quantity.text = context.getString(R.string.shipping_notice_rental_quantity, count(item))
                bin.isVisible = item.bin != null && item.bin?.binId != null && item.bin?.binTypeId != BinService.RECEIVING_ID
                bin.text = context.getString(R.string.shipping_notice_rental_bin, item.bin?.binNum ?: "N/A")
                setOnClickListener { listener.onItemClick(item) }
            }
        }

        protected abstract fun count(item: ReceivingNotice): Int
    }

    interface Listener {
        fun onItemClick(item: ReceivingNotice) = Unit
    }
}