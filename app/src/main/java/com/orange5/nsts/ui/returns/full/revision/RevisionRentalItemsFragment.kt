package com.orange5.nsts.ui.returns.full.revision

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.ui.base.TwoPageAdapter
import com.orange5.nsts.ui.returns.RentalItemsFragment

class RevisionRentalItemsFragment : RentalItemsFragment() {

    override val viewModel: RevisionViewModel by activityViewModels { vmFactory }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter.isInspection = true
    }

    override val noDataMessage = R.string.activity_open_inspection_bin_empty_message

    override fun onItemClick(item: OrderRentalItem) {
        clearSubscriptions()
        scanningViewModel.releaseScanner()
        RevisionReturnDialog.newInstance(item, TwoPageAdapter.TabType.INIT, closeCallback = this)
                .show(parentFragmentManager, RevisionReturnDialog::class.java.simpleName)
    }

    override fun onDialogClosed() {
        scanningViewModel.setUpScanService()
        subscribe()
    }
}