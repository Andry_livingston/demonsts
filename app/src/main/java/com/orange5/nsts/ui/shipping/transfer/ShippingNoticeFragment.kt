package com.orange5.nsts.ui.shipping.transfer

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.observe
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.ShippingNotice
import com.orange5.nsts.ui.base.BaseListFragment
import com.orange5.nsts.ui.shipping.receiving.ReceivingActivity
import kotlinx.android.synthetic.main.fragment_refreshed_list.*

class ShippingNoticeFragment : BaseListFragment(), ShippingNoticeAdapter.Listener {

    private val viewModel by activityViewModels<ShippingNoticeViewModel> { vmFactory }

    private val adapter = ShippingNoticeAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.shippingNotice.observe(this, ::submitList)
        viewModel.liveProgress.observe(this, ::showLoading)
    }

    override val noDataMessage: Int = R.string.shipping_notice_empty_data_message

    override fun onRefresh() = viewModel.loadShippingNotices()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.adapter = adapter
    }

    private fun submitList(items: List<ShippingNotice>) {
        noData.isVisible = items.isEmpty()
        adapter.submitList(items)
    }

    override fun onReceiveClick(transferNumber: String) {
        startActivity(ReceivingActivity.intent(requireContext(), transferNumber))
    }
}
