package com.orange5.nsts.ui.base;

import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;

import com.orange5.nsts.util.Colors;

public abstract class BaseViewHolder {
    public static SpannableString highlightBackground(String destination, String highlight) {
        SpannableString str = new SpannableString(destination);

        String[] split = highlight.split("\\s+");

        String lowerCaseDestination = destination.toLowerCase();

        for (String word : split) {
            String lowerCaseWord = word.toLowerCase();
            int index = lowerCaseDestination.indexOf(lowerCaseWord);

            if (word.isEmpty() || index == -1) {
                continue;
            }

            while (index >= 0) {
                str.setSpan(new BackgroundColorSpan(Colors.HIGHLIGHT),
                        index, index + word.length(), 0);
                index = lowerCaseDestination.indexOf(lowerCaseWord, index + 1);
            }
        }
        return str;
    }

    public static SpannableString highlightTextColor(String destination, String highlight, int color) {
        SpannableString str = new SpannableString(destination);

        int index = destination.toLowerCase().indexOf(highlight.toLowerCase());

        if (highlight.isEmpty() || index == -1) {
            return str;
        }
        str.setSpan(new ForegroundColorSpan(color),
                index, index + highlight.length(), 0);
        return str;
    }
}
