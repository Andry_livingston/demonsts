package com.orange5.nsts.ui.returns.quick

import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.data.db.entities.RentalProductItem
import com.orange5.nsts.service.bin.BinService
import com.orange5.nsts.service.order.items.OrderRentalItemService
import com.orange5.nsts.service.rental.RentalProductItemService
import com.orange5.nsts.service.rental.RentalStatus
import com.orange5.nsts.service.rentalReturn.RentalReturnService
import com.orange5.nsts.ui.base.TwoPageAdapter
import com.orange5.nsts.ui.returns.ReturnViewModel
import com.orange5.nsts.util.format.DateFormatter
import javax.inject.Inject

class QuickReturnViewModel @Inject constructor(private val returnService: RentalReturnService,
                                               private val orderRentalItemService: OrderRentalItemService,
                                               private val rentalProductService: RentalProductItemService,
                                               binService: BinService) : ReturnViewModel(orderRentalItemService, binService) {

    init {
        fetchReturnItems(true)
        loadBins()
    }

    override fun handleTabSelection(tabType: TwoPageAdapter.TabType) {
        var title = "Quick Rental Return"
        when (tabType) {
            TwoPageAdapter.TabType.INIT -> {
                setCount(allRentalItems.value?.size ?: 0)
                setTitle(title)
            }
            TwoPageAdapter.TabType.RESULT -> {
                val items = allReturnItems.value
                when {
                    items?.size == 1 -> title = "${items[0].returnedByCustomer.fName} ${items[0].returnedByCustomer.lName}"
                    items != null && items.size > 1 -> title = "Multiple customers"
                }
                setCount(items?.size ?: 0)
                setBadge(items?.size ?: 0)
                setTitle(title)
            }
        }
    }

    override fun createReturn(signature: ByteArray) {
        rentalReturn.apply {
            completeDate = DateFormatter.currentTimeDb()
            signImage = signature
            runDbTransaction {
                returnService.insert(this)

                mappedReturns.value?.forEach { item, product ->
                    item.rentalReturnId = rentalReturnId
                    item.returnDate = completeDate
                    product.status = RentalStatus.Available.id

                    orderRentalItemService.update(item)
                    rentalProductService.update(product)
                }
            }
        }
    }

    override fun loadRentalReturnItem(items: List<OrderRentalItem>, showAll: Boolean)
            : MutableMap<OrderRentalItem, RentalProductItem> {
        val mapOrderRentalItemByUun = mutableMapOf<String, OrderRentalItem>()
        items.forEach {
            if (!mapOrderRentalItemByUun.containsKey(it.uniqueUnitNumber)) {
                mapOrderRentalItemByUun[it.uniqueUnitNumber] = it
            } else if (!DateFormatter.compare(mapOrderRentalItemByUun
                            .getValue(it.uniqueUnitNumber).startDate,it.startDate)) {
                mapOrderRentalItemByUun[it.uniqueUnitNumber] = it

            }
        }
        val map = mutableMapOf<OrderRentalItem, RentalProductItem>()
        mapOrderRentalItemByUun.forEach {
            rentalProductService.loadItemByUUN(it.key)?.let { rental -> map[it.value] = rental }
        }
        return map
    }
}

