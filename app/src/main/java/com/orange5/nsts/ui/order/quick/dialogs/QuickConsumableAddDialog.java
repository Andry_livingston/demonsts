package com.orange5.nsts.ui.order.quick.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.Toast;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.entities.ConsumableProduct;
import com.orange5.nsts.service.order.BinQuantitySelection;
import com.orange5.nsts.ui.base.BaseActivity;
import com.orange5.nsts.ui.order.quick.QuickOrderViewModel;
import com.orange5.nsts.ui.order.search.ProductWrapper;
import com.orange5.nsts.util.CollectionUtils;
import com.orange5.nsts.util.view.dialogs.QuantityInputDialog;
import com.orange5.nsts.util.view.picker.PickerView;
import com.orange5.nsts.util.view.picker.ValuePickerAdapter;

import java.util.List;
import java.util.function.BiConsumer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class QuickConsumableAddDialog {
    private final AlertDialog dialog;

    private static final int MAX_OVERSELL = 10001;
    @BindView(R.id.leftPicker)
    PickerView leftPicker;
    @BindView(R.id.rightPicker)
    PickerView rightPicker;
    @BindView(R.id.actionButton)
    Button addButton;
    @BindView(R.id.customQuantity)
    ImageButton inputQuantity;
    @BindView(R.id.isBackorder)
    CheckBox isBackorder;

    private final Context context;
    private final boolean hasBackorder;
    private List<BinQuantitySelection> binQuantitySelections;
    private BinQuantitySelection oversellBin = new BinQuantitySelection(QuickOrderViewModel.OVERSELL,
            MAX_OVERSELL, 0);
    private BiConsumer<List<BinQuantitySelection>, Boolean> selectionConsumer;
    private int totalSelected;
    private int totalQuantity;
    private int availableQuantity;
    private boolean editable;
    private Integer currentSelectedQuantity;
    private String currentSelectedBin;
    private Handler defferHandler = new Handler();

    public static QuickConsumableAddDialog with(Context context, ProductWrapper wrapper,
                                                QuickOrderViewModel openOrderService,
                                                boolean editableMode) {
        return new QuickConsumableAddDialog(context, wrapper, openOrderService, editableMode);
    }

    private void updateOversellBin() {
        binQuantitySelections.forEach(binQuantitySelection -> {
            if (QuickOrderViewModel.OVERSELL.equals(binQuantitySelection.getBinNumber())) {
                oversellBin = binQuantitySelection;
            }
        });
    }

    private QuickConsumableAddDialog(Context context, ProductWrapper wrapper,
                                     QuickOrderViewModel openOrderService, boolean editableMode) {
        this.context = context;
        this.editable = editableMode;
        this.hasBackorder = wrapper.isBackorder();
        ConsumableProduct consumableProduct = (ConsumableProduct) wrapper.getProduct();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.quick_consumable_add_dialog_consumable_add, consumableProduct.getProductNumber()));
        View view = View.inflate(BaseActivity.getActive(), R.layout.dialog_add_consumable, null);
        ButterKnife.bind(this, view);
        builder.setView(view);

        binQuantitySelections = openOrderService.getSelectedItemQuantity(consumableProduct);
        openOrderService.getSelectedProductQuantity(consumableProduct)
                .forEach(binQuantitySelection -> totalQuantity
                        += binQuantitySelection.getMaxQuantity());
        updateOversellBin();
        currentSelectedQuantity = 0;
        if (binQuantitySelections.size() > 0) {
            currentSelectedBin = binQuantitySelections.get(0).getBinNumber();
        } else {
            currentSelectedBin = configEmptyBin();
        }
        availableQuantity = new ProductWrapper(consumableProduct).getAvailableQuantity();
        updateBins();
        updateQuantities(currentSelectedBin);
        dialog = builder.create();
        show();
    }

    private String configEmptyBin() {
        binQuantitySelections.add(oversellBin);
        return oversellBin.getBinNumber();
    }

    public QuickConsumableAddDialog selectionConsumer(BiConsumer<List<BinQuantitySelection>, Boolean> selectionConsumer) {
        this.selectionConsumer = selectionConsumer;
        return this;
    }

    @OnClick(R.id.actionButton)
    void addProduct() {
        selectionConsumer.accept(binQuantitySelections, isBackorder.isChecked());
        dialog.dismiss();
    }

    @OnClick(R.id.customQuantity)
    void actionInputQuantity() {
        QuantityInputDialog.show(context, currentSelectedQuantity, quantity -> {
            if (quantity < 0) {
                Toast.makeText(context, R.string.invalid_quantity, Toast.LENGTH_LONG).show();
                rightPicker.setSelectedItemPosition(0);
                return;
            }
            if (currentSelectedBin.equals(QuickOrderViewModel.OVERSELL)) {
                oversellBin.setMaxQuantity(quantity + 10);
                updateQuantities(currentSelectedBin);
                rightPicker.setSelectedItemPosition(quantity);
                return;
            }

            Integer maxQuantity = getSelection(currentSelectedBin).getMaxQuantity();
            int difference = maxQuantity - quantity;
            if (difference >= 0) {
                rightPicker.setSelectedItemPosition(quantity);

                return;
            }
            rightPicker.setSelectedItemPosition(maxQuantity);
            Toast.makeText(context, context.getString(R.string.oversell_quantity, maxQuantity, currentSelectedBin), Toast.LENGTH_LONG).show();
        });
    }

    private boolean oversellEnabled() {
        return totalQuantity == 0//false
                ||
                totalSelected >= totalQuantity;
    }

    private void updateBins() {
        updateTotalSelected();
        if (oversellEnabled()) {
            if (!binQuantitySelections.contains(oversellBin)) {
                binQuantitySelections.add(oversellBin);
            }
            leftPicker.setOverflowPosition(binQuantitySelections.size() - 1);
        } else {
            oversellBin.setSelectedQuantity(0);
            binQuantitySelections.remove(oversellBin);
            leftPicker.setOverflowPosition(binQuantitySelections.size());
        }
        ValuePickerAdapter<BinQuantitySelection> binAdapter
                = new ValuePickerAdapter<>(binQuantitySelections);
        binAdapter.selectedItemConsumer(binQuantitySelection -> {
            currentSelectedBin = binQuantitySelection.getBinNumber();
            updateQuantities(currentSelectedBin);
        }).attachTo(leftPicker);
        if (oversellBin.getSelectedQuantity() != 0) {
            leftPicker.setSelectedItemPosition(binQuantitySelections.indexOf(oversellBin));
        }
        updateButton();
        setBackorder();
    }

    private void updateButton() {
        if (totalSelected == 0 && !editable) {
            addButton.setText(R.string.common_no_items_selected);
            addButton.setEnabled(false);
            return;
        }
        addButton.setEnabled(true);
        addButton.setText(context.getString(R.string.edit_quantity_dialog_add_d_s_to_order,
                totalSelected, totalSelected > 1 ? "items" : "item"));
    }

    private void updateTotalSelected() {
        totalSelected = binQuantitySelections.stream()
                .mapToInt(BinQuantitySelection::getSelectedQuantity).sum();
    }

    private void updateQuantities(String binNumber) {
        BinQuantitySelection selectedBin = getSelection(binNumber);
        rightPicker.setOverflowPosition(binNumber.equals(QuickOrderViewModel.OVERSELL)
                ? 0 : availableQuantity + 1);

        ValuePickerAdapter<Integer> quantityAdapter
                = new ValuePickerAdapter<>(CollectionUtils.rangeClosedList(
                0, selectedBin.getMaxQuantity()));

        quantityAdapter.selectedItemConsumer(quantity -> {
            selectedBin.setSelectedQuantity(quantity);
            currentSelectedQuantity = quantity;
            delayedUpdate();
        });
        quantityAdapter.attachTo(rightPicker);

        if (selectedBin.getBinNumber().equals(QuickOrderViewModel.OVERSELL)
                && selectedBin.getSelectedQuantity() < 1) {
            rightPicker.setSelectedItemPosition(0);
        } else {
            rightPicker.setSelectedItemPosition(selectedBin.getSelectedQuantity() == 0 ?
                    1 : selectedBin.getSelectedQuantity());
        }

    }

    private void setBackorder() {
        if(totalSelected > totalQuantity) {
            isBackorder.setVisibility(View.VISIBLE);
            isBackorder.setChecked(hasBackorder);
        }else {
            isBackorder.setVisibility(View.GONE);
            isBackorder.setChecked(false);
        }
    }

    private BinQuantitySelection getSelection(String binNumber) {
        return binQuantitySelections.stream().filter(binQuantitySelection ->
                binNumber.equals(binQuantitySelection.getBinNumber())).findFirst().get();
    }

    private void delayedUpdate() {
        defferHandler.removeCallbacksAndMessages(null);
        defferHandler.postDelayed(this::updateBins, 50);
    }

    public void show() {
        dialog.show();
    }

    private boolean isShowing() {
        return dialog != null && dialog.isShowing();
    }

    public void dismiss() {
        if (isShowing()) {
            dialog.dismiss();
        }
    }
}
