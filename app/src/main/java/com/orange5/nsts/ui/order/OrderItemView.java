package com.orange5.nsts.ui.order;

import android.content.Context;
import android.text.SpannableString;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.orange5.nsts.R;
import com.orange5.nsts.app.App;
import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.data.db.entities.OrderRentalItem;
import com.orange5.nsts.service.order.OrderType;
import com.orange5.nsts.ui.extensions.OrderExtensionsKt;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderItemView extends FrameLayout {

    @BindView(R.id.productDescription)
    TextView itemName;
    @BindView(R.id.productNumber)
    TextView itemNumber;
    @BindView(R.id.itemQuantity)
    TextView itemQuantity;
    @BindView(R.id.itemType)
    ImageView itemType;
    @BindView(R.id.itemPrice)
    TextView itemPrice;
    @BindView(R.id.availability)
    TextView itemAvailability;
    @BindView(R.id.sectionHeader)
    TextView sectionHeader;
    @BindView(R.id.bins)
    TextView bins;
    @BindView(R.id.isBackorder)
    CheckBox isBackorder;

    @BindView(R.id.bin_label)
    TextView binLabel;
    OrderType orderType;

    public OrderItemView(@NonNull Context context, OrderType orderType) {
        this(context, orderType, null);
    }

    public OrderItemView(@NonNull Context context, OrderType orderType,
                         @Nullable AttributeSet attrs) {
        this(context, orderType, attrs, 0);
    }

    public OrderItemView(@NonNull Context context, OrderType orderType,
                         @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, orderType, attrs, defStyleAttr, 0);
    }

    public OrderItemView(@NonNull Context context, OrderType orderType,
                         @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.orderType = orderType;
        inflate(context, R.layout.order_selected_item, this);
        ButterKnife.bind(this, this);
    }

    public void fill(OrderItem item) {
        fill(item, "");
    }

    public void fill(OrderItem item, String highlight) {
        item.__setDaoSession(App.getInstance().daoSession);
        isBackorder.setVisibility(item.isConsumable() && item.isOversell() ? VISIBLE : GONE);
        OrderItemViewHolder viewModel = new OrderItemViewHolder(getContext(), item);
        itemName.setText(viewModel.getProductDescription(highlight));
        itemNumber.setText(viewModel.getProductNumber(highlight));
        itemType.setBackgroundResource(OrderExtensionsKt.orderIcon(item));
        itemPrice.setText(viewModel.getFormattedPrice());
        itemQuantity.setText(viewModel.getFormattedQuantity());
        item.ifConsumable(it -> this.isBackorder.setChecked(it.isBackorder()));
        SpannableString availability = viewModel.getAvailableAndOversell();
        if (!availability.toString().isEmpty()) {
            itemAvailability.setVisibility(VISIBLE);
            itemAvailability.setText(availability);
        } else {
            itemAvailability.setVisibility(GONE);
        }

        updateLabel(item, viewModel);
    }

    private void updateLabel(OrderItem item, OrderItemViewHolder viewModel) {
        if (orderType.equals(OrderType.QUICK_ORDER)
                && !viewModel.ifItemConsumable()) {
            setSectionLabel(getContext().getString(R.string.common_uun),
                    ((OrderRentalItem) item).getUniqueUnitNumber());
        } else {
            setSectionLabel(getContext().getString(R.string.order_item_bins), viewModel.getBinQuantities());
        }
    }

    private void setSectionLabel(String label, String text) {
        binLabel.setText(label);
        bins.setText(text);
    }

    public void showSectionHeader(String sectionTitle) {
        sectionHeader.setText(sectionTitle);
        sectionHeader.setVisibility(VISIBLE);
    }

    public void hideSectionHeader() {
        sectionHeader.setVisibility(GONE);
    }

}