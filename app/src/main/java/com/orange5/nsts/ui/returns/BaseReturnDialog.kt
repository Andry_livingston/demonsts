package com.orange5.nsts.ui.returns

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.BinNumber
import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.scan.ScanningDialog
import com.orange5.nsts.service.bin.BinService.*
import com.orange5.nsts.service.rental.RentalStatus
import com.orange5.nsts.ui.base.TwoPageAdapter.TabType
import com.orange5.nsts.ui.base.TwoPageAdapter.TabType.INIT
import com.orange5.nsts.ui.returns.quick.ReturnInformation
import kotlinx.android.synthetic.main.dialog_rental_return.*
import kotlinx.android.synthetic.main.view_spinner.*
import com.orange5.nsts.util.view.picker.ValuePickerAdapter

abstract class BaseReturnDialog : ScanningDialog(), SearchView.OnQueryTextListener {

    abstract val viewModel: ReturnViewModel
    abstract val args: Bundle?

    private var selectedBin: BinNumber? = null
    private var type = INIT
    private var initialPosition = 0

    override fun getLayoutResourceId(): Int = R.layout.dialog_rental_return

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.liveBins.observe(this, Observer(::setUpPicker))
        viewModel.orderItem.observe(this, Observer(::setUpView))
        viewModel.binPosition.observe(this, Observer(::scrollPickerToBinPosition))
        scanningViewModel.liveBinScanned.observe(this, Observer(::setScannedBin))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val itemId = args?.getString(ORDER_ITEM_ID) ?: "layout_dummy"
        type = args?.getSerializable(TYPE_ID) as? TabType ?: INIT
        handleWasScanned(args?.getBoolean(WAS_SCANNED_ID) ?: false)
        viewModel.loadOrderItem(itemId, type)
        handleType(type)
        setListeners()
        val statusTitles = RentalStatus.values().map { it.getName() }
        ArrayAdapter(requireContext(), R.layout.item_spinner_top, statusTitles).also {
            it.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = it
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean = false

    override fun onQueryTextChange(newText: String?): Boolean {
        viewModel.searchBin(newText, initialPosition)
        return false
    }

    private fun scrollPickerToBinPosition(position: Int) {
        picker.selectedItemPosition = position
    }

    private fun setScannedBin(bin: BinNumber) {
        viewModel.searchScannedBin(bin.binId)
    }

    private fun handleWasScanned(wasScanned: Boolean) {
        wasDamaged.isVisible = wasScanned
        wasDamaged.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                val repairPosition = viewModel.liveBins.value?.indexOfLast { it.binNum == REPAIR } ?: 0
                picker.selectedItemPosition = repairPosition
                handleDamaged(isChecked)
            }
        }
    }

    private fun handleType(type: TabType) =
            when (type) {
                INIT -> ok.text = getString(R.string.common_return)
                TabType.RESULT -> ok.text = getString(R.string.update)
            }

    private fun setListeners() {
        searchBin.setOnQueryTextListener(this)
        cancel.setOnClickListener { dismiss() }
        ok.setOnClickListener {
            val status =
                    if (statusAvailable.isVisible) retrieveStatus(statusAvailable.text.toString())
                    else retrieveStatus(spinner.selectedItem as? String ?: "")
            val info = ReturnInformation(selectedBin, status, remark.text.toString())
            if (type == INIT) viewModel.onItemReturned(info)
            else viewModel.onUpdateReturnedItem(info)
            dismiss()
        }
    }

    private fun setUpView(item: OrderRentalItem?) {
        item?.let {
            productNumber.setValueText(it.productNumber)
            uun.setValueText(it.uniqueUnitNumber)
            remark.setText(it.rentalReturnNotes)
            handleDamaged(it.wasDamaged)
            setBinPosition(it)
        } ?: errorOccur(R.string.rental_return_order_item_does_not_exists)
    }

    private fun setUpPicker(bins: List<BinNumber>?) {
        if (!bins.isNullOrEmpty()) {
            picker.isUsedForBins = true
            val repairPosition = bins.indexOfLast { it.binNum == REPAIR }
            val adapter = ValuePickerAdapter(bins)
            adapter.selectedItemConsumer { handleSelectedBin(it) }
            adapter.attachTo(picker)
            picker.selectedItemPosition = repairPosition
        }
    }

    private fun setBinPosition(item: OrderRentalItem) {
        if (item.wasDamaged) return
        initialPosition = viewModel.liveBins.value?.indexOfLast { it.binNum == item.scannedBin?.binNum } ?: 0
        picker.selectedItemPosition = initialPosition
    }

    private fun handleSelectedBin(bin: BinNumber) {
        selectedBin = bin
        setSpinnerData(bin)
        statusAvailable.isVisible = !SYSTEM_BINS_ID.contains(bin.binTypeId)
        statusAvailable.text = RentalStatus.Available.name
    }

    private fun setSpinnerData(bin: BinNumber) {
        val isSystemBin = SYSTEM_BINS_ID.contains(bin.binTypeId)
        spinnerContainer.isVisible = isSystemBin
        if (isSystemBin) {
            spinner.setSelection(when (bin.binTypeId) {
                REPAIR_ID -> RentalStatus.InRepair.id - 1
                SCRAPPED_ID -> RentalStatus.Scrapped.id - 1
                else -> spinner.selectedItemPosition
            })
        }
    }

    private fun handleDamaged(wasDamaged: Boolean) {
        if (wasDamaged) spinner.setSelection(RentalStatus.InRepair.id - 1)
        else picker.selectedItemPosition = 0
    }

    private fun errorOccur(@StringRes errorMessage: Int) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
        dismiss()
    }

    private fun retrieveStatus(status: String) = RentalStatus.values().find { it.getName() == status }
            ?: RentalStatus.Available

    companion object {
        const val ORDER_ITEM_ID = "order_item_id_key"
        const val WAS_SCANNED_ID = "was_scanned_key"
        const val TYPE_ID = "return_tab_type_key"
    }
}
