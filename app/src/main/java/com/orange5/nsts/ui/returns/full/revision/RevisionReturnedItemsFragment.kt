package com.orange5.nsts.ui.returns.full.revision

import androidx.fragment.app.activityViewModels
import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.ui.base.TwoPageAdapter
import com.orange5.nsts.ui.returns.ReturnedItemsFragment

class RevisionReturnedItemsFragment : ReturnedItemsFragment() {

    override val viewModel: RevisionViewModel by activityViewModels { vmFactory }

    override fun onItemClick(item: OrderRentalItem) {
        RevisionReturnDialog.newInstance(item, TwoPageAdapter.TabType.RESULT, closeCallback = this)
                .show(requireFragmentManager(), RevisionReturnDialog::class.java.simpleName)
    }
}