package com.orange5.nsts.ui.shipping.takeaway

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.BinNumber
import com.orange5.nsts.service.bin.BinService
import com.orange5.nsts.service.consumable.ConsumableProductService
import com.orange5.nsts.service.rental.RentalProductService
import com.orange5.nsts.ui.base.ActionLiveData
import com.orange5.nsts.ui.shipping.ShippingProcessViewModel
import com.orange5.nsts.ui.shipping.discrepancy.repository.DiscrepancyRepository
import com.orange5.nsts.ui.shipping.repository.Quantity
import com.orange5.nsts.ui.shipping.repository.ReceivingNotice
import com.orange5.nsts.ui.shipping.repository.ReceivingRepository
import javax.inject.Inject

class TakeAwayViewModel @Inject constructor(
        private val receivingRepository: ReceivingRepository,
        private val consumableService: ConsumableProductService,
        private val rentalService: RentalProductService,
        private val binService: BinService,
        private val discrepancyRepository: DiscrepancyRepository) : ShippingProcessViewModel(discrepancyRepository) {

    val onFinishReceivingNotice: MutableLiveData<Unit> = ActionLiveData()
    val processedNotices = MutableLiveData<List<ReceivingNotice>>()
    val processedProduct = MutableLiveData<ReceivingNotice>()
    val onBinsLoaded = MutableLiveData<List<BinNumber>>()
    val onNoticeLoaded = MutableLiveData<Pair<ReceivingNotice?, Int>>()
    val binPosition = MutableLiveData<Int>()
    val onConsumableScanned = ActionLiveData<ReceivingNotice>()
    val onRentalScanned = ActionLiveData<ReceivingNotice>()
    override val receivingBadges: LiveData<Int> = processedNotices.map { it.sumBy { notice -> notice.quantity.processed } }

    override fun finishReceivingProcess() {
        liveProgress.value = true
        runJobIO({
            val takeAwayItems = processedNotices.value
            if (!takeAwayItems.isNullOrEmpty()) receivingRepository.processTakeAwayItems(takeAwayItems)
            processDiscrepancy()
        }, { onFinishReceivingNotice.postValue(Unit) })
    }

    private fun processDiscrepancy() {
        val items = discrepancyItems.value ?: return
        if (items.isNotEmpty()) {
            discrepancyRepository.saveDiscrepancy(items)
            discrepancyRepository.updateProducts(items)
        }
    }

    fun loadItems() {
        liveProgress.value = true
        callJobComputation(
                { receivingRepository.loadReceivingItems() },
                { receivingItems.postValue(it) })
    }

    fun loadBins() {
        callJobComputation(
                { binService.binsForShippingNotice() },
                { onBinsLoaded.postValue(it) })
    }

    fun searchBin(query: String?, initialPosition: Int) {
        var foundPosition = initialPosition
        val bins = onBinsLoaded.value ?: return
        if (query != null && query.isNotEmpty()) {
            val position = bins.indexOfFirst { it.binNum.contains(query, ignoreCase = true) }
            if (position != -1) {
                foundPosition = position
            }
        }
        binPosition.value = foundPosition
    }

    fun searchScannedBin(binId: String) {
        val index = onBinsLoaded.value?.indexOfFirst { it.binId == binId }
        if (index == -1) return
        else binPosition.value = index
    }

    override fun refresh() {
        super.refresh()
        processedNotices.value?.let { processedNotices.value = it }
    }

    override fun loadReceivingNotice(noticeId: String) {
        callJobComputation({
            val notice = receivingItems.value?.find { it.id == noticeId }
            val defaultBin =
                    if (notice == null) 0
                    else {
                        val binId = consumableService.byProductNumber(notice.productNumber)?.defaultBinId
                        val index = onBinsLoaded.value?.indexOfFirst { it.binId == binId }
                        if (index == null || index == -1) 0 else index
                    }
            return@callJobComputation Pair(notice, defaultBin)
        }, {
            onNoticeLoaded.postValue(it)
        })
    }

    fun loadProcessedProduct(noticeId: String) {
        processedProduct.value = processedNotices.value?.find { it.id == noticeId }
    }

    fun takeAwayRental(bin: BinNumber?, uun: String) {
        val notice = receivingItems.value?.find { it.uniqueUnitNumber == uun } ?: return
        notice.quantity = Quantity(1, 1, 0, 0)
        notice.isProcessed(true)
        notice.setBin(bin)
        processedNotices.value = setProcessedItems(bin, 1, notice)
        refresh()
    }

    fun returnRental(notice: ReceivingNotice) {
        val product = receivingItems.value?.find { it.uniqueUnitNumber == notice.uniqueUnitNumber } ?: return
        val processed = processedNotices.value as? MutableList ?: mutableListOf()
        if (processed.isNotEmpty()) processed.remove(notice)
        product.quantity = Quantity(1, 0, 0, 1)
        product.isProcessed(false)
        product.setBin(binService.receivingBin())
        refresh()
    }

    fun takeAwayConsumable(bin: BinNumber?, quantity: Int) {
        val notice = onNoticeLoaded.value?.first ?: return
        notice.takeAwayProcessed(quantity)
        processedNotices.value = setProcessedItems(bin, quantity, notice)
        refresh()
    }

    fun onReturnConsumable(selectedQuantity: Int) {
        val product = processedProduct.value ?: return
        val processed = processedNotices.value as? MutableList ?: mutableListOf()
        val notice = receivingItems.value?.find { it.productNumber == product.productNumber }
        product.returned(selectedQuantity)
        notice?.takeAwayReturn(selectedQuantity)
        if (product.quantity.processed == 0) processed.remove(product)
        refresh()
    }

    fun loadRentalProduct(uun: String?) {
        if (uun == null) return
        val notice = receivingItems.value?.find { uun == it.uniqueUnitNumber } ?: return
        val binId = rentalService.byProductNumber(notice.productNumber)?.defaultBinId
        val index = onBinsLoaded.value?.indexOfFirst { binId == it.binId }
        if (index != -1) binPosition.value = index
    }

    fun onBarcodeScanned(code: String) {
        receivingItems.value?.forEach { notice ->
            if (notice.isRental()) {
                if ((notice.uniqueUnitNumber == code || notice.productNumber == code) && notice.quantity.remaining > 0) {
                    onRentalScanned.value = notice
                    return@onBarcodeScanned
                }
            } else {
                if (notice.productNumber == code && notice.quantity.remaining > 0) {
                    onConsumableScanned.value = notice
                    return@onBarcodeScanned
                }
            }
        }
        onScanFails(R.string.take_away_scanning_item_not_found)
    }

    private fun setProcessedItems(bin: BinNumber?, quantity: Int, notice: ReceivingNotice): MutableList<ReceivingNotice> {
        val processed = processedNotices.value as? MutableList ?: mutableListOf()
        val alreadyProcessed = processed.find { it.productNumber == notice.productNumber && it.bin?.binId == bin?.binId }
        if (alreadyProcessed != null) {
            val imported = alreadyProcessed.quantity.imported + quantity
            alreadyProcessed.quantity = Quantity(imported, imported, 0)
        } else {
            val processedNotice = ReceivingNotice.from(notice)
            processedNotice.setBin(bin)
            processedNotice.quantity = Quantity(quantity, quantity, 0)
            processed.add(processedNotice)
        }
        return processed
    }
}