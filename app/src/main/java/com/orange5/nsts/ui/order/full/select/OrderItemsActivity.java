package com.orange5.nsts.ui.order.full.select;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.orange5.nsts.R;
import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.data.db.base.OrderProduct;
import com.orange5.nsts.data.db.entities.ConsumableProduct;
import com.orange5.nsts.data.db.entities.Order;
import com.orange5.nsts.data.db.entities.RentalProduct;
import com.orange5.nsts.data.local.OrderWrapper;
import com.orange5.nsts.data.preferencies.RawSharedPreferences;
import com.orange5.nsts.scan.ScanningActivity;
import com.orange5.nsts.service.order.OrderType;
import com.orange5.nsts.service.product.ProductSearchFilter;
import com.orange5.nsts.ui.extensions.ContextExtensionsKt;
import com.orange5.nsts.ui.extensions.ViewExtensionsKt;
import com.orange5.nsts.ui.home.BottomMenuDialog;
import com.orange5.nsts.ui.order.EditQuantityDialog;
import com.orange5.nsts.ui.order.OrderItemView;
import com.orange5.nsts.ui.order.full.pickup.PickUpActivity;
import com.orange5.nsts.ui.order.search.ProductWrapper;
import com.orange5.nsts.ui.order.search.SearchProductDialog;
import com.orange5.nsts.util.CollectionUtils;
import com.orange5.nsts.util.UI;
import com.orange5.nsts.util.adapters.recycler.RecyclerAdapterBuilder;
import com.orange5.nsts.util.format.CurrencyFormatter;
import com.orange5.nsts.util.view.dialogs.Dialogs;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;

import static android.widget.Toast.LENGTH_SHORT;
import static com.orange5.nsts.ui.order.full.pickup.PickUpActivity.ORDER_ID;


public class OrderItemsActivity extends ScanningActivity implements BottomMenuDialog.Listener  {

    @Inject
    RawSharedPreferences sharedPreferences;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.orderTotals)
    TextView orderTotals;
    @BindView(R.id.customerName)
    TextView customerName;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.buttonAddTenProduct)
    Button addTenProduct;

    private EditQuantityDialog dialog;
    private OrderItemViewModel viewModel;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_order;
    }

    @Override
    protected int getMenuResourceId() {
        return R.menu.finish_order_menu;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = getVMFromActivity(OrderItemViewModel.class);
        viewModel.onLoadOrderFail.observe(this, (id -> forceCloseIfError(getString(R.string.logging_error_messages_order_from_db_is_null, id))));
        viewModel.onLoadOrderSuccess.observe(this, this::onOrderLoaded);
        viewModel.onViewUpdated.observe(this, this::updateView);
        viewModel.liveRunSync.observe(this, Void -> runSync());
        viewModel.onFinishOrder.observe(this, this::finishOrder);
        viewModel.liveScannedNewProduct.observe(this, this::showSearchResult);
        viewModel.liveScannedExistingProduct.observe(this, it -> showAdjustQuantityDialog(0, it));
        viewModel.isStressTestEnabled.observe(this,this::activateStressButton);
        getScanningViewModel().liveRentalScanned.observe(this, this::onRentalScanned);
        getScanningViewModel().liveConsumableScanned.observe(this, this::onConsumableScanned);
        setOrder();
        enableFloatingActionButton(v -> showProductDialog());
        ViewExtensionsKt.hideOnScroll(fab, recyclerView);
    }

    private void activateStressButton(boolean isShow) {
        if (isShow) {
            addTenProduct.setVisibility(View.VISIBLE);
            addTenProduct.setOnClickListener(view -> {
                viewModel.add10Product();

            });
        }
    }

    protected void enableFloatingActionButton(View.OnClickListener clickListener) {
        fab.setOnClickListener(clickListener);
        fab.setVisibility(View.VISIBLE);
        fab.post(() -> fab.animate().y(contentView.getMeasuredHeight() - 2 * fab.getMeasuredHeight())
                .setDuration(300).start());
    }

    private void setOrder() {
        String orderId = getIntent().getStringExtra(ORDER_ID);
        if (orderId == null) {
            forceCloseIfError(getString(R.string.logging_error_messages_order_id_is_null, this.getClass().getSimpleName()));
            return;
        }
        viewModel.loadExisting(orderId);
    }

    private void onOrderLoaded(Order order) {
        setTitle("Order #" + order.getOrderNumber());
        customerName.setText(order.getCustomer().toString());
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        if (item.getItemId() == R.id.finish) {
            finishOrder();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onRentalScanned(RentalProduct product) {
        handleScannedProduct(product);
    }

    public void onConsumableScanned(ConsumableProduct product) {
        handleScannedProduct(product);
    }

    private void showProductDialog() {
        SearchProductDialog.show(viewModel.getSelectedItems(), this::showSearchResult,
                new ProductSearchFilter(sharedPreferences), OrderType.FULL_ORDER);
    }

    private void showSearchResult(ProductWrapper product) {
        if (product.hasError()) {
            ContextExtensionsKt.toast(this, product.getMessage(), LENGTH_SHORT);
        } else {
            if (dialog != null && dialog.isShowing()) dialog.dismiss();
            dialog = EditQuantityDialog.create(product,
                    (quantity, isBackorder) -> viewModel.addProduct(product.getProduct(), quantity, isBackorder));
        }
    }

    private void handleScannedProduct(OrderProduct orderProduct) {
        ProductWrapper product = new ProductWrapper(orderProduct);
        int totalQuantity = 0;
        for (Map.Entry<String, Integer> entry : product.getProduct().getBinInformation().getCachedProductBinCount().entrySet()) {
            Integer binQuantity = entry.getValue();
            totalQuantity += binQuantity;
        }

        int selectedQuantity = getSelectedQuantity(product.getProduct());
        if (totalQuantity == 0) {
            if (!product.isConsumable()) {
                product.setError(R.string.warning_order_rental_zero_quantity_availability);
            }
            viewModel.onScanProduct(product);
            return;
        }
        if (selectedQuantity == product.getTotalQuantity()) {
            if (!product.isConsumable()) {
                product.setError(R.string.warning_rental_all_products_already_are_added);
            }
            viewModel.onScanProduct(product);
            return;
        }
        product.setAvailableQuantity(selectedQuantity);
        viewModel.onScanProduct(product);
    }

    private int getSelectedQuantity(OrderProduct product) {
        Integer quantity = getExistingItems(viewModel.getSelectedItems()).get(product.getProductId());
        return quantity == null ? 0 : quantity;
    }

    private Map<String, Integer> getExistingItems(List<OrderItem> items) {
        Map<String, Integer> map = new HashMap<>();
        if (CollectionUtils.isNotEmpty(items)) {
            items.forEach(item -> map.put(item.getProductId(), item.getQuantity()));
        }
        return map;
    }

    private boolean showRemoveConfirmationDialog(int i, OrderItem orderItem) {
        Dialogs.newDialog()
                .setMessage("Are you sure you want to remove " + orderItem.getProductNumber() + " from the order?")
                .setTitle("Remove item")
                .yes(() -> viewModel.removeItem(orderItem))
                .no(null)
                .show();
        return false;
    }

    private void updateView(OrderWrapper order) {
        reloadSelectedItems(order.getOrderItems());
        UI.setVisibility(findViewById(R.id.emptyOrderText), order.getEmptyOrder());
        String sum = CurrencyFormatter.Companion.formatPrice(order.getOrderSum());
        int count = order.getSelectedItemCount();
        orderTotals.setText(getResources().getQuantityString(R.plurals.sum_n_items, count, sum, count));
    }

    private void reloadSelectedItems(List<OrderItem> items) {
        new RecyclerAdapterBuilder<OrderItemView, OrderItem>(recyclerView)
                .viewFactory(() -> new OrderItemView(this, OrderType.FULL_ORDER))
                .data(() -> items)
                .bind(OrderItemView::fill)
                .onCheckedChange(R.id.isBackorder, (this::setBackorder))
                .onLongClick(R.id.contents, this::showRemoveConfirmationDialog)
                .onClick(R.id.contents, this::showAdjustQuantityDialog)
                .vertical();
        recyclerView.smoothScrollToPosition(items.size());
    }

    private void showAdjustQuantityDialog(int i, OrderItem item) {
        if (dialog != null && dialog.isShowing()) dialog.dismiss();
        ProductWrapper wrapper = ProductWrapper.from(item);
        dialog = EditQuantityDialog.create(wrapper, (quantity, isBackorder) -> viewModel.addProduct(item, quantity, isBackorder));
    }

    private void setBackorder(int position, OrderItem item, boolean isBackorder) {
        viewModel.setBackorderFlag(item, isBackorder);
    }

    private void finishOrder() {
        if (viewModel.getSelectedItems().isEmpty()) {
            ContextExtensionsKt.toast(this, R.string.order_items_activity_seems_order_is_empty, LENGTH_SHORT);
        } else {
            showCreatePickTicketDialog();
        }
    }

    private void showCreatePickTicketDialog() {
        Dialogs.newDialog()
                .setTitle(R.string.order_items_create_pick_ticket)
                .no(null)
                .yes(() -> viewModel.createPickTicket(true))
                .show();
    }

    private void finishOrder(boolean shouldStartPickUp) {
        if (shouldStartPickUp) {
            startActivity(PickUpActivity.newIntent(this, viewModel.getOrderId()));
        }
        finish();
    }

    private void forceCloseIfError(String error) {
        finish();
        ContextExtensionsKt.toast(this, error, LENGTH_SHORT);
        Timber.e(error);
    }

    @Override
    public int notFoundMessage() {
        return R.string.order_items_scan_rental_or_consumable_product_number;
    }

    @Override
    public void onBottomButtonClick() {
        Dialogs.newDialog()
                .setTitle(R.string.common_remove_from_order)
                .setMessage(getString(R.string.common_confirm_order_removal, viewModel.getCurrentOrder().getOrderNumber()))
                .action(getString(R.string.common_remove), () -> viewModel.deleteOrder())
                .no(getString(R.string.common_cancel), null)
                .show();
    }

    @Override
    public void onTopButtonClick() {
        viewModel.saveForLater();
    }

    @Override
    public void onBackButtonClick() {
        ContextExtensionsKt.toast(this, R.string.common_order_was_saved, LENGTH_SHORT);

    }

    @Override
    public void onBackPressed() {
        BottomMenuDialog.show(getSupportFragmentManager());
    }
}
