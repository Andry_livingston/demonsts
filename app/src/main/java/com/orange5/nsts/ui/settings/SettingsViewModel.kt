package com.orange5.nsts.ui.settings

import com.orange5.nsts.R
import com.orange5.nsts.app.modules.ApiModule
import com.orange5.nsts.data.db.DBExporter
import com.orange5.nsts.data.preferencies.RawSharedPreferences
import com.orange5.nsts.service.base.SyncObjectService
import com.orange5.nsts.service.user.RoleService
import com.orange5.nsts.service.user.UserRoleService
import com.orange5.nsts.ui.base.ActionLiveData
import com.orange5.nsts.ui.base.BaseViewModel
import com.orange5.nsts.ui.settings.models.ApiSettings
import javax.inject.Inject

class SettingsViewModel @Inject constructor(
        private val userRoleService: UserRoleService,
        private val preferences: RawSharedPreferences,
        private val roleService: RoleService,
        private val dbExporter: DBExporter,
        private val syncObjectService: SyncObjectService,
        private var apiModule: ApiModule) : BaseViewModel() {

    val onDataBaseExported = ActionLiveData<Unit>()
    val ipChangeData = ActionLiveData<String>()
    val syncDataCleared = ActionLiveData<Int>()
    val existDataForSync = ActionLiveData<Unit>()
    val enabledStressTest = ActionLiveData<Boolean>()

    fun isUserAdministrator(): Boolean {
        return getTypeUserRole() == RoleService.ROLE_ADMINISTRATOR
    }

    fun exportDB() {
        liveProgress.value = true
        runJobComputation((dbExporter::exportDB), {
            liveProgress.postValue(false)
            onDataBaseExported.postValue(Unit)
        })
    }

    fun rebuildSyncService(ip: String) {
        preferences.apiSettings = ApiSettings(ip)
        apiModule.syncClient(ip)
    }

    fun changeRequiredAccountingCode(checked: Boolean) {
        preferences.isAccountingCodeRequired = checked
    }

    fun hideQuickOrder(checked: Boolean) {
        preferences.isHideQuickOrder = checked
    }

    fun hideQuickReturn(checked: Boolean) {
        preferences.isHideQuickReturn = checked
    }

    private fun getTypeUserRole() =
            preferences.user?.userId?.let { roleService.getUserRoleByRoleId(userRoleService.getUserRoleIdByUserId(it)) } ?: ""

    fun onChangeIPClick() {
        ipChangeData.value = preferences.apiSettings.host
    }

    fun onClearSyncData() {
        if (syncObjectService.isAllDataSynced) clearSyncData()
        else existDataForSync.value = Unit
    }

    fun clearSyncData() {
        runJobComputation(
                (syncObjectService::deleteAll),
                { syncDataCleared.postValue(R.string.settings_sync_data_cleared_successfully) })
    }

    fun enableVibration(checked: Boolean) {
        preferences.isScanVibrationEnabled = checked
    }

    fun enableStressTest(checked: Boolean) {
        preferences.isStressTestEnabled = checked
        enabledStressTest.value = checked
    }
}