package com.orange5.nsts.ui.extensions

import com.orange5.nsts.R
import com.orange5.nsts.data.db.base.OrderItem
import com.orange5.nsts.data.db.base.OrderProduct
import com.orange5.nsts.data.db.entities.ConsumableProduct
import com.orange5.nsts.data.db.entities.OrderConsumableItem

fun OrderItem.orderIcon() =
        if (this is OrderConsumableItem) R.drawable.ic_consumable else R.drawable.ic_rental


fun OrderProduct.orderIcon() =
        if (this is ConsumableProduct) R.drawable.ic_consumable else R.drawable.ic_rental
