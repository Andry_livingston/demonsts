package com.orange5.nsts.ui.report.returns

import android.content.Context
import android.util.AttributeSet
import android.util.Pair
import android.widget.LinearLayout
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.RentalReturn
import com.orange5.nsts.util.format.DateFormatter
import com.orange5.nsts.util.view.keyvalue.KeyValueRowView
import kotlinx.android.synthetic.main.return_info_view.view.*
import com.orange5.nsts.util.Bitmaps

class ReturnInfoView @JvmOverloads constructor(
        context: Context, attrsSet: AttributeSet? = null, defStyleAttr: Int = 0, defStyleRes: Int = 0
) : LinearLayout(context, attrsSet, defStyleAttr, defStyleRes){

    init {
        inflate(context, R.layout.return_info_view, this)
    }

    fun fillView(rentalReturn: RentalReturn) {
        val signature = Bitmaps.fromBytes(rentalReturn.signImage)
        signaturePad.isEnabled = false
        if(signature!= null) {
            signaturePad?.signatureBitmap = signature
        }

        addDetail(R.string.finish_order_view_created_at, DateFormatter.convertDbDateToUI(rentalReturn.startDate))
        addDetail(R.string.finish_order_view_closed_at, DateFormatter.convertDbDateToUI(rentalReturn.completeDate))
        addDetail(R.string.finish_order_view_created_by, rentalReturn.customerInformText)
        addDetail(R.string.finish_order_view_picked_by, rentalReturn.customerInformText)
        addDetail(R.string.common_rentals, rentalReturn.items.size.toString())
    }

    private fun addDetail(keyResourceId: Int, value: String) {
        if (value.isNotEmpty()) {
            val rowView = KeyValueRowView(context)
            rowView.fill(Pair(context.getString(keyResourceId), value))
            rowView.layoutParams = LayoutParams(-1, -2)
            detailsRowsContainer.addView(rowView)
        }
    }

}