package com.orange5.nsts.ui.order;

import android.content.Context;
import android.text.SpannableString;

import com.orange5.nsts.R;
import com.orange5.nsts.app.App;
import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.data.db.base.OrderProduct;
import com.orange5.nsts.data.db.entities.OrderConsumableItem;
import com.orange5.nsts.data.db.entities.OrderRentalItem;
import com.orange5.nsts.ui.base.BaseViewHolder;
import com.orange5.nsts.util.format.CurrencyFormatter;

public class OrderItemViewHolder extends BaseViewHolder {

    private final OrderItem currentItem;
    private final OrderProduct currentProduct;
    private final Context context;

    public OrderItemViewHolder(Context context, OrderItem currentItem) {
        this.context = context;
        this.currentItem = currentItem;
        currentItem.__setDaoSession(App.getInstance().getDaoSession());
        this.currentProduct = getProduct();
    }

    private OrderProduct getProduct() {
        if (ifItemConsumable()) {
            OrderConsumableItem currentItem = (OrderConsumableItem) this.currentItem;
            return currentItem.getConsumableProduct();
        }

        if (!ifItemConsumable()) {
            OrderRentalItem currentItem = (OrderRentalItem) this.currentItem;
            return currentItem.getRentalProduct();
        }
        throw new IllegalStateException();
    }

    public boolean ifItemConsumable() {
        return currentItem instanceof OrderConsumableItem;
    }

    public String getFormattedPrice() {
        return CurrencyFormatter.formatPrice(currentProduct.getPrice());
    }

    public String getFormattedQuantity() {
        return currentItem.getQuantity() != null
                ? context.getString(R.string.total_quantity, currentItem.getQuantity())
                : context.getString(R.string.total_quantity, 0);
    }

    public SpannableString getProductDescription(String highlight) {
        return highlightBackground(currentProduct.getProductDescription(), highlight);
    }

    public SpannableString getProductNumber(String highlight) {
        return highlightBackground(currentProduct.getProductNumber(), highlight);
    }

    public String getBinQuantities() {
        return currentProduct.getBinInformation().getAvailableText();
    }

    public SpannableString getAvailableAndOversell() {
        String oversell = "";
        String availability = "";
        if (ifItemConsumable()) {
            int over = ((OrderConsumableItem) currentItem).getOversell();
            if (over > 0) {
                oversell = context.getString(R.string.order_items_oversell, over);
                availability = context.getString(R.string.order_items_availability, currentItem.getQuantity() - over);
            }
        }
        return highlightTextColor(availability.concat(oversell), oversell, context.getColor(R.color.errorRed));
    }
}
