package com.orange5.nsts.ui.order.search;

import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.base.OrderProduct;
import com.orange5.nsts.data.db.entities.BinNumber;
import com.orange5.nsts.data.db.entities.RentalProductItem;
import com.orange5.nsts.service.bin.BinService;
import com.orange5.nsts.service.order.OrderType;
import com.orange5.nsts.ui.base.BaseViewHolder;
import com.orange5.nsts.ui.extensions.OrderExtensionsKt;
import com.orange5.nsts.util.Spannable;
import com.orange5.nsts.util.StringUtils;
import com.orange5.nsts.util.format.CurrencyFormatter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class SearchRVAdapter extends RecyclerView.Adapter<SearchRVAdapter.SearchViewHolder> {

    private List<OrderProduct> items = new ArrayList<>();
    private final OnProductClickListener listener;
    private final Map<String, Integer> existingItems;
    private OrderType orderType;
    private String query;

    SearchRVAdapter(OnProductClickListener listener,
                    Map<String, Integer> existingItems, OrderType type) {
        this.listener = listener;
        this.existingItems = existingItems;
        this.orderType = type;
    }

    public void setItems(List<OrderProduct> items, String query) {
        this.items.clear();
        this.items.addAll(items);
        this.query = query;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_item, parent, false);
        return new SearchViewHolder(view, listener, existingItems);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder holder, int position) {
        holder.bind(items.get(position), query, orderType);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class SearchViewHolder extends RecyclerView.ViewHolder {

        private final Map<String, Integer> existingItems;
        private final OnProductClickListener listener;

        @BindView(R.id.productDescription)
        TextView itemName;

        @BindView(R.id.productNumber)
        TextView itemNumber;

        @BindView(R.id.itemType)
        ImageView itemType;

        @BindView(R.id.itemPrice)
        TextView itemPrice;

        @BindView(R.id.uun)
        TextView uun;

        @BindView(R.id.uunField)
        TextView uunField;

        @BindView(R.id.availability)
        TextView itemAvailability;

        SearchViewHolder(View itemView, OnProductClickListener listener, Map<String, Integer> existingItems) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.existingItems = existingItems;
            this.listener = listener;
        }

        public void bind(OrderProduct product, String highlight, OrderType orderType) {
            setVisibleUun(product.isConsumable() ? GONE : VISIBLE);
            itemName.setText(getProductDescription(product, highlight));
            itemNumber.setText(getProductNumber(product, highlight));
            itemType.setBackgroundResource(OrderExtensionsKt.orderIcon(product));
            itemPrice.setText(CurrencyFormatter.formatPrice(product.getPrice()));
            itemAvailability.setText(getAvailabilityAndSelected(new ProductWrapper(product),
                    orderType.equals(OrderType.QUICK_ORDER), highlight));
        }

        private SpannableString getProductDescription(OrderProduct product, String highlight) {
            return BaseViewHolder.highlightBackground(product.getProductDescription(), highlight);
        }

        private SpannableString getProductNumber(OrderProduct product, String highlight) {
            return BaseViewHolder.highlightBackground(product.getProductNumber(), highlight);
        }

        private void highlightUUN(OrderProduct product, String highlight) {
            String uunText = product.getUUN();
            if (StringUtils.isNotEmpty(uunText)) {
                setVisibleUun(VISIBLE);
                uun.setText(BaseViewHolder.highlightBackground(product.getUUN(), highlight));
            } else {
                setVisibleUun(GONE);
            }
        }

        private void setVisibleUun(int visible) {
            uun.setVisibility(visible);
            uunField.setVisibility(visible);
        }

        private SpannableString getAvailabilityAndSelected(ProductWrapper product,
                                                           boolean typeQuickOrder,
                                                           String highlight) {
            setItemClick(product);
            int totalQuantity = 0;
            int selectedQuantity = getSelectedQuantity(product.getProduct());

            List<String> listBinInformation = new ArrayList<>();
            if (typeQuickOrder && !product.isConsumable()) {
                return configRentalSelectedAndAvailableString(product, highlight);
            } else {
                for (Map.Entry<String, Integer> entry : product.getProduct().getBinInformation()
                        .getCachedProductBinCount().entrySet()) {
                    if (!entry.getKey().equals(BinNumber.BIN_TYPE_RECV)
                            && !entry.getKey().equals(BinNumber.BIN_TYPE_TRANSFERS)) {
                        Integer binQuantity = entry.getValue();
                        totalQuantity += binQuantity;
                        listBinInformation.add(configBinNumText(entry.getKey(), binQuantity));
                    }
                }
            }

            String text = configAvailableAndSelectedText(totalQuantity,
                    listBinInformation, selectedQuantity);

            if (totalQuantity == 0) {
                int color = selectedQuantity > totalQuantity ? R.color.errorRed : R.color.darkGray;
                return Spannable.highlightTextColor(text, getColor(color), text);
            } else {
                return Spannable.highlightTextColor(text,
                        getNeededColor(selectedQuantity, totalQuantity), text);
            }
        }

        private String configAvailableAndSelectedText(int totalQuantity,
                                                      List<String> listBinInformation,
                                                      int selectedQuantity) {
            String prefix = selectedQuantity == 0 ?
                    itemView.getResources().getString(R.string.available_items, totalQuantity)
                    :
                    itemView.getResources().getString(R.string.search_rva_selected_and_available_items,
                            selectedQuantity, totalQuantity);

            StringJoiner stringJoiner = new StringJoiner(", ", prefix, "");
            listBinInformation.forEach(stringJoiner::add);
            return stringJoiner.toString();
        }

        private String configBinNumText(String binNumber, Integer binQuantity) {
            return itemView.getContext().getString(R.string.search_rva_bin_number_quantity, binNumber, binQuantity);
        }

        private SpannableString configRentalSelectedAndAvailableString(ProductWrapper product,
                                                                       String highlight) {
            if (((RentalProductItem) product.getProduct()).getBinId() != null) {
                String text = itemView.getResources()
                        .getString(R.string.search_rva_bin_rental_items,
                                BinService.getById(((RentalProductItem) product.getProduct())
                                        .getBinId()).getBinNum());
                highlightUUN(product.getProduct(), highlight);
                return Spannable.highlightTextColor(text, getColor(R.color.black), text);
            } else {
                product.setError(R.string.warning_order_rental_zero_quantity_availability);
                setVisibleUun(GONE);
                return Spannable.highlightTextColor(itemView.getResources()
                        .getString(R.string.available_items, 0), getColor(R.color.okGreen));
            }
        }

        private void setItemClick(ProductWrapper product) {
            itemView.setOnClickListener(v -> listener.onProductClick(product));
        }

        private int getSelectedQuantity(OrderProduct product) {
            Integer quantity = existingItems.get(product.getProductId());
            return quantity == null ? 0 : quantity;
        }

        private int getNeededColor(int selectedQuantity, int totalQuantity) {
            int color = R.color.okGreen;
            if (selectedQuantity == 0) {
                color = R.color.black;
            } else if (selectedQuantity > totalQuantity) {
                color = R.color.errorRed;
            }
            return getColor(color);
        }

        private int getColor(@ColorRes int colorRes) {
            return itemView.getContext().getColor(colorRes);
        }

    }


    interface OnProductClickListener {
        void onProductClick(ProductWrapper product);
    }
}
