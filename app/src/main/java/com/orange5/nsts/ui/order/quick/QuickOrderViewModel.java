package com.orange5.nsts.ui.order.quick;

import android.graphics.Bitmap;

import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.data.db.base.OrderProduct;
import com.orange5.nsts.data.db.entities.BinNumber;
import com.orange5.nsts.data.db.entities.ConsumableBin;
import com.orange5.nsts.data.db.entities.ConsumableProduct;
import com.orange5.nsts.data.db.entities.Order;
import com.orange5.nsts.data.db.entities.OrderConsumableItem;
import com.orange5.nsts.data.db.entities.OrderRentalItem;
import com.orange5.nsts.data.db.entities.RentalProduct;
import com.orange5.nsts.data.db.entities.RentalProductItem;
import com.orange5.nsts.data.preferencies.RawSharedPreferences;
import com.orange5.nsts.service.bin.BinService;
import com.orange5.nsts.service.bin.ConsumableBinService;
import com.orange5.nsts.service.order.BinQuantitySelection;
import com.orange5.nsts.service.order.OrderService;
import com.orange5.nsts.service.order.OrderStatus;
import com.orange5.nsts.service.order.items.OrderConsumableItemService;
import com.orange5.nsts.service.order.items.OrderRentalItemService;
import com.orange5.nsts.service.parameters.ParametersService;
import com.orange5.nsts.service.product.ProductSearchFilter;
import com.orange5.nsts.service.product.ProductService;
import com.orange5.nsts.service.rental.RentalProductItemService;
import com.orange5.nsts.service.rental.RentalStatus;
import com.orange5.nsts.ui.base.ActionLiveData;
import com.orange5.nsts.ui.base.BaseViewModel;
import com.orange5.nsts.ui.extensions.RandomListExtensionsKt;
import com.orange5.nsts.util.CollectionUtils;
import com.orange5.nsts.util.Operation;
import com.orange5.nsts.util.db.OrderNumberGenerator;
import com.orange5.nsts.util.format.DateFormatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import io.reactivex.schedulers.Schedulers;

import static com.orange5.nsts.service.bin.BinService.byNumber;
import static com.orange5.nsts.service.rental.RentalStatus.CheckedOut;
import static com.orange5.nsts.util.SignatureManager.bitmapToByteArray;

public class QuickOrderViewModel extends BaseViewModel {

    public static final String OVERSELL = "Oversell";

    private Map<String, Integer> cachedQuantities = new HashMap<>();
    private Map<String, List<BinQuantitySelection>> mapSelectedConsumableItems = new HashMap<>();
    private List<OrderConsumableItem> selectedConsumableItems = new ArrayList<>();
    private List<OrderRentalItem> selectedRentalItems = new ArrayList<>();
    public ActionLiveData<Boolean> liveCompleteOrder = new ActionLiveData<>();
    public ActionLiveData<Boolean> liveDeleteOrder = new ActionLiveData<>();
    public ActionLiveData<Boolean> orderIsEmpty = new ActionLiveData<>();
    public ActionLiveData<Boolean> orderHasBackorder = new ActionLiveData<>();
    public ActionLiveData<Boolean> showSigningDialog = new ActionLiveData<>();
    public ActionLiveData<Boolean> isStressButtonEnabled = new ActionLiveData<>();
    public ActionLiveData<Boolean> isNeedToUpdateView = new ActionLiveData<>();
    public ActionLiveData<Boolean> isCheckedOut = new ActionLiveData<>();

    private final OrderService orderService;
    private final OrderRentalItemService rentalService;
    private final OrderConsumableItemService consumableService;
    private final RentalProductItemService productService;
    private final ConsumableBinService consumableBinService;
    private final ParametersService parametersService;
    private final BinService binService;
    private final RawSharedPreferences preferences;
    private Order order;
    public boolean shouldCreateNewOrder = false;

    @Inject
    public QuickOrderViewModel(OrderService orderService,
                               OrderRentalItemService orderRentalItemService,
                               OrderConsumableItemService orderConsumableItemService,
                               RentalProductItemService rentalProductService,
                               ConsumableBinService consumableBinService,
                               ParametersService parametersService,
                               BinService binService,
                               RawSharedPreferences preferences) {
        this.orderService = orderService;
        this.rentalService = orderRentalItemService;
        this.consumableService = orderConsumableItemService;
        this.productService = rentalProductService;
        this.consumableBinService = consumableBinService;
        this.parametersService = parametersService;
        this.binService = binService;
        this.preferences = preferences;
    }

    private void clearTransientData() {
        selectedConsumableItems.clear();
        selectedRentalItems.clear();
        cachedQuantities.clear();
        mapSelectedConsumableItems.clear();
    }

    public void createOrder() {
        if (order.isEmpty() || isEmptyItems()) {
            orderIsEmpty.setValue(true);
        } else if (isOrderHasBackorder()) {
            orderHasBackorder.setValue(true);
        } else {
            showSigningDialog.setValue(true);
        }
    }

    public void createBackorder() {
        shouldCreateNewOrder = false;
        List<OrderConsumableItem> items = order.getConsumableItems()
                .stream()
                .filter(OrderConsumableItem::isBackorder)
                .collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(items)) {
            Order backorder = new Order();
            int sum = calculateOrderSum(items);
            backorder.setOrderSum(sum);
            backorder.setCreatedByUserId(order.getCreatedByUserId());
            backorder.setOrderDate(DateFormatter.currentTimeDb());
            backorder.setDeliveryDate(DateFormatter.currentTimeDb());
            backorder.setCustomer(order.getCustomer());
            if (order.getAccountingCode() != null) {
                backorder.setAccountingCode(order.getAccountingCode());
            }
            backorder.setWebOrderNumber(order.getWebOrderNumber());
            backorder.setOrderStatusId(OrderStatus.pickUp.getId());
            String prefix = parametersService.getOrderPrefix();
            backorder.setOrderNumber(OrderNumberGenerator.getNext(orderService.getLastOrder(prefix), prefix));
            orderService.insert(backorder);
            items.forEach(it -> {
                if (it.getOversell() > 0) {
                    OrderConsumableItem newItem = new OrderConsumableItem();
                    newItem.setQuantity(it.getOversell());
                    newItem.setOrderId(backorder.getOrderId());
                    newItem.isBackorder(true);
                    newItem.setPrice(it.getPrice());
                    newItem.setProductId(it.getProductId());
                    it.setQuantity(it.getQuantity());
                    it.isBackorder(false);
                    consumableService.update(it);
                    consumableService.insert(newItem);
                }
            });
            order.resetConsumableItems();
            orderService.update(order);
            orderService.update(backorder);

        }
    }

    private int calculateOrderSum(List<OrderConsumableItem> items) {
        int sum = 0;
        for (OrderConsumableItem it : items) {
            if (it.getOversell() > 0) {
                sum += it.getPrice() * it.getOversell();
            }
        }
        return sum;
    }

    public void deleteOrder() {
        addDisposable(rxCompletableCreate(() -> {
            removeOpenQuickOrder();
            clearTransientData();
            if (order != null) {
                order.getAllItems().forEach(it -> {
                    it.ifConsumable(consumableService::delete);
                    it.ifRental(rentalService::delete);
                });
                orderService.delete(order);
            }
        })
                .subscribeOn(Schedulers.computation())
                .subscribe(() -> {
                    liveRunSync.postValue(null);
                    liveDeleteOrder.postValue(false);
                }, this::onError));
    }


    private void removeOpenQuickOrder() {
        for (OrderItem orderItem : getSelectedItems()) {
            orderItem.ifRental(orderRentalItem -> removeRentalItem(orderRentalItem,
                    binService.getById(orderRentalItem.getRentalProduct().getDefaultBinId())));
        }
    }

    public void saveForLater() {
        changeOrderType();
        if (!selectedRentalItems.isEmpty()) {
            revertPickRentalProducts();
        }
        recalculateOrder();
    }

    private void revertPickRentalProducts() {
        for (OrderRentalItem orderRentalItem : selectedRentalItems) {
            orderRentalItem.setUniqueUnitNumber(null);
            rentalService.update(orderRentalItem);
        }
    }

    private void changeOrderType() {
        order.setOrderStatusId(OrderStatus.open.getId());
        orderService.update(order);
    }

    public Order getCurrentOrder() {
        return order;
    }

    private void withdrawConsumableFromInventory() {
        order.getConsumableItems().forEach(this::iterateWithdrawConsumableItem);
    }

    public List<OrderItem> getSelectedItems() {
        List<OrderItem> result = new ArrayList<>(selectedRentalItems);
        result.addAll(selectedConsumableItems);
        return result;
    }

    private void recalculateOrder() {
        recalculateOrderSum(selectedConsumableItems);
    }

    private void recalculateSumOrderWithPickedProducts() {
        recalculateOrderSum(order.getConsumableItems());
    }

    private void recalculateOrderSum(List<OrderConsumableItem> selectedOrPickedConsumableItems) {
        Operation orderSum = Operation.with(0);
        selectedOrPickedConsumableItems.forEach(item -> {
            Operation itemTotal = Operation
                    .with(item.getPrice())
                    .multiply(item.getPicked());
            orderSum.add(itemTotal);
        });
        selectedRentalItems.forEach(item -> orderSum.add(item.getPrice()));
        order.setOrderSum(orderSum.toDouble());
        orderService.update(order);
    }

    private void cacheQuantity(OrderItem orderItem) {
        cachedQuantities.put(orderItem.getProductId(), orderItem.getQuantity());
    }

    private void clearCachedQuantity(OrderItem orderItem) {
        cachedQuantities.remove(orderItem.getProductId());
    }

    private ConsumableBin getOrCreateConsumableBin(ConsumableBin consumableBin, String productId,
                                                   BinNumber binNumber) {
        if (consumableBin != null) {
            return consumableBin;
        }
        consumableBin = new ConsumableBin();
        consumableBin.setBinNumber(binNumber);
        consumableBin.setProductId(productId);
        consumableBin.setAddingDate(DateFormatter.currentTimeDb());
        consumableBin.setQuantity(0);
        consumableBinService.insert(consumableBin);
        return consumableBin;
    }

    private OrderConsumableItem getExistingConsumableItem(ConsumableProduct consumableProduct) {
        Optional<OrderConsumableItem> consumableItem = selectedConsumableItems
                .stream()
                .filter(item -> item.getProductId().equals(consumableProduct.getProductId()))
                .findFirst();
        return consumableItem.orElse(null);
    }

    public void removeRentalItem(OrderRentalItem rentalItem, BinNumber returnToBin) {
        String uun = rentalItem.getUniqueUnitNumber();
        rentalItem.setUniqueUnitNumber(null);
        rentalService.update(rentalItem);
        RentalProductItem productItem = rentalItem
                .getRentalProduct()
                .getProductItems()
                .stream()
                .filter(rentalProductItem -> rentalProductItem.getStatus()
                        .equals(CheckedOut.getId())
                        && rentalProductItem.getUniqueUnitNumber().equals(uun))
                .collect(Collectors.toList()).get(0);
        productItem.setBinId(returnToBin.getBinId());
        productItem.setStatus(RentalStatus.Available.getId());
        productService.update(productItem);
        clearCachedQuantity(rentalItem);
        rentalService.delete(rentalItem);
        selectedRentalItems.remove(rentalItem);
        recalculateOrder();
    }

    public void addRentalProduct(RentalProduct rentalProduct, String uniqueUnitNumber) {
        order.resetRentalItems();
        if (order.getRentalItems().stream().anyMatch(it -> it.getUniqueUnitNumber().equals(uniqueUnitNumber))) {
            liveError.setValue("UUN is checked out or is already in current order");
            return;
        }
        addRentalItemToOrder(rentalProduct, uniqueUnitNumber);
        updateRentalProductInformationInBD(rentalProduct, uniqueUnitNumber);
        recalculateOrder();
    }

    private void updateRentalProductInformationInBD(RentalProduct rentalProduct, String uniqueUnitNumber) {
        rentalProduct.getProductItems()
                .stream()
                .filter(customer -> uniqueUnitNumber.equals(customer.getUniqueUnitNumber()))
                .findFirst()
                .ifPresent(it -> {
                    it.setBinId(null);
                    it.setStatus(RentalStatus.CheckedOut.getId());
                    productService.update(it);
                });
    }

    private void addRentalItemToOrder(RentalProduct rentalProduct, String uniqueUnitNumber) {
        OrderRentalItem rentalItem = new OrderRentalItem();
        rentalItem.setOrderId(order.getOrderId());
        rentalItem.setProductId(rentalProduct.getProductId());
        rentalItem.setProductNumber(rentalProduct.getProductNumber());
        rentalItem.setRentPrice(rentalProduct.getPrice());
        rentalItem.setRentalPeriod(rentalProduct.getRentalPeriod());
        rentalItem.setUniqueUnitNumber(uniqueUnitNumber);
        rentalItem.setReturnCustomerId(order.getCreatorCustomerId());
        cacheQuantity(rentalItem);
        order.resetRentalItems();
        rentalService.insert(rentalItem);
        selectedRentalItems.add(rentalItem);
    }

    public int getSelectedItemCount() {
        return getSelectedItems().stream().mapToInt(OrderItem::getQuantity).sum();
    }

    public boolean isEmptyItems() {
        return selectedConsumableItems.isEmpty() && selectedRentalItems.isEmpty();
    }

    public void removeConsumableItem(OrderConsumableItem item) {
        mapSelectedConsumableItems.remove(item.getProductId());
        consumableService.delete(item);
        selectedConsumableItems.remove(item);
        clearCachedQuantity(item);
        recalculateOrder();
    }

    public void setBackorderFlag(OrderItem item, boolean isBackorder) {
        OrderConsumableItem consumableItem = item.asConsumable();
        consumableItem.isBackorder(isBackorder);
        consumableService.update(consumableItem);
        orderService.update(order);
        orderService.resetConsumable(order);
        cacheQuantity(consumableItem);
        recalculateOrder();
    }

    public void addConsumableProduct(ConsumableProduct consumableProduct,
                                     List<BinQuantitySelection> selections,
                                     boolean hasBackorder, boolean isStressAdding) {
        int totalAddedQuantity = isStressAdding ? 1 : selections.stream()
                .mapToInt(BinQuantitySelection::getSelectedQuantity).sum();
        mapSelectedConsumableItems.put(consumableProduct.getProductId(), selections);
        OrderConsumableItem consumableItem = getExistingConsumableItem(consumableProduct);
        if (consumableItem != null) {
            consumableItem.setQuantity(totalAddedQuantity);
            consumableItem.isBackorder(hasBackorder);
            consumableService.update(consumableItem);
            orderService.update(order);
        } else {
            consumableItem = new OrderConsumableItem();
            consumableItem.setQuantity(totalAddedQuantity);
            consumableItem.isBackorder(hasBackorder);
            consumableItem.setPrice(consumableProduct.getPrice());
            consumableItem.setOrderId(order.getOrderId());
            consumableItem.setProductId(consumableProduct.getProductId());
            consumableService.insert(consumableItem);
            selectedConsumableItems.add(consumableItem);
            orderService.update(order);
        }
        orderService.resetConsumable(order);
        cacheQuantity(consumableItem);
        recalculateOrder();
    }

    public void loadExisting(String orderId) {
        order = orderService.getOrderById(orderId);
        isStressButtonEnabled.postValue(preferences.isStressTestEnabled());
    }

    public List<BinQuantitySelection> getSelectedItemQuantity(ConsumableProduct product) {
        if (mapSelectedConsumableItems.size() > 0
                && mapSelectedConsumableItems.containsKey(product.getProductId())) {
            return mapSelectedConsumableItems.get(product.getProductId());
        } else {
            return getSelectedProductQuantity(product);
        }
    }

    public List<BinQuantitySelection> getSelectedProductQuantity(ConsumableProduct product) {
        List<BinQuantitySelection> binQuantitySelections = new ArrayList<>();
        product.getBinInformation().getCachedProductBinCount()
                .forEach((key, value) ->
                        addBin(binQuantitySelections, key, value));
        return binQuantitySelections;
    }

    public void addTenProduct() {
        preferences.setShouldShowConsumables(true);
        preferences.setShouldShowRentals(true);
        preferences.setShouldShowZeroQuantity(false);
        ProductService.get().searchProductsQuickOrder(new ProductSearchFilter(preferences),
                this::addTenItems);

    }

    public void actionAddingRentalProduct(RentalProductItem item) {
        if (item.getStatus().equals(RentalProductItem.STATUS_RENTAL_ITEM)) {
            addRentalProduct(item.getRentalProduct(), item.getUniqueUnitNumber());
        } else {
            isCheckedOut.postValue(true);
        }
        isNeedToUpdateView.postValue(true);
    }

    private void iterateWithdrawConsumableItem(OrderConsumableItem item) {
        getSelectedItemQuantity(item.getConsumableProduct()).forEach(selectedBin -> {
            if (selectedBin.getSelectedQuantity() > 0
                    && !selectedBin.getBinNumber().equals(QuickOrderViewModel.OVERSELL)) {
                BinNumber binNumber = byNumber(selectedBin.getBinNumber());
                ConsumableBin consumableBin = getOrCreateConsumableBin(
                        consumableBinService.byProductIdAndBin(item.getProductId(), binNumber.getBinId()),
                        item.getProductId(),
                        binNumber);
                consumableBin.setQuantity(consumableBin.getQuantity() - selectedBin.getSelectedQuantity());
                consumableBinService.update(consumableBin);
            }
        });
    }

    private void addTenItems(List<OrderProduct> items) {
        List<OrderProduct> listRentals = RandomListExtensionsKt
                .getRandomList(getProducts(items,true), 5);
        for (OrderProduct product : listRentals) {
            actionAddingRentalProduct((RentalProductItem) product);
        }

        List<OrderProduct> listConsumable = RandomListExtensionsKt
                .getRandomList(getProducts(items,false), 5);
        for (OrderProduct product : listConsumable) {
            addConsumableProduct((ConsumableProduct) product,
                    getSelectedItemQuantity((ConsumableProduct) product), false, true);
        }
        isNeedToUpdateView.postValue(true);
    }

    private List<OrderProduct> getProducts(List<OrderProduct> it, boolean isRental) {
        List<OrderProduct> list = new ArrayList<>();
        for (OrderProduct product : it) {
            if (product instanceof RentalProductItem && isRental) {
                list.add(product);
            } else if (product instanceof ConsumableProduct && !isRental) {
                list.add(product);
            }
        }
        return list;
    }

    private void addBin(List<BinQuantitySelection> binQuantitySelections, String key, Integer value) {
        if (!key.equals(BinNumber.BIN_TYPE_RECV)) {
            binQuantitySelections.add(new BinQuantitySelection(key, value, 0));
        }
    }

    public void finishOrder(Bitmap signatureBitmap, boolean shouldStartNextOrder) {
        addDisposable(rxCompletableCreate(() -> {
            createBackorder();
            pickConsumableItems();
            if (order.getPickCustomer() == null) {
                order.setPickCustomer(order.getCustomer());
            }
            order.getRentalItems().forEach(this::startRentPeriod);
            order.setOrderCompleteDate(DateFormatter.currentTimeDb());
            order.setOrderStatusId(OrderStatus.closed.getId());
            order.setSignImage(bitmapToByteArray(signatureBitmap));
            withdrawConsumableFromInventory();
            recalculateSumOrderWithPickedProducts();
            orderService.update(order);
            clearTransientData();
            order = null;
        })
                .subscribeOn(Schedulers.computation())
                .subscribe(() -> {
                    liveRunSync.postValue(null);
                    liveCompleteOrder.postValue(shouldStartNextOrder);
                }, this::onError));
    }

    private void pickConsumableItems() {
        order.getConsumableItems().forEach(item -> {
            item.setPicked(
                    item.getOversell() > 0 ? item.getQuantity() - item.getOversell()
                    : item.getQuantity());
            consumableService.update(item);
        });
    }

    private void startRentPeriod(OrderRentalItem item) {
        if (item.getUniqueUnitNumber() != null) {
            item.setStartDate(DateFormatter.currentTimeDb());
            item.setDueDate(DateFormatter.endOfRentalPeriod());
            rentalService.update(item);

            RentalProductItem rentalProductItem = productService.loadItemByUUN(item.getUniqueUnitNumber());
            rentalProductItem.setStatus(CheckedOut.getId());
            rentalProductItem.setBinId(null);
            rentalProductItem.setContainerID(null);
            productService.update(rentalProductItem);
        }
    }

    private boolean isOrderHasBackorder() {
        return selectedConsumableItems.stream().anyMatch(OrderConsumableItem::isBackorder);
    }

    boolean checkIfHasOnlyBackorder() {
        return (selectedConsumableItems.stream().anyMatch((item) -> !item.isBackorder()))
                || !selectedRentalItems.isEmpty();
    }
}
