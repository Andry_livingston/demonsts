package com.orange5.nsts.ui.shipping.transfer

import androidx.lifecycle.MutableLiveData
import com.orange5.nsts.data.db.entities.ShippingNotice
import com.orange5.nsts.service.shipping.ShippingNoticeService
import com.orange5.nsts.ui.base.BaseViewModel
import com.orange5.nsts.ui.shipping.repository.ReceivingRepository
import javax.inject.Inject

class ShippingNoticeViewModel @Inject constructor(
        private val shippingNoticeService: ShippingNoticeService,
        private val receivingRepository: ReceivingRepository) : BaseViewModel() {

    val takeAwayQuantity = MutableLiveData<Int>()
    val shippingNotice = MutableLiveData<List<ShippingNotice>>()

    fun loadShippingNotices() {
        callJobIo(
                { shippingNoticeService.unprocessedNotices },
                { shippingNotice.postValue(it) })
    }

    fun loadTakeAwayNotices() {
        callJobIo(
                { receivingRepository.getReceivingItemsCount() },
                { takeAwayQuantity.postValue(it) })
    }
}