package com.orange5.nsts.ui.returns.quick

import androidx.fragment.app.activityViewModels
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.ui.base.TwoPageAdapter.TabType.RESULT
import com.orange5.nsts.ui.returns.ReturnedItemsFragment

class QuickReturnedItemsFragment : ReturnedItemsFragment() {

    override val viewModel: QuickReturnViewModel by activityViewModels { vmFactory }

    override fun onItemClick(item: OrderRentalItem) {
        QuickRentalItemReturnDialog.show(this, item, RESULT)
    }

    override fun updateRentedList(items: List<OrderRentalItem>) {
        super.updateRentedList(items)
        if (userVisibleHint) {
            viewModel.setTitle(
                    when {
                        items.size == 1 -> {
                            val customer = items[0].returnedByCustomer
                            "${customer.fName} ${customer.lName}"
                        }
                        items.size > 1 -> getString(R.string.common_multiple_customers)
                        else -> getString(R.string.rental_return_activity_quick)
                    })
        }
    }

}
