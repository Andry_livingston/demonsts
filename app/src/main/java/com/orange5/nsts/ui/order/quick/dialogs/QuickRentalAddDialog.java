package com.orange5.nsts.ui.order.quick.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import androidx.annotation.Nullable;
import android.text.SpannableString;
import android.view.View;
import android.widget.Button;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.entities.BinNumber;
import com.orange5.nsts.data.db.entities.RentalProduct;
import com.orange5.nsts.data.db.entities.RentalProductItem;
import com.orange5.nsts.util.Colors;
import com.orange5.nsts.service.rental.RentalItemService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import com.orange5.nsts.util.Spannable;
import com.orange5.nsts.util.view.picker.PickerView;
import com.orange5.nsts.util.view.picker.ValuePickerAdapter;

public class QuickRentalAddDialog {
    private final Context context;
    private BiConsumer<BinNumber, String> resultConsumer;
    private BinNumber selectedBinNumber;
    private String selectedUniqueUnitNumber;
    private AlertDialog alertDialog;

    private PickerView picker;
    private Button pickButton;
    private List<RentalProductItem> allUniqueNumbers;
    private HashMap<String, BinNumber> uniqueMap;

    public static QuickRentalAddDialog with(Context context) {
        return new QuickRentalAddDialog(context);
    }

    private QuickRentalAddDialog(Context context) {
        this.context = context;
    }

    public QuickRentalAddDialog resultConsumer(BiConsumer<BinNumber, String> resultConsumer) {
        this.resultConsumer = resultConsumer;
        return this;
    }

    public void pick(RentalProduct rentalProduct) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.common_add_rental_n_to_order, rentalProduct.getProductNumber()));
        View contentView = View.inflate(context, R.layout.rental_dialog, null);
        builder.setView(contentView);
        picker = contentView.findViewById(R.id.picker);
        pickButton = contentView.findViewById(R.id.actionButton);

        List<String> listRentalProduct = configListRentalItems(rentalProduct);
        if (listRentalProduct == null) return;

        updateUniqueUnitNumbers(allUniqueNumbers, listRentalProduct);
        alertDialog = builder.create();
        alertDialog.show();

        pickButton.setOnClickListener(v -> {
            resultConsumer.accept(selectedBinNumber, selectedUniqueUnitNumber);
            alertDialog.dismiss();
        });
    }

    @Nullable
    private List<String> configListRentalItems(RentalProduct rentalProduct) {
        allUniqueNumbers = filterList(RentalItemService.getAllRentalItems(rentalProduct));
        List<String> listRentalProduct = new ArrayList<>();
        uniqueMap = new HashMap<>();
        for (RentalProductItem product : allUniqueNumbers) {
            if (product == null) {
                return null;
            }

            listRentalProduct.add(product.getUniqueUnitNumber());
            uniqueMap.put(product.getUniqueUnitNumber(), product.getContainerBin());
        }
        return listRentalProduct;
    }

    private List<RentalProductItem> filterList(List<RentalProductItem> allRentalItems) {
        List<RentalProductItem> rental = new ArrayList<>();
        for (RentalProductItem rentalItem : allRentalItems) {
            if (rentalItem.getContainerBin() != null
                    && !rentalItem.getContainerBin().getBinNum().equals(BinNumber.BIN_TYPE_RECV) ) {
                rental.add(rentalItem);
            }
        }
        return rental;
    }

    private void updateUniqueUnitNumbers(List<RentalProductItem> rentalProductItems,
                                         List<String> listRentalProduct) {
        if (rentalProductItems.size() > 0) {
            selectedUniqueUnitNumber = rentalProductItems.get(0).getUniqueUnitNumber();
            selectedBinNumber = getSelectedBin(rentalProductItems.get(0).getUniqueUnitNumber());
            updateButton();
        }
        new ValuePickerAdapter<>(listRentalProduct)
                .selectedItemConsumer(selected -> {
                    selectedUniqueUnitNumber = selected;
                    selectedBinNumber = getSelectedBin(selected);
                    updateButton();
                })
                .attachTo(picker);
    }

    private BinNumber getSelectedBin(String selectedUun) {
        for (Map.Entry<String, BinNumber> entry : uniqueMap.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            // ...
            System.out.println(key + " = " + value);
            if (key == selectedUun) {
                System.out.println(key + " !!!!!!!!! " + value);
                return (BinNumber) value;
            }
        }
        return new BinNumber();
    }

    private void updateButton() {
        String string = context.getString(R.string.common_pick_uun_from_bin, selectedUniqueUnitNumber,
                selectedBinNumber);
        SpannableString spannableString;
        if (selectedBinNumber != null) {
            spannableString = Spannable.highlightTextColor(string, Colors.HIGHLIGHT_BLUE,
                    selectedUniqueUnitNumber,
                    selectedBinNumber.getBinNum());
        } else {
            spannableString = Spannable.highlightTextColor(string, Colors.HIGHLIGHT_BLUE,
                    selectedUniqueUnitNumber,
                    "empty");
        }

        pickButton.setText(spannableString);
    }

    private boolean isShowing() {
        return alertDialog != null && alertDialog.isShowing();
    }

    public void dismiss() {
        if (isShowing()) {
            alertDialog.dismiss();
        }
    }

}
