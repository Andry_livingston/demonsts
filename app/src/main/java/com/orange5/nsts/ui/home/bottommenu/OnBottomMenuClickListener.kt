package com.orange5.nsts.ui.home.bottommenu

interface OnBottomMenuClickListener {
    fun onBottomItemClick(menu: Menu)
}