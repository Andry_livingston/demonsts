package com.orange5.nsts.ui.home.returns;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.entities.RentalReturn;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReturnView extends LinearLayout {

    @BindView(R.id.sectionHeader)
    protected TextView sectionHeader;

    @BindView(R.id.rentalStatus)
    protected TextView rentalStatus;

    @BindView(R.id.createdAt)
    protected TextView createdAt;

    @BindView(R.id.closedAt)
    protected TextView closedAt;

    @BindView(R.id.rentalNumber)
    protected TextView rentalNumber;

    @BindView(R.id.returningCustomer)
    protected TextView returningCustomer;

    @BindView(R.id.itemCount)
    protected TextView itemCount;

    public ReturnView(@NonNull Context context) {
        this(context, null);
    }

    public ReturnView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ReturnView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ReturnView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setOrientation(VERTICAL);
        inflate(context, R.layout.rental_return_item, this);
        ButterKnife.bind(this, this);
        sectionHeader.setVisibility(GONE);
    }


    public void fill(RentalReturn rentalReturn, String highlight) {
        ReturnViewHolder viewModel = new ReturnViewHolder(getContext(), rentalReturn);
        rentalStatus.setText(viewModel.getStatus());
        rentalStatus.setBackground(viewModel.getStatusBackground());
        itemCount.setText(viewModel.getItemCount());
        createdAt.setText(viewModel.getCreationDate());
        closedAt.setText(viewModel.getClosedDate());
        rentalNumber.setText(rentalReturn.getReturnNumber());
        returningCustomer.setText(viewModel.getCustomer());
    }


    public void showSectionHeader(String sectionTitle) {
        sectionHeader.setText(sectionTitle);
        sectionHeader.setVisibility(VISIBLE);
    }

    public void hideSectionHeader() {
        sectionHeader.setVisibility(GONE);
    }
}
