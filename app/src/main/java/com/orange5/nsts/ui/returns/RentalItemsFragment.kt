package com.orange5.nsts.ui.returns

import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.lifecycle.observe
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.scan.ScanningFragment
import com.orange5.nsts.service.rental.RentalStatus
import com.orange5.nsts.ui.returns.list.RentalAdapter
import com.orange5.nsts.ui.returns.quick.ReturnInformation
import com.orange5.nsts.ui.returns.signature.OnFinishListener
import com.orange5.nsts.util.view.CustomToast
import kotlinx.android.synthetic.main.fragment_refreshed_list.*

abstract class RentalItemsFragment : ScanningFragment(),
        RentalAdapter.Listener,
        SearchView.OnQueryTextListener {

    private var onFinishReturnListener: OnFinishListener? = null
    protected abstract val viewModel: ReturnViewModel

    protected val adapter = RentalAdapter(this)

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onFinishReturnListener = (context as OnFinishListener)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        isRefreshEnabled = false
        viewModel.liveProgress.observe(this, ::showLoading)
        viewModel.allRentalItems.observe(this, ::updateRentedList)
        viewModel.searchItems.observe(this, ::updateRentedList)
        viewModel.scannedItem.observe(this, ::onItemScanned)
        viewModel.scannedItemNotFound.observe(this, ::showScanFailToast)
    }

    override fun subscribe() {
        super.subscribe()
        scanningViewModel.liveRentalItemScanned.observe(viewLifecycleOwner, viewModel::scanByOrderItemId)
        scanningViewModel.liveRentalScanned.observe(viewLifecycleOwner, viewModel::scanByProductId)
    }

    override val notFoundMessage = R.string.rental_return_activity_scan_uun_or_rental_number

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        noData.isVisible = false
        recyclerView.adapter = adapter
        viewModel.onSuccessAdded.safeObserve { showInfoToast(getString(R.string.rental_return_activity_s_added_to_return, it)) }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) =
            inflater.inflate(R.menu.search_scan_finish, menu)

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        initSearch(menu.findItem(R.id.search))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
            when (item.itemId) {
                R.id.finish -> {
                    onFinishReturnListener?.onFinishProcessClick()
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    override fun onQueryTextSubmit(query: String?) = performSearch(query)

    override fun onQueryTextChange(newText: String?) = performSearch(newText)

    protected open fun onItemScanned(item: OrderRentalItem?) {
        item?.let {
            val defaultBin = viewModel.getDefaultBin(it)
            if (defaultBin != null) {
                successVibration()
                viewModel.onItemReturned(ReturnInformation(
                        defaultBin,
                        if (it.wasDamaged) RentalStatus.InRepair else RentalStatus.Available,
                        it.rentalReturnNotes ?: ""
                ))
            } else showScanFailToast(R.string.rental_return_error_inspection_bin)
        }
    }

    protected fun showScanFailToast(@StringRes message: Int?) {
        errorVibration()
        CustomToast.show(requireActivity(), getString(message
                ?: R.string.rental_return_activity_message_item_not_found))
    }

    private fun initSearch(item: MenuItem?) {
        if (item == null) return
        val searchView = item.actionView as SearchView
        searchView.setOnQueryTextListener(this)
    }

    private fun performSearch(query: String?) =
            if (query != null) {
                viewModel.performSearch(query)
                true
            } else false

    protected open fun updateRentedList(items: List<OrderRentalItem>?) {
        if (userVisibleHint) viewModel.setCount(items?.size ?: 0)
        if (items != null) adapter.submitList(items) { recyclerView.scrollToPosition(0) }
        showOrHideEmptyMessage(items.isNullOrEmpty())
    }

    private fun showOrHideEmptyMessage(showOrHide: Boolean) {
        noData.isVisible = showOrHide
    }

    private fun showInfoToast(message: String) =
            Toast.makeText(context, message, Toast.LENGTH_SHORT).apply {
                setGravity(Gravity.CENTER, 0, 0)
                show()
            }

}
