package com.orange5.nsts.ui.shipping.transfer

import android.content.Context
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.LinearLayout.LayoutParams.MATCH_PARENT
import androidx.core.text.color
import androidx.core.view.isVisible
import androidx.core.view.updateMargins
import androidx.recyclerview.widget.RecyclerView
import com.orange5.nsts.R
import com.orange5.nsts.data.db.entities.ShippingNotice
import com.orange5.nsts.ui.extensions.color
import com.orange5.nsts.ui.extensions.rotate
import com.orange5.nsts.ui.shipping.transfer.ShippingNoticeAdapter.ViewHolder
import com.orange5.nsts.util.format.DateFormatter
import kotlinx.android.synthetic.main.item_shipping_notice.view.*
import kotlinx.android.synthetic.main.item_shipping_notice_product.view.*

class ShippingNoticeAdapter(private val listener: Listener) : RecyclerView.Adapter<ViewHolder>() {

    private var items: List<Item> = listOf()

    fun submitList(items: List<ShippingNotice>) {
        this.items = items.groupBy { it.transferNumber }.map { Item(it.key, it.value[0].shippingDate, it.value) }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_shipping_notice, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Item) {
            itemView.run {
                transfer.text = context.getString(R.string.shipping_notice_transfer, item.transferNumber)
                itemQuantity.text = context.getString(R.string.shipping_notice_product_quantity, item.notices.size)
                shippingDate.text = context.getString(R.string.shipping_notice_scheduled_delivery, DateFormatter
                        .toUIDate(item.date))
                setOnClickListener {
                    val degrees = if (container.isVisible) -180F else 180F
                    container.isVisible = !container.isVisible
                    expand.rotate(degrees)
                }
                container.removeAllViews()
                container.addView(divider())
                item.notices.forEach {
                            container.addView(productItem(it))
                        }
                processReceiving.setOnClickListener { listener.onReceiveClick(item.transferNumber) }
            }
        }

        private fun View.productItem(notice: ShippingNotice): View {
            val productItem = LayoutInflater.from(context).inflate(R.layout.item_shipping_notice_product, container, false)
            productItem.run {
                val isRental = notice.rentalUniqueUnitNumber != null
                iconRental.isVisible = isRental
                iconConsumable.isVisible = !isRental
                uun.isVisible = isRental
                quantityRental.isVisible = isRental
                quantityConsumable.isVisible = !isRental
                productNumber.text = if (isRental) notice.rentalProductNumber else notice.consumableProductNumber
                if (isRental) {
                    uun.text = context.getString(R.string.shipping_notice_uun, notice.rentalUniqueUnitNumber)
                    quantityRental.text = context.getString(R.string.shipping_notice_rental_quantity, 1)
                } else {
                    quantityConsumable.text = formatConsumableQuantity(context, notice)
                }
            }
            return productItem
        }

        private fun formatConsumableQuantity(context: Context, item: ShippingNotice): SpannableStringBuilder {
            val blue = context.color(R.color.price)
            val black = context.color(R.color.black)
            return SpannableStringBuilder()
                    .color(blue) {
                        append(context.getString(R.string.shipping_notice_consumable_quantity)).append(SPACE)
                    }.color(black) {
                        append(context.getString(R.string.shipping_notice_consumable_quantity_total)).append(SPACE)
                    }.color(blue) {
                        append("${item.consumableQuantityStart}").append(SPACE)
                    }.color(black) {
                        append(context.getString(R.string.shipping_notice_consumable_quantity_processed)).append(SPACE)
                    }.color(blue) {
                        append("${item.consumableQuantityStart - item.consumableQuantity}").append(SPACE)
                    }.color(black) {
                        append(context.getString(R.string.shipping_notice_consumable_quantity_left)).append(SPACE)
                    }.color(blue) {
                        append("${item.consumableQuantity}")
                    }
        }

        private fun View.divider(): View {
            val divider = View(context).apply {
                val params = LinearLayout.LayoutParams(MATCH_PARENT, resources.getDimensionPixelOffset(R.dimen.common_1dp))
                params.updateMargins(left = resources.getDimensionPixelOffset(R.dimen.common_8dp), right = resources
                        .getDimensionPixelOffset(R.dimen.common_8dp))
                layoutParams = params
            }
            divider.setBackgroundColor(context.color(R.color.divider_background_color))
            return divider
        }
    }

    interface Listener {
        fun onReceiveClick(transferNumber: String)
    }

    data class Item(val transferNumber: String = "",
                    val date: String = "",
                    val notices: List<ShippingNotice>)

    private companion object {
        const val SPACE = " "
    }
}