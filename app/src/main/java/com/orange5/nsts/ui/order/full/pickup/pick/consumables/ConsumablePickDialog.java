package com.orange5.nsts.ui.order.full.pickup.pick.consumables;

import android.app.AlertDialog;
import android.text.SpannableString;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.data.db.entities.ConsumableBin;
import com.orange5.nsts.data.db.entities.OrderConsumableItem;
import com.orange5.nsts.util.Colors;
import com.orange5.nsts.ui.base.BaseActivity;
import com.orange5.nsts.util.CollectionUtils;
import com.orange5.nsts.util.view.dialogs.BaseTwoPickerDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

import com.orange5.nsts.util.Spannable;

public class ConsumablePickDialog extends BaseTwoPickerDialog<ConsumableBin, Integer> {

    public static final String OVERSELL = "oversell";
    private OrderItem orderItem;

    private ConsumablePickDialog(BaseActivity activity,
                                 BiConsumer<ConsumableBin, Integer> biConsumer,
                                 List<ConsumableBin> list,
                                 OrderItem orderItem) {
        super(activity, biConsumer);
        this.orderItem = orderItem;
        setOversellBin(list);
        init();
    }

    public static void create(BaseActivity activity,
                              List<ConsumableBin> list,
                              OrderItem orderItem,
                              BiConsumer<ConsumableBin, Integer> biConsumer) {
        new ConsumablePickDialog(activity, biConsumer, list, orderItem);
    }

    public void init() {
        if (dialog != null && dialog.isShowing()) {
            dismiss();
        }
        dialog = new AlertDialog.Builder(activity)
                .setView(view)
                .create();
        setTitle(resources.getString(R.string.pickup_consumable_title, orderItem.getProductNumber()));
        setListeners();
        setUpLeftPicker();
        show();
    }

    @Override
    protected List<String> mapBinsToString() {
        List<String> binIds = new ArrayList<>();
        for (ConsumableBin bin : bins) {
            if (bin.getBinId().equals(OVERSELL)) {
                binIds.add(OVERSELL);
                continue;
            }
            binIds.add(bin.toString());
        }
        return binIds;
    }

    @Override
    protected boolean isOversell(ConsumableBin selectedBin) {
        return selectedBin.getBinId().equals(OVERSELL);
    }

    private void setOversellBin(List<ConsumableBin> list) {
        OrderConsumableItem item = orderItem.asConsumable();
        if (item.getOversell() > 0 && item.getPicked() == item.quantityMinusOversell()) {
            ConsumableBin oversell = new ConsumableBin();
            oversell.setBinId(OVERSELL);
            oversell.setQuantity(orderItem.asConsumable().getOversell());
            list.add(oversell);
        }
        bins = list;
    }

    @Override
    protected SpannableString formatActionButtonTitle() {
        Integer quantity = getSelectedQuantity();
        String quantityString = quantity.toString();
        String binNum = getBinNum();
        String text = resources.getQuantityString(R.plurals.pick_n_items_from_bin, quantity, quantity, binNum);
        return Spannable.highlightTextColor(text, Colors.HIGHLIGHT_BLUE, quantityString, binNum);

    }

    @Override
    protected List<Integer> getRightPickerList(ConsumableBin selectedBin) {
        int quantity = Math.min(selectedBin.getQuantity(), orderItem.getQuantity() - orderItem.getPicked());
        return CollectionUtils.rangeClosedList(1, quantity);
    }

    private String getBinNum() {
        String num = OVERSELL;
        if (!getSelectedBin().getBinId().equals(OVERSELL)) {
            num = getSelectedBin().toString();
        }
        return num;
    }
}
