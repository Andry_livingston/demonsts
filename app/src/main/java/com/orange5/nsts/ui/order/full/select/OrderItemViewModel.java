package com.orange5.nsts.ui.order.full.select;

import androidx.lifecycle.MutableLiveData;

import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.data.db.base.OrderProduct;
import com.orange5.nsts.data.db.entities.Order;
import com.orange5.nsts.data.db.entities.OrderConsumableItem;
import com.orange5.nsts.data.db.entities.OrderRentalItem;
import com.orange5.nsts.data.local.ConsumableDiff;
import com.orange5.nsts.data.local.OrderWrapper;
import com.orange5.nsts.data.local.RentalDiff;
import com.orange5.nsts.data.preferencies.RawSharedPreferences;
import com.orange5.nsts.service.order.OrderService;
import com.orange5.nsts.service.order.OrderStatus;
import com.orange5.nsts.service.order.items.OrderConsumableItemService;
import com.orange5.nsts.service.order.items.OrderRentalItemService;
import com.orange5.nsts.service.product.ProductService;
import com.orange5.nsts.ui.base.ActionLiveData;
import com.orange5.nsts.ui.base.BaseViewModel;
import com.orange5.nsts.ui.extensions.RandomListExtensionsKt;
import com.orange5.nsts.ui.order.search.ProductWrapper;
import com.orange5.nsts.util.Operation;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

public class OrderItemViewModel extends BaseViewModel {

    private final OrderService orderService;
    private final OrderRentalItemService rentalService;
    private final OrderConsumableItemService consumableService;
    private final RawSharedPreferences preferences;

    private Order order;
    private OrderWrapper orderWrapper;

    public ActionLiveData<String> onLoadOrderFail = new ActionLiveData<>();
    public MutableLiveData<Order> onLoadOrderSuccess = new MutableLiveData<>();
    public MutableLiveData<OrderWrapper> onViewUpdated = new MutableLiveData<>();
    public ActionLiveData<Boolean> onFinishOrder = new ActionLiveData<>();
    public MutableLiveData<ProductWrapper> liveScannedNewProduct = new MutableLiveData<>();
    public MutableLiveData<OrderItem> liveScannedExistingProduct = new MutableLiveData<>();
    public ActionLiveData<Boolean> isStressTestEnabled = new ActionLiveData<>();

    @Inject
    public OrderItemViewModel(OrderService orderService,
                              OrderRentalItemService rentalService,
                              OrderConsumableItemService consumableService,
                              RawSharedPreferences preferences) {
        this.orderService = orderService;
        this.rentalService = rentalService;
        this.consumableService = consumableService;
        this.preferences = preferences;
    }

    public void loadExisting(String orderId) {
        callJobComputation(() -> orderService.getOrderById(orderId),
                (order) -> {
                    this.order = order;
                    orderWrapper = OrderWrapper.from(order);
                    onLoadOrderSuccess.postValue(order);
                    updateUI();
                });
        isStressTestEnabled.postValue(preferences.isStressTestEnabled());
    }

    public void saveForLater() {
        runJobComputation(
                () -> {
                    syncCashAndDb();
                    recalculateOrder();
                },
                () -> {
                    liveRunSync.postValue(null);
                    onFinishOrder.postValue(false);
                });
    }

    public Order getCurrentOrder() {
        return order;
    }

    public void addProduct(OrderProduct product, Integer quantity, boolean hasBackorder) {
        runJobComputation(() -> {
            product.ifRental(rentalProduct -> orderWrapper.setRentalItems(rentalProduct, quantity));
            product.ifConsumable(consumableProduct -> orderWrapper.setConsumableItems(consumableProduct, quantity, hasBackorder));
        }, this::updateUI);
    }

    public void addProduct(OrderItem orderItem, Integer quantity, boolean hasBackorder) {
        runJobComputation(() -> {
            orderItem.ifConsumable(it -> orderWrapper.setConsumableItems(it.getConsumableProduct(), quantity, hasBackorder));
            orderItem.ifRental(it -> orderWrapper.setRentalItems(it.getRentalProduct(), quantity));
        }, this::updateUI);
    }

    public void onScanProduct(ProductWrapper product) {
        if (order == null) return;
        product.getProduct().ifConsumable(it -> {
            OrderConsumableItem item = orderWrapper.getConsumableOrNull(it);
            if (item != null) {
                liveScannedExistingProduct.setValue(item);
            } else {
                liveScannedNewProduct.setValue(product);
            }
        });

        product.getProduct().ifRental(it -> {
            OrderRentalItem item = orderWrapper.getRentalOrNull(it);
            if (item != null) {
                liveScannedExistingProduct.setValue(item);
            } else {
                liveScannedNewProduct.setValue(product);
            }
        });
    }

    public List<OrderItem> getSelectedItems() {
        return orderWrapper.getOrderItems();
    }

    public void removeItem(OrderItem orderItem) {
        runJobComputation(() -> {
            orderItem.ifRental(this::removeRentalItem);
            orderItem.ifConsumable(this::removeConsumableItem);
            recalculateOrder();
        }, this::updateUI);
    }

    public void deleteOrder() {
        runJobComputation(
                this::delete,
                () -> {
                    liveRunSync.postValue(null);
                    onFinishOrder.postValue(false);
                });
    }

    public void createPickTicket(Boolean shouldStartPickUp) {
        runJobComputation(
                () -> {
                    rentalService.saveAll(orderWrapper.getRentalItems());
                    updateConsumableItemInOrder();
                    consumableService.saveAll(orderWrapper.getConsumableItems());
                    order.setOrderStatusId(OrderStatus.pickUp.getId());
                    recalculateOrder();
                },
                () -> {
                    liveRunSync.postValue(null);
                    onFinishOrder.postValue(shouldStartPickUp);
                });
    }

    public void add10Product() {
        List<OrderProduct> consumableProducts = RandomListExtensionsKt.getRandomList(ProductService.get()
                .loadAllConsumablesLazy(),5);
        List<OrderProduct> rentalProducts = RandomListExtensionsKt.getRandomList(ProductService.get()
                .loadAllRentalsLazy(),5);

        List<OrderProduct> products = new ArrayList<>(rentalProducts);
        products.addAll(consumableProducts);

        for (OrderProduct product : products) {
            addProduct(new ProductWrapper(product).getProduct(), 1, false);
        }
    }


    private void updateConsumableItemInOrder() {
        order.resetConsumableItems();
        ConsumableDiff consumableDiff = orderWrapper.getConsumableDiff(order.getConsumableItems());
        consumableService.insertAll(consumableDiff.getToInsert());
        consumableService.updateAll(consumableDiff.getToUpdate());
        order.resetRentalItems();
    }

    public String getOrderId() {
        return order.getOrderId();
    }

    public void setBackorderFlag(OrderItem item, boolean isBackorder) {
        runJobComputation(() -> item.ifConsumable(it -> {
            it.isBackorder(isBackorder);
            orderWrapper.updateConsumable(it);
            onViewUpdated.setValue(orderWrapper);
        }), this::updateUI);
    }

    private void delete() {
        if (order != null) {
            orderWrapper.removeAllRental();
            orderWrapper.removeAllConsumables();
            onViewUpdated.postValue(orderWrapper);
            orderService.delete(order);
            order.getAllItems().forEach(it -> {
                it.ifConsumable(consumableService::delete);
                it.ifRental(rentalService::delete);
            });
        }
    }

    private void removeRentalItem(OrderRentalItem item) {
        List<OrderRentalItem> rentalItems = order.getRentalItems()
                .stream()
                .filter(rentalItem -> rentalItem.getProductNumber().equals(item.getProductNumber()))
                .collect(Collectors.toList());
        rentalService.deleteAll(rentalItems);
        orderWrapper.removeRentalItems(item.getProductNumber());
    }

    private void removeConsumableItem(OrderConsumableItem item) {
        consumableService.delete(item);
        orderWrapper.removeConsumableItems(item);
    }

    private void recalculateOrder() {
        if (order != null) {
            Operation orderSum = Operation.with(0);
            order.resetConsumableItems();
            order.getConsumableItems().forEach(item -> {
                Operation itemTotal = Operation
                        .with(item.getPrice())
                        .multiply(item.getQuantity());
                orderSum.add(itemTotal);
            });

            order.resetRentalItems();
            order.getRentalItems().forEach(item -> {
                Operation itemTotal = Operation
                        .with(item.getPrice());
                orderSum.add(itemTotal);
            });

            order.setOrderSum(orderSum.toDouble());
            orderService.update(order);
        }
    }

    private void updateUI() {
        onViewUpdated.postValue(orderWrapper);
    }

    private void syncCashAndDb() {
        updateConsumableItemInOrder();
        RentalDiff rentalDiff = orderWrapper.getRentalDiff(order.getRentalItems());
        rentalService.deleteAll(rentalDiff.getToDelete());
        rentalService.insertAll(rentalDiff.getToInsert());
    }

}
