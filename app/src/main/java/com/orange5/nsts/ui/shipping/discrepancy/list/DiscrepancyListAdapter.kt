package com.orange5.nsts.ui.shipping.discrepancy.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.orange5.nsts.R
import com.orange5.nsts.ui.shipping.discrepancy.repository.Discrepancy
import com.orange5.nsts.ui.shipping.discrepancy.repository.Discrepancy.Type.SHIPMENT_SHORTAGE
import com.orange5.nsts.util.format.DateFormatter
import kotlinx.android.synthetic.main.item_list_discrepancy.view.*

class DiscrepancyListAdapter : ListAdapter<Discrepancy, DiscrepancyListAdapter.DiscrepanciesVH>(ListUtils()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DiscrepanciesVH =
            DiscrepanciesVH(LayoutInflater.from(parent.context).inflate(R.layout.item_list_discrepancy, parent, false))

    override fun onBindViewHolder(holder: DiscrepanciesVH, position: Int) {
        holder.bind(getItem(position))
    }

    private class ListUtils : DiffUtil.ItemCallback<Discrepancy>() {
        override fun areItemsTheSame(oldItem: Discrepancy, newItem: Discrepancy) = oldItem == newItem
        override fun areContentsTheSame(oldItem: Discrepancy, newItem: Discrepancy) = oldItem.id == newItem.id
    }

    class DiscrepanciesVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Discrepancy) {
            itemView.run {
                title.text = item.title
                type.text = resources.getString(item.type.titleResId)
                date.text = DateFormatter.toUIDate(item.date)
                description.text = item.description
                uun.isVisible = item.isRental()
                quantityConsumable.isVisible = !item.isRental()
                uun.text = resources.getString(R.string.shipping_notice_uun, item.uun)
                val count = if (item.type == SHIPMENT_SHORTAGE) -item.quantity else item.quantity
                quantityConsumable.text = resources.getString(R.string.quantity_n, count)
                notes.isVisible = item.comment.isNotEmpty()
                notes.text = item.comment
            }
        }
    }
}