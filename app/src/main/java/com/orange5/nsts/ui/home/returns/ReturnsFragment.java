package com.orange5.nsts.ui.home.returns;

import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.entities.RentalReturn;
import com.orange5.nsts.ui.base.BaseFragment;
import com.orange5.nsts.ui.home.HomeViewModel;
import com.orange5.nsts.ui.report.ReportActivity;
import com.orange5.nsts.ui.report.ReportType;
import com.orange5.nsts.util.CollectionUtils;
import com.orange5.nsts.util.DateSections;
import com.orange5.nsts.util.adapters.recycler.RecyclerAdapterBuilder;

import java.util.List;

import butterknife.BindView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ReturnsFragment extends BaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView returnsList;
    @BindView(R.id.noData)
    TextView noData;
    @BindView(R.id.refresh)
    SwipeRefreshLayout refresh;

    private HomeViewModel viewModel;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_refreshed_list;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = getVMFromActivity(HomeViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        noData.setVisibility(GONE);
        viewModel.liveReturns.observe(getViewLifecycleOwner(), this::submitList);
        viewModel.rentalsProgress.observe(getViewLifecycleOwner(), this::showLoading);
        refresh.setOnRefreshListener(() -> viewModel.refreshReturns());
    }

    private void submitList(Pair<List<RentalReturn>, String> rentalsPair) {
        if (CollectionUtils.isEmpty(rentalsPair.first)) {
            noData.setVisibility(VISIBLE);
            return;
        } else {
            noData.setVisibility(GONE);
        }

        SparseArray<String> sections = DateSections.generateSections(rentalsPair.first, true, null, null);
        new RecyclerAdapterBuilder<ReturnView, RentalReturn>(returnsList)
                .data(() -> rentalsPair.first)
                .viewFactory(() -> new ReturnView(requireContext()))
                .bind((view, model) -> view.fill(model, rentalsPair.second))
                .onClick(R.id.contents, this::openReturnDetails)
                .mutate((view, model, position) -> {
                    String sectionTitle = sections.get(position);
                    if (sectionTitle != null) {
                        view.showSectionHeader(sectionTitle);
                        return;
                    }
                    view.hideSectionHeader();
                })
                .vertical();

    }

    private void showLoading(Boolean isShown) {
        refresh.setRefreshing(isShown);
    }

    private void openReturnDetails(int position, RentalReturn rentalReturn) {
        startActivity(ReportActivity.newIntent(requireContext(), ReportType.RETURN,
                rentalReturn.getRentalReturnId()));
    }
}
