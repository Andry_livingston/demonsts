package com.orange5.nsts.api.converter

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.orange5.nsts.data.db.entities.Order
import java.lang.reflect.Type

class OrderDeserializer : JsonDeserializer<Order> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Order {
        var deserialized = Order()
        val jsonObject = json?.asJsonObject
        jsonObject?.let {
            deserialized = Order(
                    it.get("OrderId")?.asSafeString(),
                    it.get("CreatorCustomerId")?.asSafeString(),
                    it.get("OrderDate")?.asSafeString(),
                    it.get("OrderCompleteDate")?.asSafeString(),
                    it.get("Details")?.asSafeString(),
                    it.get("SignImage")?.asSafeString()?.toByteArray() ?: byteArrayOf(),
                    it.get("ReceiptText")?.asSafeString(),
                    it.get("OrderStatusId")?.asInt,
                    it.get("WhoPickedOrderCustomerId")?.asSafeString(),
                    it.get("LimitOverwriteComment")?.asSafeString(),
                    it.get("OrderSum")?.asDouble ?: 0.00,
                    it.get("OrderNumber")?.asSafeString(),
                    it.get("ApprovedByCustomerId")?.asSafeString(),
                    it.get("CreatedByUserId")?.asSafeString(),
                    it.get("WasChangedDate")?.asSafeString(),
                    it.get("AccountingCode")?.asSafeString(),
                    it.get("WebOrderNumber")?.asSafeString(),
                    it.get("DeliveryDate")?.asSafeString())
        }
        return deserialized
    }
}
