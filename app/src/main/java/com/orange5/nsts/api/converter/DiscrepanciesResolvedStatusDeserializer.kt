package com.orange5.nsts.api.converter

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.orange5.nsts.data.db.entities.DiscrepanciesResolvedStatus
import java.lang.reflect.Type

class DiscrepanciesResolvedStatusDeserializer : JsonDeserializer<DiscrepanciesResolvedStatus> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): DiscrepanciesResolvedStatus {
        var deserialized = DiscrepanciesResolvedStatus()
        val jsonObject = json?.asJsonObject
        jsonObject?.let {
            deserialized = DiscrepanciesResolvedStatus(
                    it.get("ResolvedStatusId")?.asSafeLong() ?: 0,
                    it.get("ResolvedStatusName")?.asSafeString(),
                    it.get("SelectBin")?.asSafeString(),
                    if (it.get("HideBinSelector").asBoolean) 1.toByte() else 0.toByte())
        }
        return deserialized
    }
}