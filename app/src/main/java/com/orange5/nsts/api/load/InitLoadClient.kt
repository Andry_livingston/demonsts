package com.orange5.nsts.api.load

interface InitLoadClient {

    fun initLoad(table: String): InitLoadWrapper
}