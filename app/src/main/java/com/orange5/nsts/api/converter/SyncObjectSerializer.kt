package com.orange5.nsts.api.converter

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import com.orange5.nsts.data.db.entities.SyncTransferObject
import java.lang.reflect.Type
import java.nio.charset.Charset

class SyncObjectSerializer : JsonSerializer<SyncTransferObject>, JsonDeserializer<SyncTransferObject> {

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): SyncTransferObject {
        var deserialized = SyncTransferObject()
        val jsonObject = json?.asJsonObject
        jsonObject?.let {
            deserialized = SyncTransferObject(
                    it.get("IncrementNumber").asLong,
                    it.get("TransferObjectId").asString,
                    it.get("TypeData").asString,
                    it.get("QueryType").asString,
                    it.get("QueryTable").asString,
                    it.get("QueryKey").asString,
                    it.get("QueryFields").asString,
                    it.get("DateTimeSaved").asString,
                    it.get("Action").asString,
                    it.get("IsMine").asBoolean,
                    it.get("Executed").toString(),
                    it.get("DataObject").asString.toByteArray()
            )
        }
        return deserialized
    }

    override fun serialize(src: SyncTransferObject?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement =
            JsonObject().apply {
                addProperty("TransferObjectId", src?.transferObjectId)
                addProperty("TypeData", src?.typeData)
                addProperty("QueryType", src?.queryType)
                addProperty("QueryTable", src?.queryTable)
                addProperty("QueryKey", src?.queryKey)
                addProperty("QueryFields", src?.queryFields)
                addProperty("DateTimeSaved", src?.dateTimeSaved)
                addProperty("Action", src?.action)
                addProperty("IsMine", src?.isMine)
                addProperty("Executed", src?.executed)
                addProperty("DataObject", src?.data
                        ?.toString(Charset.defaultCharset())
                        ?.replace("\n", "") ?: "")
                addProperty("IncrementNumber", src?.incrementNumber)
            }
}
