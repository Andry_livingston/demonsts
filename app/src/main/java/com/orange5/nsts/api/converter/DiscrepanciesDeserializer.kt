package com.orange5.nsts.api.converter

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.orange5.nsts.data.db.entities.Discrepancies
import java.lang.reflect.Type

class DiscrepanciesDeserializer : JsonDeserializer<Discrepancies> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Discrepancies {
        var deserialized = Discrepancies()
        val jsonObject = json?.asJsonObject
        jsonObject?.let {
            deserialized = Discrepancies(
                    it.get("DiscrepanciesID")?.asSafeString(),
                    it.get("DescripancyDescription")?.asSafeString(),
                    it.get("DiscrepancyTypeId")?.asSafeInt(),
                    it.get("AddingDate")?.toDate(),
                    it.get("ConsumableProductId")?.asSafeString(),
                    it.get("ConsumableQuantity")?.asSafeInt(),
                    it.get("RentalProductId")?.asSafeString(),
                    it.get("RentalUniqueUnitNumber")?.asSafeString(),
                    it.get("Comment")?.asSafeString(),
                    if (it.get("IsResolved")?.asSafeBoolean() == true) 1.toByte() else 0.toByte(),
                    it.get("ResolvedStatusId")?.asSafeInt(),
                    it.get("ResolvedComment")?.asSafeString(),
                    it.get("ResolvedDate")?.asSafeString(),
                    it.get("UPDescription")?.asSafeString(),
                    it.get("CreatedByUserId")?.asSafeString(),
                    it.get("UpdatedByUserId")?.asSafeString())
        }
        return deserialized
    }
}