package com.orange5.nsts.api.converter

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.orange5.nsts.data.db.entities.RentalProductItem
import java.lang.reflect.Type

class RentalProductItemDeserializer : JsonDeserializer<RentalProductItem> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): RentalProductItem {
        var deserialized = RentalProductItem()
        val jsonObject = json?.asJsonObject
        jsonObject?.let {
            deserialized = RentalProductItem(
                    it.get("UniqueUnitNumber")?.asSafeString(),
                    it.get("ProductId")?.asSafeString(),
                    it.get("Status")?.asSafeInt(),
                    if (it.get("Allocated").asSafeBoolean()) 1.toByte() else 0.toByte(),
                    it.get("BinId")?.asSafeString(),
                    it.get("CertDate")?.asSafeString(),
                    it.get("CertDuration")?.asSafeInt(),
                    it.get("ContainerID")?.asSafeString(),
                    it.get("Remarks")?.asSafeString())
        }
        return deserialized
    }
}
