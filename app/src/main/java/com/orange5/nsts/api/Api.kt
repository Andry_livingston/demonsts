package com.orange5.nsts.api

import com.orange5.nsts.api.load.InitLoadResponse
import com.orange5.nsts.sync.SyncResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface Api {

    @GET("NSTSData/entity/{entity}")
    fun initLoad(@Path("entity") entity: String,
                   @Query("pagesize") pageSize: Int,
                   @Query("page") page: Int): Call<InitLoadResponse>

    @POST("api/NSTSData/GetData")
    fun sendSyncData(@Header("Content-Type") type: String,
                     @Body syncResponse: SyncResponse): Call<SyncResponse>

    @GET("api/NSTSData/Version")
    fun getVersion(@Header("Content-Type") type: String): Call<String>

    @POST("api/NSTSData/ConfirmDataS")
    fun confirmData(@Header("Content-Type") type: String, @Body uuids: Array<String>): Call<Boolean>
}