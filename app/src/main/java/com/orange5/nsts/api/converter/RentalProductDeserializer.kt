package com.orange5.nsts.api.converter

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.orange5.nsts.data.db.entities.RentalProduct
import java.lang.reflect.Type

class RentalProductDeserializer : JsonDeserializer<RentalProduct> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): RentalProduct {
        var deserialized = RentalProduct()
        val jsonObject = json?.asJsonObject
        jsonObject?.let {
            deserialized = RentalProduct(
                    it.get("ProductId")?.asSafeString(),
                    it.get("ProductNumber")?.asSafeString(),
                    it.get("ProductDescription")?.asSafeString(),
                    it.get("Manufacturer")?.asSafeString(),
                    it.get("Make")?.asSafeString(),
                    it.get("Model")?.asSafeString(),
                    it.get("ReplacementCost")?.asDouble ?: 0.0,
                    it.get("Picture")?.asSafeString(),
                    it.get("AuthLevel")?.asInt,
                    it.get("ManufacturerNumber")?.asSafeString(),
                    it.get("RentalPeriod")?.asInt,
                    if (it.get("isCustomerOwned").asBoolean) 1.toByte() else 0.toByte(),
                    it.get("WasChangedDate")?.asSafeString(),
                    it.get("CategoryId")?.asSafeString(),
                    it.get("Info")?.asSafeString(),
                    it.get("DefaultBinId")?.asSafeString()
            )
        }
        return deserialized
    }
}
