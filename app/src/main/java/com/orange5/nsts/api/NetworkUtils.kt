package com.orange5.nsts.api

import android.content.Context
import android.net.ConnectivityManager
import timber.log.Timber
import java.net.InetAddress
import java.net.NetworkInterface
import java.util.*


class NetworkUtils {

    companion object Network {

        fun isWifiConnected(context: Context): Boolean {
            var isConnected = false
            val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
            manager?.let {
                val networkInfo = manager.activeNetworkInfo
                isConnected = networkInfo?.type == ConnectivityManager.TYPE_WIFI && networkInfo.isConnectedOrConnecting
            }
            return isConnected
        }

        fun getIPAddress(): String? {
            var ipAddress: String? = ""
            try {
                val interfaces: List<NetworkInterface> = Collections.list(NetworkInterface.getNetworkInterfaces())
                for (networkInterface in interfaces) {
                    val addresses: List<InetAddress> = Collections.list(networkInterface.inetAddresses)
                    for (address in addresses) {
                        if (!address.isLoopbackAddress) {
                            val hostAddress = address.hostAddress
                            if (isIPv4(hostAddress)) {
                                ipAddress = hostAddress
                            }
                        }
                    }
                }
            } catch (e: Exception) {
                Timber.e(e, "Can't get localHost ip address")
            }
            return ipAddress
        }

        private fun isIPv4(hostAddress: String): Boolean {
            return hostAddress.indexOf(':') == -1
        }
    }
}