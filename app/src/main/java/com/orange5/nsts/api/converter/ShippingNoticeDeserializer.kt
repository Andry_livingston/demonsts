package com.orange5.nsts.api.converter

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.orange5.nsts.data.db.entities.ShippingNotice
import java.lang.reflect.Type

class ShippingNoticeDeserializer : JsonDeserializer<ShippingNotice> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): ShippingNotice {
        var deserialized = ShippingNotice()
        val jsonObject = json?.asJsonObject
        jsonObject?.let {
            deserialized = ShippingNotice(
                    it.get("ShipingNoticeId")?.asSafeString(),
                    it.get("TransferNumber")?.asSafeString(),
                    it.get("ConsumableProductNumber")?.asSafeString(),
                    it.get("ConsumableQuantity")?.asSafeInt(),
                    it.get("ConsumableQuantityStart")?.asSafeInt(),
                    it.get("RentalProductNumber")?.asSafeString(),
                    it.get("RentalUniqueUnitNumber")?.asSafeString(),
                    it.get("ShippingDate")?.asSafeString(),
                    if (it.get("isProcessed")?.asSafeBoolean() == true) 1.toByte() else 0.toByte(),
                    it.get("CreatedByUserId")?.asSafeString(),
                    it.get("UpdatedByUserId")?.asSafeString())
        }
        return deserialized
    }
}