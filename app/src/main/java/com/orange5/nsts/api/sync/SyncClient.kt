package com.orange5.nsts.api.sync

import com.orange5.nsts.sync.SyncResponse

interface SyncClient {

    fun sendData(request: SyncResponse): SyncResponse
    fun confirmDataS(uuids: Array<String>): Boolean
    fun version() : String
}