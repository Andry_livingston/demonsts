package com.orange5.nsts.api.sync

import com.orange5.nsts.api.Api
import com.orange5.nsts.api.body
import com.orange5.nsts.sync.SyncResponse

class SyncClientImpl(private val api: Api) : SyncClient {

    override fun sendData(request: SyncResponse): SyncResponse = api.sendSyncData(JSON_TYPE, request).body()

    override fun confirmDataS(uuids: Array<String>): Boolean = api.confirmData(JSON_TYPE, uuids).body()

    override fun version(): String = api.getVersion(JSON_TYPE).body()

    private companion object {
        const val JSON_TYPE = "application/json"
    }
}