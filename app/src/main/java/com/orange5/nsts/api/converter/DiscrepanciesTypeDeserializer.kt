package com.orange5.nsts.api.converter

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.orange5.nsts.data.db.entities.DiscrepanciesType
import java.lang.reflect.Type

class DiscrepanciesTypeDeserializer : JsonDeserializer<DiscrepanciesType> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): DiscrepanciesType {
        var deserialized = DiscrepanciesType()
        val jsonObject = json?.asJsonObject
        jsonObject?.let {
            deserialized = DiscrepanciesType(
                    it.get("TypeId")?.asSafeLong() ?: 0,
                    it.get("TypeName")?.asSafeString(),
                    if (it.get("IsNegative")?.asSafeBoolean() == true) 1.toByte() else 0.toByte(),
                    it.get("ResolutionType")?.asSafeString())
        }
        return deserialized
    }
}
