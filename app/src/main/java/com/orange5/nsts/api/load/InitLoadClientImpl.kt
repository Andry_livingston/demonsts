package com.orange5.nsts.api.load

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.orange5.nsts.api.Api
import com.orange5.nsts.api.body


class InitLoadClientImpl(private val api: Api) : InitLoadClient {

    override fun initLoad(table: String): InitLoadWrapper = loadData(table, this::loadFromApi)

    private fun loadFromApi(table: String, pageSize: Int, page: Int): InitLoadResponse = api.initLoad(table, pageSize, page).body()

    private fun loadData(table: String,
                         sync: (table: String, page: Int, pageSize: Int) -> InitLoadResponse): InitLoadWrapper {
        val data = mutableListOf<JsonElement>()
        var count: Int
        var page = 0
        while (true) {
            val next = sync(table, PAGE_SIZE, page)
            val list = next.entities?.asJsonArray ?: JsonArray()
            data.addAll(list)
            count = next.entityCount
            if (list.size() < PAGE_SIZE) break
            else page++
        }
        return InitLoadWrapper(count, data)
    }

    companion object {
        private const val PAGE_SIZE = 300
    }
}

data class InitLoadResponse(val tableName: String = "",
                            val entityCount: Int = 0,
                            val entities: JsonElement? = null)

data class InitLoadWrapper(val entityCount: Int = 0,
                           val entities: List<JsonElement> = emptyList())