package com.orange5.nsts.api.converter

import com.google.gson.JsonElement
import com.orange5.nsts.util.format.DateFormatter

fun JsonElement.asSafeString() = if (isJsonNull) null else asString

fun JsonElement.asSafeInt() = if (isJsonNull) null else asInt

fun JsonElement.asSafeBoolean() = if (isJsonNull) false else asBoolean

fun JsonElement.asSafeLong() = if (isJsonNull) null else asLong

fun JsonElement.toDate() =
        if (isJsonNull) null
        else DateFormatter.convertDbDateToUI(DateFormatter.getInitLoadDateTime(asString))


