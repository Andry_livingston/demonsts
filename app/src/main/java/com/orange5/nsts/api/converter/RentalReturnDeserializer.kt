package com.orange5.nsts.api.converter

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.orange5.nsts.data.db.entities.RentalReturn
import java.lang.reflect.Type

class RentalReturnDeserializer : JsonDeserializer<RentalReturn> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): RentalReturn {
        var deserialized = RentalReturn()
        val jsonObject = json?.asJsonObject
        jsonObject?.let {
            deserialized = RentalReturn(
                    it.get("RentalReturnId")?.asSafeString(),
                    it.get("ReturnNumber")?.asSafeString(),
                    it.get("StartDate")?.asSafeString(),
                    it.get("CompleteDate")?.asSafeString(),
                    it.get("DataObject")?.asSafeString()?.toByteArray())
        }
        return deserialized
    }
}
