package com.orange5.nsts.api.converter

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import com.orange5.nsts.data.db.entities.OrderConsumableItem
import java.lang.reflect.Type

class OrderConsumableItemDeserializer : JsonSerializer<OrderConsumableItem>, JsonDeserializer<OrderConsumableItem> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): OrderConsumableItem {
        var deserialized = OrderConsumableItem()
        val jsonObject = json?.asJsonObject
        jsonObject?.let {
            deserialized = OrderConsumableItem(
                    it.get("OrderItemId")?.asSafeString(),
                    it.get("OrderId")?.asSafeString(),
                    it.get("ProductId")?.asSafeString(),
                    it.get("Quantity")?.asSafeInt(),
                    it.get("Picked")?.asSafeInt(),
                    it.get("Price")?.asDouble,
                    if (it.get("isBackOrder").asSafeBoolean()) 1.toByte() else 0.toByte())
        }
        return deserialized
    }

    override fun serialize(src: OrderConsumableItem?, typeOfSrc: Type?, context: JsonSerializationContext?) =
            JsonObject().apply {
                addProperty("OrderItemId", src?.orderItemId)
                addProperty("OrderId", src?.orderId)
                addProperty("ProductId", src?.productId)
                addProperty("Quantity", src?.quantity)
                addProperty("Picked", src?.picked)
                addProperty("Price", src?.price)
                addProperty("isBackOrder", src?.isBackOrder?.toInt() != 0)
            }
}