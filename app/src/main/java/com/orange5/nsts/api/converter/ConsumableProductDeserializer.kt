package com.orange5.nsts.api.converter

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.orange5.nsts.data.db.entities.ConsumableProduct
import com.orange5.nsts.util.format.DateFormatter
import java.lang.reflect.Type

class ConsumableProductDeserializer : JsonDeserializer<ConsumableProduct> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): ConsumableProduct {
        var deserialized = ConsumableProduct()
        val jsonObject = json?.asJsonObject
        jsonObject?.let {
            deserialized = ConsumableProduct(
                    it.get("ProductId")?.asSafeString(),
                    it.get("ProductNumber")?.asSafeString(),
                    it.get("ProductDescription")?.asSafeString(),
                    it.get("ManufacturerNumber")?.asSafeString(),
                    it.get("MaterialCategory")?.asSafeString(),
                    it.get("Make")?.asSafeString(),
                    it.get("Model")?.asSafeString(),
                    it.get("UnitOfMeasure")?.asSafeString(),
                    it.get("Price").asDouble,
                    it.get("Picture")?.asSafeString(),
                    it.get("AuthLevel")?.asInt,
                    if (it.get("isCustomerOwned").asBoolean) 1.toByte() else 0.toByte(),
                    DateFormatter.getInitLoadDateTime(it.get("WasChangedDate")?.asSafeString()).toDate(),
                    it.get("DefaultBinId")?.asSafeString(),
                    it.get("CategoryId")?.asSafeString(),
                    it.get("Info")?.asSafeString()
            )
        }
        return deserialized
    }
}
