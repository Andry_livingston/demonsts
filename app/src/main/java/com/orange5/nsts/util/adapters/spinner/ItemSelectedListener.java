package com.orange5.nsts.util.adapters.spinner;

public interface ItemSelectedListener<T> {
    void onItemSelected(T item);
}
