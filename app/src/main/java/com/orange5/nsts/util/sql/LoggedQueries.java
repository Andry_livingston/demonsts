package com.orange5.nsts.util.sql;

import org.greenrobot.greendao.query.Query;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class LoggedQueries {
    private static LoggedQueries instance = new LoggedQueries(60);


    public static List<String> asStrings() {
        List<String> result = new ArrayList<>();
        try {
            Field paramsField = Query.class.getSuperclass().getSuperclass()
                    .getDeclaredField("parameters");
            paramsField.setAccessible(true);
            Field sqlField = Query.class.getSuperclass().getSuperclass().getDeclaredField("sql");
            sqlField.setAccessible(true);
            instance.list.forEach(item -> {
                try {
                    String sql = sqlField.get(item).toString();
                    String[] params = (String[]) paramsField.get(item);
                    for (String param : params) {
                        sql = sql.replace("?", param);
                    }
                    result.add(sql);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    private final int maxCapacity;
    private final List<Query> list = new ArrayList<>();

    public LoggedQueries(int size) {
        this.maxCapacity = size;
    }

    public void add(String log) {
    }

    public static <Entity> void logQuery(Query<Entity> query) {
        if (instance.maxCapacity == instance.list.size()) {
            instance.list.remove(0);
        }
        instance.list.add(query);
    }
}
