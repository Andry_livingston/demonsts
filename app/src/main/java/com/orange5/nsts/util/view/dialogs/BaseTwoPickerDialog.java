package com.orange5.nsts.util.view.dialogs;

import android.text.SpannableString;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orange5.nsts.R;
import com.orange5.nsts.ui.base.BaseActivity;
import com.orange5.nsts.util.UI;

import java.util.Collections;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import butterknife.BindView;
import com.orange5.nsts.util.view.picker.PickerView;
import com.orange5.nsts.util.view.picker.ValuePickerAdapter;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public abstract class BaseTwoPickerDialog<T, V> extends BaseDialog {

    @BindView(R.id.left)
    TextView left;
    @BindView(R.id.right)
    TextView right;
    @BindView(R.id.leftPicker)
    PickerView leftPicker;
    @BindView(R.id.rightPicker)
    PickerView rightPicker;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.customQuantity)
    ImageButton customQuantity;
    @BindView(R.id.editQuantity)
    EditText editQuantity;
    @BindView(R.id.actionButton)
    Button actionButton;
    @BindView(R.id.pickerContainer)
    LinearLayout pickerContainer;
    @BindView(R.id.toSoftMode)
    Button toSoftMode;

    public PickerView getRightPicker() {
        return rightPicker;
    }

    protected List<T> bins = Collections.emptyList();
    private T selectedBin;
    private V selectedQuantity;
    private final BiConsumer<T, V> biConsumer;
    protected Consumer<Void> softMode;

    protected BaseTwoPickerDialog(BaseActivity activity,
                                  BiConsumer<T, V> biConsumer) {
        super(activity);
        this.biConsumer = biConsumer;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.dialog_two_pickers;
    }

    protected abstract SpannableString formatActionButtonTitle();

    protected abstract List<V> getRightPickerList(T selectedBin);

    protected void setUpLeftPicker() {
        selectedBin = getDefaultBin();
        setUpRightPicker(getRightPickerList(selectedBin));
        new ValuePickerAdapter<>(mapBinsToString())
                .selectedPositionConsumer(position -> {

                    if (position >= bins.size()) selectedBin = null;
                    else selectedBin = bins.get(position);

                    if (isOversell(selectedBin)) rightPicker.setOverflowPosition(0);
                    else rightPicker.setOverflowPosition(-1);

                    setUpRightPicker(getRightPickerList(selectedBin));
                })
                .attachTo(leftPicker);
    }

    private void setUpRightPicker(List<V> items) {
        if (items.size() > 0) {
            selectedQuantity = items.get(0);
            setActionButtonTitle();
        }
        new ValuePickerAdapter<>(items)
                .selectedItemConsumer(selected -> {
                    selectedQuantity = selected;
                    setActionButtonTitle();
                })
                .attachTo(rightPicker);
        setRightPickerPosition(items.size());
    }

    protected T getDefaultBin() {
        return bins.get(0);
    }

    protected void setListeners() {
        actionButton.setOnClickListener(v -> pickQuantity());

        customQuantity.setOnClickListener(v -> {
            if (editQuantity.getVisibility() == GONE) {
                UI.openKeyboard(editQuantity);
                editQuantity.setVisibility(VISIBLE);
                pickerContainer.setVisibility(GONE);

            } else {
                UI.closeKeyboard(editQuantity);
                editQuantity.setVisibility(GONE);
                pickerContainer.setVisibility(VISIBLE);
            }
        });

        toSoftMode.setOnClickListener(v -> {
            if (softMode != null) softMode.accept(null);
        });
    }

    protected void setEditQuantityVisibility(boolean visible) {
        left.setVisibility(visible ? GONE : VISIBLE);
        rightPicker.setVisibility(visible ? GONE : VISIBLE);
        customQuantity.setVisibility(visible ? VISIBLE : GONE);
    }

    protected void setTitle(String title) {
        this.title.setText(title);
    }

    protected List<String> mapBinsToString() {
        return bins
                .stream()
                .map(Object::toString)
                .collect(Collectors.toList());
    }

    protected boolean isOversell(T selectedBin) {
        return false;
    }


    protected T getSelectedBin() {
        return selectedBin;
    }

    protected V getSelectedQuantity() {
        return selectedQuantity;
    }

    protected void pickQuantity() {
        biConsumer.accept(selectedBin, selectedQuantity);
        dismiss();
    }

    protected void setRightPickerPosition(int position) {
        rightPicker.setSelectedItemPosition(position);
    }

    private void setActionButtonTitle() {
        actionButton.setText(formatActionButtonTitle());
    }
}
