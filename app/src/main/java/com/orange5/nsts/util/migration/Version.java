package com.orange5.nsts.util.migration;

public class Version {
    private final int oldVersion, newVersion;

    public static Version of(int oldVersion, int newVersion) {
        return new Version(oldVersion, newVersion);
    }

    private Version(int oldVersion, int newVersion) {
        this.oldVersion = oldVersion;
        this.newVersion = newVersion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Version that = (Version) o;

        return oldVersion == that.oldVersion && newVersion == that.newVersion;
    }

    @Override
    public int hashCode() {
        int result = oldVersion;
        result = 31 * result + newVersion;
        return result;
    }

    @Override
    public String toString() {
        return "Version{" +
                "oldVersion=" + oldVersion +
                ", newVersion=" + newVersion +
                '}';
    }
}
