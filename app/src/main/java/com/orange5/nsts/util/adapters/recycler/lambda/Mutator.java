package com.orange5.nsts.util.adapters.recycler.lambda;

import android.view.View;

public interface Mutator<V extends View,M> {
    void mutate(V view, M model, int position);
}
