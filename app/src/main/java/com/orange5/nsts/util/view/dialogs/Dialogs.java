package com.orange5.nsts.util.view.dialogs;

import android.app.AlertDialog;

import com.orange5.nsts.ui.base.BaseActivity;

public class Dialogs {

    private final AlertDialog.Builder builder;
    private AlertDialog alertDialog;
    private Runnable okAction = () -> {
    };

    public static Dialogs newOkDialog(Runnable okAction) {
        Dialogs dialogs = new Dialogs();
        dialogs.okAction = okAction;
        dialogs.builder.setPositiveButton("Ok", (dialog, which) -> {
            dialogs.okAction.run();
            dialogs.alertDialog.dismiss();
        });
        return dialogs;
    }

    public static Dialogs newOkDialog() {
        Dialogs dialogs = new Dialogs();
        dialogs.builder.setPositiveButton("Ok", (dialog, which) -> {
            dialogs.okAction.run();
            dialogs.alertDialog.dismiss();
        });
        return dialogs;
    }

    public static Dialogs newDialog() {
        return new Dialogs();
    }

    public void show() {
        create();
        builder.show();
    }

    public Dialogs yes(Runnable action) {
        builder.setPositiveButton("Yes", (dialog, which) -> {
            action.run();
            alertDialog.dismiss();
        });
        return this;
    }

    public Dialogs no(Runnable action) {
        builder.setNegativeButton("No", (dialog, which) -> {
            if(action != null) action.run();
            alertDialog.dismiss();
        });
        return this;
    }
    public Dialogs no(String text, Runnable action) {
        builder.setNegativeButton(text, (dialog, which) -> {
            if(action != null) action.run();
            alertDialog.dismiss();
        });
        return this;
    }

    public Dialogs neutral(String btmTitle, Runnable action) {
        builder.setNeutralButton(btmTitle, (dialog, which) -> {
            action.run();
            alertDialog.dismiss();
        });
        return this;
    }

    public Dialogs cancel(Runnable action) {
        builder.setNeutralButton("Cancel", (dialog, which) -> {
            action.run();
            alertDialog.dismiss();
        });
        return this;
    }

    public Dialogs action(String text, Runnable action) {
        builder.setPositiveButton(text, (dialog, which) -> {
            action.run();
            alertDialog.dismiss();
        });
        return this;
    }

    public AlertDialog create() {
        return alertDialog = builder.create();
    }

    private Dialogs() {
        builder = new AlertDialog.Builder(BaseActivity.getActive());
    }

    public Dialogs setTitle(String title) {
        builder.setTitle(title);
        return this;
    }

    public Dialogs setTitle(int title) {
        builder.setTitle(title);
        return this;
    }

    public Dialogs setMessage(String message) {
        builder.setMessage(message);
        return this;
    }

    public Dialogs setMessage(int message) {
        builder.setMessage(message);
        return this;
    }
}
