package com.orange5.nsts.util.db;

import java.util.Random;
import java.util.UUID;

public class GUIDFactory {
    public static String newUUID() {
        return UUID.randomUUID().toString();
    }

    public static String syncObjectUUID(byte[] uuid) {
        return UUID.nameUUIDFromBytes(uuid).toString();
    }

    public static String newUnique8() {
        Random random = new Random();
        String generated = Long.toHexString(System.currentTimeMillis() / 1000).substring(3);
        String hex = "abcdef0123456789";
        char[] chars = new char[3];
        for (int i = 0; i < chars.length; i++) {
            chars[i] = hex.charAt(random.nextInt(16));
        }
        return String.valueOf(chars) + generated;
    }
}
