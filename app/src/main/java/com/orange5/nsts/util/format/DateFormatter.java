package com.orange5.nsts.util.format;

import android.annotation.SuppressLint;

import com.orange5.nsts.util.StringUtils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.text.SimpleDateFormat;

import timber.log.Timber;

@SuppressLint("SimpleDateFormat")
public class DateFormatter {

    private static final int RENTAL_PERIOD = 10;
    private static final String VERY_BEGINNING = "0001-01-01 00:00:00.000";
    private static final String DB_DATE_DEFAULT = "yyyy-MM-dd HH:mm:ss.SSS";
    private static final String DB_DATE_PATTERN_2 = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    private static final String DB_DATE_PATTERN_3 = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS+mm:ss";
    private static final String DB_DATE_PATTERN_4 = "yyyy-MM-dd'T'HH:mm:ss.SS";
    public static final String INIT_LOAD_DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSS";


    private static final String UI_DATE_PATTERN_WITH_YEAR = "d MMM',' yyyy 'at' h:mm a";
    private static final String UI_DATE_PATTERN_SIMPLE = "yyyy:MM:dd_HH:mm";
    private static final String DATE_PATTERN_SIMPLE = "d MMM',' yyyy";

    public static SimpleDateFormat DB_DATE_T = new SimpleDateFormat(DB_DATE_PATTERN_2);

    public static String currentTimeDb() {
        return dateTimeToString(DB_DATE_DEFAULT, DateTime.now());
    }

    public static String endOfRentalPeriod() {
        return dateTimeToString(DB_DATE_DEFAULT, DateTime.now().plusDays(RENTAL_PERIOD));
    }

    public static String currentMidnightDb() {
        return dateTimeToString(INIT_LOAD_DATE_PATTERN, DateTime.now().withTimeAtStartOfDay());
    }

    public static String fromTimeStamp(Long date) {
        return dateTimeToString(INIT_LOAD_DATE_PATTERN, new DateTime(date));
    }

    public static DateTime convertToDateTime(String dateString) {
        DateTime dateTime;
        try {
            dateTime = getDateTime(dateString);
        } catch (IllegalArgumentException e) {
            Timber.e(e);
            dateTime = stringToDateTime(DB_DATE_DEFAULT, VERY_BEGINNING);
        }
        return dateTime;
    }

    public static String convertDbDateToUI(DateTime date) {
        return dateTimeToString(UI_DATE_PATTERN_WITH_YEAR, date);
    }

    public static String currentTimeDbSimpleFormat() {
        return dateTimeToString(UI_DATE_PATTERN_SIMPLE, DateTime.now());
    }

    public static String convertDbDateToUI(String dateString) {
        DateTime date;
        try {
            date = getDateTime(dateString);
        } catch (IllegalArgumentException e) {
            Timber.e(e);
            return "Invalid date";
        }
        return dateTimeToString(UI_DATE_PATTERN_WITH_YEAR, date);
    }

    public static String toUIDate(String dateString) {
        DateTime date;
        try {
            date = getDateTime(dateString);
        } catch (IllegalArgumentException e) {
            Timber.e(e);
            return "Invalid date";
        }
        return dateTimeToString(DATE_PATTERN_SIMPLE, date);
    }

    public static DateTime getInitLoadDateTime(String dateString) {
        DateTime date = new DateTime();
        if (dateString != null) {
            try {
                date = stringToDateTime(INIT_LOAD_DATE_PATTERN, dateString);
            } catch (IllegalArgumentException e) {
                Timber.d(e);
                date = new DateTime();
            }
        }
        return date;
    }

    public static DateTime getDateTime(String dateString) {
        DateTime date = new DateTime();
        if (dateString != null) {
            try {
                date = stringToDateTime(INIT_LOAD_DATE_PATTERN, dateString);
            } catch (IllegalArgumentException e) {
                String pattern;
                if (!dateString.contains("T")) {
                    pattern = DateFormatter.DB_DATE_DEFAULT;
                } else if (dateString.contains("+")) {
                    pattern = DateFormatter.DB_DATE_PATTERN_3;
                } else {
                    pattern = DateFormatter.DB_DATE_PATTERN_2;
                }
                date = stringToDateTime(pattern, dateString);
            }
        }
        return date;
    }

    public static boolean compare(String date1, String date2) {
        return getDateTime(date1).isAfter(getDateTime(date2));
    }

    private static DateTime stringToDateTime(String pattern, String dateString) {
        return DateTimeFormat.forPattern(pattern).parseDateTime(dateString);
    }

    private static String dateTimeToString(String pattern, DateTime date) {
        return DateTimeFormat.forPattern(pattern).print(date);
    }
}
