package com.orange5.nsts.util.view.picker;

import android.view.View;

import com.orange5.nsts.util.UI;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class ValuePickerAdapter<T> {

    private Consumer<T> selectedItemConsumer = t -> {
    };
    private Consumer<Integer> positionConsumer = integer -> {};
    private final List<ItemWrapper> itemWrapperList;
    private Consumer<T> deferredSelectedConsumer;
    private View showKeyboardButton;


    public static class ItemWrapper<V> implements PickerView.PickerItem {
        private V value;
        private int position;

        public ItemWrapper(V value) {
            this.value = value;
        }

        @Override
        public String getText() {
            return value.toString();
        }

        @Override
        public void setPosition(int position) {
            this.position = position;
        }

        public V getValue() {
            return value;
        }
    }

    public static <X> ValuePickerAdapter withValues(List<X> items) {
        return new ValuePickerAdapter<>(items);
    }

    public ValuePickerAdapter(List<T> items) {
        itemWrapperList = new ArrayList<>(items.size());
        for (T item : items) {
            itemWrapperList.add(new ItemWrapper<T>(item));
        }
    }

    public ValuePickerAdapter<T> selectedItemConsumer(Consumer<T> selectedItemConsumer) {
        this.selectedItemConsumer = selectedItemConsumer;
        return this;
    }

    public ValuePickerAdapter<T> selectedPositionConsumer(Consumer<Integer> positionConsumer) {
        this.positionConsumer = positionConsumer;
        return this;
    }


    public ValuePickerAdapter<T> showKeyboardButton(View showKeyboardButton) {
        this.showKeyboardButton = showKeyboardButton;
        showKeyboardButton.setOnClickListener(UI::openKeyboard);
        return this;
    }

    public ValuePickerAdapter attachTo(PickerView pickerView) {
        pickerView.setItems(itemWrapperList, this::deferCallback);
        pickerView.invalidate();
        return this;
    }

    private void deferCallback(ItemWrapper<T> item) {
        selectedItemConsumer.accept(item.getValue());
        positionConsumer.accept(item.position);
//        handler.removeCallbacksAndMessages(null);
//        handler.postDelayed(() -> selectedItemConsumer.accept(value), 20);
    }
}
