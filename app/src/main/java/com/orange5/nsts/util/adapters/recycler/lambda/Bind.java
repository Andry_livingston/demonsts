package com.orange5.nsts.util.adapters.recycler.lambda;

import android.view.View;

public interface Bind<V extends View, M> {
    void bind(V view, M model);
}
