package com.orange5.nsts.util.view.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.res.Resources;

import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import android.view.View;

import com.orange5.nsts.ui.base.BaseActivity;

import butterknife.ButterKnife;

public abstract class BaseDialog {

    protected final Activity activity;
    protected AlertDialog dialog;
    protected View view;
    protected Resources resources;

    protected BaseDialog(BaseActivity activity) {
        this.activity = BaseActivity.getActive();
        resources = activity.getResources();
        view = View.inflate(activity, getLayoutRes(), null);
        ButterKnife.bind(this, view);
    }

    protected abstract @LayoutRes int getLayoutRes();

    public void dismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    public void show() {
        if (dialog != null) {
            if (!activity.isFinishing()) {
                dialog.show();
            }
        }
    }

    public boolean isShowing() {
        return dialog.isShowing();
    }
}
