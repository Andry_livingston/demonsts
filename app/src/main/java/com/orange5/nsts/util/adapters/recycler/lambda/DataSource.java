package com.orange5.nsts.util.adapters.recycler.lambda;

import java.util.List;

public interface DataSource<MODEL> {
    List<MODEL> getItems();
}
