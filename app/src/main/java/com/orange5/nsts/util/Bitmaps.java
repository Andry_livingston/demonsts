package com.orange5.nsts.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

public class Bitmaps {
    public static byte[] toJpegBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        bitmap.recycle();
        return stream.toByteArray();
    }

    public static Bitmap fromBytes(byte[] data) {
        Bitmap bitmap;
        if (data == null) {
            return null;
        }
        byte[] decoded;
        try {
            decoded = Base64.decode(data, Base64.DEFAULT);
        } catch (IllegalArgumentException e) {
            decoded = data;
        }
        bitmap = BitmapFactory.decodeByteArray(decoded, 0, decoded.length);
        return bitmap;
    }
}
