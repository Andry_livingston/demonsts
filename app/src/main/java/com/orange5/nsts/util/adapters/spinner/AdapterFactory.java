package com.orange5.nsts.util.adapters.spinner;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.orange5.nsts.R;
import com.orange5.nsts.util.RealTimeSearch;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.orange5.nsts.util.ViewFactory;
import com.orange5.nsts.util.adapters.recycler.RecyclerAdapterBuilder;

public class AdapterFactory {

    public static <MODEL> void createSearchableSpinnerAdapter(TextView textView,
                                                              List<MODEL> objects,
                                                              Supplier<MODEL> defaultSelectionSupplier,
                                                              Consumer<MODEL> selectedItemConsumer) {
        textView.setOnClickListener(new View.OnClickListener() {

            private EditText searchInput;
            private AlertDialog alertDialog;
            private Context context;
            private RecyclerView itemsList;

            @Override
            public void onClick(View v) {
                context = textView.getContext();
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                View contents = LayoutInflater.from(context).inflate(R.layout.searchable_dialog, null);
                searchInput = contents.findViewById(R.id.searchInput);
                itemsList = contents.findViewById(R.id.itemsList);

                RealTimeSearch.with(searchInput).searchHandler(s -> {
                    List<MODEL> filtered = objects.stream().filter(object -> object.toString().toLowerCase().contains(s.toLowerCase())).collect(Collectors.toList());
                    updateItems(filtered);
                });
                updateItems(objects);
                builder.setView(contents);
                alertDialog = builder.create();
                alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                alertDialog.show();
            }

            private void updateItems(List<MODEL> items) {
                MODEL defaultSelection = defaultSelectionSupplier.get();

                new RecyclerAdapterBuilder<TextView, MODEL>(itemsList)
                        .bind((view, model) -> view.setText(model.toString()))
                        .data(() -> items)
                        .viewFactory(() -> ViewFactory.createListItem(context, itemsList))
                        .onClick(android.R.id.text1, (position, item) -> {
                            textView.setText(item.toString());
                            selectedItemConsumer.accept(item);
                            textView.setTextColor(context.getColor(R.color.black));
                            InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(searchInput.getWindowToken(), 0);
                            alertDialog.dismiss();
                        })
                        .mutate((view, model, position) -> {
                            if (model.equals(defaultSelection)) {
                                view.setBackgroundResource(R.color.lightGray);
                            } else {
                                view.setBackground(null);
                            }
                        })
                        .vertical();
            }
        });
    }


    public static void createSpinnerAdapter(Spinner spinner, List objects, int hintResourceId, ItemSelectedListener listener) {
        List<Object> items = new ArrayList<>();
        spinner.setTag(R.id.zeroHint);
        items.addAll(objects);
        items.add(spinner.getContext().getString(hintResourceId));
        ArrayAdapter<Object> adapter = new ArrayAdapter<Object>(spinner.getContext(), android.R.layout.simple_spinner_dropdown_item, items) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(hintResourceId);
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }

        };

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        spinner.setSelection(adapter.getCount());

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == adapter.getCount()) {
                    listener.onItemSelected(null);
                    return;
                }
                listener.onItemSelected(objects.get(position));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                listener.onItemSelected(null);
            }
        });

    }


}
