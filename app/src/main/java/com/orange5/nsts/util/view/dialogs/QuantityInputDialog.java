package com.orange5.nsts.util.view.dialogs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.orange5.nsts.R;

import java.util.function.Consumer;

public class QuantityInputDialog {

    @SuppressLint("SetTextI18n")
    public static void show(Context context, Integer defaultQuantity, Consumer<Integer> selectedQuantityConsumer) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View contents = LayoutInflater.from(context).inflate(R.layout.quantity_select_dialog, null, false);


        EditText input = contents.findViewById(R.id.input);

        Runnable okRunnable = () -> {
            selectedQuantityConsumer.accept(input.getText().toString().length()>0
                    ? Integer.parseInt(input.getText().toString()) : 1);
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
        };


        input.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                okRunnable.run();
                return true;
            }
            return false;
        });
        input.setText(defaultQuantity.toString());
        builder.setView(contents);
        builder.setPositiveButton(R.string.common_ok, (dialog, which) -> {
            okRunnable.run();
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        alertDialog.show();
        input.requestFocus();
    }
}
