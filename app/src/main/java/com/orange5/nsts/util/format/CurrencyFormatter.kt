package com.orange5.nsts.util.format

import java.text.NumberFormat
import java.util.*

class CurrencyFormatter {

    companion object {
        @JvmStatic
        fun formatPrice(price: Double): String = NumberFormat.getCurrencyInstance(Locale.US).format(price)
    }
}