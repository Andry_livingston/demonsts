package com.orange5.nsts.util.adapters.recycler;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.util.List;

import com.orange5.nsts.util.adapters.recycler.lambda.Bind;
import com.orange5.nsts.util.adapters.recycler.lambda.Checked;
import com.orange5.nsts.util.adapters.recycler.lambda.Click;
import com.orange5.nsts.util.adapters.recycler.lambda.ClickEx;
import com.orange5.nsts.util.adapters.recycler.lambda.DataSource;
import com.orange5.nsts.util.adapters.recycler.lambda.LongClick;
import com.orange5.nsts.util.adapters.recycler.lambda.Mutator;
import com.orange5.nsts.util.adapters.recycler.lambda.ViewFactory;

public class RecyclerAdapterBuilder<VIEW extends View, MODEL> {


    private Bind<VIEW, MODEL> bind = (view, model) -> {
        throw new IllegalStateException("You must implement this method");
    };
    private ViewFactory<VIEW> viewFactory = () -> {
        throw new IllegalStateException("You must implement this method");
    };
    private DataSource<MODEL> dataSource = () -> {
        throw new IllegalStateException("You must implement this method");

    };
    private Click<MODEL> click = (position, item) -> {

    };

    private ClickEx<VIEW, MODEL> clickEx = (view, model, position) -> {

    };

    private SparseArray<Checked<MODEL>> checkedChange = new SparseArray<>();

    private SparseArray<Click<MODEL>> secondaryClick = new SparseArray<>();

    private SparseArray<LongClick<MODEL>> secondaryLongClick = new SparseArray<>();


    private LongClick<MODEL> longClick = (position, item) -> false;

    private Mutator<VIEW, MODEL> mutator = (view, model, position) -> {

    };
    private RecyclerView.Adapter<RecyclerView.ViewHolder> adapter;

    public RecyclerAdapterBuilder<VIEW, MODEL> viewFactory(ViewFactory<VIEW> viewFactory) {
        this.viewFactory = viewFactory;
        return this;
    }

    public RecyclerAdapterBuilder<VIEW, MODEL> data(DataSource<MODEL> dataSource) {
        this.dataSource = dataSource;
        return this;
    }

    public RecyclerAdapterBuilder<VIEW, MODEL> mutate(Mutator<VIEW, MODEL> mutator) {
        this.mutator = mutator;
        return this;
    }

    public RecyclerAdapterBuilder<VIEW, MODEL> onClick(Click<MODEL> click) {
        this.click = click;
        return this;
    }

    public RecyclerAdapterBuilder<VIEW, MODEL> onClick(int id, Click<MODEL> click) {
        this.secondaryClick.put(id, click);
        return this;
    }


    public RecyclerAdapterBuilder<VIEW, MODEL> onCheckedChange(int id, Checked<MODEL> checked) {
        this.checkedChange.put(id, checked);
        return this;
    }

    public RecyclerAdapterBuilder<VIEW, MODEL> onLongClick(int id, LongClick<MODEL> longClick) {
        this.secondaryLongClick.put(id, longClick);
        return this;
    }

    public RecyclerAdapterBuilder<VIEW, MODEL> onLongClick(LongClick<MODEL> longClick) {
        this.longClick = longClick;
        return this;
    }

    public RecyclerAdapterBuilder<VIEW, MODEL> bind(Bind<VIEW, MODEL> bind) {
        this.bind = bind;
        return this;
    }

    private RecyclerView recyclerView;


    public RecyclerAdapterBuilder(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    public RecyclerAdapterBuilder<VIEW, MODEL> vertical() {
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), LinearLayoutManager.VERTICAL, false));
        build();
        return this;
    }

    public RecyclerAdapterBuilder<VIEW, MODEL> custom(RecyclerView.LayoutManager layoutManager) {
        recyclerView.setLayoutManager(layoutManager);
        build();
        return this;
    }

    public RecyclerAdapterBuilder<VIEW, MODEL> horizontal() {
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), LinearLayoutManager.HORIZONTAL, false));
        build();
        return this;
    }

    public RecyclerAdapterBuilder<VIEW, MODEL> grid(int columnCount) {
        recyclerView.setLayoutManager(new GridLayoutManager(recyclerView.getContext(), columnCount));
        build();
        return this;
    }

    private List<MODEL> items;

    @SuppressWarnings("unchecked")
    private void build() {

        adapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {

            {
                items = dataSource.getItems();
            }

            public void reloadItems() {
                items = dataSource.getItems();
            }

            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                return new RecyclerView.ViewHolder(viewFactory.createView()) {

                    private final View.OnClickListener clickListener =
                            v -> click.onClick(getAdapterPosition(), items.get(getAdapterPosition()));
                    private final View.OnLongClickListener longClickListener =
                            v -> longClick.onLongClick(getAdapterPosition(), items.get(getAdapterPosition()));

                    {

                        itemView.post(new Runnable() {
                            @Override
                            public void run() {
                                //todo investigate why sometimes click only works on parent. dont use descendantFocusability
//                                ((ViewGroup) itemView).getChildAt(0).setOnClickListener(clickListener);
//                                ((ViewGroup) itemView).getChildAt(0).setOnLongClickListener(longClickListener);

                                for (int i = 0; i < checkedChange.size(); i++) {
                                    int id = checkedChange.keyAt(i);
                                    Checked<MODEL> checked = checkedChange.get(id);
                                    ((CompoundButton) itemView.findViewById(id))
                                            .setOnCheckedChangeListener((buttonView, isChecked) -> {
                                                int position = getAdapterPosition();
                                                checked.onCheckedChange(position, items.get(position), isChecked);
                                            });
                                }

                                for (int i = 0; i < secondaryClick.size(); i++) {
                                    int id = secondaryClick.keyAt(i);
                                    Click click = secondaryClick.get(id);
                                    itemView.findViewById(id).setOnClickListener(v -> {
                                        int position = getAdapterPosition();
                                        click.onClick(position, items.get(position));
                                    });
                                }

                                for (int i = 0; i < secondaryLongClick.size(); i++) {
                                    int id = secondaryLongClick.keyAt(i);
                                    LongClick click = secondaryLongClick.get(id);
                                    itemView.findViewById(id).setOnLongClickListener(v -> {
                                        int position = getAdapterPosition();
                                        return click.onLongClick(position, items.get(position));
                                    });
                                }
                            }
                        });

                    }

                };
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                MODEL model = items.get(position);
                VIEW itemView = (VIEW) holder.itemView;
                bind.bind(itemView, model);
                mutator.mutate(itemView, model, position);
            }

            @Override
            public int getItemCount() {
                return items.size();
            }
        };
        recyclerView.setAdapter(adapter);
    }

    public void setItems(List<MODEL> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public List<MODEL> getItems() {
        return items;
    }

    public void notifyDataSetChanged() {
        adapter.notifyDataSetChanged();
    }

    public void notifyItemRemoved(int position) {
        adapter.notifyItemRemoved(position);
    }
    public void notifyItemChanged(int position) {
        adapter.notifyItemChanged(position);
    }
}
