package com.orange5.nsts.util.adapters.spinner;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.orange5.nsts.R;
import com.orange5.nsts.util.RealTimeSearch;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.orange5.nsts.util.ViewFactory;
import com.orange5.nsts.util.adapters.recycler.RecyclerAdapterBuilder;

public class SearchableSpinner<MODEL> implements Consumer<MODEL> {
    private final TextView textView;
    private EditText searchInput;
    private AlertDialog alertDialog;
    private Context context;
    private final List<MODEL> objects;
    private final Supplier<MODEL> defaultSelectionSupplier;
    private final Consumer<MODEL> selectedItemConsumer;
    private RecyclerView itemsList;

    private SearchableSpinner(TextView textView,
                              List<MODEL> objects,
                              Supplier<MODEL> defaultSelectionSupplier,
                              Consumer<MODEL> selectedItemConsumer) {

        this.context = textView.getContext();
        this.objects = objects;
        this.defaultSelectionSupplier = defaultSelectionSupplier;
        this.selectedItemConsumer = selectedItemConsumer;
        this.textView = textView;

        textView.setOnClickListener(v -> {
            show();
        });
    }

    public static <MODEL> SearchableSpinner<MODEL> createForTextView(TextView textView,
                                                                     List<MODEL> objects,
                                                                     Supplier<MODEL> defaultSelectionSupplier,
                                                                     Consumer<MODEL> selectedItemConsumer) {
        return new SearchableSpinner<>(textView, objects, defaultSelectionSupplier, selectedItemConsumer);
    }

    private void show() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View contents = LayoutInflater.from(context).inflate(R.layout.searchable_dialog, null);
        searchInput = contents.findViewById(R.id.searchInput);
        itemsList = contents.findViewById(R.id.itemsList);

        RealTimeSearch.with(searchInput).searchHandler(s -> {
            List<MODEL> filtered = objects.stream().filter(object -> object.toString().toLowerCase().contains(s.toLowerCase())).collect(Collectors.toList());
            updateItems(filtered);
        });
        updateItems(objects);
        builder.setView(contents);
        alertDialog = builder.create();
        alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        alertDialog.show();
    }

    private void updateItems(List<MODEL> items) {
        MODEL defaultSelection = defaultSelectionSupplier.get();

        new RecyclerAdapterBuilder<TextView, MODEL>(itemsList)
                .bind((view, model) -> view.setText(model.toString()))
                .data(() -> items)
                .viewFactory(() -> ViewFactory.createListItem(context, itemsList))
                .onClick(android.R.id.text1, (position, item) -> {
                    selectItem(item);
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(searchInput.getWindowToken(), 0);
                    alertDialog.dismiss();
                })
                .mutate((view, model, position) -> {
                    if (model.equals(defaultSelection)) {
                        view.setBackgroundResource(R.color.lightGray);
                    } else {
                        view.setBackground(null);
                    }
                })
                .vertical();
    }

    public void selectItem(MODEL item) {
        accept(item);
    }

    @Override
    public void accept(MODEL item) {
        textView.setText(item.toString());
        selectedItemConsumer.accept(item);
        textView.setTextColor(context.getColor(R.color.black));
    }
}
