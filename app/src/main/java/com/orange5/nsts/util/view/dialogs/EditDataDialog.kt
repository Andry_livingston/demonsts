package com.orange5.nsts.util.view.dialogs

import android.app.AlertDialog
import android.content.Context
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.annotation.StringRes
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseActivity
import com.orange5.nsts.ui.extensions.hideKeyboard


class EditDataDialog(params: Params,
                     actionText: (String) -> Unit = {},
                     actionNumber: (Int) -> Unit = {},
                     activity: BaseActivity) : BaseDialog(activity) {
    override fun getLayoutRes(): Int = R.layout.dialog_edit_data

    private val title: TextView = view.findViewById(R.id.title)
    private val input: TextView = view.findViewById(R.id.input)
    private val actionButton: TextView = view.findViewById(R.id.actionButton)

    init {
        dialog = AlertDialog.Builder(activity)
                .setView(view)
                .create()

        title.text = resources.getString(params.titleRes)
        input.inputType = params.type
        input.text = params.actualValue
        input.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) dialog.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
        input.requestFocus()
        actionButton.setOnClickListener {
            (activity as Context).hideKeyboard(view)
            actionText(input.text.toString())
            actionNumber(input.text.toString().toIntOrNull() ?: 1)
            dismiss()
        }
    }



    data class Params(@StringRes val titleRes: Int, val type: Int, val actualValue: String)

    companion object {
        fun editNote(note: String, activity: BaseActivity, actionText: (String) -> Unit) =
                EditDataDialog(
                        Params(R.string.common_enter_comment, EditorInfo.TYPE_CLASS_TEXT, note),
                        actionText = actionText, activity = activity).apply { show() }

        fun editNumber(quantity: Int, activity: BaseActivity, actionNumber: (Int) -> Unit) =
                EditDataDialog(
                        Params(R.string.common_enter_quantity, EditorInfo.TYPE_CLASS_NUMBER, quantity.toString()),
                        actionNumber = actionNumber, activity = activity).apply { show() }
    }
}