package com.orange5.nsts.util;

import android.graphics.Color;

import com.orange5.nsts.R;
import com.orange5.nsts.app.App;

public class Colors {

    private static final App context = App.getInstance();

    public static final int HIGHLIGHT = Color.parseColor("#faebae");
    public static final int HIGHLIGHT_BLUE = context.getColor(R.color.blue_select_text);

    public static final int GREEN = context.getColor(R.color.okGreen);
    public static final int LIGHT_GREEN = context.getColor(R.color.green_highlight);
    public static final int RED = context.getColor(R.color.errorRed);
    public static final int ORANGE = context.getColor(R.color.orange);
    public static final int GRAY = context.getColor(R.color.lightGray);

}
