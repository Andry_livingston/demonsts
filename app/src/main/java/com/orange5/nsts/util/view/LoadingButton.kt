package com.orange5.nsts.util.view

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.orange5.nsts.R
import kotlinx.android.synthetic.main.view_loading_button.view.*

class LoadingButton @JvmOverloads constructor(
        context: Context, attrsSet: AttributeSet? = null, defStyleAttr: Int = 0, defStyleRes: Int = 0
) : FrameLayout(context, attrsSet, defStyleAttr, defStyleRes) {

    init {
        inflate(context, R.layout.view_loading_button, this)
        applyAttributes(context, attrsSet, defStyleAttr, defStyleRes)
    }

    private var loadingText: String? = null
    private var defaultText: String? = null

    fun setText(text: String?) {
        if (!text.isNullOrEmpty()) {
            defaultText = text
            button.text = text
        }
    }

    fun setButtonBackground(@DrawableRes background: Int) {
        setButtonBackground(ContextCompat.getDrawable(context, background))
    }

    fun setTextColor(@ColorRes color: Int) {
        button.setTextColor(getColor(color))
    }

    fun setIndeterminateTint(tint: ColorStateList) {
        progress.indeterminateTintList = tint
    }

    fun setLoading(loading: Boolean) {
        if (loading && !loadingText.isNullOrEmpty()) button.text = loadingText
        else button.text = defaultText ?: ""
        progress.visibility = if (loading) VISIBLE else GONE
        button.isEnabled = loading.not()
    }

    fun setOnClickListener(action: (v: View) -> Unit) {
        button.setOnClickListener {
            action(it)
        }
    }

    fun setOnClickListener(action: Runnable) {
        button.setOnClickListener {
            action.run()
        }
    }

    private fun setButtonBackground(background: Drawable?) {
        if (background != null) {
            button.background = background
        }
    }

    private fun applyAttributes(context: Context, attrsSet: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) {
        context.theme.obtainStyledAttributes(attrsSet, R.styleable.LoadingButton, defStyleAttr,
                defStyleRes).also { attr ->
            setText(attr.getString(R.styleable.LoadingButton_android_text))
            setButtonBackground(attr.getDrawable(R.styleable.LoadingButton_android_background))
            setIndeterminateTint(attr.getColorStateList(R.styleable.LoadingButton_android_indeterminateTint)
                    ?: ColorStateList.valueOf(Color.WHITE))
            button.setTextColor(attr.getColor(R.styleable.LoadingButton_android_textColor, getColor(android.R.color.white)))
            loadingText = attr.getString(R.styleable.LoadingButton_loadingText) ?: ""
            attr.recycle()
        }
    }

    private fun getColor(@ColorRes color: Int) = ContextCompat.getColor(context, color)
}
