package com.orange5.nsts.util;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class StringUtils {

    public static boolean isEmpty(CharSequence str) {
        return str == null || str.length() == 0;
    }

    public static boolean isNotEmpty(CharSequence str) {
        return !isEmpty(str);
    }

    public static boolean containsIgnoreCase(String input, String match) {
        return input.matches("(?i).*" + Pattern.quote(match) + ".*");
    }

    public static boolean containsIgnoreCase2(String input, String match) {
        Pattern p = Pattern.compile(match.toUpperCase(), Pattern.LITERAL);
        Matcher m = p.matcher(input.toUpperCase());
        return m.find();
    }

    public static String arrayToString(String[] array) {
        return Arrays.toString(array)
                .replace("[", "")
                .replace("]", "");
    }

    public static String capitalize(String original) {
        if (original == null || original.length() == 0) {
            return original;
        }
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }

}
