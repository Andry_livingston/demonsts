package com.orange5.nsts.util;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CollectionUtils {
    public static List<Integer> rangeClosedList(int start, int end) {
        return IntStream.rangeClosed(start, end).boxed().collect(Collectors.toList());
    }

    public static <T> List<T> safe(List<T> collections) {
        return collections != null ? collections : Collections.emptyList();
    }

    public static <K, V> Map<K, V> safe(Map<K, V> collections) {
        return collections != null ? collections : Collections.emptyMap();
    }

    public static boolean isEmpty(List collections) {
        return collections == null || collections.isEmpty();
    }

    public static boolean isNotEmpty(List collections) {
        return !isEmpty(collections);
    }

    public static boolean isEmpty(Map collections) {
        return collections == null || collections.isEmpty();
    }

}
