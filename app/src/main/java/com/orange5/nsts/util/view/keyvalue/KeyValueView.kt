package com.orange5.nsts.util.view.keyvalue

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.orange5.nsts.R
import kotlinx.android.synthetic.main.view_loading_button.view.*
import kotlinx.android.synthetic.main.view_key_value.view.*

class KeyValueView @JvmOverloads constructor(
        context: Context, attrsSet: AttributeSet? = null, defStyleAttr: Int = 0, defStyleRes: Int = 0
) : LinearLayout(context, attrsSet, defStyleAttr, defStyleRes) {

    init {
        inflate(context, R.layout.view_key_value, this)
        layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT).apply {
            setPadding(resources.getDimensionPixelSize(R.dimen.common_8dp),
                    resources.getDimensionPixelSize(R.dimen.common_8dp),
                    resources.getDimensionPixelSize(R.dimen.common_8dp),
                    0)
            setMargins(resources.getDimensionPixelSize(R.dimen.common_1dp),
                    resources.getDimensionPixelSize(R.dimen.common_1dp),
                    resources.getDimensionPixelSize(R.dimen.common_1dp),
                    resources.getDimensionPixelSize(R.dimen.common_1dp))
        }
        orientation = VERTICAL
        setBackgroundColor(getColor(R.color.lighterGray))
        applyAttributes(context, attrsSet, defStyleAttr, defStyleRes)
    }

    fun getValueText() =  value.text.toString()

    fun setKeyText(text: String?) {
        if (!text.isNullOrEmpty()) {
            key.text = text
        }
    }

    fun setValueText(text: String?) {
        if (!text.isNullOrEmpty()) {
            value.text = text
        }
    }

    fun setKeyColor(@ColorInt color: Int) {
        key.setTextColor(color)
    }

    fun setValueColor(@ColorInt color: Int) {
        value.setTextColor(color)
    }

    fun setOnClickListener(action: (v: View) -> Unit) {
        button.setOnClickListener {
            action(it)
        }
    }

    private fun applyAttributes(context: Context, attrsSet: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) {
        context.theme.obtainStyledAttributes(attrsSet, R.styleable.KeyValueView, defStyleAttr,
                defStyleRes).also { attr ->
            setKeyText(attr.getString(R.styleable.KeyValueView_keyText))
            setValueText(attr.getString(R.styleable.KeyValueView_valueText))
            setKeyColor(attr.getColor(R.styleable.KeyValueView_keyColor, Color.BLACK))
            setValueColor(attr.getColor(R.styleable.KeyValueView_valueColor, Color.BLACK))
            attr.recycle()
        }
    }

    private fun getColor(@ColorRes color: Int) = ContextCompat.getColor(context, color)
}
