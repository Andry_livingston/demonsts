package com.orange5.nsts.util.sql;

import android.Manifest;
import android.os.Environment;
import androidx.core.app.ActivityCompat;

import com.orange5.nsts.app.App;

import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.List;

import static com.orange5.nsts.di.modules.DbModule.DB_NAME;


public class SQLDebugger {
    public static boolean ENABLED = false;

    static {
        init();
    /*    Settings.Values.EXPORT_DB_FILE.registerHandler(aBoolean -> {
            //  ENABLED = Setting.Values.EXPORT_DB_FILE.get();
            init();
        });*/
    }

    private static void init() {
        if (ENABLED) {
            ActivityCompat.requestPermissions(App.getInstance()
                            .getAppLifecycleListener().getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1);
        }
    }

    public static <Entity> Query<Entity> build(QueryBuilder<Entity> queryBuilder) {
        if (!ENABLED) {
            return queryBuilder.build();
        }
        Query<Entity> build = queryBuilder.build();
        LoggedQueries.logQuery(build);
        return build;
    }

    public static <Entity> List<Entity> list(QueryBuilder<Entity> queryBuilder) {
        return build(queryBuilder).list();
    }

    public static String dumpDB() {
        File sd = Environment.getExternalStorageDirectory();
        File data = Environment.getDataDirectory();
        FileChannel source = null;
        FileChannel destination = null;
        String currentDBPath = "/data/" + App.getInstance().getPackageName()
                + "/databases/" + DB_NAME;
        File currentDB = new File(data, currentDBPath);
        File backupDB = new File(sd, DB_NAME);
        try {
            source = new FileInputStream(currentDB).getChannel();
            destination = new FileOutputStream(backupDB).getChannel();
            destination.transferFrom(source, 0, source.size());
            source.close();
            destination.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return backupDB.getAbsolutePath();
    }

}
