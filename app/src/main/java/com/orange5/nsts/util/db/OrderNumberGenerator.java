package com.orange5.nsts.util.db;

import com.orange5.nsts.data.db.entities.Order;

import java.util.Locale;

public final class OrderNumberGenerator {
    private static final int INCREMENTAL_LENGTH = 7;

    public static String getNext(Order lastOrder, String orderPrefix) {
        String orderNumber;
        if (lastOrder == null) {
            orderNumber = createOrderNumber(1, orderPrefix);
        } else {
            orderNumber = lastOrder.getOrderNumber();
            try {
                int number = Integer.parseInt(orderNumber.substring(orderPrefix.length()));
                number++;
                orderNumber = createOrderNumber(number, orderPrefix);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return orderNumber;
    }

    private static String createOrderNumber(int number, String orderPrefix) {
        return String.format(Locale.US, "%s%0" + INCREMENTAL_LENGTH + "d", orderPrefix, number);
    }

}
