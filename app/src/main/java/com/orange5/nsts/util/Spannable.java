package com.orange5.nsts.util;

import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;

import com.orange5.nsts.util.Colors;

import static android.text.TextUtils.isEmpty;

public class Spannable {
    public static SpannableString highlightBackground(String destination, String highlight) {
        SpannableString str = new SpannableString(destination);

        int index = destination.toLowerCase().indexOf(highlight.toLowerCase());

        if (highlight.isEmpty() || index == -1) {
            return str;
        }
        str.setSpan(new BackgroundColorSpan(Colors.HIGHLIGHT),
                index, index + highlight.length(), 0);
        return str;
    }

    public static SpannableString highlightTextColor(String destination, int color, String... highlights) {
        SpannableString str = new SpannableString(destination);
        for (String highlight : highlights) {
            if(isEmpty(highlight)) continue;
            int index = destination.toLowerCase().indexOf(highlight.toLowerCase());

            if (highlight.isEmpty() || index == -1) {
                continue;
            }
            str.setSpan(new ForegroundColorSpan(color),
                    index, index + highlight.length(), 0);
        }
        return str;
    }

    public static SpannableString highlightTextColor(SpannableString destination, int color, String... highlights) {
        String string = destination.toString().toLowerCase();

        for (String highlight : highlights) {
            int index = string.indexOf(highlight.toLowerCase());

            if (highlight.isEmpty() || index == -1) {
                continue;
            }
            destination.setSpan(new ForegroundColorSpan(color),
                    index, index + highlight.length(), 0);
        }
        return destination;
    }
}
