package com.orange5.nsts.util.adapters.recycler.lambda;

import android.view.View;

public interface ViewFactory<V extends View> {
    V createView();

}
