package com.orange5.nsts.util.adapters.recycler.lambda;

public interface Checked<MODEL> {
    void onCheckedChange(int position, MODEL item, boolean isChecked);
}
