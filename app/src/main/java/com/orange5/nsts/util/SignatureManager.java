package com.orange5.nsts.util;

import android.graphics.Bitmap;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

public class SignatureManager {

    public static Bitmap getConfigSizeBitmap(Bitmap signatureBitmap) {
        return Bitmap.createScaledBitmap(signatureBitmap, 300, 100, false);
    }

    static public byte[] configSignBytesWithDpi(Bitmap signatureBitmap, int dpi) {
        return setDpi(Bitmaps.toJpegBytes(signatureBitmap), dpi);
    }

    private static byte[] setDpi(byte[] imageData, int dpi) {
        imageData[13] = 1;
        imageData[14] = (byte) (dpi >> 8);
        imageData[15] = (byte) (dpi & 0xff);
        imageData[16] = (byte) (dpi >> 8);
        imageData[17] = (byte) (dpi & 0xff);
        return imageData;
    }

    public static Bitmap convertByteToBitmap(byte[] signImage) {
        return Bitmaps.fromBytes(signImage);
    }

    public static byte[] bitmapToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public static String bytesToBase64(byte[] bytes) {
       String encoded =  Base64.encodeToString(bytes, Base64.DEFAULT);
       return encoded.replaceAll("\n", "");
    }
}
