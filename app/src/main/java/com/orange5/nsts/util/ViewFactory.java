package com.orange5.nsts.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

public class ViewFactory {
    public static TextView createListItem(Context context, ViewGroup parent) {
        return (TextView) LayoutInflater.from(context).inflate(android.R.layout.simple_list_item_1, parent, false);
    }
}
