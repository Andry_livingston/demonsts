package com.orange5.nsts.util.view.keyvalue;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Pair;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.orange5.nsts.R;

public class KeyValueRowView extends FrameLayout {
    private TextView key, value;

    public KeyValueRowView(Context context) {
        super(context);
        init();
    }

    public KeyValueRowView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public KeyValueRowView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public KeyValueRowView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.view_key_value_detail, this);
        key = findViewById(R.id.key);
        value = findViewById(R.id.value);
    }

    public void fill(Pair<String, String> entry) {
        key.setText(entry.first);
        value.setText(entry.second);
    }
}
