package com.orange5.nsts.util.migration;

import com.orange5.nsts.util.migration.strategies.DefaultMigrationStrategy;
import com.orange5.nsts.util.migration.strategies.Dummy2to3MigrationStrategy;

import java.util.HashMap;
import java.util.Map;

import timber.log.Timber;

public class MigrationStrategyFactory {

    private static Map<Version, Class<? extends MigrationStrategy>> definedStrategies = new HashMap<>();

    static {
        definedStrategies.put(Version.of(1, 2), DefaultMigrationStrategy.class);
        definedStrategies.put(Version.of(2, 3), Dummy2to3MigrationStrategy.class);
    }

    public static MigrationStrategy getMigrationStrategy(int oldVersion, int newVersion) {

        Version version = Version.of(oldVersion, newVersion);
        Class<? extends MigrationStrategy> result = definedStrategies.get(version);
        if (result != null) {
            return createInstance(result, version);
        }
        return new DefaultMigrationStrategy();
    }


    private static MigrationStrategy createInstance(Class<? extends MigrationStrategy> result,
                                                    Version version) {
        try {
            result.newInstance();
        } catch (Exception e) {
            Timber.e("Unable to get migration strategy for " + version
                    + " (see exception)");
            Timber.e(e);
        }
        return new DefaultMigrationStrategy();
    }
}
