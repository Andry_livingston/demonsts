package com.orange5.nsts.util.view.keyvalue;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.orange5.nsts.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class KeyValueComplexView extends FrameLayout {

    @BindView(R.id.key)
    TextView title;
    @BindView(R.id.total)
    TextView total;
    @BindView(R.id.picked)
    TextView picked;

    public KeyValueComplexView(Context context) {
        super(context);
        init();
    }

    public KeyValueComplexView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public KeyValueComplexView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public KeyValueComplexView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.view_key_value_complex, this);
        ButterKnife.bind(this, this);
    }

    public void fill(PickedDetails details) {
        title.setText(details.title);
        total.setText(String.valueOf(details.total));
        picked.setText(details.unpicked > 0
                ? getContext().getString(R.string.pick_up_product_pickup_info, details.picked, details.total)
                : getContext().getString(R.string.pick_up_product_pickup_all_info, details.picked));
    }

    public static class PickedDetails {
        private String title;
        private int total;
        private int picked;
        private int unpicked;

        public PickedDetails(String title, int total, int picked, int unpicked) {
            this.title = title;
            this.total = total;
            this.picked = picked;
            this.unpicked = unpicked;
        }

        public int getTotal() {
            return total;
        }
    }

}
