package com.orange5.nsts.util;

import android.util.SparseArray;

import com.orange5.nsts.data.db.base.DatabaseEntity;
import com.orange5.nsts.data.db.entities.Order;
import com.orange5.nsts.data.db.entities.RentalReturn;

import org.joda.time.DateTime;

import java.util.List;

public class DateSections {

    public static SparseArray<String> generateSections(List<? extends DatabaseEntity> entities,
                                                       boolean isReturn,
                                                       Integer closed, Integer active) {
        SparseArray<String> result = new SparseArray<>();

        int todayIndex = -1;
        int yesterdayIndex = -1;
        int currentWeekIndex = -1;
        int lastWeekIndex = -1;
        int lastYearIndex = -1;
        int olderIndex = -1;
        int closeOrProcessedIndex = -1;

        DateTime startingPointOfToday = DateTime.now().withZone(DateTime.now().getZone()).withTimeAtStartOfDay();
        DateTime startingPointOfYesterday = startingPointOfToday.minusDays(1).withTimeAtStartOfDay();
        DateTime startingPointOfCurrentWeek = startingPointOfToday.minusDays(getStartWeek(startingPointOfToday)).withTimeAtStartOfDay();
        DateTime startingPointOfLastWeekAgo = startingPointOfCurrentWeek.minusWeeks(1).withTimeAtStartOfDay();
        DateTime startingPointOfOneYearAgo = startingPointOfToday.minusYears(1).withTimeAtStartOfDay();

        result = configSection(isReturn, startingPointOfToday, todayIndex,
                startingPointOfYesterday, yesterdayIndex,
                startingPointOfCurrentWeek, currentWeekIndex,
                startingPointOfLastWeekAgo, lastWeekIndex,
                startingPointOfOneYearAgo, lastYearIndex,
                olderIndex, closeOrProcessedIndex, result,
                entities, closed == null ? 0 : closed, active == null ? 0 : active);
        return result;
    }

    private static int getStartWeek(DateTime startingPointOfToday) {
        return startingPointOfToday.getDayOfWeek() > 1
                ? startingPointOfToday.getDayOfWeek() - 1
                : startingPointOfToday.getDayOfWeek();
    }

    private static SparseArray<String> configSection(boolean isReturn,
                                                     DateTime midnightToday, int todayIndex,
                                                     DateTime midnightYesterday, int yesterdayIndex,
                                                     DateTime midnightStartedCurrentWeek, int currentWeekIndex,
                                                     DateTime midnightOneWeekAgo, int lastWeekIndex,
                                                     DateTime midnightOneYearAgo, int lastYearIndex,
                                                     int olderIndex, int closedOrProcessedIndex,
                                                     SparseArray<String> result,
                                                     List<? extends DatabaseEntity> entities,
                                                     int closedStatus,
                                                     int activeStatus) {

        for (int i = 0; i < entities.size(); i++) {
            DateTime orderDate = entities.get(i).getDateTime();

            boolean isClosedOrProcessed = isReturn
                    ? ((RentalReturn)entities.get(i)).getCompleteDate() != null
            : isClosedOrder(entities.get(i),closedStatus, activeStatus);

            if (orderDate.isAfter(midnightToday) && !isClosedOrProcessed) {
                if (todayIndex == -1) {
                    todayIndex = i;
                }
                continue;
            }

            if (orderDate.isAfter(midnightYesterday) && !isClosedOrProcessed) {
                if (yesterdayIndex == -1) {
                    yesterdayIndex = i;
                }
                continue;
            }

            if (orderDate.isAfter(midnightStartedCurrentWeek)
                    && orderDate.isAfter(midnightYesterday)
                    && !isClosedOrProcessed) {
                if (currentWeekIndex == -1) {
                    currentWeekIndex = i;
                }
                continue;
            }

            if (orderDate.isAfter(midnightOneWeekAgo) && !isClosedOrProcessed) {
                if (lastWeekIndex == -1) {
                    lastWeekIndex = i;
                }
                continue;
            }

            if (orderDate.isAfter(midnightOneYearAgo) && !orderDate.isAfter(midnightOneWeekAgo)
                    && !isClosedOrProcessed) {
                if (lastYearIndex == -1) {
                    lastYearIndex = i;
                }
            } else {
                if (isClosedOrProcessed) {
                    if (closedOrProcessedIndex == -1) {
                        closedOrProcessedIndex = i;
                        break;
                    }
                } else {
                    olderIndex = i;
                }
            }

        }

        return configDateIndexParams(todayIndex, yesterdayIndex, currentWeekIndex,
                lastWeekIndex, lastYearIndex, olderIndex,
                closedOrProcessedIndex, result, !isReturn ? "Closed" : "Processed");

    }

    private static boolean isClosedOrder(DatabaseEntity databaseEntity, int closedStatus, int activeStatus) {
        return ((Order) databaseEntity).getOrderStatusId() == closedStatus
                || ((Order) databaseEntity).getOrderStatusId() == activeStatus;
    }

    private static SparseArray<String> configDateIndexParams(int todayIndex, int yesterdayIndex,
                                                             int currentWeekIndex,
                                                             int lastWeekIndex, int lastYearIndex,
                                                             int olderIndex, int closeIndex,
                                                             SparseArray<String> result,
                                                             String closedOrProcessed) {
        result.put(todayIndex, "Today");
        result.put(yesterdayIndex, "Yesterday");
        result.put(currentWeekIndex, "Current week");
        result.put(lastWeekIndex, "Last week");
        result.put(lastYearIndex, "Last year");
        result.put(olderIndex, "Older");
        result.put(closeIndex, closedOrProcessed);
        return result;
    }
}
