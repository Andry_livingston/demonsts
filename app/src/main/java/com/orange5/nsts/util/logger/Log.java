package com.orange5.nsts.util.logger;

import androidx.annotation.NonNull;

import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import timber.log.Timber;

public class Log {
    public static boolean ENABLED = true;
    private static Map<String, Long> benchmarkTimes = new ConcurrentHashMap<>();
    private static Map<String, Long> benchmarkAverageTimes = new ConcurrentHashMap<>();

    @NonNull
    private static String getTag() {
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[5];
        return stackTraceElement.getClassName() + "@" + stackTraceElement.getMethodName() + "() ";
    }

    public static void e(String message, Object... formatArgs) {
        if (!ENABLED) {
            return;
        }
        Timber.e(message, formatArgs);
    }

    public static void v(String message) {
        if (!ENABLED) {
            return;
        }
        Timber.v(getTag(), message);
    }

    public static void v(String message, Object... formatArgs) {
        if (!ENABLED) {
            return;
        }
        v(String.format(message, formatArgs));
    }

    public static void startBenchmark(String name) {
        benchmarkTimes.put(name, System.currentTimeMillis());
        e(String.format(Locale.getDefault(), "Benchmark %s started", name));
    }

    public static void endBenchmark(String name) {
        long duration = System.currentTimeMillis() - benchmarkTimes.get(name);
        benchmarkTimes.remove(name);
        Long average = benchmarkAverageTimes.get(name);
        if (average == null) {
            benchmarkAverageTimes.put(name, average = duration);
        } else {
            benchmarkAverageTimes.put(name, average = (average + duration) / 2);
        }
        e(String.format(Locale.getDefault(), "Benchmark %s finished. Duration: %d ms. Avg: %d", name, duration, average));
    }
}
