package com.orange5.nsts.util.adapters.recycler.lambda;

public interface Click<M> {
    void onClick(int position, M item);
}
