package com.orange5.nsts.util.migration;

import org.greenrobot.greendao.database.Database;

public interface MigrationStrategy {
    void migrate(Database database);
}
