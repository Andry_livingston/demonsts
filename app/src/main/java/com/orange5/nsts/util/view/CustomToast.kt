package com.orange5.nsts.util.view

import android.app.Activity
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.orange5.nsts.R

object CustomToast {

    @JvmStatic
    fun show(activity: Activity, errorMessage: String): Toast {
        activity.apply {
            val toast = Toast(this)
            val inflater = layoutInflater
            val toastView = inflater.inflate(R.layout.layout_custom_toast, findViewById<LinearLayout>(R.id.custom_toast_container))
            toastView.findViewById<TextView>(R.id.text).text = errorMessage
            with(toast) {
                setGravity(Gravity.CENTER, 0, 0)
                duration = Toast.LENGTH_SHORT
                view = toastView
                show()
            }
            return toast
        }
    }
}