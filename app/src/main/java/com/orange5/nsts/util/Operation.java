package com.orange5.nsts.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Operation {
    private BigDecimal currentValue;
    private final RoundingMode ROUNDING = RoundingMode.HALF_UP;

    private int intermediateScale = 2;
    private int finalScale = 2;

    private final BigDecimal _100 = new BigDecimal(100).setScale(finalScale, ROUNDING);


    public static Operation with(Object value) {
        return new Operation(value);
    }

    public static Operation start() {
        return new Operation(0);
    }

    private Operation(Object value) {
        this.currentValue = getValidValue(value);
    }

    public Operation add(Object value) {
        currentValue = currentValue.add(getValidValue(value));
        return this;
    }

    public Operation subtract(Object value) {
        currentValue = currentValue.subtract(getValidValue(value));
        return this;
    }

    public Operation multiply(Object value) {
        currentValue = currentValue.multiply(getValidValue(value)).setScale(intermediateScale, ROUNDING);
        return this;
    }

    public Operation divide(Object value) {
        currentValue = currentValue.divide(getValidValue(value), intermediateScale, ROUNDING);
        return this;
    }

    public Operation percentage(Object percent) {
        currentValue = currentValue
                .multiply(getValidValue(percent)).setScale(intermediateScale, ROUNDING)
                .divide(_100, intermediateScale,
                        ROUNDING);
        return this;
    }

    public Operation subtractPercentage(Object percent) {
        currentValue = currentValue.subtract(percentage(percent).getCurrentValue());
        return this;
    }

    public Operation addPercentage(Object percent) {
        currentValue = currentValue.add(percentage(percent).getCurrentValue());
        return this;
    }

    public Double toDouble() {
        return currentValue.doubleValue();
    }

    public BigDecimal getCurrentValue() {
        return currentValue;
    }

    public String getFinalValue() {
        return currentValue.setScale(finalScale, ROUNDING).toString();
    }

    @Override
    public String toString() {
        return currentValue.toString();
    }

    private BigDecimal getValidValue(Object value) {
        if (value instanceof BigDecimal) {
            BigDecimal result = (BigDecimal) value;
            return result.setScale(intermediateScale, ROUNDING);
        }
        try {
            Double.parseDouble(value.toString());
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid parameter: " + value.toString());
        }
        return new BigDecimal(value.toString()).setScale(intermediateScale, ROUNDING);
    }

    public int toInteger() {
        return currentValue.intValue();
    }
}
