package com.orange5.nsts.util;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.util.function.Consumer;

public class RealTimeSearch {
    private final EditText editText;
    private long delay = 50;
    private int minimumLength;

    private RealTimeSearch(EditText editText) {
        this.editText = editText;
    }

    public static RealTimeSearch with(EditText editText) {
        return new RealTimeSearch(editText);
    }

    public RealTimeSearch setDelay(long delay) {
        this.delay = delay;
        return this;
    }

    public RealTimeSearch searchHandler(Consumer<String> textConsumer) {
        editText.addTextChangedListener(new TextWatcher() {
            Handler handler = new Handler();
            private String text = "";
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    text = editText.getText().toString();
                    textConsumer.accept(text);
                }
            };

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                text = editText.getText().toString();
                handler.removeCallbacksAndMessages(runnable);
                handler.postDelayed(runnable, delay);
            }
        });
        return this;
    }
}
