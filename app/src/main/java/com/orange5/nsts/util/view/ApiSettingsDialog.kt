package com.orange5.nsts.util.view

import android.app.AlertDialog
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.StringRes
import com.orange5.nsts.R
import com.orange5.nsts.ui.base.BaseActivity
import com.orange5.nsts.util.view.dialogs.BaseDialog
import java.util.function.Consumer
import java.util.regex.Pattern

class ApiSettingsDialog(activity: BaseActivity,
                        private val isAlert: Boolean,
                        private val oldIp: String,
                        private var ipListener: Consumer<String>) : BaseDialog(activity) {

    override fun getLayoutRes(): Int = R.layout.dialog_request_ip

    private val messageField: TextView = view.findViewById(R.id.message)
    private val negative: TextView = view.findViewById(R.id.negative)
    private val positive: TextView = view.findViewById(R.id.positive)
    private val submit: TextView = view.findViewById(R.id.submit)
    private val ipInputField: EditText = view.findViewById(R.id.ip)
    private val ipContainer: LinearLayout = view.findViewById(R.id.ipContainer)
    private val standardControls: LinearLayout = view.findViewById(R.id.standardControls)

    init {
        dialog = AlertDialog.Builder(activity)
                .setView(view)
                .create()
        fill()
        negative.setOnClickListener { dismiss() }
        positive.setOnClickListener {
            saveIp()
        }
        ipInputField.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) = Unit

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                ipInputField.error = null
            }
        })
        submit.setOnClickListener {
            if (isIpValid()) {
                if (!isIpFirstAdding() && isIpChanged()) {
                    standardControls.visibility = View.VISIBLE
                    ipContainer.visibility = View.GONE
                    messageField.text = activity.getString(R.string.api_settings_dialog_message_confirm)
                } else {
                    saveIp()
                }

            } else {
                ipInputField.error = activity.getString(R.string.api_settings_dialog_message_wrong_format)
            }
        }
    }

    fun setMessage(@StringRes message: Int) {
        this.messageField.setText(message)
    }

    private fun fill() {
        if (!isIpFirstAdding()) {
            ipInputField.text = SpannableStringBuilder(oldIp)
            ipInputField.setSelection(oldIp.length)
            submit.text = activity.getString(R.string.api_settings_dialog_confirm_button_change)
        } else {
            submit.text = activity.getString(R.string.api_settings_dialog_confirm_button_submit)
        }
        setMessage(getRequiredMessage())
        ipInputField.hint = activity.getString(R.string.api_settings_dialog_input_hint)
    }

    private fun getRequiredMessage(): Int {
        return if (isIpFirstAdding()) (R.string.api_settings_dialog_message_set_ip)
        else (if (!isAlert) (R.string.sync_ip_address)
        else (R.string.api_settings_dialog_message_seems_you_are_using_wrong_ip))
    }

    private fun isIpChanged(): Boolean {
        return oldIp != ipInputField.text.toString()
    }

    private fun saveIp() {
        ipListener.accept(ipInputField.text.toString())
        dismiss()
    }

    private fun isIpFirstAdding(): Boolean {
        return oldIp == "localhost"
    }

    private fun isIpValid() = IP_PATTERN.matcher(ipInputField.text.toString()).matches()

    private companion object {
        val IP_PATTERN: Pattern = Pattern.compile(
                "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$")
    }
}