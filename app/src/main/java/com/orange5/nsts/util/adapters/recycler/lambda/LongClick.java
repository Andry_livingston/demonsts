package com.orange5.nsts.util.adapters.recycler.lambda;

public interface LongClick<M> {
    boolean onLongClick(int position, M item);

}
