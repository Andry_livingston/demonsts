package com.orange5.nsts.util.adapters.spinner;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.orange5.nsts.R;
import com.orange5.nsts.util.RealTimeSearch;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.orange5.nsts.util.ViewFactory;
import com.orange5.nsts.util.adapters.recycler.RecyclerAdapterBuilder;

public class SearchableSpinnerAdapter<MODEL> {


    public SearchableSpinnerAdapter(TextView textView,
                                    List<MODEL> objects,
                                    Supplier<MODEL> defaultSelectionSupplier,
                                    Consumer<MODEL> selectedItemConsumer) {

        textView.setOnClickListener(new View.OnClickListener() {

            private AlertDialog alertDialog;
            private Context context;
            private RecyclerView itemsList;

            @Override
            public void onClick(View v) {
                context = textView.getContext();
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                View contents = LayoutInflater.from(context).inflate(R.layout.searchable_dialog, null);
                EditText searchInput = contents.findViewById(R.id.searchInput);
                itemsList = contents.findViewById(R.id.itemsList);

                RealTimeSearch.with(searchInput).searchHandler(s -> {
                    List<MODEL> filtered = objects.stream().filter(object -> object.toString().toLowerCase().contains(s.toLowerCase())).collect(Collectors.toList());
                    updateItems(filtered);
                });
                updateItems(objects);
                builder.setView(contents);
                alertDialog = builder.create();
                alertDialog.show();
            }

            private void updateItems(List<MODEL> items) {
                MODEL defaultSelection = defaultSelectionSupplier.get();

                new RecyclerAdapterBuilder<TextView, MODEL>(itemsList)
                        .bind((view, model) -> view.setText(model.toString()))
                        .data(() -> items)
                        .viewFactory(() -> ViewFactory.createListItem(context, itemsList))
                        .onClick(android.R.id.text1, (position, item) -> {
                            textView.setText(item.toString());
                            selectedItemConsumer.accept(item);
                            textView.setTextColor(context.getColor(R.color.black));
                            alertDialog.dismiss();
                        })
                        .mutate((view, model, position) -> {
                            if (model.equals(defaultSelection)) {
                                view.setBackgroundResource(R.color.lightGray);
                            } else {
                                view.setBackground(null);
                            }
                        })
                        .vertical();
            }
        });
    }

}
