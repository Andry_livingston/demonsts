package com.orange5.nsts.di.modules;

import com.orange5.nsts.data.db.dao.AccessoryCrossReferenceDao;
import com.orange5.nsts.data.db.dao.AccountingCodeDao;
import com.orange5.nsts.data.db.dao.AppStateDao;
import com.orange5.nsts.data.db.dao.BinNumberDao;
import com.orange5.nsts.data.db.dao.BinTypeDao;
import com.orange5.nsts.data.db.dao.CategoryDao;
import com.orange5.nsts.data.db.dao.ConsumableAlternateProductNumberXrefDao;
import com.orange5.nsts.data.db.dao.ConsumableBinDao;
import com.orange5.nsts.data.db.dao.ConsumableProduct2TagDao;
import com.orange5.nsts.data.db.dao.ConsumableProductBinHistoryDao;
import com.orange5.nsts.data.db.dao.ConsumableProductCustomFieldsDao;
import com.orange5.nsts.data.db.dao.ConsumableProductDao;
import com.orange5.nsts.data.db.dao.Customer2RoleDao;
import com.orange5.nsts.data.db.dao.CustomerCustomFieldsDao;
import com.orange5.nsts.data.db.dao.CustomerCustomFieldsTypesDao;
import com.orange5.nsts.data.db.dao.CustomerDao;
import com.orange5.nsts.data.db.dao.CustomerPositionDao;
import com.orange5.nsts.data.db.dao.CustomerProductNumberXrefDao;
import com.orange5.nsts.data.db.dao.CustomerRoleDao;
import com.orange5.nsts.data.db.dao.CustomerStatusDao;
import com.orange5.nsts.data.db.dao.DaoSession;
import com.orange5.nsts.data.db.dao.DiscrepanciesDao;
import com.orange5.nsts.data.db.dao.DiscrepanciesResolvedStatusDao;
import com.orange5.nsts.data.db.dao.DiscrepanciesTypeDao;
import com.orange5.nsts.data.db.dao.DisplayColumnNameDao;
import com.orange5.nsts.data.db.dao.ErrorDao;
import com.orange5.nsts.data.db.dao.InventoryAdjustmentDao;
import com.orange5.nsts.data.db.dao.InventoryAdjustmentReasonCodeDao;
import com.orange5.nsts.data.db.dao.OrderConsumableItemDao;
import com.orange5.nsts.data.db.dao.OrderConsumableReturnDao;
import com.orange5.nsts.data.db.dao.OrderConsumableReturnItemByCustomerDao;
import com.orange5.nsts.data.db.dao.OrderDao;
import com.orange5.nsts.data.db.dao.OrderRentalItemDao;
import com.orange5.nsts.data.db.dao.OrderStatusDao;
import com.orange5.nsts.data.db.dao.ParametersDao;
import com.orange5.nsts.data.db.dao.PickTicketDao;
import com.orange5.nsts.data.db.dao.ProductReceivingLogDao;
import com.orange5.nsts.data.db.dao.RentalProduct2TagDao;
import com.orange5.nsts.data.db.dao.RentalProductBinHistoryDao;
import com.orange5.nsts.data.db.dao.RentalProductCustomFieldsDao;
import com.orange5.nsts.data.db.dao.RentalProductDao;
import com.orange5.nsts.data.db.dao.RentalProductItemDao;
import com.orange5.nsts.data.db.dao.RentalProductStatusDao;
import com.orange5.nsts.data.db.dao.RentalProductStatusHistoryDao;
import com.orange5.nsts.data.db.dao.RentalReturnDao;
import com.orange5.nsts.data.db.dao.RoleDao;
import com.orange5.nsts.data.db.dao.ShippingNoticeDao;
import com.orange5.nsts.data.db.dao.SyncTransferObjectDao;
import com.orange5.nsts.data.db.dao.Tag2CustomerRoleDao;
import com.orange5.nsts.data.db.dao.TagDao;
import com.orange5.nsts.data.db.dao.TransactionCustomFieldsDao;
import com.orange5.nsts.data.db.dao.TransactionHistoryDao;
import com.orange5.nsts.data.db.dao.TransactionTypeDao;
import com.orange5.nsts.data.db.dao.TransferCarrierDao;
import com.orange5.nsts.data.db.dao.TransferDao;
import com.orange5.nsts.data.db.dao.TransferItemDao;
import com.orange5.nsts.data.db.dao.TransferStatusDao;
import com.orange5.nsts.data.db.dao.User2RoleDao;
import com.orange5.nsts.data.db.dao.UserDao;
import com.orange5.nsts.service.base.SyncObjectService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DbDaoModule {

    @Singleton
    @Provides
    AccessoryCrossReferenceDao accessoryCrossReferenceDao(DaoSession daoSession) {
        return daoSession.getAccessoryCrossReferenceDao();
    }

    @Singleton
    @Provides
    AccountingCodeDao accountingCodeDao(DaoSession daoSession) {
        return daoSession.getAccountingCodeDao();
    }

    @Singleton
    @Provides
    AppStateDao appStateDao(DaoSession daoSession) {
        return daoSession.getAppStateDao();
    }

    @Singleton
    @Provides
    BinNumberDao binNumberDao(DaoSession daoSession) {
        return daoSession.getBinNumberDao();
    }

    @Singleton
    @Provides
    BinTypeDao binTypeDao(DaoSession daoSession) {
        return daoSession.getBinTypeDao();
    }

    @Singleton
    @Provides
    CategoryDao categoryDao(DaoSession daoSession) {
        return daoSession.getCategoryDao();
    }

    @Singleton
    @Provides
    ConsumableAlternateProductNumberXrefDao consumableAlternateProductNumberXrefDao(DaoSession daoSession) {
        return daoSession.getConsumableAlternateProductNumberXrefDao();
    }

    @Singleton
    @Provides
    ConsumableBinDao consumableBinDao(DaoSession daoSession) {
        return daoSession.getConsumableBinDao();
    }

    @Singleton
    @Provides
    ConsumableProduct2TagDao consumableProduct2TagDao(DaoSession daoSession) {
        return daoSession.getConsumableProduct2TagDao();
    }

    @Singleton
    @Provides
    ConsumableProductBinHistoryDao consumableProductBinHistoryDao(DaoSession daoSession) {
        return daoSession.getConsumableProductBinHistoryDao();
    }

    @Singleton
    @Provides
    ConsumableProductCustomFieldsDao consumableProductCustomFieldsDao(DaoSession daoSession) {
        return daoSession.getConsumableProductCustomFieldsDao();
    }

    @Singleton
    @Provides
    ConsumableProductDao consumableProductDao(DaoSession daoSession) {
        return daoSession.getConsumableProductDao();
    }

    @Singleton
    @Provides
    Customer2RoleDao customer2RoleDao(DaoSession daoSession) {
        return daoSession.getCustomer2RoleDao();
    }

    @Singleton
    @Provides
    CustomerCustomFieldsDao customerCustomFieldsDao(DaoSession daoSession) {
        return daoSession.getCustomerCustomFieldsDao();
    }

    @Singleton
    @Provides
    CustomerCustomFieldsTypesDao customerCustomFieldsTypesDao(DaoSession daoSession) {
        return daoSession.getCustomerCustomFieldsTypesDao();
    }

    @Singleton
    @Provides
    CustomerDao customerDao(DaoSession daoSession) {
        return daoSession.getCustomerDao();
    }

    @Singleton
    @Provides
    CustomerPositionDao customerPositionDao(DaoSession daoSession) {
        return daoSession.getCustomerPositionDao();
    }

    @Singleton
    @Provides
    CustomerProductNumberXrefDao customerProductNumberXrefDao(DaoSession daoSession) {
        return daoSession.getCustomerProductNumberXrefDao();
    }

    @Singleton
    @Provides
    CustomerRoleDao customerRoleDao(DaoSession daoSession) {
        return daoSession.getCustomerRoleDao();
    }

    @Singleton
    @Provides
    CustomerStatusDao customerStatusDao(DaoSession daoSession) {
        return daoSession.getCustomerStatusDao();
    }

    @Singleton
    @Provides
    DiscrepanciesDao discrepanciesDao(DaoSession daoSession) {
        return daoSession.getDiscrepanciesDao();
    }

    @Singleton
    @Provides
    DiscrepanciesResolvedStatusDao discrepanciesResolvedStatusDao(DaoSession daoSession) {
        return daoSession.getDiscrepanciesResolvedStatusDao();
    }

    @Singleton
    @Provides
    DiscrepanciesTypeDao discrepanciesTypeDao(DaoSession daoSession) {
        return daoSession.getDiscrepanciesTypeDao();
    }

    @Singleton
    @Provides
    DisplayColumnNameDao displayColumnNameDao(DaoSession daoSession) {
        return daoSession.getDisplayColumnNameDao();
    }

    @Singleton
    @Provides
    ErrorDao errorDao(DaoSession daoSession) {
        return daoSession.getErrorDao();
    }

    @Singleton
    @Provides
    InventoryAdjustmentDao inventoryAdjustmentDao(DaoSession daoSession) {
        return daoSession.getInventoryAdjustmentDao();
    }

    @Singleton
    @Provides
    InventoryAdjustmentReasonCodeDao inventoryAdjustmentReasonCodeDao(DaoSession daoSession) {
        return daoSession.getInventoryAdjustmentReasonCodeDao();
    }

    @Singleton
    @Provides
    OrderConsumableItemDao orderConsumableItemDao(DaoSession daoSession) {
        return daoSession.getOrderConsumableItemDao();
    }

    @Singleton
    @Provides
    OrderConsumableReturnDao orderConsumableReturnDao(DaoSession daoSession) {
        return daoSession.getOrderConsumableReturnDao();
    }

    @Singleton
    @Provides
    OrderConsumableReturnItemByCustomerDao orderConsumableReturnItemByCustomerDao(DaoSession daoSession) {
        return daoSession.getOrderConsumableReturnItemByCustomerDao();
    }

    @Singleton
    @Provides
    OrderDao orderDao(DaoSession daoSession) {
        return daoSession.getOrderDao();
    }

    @Singleton
    @Provides
    OrderRentalItemDao orderRentalItemDao(DaoSession daoSession) {
        return daoSession.getOrderRentalItemDao();
    }

    @Singleton
    @Provides
    OrderStatusDao orderStatusDao(DaoSession daoSession) {
        return daoSession.getOrderStatusDao();
    }

    @Singleton
    @Provides
    ParametersDao parametersDao(DaoSession daoSession) {
        return daoSession.getParametersDao();
    }

    @Singleton
    @Provides
    PickTicketDao pickTicketDao(DaoSession daoSession) {
        return daoSession.getPickTicketDao();
    }

    @Singleton
    @Provides
    ProductReceivingLogDao productReceivingLogDao(DaoSession daoSession) {
        return daoSession.getProductReceivingLogDao();
    }

    @Singleton
    @Provides
    RentalProduct2TagDao rentalProduct2TagDao(DaoSession daoSession) {
        return daoSession.getRentalProduct2TagDao();
    }

    @Singleton
    @Provides
    RentalProductBinHistoryDao rentalProductBinHistoryDao(DaoSession daoSession) {
        return daoSession.getRentalProductBinHistoryDao();
    }

    @Singleton
    @Provides
    RentalProductCustomFieldsDao rentalProductCustomFieldsDao(DaoSession daoSession) {
        return daoSession.getRentalProductCustomFieldsDao();
    }

    @Singleton
    @Provides
    RentalProductDao rentalProductDao(DaoSession daoSession) {
        return daoSession.getRentalProductDao();
    }

    @Singleton
    @Provides
    RentalProductItemDao rentalProductItemDao(DaoSession daoSession) {
        return daoSession.getRentalProductItemDao();
    }

    @Singleton
    @Provides
    RentalProductStatusDao rentalProductStatusDao(DaoSession daoSession) {
        return daoSession.getRentalProductStatusDao();
    }

    @Singleton
    @Provides
    RentalProductStatusHistoryDao rentalProductStatusHistoryDao(DaoSession daoSession) {
        return daoSession.getRentalProductStatusHistoryDao();
    }

    @Singleton
    @Provides
    RentalReturnDao rentalReturnDao(DaoSession daoSession) {
        return daoSession.getRentalReturnDao();
    }

    @Singleton
    @Provides
    RoleDao roleDao(DaoSession daoSession) {
        return daoSession.getRoleDao();
    }

    @Singleton
    @Provides
    ShippingNoticeDao shippingNoticeDao(DaoSession daoSession) {
        return daoSession.getShippingNoticeDao();
    }

    @Singleton
    @Provides
    SyncTransferObjectDao syncTransferObjectDao(DaoSession daoSession) {
        return daoSession.getSyncTransferObjectDao();
    }

    @Singleton
    @Provides
    Tag2CustomerRoleDao tag2CustomerRoleDao(DaoSession daoSession) {
        return daoSession.getTag2CustomerRoleDao();
    }

    @Singleton
    @Provides
    TagDao tagDao(DaoSession daoSession) {
        return daoSession.getTagDao();
    }

    @Singleton
    @Provides
    TransactionCustomFieldsDao transactionCustomFieldsDao(DaoSession daoSession) {
        return daoSession.getTransactionCustomFieldsDao();
    }

    @Singleton
    @Provides
    TransactionHistoryDao transactionHistoryDao(DaoSession daoSession) {
        return daoSession.getTransactionHistoryDao();
    }

    @Singleton
    @Provides
    TransactionTypeDao transactionTypeDao(DaoSession daoSession) {
        return daoSession.getTransactionTypeDao();
    }

    @Singleton
    @Provides
    TransferCarrierDao transferCarrierDao(DaoSession daoSession) {
        return daoSession.getTransferCarrierDao();
    }

    @Singleton
    @Provides
    TransferDao transferDao(DaoSession daoSession) {
        return daoSession.getTransferDao();
    }

    @Singleton
    @Provides
    TransferItemDao transferItemDao(DaoSession daoSession) {
        return daoSession.getTransferItemDao();
    }

    @Singleton
    @Provides
    TransferStatusDao transferStatusDao(DaoSession daoSession) {
        return daoSession.getTransferStatusDao();
    }

    @Singleton
    @Provides
    User2RoleDao user2RoleDao(DaoSession daoSession) {
        return daoSession.getUser2RoleDao();
    }

    @Singleton
    @Provides
    UserDao userDao(DaoSession daoSession) {
        return daoSession.getUserDao();
    }

    @Singleton
    @Provides
    SyncObjectService syncObjectService(DaoSession daoSession) {
        return new SyncObjectService(daoSession.getSyncTransferObjectDao());
    }
}
