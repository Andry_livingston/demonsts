package com.orange5.nsts.di.modules;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Vibrator;

import com.orange5.nsts.app.App;
import com.orange5.nsts.app.AppLifecycleListener;
import com.orange5.nsts.di.scopes.DataBaseFile;
import com.orange5.nsts.di.scopes.ExportFile;
import com.orange5.nsts.receivers.BroadcastService;
import com.orange5.nsts.sync.udp.UdpClientService;

import java.io.File;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static com.orange5.nsts.di.modules.DbModule.DB_NAME;

@Module
public class AppModule {

    @Provides
    @Singleton
    Context context(App app) {
        return app.getApplicationContext();
    }

    @Provides
    @Singleton
    Resources resources(Context context) {
        return context.getResources();
    }

    @Provides
    @Singleton
    AssetManager assetManager(Context context) {
        return context.getAssets();
    }

    @Provides
    @DataBaseFile
    File dbStoreDirectory(Context context) {
        return context.getDatabasePath(DB_NAME);
    }

    @Provides
    @ExportFile
    File exportDirectory(Context context) {
        return new File(context.getExternalFilesDir(null), "NSTS_DB");
    }

    @Singleton
    @Provides
    BroadcastService broadcastService(Context context) {
        return new BroadcastService(context);
    }

    @Provides
    @Singleton
    UdpClientService udpClientService(BroadcastService broadcastService) {
        return new UdpClientService(broadcastService);
    }

    @Singleton
    @Provides
    AppLifecycleListener appLifecycleListener(App app, UdpClientService udpClientService) {
        AppLifecycleListener callback = new AppLifecycleListener(udpClientService);
        app.registerActivityLifecycleCallbacks(callback);
        return callback;
    }

    @Singleton
    @Provides
    Vibrator vibrator(Context context) {
        return (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
    }
}
