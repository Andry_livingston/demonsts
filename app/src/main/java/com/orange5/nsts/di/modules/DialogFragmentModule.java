package com.orange5.nsts.di.modules;

import com.orange5.nsts.di.scopes.FragmentDialogScope;
import com.orange5.nsts.di.scopes.FragmentScope;
import com.orange5.nsts.ui.returns.full.inspect.FullRentalItemReturnDialog;
import com.orange5.nsts.ui.returns.full.revision.RevisionReturnDialog;
import com.orange5.nsts.ui.returns.quick.QuickRentalItemReturnDialog;
import com.orange5.nsts.ui.returns.signature.FinishReturnDialog;
import com.orange5.nsts.ui.shipping.discrepancy.add.ReceivingAddDiscrepancyDetailsDialog;
import com.orange5.nsts.ui.shipping.discrepancy.add.ReceivingAddDiscrepancyDialog;
import com.orange5.nsts.ui.shipping.discrepancy.add.ReceivingAddExistingExtrasDetailsDialog;
import com.orange5.nsts.ui.shipping.discrepancy.add.ReceivingAddUnknownExtrasDialog;
import com.orange5.nsts.ui.shipping.discrepancy.add.ReceivingEditDiscrepancyDetailsDialog;
import com.orange5.nsts.ui.shipping.discrepancy.add.ReceivingEditUnknownExtrasDialog;
import com.orange5.nsts.ui.shipping.discrepancy.add.TakeAwayAddDiscrepancyDetailsDialog;
import com.orange5.nsts.ui.shipping.discrepancy.add.TakeAwayAddDiscrepancyDialog;
import com.orange5.nsts.ui.shipping.discrepancy.add.TakeAwayAddExistingExtrasDetailsDialog;
import com.orange5.nsts.ui.shipping.discrepancy.add.TakeAwayAddUnknownExtrasDialog;
import com.orange5.nsts.ui.shipping.discrepancy.add.TakeAwayEditDiscrepancyDetailsDialog;
import com.orange5.nsts.ui.shipping.discrepancy.add.TakeAwayEditUnknownExtrasDialog;
import com.orange5.nsts.ui.shipping.receiving.ConsumeASNConsumableDialog;
import com.orange5.nsts.ui.shipping.receiving.ConsumeReceivingConsumableDialog;
import com.orange5.nsts.ui.shipping.receiving.ReceivingConsumableDialog;
import com.orange5.nsts.ui.shipping.takeaway.TakeAwayConsumableDialog;
import com.orange5.nsts.ui.shipping.takeaway.TakeAwayRentalDialog;
import com.orange5.nsts.ui.shipping.takeaway.TakeAwayReturnConsumableDialog;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class DialogFragmentModule {

    @FragmentDialogScope
    @ContributesAndroidInjector
    abstract QuickRentalItemReturnDialog quickRentalItemReturnDialog();

    @FragmentDialogScope
    @ContributesAndroidInjector
    abstract FinishReturnDialog finishReturnDialog();

    @FragmentDialogScope
    @ContributesAndroidInjector
    abstract FullRentalItemReturnDialog fullRentalItemReturnDialog();

    @FragmentDialogScope
    @ContributesAndroidInjector
    abstract RevisionReturnDialog revisionReturnDialog();

    @FragmentDialogScope
    @ContributesAndroidInjector
    abstract TakeAwayConsumableDialog takeAwayConsumablesDialog();

    @FragmentDialogScope
    @ContributesAndroidInjector
    abstract TakeAwayReturnConsumableDialog takeAwayReturnConsumableDialog();

    @FragmentDialogScope
    @ContributesAndroidInjector
    abstract TakeAwayRentalDialog takeAwayRentalDialog();

    @FragmentDialogScope
    @ContributesAndroidInjector
    abstract ConsumeASNConsumableDialog consumeASNConsumableDialog();

    @FragmentDialogScope
    @ContributesAndroidInjector
    abstract ConsumeReceivingConsumableDialog consumeReceivingConsumableDialog();

    @FragmentDialogScope
    @ContributesAndroidInjector
    abstract ReceivingEditDiscrepancyDetailsDialog receivingEditDiscrepancyDetailsDialog();

    @FragmentDialogScope
    @ContributesAndroidInjector
    abstract TakeAwayEditDiscrepancyDetailsDialog takeAwayEditDiscrepancyDetailsDialog();

    @FragmentDialogScope
    @ContributesAndroidInjector
    abstract ReceivingAddDiscrepancyDetailsDialog receivingAddDiscrepancyDetailsDialog();

    @FragmentDialogScope
    @ContributesAndroidInjector
    abstract TakeAwayAddDiscrepancyDetailsDialog takeAwayAddDiscrepancyDetailsDialog();

    @FragmentDialogScope
    @ContributesAndroidInjector
    abstract ReceivingAddExistingExtrasDetailsDialog receivingAddUnknownExtrasDetailsDialog();

    @FragmentDialogScope
    @ContributesAndroidInjector
    abstract TakeAwayAddExistingExtrasDetailsDialog takeAwayAddUnknownExtrasDetailsDialog();

    @FragmentScope
    @ContributesAndroidInjector
    abstract ReceivingAddDiscrepancyDialog receivingAddDiscrepancyDialog();

    @FragmentScope
    @ContributesAndroidInjector
    abstract TakeAwayAddDiscrepancyDialog takeAwayAddDiscrepancyDialog();

    @FragmentScope
    @ContributesAndroidInjector
    abstract ReceivingAddUnknownExtrasDialog receivingAddUnknownExtrasDialog();

    @FragmentScope
    @ContributesAndroidInjector
    abstract TakeAwayAddUnknownExtrasDialog takeAwayAddUnknownExtrasDialog();

    @FragmentScope
    @ContributesAndroidInjector
    abstract ReceivingEditUnknownExtrasDialog receivingEditUnknownExtrasDialog();

    @FragmentScope
    @ContributesAndroidInjector
    abstract TakeAwayEditUnknownExtrasDialog takeAwayEditUnknownExtrasDialog();
}
