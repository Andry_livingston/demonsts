package com.orange5.nsts.di.modules;

import com.orange5.nsts.di.scopes.FragmentScope;
import com.orange5.nsts.ui.home.BottomMenuDialog;
import com.orange5.nsts.ui.home.orders.OrdersFragment;
import com.orange5.nsts.ui.home.returns.ReturnsFragment;
import com.orange5.nsts.ui.report.closed.ClosedOrderReportFragment;
import com.orange5.nsts.ui.report.returns.ReturnReportFragment;
import com.orange5.nsts.ui.returns.full.inspect.FullRentalItemsFragment;
import com.orange5.nsts.ui.returns.full.inspect.FullReturnedItemsFragment;
import com.orange5.nsts.ui.returns.full.revision.RevisionRentalItemsFragment;
import com.orange5.nsts.ui.returns.full.revision.RevisionReturnedItemsFragment;
import com.orange5.nsts.ui.returns.quick.QuickRentalItemsFragment;
import com.orange5.nsts.ui.returns.quick.QuickReturnedItemsFragment;
import com.orange5.nsts.ui.shipping.discrepancy.list.DiscrepancyListFragment;
import com.orange5.nsts.ui.shipping.receiving.ReceivingDiscrepancyFragment;
import com.orange5.nsts.ui.shipping.receiving.ReceivingInitFragment;
import com.orange5.nsts.ui.shipping.receiving.ReceivingResultFragment;
import com.orange5.nsts.ui.shipping.takeaway.TakeAwayDiscrepancyFragment;
import com.orange5.nsts.ui.shipping.takeaway.TakeAwayInitFragment;
import com.orange5.nsts.ui.shipping.takeaway.TakeAwayResultFragment;
import com.orange5.nsts.ui.shipping.transfer.ShippingNoticeFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract OrdersFragment ordersFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract ReturnsFragment returnsFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract QuickRentalItemsFragment quickRentalItemsFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract QuickReturnedItemsFragment quickReturnedItemsFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract FullRentalItemsFragment fullRentalItemsFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract FullReturnedItemsFragment fullReturnedItemsFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract RevisionRentalItemsFragment revisionRentalItemsFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract RevisionReturnedItemsFragment revisionReturnedItemsFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract ReturnReportFragment returnReportFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract ClosedOrderReportFragment closedOrderReportFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract ShippingNoticeFragment shippingNoticeFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract ReceivingResultFragment receivingBinFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract ReceivingInitFragment receivingNoticeFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract TakeAwayInitFragment takeAwayInitFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract TakeAwayResultFragment takeAwayResultFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract BottomMenuDialog bottomSheetCreatingDialogFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract DiscrepancyListFragment discrepancyFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract ReceivingDiscrepancyFragment receivingDiscrepancyFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract TakeAwayDiscrepancyFragment takeAwayDiscrepancyFragment();

}
