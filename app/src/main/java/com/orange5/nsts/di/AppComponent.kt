package com.orange5.nsts.di

import com.orange5.nsts.app.App
import com.orange5.nsts.di.modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            AndroidInjectionModule::class,
            AppModule::class,
            ActivityModule::class,
            ViewModelModule::class,
            ScannerModule::class,
            DbModule::class,
            NetworkModule::class,
            DbDaoModule::class,
            SyncModule::class,
            FragmentModule::class,
            ServiceModule::class,
            DialogFragmentModule::class,
            UiModule::class]
)
interface AppComponent : AndroidInjector<App> {

    override fun inject(instance: App)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(app: App): Builder

        fun build(): AppComponent
    }
}