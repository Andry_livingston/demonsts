package com.orange5.nsts.di.modules;


import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.orange5.nsts.di.scopes.ViewModelKey;
import com.orange5.nsts.scan.ScanningViewModel;
import com.orange5.nsts.ui.base.ViewModelFactory;
import com.orange5.nsts.ui.customer.SelectCustomerViewModel;
import com.orange5.nsts.ui.home.HomeViewModel;
import com.orange5.nsts.ui.load.InitLoadViewModel;
import com.orange5.nsts.ui.order.full.pickup.PickUpViewModel;
import com.orange5.nsts.ui.order.full.select.OrderItemViewModel;
import com.orange5.nsts.ui.order.quick.QuickOrderViewModel;
import com.orange5.nsts.ui.report.ReportViewModel;
import com.orange5.nsts.ui.returns.full.inspect.FullReturnViewModel;
import com.orange5.nsts.ui.returns.full.revision.RevisionViewModel;
import com.orange5.nsts.ui.returns.quick.QuickReturnViewModel;
import com.orange5.nsts.ui.settings.SettingsViewModel;
import com.orange5.nsts.ui.shipping.discrepancy.list.DiscrepancyListViewModel;
import com.orange5.nsts.ui.shipping.receiving.ReceivingViewModel;
import com.orange5.nsts.ui.shipping.takeaway.TakeAwayViewModel;
import com.orange5.nsts.ui.shipping.transfer.ShippingNoticeViewModel;
import com.orange5.nsts.ui.splash.SplashViewModel;
import com.orange5.nsts.ui.stresstesting.StressTestViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel.class)
    abstract ViewModel splashViewModel(SplashViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SettingsViewModel.class)
    abstract ViewModel settingsViewModel(SettingsViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel.class)
    abstract ViewModel homeViewModel(HomeViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SelectCustomerViewModel.class)
    abstract ViewModel selectCustomerViewModel(SelectCustomerViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(QuickOrderViewModel.class)
    abstract ViewModel quickOrderViewModel(QuickOrderViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(OrderItemViewModel.class)
    abstract ViewModel orderItemViewModel(OrderItemViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PickUpViewModel.class)
    abstract ViewModel pickUpViewModel(PickUpViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ScanningViewModel.class)
    abstract ViewModel scannableViewModel(ScanningViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(QuickReturnViewModel.class)
    abstract ViewModel quickReturnViewModel(QuickReturnViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(FullReturnViewModel.class)
    abstract ViewModel fullReturnViewModel(FullReturnViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(RevisionViewModel.class)
    abstract ViewModel revisionViewModel(RevisionViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ReportViewModel.class)
    abstract ViewModel reportActivity(ReportViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(InitLoadViewModel.class)
    abstract ViewModel initLoadViewModel(InitLoadViewModel initLoadViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ShippingNoticeViewModel.class)
    abstract ViewModel shippingNoticeViewModel(ShippingNoticeViewModel shippingNoticeViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ReceivingViewModel.class)
    abstract ViewModel receivingViewModel(ReceivingViewModel receivingViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(TakeAwayViewModel.class)
    abstract ViewModel takeAwayViewModel(TakeAwayViewModel takeAwayViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(DiscrepancyListViewModel.class)
    abstract ViewModel discrepancyViewModel(DiscrepancyListViewModel discrepancyViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(StressTestViewModel.class)
    abstract ViewModel stressTestViewModel(StressTestViewModel stressTestViewModel);

}
