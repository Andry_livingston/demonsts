package com.orange5.nsts.di.modules;

import com.orange5.nsts.di.scopes.ServiceScope;
import com.orange5.nsts.sync.service.SyncService;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ServiceModule {

    @ServiceScope
    @ContributesAndroidInjector
    abstract SyncService syncService();

}
