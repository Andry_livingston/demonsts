package com.orange5.nsts.di.modules;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orange5.nsts.api.converter.ConsumableProductDeserializer;
import com.orange5.nsts.api.converter.DiscrepanciesDeserializer;
import com.orange5.nsts.api.converter.DiscrepanciesResolvedStatusDeserializer;
import com.orange5.nsts.api.converter.DiscrepanciesTypeDeserializer;
import com.orange5.nsts.api.converter.OrderConsumableItemDeserializer;
import com.orange5.nsts.api.converter.OrderDeserializer;
import com.orange5.nsts.api.converter.RentalProductDeserializer;
import com.orange5.nsts.api.converter.RentalProductItemDeserializer;
import com.orange5.nsts.api.converter.RentalReturnDeserializer;
import com.orange5.nsts.api.converter.ShippingNoticeDeserializer;
import com.orange5.nsts.api.converter.SyncObjectSerializer;
import com.orange5.nsts.app.modules.ApiModule;
import com.orange5.nsts.data.db.entities.ConsumableProduct;
import com.orange5.nsts.data.db.entities.Discrepancies;
import com.orange5.nsts.data.db.entities.DiscrepanciesResolvedStatus;
import com.orange5.nsts.data.db.entities.DiscrepanciesType;
import com.orange5.nsts.data.db.entities.Order;
import com.orange5.nsts.data.db.entities.OrderConsumableItem;
import com.orange5.nsts.data.db.entities.RentalProduct;
import com.orange5.nsts.data.db.entities.RentalProductItem;
import com.orange5.nsts.data.db.entities.RentalReturn;
import com.orange5.nsts.data.db.entities.ShippingNotice;
import com.orange5.nsts.data.db.entities.SyncTransferObject;
import com.orange5.nsts.data.preferencies.RawSharedPreferences;
import com.orange5.nsts.util.format.DateFormatter;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

@Module
public class NetworkModule {

    private static final long NETWORK_TIMEOUT = 180L;

    @Singleton
    @Provides
    OkHttpClient okHttpClient() {
        return new OkHttpClient.Builder()
                .readTimeout(NETWORK_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(NETWORK_TIMEOUT, TimeUnit.SECONDS)
                .build();
    }

    @Singleton
    @Provides
    public Gson gson() {
        return new GsonBuilder()
                .setDateFormat(DateFormatter.INIT_LOAD_DATE_PATTERN)
                .registerTypeAdapter(SyncTransferObject.class, new SyncObjectSerializer())
                .registerTypeAdapter(RentalProductItem.class, new RentalProductItemDeserializer())
                .registerTypeAdapter(ConsumableProduct.class, new ConsumableProductDeserializer())
                .registerTypeAdapter(Order.class, new OrderDeserializer())
                .registerTypeAdapter(ShippingNotice.class, new ShippingNoticeDeserializer())
                .registerTypeAdapter(RentalProduct.class, new RentalProductDeserializer())
                .registerTypeAdapter(OrderConsumableItem.class, new OrderConsumableItemDeserializer())
                .registerTypeAdapter(Discrepancies.class, new DiscrepanciesDeserializer())
                .registerTypeAdapter(DiscrepanciesType.class, new DiscrepanciesTypeDeserializer())
                .registerTypeAdapter(DiscrepanciesResolvedStatus.class, new DiscrepanciesResolvedStatusDeserializer())
                .registerTypeAdapter(RentalReturn.class, new RentalReturnDeserializer())
                .serializeNulls()
                .create();
    }

    @Singleton
    @Provides
    public ApiModule apiModule(Gson gson, RawSharedPreferences rawSharedPreferences,
                               OkHttpClient client) {
        return new ApiModule(gson, rawSharedPreferences, client);
    }

}
