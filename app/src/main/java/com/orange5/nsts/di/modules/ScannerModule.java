package com.orange5.nsts.di.modules;

import android.content.Context;
import android.os.Vibrator;

import com.orange5.scanner.ScannerService;
import com.orange5.scanner.ScannerServiceImpl;
import com.orange5.scanner.VibratorHandler;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ScannerModule {

    @Provides
    @Singleton
    ScannerService scannerService(Context context) {
        return new ScannerServiceImpl(context);
    }

    @Provides
    VibratorHandler vibratorHandler(Vibrator vibrator) {
        return new VibratorHandler(vibrator);
    }
}
