package com.orange5.nsts.di.modules;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;

import com.orange5.nsts.data.db.DBExporter;
import com.orange5.nsts.data.db.DbLoader;
import com.orange5.nsts.data.db.dao.DaoMaster;
import com.orange5.nsts.data.db.dao.DaoSession;
import com.orange5.nsts.data.preferencies.RawSharedPreferences;
import com.orange5.nsts.di.scopes.DataBaseFile;
import com.orange5.nsts.di.scopes.ExportFile;
import com.orange5.nsts.util.migration.MigrationStrategyFactory;

import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.identityscope.IdentityScopeType;

import java.io.File;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static android.os.Build.VERSION_CODES.P;

@Module
public class DbModule {

    public static final String DB_NAME = "nsts.db";
    private static final String NSTS_PREFERENCES = "NSTS_Preferences";

    @Singleton
    @Provides
    public DaoMaster.OpenHelper helper(Context context) {
        return new DaoMaster.OpenHelper(context, DB_NAME) {
            @Override
            public void onUpgrade(Database db, int oldVersion, int newVersion) {
                MigrationStrategyFactory.getMigrationStrategy(oldVersion, newVersion).migrate(db);
            }

            @Override
            public void onCreate(Database db) {
            }

            @Override
            public void onConfigure(SQLiteDatabase db) {
                super.onConfigure(db);
                db.disableWriteAheadLogging();
            }
        };
    }

    @Singleton
    @Provides
    SQLiteDatabase sqLiteDatabase(DaoMaster.OpenHelper helper) {
        return helper.getWritableDatabase();
    }

    @Singleton
    @Provides
    DaoSession daoSession(SQLiteDatabase database) {
        return new DaoMaster(database).newSession(IdentityScopeType.None);
    }

    @Provides
    public DbLoader dbLoader(AssetManager assetManager, @DataBaseFile File dbPath, RawSharedPreferences preferences) {
        return new DbLoader(assetManager, dbPath, preferences);
    }

    @Provides
    public DBExporter dbExporter(@DataBaseFile File dbPath, @ExportFile File exportPath) {
        return new DBExporter(dbPath, exportPath);
    }

    @Singleton
    @Provides
    public RawSharedPreferences rawSharedPreferences(Context context) {
        return RawSharedPreferences.instance(context.getSharedPreferences(NSTS_PREFERENCES, Context.MODE_PRIVATE));
    }

}
