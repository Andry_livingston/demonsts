package com.orange5.nsts.di.modules

import android.content.Context
import com.orange5.nsts.data.preferencies.RawSharedPreferences
import com.orange5.nsts.ui.settings.SettingsViewFactory
import dagger.Module
import dagger.Provides

@Module
class UiModule {

    @Provides
    fun settingsViewFactory(context: Context, sharedPreferences: RawSharedPreferences) = SettingsViewFactory(context, sharedPreferences)
}