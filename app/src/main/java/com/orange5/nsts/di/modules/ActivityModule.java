package com.orange5.nsts.di.modules;

import com.orange5.nsts.di.scopes.ActivityScope;
import com.orange5.nsts.ui.customer.SelectCustomerActivity;
import com.orange5.nsts.ui.shipping.discrepancy.list.DiscrepancyListActivity;
import com.orange5.nsts.ui.home.HomeActivity;
import com.orange5.nsts.ui.load.InitLoadActivity;
import com.orange5.nsts.ui.order.full.pickup.PickUpActivity;
import com.orange5.nsts.ui.order.full.select.OrderItemsActivity;
import com.orange5.nsts.ui.order.quick.QuickOrderItemsActivity;
import com.orange5.nsts.ui.report.ReportActivity;
import com.orange5.nsts.ui.returns.full.inspect.FullReturnActivity;
import com.orange5.nsts.ui.returns.full.revision.RevisionActivity;
import com.orange5.nsts.ui.returns.quick.QuickReturnActivity;
import com.orange5.nsts.ui.settings.SettingsActivity;
import com.orange5.nsts.ui.shipping.takeaway.TakeAwayActivity;
import com.orange5.nsts.ui.shipping.transfer.ShippingNoticeActivity;
import com.orange5.nsts.ui.shipping.receiving.ReceivingActivity;
import com.orange5.nsts.ui.splash.SplashActivity;
import com.orange5.nsts.ui.stresstesting.StressTestActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {

    @ActivityScope
    @ContributesAndroidInjector
    abstract SplashActivity splashActivity();

    @ActivityScope
    @ContributesAndroidInjector
    abstract HomeActivity homeActivity();

    @ActivityScope
    @ContributesAndroidInjector
    abstract PickUpActivity pickUpActivity();

    @ActivityScope
    @ContributesAndroidInjector
    abstract SelectCustomerActivity customerSelectActivity();

    @ActivityScope
    @ContributesAndroidInjector
    abstract QuickOrderItemsActivity quickOrderItemsActivity();

    @ActivityScope
    @ContributesAndroidInjector
    abstract OrderItemsActivity orderItemsActivity();

    @ActivityScope
    @ContributesAndroidInjector
    abstract SettingsActivity settingsActivity();

    @ActivityScope
    @ContributesAndroidInjector
    abstract RevisionActivity revisionActivity();

    @ActivityScope
    @ContributesAndroidInjector
    abstract QuickReturnActivity quickReturnActivity();

    @ActivityScope
    @ContributesAndroidInjector
    abstract FullReturnActivity fullReturnActivity();

    @ActivityScope
    @ContributesAndroidInjector
    abstract ReportActivity reportActivity();

    @ActivityScope
    @ContributesAndroidInjector
    abstract InitLoadActivity initLoadActivity();

    @ActivityScope
    @ContributesAndroidInjector
    abstract ShippingNoticeActivity shippingNoticeActivity();

    @ActivityScope
    @ContributesAndroidInjector
    abstract ReceivingActivity receivingActivity();

    @ActivityScope
    @ContributesAndroidInjector
    abstract TakeAwayActivity takeAwayActivity();

    @ActivityScope
    @ContributesAndroidInjector
    abstract DiscrepancyListActivity discrepancyActivity();

    @ActivityScope
    @ContributesAndroidInjector
    abstract StressTestActivity stressTestActivity();

}
