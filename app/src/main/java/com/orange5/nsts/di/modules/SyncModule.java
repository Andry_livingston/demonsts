package com.orange5.nsts.di.modules;

import android.database.sqlite.SQLiteDatabase;

import com.google.gson.Gson;
import com.orange5.nsts.data.db.dao.ConsumableBinDao;
import com.orange5.nsts.data.db.dao.DiscrepanciesDao;
import com.orange5.nsts.data.db.dao.OrderConsumableItemDao;
import com.orange5.nsts.data.db.dao.OrderDao;
import com.orange5.nsts.data.db.dao.OrderRentalItemDao;
import com.orange5.nsts.data.db.dao.ProductReceivingLogDao;
import com.orange5.nsts.data.db.dao.RentalProductItemDao;
import com.orange5.nsts.data.db.dao.RentalReturnDao;
import com.orange5.nsts.data.db.dao.ShippingNoticeDao;
import com.orange5.nsts.service.base.SyncObjectService;
import com.orange5.nsts.sync.NstsSqliteRunner;
import com.orange5.nsts.sync.SyncManager;
import com.orange5.nsts.sync.converter.FromServerConverter;
import com.orange5.nsts.sync.converter.ToServerConverter;
import com.orange5.nsts.sync.local.LocalSyncHelper;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class SyncModule {

    public static final String ORDER_SERVICE = "order_service";
    public static final String CONSUMABLE_BIN_SERVICE = "consumable_bin_service";
    public static final String ORDER_CONSUMABLE_ITEM_SERVICE = "order_consumable_item_service";
    public static final String ORDER_RENTAL_ITEM_SERVICE = "order_rental_item_service";
    public static final String RENTAL_PRODUCT_ITEM_SERVICE = "rental_product_item_service";
    public static final String RENTAL_RETURN_SERVICE = "rental_product_service";
    public static final String ACCOUNTING_SERVICE = "accounting_service";
    public static final String SHIPPING_NOTICE_SERVICE = "shipping_notice_service";
    public static final String RECEIVING_LOG_SERVICE = "receiving_log_service";
    public static final String DISCREPANCIES_SERVICE = "discrepancies_service";

    @Singleton
    @Provides
    SyncManager syncManager(FromServerConverter converter,
                            NstsSqliteRunner sqliteRunner, SyncObjectService syncObjectService) {
        return SyncManager.instance(converter, sqliteRunner, syncObjectService);
    }

    @Provides
    FromServerConverter fromServerConverter(Gson gson) {
        return new FromServerConverter(gson);
    }

    @Provides
    ToServerConverter toServerConverter(Gson gson) {
        return new ToServerConverter(gson);
    }

    @Provides
    NstsSqliteRunner nstsSqliteRunner(SQLiteDatabase database) {
        return new NstsSqliteRunner(database);
    }

    @Provides
    @Named(ORDER_SERVICE)
    LocalSyncHelper orderSyncHelper(OrderDao dao, ToServerConverter converter,
                                    SyncObjectService objectService) {
        return new LocalSyncHelper(dao, converter, objectService);
    }

    @Provides
    @Named(CONSUMABLE_BIN_SERVICE)
    LocalSyncHelper consumableBinSyncHelper(ConsumableBinDao dao, ToServerConverter converter,
                                            SyncObjectService objectService) {
        return new LocalSyncHelper(dao, converter, objectService);
    }

    @Provides
    @Named(ORDER_CONSUMABLE_ITEM_SERVICE)
    LocalSyncHelper orderConsumableItemSyncHelper(OrderConsumableItemDao dao, ToServerConverter converter,
                                                  SyncObjectService objectService) {
        return new LocalSyncHelper(dao, converter, objectService);
    }

    @Provides
    @Named(ORDER_RENTAL_ITEM_SERVICE)
    LocalSyncHelper orderRentalItemSyncHelper(OrderRentalItemDao dao,
                                              ToServerConverter converter,
                                              SyncObjectService objectService) {
        return new LocalSyncHelper(dao, converter, objectService);
    }

    @Provides
    @Named(RENTAL_PRODUCT_ITEM_SERVICE)
    LocalSyncHelper rentalProductItemSyncHelper(RentalProductItemDao dao,
                                                ToServerConverter converter,
                                                SyncObjectService objectService) {
        return new LocalSyncHelper(dao, converter, objectService);
    }

    @Provides
    @Named(RENTAL_RETURN_SERVICE)
    LocalSyncHelper rentalProductSyncHelper(RentalReturnDao dao,
                                            ToServerConverter converter,
                                            SyncObjectService objectService) {
        return new LocalSyncHelper(dao, converter, objectService);
    }

    @Provides
    @Named(ACCOUNTING_SERVICE)
    LocalSyncHelper accountingSyncHelper(OrderDao dao, ToServerConverter converter,
                                         SyncObjectService objectService) {
        return new LocalSyncHelper(dao, converter, objectService);
    }

    @Provides
    @Named(SHIPPING_NOTICE_SERVICE)
    LocalSyncHelper shippingSyncHelper(ShippingNoticeDao dao, ToServerConverter converter,
                                       SyncObjectService objectService) {
        return new LocalSyncHelper(dao, converter, objectService);
    }

    @Provides
    @Named(RECEIVING_LOG_SERVICE)
    LocalSyncHelper receivingLogService(ProductReceivingLogDao dao, ToServerConverter converter,
                                        SyncObjectService objectService) {
        return new LocalSyncHelper(dao, converter, objectService);
    }

    @Provides
    @Named(DISCREPANCIES_SERVICE)
    LocalSyncHelper discrepanciesService(DiscrepanciesDao dao, ToServerConverter converter,
                                         SyncObjectService objectService) {
        return new LocalSyncHelper(dao, converter, objectService);
    }
}

