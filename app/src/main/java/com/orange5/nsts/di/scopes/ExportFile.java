package com.orange5.nsts.di.scopes;

import javax.inject.Qualifier;

@Qualifier
public @interface ExportFile {
}
