package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
FOREIGN KEY(`OrderId`) REFERENCES `Order`(`OrderId`)
PRIMARY KEY(`PickTicketId`)
*/
@Entity(nameInDb = "PickTicket")
public class PickTicket implements DatabaseEntity {

    @Id
    @SerializedName("PickTicketId")
    @Property(nameInDb = "PickTicketId")
    private String pickTicketId;

    @SerializedName("OrderId")
    @Property(nameInDb = "OrderId")
    private String orderId;

    @SerializedName("ProductId")
    @Property(nameInDb = "ProductId")
    private String productId;

    @SerializedName("IsRental")
    @Property(nameInDb = "IsRental")
    private byte isRental;

    @SerializedName("BinId")
    @Property(nameInDb = "BinId")
    private String binId;

    @SerializedName("Quantity")
    @Property(nameInDb = "Quantity")
    private Integer quantity;

    @SerializedName("Picked")
    @Property(nameInDb = "Picked")
    private Integer picked;

    @Generated(hash = 1679919978)
    public PickTicket(String pickTicketId, String orderId, String productId,
                      byte isRental, String binId, Integer quantity, Integer picked) {
        this.pickTicketId = pickTicketId;
        this.orderId = orderId;
        this.productId = productId;
        this.isRental = isRental;
        this.binId = binId;
        this.quantity = quantity;
        this.picked = picked;
    }

    @Generated(hash = 56314070)
    public PickTicket() {
    }

    public String getPickTicketId() {
        return this.pickTicketId;
    }

    public void setPickTicketId(String pickTicketId) {
        this.pickTicketId = pickTicketId;
    }

    public String getOrderId() {
        return this.orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProductId() {
        return this.productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public byte getIsRental() {
        return this.isRental;
    }

    public void setIsRental(byte isRental) {
        this.isRental = isRental;
    }

    public String getBinId() {
        return this.binId;
    }

    public void setBinId(String binId) {
        this.binId = binId;
    }

    public Integer getQuantity() {
        return this.quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getPicked() {
        return this.picked;
    }

    public void setPicked(Integer picked) {
        this.picked = picked;
    }

    @Override
    public Object getPrimaryKey() {
        return getPickTicketId();
    }
}
