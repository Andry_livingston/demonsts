package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
PRIMARY KEY(`OrderStatusId`)
*/
@Entity(nameInDb = "OrderStatus")
public class OrderStatus implements DatabaseEntity {

    @Id
    @SerializedName("OrderStatusId")
    @Property(nameInDb = "OrderStatusId")
    private long orderStatusId;

    @SerializedName("OrderStatusName")
    @Property(nameInDb = "OrderStatusName")
    private String orderStatusName;

    @Generated(hash = 1926822828)
    public OrderStatus(long orderStatusId, String orderStatusName) {
        this.orderStatusId = orderStatusId;
        this.orderStatusName = orderStatusName;
    }

    @Generated(hash = 1836745214)
    public OrderStatus() {
    }

    public long getOrderStatusId() {
        return this.orderStatusId;
    }

    public void setOrderStatusId(long orderStatusId) {
        this.orderStatusId = orderStatusId;
    }

    public String getOrderStatusName() {
        return this.orderStatusName;
    }

    public void setOrderStatusName(String orderStatusName) {
        this.orderStatusName = orderStatusName;
    }

    @Override
    public Object getPrimaryKey() {
        return getOrderStatusId();
    }
}
