package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.util.Date;

/*
  FOREIGN KEY(`ConsumableProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
  FOREIGN KEY(`ChangedByUserId`) REFERENCES `User`(`UserId`),
  FOREIGN KEY(`ReasonCodeId`) REFERENCES `InventoryAdjustmentReasonCode`(`ReasonCodeId`),
  PRIMARY KEY(`InventoryAdjustmentId`)
*/
@Entity(nameInDb = "InventoryAdjustment")
public class InventoryAdjustment implements DatabaseEntity {

    @Id
    @SerializedName("InventoryAdjustmentId")
    @Property(nameInDb = "InventoryAdjustmentId")
    private String inventoryAdjustmentId;

    @SerializedName("ConsumableProductId")
    @Property(nameInDb = "ConsumableProductId")
    private String consumableProductId;

    @SerializedName("OldQnty")
    @Property(nameInDb = "OldQnty")
    private Integer oldQnty;

    @SerializedName("NewQnty")
    @Property(nameInDb = "NewQnty")
    private Integer newQnty;

    @SerializedName("ReasonCodeId")
    @Property(nameInDb = "ReasonCodeId")
    private String reasonCodeId;

    @SerializedName("ChangedByUserId")
    @Property(nameInDb = "ChangedByUserId")
    private String changedByUserId;

    @SerializedName("Notes")
    @Property(nameInDb = "Notes")
    private String notes;

    @SerializedName("AddDate")
    @Property(nameInDb = "AddDate")
    private Date addDate;

    @Generated(hash = 148848430)
    public InventoryAdjustment(String inventoryAdjustmentId, String consumableProductId,
                               Integer oldQnty, Integer newQnty, String reasonCodeId, String changedByUserId,
                               String notes, Date addDate) {
        this.inventoryAdjustmentId = inventoryAdjustmentId;
        this.consumableProductId = consumableProductId;
        this.oldQnty = oldQnty;
        this.newQnty = newQnty;
        this.reasonCodeId = reasonCodeId;
        this.changedByUserId = changedByUserId;
        this.notes = notes;
        this.addDate = addDate;
    }

    @Generated(hash = 601068299)
    public InventoryAdjustment() {
    }

    public String getInventoryAdjustmentId() {
        return this.inventoryAdjustmentId;
    }

    public void setInventoryAdjustmentId(String inventoryAdjustmentId) {
        this.inventoryAdjustmentId = inventoryAdjustmentId;
    }

    public String getConsumableProductId() {
        return this.consumableProductId;
    }

    public void setConsumableProductId(String consumableProductId) {
        this.consumableProductId = consumableProductId;
    }

    public Integer getOldQnty() {
        return this.oldQnty;
    }

    public void setOldQnty(Integer oldQnty) {
        this.oldQnty = oldQnty;
    }

    public Integer getNewQnty() {
        return this.newQnty;
    }

    public void setNewQnty(Integer newQnty) {
        this.newQnty = newQnty;
    }

    public String getReasonCodeId() {
        return this.reasonCodeId;
    }

    public void setReasonCodeId(String reasonCodeId) {
        this.reasonCodeId = reasonCodeId;
    }

    public String getChangedByUserId() {
        return this.changedByUserId;
    }

    public void setChangedByUserId(String changedByUserId) {
        this.changedByUserId = changedByUserId;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getAddDate() {
        return this.addDate;
    }

    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }

    @Override
    public Object getPrimaryKey() {
        return getInventoryAdjustmentId();
    }
}
