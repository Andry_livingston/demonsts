package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

/*
  PRIMARY KEY(`ProductID`,`TagId`),
  FOREIGN KEY(`TagId`) REFERENCES `Tag`(`TagId`),
  FOREIGN KEY(`ProductID`) REFERENCES `RentalProduct`(`ProductId`)
*/
@Entity(nameInDb = "RentalProduct2Tag")
public class RentalProduct2Tag implements DatabaseEntity {

    @Id
    @SerializedName("ProductID")
    @Property(nameInDb = "ProductID")
    private String productID;

    @SerializedName("TagId")
    @Property(nameInDb = "TagId")
    private String tagId;

    @Generated(hash = 1253381967)
    public RentalProduct2Tag(String productID, String tagId) {
        this.productID = productID;
        this.tagId = tagId;
    }

    @Generated(hash = 1253711706)
    public RentalProduct2Tag() {
    }

    public String getProductID() {
        return this.productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getTagId() {
        return this.tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    @Override
    public Object getPrimaryKey() {
        return getProductID();
    }
}
