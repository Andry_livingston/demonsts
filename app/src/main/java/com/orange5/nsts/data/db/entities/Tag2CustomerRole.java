package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
PRIMARY KEY(`TagId`,`CustomerRoleId`),
FOREIGN KEY(`TagId`) REFERENCES `Tag`(`TagId`),
FOREIGN KEY(`CustomerRoleId`) REFERENCES `CustomerRole`(`CustomerRoleId`)
*/
@Entity(nameInDb = "Tag2CustomerRole")
public class Tag2CustomerRole implements DatabaseEntity {

    @Id
    @SerializedName("TagId")
    @Property(nameInDb = "TagId")
    private String tagId;

    @SerializedName("CustomerRoleId")
    @Property(nameInDb = "CustomerRoleId")
    private String customerRoleId;

    @Generated(hash = 1693140734)
    public Tag2CustomerRole(String tagId, String customerRoleId) {
        this.tagId = tagId;
        this.customerRoleId = customerRoleId;
    }

    @Generated(hash = 1642779309)
    public Tag2CustomerRole() {
    }

    public String getTagId() {
        return this.tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getCustomerRoleId() {
        return this.customerRoleId;
    }

    public void setCustomerRoleId(String customerRoleId) {
        this.customerRoleId = customerRoleId;
    }

    @Override
    public Object getPrimaryKey() {
        return getTagId();
    }
}
