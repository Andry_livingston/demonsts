package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
  FOREIGN KEY(`TransactionId`) REFERENCES `TransactionHistory`(`TransactionId`),
  PRIMARY KEY(`CustomFieldId`)
*/
@Entity(nameInDb = "TransactionCustomFields")
public class TransactionCustomFields implements DatabaseEntity {

    @Id
    @SerializedName("CustomFieldId")
    @Property(nameInDb = "CustomFieldId")
    private String customFieldId;

    @SerializedName("TransactionId")
    @Property(nameInDb = "TransactionId")
    private String transactionId;

    @SerializedName("FieldName")
    @Property(nameInDb = "FieldName")
    private String fieldName;

    @SerializedName("FieldValue")
    @Property(nameInDb = "FieldValue")
    private String fieldValue;

    @Generated(hash = 627221324)
    public TransactionCustomFields(String customFieldId, String transactionId,
                                   String fieldName, String fieldValue) {
        this.customFieldId = customFieldId;
        this.transactionId = transactionId;
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }

    @Generated(hash = 672846738)
    public TransactionCustomFields() {
    }

    public String getCustomFieldId() {
        return this.customFieldId;
    }

    public void setCustomFieldId(String customFieldId) {
        this.customFieldId = customFieldId;
    }

    public String getTransactionId() {
        return this.transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getFieldName() {
        return this.fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldValue() {
        return this.fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    @Override
    public Object getPrimaryKey() {
        return getCustomFieldId();
    }
}
