package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
PRIMARY KEY(`CustomerPositionId`)
*/
@Entity(nameInDb = "CustomerPosition")
public class CustomerPosition implements DatabaseEntity {

    @Id
    @SerializedName("CustomerPositionId")
    @Property(nameInDb = "CustomerPositionId")
    private String customerPositionId;

    @SerializedName("PositionName")
    @Property(nameInDb = "PositionName")
    private String positionName;

    @SerializedName("PositionLevel")
    @Property(nameInDb = "PositionLevel")
    private Integer positionLevel;//smallint

    @Generated(hash = 1071494588)
    public CustomerPosition(String customerPositionId, String positionName,
                            Integer positionLevel) {
        this.customerPositionId = customerPositionId;
        this.positionName = positionName;
        this.positionLevel = positionLevel;
    }

    @Generated(hash = 1227827040)
    public CustomerPosition() {
    }

    public String getCustomerPositionId() {
        return this.customerPositionId;
    }

    public void setCustomerPositionId(String customerPositionId) {
        this.customerPositionId = customerPositionId;
    }

    public String getPositionName() {
        return this.positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public Integer getPositionLevel() {
        return this.positionLevel;
    }

    public void setPositionLevel(Integer positionLevel) {
        this.positionLevel = positionLevel;
    }

    @Override
    public Object getPrimaryKey() {
        return getCustomerPositionId();
    }
}
