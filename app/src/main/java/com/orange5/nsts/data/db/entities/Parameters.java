package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
PRIMARY KEY(`ParameterName
*/
@Entity(nameInDb = "Parameters")
public class Parameters implements DatabaseEntity {

    @Id
    @SerializedName("ParameterName")
    @Property(nameInDb = "ParameterName")
    private String parameterName;

    @SerializedName("ParameterValue")
    @Property(nameInDb = "ParameterValue")
    private String parameterValue;

    @SerializedName("ParameterTitle")
    @Property(nameInDb = "ParameterTitle")
    private String parameterTitle;

    @SerializedName("OrderNum")
    @Property(nameInDb = "OrderNum")
    private Integer orderNum;

    @Generated(hash = 1015881753)
    public Parameters(String parameterName, String parameterValue,
            String parameterTitle, Integer orderNum) {
        this.parameterName = parameterName;
        this.parameterValue = parameterValue;
        this.parameterTitle = parameterTitle;
        this.orderNum = orderNum;
    }

    @Generated(hash = 1848746217)
    public Parameters() {
    }

    public String getParameterName() {
        return this.parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getParameterValue() {
        return this.parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    public String getParameterTitle() {
        return this.parameterTitle;
    }

    public void setParameterTitle(String parameterTitle) {
        this.parameterTitle = parameterTitle;
    }

    public Integer getOrderNum() {
        return this.orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    @Override
    public Object getPrimaryKey() {
        return getParameterName();
    }
}
