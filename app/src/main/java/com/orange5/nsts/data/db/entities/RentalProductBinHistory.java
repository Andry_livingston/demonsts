package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.util.Date;

/*
PRIMARY KEY(`HistoryId`),
FOREIGN KEY(`UniqueUnitNumber`) REFERENCES `RentalProductItem`(`UniqueUnitNumber`),
FOREIGN KEY(`BinId`) REFERENCES `BinNumber`(`BinId`)
*/
@Entity(nameInDb = "RentalProductBinHistory")
public class RentalProductBinHistory implements DatabaseEntity {

    @Id
    @SerializedName("HistoryId")
    @Property(nameInDb = "HistoryId")
    private String historyId;

    @SerializedName("UniqueUnitNumber")
    @Property(nameInDb = "UniqueUnitNumber")
    private String uniqueUnitNumber;

    @SerializedName("BinId")
    @Property(nameInDb = "BinId")
    private String binId;

    @SerializedName("AddDate")
    @Property(nameInDb = "AddDate")
    private Date addDate;

    @Generated(hash = 855406726)
    public RentalProductBinHistory(String historyId, String uniqueUnitNumber,
                                   String binId, Date addDate) {
        this.historyId = historyId;
        this.uniqueUnitNumber = uniqueUnitNumber;
        this.binId = binId;
        this.addDate = addDate;
    }

    @Generated(hash = 2002262771)
    public RentalProductBinHistory() {
    }

    public String getHistoryId() {
        return this.historyId;
    }

    public void setHistoryId(String historyId) {
        this.historyId = historyId;
    }

    public String getUniqueUnitNumber() {
        return this.uniqueUnitNumber;
    }

    public void setUniqueUnitNumber(String uniqueUnitNumber) {
        this.uniqueUnitNumber = uniqueUnitNumber;
    }

    public String getBinId() {
        return this.binId;
    }

    public void setBinId(String binId) {
        this.binId = binId;
    }

    public Date getAddDate() {
        return this.addDate;
    }

    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }

    @Override
    public Object getPrimaryKey() {
        return getHistoryId();
    }
}
