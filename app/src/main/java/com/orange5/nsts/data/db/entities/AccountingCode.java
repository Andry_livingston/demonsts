package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

/*
  PRIMARY KEY(`AccountingCodeId`)
*/
@Entity(nameInDb = "AccountingCode")
public class AccountingCode implements DatabaseEntity {

    @Id
    @SerializedName("AccountingCodeId")
    @Property(nameInDb = "AccountingCodeId")
    private String accountingCodeId;

    @SerializedName("AccountingCodeName")
    @Property(nameInDb = "AccountingCodeName")
    private String accountingCodeName;

    @Generated(hash = 1587024356)
    public AccountingCode(String accountingCodeId, String accountingCodeName) {
        this.accountingCodeId = accountingCodeId;
        this.accountingCodeName = accountingCodeName;
    }

    @Generated(hash = 1504296741)
    public AccountingCode() {
    }

    public String getAccountingCodeId() {
        return this.accountingCodeId;
    }

    public void setAccountingCodeId(String accountingCodeId) {
        this.accountingCodeId = accountingCodeId;
    }

    public String getAccountingCodeName() {
        return this.accountingCodeName;
    }

    public void setAccountingCodeName(String accountingCodeName) {
        this.accountingCodeName = accountingCodeName;
    }

    @Override
    public String toString() {
        return accountingCodeName;
    }

    @Override
    public Object getPrimaryKey() {
        return getAccountingCodeId();
    }
}
