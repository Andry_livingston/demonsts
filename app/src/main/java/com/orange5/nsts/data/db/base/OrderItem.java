package com.orange5.nsts.data.db.base;

import com.orange5.nsts.data.db.entities.BinNumber;
import com.orange5.nsts.data.db.entities.OrderConsumableItem;
import com.orange5.nsts.data.db.entities.OrderRentalItem;
import com.orange5.nsts.service.bin.ProductBinInformation;
import com.orange5.nsts.service.rental.AggregatedRentalItem;

import java.util.function.Consumer;

public interface OrderItem extends DatabaseEntity {

    String getProductNumber();

    String getProductId();

    Integer getPicked();

    BinNumber getScannedBin();

    void setScannedBin(BinNumber bin);

    default boolean isConsumable() {
        return this instanceof OrderConsumableItem;
    }

    default Double getPrice() {
        return 0.0;
    }

    default Integer getQuantity() {
        return 1;
    }

    default boolean isOversell() {
        return false;
    }

    default int getAvailableQuantity() {
        return getBinInformation().getTotalQuantity() - getQuantity();
    }

    default boolean allowOversell() {
        return this instanceof OrderConsumableItem;
    }

    default void ifConsumable(Consumer<OrderConsumableItem> consumer) {
        if (this instanceof OrderConsumableItem) {
            consumer.accept((OrderConsumableItem) this);
        }
    }

    default void ifRental(Consumer<OrderRentalItem> consumer) {
        if (this instanceof OrderRentalItem) {
            consumer.accept((OrderRentalItem) this);
        }
    }

    default void ifAggregatedRental(Consumer<AggregatedRentalItem> consumer) {
        if (this instanceof AggregatedRentalItem) {
            consumer.accept((AggregatedRentalItem) this);
        }
    }

    default OrderRentalItem asRental() {
        return (OrderRentalItem) this;
    }


    default OrderConsumableItem asConsumable() {
        return (OrderConsumableItem) this;
    }

    default AggregatedRentalItem asAggregatedRental() {
        return (AggregatedRentalItem) this;
    }

    default ProductBinInformation getBinInformation() {
        ProductBinInformation[] result = new ProductBinInformation[1];
        ifConsumable(orderConsumableItem -> result[0] = orderConsumableItem.getConsumableProduct().getBinInformation());
        ifRental(orderRentalItem -> result[0] = orderRentalItem.getRentalProduct().getBinInformation());
        return result[0];
    }
}
