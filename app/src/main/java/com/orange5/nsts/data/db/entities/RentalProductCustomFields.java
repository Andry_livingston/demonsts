package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

/*
FOREIGN KEY(`ProductId`) REFERENCES `RentalProduct`(`ProductId`),
PRIMARY KEY(`CustomFieldId`)
*/
@Entity(nameInDb = "RentalProductCustomFields")
public class RentalProductCustomFields implements DatabaseEntity {

    @Id
    @SerializedName("CustomFieldId")
    @Property(nameInDb = "CustomFieldId")
    private String customFieldId;

    @SerializedName("ProductId")
    @Property(nameInDb = "ProductId")
    private String productId;

    @SerializedName("FieldName")
    @Property(nameInDb = "FieldName")
    private String fieldName;

    @SerializedName("FieldValue")
    @Property(nameInDb = "FieldValue")
    private String fieldValue;

    @Generated(hash = 1622154271)
    public RentalProductCustomFields(String customFieldId, String productId,
                                     String fieldName, String fieldValue) {
        this.customFieldId = customFieldId;
        this.productId = productId;
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }

    @Generated(hash = 2115025012)
    public RentalProductCustomFields() {
    }

    public String getCustomFieldId() {
        return this.customFieldId;
    }

    public void setCustomFieldId(String customFieldId) {
        this.customFieldId = customFieldId;
    }

    public String getProductId() {
        return this.productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getFieldName() {
        return this.fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldValue() {
        return this.fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    @Override
    public Object getPrimaryKey() {
        return getCustomFieldId();
    }
}
