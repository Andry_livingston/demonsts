package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.util.Date;
/*
PRIMARY KEY(`ErrorId`)
 */
@Entity(nameInDb = "Error")
public class Error implements DatabaseEntity {

    @Id
    @SerializedName("ErrorId")
    @Property(nameInDb = "ErrorId")
    private String errorId;

    @SerializedName("ErrorType")
    @Property(nameInDb = "ErrorType")
    private String errorType;

    @SerializedName("ErrorMessage")
    @Property(nameInDb = "ErrorMessage")
    private String errorMessage;

    @SerializedName("ErrorDate")
    @Property(nameInDb = "ErrorDate")
    private Date errorDate;

    @Generated(hash = 1794410442)
    public Error(String errorId, String errorType, String errorMessage,
                 Date errorDate) {
        this.errorId = errorId;
        this.errorType = errorType;
        this.errorMessage = errorMessage;
        this.errorDate = errorDate;
    }

    @Generated(hash = 1198744959)
    public Error() {
    }

    public String getErrorId() {
        return this.errorId;
    }

    public void setErrorId(String errorId) {
        this.errorId = errorId;
    }

    public String getErrorType() {
        return this.errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Date getErrorDate() {
        return this.errorDate;
    }

    public void setErrorDate(Date errorDate) {
        this.errorDate = errorDate;
    }

    @Override
    public Object getPrimaryKey() {
        return getErrorId();
    }
}
