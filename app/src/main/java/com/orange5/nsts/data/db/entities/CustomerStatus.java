package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

/*
PRIMARY KEY(`StatusId`)
*/
@Entity(nameInDb = "CustomerStatus")
public class CustomerStatus implements DatabaseEntity {

    @Id
    @SerializedName("StatusId")
    @Property(nameInDb = "StatusId")
    private long statusId;

    @SerializedName("StatusName")
    @Property(nameInDb = "StatusName")
    private String statusName;

    @Generated(hash = 981277201)
    public CustomerStatus(long statusId, String statusName) {
        this.statusId = statusId;
        this.statusName = statusName;
    }

    @Generated(hash = 1330642805)
    public CustomerStatus() {
    }

    public long getStatusId() {
        return this.statusId;
    }

    public void setStatusId(long statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return this.statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    @Override
    public Object getPrimaryKey() {
        return getStatusId();
    }
}

