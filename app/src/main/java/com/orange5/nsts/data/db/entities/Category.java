package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
  PRIMARY KEY(`CategoryID`)
*/
@Entity(nameInDb = "Category")
public class Category implements DatabaseEntity {

    @Id
    @SerializedName("CategoryID")
    @Property(nameInDb = "CategoryID")
    private String categoryID;

    @SerializedName("CategoryName")
    @Property(nameInDb = "CategoryName")
    private String categoryName;

    @Generated(hash = 1332428146)
    public Category(String categoryID, String categoryName) {
        this.categoryID = categoryID;
        this.categoryName = categoryName;
    }

    @Generated(hash = 1150634039)
    public Category() {
    }

    public String getCategoryID() {
        return this.categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getCategoryName() {
        return this.categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public Object getPrimaryKey() {
        return getCategoryID();
    }
}
