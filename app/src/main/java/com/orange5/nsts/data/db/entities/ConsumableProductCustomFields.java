package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
  FOREIGN KEY(`ProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
  PRIMARY KEY(`CustomFieldId`)
*/
@Entity(nameInDb = "ConsumableProductCustomFields")
public class ConsumableProductCustomFields implements DatabaseEntity {

    @Id
    @SerializedName("CustomFieldId")
    @Property(nameInDb = "CustomFieldId")
    private String customFieldId;

    @SerializedName("ProductId")
    @Property(nameInDb = "ProductId")
    private String productId;

    @SerializedName("FieldName")
    @Property(nameInDb = "FieldName")
    private String fieldName;

    @SerializedName("FieldVaule")
    @Property(nameInDb = "FieldVaule")
    private String fieldVaule;

    @Generated(hash = 812822511)
    public ConsumableProductCustomFields(String customFieldId, String productId,
                                         String fieldName, String fieldVaule) {
        this.customFieldId = customFieldId;
        this.productId = productId;
        this.fieldName = fieldName;
        this.fieldVaule = fieldVaule;
    }

    @Generated(hash = 1772058090)
    public ConsumableProductCustomFields() {
    }

    public String getCustomFieldId() {
        return this.customFieldId;
    }

    public void setCustomFieldId(String customFieldId) {
        this.customFieldId = customFieldId;
    }

    public String getProductId() {
        return this.productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getFieldName() {
        return this.fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldVaule() {
        return this.fieldVaule;
    }

    public void setFieldVaule(String fieldVaule) {
        this.fieldVaule = fieldVaule;
    }

    @Override
    public Object getPrimaryKey() {
        return getCustomFieldId();
    }
}
