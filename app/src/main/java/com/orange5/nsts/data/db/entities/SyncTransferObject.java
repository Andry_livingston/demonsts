package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;
import com.orange5.nsts.sync.DataObject;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

@Entity(nameInDb = "SyncTransferObject")
public class SyncTransferObject implements DatabaseEntity {

    @Property(nameInDb = "IncrementNumber")
    @Id()
    @SerializedName("IncrementNumber")
    private long incrementNumber;

    @SerializedName("TransferObjectId")
    @Property(nameInDb = "TransferObjectId")
    private String transferObjectId;

    @SerializedName("TypeData")
    @Property(nameInDb = "TypeData")
    private String typeData;

    @SerializedName("QueryType")
    @Property(nameInDb = "QueryType")
    private String queryType;

    @SerializedName("QueryTable")
    @Property(nameInDb = "QueryTable")
    private String queryTable;

    @SerializedName("QueryKey")
    @Property(nameInDb = "QueryKey")
    private String queryKey;

    @SerializedName("QueryFields")
    @Property(nameInDb = "QueryFields")
    private String queryFields;

    @SerializedName("DateTimeSaved")
    @Property(nameInDb = "DateTimeSaved")
    private String dateTimeSaved;

    @SerializedName("Action")
    @Property(nameInDb = "Action")
    private String action;//char

    @SerializedName("IsMine")
    @Property(nameInDb = "IsMine")
    private boolean isMine;//bit

    @SerializedName("Executed")
    @Property(nameInDb = "Executed")
    private String executed;

    @SerializedName("DataObject")
    @Property(nameInDb = "DataObject")
    private byte[] data;//blob

    private transient DataObject dataObject;


    @Generated(hash = 744541941)
    public SyncTransferObject(long incrementNumber, String transferObjectId, String typeData, String queryType, String queryTable, String queryKey,
            String queryFields, String dateTimeSaved, String action, boolean isMine, String executed, byte[] data) {
        this.incrementNumber = incrementNumber;
        this.transferObjectId = transferObjectId;
        this.typeData = typeData;
        this.queryType = queryType;
        this.queryTable = queryTable;
        this.queryKey = queryKey;
        this.queryFields = queryFields;
        this.dateTimeSaved = dateTimeSaved;
        this.action = action;
        this.isMine = isMine;
        this.executed = executed;
        this.data = data;
    }

    @Generated(hash = 1705609998)
    public SyncTransferObject() {
    }

    public String getTransferObjectId() {
        return this.transferObjectId;
    }

    public String getTypeData() {
        return this.typeData;
    }

    public void setTypeData(String typeData) {
        this.typeData = typeData;
    }

    public String getQueryType() {
        return this.queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getQueryTable() {
        return this.queryTable;
    }

    public void setQueryTable(String queryTable) {
        this.queryTable = queryTable;
    }

    public String getQueryKey() {
        return this.queryKey;
    }

    public void setQueryKey(String queryKey) {
        this.queryKey = queryKey;
    }

    public String getQueryFields() {
        return this.queryFields;
    }

    public void setQueryFields(String queryFields) {
        this.queryFields = queryFields;
    }

    public String getDateTimeSaved() {
        return this.dateTimeSaved;
    }

    public void setDateTimeSaved(String dateTimeSaved) {
        this.dateTimeSaved = dateTimeSaved;
    }

    public String getAction() {
        return this.action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public boolean getIsMine() {
        return this.isMine;
    }

    public void setIsMine(boolean isMine) {
        this.isMine = isMine;
    }

    public byte[] getData() {
        return this.data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public long getIncrementNumber() {
        return incrementNumber;
    }

    public DataObject getDataObject() {
        return dataObject;
    }

    public String getExecuted() {
        return executed;
    }

    public void setExecuted(String executed) {
        this.executed = executed;
    }

    public void setDataObject(DataObject dataObject) {
        this.dataObject = dataObject;
    }

    @Override
    public Object getPrimaryKey() {
        return getIncrementNumber();
    }

    public void setTransferObjectId(String transferObjectId) {
        this.transferObjectId = transferObjectId;
    }

    public void setIncrementNumber(long incrementNumber) {
        this.incrementNumber = incrementNumber;
    }
}
