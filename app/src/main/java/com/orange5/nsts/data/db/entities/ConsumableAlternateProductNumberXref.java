package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
  FOREIGN KEY(`ProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
  PRIMARY KEY(`AlternateNumber`)
*/
@Entity(nameInDb = "ConsumableAlternateProductNumberXref")
public class ConsumableAlternateProductNumberXref implements DatabaseEntity {

    @Id
    @SerializedName("AlternateNumber")
    @Property(nameInDb = "AlternateNumber")
    private String alternateNumber;

    @SerializedName("ProductId")
    @Property(nameInDb = "ProductId")
    private String productId;

    @Generated(hash = 1469326771)
    public ConsumableAlternateProductNumberXref(String alternateNumber,
                                                String productId) {
        this.alternateNumber = alternateNumber;
        this.productId = productId;
    }

    @Generated(hash = 299381745)
    public ConsumableAlternateProductNumberXref() {
    }

    public String getAlternateNumber() {
        return this.alternateNumber;
    }

    public void setAlternateNumber(String alternateNumber) {
        this.alternateNumber = alternateNumber;
    }

    public String getProductId() {
        return this.productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Override
    public Object getPrimaryKey() {
        return getAlternateNumber();
    }
}
