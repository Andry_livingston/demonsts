package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

/*
  PRIMARY KEY(`BinId`),
  FOREIGN KEY(`BinTypeId`) REFERENCES `BinType`(`BinTypeId`)
*/
@Entity(nameInDb = "BinNumber")
public class BinNumber implements DatabaseEntity {
    public static final String BIN_TYPE_RECV = "Recv";
    public static final String BIN_TYPE_TRANSFERS = "Transfers";

    public boolean isSystemBin() {
        return !binTypeId.equals(0);
    }

    @Id
    @SerializedName("BinId")
    @Property(nameInDb = "BinId")
    private String binId;

    @SerializedName("BinNum")
    @Property(nameInDb = "BinNum")
    private String binNum;

    @SerializedName("BinTypeId")
    @Property(nameInDb = "BinTypeId")
    private Integer binTypeId;

    @SerializedName("OrdNumber")
    @Property(nameInDb = "OrdNumber")
    private Integer ordNumber;

    @Generated(hash = 2000632621)
    public BinNumber(String binId, String binNum, Integer binTypeId,
                     Integer ordNumber) {
        this.binId = binId;
        this.binNum = binNum;
        this.binTypeId = binTypeId;
        this.ordNumber = ordNumber;
    }

    @Generated(hash = 1403840)
    public BinNumber() {
    }

    public String getBinId() {
        return this.binId;
    }

    public void setBinId(String binId) {
        this.binId = binId;
    }

    public String getBinNum() {
        return this.binNum;
    }

    public void setBinNum(String binNum) {
        this.binNum = binNum;
    }

    public Integer getBinTypeId() {
        return this.binTypeId;
    }

    public void setBinTypeId(Integer binTypeId) {
        this.binTypeId = binTypeId;
    }

    public Integer getOrdNumber() {
        return this.ordNumber;
    }

    public void setOrdNumber(Integer ordNumber) {
        this.ordNumber = ordNumber;
    }

    @Override
    public String toString() {
        return binNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BinNumber binNumber = (BinNumber) o;

        if (!binId.equals(binNumber.binId)) return false;
        return binNum != null ? binNum.equals(binNumber.binNum) : binNumber.binNum == null;
    }

    @Override
    public int hashCode() {
        int result = binId.hashCode();
        result = 31 * result + (binNum != null ? binNum.hashCode() : 0);
        return result;
    }

    @Override
    public Object getPrimaryKey() {
        return getBinId();
    }
}
