package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;
import com.orange5.nsts.data.db.dao.BinNumberDao;
import com.orange5.nsts.data.db.dao.ConsumableBinDao;
import com.orange5.nsts.data.db.dao.DaoSession;
import com.orange5.nsts.util.db.GUIDFactory;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.ToOne;


/*
  FOREIGN KEY(`BinId`) REFERENCES `BinNumber`(`BinId`),
  FOREIGN KEY(`ProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
  PRIMARY KEY(`ConsumableBinId`)
*/
@Entity(nameInDb = "ConsumableBin")
public class ConsumableBin implements DatabaseEntity {

    @ToOne(joinProperty = "binId")
    private BinNumber binNumber;

    @Id
    @SerializedName("ConsumableBinId")
    @Property(nameInDb = "ConsumableBinId")
    private String consumableBinId = GUIDFactory.newUUID();

    @SerializedName("ProductId")
    @Property(nameInDb = "ProductId")
    private String productId;

    @SerializedName("Quantity")
    @Property(nameInDb = "Quantity")
    private int quantity;

    @SerializedName("AddingDate")
    @Property(nameInDb = "AddingDate")
    private String addingDate;

    @SerializedName("Allocated")
    @Property(nameInDb = "Allocated")
    private int allocated;

    @SerializedName("BinId")
    @Property(nameInDb = "BinId")
    private String binId;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 2137903846)
    private transient ConsumableBinDao myDao;

    @Generated(hash = 2070629062)
    private transient String binNumber__resolvedKey;

    @Generated(hash = 2123998551)
    public ConsumableBin(String consumableBinId, String productId, int quantity,
            String addingDate, int allocated, String binId) {
        this.consumableBinId = consumableBinId;
        this.productId = productId;
        this.quantity = quantity;
        this.addingDate = addingDate;
        this.allocated = allocated;
        this.binId = binId;
    }

    @Generated(hash = 1992696738)
    public ConsumableBin() {
    }

    public String getConsumableBinId() {
        return this.consumableBinId;
    }

    public void setConsumableBinId(String consumableBinId) {
        this.consumableBinId = consumableBinId;
    }

    public String getProductId() {
        return this.productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getAddingDate() {
        return this.addingDate;
    }

    public void setAddingDate(String addingDate) {
        this.addingDate = addingDate;
    }

    public int getAllocated() {
        return this.allocated;
    }

    public void setAllocated(int allocated) {
        this.allocated = allocated;
    }

    public String getBinId() {
        return this.binId;
    }

    public void setBinId(String binId) {
        this.binId = binId;
    }

    /** To-one relationship, resolved on first access. */
    @Generated(hash = 3807229)
    public BinNumber getBinNumber() {
        String __key = this.binId;
        if (binNumber__resolvedKey == null || binNumber__resolvedKey != __key) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            BinNumberDao targetDao = daoSession.getBinNumberDao();
            BinNumber binNumberNew = targetDao.load(__key);
            synchronized (this) {
                binNumber = binNumberNew;
                binNumber__resolvedKey = __key;
            }
        }
        return binNumber;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 474252877)
    public void setBinNumber(BinNumber binNumber) {
        synchronized (this) {
            this.binNumber = binNumber;
            binId = binNumber == null ? null : binNumber.getBinId();
            binNumber__resolvedKey = binId;
        }
    }

    @Override
    public Object getPrimaryKey() {
        return getConsumableBinId();
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    @Override
    public String toString() {
        if (getBinNumber() == null) {
            return "INVALID_BIN";
        }
        return binNumber.getBinNum();
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 224154946)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getConsumableBinDao() : null;
    }

}
