package com.orange5.nsts.data.preferencies

import android.content.SharedPreferences
import androidx.core.content.edit
import com.google.gson.Gson
import com.orange5.nsts.data.db.entities.User
import com.orange5.nsts.ui.settings.Setting
import com.orange5.nsts.ui.settings.models.ApiSettings

class RawSharedPreferences(private val preferences: SharedPreferences) {

    private val gson = Gson()

    var apiSettings: ApiSettings
        get(): ApiSettings {
            val json = preferences.getString(API_SETTINGS_KEY, "")
            return if (json.isNullOrEmpty()) ApiSettings() else gson.fromJson(json, ApiSettings::class.java)
        }
        set(value) {
            preferences.edit().putString(API_SETTINGS_KEY, gson.toJson(value)).apply()
        }

    var user: User?
        get(): User? {
            val json = preferences.getString(USER_KEY, "")
            return if (json.isNullOrEmpty()) null else gson.fromJson(json, User::class.java)
        }
        set(value) {
            preferences.edit().putString(USER_KEY, gson.toJson(value)).apply()
        }

    var isShouldRemember: Boolean
        get() = preferences.getBoolean(SHOULD_REMEMBER_USER_KEY, false)
        set(value) {
            preferences.edit { putBoolean(SHOULD_REMEMBER_USER_KEY, value) }
        }

    var shouldShowRentals: Boolean
        get() = preferences.getBoolean(SHOW_RENTALS_KEY, true)
        set(value) {
            preferences.edit().putBoolean(SHOW_RENTALS_KEY, value).apply()
        }

    var shouldShowConsumables: Boolean
        get() = preferences.getBoolean(SHOW_CONSUMABLE_KEY, true)
        set(value) {
            preferences.edit().putBoolean(SHOW_CONSUMABLE_KEY, value).apply()
        }

    var shouldShowZeroQuantity: Boolean
        get() = preferences.getBoolean(SHOW_ZERO_CONSUMABLE_KEY, false)
        set(value) {
            preferences.edit().putBoolean(SHOW_ZERO_CONSUMABLE_KEY, value).apply()
        }

    var shouldShowOnlyMultiBin: Boolean
        get() = preferences.getBoolean(SHOW_ONLY_MULTI_BIN_KEY, false)
        set(value) {
            preferences.edit().putBoolean(SHOW_ONLY_MULTI_BIN_KEY, value).apply()
        }


    var isDbExists: Boolean
        get() = preferences.getBoolean(DATA_BASE_EXISTS_KEY, false)
        set(value) {
            preferences.edit().putBoolean(DATA_BASE_EXISTS_KEY, value).apply()
        }

    var isSQLDebugEnable: Boolean
        get() = preferences.getBoolean(SQL_DEBUG_KEY, false)
        set(value) {
            preferences.edit().putBoolean(SQL_DEBUG_KEY, value).apply()
        }

    var isAccountingCodeRequired: Boolean
        get() = preferences.getBoolean(ACCOUNTING_CODE_IS_REQUIRED_KEY, false)
        set(value) {
            preferences.edit().putBoolean(ACCOUNTING_CODE_IS_REQUIRED_KEY, value).apply()
        }

    var isHideQuickReturn: Boolean
        get() = preferences.getBoolean(HIDE_QUICK_RETURN_KEY, false)
        set(value) {
            preferences.edit().putBoolean(HIDE_QUICK_RETURN_KEY, value).apply()
        }

    var isHideQuickOrder: Boolean
        get() = preferences.getBoolean(HIDE_QUICK_ORDER_KEY, false)
        set(value) {
            preferences.edit().putBoolean(HIDE_QUICK_ORDER_KEY, value).apply()
        }

    var isScanVibrationEnabled: Boolean
        get() = preferences.getBoolean(VIBRATION_KEY, false)
        set(value) {
            preferences.edit { putBoolean(VIBRATION_KEY, value) }
        }

    var isStressTestEnabled: Boolean
        get() = preferences.getBoolean(STRESS_TEST_ENABLED, false)
        set(value) {
            preferences.edit { putBoolean(STRESS_TEST_ENABLED, value) }
        }

    fun isSettingChecked(setting: Setting): Boolean =
            when (setting) {
                Setting.EXPORT_DB_FILE -> isSQLDebugEnable
                Setting.ACCOUNTING_CODE_REQUIRED -> isAccountingCodeRequired
                Setting.HIDE_QUICK_ORDER -> isHideQuickOrder
                Setting.HIDE_QUICK_RETURN -> isHideQuickReturn
                Setting.STRESS_TES_ENABLED -> isStressTestEnabled
                else -> isScanVibrationEnabled
            }

    companion object {
        @JvmStatic
        fun instance(sharedPreferences: SharedPreferences): RawSharedPreferences {
            return instance
                    ?: synchronized(this) { RawSharedPreferences(sharedPreferences).also { instance = it } }
        }

        private var instance: RawSharedPreferences? = null

        const val API_SETTINGS_KEY = "api_settings_key"
        private const val USER_KEY = "user_key"
        private const val SHOULD_REMEMBER_USER_KEY = "should_remember_user"
        private const val SHOW_RENTALS_KEY = "show_rentals"
        private const val SHOW_CONSUMABLE_KEY = "show_consumables"
        private const val SHOW_ZERO_CONSUMABLE_KEY = "show_zero_quantity"
        private const val SHOW_ONLY_MULTI_BIN_KEY = "show_only_multi_bin"
        private const val DATA_BASE_EXISTS_KEY = "db_base_exists_key"
        private const val SQL_DEBUG_KEY = "sql_debug_key"
        private const val VIBRATION_KEY = "vibration_key"
        private const val HIDE_QUICK_ORDER_KEY = "hide_quick_order"
        private const val HIDE_QUICK_RETURN_KEY = "hide_quick_return"
        private const val ACCOUNTING_CODE_IS_REQUIRED_KEY = "accounting_code_required_key"
        private const val STRESS_TEST_ENABLED = "stress_test_enabled_key"
    }
}
