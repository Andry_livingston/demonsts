package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
PRIMARY KEY(`ReasonCodeId`)
*/
@Entity(nameInDb = "InventoryAdjustmentReasonCode")
public class InventoryAdjustmentReasonCode implements DatabaseEntity {

    @Id
    @SerializedName("ReasonCodeId")
    @Property(nameInDb = "ReasonCodeId")
    private String reasonCodeId;

    @SerializedName("ReasonCodeValue")
    @Property(nameInDb = "ReasonCodeValue")
    private String reasonCodeValue;

    @Generated(hash = 1656418246)
    public InventoryAdjustmentReasonCode(String reasonCodeId,
                                         String reasonCodeValue) {
        this.reasonCodeId = reasonCodeId;
        this.reasonCodeValue = reasonCodeValue;
    }

    @Generated(hash = 1645708505)
    public InventoryAdjustmentReasonCode() {
    }

    public String getReasonCodeId() {
        return this.reasonCodeId;
    }

    public void setReasonCodeId(String reasonCodeId) {
        this.reasonCodeId = reasonCodeId;
    }

    public String getReasonCodeValue() {
        return this.reasonCodeValue;
    }

    public void setReasonCodeValue(String reasonCodeValue) {
        this.reasonCodeValue = reasonCodeValue;
    }

    @Override
    public Object getPrimaryKey() {
        return getReasonCodeId();
    }
}
