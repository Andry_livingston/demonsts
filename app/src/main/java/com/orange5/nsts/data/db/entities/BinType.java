package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

/*
  PRIMARY KEY(`BinTypeId`)
*/
@Entity(nameInDb = "BinType")
public class BinType implements DatabaseEntity {

    @Id
    @SerializedName("BinTypeId")
    @Property(nameInDb = "BinTypeId")
    private long binTypeId;

    @SerializedName("BinTypeName")
    @Property(nameInDb = "BinTypeName")
    private String binTypeName;

    @Generated(hash = 1596769951)
    public BinType(long binTypeId, String binTypeName) {
        this.binTypeId = binTypeId;
        this.binTypeName = binTypeName;
    }

    @Generated(hash = 2025438009)
    public BinType() {
    }

    public long getBinTypeId() {
        return this.binTypeId;
    }

    public void setBinTypeId(long binTypeId) {
        this.binTypeId = binTypeId;
    }

    public String getBinTypeName() {
        return this.binTypeName;
    }

    public void setBinTypeName(String binTypeName) {
        this.binTypeName = binTypeName;
    }

    @Override
    public Object getPrimaryKey() {
        return getBinTypeId();
    }
}
