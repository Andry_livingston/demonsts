package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
PRIMARY KEY(`Id`)
*/
@Entity(nameInDb = "Role")
public class Role implements DatabaseEntity {

    @Id
    @SerializedName("Id")
    @Property(nameInDb = "Id")
    private String id;

    @SerializedName("Name")
    @Property(nameInDb = "Name")
    private String name;

    @Generated(hash = 1016345793)
    public Role(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Generated(hash = 844947497)
    public Role() {
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Object getPrimaryKey() {
        return getId();
    }
}
