package com.orange5.nsts.data.db.dao;

import java.util.Map;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.AbstractDaoSession;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.identityscope.IdentityScopeType;
import org.greenrobot.greendao.internal.DaoConfig;

import com.orange5.nsts.data.db.entities.AccessoryCrossReference;
import com.orange5.nsts.data.db.entities.AccountingCode;
import com.orange5.nsts.data.db.entities.AppState;
import com.orange5.nsts.data.db.entities.BinNumber;
import com.orange5.nsts.data.db.entities.BinType;
import com.orange5.nsts.data.db.entities.Category;
import com.orange5.nsts.data.db.entities.ConsumableAlternateProductNumberXref;
import com.orange5.nsts.data.db.entities.ConsumableBin;
import com.orange5.nsts.data.db.entities.ConsumableProduct;
import com.orange5.nsts.data.db.entities.ConsumableProduct2Tag;
import com.orange5.nsts.data.db.entities.ConsumableProductBinHistory;
import com.orange5.nsts.data.db.entities.ConsumableProductCustomFields;
import com.orange5.nsts.data.db.entities.Customer;
import com.orange5.nsts.data.db.entities.Customer2Role;
import com.orange5.nsts.data.db.entities.CustomerCustomFields;
import com.orange5.nsts.data.db.entities.CustomerCustomFieldsTypes;
import com.orange5.nsts.data.db.entities.CustomerPosition;
import com.orange5.nsts.data.db.entities.CustomerProductNumberXref;
import com.orange5.nsts.data.db.entities.CustomerRole;
import com.orange5.nsts.data.db.entities.CustomerStatus;
import com.orange5.nsts.data.db.entities.Discrepancies;
import com.orange5.nsts.data.db.entities.DiscrepanciesResolvedStatus;
import com.orange5.nsts.data.db.entities.DiscrepanciesType;
import com.orange5.nsts.data.db.entities.DisplayColumnName;
import com.orange5.nsts.data.db.entities.Error;
import com.orange5.nsts.data.db.entities.InventoryAdjustment;
import com.orange5.nsts.data.db.entities.InventoryAdjustmentReasonCode;
import com.orange5.nsts.data.db.entities.Order;
import com.orange5.nsts.data.db.entities.OrderConsumableItem;
import com.orange5.nsts.data.db.entities.OrderConsumableReturn;
import com.orange5.nsts.data.db.entities.OrderConsumableReturnItemByCustomer;
import com.orange5.nsts.data.db.entities.OrderRentalItem;
import com.orange5.nsts.data.db.entities.OrderStatus;
import com.orange5.nsts.data.db.entities.Parameters;
import com.orange5.nsts.data.db.entities.PickTicket;
import com.orange5.nsts.data.db.entities.ProductReceivingLog;
import com.orange5.nsts.data.db.entities.RentalProduct;
import com.orange5.nsts.data.db.entities.RentalProduct2Tag;
import com.orange5.nsts.data.db.entities.RentalProductBinHistory;
import com.orange5.nsts.data.db.entities.RentalProductCustomFields;
import com.orange5.nsts.data.db.entities.RentalProductItem;
import com.orange5.nsts.data.db.entities.RentalProductStatus;
import com.orange5.nsts.data.db.entities.RentalProductStatusHistory;
import com.orange5.nsts.data.db.entities.RentalReturn;
import com.orange5.nsts.data.db.entities.Role;
import com.orange5.nsts.data.db.entities.ShippingNotice;
import com.orange5.nsts.data.db.entities.SyncTransferObject;
import com.orange5.nsts.data.db.entities.Tag;
import com.orange5.nsts.data.db.entities.Tag2CustomerRole;
import com.orange5.nsts.data.db.entities.TransactionCustomFields;
import com.orange5.nsts.data.db.entities.TransactionHistory;
import com.orange5.nsts.data.db.entities.TransactionType;
import com.orange5.nsts.data.db.entities.Transfer;
import com.orange5.nsts.data.db.entities.TransferCarrier;
import com.orange5.nsts.data.db.entities.TransferItem;
import com.orange5.nsts.data.db.entities.TransferStatus;
import com.orange5.nsts.data.db.entities.User;
import com.orange5.nsts.data.db.entities.User2Role;

import com.orange5.nsts.data.db.dao.AccessoryCrossReferenceDao;
import com.orange5.nsts.data.db.dao.AccountingCodeDao;
import com.orange5.nsts.data.db.dao.AppStateDao;
import com.orange5.nsts.data.db.dao.BinNumberDao;
import com.orange5.nsts.data.db.dao.BinTypeDao;
import com.orange5.nsts.data.db.dao.CategoryDao;
import com.orange5.nsts.data.db.dao.ConsumableAlternateProductNumberXrefDao;
import com.orange5.nsts.data.db.dao.ConsumableBinDao;
import com.orange5.nsts.data.db.dao.ConsumableProductDao;
import com.orange5.nsts.data.db.dao.ConsumableProduct2TagDao;
import com.orange5.nsts.data.db.dao.ConsumableProductBinHistoryDao;
import com.orange5.nsts.data.db.dao.ConsumableProductCustomFieldsDao;
import com.orange5.nsts.data.db.dao.CustomerDao;
import com.orange5.nsts.data.db.dao.Customer2RoleDao;
import com.orange5.nsts.data.db.dao.CustomerCustomFieldsDao;
import com.orange5.nsts.data.db.dao.CustomerCustomFieldsTypesDao;
import com.orange5.nsts.data.db.dao.CustomerPositionDao;
import com.orange5.nsts.data.db.dao.CustomerProductNumberXrefDao;
import com.orange5.nsts.data.db.dao.CustomerRoleDao;
import com.orange5.nsts.data.db.dao.CustomerStatusDao;
import com.orange5.nsts.data.db.dao.DiscrepanciesDao;
import com.orange5.nsts.data.db.dao.DiscrepanciesResolvedStatusDao;
import com.orange5.nsts.data.db.dao.DiscrepanciesTypeDao;
import com.orange5.nsts.data.db.dao.DisplayColumnNameDao;
import com.orange5.nsts.data.db.dao.ErrorDao;
import com.orange5.nsts.data.db.dao.InventoryAdjustmentDao;
import com.orange5.nsts.data.db.dao.InventoryAdjustmentReasonCodeDao;
import com.orange5.nsts.data.db.dao.OrderDao;
import com.orange5.nsts.data.db.dao.OrderConsumableItemDao;
import com.orange5.nsts.data.db.dao.OrderConsumableReturnDao;
import com.orange5.nsts.data.db.dao.OrderConsumableReturnItemByCustomerDao;
import com.orange5.nsts.data.db.dao.OrderRentalItemDao;
import com.orange5.nsts.data.db.dao.OrderStatusDao;
import com.orange5.nsts.data.db.dao.ParametersDao;
import com.orange5.nsts.data.db.dao.PickTicketDao;
import com.orange5.nsts.data.db.dao.ProductReceivingLogDao;
import com.orange5.nsts.data.db.dao.RentalProductDao;
import com.orange5.nsts.data.db.dao.RentalProduct2TagDao;
import com.orange5.nsts.data.db.dao.RentalProductBinHistoryDao;
import com.orange5.nsts.data.db.dao.RentalProductCustomFieldsDao;
import com.orange5.nsts.data.db.dao.RentalProductItemDao;
import com.orange5.nsts.data.db.dao.RentalProductStatusDao;
import com.orange5.nsts.data.db.dao.RentalProductStatusHistoryDao;
import com.orange5.nsts.data.db.dao.RentalReturnDao;
import com.orange5.nsts.data.db.dao.RoleDao;
import com.orange5.nsts.data.db.dao.ShippingNoticeDao;
import com.orange5.nsts.data.db.dao.SyncTransferObjectDao;
import com.orange5.nsts.data.db.dao.TagDao;
import com.orange5.nsts.data.db.dao.Tag2CustomerRoleDao;
import com.orange5.nsts.data.db.dao.TransactionCustomFieldsDao;
import com.orange5.nsts.data.db.dao.TransactionHistoryDao;
import com.orange5.nsts.data.db.dao.TransactionTypeDao;
import com.orange5.nsts.data.db.dao.TransferDao;
import com.orange5.nsts.data.db.dao.TransferCarrierDao;
import com.orange5.nsts.data.db.dao.TransferItemDao;
import com.orange5.nsts.data.db.dao.TransferStatusDao;
import com.orange5.nsts.data.db.dao.UserDao;
import com.orange5.nsts.data.db.dao.User2RoleDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see org.greenrobot.greendao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig accessoryCrossReferenceDaoConfig;
    private final DaoConfig accountingCodeDaoConfig;
    private final DaoConfig appStateDaoConfig;
    private final DaoConfig binNumberDaoConfig;
    private final DaoConfig binTypeDaoConfig;
    private final DaoConfig categoryDaoConfig;
    private final DaoConfig consumableAlternateProductNumberXrefDaoConfig;
    private final DaoConfig consumableBinDaoConfig;
    private final DaoConfig consumableProductDaoConfig;
    private final DaoConfig consumableProduct2TagDaoConfig;
    private final DaoConfig consumableProductBinHistoryDaoConfig;
    private final DaoConfig consumableProductCustomFieldsDaoConfig;
    private final DaoConfig customerDaoConfig;
    private final DaoConfig customer2RoleDaoConfig;
    private final DaoConfig customerCustomFieldsDaoConfig;
    private final DaoConfig customerCustomFieldsTypesDaoConfig;
    private final DaoConfig customerPositionDaoConfig;
    private final DaoConfig customerProductNumberXrefDaoConfig;
    private final DaoConfig customerRoleDaoConfig;
    private final DaoConfig customerStatusDaoConfig;
    private final DaoConfig discrepanciesDaoConfig;
    private final DaoConfig discrepanciesResolvedStatusDaoConfig;
    private final DaoConfig discrepanciesTypeDaoConfig;
    private final DaoConfig displayColumnNameDaoConfig;
    private final DaoConfig errorDaoConfig;
    private final DaoConfig inventoryAdjustmentDaoConfig;
    private final DaoConfig inventoryAdjustmentReasonCodeDaoConfig;
    private final DaoConfig orderDaoConfig;
    private final DaoConfig orderConsumableItemDaoConfig;
    private final DaoConfig orderConsumableReturnDaoConfig;
    private final DaoConfig orderConsumableReturnItemByCustomerDaoConfig;
    private final DaoConfig orderRentalItemDaoConfig;
    private final DaoConfig orderStatusDaoConfig;
    private final DaoConfig parametersDaoConfig;
    private final DaoConfig pickTicketDaoConfig;
    private final DaoConfig productReceivingLogDaoConfig;
    private final DaoConfig rentalProductDaoConfig;
    private final DaoConfig rentalProduct2TagDaoConfig;
    private final DaoConfig rentalProductBinHistoryDaoConfig;
    private final DaoConfig rentalProductCustomFieldsDaoConfig;
    private final DaoConfig rentalProductItemDaoConfig;
    private final DaoConfig rentalProductStatusDaoConfig;
    private final DaoConfig rentalProductStatusHistoryDaoConfig;
    private final DaoConfig rentalReturnDaoConfig;
    private final DaoConfig roleDaoConfig;
    private final DaoConfig shippingNoticeDaoConfig;
    private final DaoConfig syncTransferObjectDaoConfig;
    private final DaoConfig tagDaoConfig;
    private final DaoConfig tag2CustomerRoleDaoConfig;
    private final DaoConfig transactionCustomFieldsDaoConfig;
    private final DaoConfig transactionHistoryDaoConfig;
    private final DaoConfig transactionTypeDaoConfig;
    private final DaoConfig transferDaoConfig;
    private final DaoConfig transferCarrierDaoConfig;
    private final DaoConfig transferItemDaoConfig;
    private final DaoConfig transferStatusDaoConfig;
    private final DaoConfig userDaoConfig;
    private final DaoConfig user2RoleDaoConfig;

    private final AccessoryCrossReferenceDao accessoryCrossReferenceDao;
    private final AccountingCodeDao accountingCodeDao;
    private final AppStateDao appStateDao;
    private final BinNumberDao binNumberDao;
    private final BinTypeDao binTypeDao;
    private final CategoryDao categoryDao;
    private final ConsumableAlternateProductNumberXrefDao consumableAlternateProductNumberXrefDao;
    private final ConsumableBinDao consumableBinDao;
    private final ConsumableProductDao consumableProductDao;
    private final ConsumableProduct2TagDao consumableProduct2TagDao;
    private final ConsumableProductBinHistoryDao consumableProductBinHistoryDao;
    private final ConsumableProductCustomFieldsDao consumableProductCustomFieldsDao;
    private final CustomerDao customerDao;
    private final Customer2RoleDao customer2RoleDao;
    private final CustomerCustomFieldsDao customerCustomFieldsDao;
    private final CustomerCustomFieldsTypesDao customerCustomFieldsTypesDao;
    private final CustomerPositionDao customerPositionDao;
    private final CustomerProductNumberXrefDao customerProductNumberXrefDao;
    private final CustomerRoleDao customerRoleDao;
    private final CustomerStatusDao customerStatusDao;
    private final DiscrepanciesDao discrepanciesDao;
    private final DiscrepanciesResolvedStatusDao discrepanciesResolvedStatusDao;
    private final DiscrepanciesTypeDao discrepanciesTypeDao;
    private final DisplayColumnNameDao displayColumnNameDao;
    private final ErrorDao errorDao;
    private final InventoryAdjustmentDao inventoryAdjustmentDao;
    private final InventoryAdjustmentReasonCodeDao inventoryAdjustmentReasonCodeDao;
    private final OrderDao orderDao;
    private final OrderConsumableItemDao orderConsumableItemDao;
    private final OrderConsumableReturnDao orderConsumableReturnDao;
    private final OrderConsumableReturnItemByCustomerDao orderConsumableReturnItemByCustomerDao;
    private final OrderRentalItemDao orderRentalItemDao;
    private final OrderStatusDao orderStatusDao;
    private final ParametersDao parametersDao;
    private final PickTicketDao pickTicketDao;
    private final ProductReceivingLogDao productReceivingLogDao;
    private final RentalProductDao rentalProductDao;
    private final RentalProduct2TagDao rentalProduct2TagDao;
    private final RentalProductBinHistoryDao rentalProductBinHistoryDao;
    private final RentalProductCustomFieldsDao rentalProductCustomFieldsDao;
    private final RentalProductItemDao rentalProductItemDao;
    private final RentalProductStatusDao rentalProductStatusDao;
    private final RentalProductStatusHistoryDao rentalProductStatusHistoryDao;
    private final RentalReturnDao rentalReturnDao;
    private final RoleDao roleDao;
    private final ShippingNoticeDao shippingNoticeDao;
    private final SyncTransferObjectDao syncTransferObjectDao;
    private final TagDao tagDao;
    private final Tag2CustomerRoleDao tag2CustomerRoleDao;
    private final TransactionCustomFieldsDao transactionCustomFieldsDao;
    private final TransactionHistoryDao transactionHistoryDao;
    private final TransactionTypeDao transactionTypeDao;
    private final TransferDao transferDao;
    private final TransferCarrierDao transferCarrierDao;
    private final TransferItemDao transferItemDao;
    private final TransferStatusDao transferStatusDao;
    private final UserDao userDao;
    private final User2RoleDao user2RoleDao;

    public DaoSession(Database db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        accessoryCrossReferenceDaoConfig = daoConfigMap.get(AccessoryCrossReferenceDao.class).clone();
        accessoryCrossReferenceDaoConfig.initIdentityScope(type);

        accountingCodeDaoConfig = daoConfigMap.get(AccountingCodeDao.class).clone();
        accountingCodeDaoConfig.initIdentityScope(type);

        appStateDaoConfig = daoConfigMap.get(AppStateDao.class).clone();
        appStateDaoConfig.initIdentityScope(type);

        binNumberDaoConfig = daoConfigMap.get(BinNumberDao.class).clone();
        binNumberDaoConfig.initIdentityScope(type);

        binTypeDaoConfig = daoConfigMap.get(BinTypeDao.class).clone();
        binTypeDaoConfig.initIdentityScope(type);

        categoryDaoConfig = daoConfigMap.get(CategoryDao.class).clone();
        categoryDaoConfig.initIdentityScope(type);

        consumableAlternateProductNumberXrefDaoConfig = daoConfigMap.get(ConsumableAlternateProductNumberXrefDao.class).clone();
        consumableAlternateProductNumberXrefDaoConfig.initIdentityScope(type);

        consumableBinDaoConfig = daoConfigMap.get(ConsumableBinDao.class).clone();
        consumableBinDaoConfig.initIdentityScope(type);

        consumableProductDaoConfig = daoConfigMap.get(ConsumableProductDao.class).clone();
        consumableProductDaoConfig.initIdentityScope(type);

        consumableProduct2TagDaoConfig = daoConfigMap.get(ConsumableProduct2TagDao.class).clone();
        consumableProduct2TagDaoConfig.initIdentityScope(type);

        consumableProductBinHistoryDaoConfig = daoConfigMap.get(ConsumableProductBinHistoryDao.class).clone();
        consumableProductBinHistoryDaoConfig.initIdentityScope(type);

        consumableProductCustomFieldsDaoConfig = daoConfigMap.get(ConsumableProductCustomFieldsDao.class).clone();
        consumableProductCustomFieldsDaoConfig.initIdentityScope(type);

        customerDaoConfig = daoConfigMap.get(CustomerDao.class).clone();
        customerDaoConfig.initIdentityScope(type);

        customer2RoleDaoConfig = daoConfigMap.get(Customer2RoleDao.class).clone();
        customer2RoleDaoConfig.initIdentityScope(type);

        customerCustomFieldsDaoConfig = daoConfigMap.get(CustomerCustomFieldsDao.class).clone();
        customerCustomFieldsDaoConfig.initIdentityScope(type);

        customerCustomFieldsTypesDaoConfig = daoConfigMap.get(CustomerCustomFieldsTypesDao.class).clone();
        customerCustomFieldsTypesDaoConfig.initIdentityScope(type);

        customerPositionDaoConfig = daoConfigMap.get(CustomerPositionDao.class).clone();
        customerPositionDaoConfig.initIdentityScope(type);

        customerProductNumberXrefDaoConfig = daoConfigMap.get(CustomerProductNumberXrefDao.class).clone();
        customerProductNumberXrefDaoConfig.initIdentityScope(type);

        customerRoleDaoConfig = daoConfigMap.get(CustomerRoleDao.class).clone();
        customerRoleDaoConfig.initIdentityScope(type);

        customerStatusDaoConfig = daoConfigMap.get(CustomerStatusDao.class).clone();
        customerStatusDaoConfig.initIdentityScope(type);

        discrepanciesDaoConfig = daoConfigMap.get(DiscrepanciesDao.class).clone();
        discrepanciesDaoConfig.initIdentityScope(type);

        discrepanciesResolvedStatusDaoConfig = daoConfigMap.get(DiscrepanciesResolvedStatusDao.class).clone();
        discrepanciesResolvedStatusDaoConfig.initIdentityScope(type);

        discrepanciesTypeDaoConfig = daoConfigMap.get(DiscrepanciesTypeDao.class).clone();
        discrepanciesTypeDaoConfig.initIdentityScope(type);

        displayColumnNameDaoConfig = daoConfigMap.get(DisplayColumnNameDao.class).clone();
        displayColumnNameDaoConfig.initIdentityScope(type);

        errorDaoConfig = daoConfigMap.get(ErrorDao.class).clone();
        errorDaoConfig.initIdentityScope(type);

        inventoryAdjustmentDaoConfig = daoConfigMap.get(InventoryAdjustmentDao.class).clone();
        inventoryAdjustmentDaoConfig.initIdentityScope(type);

        inventoryAdjustmentReasonCodeDaoConfig = daoConfigMap.get(InventoryAdjustmentReasonCodeDao.class).clone();
        inventoryAdjustmentReasonCodeDaoConfig.initIdentityScope(type);

        orderDaoConfig = daoConfigMap.get(OrderDao.class).clone();
        orderDaoConfig.initIdentityScope(type);

        orderConsumableItemDaoConfig = daoConfigMap.get(OrderConsumableItemDao.class).clone();
        orderConsumableItemDaoConfig.initIdentityScope(type);

        orderConsumableReturnDaoConfig = daoConfigMap.get(OrderConsumableReturnDao.class).clone();
        orderConsumableReturnDaoConfig.initIdentityScope(type);

        orderConsumableReturnItemByCustomerDaoConfig = daoConfigMap.get(OrderConsumableReturnItemByCustomerDao.class).clone();
        orderConsumableReturnItemByCustomerDaoConfig.initIdentityScope(type);

        orderRentalItemDaoConfig = daoConfigMap.get(OrderRentalItemDao.class).clone();
        orderRentalItemDaoConfig.initIdentityScope(type);

        orderStatusDaoConfig = daoConfigMap.get(OrderStatusDao.class).clone();
        orderStatusDaoConfig.initIdentityScope(type);

        parametersDaoConfig = daoConfigMap.get(ParametersDao.class).clone();
        parametersDaoConfig.initIdentityScope(type);

        pickTicketDaoConfig = daoConfigMap.get(PickTicketDao.class).clone();
        pickTicketDaoConfig.initIdentityScope(type);

        productReceivingLogDaoConfig = daoConfigMap.get(ProductReceivingLogDao.class).clone();
        productReceivingLogDaoConfig.initIdentityScope(type);

        rentalProductDaoConfig = daoConfigMap.get(RentalProductDao.class).clone();
        rentalProductDaoConfig.initIdentityScope(type);

        rentalProduct2TagDaoConfig = daoConfigMap.get(RentalProduct2TagDao.class).clone();
        rentalProduct2TagDaoConfig.initIdentityScope(type);

        rentalProductBinHistoryDaoConfig = daoConfigMap.get(RentalProductBinHistoryDao.class).clone();
        rentalProductBinHistoryDaoConfig.initIdentityScope(type);

        rentalProductCustomFieldsDaoConfig = daoConfigMap.get(RentalProductCustomFieldsDao.class).clone();
        rentalProductCustomFieldsDaoConfig.initIdentityScope(type);

        rentalProductItemDaoConfig = daoConfigMap.get(RentalProductItemDao.class).clone();
        rentalProductItemDaoConfig.initIdentityScope(type);

        rentalProductStatusDaoConfig = daoConfigMap.get(RentalProductStatusDao.class).clone();
        rentalProductStatusDaoConfig.initIdentityScope(type);

        rentalProductStatusHistoryDaoConfig = daoConfigMap.get(RentalProductStatusHistoryDao.class).clone();
        rentalProductStatusHistoryDaoConfig.initIdentityScope(type);

        rentalReturnDaoConfig = daoConfigMap.get(RentalReturnDao.class).clone();
        rentalReturnDaoConfig.initIdentityScope(type);

        roleDaoConfig = daoConfigMap.get(RoleDao.class).clone();
        roleDaoConfig.initIdentityScope(type);

        shippingNoticeDaoConfig = daoConfigMap.get(ShippingNoticeDao.class).clone();
        shippingNoticeDaoConfig.initIdentityScope(type);

        syncTransferObjectDaoConfig = daoConfigMap.get(SyncTransferObjectDao.class).clone();
        syncTransferObjectDaoConfig.initIdentityScope(type);

        tagDaoConfig = daoConfigMap.get(TagDao.class).clone();
        tagDaoConfig.initIdentityScope(type);

        tag2CustomerRoleDaoConfig = daoConfigMap.get(Tag2CustomerRoleDao.class).clone();
        tag2CustomerRoleDaoConfig.initIdentityScope(type);

        transactionCustomFieldsDaoConfig = daoConfigMap.get(TransactionCustomFieldsDao.class).clone();
        transactionCustomFieldsDaoConfig.initIdentityScope(type);

        transactionHistoryDaoConfig = daoConfigMap.get(TransactionHistoryDao.class).clone();
        transactionHistoryDaoConfig.initIdentityScope(type);

        transactionTypeDaoConfig = daoConfigMap.get(TransactionTypeDao.class).clone();
        transactionTypeDaoConfig.initIdentityScope(type);

        transferDaoConfig = daoConfigMap.get(TransferDao.class).clone();
        transferDaoConfig.initIdentityScope(type);

        transferCarrierDaoConfig = daoConfigMap.get(TransferCarrierDao.class).clone();
        transferCarrierDaoConfig.initIdentityScope(type);

        transferItemDaoConfig = daoConfigMap.get(TransferItemDao.class).clone();
        transferItemDaoConfig.initIdentityScope(type);

        transferStatusDaoConfig = daoConfigMap.get(TransferStatusDao.class).clone();
        transferStatusDaoConfig.initIdentityScope(type);

        userDaoConfig = daoConfigMap.get(UserDao.class).clone();
        userDaoConfig.initIdentityScope(type);

        user2RoleDaoConfig = daoConfigMap.get(User2RoleDao.class).clone();
        user2RoleDaoConfig.initIdentityScope(type);

        accessoryCrossReferenceDao = new AccessoryCrossReferenceDao(accessoryCrossReferenceDaoConfig, this);
        accountingCodeDao = new AccountingCodeDao(accountingCodeDaoConfig, this);
        appStateDao = new AppStateDao(appStateDaoConfig, this);
        binNumberDao = new BinNumberDao(binNumberDaoConfig, this);
        binTypeDao = new BinTypeDao(binTypeDaoConfig, this);
        categoryDao = new CategoryDao(categoryDaoConfig, this);
        consumableAlternateProductNumberXrefDao = new ConsumableAlternateProductNumberXrefDao(consumableAlternateProductNumberXrefDaoConfig, this);
        consumableBinDao = new ConsumableBinDao(consumableBinDaoConfig, this);
        consumableProductDao = new ConsumableProductDao(consumableProductDaoConfig, this);
        consumableProduct2TagDao = new ConsumableProduct2TagDao(consumableProduct2TagDaoConfig, this);
        consumableProductBinHistoryDao = new ConsumableProductBinHistoryDao(consumableProductBinHistoryDaoConfig, this);
        consumableProductCustomFieldsDao = new ConsumableProductCustomFieldsDao(consumableProductCustomFieldsDaoConfig, this);
        customerDao = new CustomerDao(customerDaoConfig, this);
        customer2RoleDao = new Customer2RoleDao(customer2RoleDaoConfig, this);
        customerCustomFieldsDao = new CustomerCustomFieldsDao(customerCustomFieldsDaoConfig, this);
        customerCustomFieldsTypesDao = new CustomerCustomFieldsTypesDao(customerCustomFieldsTypesDaoConfig, this);
        customerPositionDao = new CustomerPositionDao(customerPositionDaoConfig, this);
        customerProductNumberXrefDao = new CustomerProductNumberXrefDao(customerProductNumberXrefDaoConfig, this);
        customerRoleDao = new CustomerRoleDao(customerRoleDaoConfig, this);
        customerStatusDao = new CustomerStatusDao(customerStatusDaoConfig, this);
        discrepanciesDao = new DiscrepanciesDao(discrepanciesDaoConfig, this);
        discrepanciesResolvedStatusDao = new DiscrepanciesResolvedStatusDao(discrepanciesResolvedStatusDaoConfig, this);
        discrepanciesTypeDao = new DiscrepanciesTypeDao(discrepanciesTypeDaoConfig, this);
        displayColumnNameDao = new DisplayColumnNameDao(displayColumnNameDaoConfig, this);
        errorDao = new ErrorDao(errorDaoConfig, this);
        inventoryAdjustmentDao = new InventoryAdjustmentDao(inventoryAdjustmentDaoConfig, this);
        inventoryAdjustmentReasonCodeDao = new InventoryAdjustmentReasonCodeDao(inventoryAdjustmentReasonCodeDaoConfig, this);
        orderDao = new OrderDao(orderDaoConfig, this);
        orderConsumableItemDao = new OrderConsumableItemDao(orderConsumableItemDaoConfig, this);
        orderConsumableReturnDao = new OrderConsumableReturnDao(orderConsumableReturnDaoConfig, this);
        orderConsumableReturnItemByCustomerDao = new OrderConsumableReturnItemByCustomerDao(orderConsumableReturnItemByCustomerDaoConfig, this);
        orderRentalItemDao = new OrderRentalItemDao(orderRentalItemDaoConfig, this);
        orderStatusDao = new OrderStatusDao(orderStatusDaoConfig, this);
        parametersDao = new ParametersDao(parametersDaoConfig, this);
        pickTicketDao = new PickTicketDao(pickTicketDaoConfig, this);
        productReceivingLogDao = new ProductReceivingLogDao(productReceivingLogDaoConfig, this);
        rentalProductDao = new RentalProductDao(rentalProductDaoConfig, this);
        rentalProduct2TagDao = new RentalProduct2TagDao(rentalProduct2TagDaoConfig, this);
        rentalProductBinHistoryDao = new RentalProductBinHistoryDao(rentalProductBinHistoryDaoConfig, this);
        rentalProductCustomFieldsDao = new RentalProductCustomFieldsDao(rentalProductCustomFieldsDaoConfig, this);
        rentalProductItemDao = new RentalProductItemDao(rentalProductItemDaoConfig, this);
        rentalProductStatusDao = new RentalProductStatusDao(rentalProductStatusDaoConfig, this);
        rentalProductStatusHistoryDao = new RentalProductStatusHistoryDao(rentalProductStatusHistoryDaoConfig, this);
        rentalReturnDao = new RentalReturnDao(rentalReturnDaoConfig, this);
        roleDao = new RoleDao(roleDaoConfig, this);
        shippingNoticeDao = new ShippingNoticeDao(shippingNoticeDaoConfig, this);
        syncTransferObjectDao = new SyncTransferObjectDao(syncTransferObjectDaoConfig, this);
        tagDao = new TagDao(tagDaoConfig, this);
        tag2CustomerRoleDao = new Tag2CustomerRoleDao(tag2CustomerRoleDaoConfig, this);
        transactionCustomFieldsDao = new TransactionCustomFieldsDao(transactionCustomFieldsDaoConfig, this);
        transactionHistoryDao = new TransactionHistoryDao(transactionHistoryDaoConfig, this);
        transactionTypeDao = new TransactionTypeDao(transactionTypeDaoConfig, this);
        transferDao = new TransferDao(transferDaoConfig, this);
        transferCarrierDao = new TransferCarrierDao(transferCarrierDaoConfig, this);
        transferItemDao = new TransferItemDao(transferItemDaoConfig, this);
        transferStatusDao = new TransferStatusDao(transferStatusDaoConfig, this);
        userDao = new UserDao(userDaoConfig, this);
        user2RoleDao = new User2RoleDao(user2RoleDaoConfig, this);

        registerDao(AccessoryCrossReference.class, accessoryCrossReferenceDao);
        registerDao(AccountingCode.class, accountingCodeDao);
        registerDao(AppState.class, appStateDao);
        registerDao(BinNumber.class, binNumberDao);
        registerDao(BinType.class, binTypeDao);
        registerDao(Category.class, categoryDao);
        registerDao(ConsumableAlternateProductNumberXref.class, consumableAlternateProductNumberXrefDao);
        registerDao(ConsumableBin.class, consumableBinDao);
        registerDao(ConsumableProduct.class, consumableProductDao);
        registerDao(ConsumableProduct2Tag.class, consumableProduct2TagDao);
        registerDao(ConsumableProductBinHistory.class, consumableProductBinHistoryDao);
        registerDao(ConsumableProductCustomFields.class, consumableProductCustomFieldsDao);
        registerDao(Customer.class, customerDao);
        registerDao(Customer2Role.class, customer2RoleDao);
        registerDao(CustomerCustomFields.class, customerCustomFieldsDao);
        registerDao(CustomerCustomFieldsTypes.class, customerCustomFieldsTypesDao);
        registerDao(CustomerPosition.class, customerPositionDao);
        registerDao(CustomerProductNumberXref.class, customerProductNumberXrefDao);
        registerDao(CustomerRole.class, customerRoleDao);
        registerDao(CustomerStatus.class, customerStatusDao);
        registerDao(Discrepancies.class, discrepanciesDao);
        registerDao(DiscrepanciesResolvedStatus.class, discrepanciesResolvedStatusDao);
        registerDao(DiscrepanciesType.class, discrepanciesTypeDao);
        registerDao(DisplayColumnName.class, displayColumnNameDao);
        registerDao(Error.class, errorDao);
        registerDao(InventoryAdjustment.class, inventoryAdjustmentDao);
        registerDao(InventoryAdjustmentReasonCode.class, inventoryAdjustmentReasonCodeDao);
        registerDao(Order.class, orderDao);
        registerDao(OrderConsumableItem.class, orderConsumableItemDao);
        registerDao(OrderConsumableReturn.class, orderConsumableReturnDao);
        registerDao(OrderConsumableReturnItemByCustomer.class, orderConsumableReturnItemByCustomerDao);
        registerDao(OrderRentalItem.class, orderRentalItemDao);
        registerDao(OrderStatus.class, orderStatusDao);
        registerDao(Parameters.class, parametersDao);
        registerDao(PickTicket.class, pickTicketDao);
        registerDao(ProductReceivingLog.class, productReceivingLogDao);
        registerDao(RentalProduct.class, rentalProductDao);
        registerDao(RentalProduct2Tag.class, rentalProduct2TagDao);
        registerDao(RentalProductBinHistory.class, rentalProductBinHistoryDao);
        registerDao(RentalProductCustomFields.class, rentalProductCustomFieldsDao);
        registerDao(RentalProductItem.class, rentalProductItemDao);
        registerDao(RentalProductStatus.class, rentalProductStatusDao);
        registerDao(RentalProductStatusHistory.class, rentalProductStatusHistoryDao);
        registerDao(RentalReturn.class, rentalReturnDao);
        registerDao(Role.class, roleDao);
        registerDao(ShippingNotice.class, shippingNoticeDao);
        registerDao(SyncTransferObject.class, syncTransferObjectDao);
        registerDao(Tag.class, tagDao);
        registerDao(Tag2CustomerRole.class, tag2CustomerRoleDao);
        registerDao(TransactionCustomFields.class, transactionCustomFieldsDao);
        registerDao(TransactionHistory.class, transactionHistoryDao);
        registerDao(TransactionType.class, transactionTypeDao);
        registerDao(Transfer.class, transferDao);
        registerDao(TransferCarrier.class, transferCarrierDao);
        registerDao(TransferItem.class, transferItemDao);
        registerDao(TransferStatus.class, transferStatusDao);
        registerDao(User.class, userDao);
        registerDao(User2Role.class, user2RoleDao);
    }
    
    public void clear() {
        accessoryCrossReferenceDaoConfig.clearIdentityScope();
        accountingCodeDaoConfig.clearIdentityScope();
        appStateDaoConfig.clearIdentityScope();
        binNumberDaoConfig.clearIdentityScope();
        binTypeDaoConfig.clearIdentityScope();
        categoryDaoConfig.clearIdentityScope();
        consumableAlternateProductNumberXrefDaoConfig.clearIdentityScope();
        consumableBinDaoConfig.clearIdentityScope();
        consumableProductDaoConfig.clearIdentityScope();
        consumableProduct2TagDaoConfig.clearIdentityScope();
        consumableProductBinHistoryDaoConfig.clearIdentityScope();
        consumableProductCustomFieldsDaoConfig.clearIdentityScope();
        customerDaoConfig.clearIdentityScope();
        customer2RoleDaoConfig.clearIdentityScope();
        customerCustomFieldsDaoConfig.clearIdentityScope();
        customerCustomFieldsTypesDaoConfig.clearIdentityScope();
        customerPositionDaoConfig.clearIdentityScope();
        customerProductNumberXrefDaoConfig.clearIdentityScope();
        customerRoleDaoConfig.clearIdentityScope();
        customerStatusDaoConfig.clearIdentityScope();
        discrepanciesDaoConfig.clearIdentityScope();
        discrepanciesResolvedStatusDaoConfig.clearIdentityScope();
        discrepanciesTypeDaoConfig.clearIdentityScope();
        displayColumnNameDaoConfig.clearIdentityScope();
        errorDaoConfig.clearIdentityScope();
        inventoryAdjustmentDaoConfig.clearIdentityScope();
        inventoryAdjustmentReasonCodeDaoConfig.clearIdentityScope();
        orderDaoConfig.clearIdentityScope();
        orderConsumableItemDaoConfig.clearIdentityScope();
        orderConsumableReturnDaoConfig.clearIdentityScope();
        orderConsumableReturnItemByCustomerDaoConfig.clearIdentityScope();
        orderRentalItemDaoConfig.clearIdentityScope();
        orderStatusDaoConfig.clearIdentityScope();
        parametersDaoConfig.clearIdentityScope();
        pickTicketDaoConfig.clearIdentityScope();
        productReceivingLogDaoConfig.clearIdentityScope();
        rentalProductDaoConfig.clearIdentityScope();
        rentalProduct2TagDaoConfig.clearIdentityScope();
        rentalProductBinHistoryDaoConfig.clearIdentityScope();
        rentalProductCustomFieldsDaoConfig.clearIdentityScope();
        rentalProductItemDaoConfig.clearIdentityScope();
        rentalProductStatusDaoConfig.clearIdentityScope();
        rentalProductStatusHistoryDaoConfig.clearIdentityScope();
        rentalReturnDaoConfig.clearIdentityScope();
        roleDaoConfig.clearIdentityScope();
        shippingNoticeDaoConfig.clearIdentityScope();
        syncTransferObjectDaoConfig.clearIdentityScope();
        tagDaoConfig.clearIdentityScope();
        tag2CustomerRoleDaoConfig.clearIdentityScope();
        transactionCustomFieldsDaoConfig.clearIdentityScope();
        transactionHistoryDaoConfig.clearIdentityScope();
        transactionTypeDaoConfig.clearIdentityScope();
        transferDaoConfig.clearIdentityScope();
        transferCarrierDaoConfig.clearIdentityScope();
        transferItemDaoConfig.clearIdentityScope();
        transferStatusDaoConfig.clearIdentityScope();
        userDaoConfig.clearIdentityScope();
        user2RoleDaoConfig.clearIdentityScope();
    }

    public AccessoryCrossReferenceDao getAccessoryCrossReferenceDao() {
        return accessoryCrossReferenceDao;
    }

    public AccountingCodeDao getAccountingCodeDao() {
        return accountingCodeDao;
    }

    public AppStateDao getAppStateDao() {
        return appStateDao;
    }

    public BinNumberDao getBinNumberDao() {
        return binNumberDao;
    }

    public BinTypeDao getBinTypeDao() {
        return binTypeDao;
    }

    public CategoryDao getCategoryDao() {
        return categoryDao;
    }

    public ConsumableAlternateProductNumberXrefDao getConsumableAlternateProductNumberXrefDao() {
        return consumableAlternateProductNumberXrefDao;
    }

    public ConsumableBinDao getConsumableBinDao() {
        return consumableBinDao;
    }

    public ConsumableProductDao getConsumableProductDao() {
        return consumableProductDao;
    }

    public ConsumableProduct2TagDao getConsumableProduct2TagDao() {
        return consumableProduct2TagDao;
    }

    public ConsumableProductBinHistoryDao getConsumableProductBinHistoryDao() {
        return consumableProductBinHistoryDao;
    }

    public ConsumableProductCustomFieldsDao getConsumableProductCustomFieldsDao() {
        return consumableProductCustomFieldsDao;
    }

    public CustomerDao getCustomerDao() {
        return customerDao;
    }

    public Customer2RoleDao getCustomer2RoleDao() {
        return customer2RoleDao;
    }

    public CustomerCustomFieldsDao getCustomerCustomFieldsDao() {
        return customerCustomFieldsDao;
    }

    public CustomerCustomFieldsTypesDao getCustomerCustomFieldsTypesDao() {
        return customerCustomFieldsTypesDao;
    }

    public CustomerPositionDao getCustomerPositionDao() {
        return customerPositionDao;
    }

    public CustomerProductNumberXrefDao getCustomerProductNumberXrefDao() {
        return customerProductNumberXrefDao;
    }

    public CustomerRoleDao getCustomerRoleDao() {
        return customerRoleDao;
    }

    public CustomerStatusDao getCustomerStatusDao() {
        return customerStatusDao;
    }

    public DiscrepanciesDao getDiscrepanciesDao() {
        return discrepanciesDao;
    }

    public DiscrepanciesResolvedStatusDao getDiscrepanciesResolvedStatusDao() {
        return discrepanciesResolvedStatusDao;
    }

    public DiscrepanciesTypeDao getDiscrepanciesTypeDao() {
        return discrepanciesTypeDao;
    }

    public DisplayColumnNameDao getDisplayColumnNameDao() {
        return displayColumnNameDao;
    }

    public ErrorDao getErrorDao() {
        return errorDao;
    }

    public InventoryAdjustmentDao getInventoryAdjustmentDao() {
        return inventoryAdjustmentDao;
    }

    public InventoryAdjustmentReasonCodeDao getInventoryAdjustmentReasonCodeDao() {
        return inventoryAdjustmentReasonCodeDao;
    }

    public OrderDao getOrderDao() {
        return orderDao;
    }

    public OrderConsumableItemDao getOrderConsumableItemDao() {
        return orderConsumableItemDao;
    }

    public OrderConsumableReturnDao getOrderConsumableReturnDao() {
        return orderConsumableReturnDao;
    }

    public OrderConsumableReturnItemByCustomerDao getOrderConsumableReturnItemByCustomerDao() {
        return orderConsumableReturnItemByCustomerDao;
    }

    public OrderRentalItemDao getOrderRentalItemDao() {
        return orderRentalItemDao;
    }

    public OrderStatusDao getOrderStatusDao() {
        return orderStatusDao;
    }

    public ParametersDao getParametersDao() {
        return parametersDao;
    }

    public PickTicketDao getPickTicketDao() {
        return pickTicketDao;
    }

    public ProductReceivingLogDao getProductReceivingLogDao() {
        return productReceivingLogDao;
    }

    public RentalProductDao getRentalProductDao() {
        return rentalProductDao;
    }

    public RentalProduct2TagDao getRentalProduct2TagDao() {
        return rentalProduct2TagDao;
    }

    public RentalProductBinHistoryDao getRentalProductBinHistoryDao() {
        return rentalProductBinHistoryDao;
    }

    public RentalProductCustomFieldsDao getRentalProductCustomFieldsDao() {
        return rentalProductCustomFieldsDao;
    }

    public RentalProductItemDao getRentalProductItemDao() {
        return rentalProductItemDao;
    }

    public RentalProductStatusDao getRentalProductStatusDao() {
        return rentalProductStatusDao;
    }

    public RentalProductStatusHistoryDao getRentalProductStatusHistoryDao() {
        return rentalProductStatusHistoryDao;
    }

    public RentalReturnDao getRentalReturnDao() {
        return rentalReturnDao;
    }

    public RoleDao getRoleDao() {
        return roleDao;
    }

    public ShippingNoticeDao getShippingNoticeDao() {
        return shippingNoticeDao;
    }

    public SyncTransferObjectDao getSyncTransferObjectDao() {
        return syncTransferObjectDao;
    }

    public TagDao getTagDao() {
        return tagDao;
    }

    public Tag2CustomerRoleDao getTag2CustomerRoleDao() {
        return tag2CustomerRoleDao;
    }

    public TransactionCustomFieldsDao getTransactionCustomFieldsDao() {
        return transactionCustomFieldsDao;
    }

    public TransactionHistoryDao getTransactionHistoryDao() {
        return transactionHistoryDao;
    }

    public TransactionTypeDao getTransactionTypeDao() {
        return transactionTypeDao;
    }

    public TransferDao getTransferDao() {
        return transferDao;
    }

    public TransferCarrierDao getTransferCarrierDao() {
        return transferCarrierDao;
    }

    public TransferItemDao getTransferItemDao() {
        return transferItemDao;
    }

    public TransferStatusDao getTransferStatusDao() {
        return transferStatusDao;
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public User2RoleDao getUser2RoleDao() {
        return user2RoleDao;
    }

}
