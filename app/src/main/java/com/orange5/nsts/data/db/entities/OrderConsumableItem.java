package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.data.db.dao.ConsumableProductDao;
import com.orange5.nsts.data.db.dao.DaoSession;
import com.orange5.nsts.data.db.dao.OrderConsumableItemDao;
import com.orange5.nsts.data.db.dao.OrderDao;
import com.orange5.nsts.util.db.GUIDFactory;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.ToOne;

import java.util.Objects;

/*
  PRIMARY KEY(`OrderItemId`),
  FOREIGN KEY(`ProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
  FOREIGN KEY(`OrderId`) REFERENCES `Order`(`OrderId`)
*/
@Entity(nameInDb = "OrderConsumableItem")
public class OrderConsumableItem implements OrderItem {

    @ToOne(joinProperty = "orderId")
    private Order order;

    public int quantityMinusOversell() {
        return quantity - getOversell();
    }

    public int pickedPlusOversell() {
        return picked + getOversell();
    }

    @Override
    public boolean isOversell() {
        return getOversell() > 0;
    }

    public int getOversell() {
        int totalAvailable = getConsumableProduct().getBinInformation().getTotalQuantity() + picked;
        int result = quantity - totalAvailable;
        return result > 0 ? result : 0;
    }

    public boolean allPicked() {
        return (isOversell() && oversellAllowed
                && quantity == getOversell() + picked) || Objects.equals(quantity, picked);
    }

    private transient boolean oversellAllowed = false;

    public BinNumber getScannedBin() {
        return scannedBin;
    }

    public void setScannedBin(BinNumber scannedBin) {
        this.scannedBin = scannedBin;
    }

    private transient BinNumber scannedBin;

    public boolean oversellAllowed() {
        return oversellAllowed;
    }

    public void setOversellAllowed(boolean allowOversell) {
        this.oversellAllowed = allowOversell;
    }

    @ToOne(joinProperty = "productId")
    private ConsumableProduct consumableProduct;

    @Id
    @SerializedName("OrderItemId")
    @Property(nameInDb = "OrderItemId")
    private String orderItemId = GUIDFactory.newUUID();

    @SerializedName("OrderId")
    @Property(nameInDb = "OrderId")
    private String orderId;

    @SerializedName("ProductId")
    @Property(nameInDb = "ProductId")
    private String productId;

    @SerializedName("Quantity")
    @Property(nameInDb = "Quantity")
    private Integer quantity = 0;

    @SerializedName("Picked")
    @Property(nameInDb = "Picked")
    private Integer picked = 0;

    @SerializedName("Price")
    @Property(nameInDb = "Price")
    private Double price;

    @SerializedName("isBackOrder")
    @Property(nameInDb = "isBackOrder")
    private Byte isBackOrder = 0;

    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /**
     * Used for active entity operations.
     */
    @Generated(hash = 5332216)
    private transient OrderConsumableItemDao myDao;

    @Generated(hash = 1642827989)
    private transient String consumableProduct__resolvedKey;

    @Generated(hash = 1063247591)
    private transient String order__resolvedKey;

    @Generated(hash = 1330918748)
    public OrderConsumableItem(String orderItemId, String orderId, String productId, Integer quantity,
                               Integer picked, Double price, Byte isBackOrder) {
        this.orderItemId = orderItemId;
        this.orderId = orderId;
        this.productId = productId;
        this.quantity = quantity;
        this.picked = picked;
        this.price = price;
        this.isBackOrder = isBackOrder;
    }

    @Generated(hash = 2094299787)
    public OrderConsumableItem() {
    }

    public String getOrderItemId() {
        return this.orderItemId;
    }

    public void setOrderItemId(String orderItemId) {
        this.orderItemId = orderItemId;
    }

    public String getOrderId() {
        return this.orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String getProductId() {
        return this.productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return this.quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getPicked() {
        return this.picked;
    }

    public void setPicked(Integer picked) {
        this.picked = picked;
    }

    @Override
    public String getProductNumber() {
        return getConsumableProduct().getProductNumber();
    }

    public Double getPrice() {
        return this.price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * To-one relationship, resolved on first access.
     */
    @Generated(hash = 1509535263)
    public ConsumableProduct getConsumableProduct() {
        String __key = this.productId;
        if (consumableProduct__resolvedKey == null
                || consumableProduct__resolvedKey != __key) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            ConsumableProductDao targetDao = daoSession.getConsumableProductDao();
            ConsumableProduct consumableProductNew = targetDao.load(__key);
            synchronized (this) {
                consumableProduct = consumableProductNew;
                consumableProduct__resolvedKey = __key;
            }
        }
        return consumableProduct;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 1988400433)
    public void setConsumableProduct(ConsumableProduct consumableProduct) {
        synchronized (this) {
            this.consumableProduct = consumableProduct;
            productId = consumableProduct == null ? null
                    : consumableProduct.getProductId();
            consumableProduct__resolvedKey = productId;
        }
    }

    @Override
    public Object getPrimaryKey() {
        return getOrderItemId();
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    public void increaseQuantity() {
        if (quantity == null) {
            quantity = 0;//todo debug when this might happen. it shouldn't
        }
        quantity++;
    }

    public void increaseQuantity(int increase) {
        if (quantity == null) {
            quantity = 0;//todo debug when this might happen. it shouldn't
        }
        quantity += increase;
    }


    /**
     * To-one relationship, resolved on first access.
     */
    @Generated(hash = 1955925347)
    public Order getOrder() {
        String __key = this.orderId;
        if (order__resolvedKey == null || order__resolvedKey != __key) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            OrderDao targetDao = daoSession.getOrderDao();
            Order orderNew = targetDao.load(__key);
            synchronized (this) {
                order = orderNew;
                order__resolvedKey = __key;
            }
        }
        return order;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 659365769)
    public void setOrder(Order order) {
        synchronized (this) {
            this.order = order;
            orderId = order == null ? null : order.getOrderId();
            order__resolvedKey = orderId;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderConsumableItem that = (OrderConsumableItem) o;

        return orderItemId.equals(that.orderItemId);
    }

    @Override
    public int hashCode() {
        return orderItemId.hashCode();
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 507095900)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getOrderConsumableItemDao() : null;
    }

    public Byte getIsBackOrder() {
        if (this.isBackOrder == null) this.isBackOrder = 0;
        return this.isBackOrder;
    }

    public void setIsBackOrder(Byte isBackOrder) {
        this.isBackOrder = isBackOrder;
    }

    public boolean isBackorder() {
        return getIsBackOrder() != 0;
    }

    public void isBackorder(boolean isBackorder) {
        byte b = 0;
        if (isBackorder) b = 1;
        setIsBackOrder(b);
    }
}
