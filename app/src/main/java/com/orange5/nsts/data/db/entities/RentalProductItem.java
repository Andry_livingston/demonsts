package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.OrderProduct;
import com.orange5.nsts.data.db.dao.BinNumberDao;
import com.orange5.nsts.data.db.dao.DaoSession;
import com.orange5.nsts.data.db.dao.RentalProductDao;
import com.orange5.nsts.data.db.dao.RentalProductItemDao;
import com.orange5.nsts.service.bin.ProductBinInformation;
import com.orange5.nsts.util.db.GUIDFactory;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.ToOne;

/*
  FOREIGN KEY(`BinId`) REFERENCES `BinNumber`(`BinId`),
  PRIMARY KEY(`UniqueUnitNumber`),
  FOREIGN KEY(`ProductId`) REFERENCES `RentalProduct`(`ProductId`),
  FOREIGN KEY(`Status`) REFERENCES `RentalProductStatus`(`StatusId`)
*/
@Entity(nameInDb = "RentalProductItem")
public class RentalProductItem implements OrderProduct {
    public static final int STATUS_RENTAL_ITEM = 1;

    @ToOne(joinProperty = "binId")
    private BinNumber containerBin;

    @ToOne(joinProperty = "productId")
    private RentalProduct rentalProduct;

    @Id
    @SerializedName("UniqueUnitNumber")
    @Property(nameInDb = "UniqueUnitNumber")
    private String uniqueUnitNumber = GUIDFactory.newUUID();

    @SerializedName("ProductId")
    @Property(nameInDb = "ProductId")
    private String productId;

    @SerializedName("Status")
    @Property(nameInDb = "Status")
    private Integer status;//smallint

    @SerializedName("Allocated")
    @Property(nameInDb = "Allocated")
    private byte allocated;//bit

    @SerializedName("BinId")
    @Property(nameInDb = "BinId")
    private String binId;

    @SerializedName("CertDate")
    @Property(nameInDb = "CertDate")
    private String certDate;

    @SerializedName("CertDuration")
    @Property(nameInDb = "CertDuration")
    private Integer certDuration;

    @SerializedName("ContainerID")
    @Property(nameInDb = "ContainerID")
    private String containerID;

    @SerializedName("Remarks")
    @Property(nameInDb = "Remarks")
    private String remarks;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 1759802138)
    private transient RentalProductItemDao myDao;

    @Generated(hash = 2133341400)
    private transient String containerBin__resolvedKey;

    @Generated(hash = 1355849290)
    private transient String rentalProduct__resolvedKey;

    @Generated(hash = 1518399947)
    public RentalProductItem(String uniqueUnitNumber, String productId, Integer status,
            byte allocated, String binId, String certDate, Integer certDuration,
            String containerID, String remarks) {
        this.uniqueUnitNumber = uniqueUnitNumber;
        this.productId = productId;
        this.status = status;
        this.allocated = allocated;
        this.binId = binId;
        this.certDate = certDate;
        this.certDuration = certDuration;
        this.containerID = containerID;
        this.remarks = remarks;
    }

    @Generated(hash = 955127027)
    public RentalProductItem() {
    }

    public String getUniqueUnitNumber() {
        return this.uniqueUnitNumber;
    }

    public void setUniqueUnitNumber(String uniqueUnitNumber) {
        this.uniqueUnitNumber = uniqueUnitNumber;
    }

    public String getProductId() {
        return this.productId;
    }

    @Override
    public String getProductNumber() {
        String number = "";
        if (getRentalProduct() != null) {
            number = rentalProduct.getProductNumber();
        }
        return number;
    }

    @Override
    public String getProductDescription() {
        String description = "";
        if (getRentalProduct() != null) {
            description = rentalProduct.getProductDescription();
        }
        return description;
    }

    @Override
    public String getManufacturerNumber() {
        String number = "";
        if (getRentalProduct() != null) {
            number = rentalProduct.getManufacturerNumber();
        }
        return number;
    }

    @Override
    public String getMake() {
        String make = "";
        if (getRentalProduct() != null) {
            make = rentalProduct.getMake();
        }
        return make;
    }

    @Override
    public String getCategoryId() {
        String categoryId = "";
        if (getRentalProduct() != null) {
            categoryId = rentalProduct.getCategoryId();
        }
        return categoryId;
    }

    @Override
    public String getInfo() {
        String info = "";
        if (getRentalProduct() != null) {
            info = rentalProduct.getInfo();
        }
        return info;
    }

    @Override
    public int getQuantity() {
        int quantity = 0;
        if (getRentalProduct() != null) {
            quantity = rentalProduct.getQuantity();
        }
        return quantity;
    }

    @Override
    public ProductBinInformation getBinInformation() {
        ProductBinInformation productBinInformation = null;
        if (getRentalProduct() != null) {
            productBinInformation = rentalProduct.getBinInformation();
        }
        return productBinInformation;
    }

    @Override
    public void filterOut() {
        if (getRentalProduct() != null) {
            rentalProduct.filterOut();
        }
    }

    @Override
    public boolean isFilteredOut() {
        boolean isFilteredOut = false;
        if (getRentalProduct() != null) {
            isFilteredOut = rentalProduct.isFilteredOut();
        }
        return isFilteredOut;
    }

    @Override
    public double getPrice() {
        double price = 0;
        if (getRentalProduct() != null) {
            price = rentalProduct.getPrice();
        }
        return price;
    }

    @Override
    public String getUUN() {
        return uniqueUnitNumber;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public byte getAllocated() {
        return this.allocated;
    }

    public void setAllocated(byte allocated) {
        this.allocated = allocated;
    }

    public String getBinId() {
        return this.binId;
    }

    public void setBinId(String binId) {
        this.binId = binId;
    }

    public String getCertDate() {
        return this.certDate;
    }

    public void setCertDate(String certDate) {
        this.certDate = certDate;
    }

    public Integer getCertDuration() {
        return this.certDuration;
    }

    public void setCertDuration(Integer certDuration) {
        this.certDuration = certDuration;
    }

    public String getContainerID() {
        return this.containerID;
    }

    public void setContainerID(String containerID) {
        this.containerID = containerID;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * To-one relationship, resolved on first access.
     */
    @Generated(hash = 856404112)
    public BinNumber getContainerBin() {
        String __key = this.binId;
        if (containerBin__resolvedKey == null || containerBin__resolvedKey != __key) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            BinNumberDao targetDao = daoSession.getBinNumberDao();
            BinNumber containerBinNew = targetDao.load(__key);
            synchronized (this) {
                containerBin = containerBinNew;
                containerBin__resolvedKey = __key;
            }
        }
        return containerBin;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 1720797646)
    public void setContainerBin(BinNumber containerBin) {
        synchronized (this) {
            this.containerBin = containerBin;
            binId = containerBin == null ? null : containerBin.getBinId();
            containerBin__resolvedKey = binId;
        }
    }

    @Override
    public Object getPrimaryKey() {
        return getUniqueUnitNumber();
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /**
     * To-one relationship, resolved on first access.
     */
    @Generated(hash = 1429405704)
    public RentalProduct getRentalProduct() {
        String __key = this.productId;
        if (rentalProduct__resolvedKey == null || rentalProduct__resolvedKey != __key) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            RentalProductDao targetDao = daoSession.getRentalProductDao();
            RentalProduct rentalProductNew = targetDao.load(__key);
            synchronized (this) {
                rentalProduct = rentalProductNew;
                rentalProduct__resolvedKey = __key;
            }
        }
        return rentalProduct;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 249590766)
    public void setRentalProduct(RentalProduct rentalProduct) {
        synchronized (this) {
            this.rentalProduct = rentalProduct;
            productId = rentalProduct == null ? null : rentalProduct.getProductId();
            rentalProduct__resolvedKey = productId;
        }
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 815102400)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getRentalProductItemDao() : null;
    }

}
