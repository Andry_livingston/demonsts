package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

@Entity(nameInDb = "TransferCarrier")
public class TransferCarrier implements DatabaseEntity {

    @Property(nameInDb = "CarrierId")
    @Id
    @SerializedName("CarrierId")
    private long carrierId;

    @SerializedName("CarrierName")
    @Property(nameInDb = "CarrierName")
    private String carrierName;

    @Generated(hash = 1351200250)
    public TransferCarrier(long carrierId, String carrierName) {
        this.carrierId = carrierId;
        this.carrierName = carrierName;
    }

    @Generated(hash = 148415083)
    public TransferCarrier() {
    }

    public long getCarrierId() {
        return this.carrierId;
    }

    public void setCarrierId(long carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierName() {
        return this.carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    @Override
    public Object getPrimaryKey() {
        return getCarrierId();
    }
}
