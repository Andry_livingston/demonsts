package com.orange5.nsts.data.db.entities;

import android.util.ArraySet;

import com.orange5.nsts.data.db.base.DatabaseEntity;
import com.orange5.nsts.data.db.dao.DaoSession;
import com.orange5.nsts.data.db.dao.OrderRentalItemDao;
import com.orange5.nsts.data.db.dao.RentalReturnDao;
import com.orange5.nsts.service.rentalReturn.RentalReturnStatus;
import com.orange5.nsts.util.CollectionUtils;
import com.orange5.nsts.util.db.GUIDFactory;
import com.orange5.nsts.util.format.DateFormatter;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.ToMany;
import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;

import java.util.List;
import java.util.Set;

/*
  PRIMARY KEY(`RentalReturnId`)
*/
@Entity(nameInDb = "RentalReturn", active = true)
public class RentalReturn implements DatabaseEntity {

    public RentalReturnStatus getStatus() {
        return RentalReturnStatus.of(this);
    }

    @Override
    public Object getPrimaryKey() {
        return getRentalReturnId();
    }

    @Override
    public DateTime getDateTime() {
        return DateFormatter.convertToDateTime(getStartDate());
    }

    public String getCustomerInformText() {
        Set<String> allUniqueCustomersByOrderItems = collectAllUniqueCustomersByOrder(getItems());
        return allUniqueCustomersByOrderItems.size() > 0
                ? configCustomersText(allUniqueCustomersByOrderItems) : "";
    }

    @NotNull
    private Set<String> collectAllUniqueCustomersByOrder(List<OrderRentalItem> items) {
        Set<String> allCustomersByOrderItems = new ArraySet<>();
        if (items != null && items.size() > 0) {
            for (OrderRentalItem item: items) {
                allCustomersByOrderItems.add(item.getReturnedByCustomer().toString());
            }
        }
        return allCustomersByOrderItems;
    }

    private String configCustomersText(Set<String> allCustomersByOrderItems) {
        return allCustomersByOrderItems.size() > 1
                ? "Multiple Customers"
                : allCustomersByOrderItems.iterator().next();
    }

    public Customer getCustomerOrNull() {
        Customer customer = null;
        List<OrderRentalItem> items = getItems();
        if (CollectionUtils.isNotEmpty(items)) {
            customer = items.get(0).getReturnedByCustomer();
            if (customer != null) {
                String customerId = items.get(0).getReturnCustomerId();
                for (OrderRentalItem item : items) {
                    if (item.getRentalReturnId() == null) continue;
                    if (!item.getReturnCustomerId().equals(customerId)) {
                        customer = null;
                        break;
                    }
                }
            }
        }
        return customer;
    }

    @ToMany(referencedJoinProperty = "rentalReturnId")
    private List<OrderRentalItem> items;

    @Id
    @Property(nameInDb = "RentalReturnId")
    private String rentalReturnId = GUIDFactory.newUUID();

    @Property(nameInDb = "ReturnNumber")
    private String returnNumber = GUIDFactory.newUnique8();

    @Property(nameInDb = "StartDate")
    private String startDate;

    @Property(nameInDb = "CompleteDate")
    private String completeDate;

    @Property(nameInDb = "SignImage")
    private byte[] signImage;

    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /**
     * Used for active entity operations.
     */
    @Generated(hash = 714211474)
    private transient RentalReturnDao myDao;

    @Generated(hash = 1621318116)
    public RentalReturn(String rentalReturnId, String returnNumber,
                        String startDate, String completeDate, byte[] signImage) {
        this.rentalReturnId = rentalReturnId;
        this.returnNumber = returnNumber;
        this.startDate = startDate;
        this.completeDate = completeDate;
        this.signImage = signImage;
    }

    @Generated(hash = 1163409375)
    public RentalReturn() {
    }

    public String getRentalReturnId() {
        return this.rentalReturnId;
    }

    public void setRentalReturnId(String rentalReturnId) {
        this.rentalReturnId = rentalReturnId;
    }

    public String getReturnNumber() {
        return this.returnNumber;
    }

    public void setReturnNumber(String returnNumber) {
        this.returnNumber = returnNumber;
    }

    public String getStartDate() {
        return this.startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getCompleteDate() {
        return this.completeDate;
    }

    public void setCompleteDate(String completeDate) {
        this.completeDate = completeDate;
    }

    public byte[] getSignImage() {
        return this.signImage;
    }

    public void setSignImage(byte[] signImage) {
        this.signImage = signImage;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1362465217)
    public List<OrderRentalItem> getItems() {
        if (items == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            OrderRentalItemDao targetDao = daoSession.getOrderRentalItemDao();
            List<OrderRentalItem> itemsNew = targetDao
                    ._queryRentalReturn_Items(rentalReturnId);
            synchronized (this) {
                if (items == null) {
                    items = itemsNew;
                }
            }
        }
        return items;
    }

    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 1727286264)
    public synchronized void resetItems() {
        items = null;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 940223493)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getRentalReturnDao() : null;
    }

}
