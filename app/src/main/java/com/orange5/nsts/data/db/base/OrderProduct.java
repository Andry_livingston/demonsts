package com.orange5.nsts.data.db.base;

import com.orange5.nsts.data.db.entities.ConsumableProduct;
import com.orange5.nsts.data.db.entities.RentalProduct;
import com.orange5.nsts.service.bin.ProductBinInformation;

import java.util.function.Consumer;

public interface OrderProduct extends DatabaseEntity {

    String getProductId();

    String getProductNumber();

    String getProductDescription();

    String getManufacturerNumber();

    String getMake();

    String getCategoryId();

    String getInfo();

    int getQuantity();

    ProductBinInformation getBinInformation();

    void filterOut();

    boolean isFilteredOut();

    default double getPrice() {
        return 0.0;
    }

    default String getUUN() {
        return "";
    }

    default boolean isRental() {
        return this instanceof RentalProduct;
    }

    default boolean isConsumable() {
        return this instanceof ConsumableProduct;
    }

    default void ifConsumable(Consumer<ConsumableProduct> productConsumer) {
        if (this instanceof ConsumableProduct) {
            productConsumer.accept((ConsumableProduct) this);
        }
    }

    default void ifRental(Consumer<RentalProduct> productConsumer) {
        if (this instanceof RentalProduct) {
            productConsumer.accept((RentalProduct) this);
        }
    }

}
