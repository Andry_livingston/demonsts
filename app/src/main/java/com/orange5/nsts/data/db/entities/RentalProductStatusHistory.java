package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.util.Date;

/*
FOREIGN KEY(`StatusId`) REFERENCES `RentalProductStatus`(`StatusId`),
PRIMARY KEY(`HistoryId`)
*/
@Entity(nameInDb = "RentalProductStatusHistory")
public class RentalProductStatusHistory implements DatabaseEntity {

    @Id
    @SerializedName("RentalReturnId")
    @Property(nameInDb = "RentalReturnId")
    private String rentalReturnId;

    @SerializedName("HistoryId")
    @Property(nameInDb = "HistoryId")
    private String historyId;

    @SerializedName("UniqueUnitNumber")
    @Property(nameInDb = "UniqueUnitNumber")
    private String uniqueUnitNumber;

    @SerializedName("StatusId")
    @Property(nameInDb = "StatusId")
    private Integer statusId;//smallint

    @SerializedName("AddDate")
    @Property(nameInDb = "AddDate")
    private Date addDate;

    @SerializedName("Comment")
    @Property(nameInDb = "Comment")
    private String comment;

    @Generated(hash = 1352573411)
    public RentalProductStatusHistory(String rentalReturnId, String historyId,
                                      String uniqueUnitNumber, Integer statusId, Date addDate,
                                      String comment) {
        this.rentalReturnId = rentalReturnId;
        this.historyId = historyId;
        this.uniqueUnitNumber = uniqueUnitNumber;
        this.statusId = statusId;
        this.addDate = addDate;
        this.comment = comment;
    }

    @Generated(hash = 692342342)
    public RentalProductStatusHistory() {
    }

    public String getRentalReturnId() {
        return this.rentalReturnId;
    }

    public void setRentalReturnId(String rentalReturnId) {
        this.rentalReturnId = rentalReturnId;
    }

    public String getHistoryId() {
        return this.historyId;
    }

    public void setHistoryId(String historyId) {
        this.historyId = historyId;
    }

    public String getUniqueUnitNumber() {
        return this.uniqueUnitNumber;
    }

    public void setUniqueUnitNumber(String uniqueUnitNumber) {
        this.uniqueUnitNumber = uniqueUnitNumber;
    }

    public Integer getStatusId() {
        return this.statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Date getAddDate() {
        return this.addDate;
    }

    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public Object getPrimaryKey() {
        return getRentalReturnId();
    }
}
