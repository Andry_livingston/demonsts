package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
  PRIMARY KEY(`ProductID`,`TagId`),
  FOREIGN KEY(`TagId`) REFERENCES `Tag`(`TagId`),
  FOREIGN KEY(`ProductID`) REFERENCES `ConsumableProduct`(`ProductId`)
*/
@Entity(nameInDb = "ConsumableProduct2Tag")
public class ConsumableProduct2Tag implements DatabaseEntity {

    @Id
    @SerializedName("ProductID")
    @Property(nameInDb = "ProductID")
    private String productID;

    @SerializedName("TagId")
    @Property(nameInDb = "TagId")
    private String tagId;

    @Generated(hash = 1136860904)
    public ConsumableProduct2Tag(String productID, String tagId) {
        this.productID = productID;
        this.tagId = tagId;
    }

    @Generated(hash = 1812819289)
    public ConsumableProduct2Tag() {
    }

    public String getProductID() {
        return this.productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getTagId() {
        return this.tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    @Override
    public Object getPrimaryKey() {
        return getProductID();
    }
}
