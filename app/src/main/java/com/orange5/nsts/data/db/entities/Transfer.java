package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.util.Date;
/*
  FOREIGN KEY(`CarrierId`) REFERENCES `TransferCarrier`(`CarrierId`),
  FOREIGN KEY(`CreatedByUserId`) REFERENCES `User`(`UserId`),
  FOREIGN KEY(`StatusId`) REFERENCES `TransferStatus`(`StatusId`),
  PRIMARY KEY(`TransferId`)
*/
@Entity(nameInDb = "Transfer")
public class Transfer implements DatabaseEntity {

    @Id
    @SerializedName("TransferId")
    @Property(nameInDb = "TransferId")
    private String transferId;

    @SerializedName("TransferNumber")
    @Property(nameInDb = "TransferNumber")
    private String transferNumber;

    @SerializedName("StatusId")
    @Property(nameInDb = "StatusId")
    private int statusId;

    @SerializedName("OpennedDate")
    @Property(nameInDb = "OpennedDate")
    private Date opennedDate;

    @SerializedName("ShippedDate")
    @Property(nameInDb = "ShippedDate")
    private Date shippedDate;

    @SerializedName("CarrierId")
    @Property(nameInDb = "CarrierId")
    private  int carrierId;

    @SerializedName("TrackingNumber")
    @Property(nameInDb = "TrackingNumber")
    private  String trackingNumber;

    @SerializedName("CreatedByUserId")
    @Property(nameInDb = "CreatedByUserId")
    private String createdByUserId;

    @Generated(hash = 94160025)
    public Transfer(String transferId, String transferNumber, int statusId,
                    Date opennedDate, Date shippedDate, int carrierId,
                    String trackingNumber, String createdByUserId) {
        this.transferId = transferId;
        this.transferNumber = transferNumber;
        this.statusId = statusId;
        this.opennedDate = opennedDate;
        this.shippedDate = shippedDate;
        this.carrierId = carrierId;
        this.trackingNumber = trackingNumber;
        this.createdByUserId = createdByUserId;
    }

    @Generated(hash = 137042952)
    public Transfer() {
    }

    public String getTransferId() {
        return this.transferId;
    }

    public void setTransferId(String transferId) {
        this.transferId = transferId;
    }

    public String getTransferNumber() {
        return this.transferNumber;
    }

    public void setTransferNumber(String transferNumber) {
        this.transferNumber = transferNumber;
    }

    public int getStatusId() {
        return this.statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public Date getOpennedDate() {
        return this.opennedDate;
    }

    public void setOpennedDate(Date opennedDate) {
        this.opennedDate = opennedDate;
    }

    public Date getShippedDate() {
        return this.shippedDate;
    }

    public void setShippedDate(Date shippedDate) {
        this.shippedDate = shippedDate;
    }

    public int getCarrierId() {
        return this.carrierId;
    }

    public void setCarrierId(int carrierId) {
        this.carrierId = carrierId;
    }

    public String getTrackingNumber() {
        return this.trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getCreatedByUserId() {
        return this.createdByUserId;
    }

    public void setCreatedByUserId(String createdByUserId) {
        this.createdByUserId = createdByUserId;
    }

    @Override
    public Object getPrimaryKey() {
        return getTransferId();
    }
}
