package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
  FOREIGN KEY(`OrderConsumableReturnId`) REFERENCES `OrderConsumableReturn`(`OrderConsumableReturnId`),
  FOREIGN KEY(`OrderItemId`) REFERENCES `OrderConsumableItem`(`OrderItemId`),
  PRIMARY KEY(`OrderConsumableReturnItemByCustomerId`)
*/
@Entity(nameInDb = "OrderConsumableReturnItemByCustomer")
public class OrderConsumableReturnItemByCustomer implements DatabaseEntity {

    @Id
    @SerializedName("OrderConsumableReturnItemByCustomerId")
    @Property(nameInDb = "OrderConsumableReturnItemByCustomerId")
    private String orderConsumableReturnItemByCustomerId;

    @SerializedName("OrderItemId")
    @Property(nameInDb = "OrderItemId")
    private String orderItemId;

    @SerializedName("Quantity")
    @Property(nameInDb = "Quantity")
    private Integer quantity;

    @SerializedName("OrderConsumableReturnId")
    @Property(nameInDb = "OrderConsumableReturnId")
    private String orderConsumableReturnId;

    @Generated(hash = 1532528156)
    public OrderConsumableReturnItemByCustomer(String orderConsumableReturnItemByCustomerId,
                                               String orderItemId, Integer quantity, String orderConsumableReturnId) {
        this.orderConsumableReturnItemByCustomerId = orderConsumableReturnItemByCustomerId;
        this.orderItemId = orderItemId;
        this.quantity = quantity;
        this.orderConsumableReturnId = orderConsumableReturnId;
    }

    @Generated(hash = 1424459146)
    public OrderConsumableReturnItemByCustomer() {
    }

    public String getOrderConsumableReturnItemByCustomerId() {
        return this.orderConsumableReturnItemByCustomerId;
    }

    public void setOrderConsumableReturnItemByCustomerId(String orderConsumableReturnItemByCustomerId) {
        this.orderConsumableReturnItemByCustomerId = orderConsumableReturnItemByCustomerId;
    }

    public String getOrderItemId() {
        return this.orderItemId;
    }

    public void setOrderItemId(String orderItemId) {
        this.orderItemId = orderItemId;
    }

    public Integer getQuantity() {
        return this.quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getOrderConsumableReturnId() {
        return this.orderConsumableReturnId;
    }

    public void setOrderConsumableReturnId(String orderConsumableReturnId) {
        this.orderConsumableReturnId = orderConsumableReturnId;
    }

    @Override
    public Object getPrimaryKey() {
        return getOrderConsumableReturnItemByCustomerId();
    }
}
