package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
PRIMARY KEY(`TypeId`)
*/
@Entity(nameInDb = "DiscrepanciesType")
public class DiscrepanciesType implements DatabaseEntity {

    @Id
    @SerializedName("TypeId")
    @Property(nameInDb = "TypeId")
    private long typeId;

    @SerializedName("TypeName")
    @Property(nameInDb = "TypeName")
    private String typeName;

    @SerializedName("IsNegative")
    @Property(nameInDb = "IsNegative")
    private byte isNegative;

    @SerializedName("ResolutionType")
    @Property(nameInDb = "ResolutionType")
    private String resolutionType;

    @Generated(hash = 976239910)
    public DiscrepanciesType(long typeId, String typeName, byte isNegative,
            String resolutionType) {
        this.typeId = typeId;
        this.typeName = typeName;
        this.isNegative = isNegative;
        this.resolutionType = resolutionType;
    }

    @Generated(hash = 1589779777)
    public DiscrepanciesType() {
    }

    public long getTypeId() {
        return this.typeId;
    }

    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return this.typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public byte getIsNegative() {
        return this.isNegative;
    }

    public void setIsNegative(byte isNegative) {
        this.isNegative = isNegative;
    }

    public String getResolutionType() {
        return this.resolutionType;
    }

    public void setResolutionType(String resolutionType) {
        this.resolutionType = resolutionType;
    }

    @Override
    public Object getPrimaryKey() {
        return getTypeId();
    }
}
