package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
FOREIGN KEY(`TransactionType`) REFERENCES `TransactionType`(`TransactionTypeId`),
FOREIGN KEY(`RentalProductId`) REFERENCES `RentalProduct`(`ProductId`),
FOREIGN KEY(`ConsumableProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
FOREIGN KEY(`CustomerId`) REFERENCES `Customer`(`CustomerId`),
*/
@Entity(nameInDb = "TransactionHistory")
public class TransactionHistory implements DatabaseEntity {
	@Id
	@SerializedName("TransactionId")
	@Property(nameInDb = "TransactionId")
	private String transactionId;

	@SerializedName("Device")
	@Property(nameInDb = "Device")
	private String device;

	@SerializedName("CustomerId")
	@Property(nameInDb = "CustomerId")
	private String customerId;

	@SerializedName("TransactionDate")
	@Property(nameInDb = "TransactionDate")
	private String transactionDate;

	@SerializedName("Details")
	@Property(nameInDb = "Details")
	private String details;

	@SerializedName("Details")
	@Property(nameInDb = "Details")
	private int transactionType;

	@SerializedName("ConsumableProductId")
	@Property(nameInDb = "ConsumableProductId")
	private String consumableProductId;

	@SerializedName("Quantity")
	@Property(nameInDb = "Quantity")
	private int quantity;

	@SerializedName("RentalProductId")
	@Property(nameInDb = "RentalProductId")
	private String rentalProductId;

	@SerializedName("UniqueUnitNumber")
	@Property(nameInDb = "UniqueUnitNumber")
	private String uniqueUnitNumber;

	@SerializedName("SignImage")
	@Property(nameInDb = "SignImage")
	private byte[] signImage;

	@SerializedName("ReceiptText")
	@Property(nameInDb = "ReceiptText")
	private String receiptText;

	@SerializedName("OrderId")
	@Property(nameInDb = "OrderId")
	private String orderId;

	@Generated(hash = 768419199)
	public TransactionHistory(String transactionId, String device,
							  String customerId, String transactionDate, String details,
							  int transactionType, String consumableProductId, int quantity,
							  String rentalProductId, String uniqueUnitNumber, byte[] signImage,
							  String receiptText, String orderId) {
		this.transactionId = transactionId;
		this.device = device;
		this.customerId = customerId;
		this.transactionDate = transactionDate;
		this.details = details;
		this.transactionType = transactionType;
		this.consumableProductId = consumableProductId;
		this.quantity = quantity;
		this.rentalProductId = rentalProductId;
		this.uniqueUnitNumber = uniqueUnitNumber;
		this.signImage = signImage;
		this.receiptText = receiptText;
		this.orderId = orderId;
	}

	@Generated(hash = 63079048)
	public TransactionHistory() {
	}

	public String getTransactionId() {
		return this.transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getDevice() {
		return this.device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getTransactionDate() {
		return this.transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getDetails() {
		return this.details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public int getTransactionType() {
		return this.transactionType;
	}

	public void setTransactionType(int transactionType) {
		this.transactionType = transactionType;
	}

	public String getConsumableProductId() {
		return this.consumableProductId;
	}

	public void setConsumableProductId(String consumableProductId) {
		this.consumableProductId = consumableProductId;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getRentalProductId() {
		return this.rentalProductId;
	}

	public void setRentalProductId(String rentalProductId) {
		this.rentalProductId = rentalProductId;
	}

	public String getUniqueUnitNumber() {
		return this.uniqueUnitNumber;
	}

	public void setUniqueUnitNumber(String uniqueUnitNumber) {
		this.uniqueUnitNumber = uniqueUnitNumber;
	}

	public byte[] getSignImage() {
		return this.signImage;
	}

	public void setSignImage(byte[] signImage) {
		this.signImage = signImage;
	}

	public String getReceiptText() {
		return this.receiptText;
	}

	public void setReceiptText(String receiptText) {
		this.receiptText = receiptText;
	}

	public String getOrderId() {
		return this.orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Override
	public Object getPrimaryKey() {
		return getTransactionId();
	}
}
