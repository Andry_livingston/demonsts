package com.orange5.nsts.data.db.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

import com.orange5.nsts.data.db.entities.RentalProduct2Tag;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "RentalProduct2Tag".
*/
public class RentalProduct2TagDao extends AbstractDao<RentalProduct2Tag, String> {

    public static final String TABLENAME = "RentalProduct2Tag";

    /**
     * Properties of entity RentalProduct2Tag.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property ProductID = new Property(0, String.class, "productID", true, "ProductID");
        public final static Property TagId = new Property(1, String.class, "tagId", false, "TagId");
    }


    public RentalProduct2TagDao(DaoConfig config) {
        super(config);
    }
    
    public RentalProduct2TagDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"RentalProduct2Tag\" (" + //
                "\"ProductID\" TEXT PRIMARY KEY NOT NULL ," + // 0: productID
                "\"TagId\" TEXT);"); // 1: tagId
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"RentalProduct2Tag\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, RentalProduct2Tag entity) {
        stmt.clearBindings();
 
        String productID = entity.getProductID();
        if (productID != null) {
            stmt.bindString(1, productID);
        }
 
        String tagId = entity.getTagId();
        if (tagId != null) {
            stmt.bindString(2, tagId);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, RentalProduct2Tag entity) {
        stmt.clearBindings();
 
        String productID = entity.getProductID();
        if (productID != null) {
            stmt.bindString(1, productID);
        }
 
        String tagId = entity.getTagId();
        if (tagId != null) {
            stmt.bindString(2, tagId);
        }
    }

    @Override
    public String readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0);
    }    

    @Override
    public RentalProduct2Tag readEntity(Cursor cursor, int offset) {
        RentalProduct2Tag entity = new RentalProduct2Tag( //
            cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0), // productID
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1) // tagId
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, RentalProduct2Tag entity, int offset) {
        entity.setProductID(cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0));
        entity.setTagId(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
     }
    
    @Override
    protected final String updateKeyAfterInsert(RentalProduct2Tag entity, long rowId) {
        return entity.getProductID();
    }
    
    @Override
    public String getKey(RentalProduct2Tag entity) {
        if(entity != null) {
            return entity.getProductID();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(RentalProduct2Tag entity) {
        return entity.getProductID() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
