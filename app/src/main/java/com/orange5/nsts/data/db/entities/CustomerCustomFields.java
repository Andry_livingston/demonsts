package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
  FOREIGN KEY(`CustomerId`) REFERENCES `Customer`(`CustomerId`),
  PRIMARY KEY(`CustomFieldId`),
  FOREIGN KEY(`CustomFieldTypeId`) REFERENCES `CustomerCustomFieldsTypes`(`FieldTypeId`)
*/
@Entity(nameInDb = "CustomerCustomFields")
public class CustomerCustomFields implements DatabaseEntity {

    @Id
    @SerializedName("CustomFieldId")
    @Property(nameInDb = "CustomFieldId")
    private String customFieldId;

    @SerializedName("CustomerId")
    @Property(nameInDb = "CustomerId")
    private String customerId;

    @SerializedName("CustomFieldTypeId")
    @Property(nameInDb = "CustomFieldTypeId")
    private String customFieldTypeId;

    @SerializedName("FieldValue")
    @Property(nameInDb = "FieldValue")
    private String fieldValue;

    @Generated(hash = 1413796154)
    public CustomerCustomFields(String customFieldId, String customerId,
                                String customFieldTypeId, String fieldValue) {
        this.customFieldId = customFieldId;
        this.customerId = customerId;
        this.customFieldTypeId = customFieldTypeId;
        this.fieldValue = fieldValue;
    }

    @Generated(hash = 715129565)
    public CustomerCustomFields() {
    }

    public String getCustomFieldId() {
        return this.customFieldId;
    }

    public void setCustomFieldId(String customFieldId) {
        this.customFieldId = customFieldId;
    }

    public String getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomFieldTypeId() {
        return this.customFieldTypeId;
    }

    public void setCustomFieldTypeId(String customFieldTypeId) {
        this.customFieldTypeId = customFieldTypeId;
    }

    public String getFieldValue() {
        return this.fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    @Override
    public Object getPrimaryKey() {
        return getCustomFieldId();
    }
}
