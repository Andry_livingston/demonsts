package com.orange5.nsts.data.db.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

import com.orange5.nsts.data.db.entities.SyncTransferObject;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "SyncTransferObject".
*/
public class SyncTransferObjectDao extends AbstractDao<SyncTransferObject, Long> {

    public static final String TABLENAME = "SyncTransferObject";

    /**
     * Properties of entity SyncTransferObject.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property IncrementNumber = new Property(0, long.class, "incrementNumber", true, "IncrementNumber");
        public final static Property TransferObjectId = new Property(1, String.class, "transferObjectId", false, "TransferObjectId");
        public final static Property TypeData = new Property(2, String.class, "typeData", false, "TypeData");
        public final static Property QueryType = new Property(3, String.class, "queryType", false, "QueryType");
        public final static Property QueryTable = new Property(4, String.class, "queryTable", false, "QueryTable");
        public final static Property QueryKey = new Property(5, String.class, "queryKey", false, "QueryKey");
        public final static Property QueryFields = new Property(6, String.class, "queryFields", false, "QueryFields");
        public final static Property DateTimeSaved = new Property(7, String.class, "dateTimeSaved", false, "DateTimeSaved");
        public final static Property Action = new Property(8, String.class, "action", false, "Action");
        public final static Property IsMine = new Property(9, boolean.class, "isMine", false, "IsMine");
        public final static Property Executed = new Property(10, String.class, "executed", false, "Executed");
        public final static Property Data = new Property(11, byte[].class, "data", false, "DataObject");
    }


    public SyncTransferObjectDao(DaoConfig config) {
        super(config);
    }
    
    public SyncTransferObjectDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"SyncTransferObject\" (" + //
                "\"IncrementNumber\" INTEGER PRIMARY KEY NOT NULL ," + // 0: incrementNumber
                "\"TransferObjectId\" TEXT," + // 1: transferObjectId
                "\"TypeData\" TEXT," + // 2: typeData
                "\"QueryType\" TEXT," + // 3: queryType
                "\"QueryTable\" TEXT," + // 4: queryTable
                "\"QueryKey\" TEXT," + // 5: queryKey
                "\"QueryFields\" TEXT," + // 6: queryFields
                "\"DateTimeSaved\" TEXT," + // 7: dateTimeSaved
                "\"Action\" TEXT," + // 8: action
                "\"IsMine\" INTEGER NOT NULL ," + // 9: isMine
                "\"Executed\" TEXT," + // 10: executed
                "\"DataObject\" BLOB);"); // 11: data
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"SyncTransferObject\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, SyncTransferObject entity) {
        stmt.clearBindings();
        stmt.bindLong(1, entity.getIncrementNumber());
 
        String transferObjectId = entity.getTransferObjectId();
        if (transferObjectId != null) {
            stmt.bindString(2, transferObjectId);
        }
 
        String typeData = entity.getTypeData();
        if (typeData != null) {
            stmt.bindString(3, typeData);
        }
 
        String queryType = entity.getQueryType();
        if (queryType != null) {
            stmt.bindString(4, queryType);
        }
 
        String queryTable = entity.getQueryTable();
        if (queryTable != null) {
            stmt.bindString(5, queryTable);
        }
 
        String queryKey = entity.getQueryKey();
        if (queryKey != null) {
            stmt.bindString(6, queryKey);
        }
 
        String queryFields = entity.getQueryFields();
        if (queryFields != null) {
            stmt.bindString(7, queryFields);
        }
 
        String dateTimeSaved = entity.getDateTimeSaved();
        if (dateTimeSaved != null) {
            stmt.bindString(8, dateTimeSaved);
        }
 
        String action = entity.getAction();
        if (action != null) {
            stmt.bindString(9, action);
        }
        stmt.bindLong(10, entity.getIsMine() ? 1L: 0L);
 
        String executed = entity.getExecuted();
        if (executed != null) {
            stmt.bindString(11, executed);
        }
 
        byte[] data = entity.getData();
        if (data != null) {
            stmt.bindBlob(12, data);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, SyncTransferObject entity) {
        stmt.clearBindings();
        stmt.bindLong(1, entity.getIncrementNumber());
 
        String transferObjectId = entity.getTransferObjectId();
        if (transferObjectId != null) {
            stmt.bindString(2, transferObjectId);
        }
 
        String typeData = entity.getTypeData();
        if (typeData != null) {
            stmt.bindString(3, typeData);
        }
 
        String queryType = entity.getQueryType();
        if (queryType != null) {
            stmt.bindString(4, queryType);
        }
 
        String queryTable = entity.getQueryTable();
        if (queryTable != null) {
            stmt.bindString(5, queryTable);
        }
 
        String queryKey = entity.getQueryKey();
        if (queryKey != null) {
            stmt.bindString(6, queryKey);
        }
 
        String queryFields = entity.getQueryFields();
        if (queryFields != null) {
            stmt.bindString(7, queryFields);
        }
 
        String dateTimeSaved = entity.getDateTimeSaved();
        if (dateTimeSaved != null) {
            stmt.bindString(8, dateTimeSaved);
        }
 
        String action = entity.getAction();
        if (action != null) {
            stmt.bindString(9, action);
        }
        stmt.bindLong(10, entity.getIsMine() ? 1L: 0L);
 
        String executed = entity.getExecuted();
        if (executed != null) {
            stmt.bindString(11, executed);
        }
 
        byte[] data = entity.getData();
        if (data != null) {
            stmt.bindBlob(12, data);
        }
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.getLong(offset + 0);
    }    

    @Override
    public SyncTransferObject readEntity(Cursor cursor, int offset) {
        SyncTransferObject entity = new SyncTransferObject( //
            cursor.getLong(offset + 0), // incrementNumber
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // transferObjectId
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // typeData
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // queryType
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // queryTable
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // queryKey
            cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6), // queryFields
            cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7), // dateTimeSaved
            cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8), // action
            cursor.getShort(offset + 9) != 0, // isMine
            cursor.isNull(offset + 10) ? null : cursor.getString(offset + 10), // executed
            cursor.isNull(offset + 11) ? null : cursor.getBlob(offset + 11) // data
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, SyncTransferObject entity, int offset) {
        entity.setIncrementNumber(cursor.getLong(offset + 0));
        entity.setTransferObjectId(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setTypeData(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setQueryType(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setQueryTable(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setQueryKey(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setQueryFields(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
        entity.setDateTimeSaved(cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7));
        entity.setAction(cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8));
        entity.setIsMine(cursor.getShort(offset + 9) != 0);
        entity.setExecuted(cursor.isNull(offset + 10) ? null : cursor.getString(offset + 10));
        entity.setData(cursor.isNull(offset + 11) ? null : cursor.getBlob(offset + 11));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(SyncTransferObject entity, long rowId) {
        entity.setIncrementNumber(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(SyncTransferObject entity) {
        if(entity != null) {
            return entity.getIncrementNumber();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(SyncTransferObject entity) {
        throw new UnsupportedOperationException("Unsupported for entities with a non-null key");
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
