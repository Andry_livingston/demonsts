package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
  PRIMARY KEY(`TagId`)
*/
@Entity(nameInDb = "Tag")
public class Tag implements DatabaseEntity {

    @Id
    @SerializedName("TagId")
    @Property(nameInDb = "TagId")
    private String tagId;

    @SerializedName("TagName")
    @Property(nameInDb = "TagName")
    private String tagName;

    @Generated(hash = 1117822684)
    public Tag(String tagId, String tagName) {
        this.tagId = tagId;
        this.tagName = tagName;
    }

    @Generated(hash = 1605720318)
    public Tag() {
    }

    public String getTagId() {
        return this.tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return this.tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    @Override
    public Object getPrimaryKey() {
        return getTagId();
    }
}
