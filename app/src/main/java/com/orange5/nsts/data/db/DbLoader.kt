package com.orange5.nsts.data.db

import android.content.res.AssetManager
import com.orange5.nsts.data.preferencies.RawSharedPreferences
import com.orange5.nsts.di.modules.DbModule.DB_NAME
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject

class DbLoader @Inject constructor(
        private val assets: AssetManager,
        private val dbFile: File,
        private val preferences: RawSharedPreferences) {

    fun loadDbIfNeeded() {
        if (preferences.isDbExists.not()) loadDbFromAssets()
    }

    private fun loadDbFromAssets() {
        try {
            val inputStream = assets.open(DB_NAME)
            val outputStream = FileOutputStream(dbFile)

            inputStream.use {
                it.copyTo(outputStream)
            }
            outputStream.use {
                it.flush()
            }
            preferences.isDbExists = true
        } catch (t: Throwable) {
            Timber.e(t)
            Timber.e("DB deployment failed (see exception)")
            throw RuntimeException(t)
        }
    }
}