package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.util.Date;

/*
  PRIMARY KEY(`HistoryId`),
  FOREIGN KEY(`ProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
  FOREIGN KEY(`ToBinId`) REFERENCES `BinNumber`(`BinId`)
*/
@Entity(nameInDb = "ConsumableProductBinHistory")
public class ConsumableProductBinHistory implements DatabaseEntity {

    @Id
    @SerializedName("HistoryId")
    @Property(nameInDb = "HistoryId")
    private String historyId;

    @SerializedName("ProductId")
    @Property(nameInDb = "ProductId")
    private String productId;

    @SerializedName("Quantity")
    @Property(nameInDb = "Quantity")
    private Integer quantity;

    @SerializedName("ToBinId")
    @Property(nameInDb = "ToBinId")
    private String toBinId;

    @SerializedName("AddDate")
    @Property(nameInDb = "AddDate")
    private Date addDate;

    @Generated(hash = 1095291605)
    public ConsumableProductBinHistory(String historyId, String productId,
                                       Integer quantity, String toBinId, Date addDate) {
        this.historyId = historyId;
        this.productId = productId;
        this.quantity = quantity;
        this.toBinId = toBinId;
        this.addDate = addDate;
    }

    @Generated(hash = 852346004)
    public ConsumableProductBinHistory() {
    }

    public String getHistoryId() {
        return this.historyId;
    }

    public void setHistoryId(String historyId) {
        this.historyId = historyId;
    }

    public String getProductId() {
        return this.productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return this.quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getToBinId() {
        return this.toBinId;
    }

    public void setToBinId(String toBinId) {
        this.toBinId = toBinId;
    }

    public Date getAddDate() {
        return this.addDate;
    }

    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }

    @Override
    public Object getPrimaryKey() {
        return getHistoryId();
    }
}
