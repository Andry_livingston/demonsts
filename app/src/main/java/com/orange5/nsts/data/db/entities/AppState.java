package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

/*
  PRIMARY KEY(`AppName`)
*/
@Entity(nameInDb = "AppState")
public class AppState implements DatabaseEntity {

    @Id
    @SerializedName("AppName")
    @Property(nameInDb = "AppName")
    private String appName;

    @SerializedName("LastSyncDate")
    @Property(nameInDb = "LastSyncDate")
    private String lastSyncDate;

    @Generated(hash = 740936181)
    public AppState(String appName, String lastSyncDate) {
        this.appName = appName;
        this.lastSyncDate = lastSyncDate;
    }

    @Generated(hash = 543025143)
    public AppState() {
    }

    public String getAppName() {
        return this.appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getLastSyncDate() {
        return this.lastSyncDate;
    }

    public void setLastSyncDate(String lastSyncDate) {
        this.lastSyncDate = lastSyncDate;
    }

    @Override
    public Object getPrimaryKey() {
        return getAppName();
    }
}
