package com.orange5.nsts.data.db.entities;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.OrderProduct;
import com.orange5.nsts.data.db.dao.BinNumberDao;
import com.orange5.nsts.data.db.dao.DaoSession;
import com.orange5.nsts.data.db.dao.RentalProductDao;
import com.orange5.nsts.data.db.dao.RentalProductItemDao;
import com.orange5.nsts.service.bin.ProductBinInformation;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.ToOne;
import org.greenrobot.greendao.annotation.Transient;

import java.util.List;

/*
PRIMARY KEY(`ProductId`)
FOREIGN KEY(`DefaultBinId`) REFERENCES `BinNumber`(`BinId`)
*/
@Entity(nameInDb = "rentalProduct")
public class RentalProduct implements OrderProduct {

    @ToOne(joinProperty = "defaultBinId")
    private BinNumber defaultBin;

    @Transient
    private transient boolean filteredOut = false;

    @Override
    public void filterOut() {
        filteredOut = true;
    }

    @Override
    public boolean isFilteredOut() {
        return filteredOut;
    }

    @Transient
    private ProductBinInformation productBins;

    @ToMany(referencedJoinProperty = "productId")
    private List<RentalProductItem> productItems;

    @Id
    @SerializedName("ProductId")
    @Property(nameInDb = "ProductId")
    private String productId;

    @SerializedName("ProductNumber")
    @Property(nameInDb = "ProductNumber")
    private String productNumber;

    @SerializedName("ProductDescription")
    @Property(nameInDb = "ProductDescription")
    private String productDescription;

    @SerializedName("Manufacturer")
    @Property(nameInDb = "Manufacturer")
    private String manufacturer;

    @SerializedName("Make")
    @Property(nameInDb = "Make")
    private String make;

    @SerializedName("Model")
    @Property(nameInDb = "Model")
    private String model;

    @SerializedName("ReplacementCost")
    @Property(nameInDb = "ReplacementCost")
    private double replacementCost;//float

    @SerializedName("Picture")
    @Property(nameInDb = "Picture")
    private String picture;

    @SerializedName("AuthLevel")
    @Property(nameInDb = "AuthLevel")
    private Integer authLevel;

    @SerializedName("ManufacturerNumber")
    @Property(nameInDb = "ManufacturerNumber")
    private String manufacturerNumber;

    @SerializedName("RentalPeriod")
    @Property(nameInDb = "RentalPeriod")
    private Integer rentalPeriod;

    @SerializedName("isCustomerOwned")
    @Property(nameInDb = "isCustomerOwned")
    private byte isCustomerOwned;

    @SerializedName("WasChangedDate")
    @Property(nameInDb = "WasChangedDate")
    private String wasChangedDate;

    @SerializedName("CategoryId")
    @Property(nameInDb = "CategoryId")
    private String categoryId;

    @SerializedName("Info")
    @Property(nameInDb = "Info")
    private String info;

    @SerializedName("DefaultBinId")
    @Property(nameInDb = "DefaultBinId")
    private String defaultBinId;


    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 1702967371)
    private transient RentalProductDao myDao;

    @Generated(hash = 1741317775)
    private transient String defaultBin__resolvedKey;

    @Generated(hash = 1437606289)
    public RentalProduct(String productId, String productNumber, String productDescription,
            String manufacturer, String make, String model, double replacementCost, String picture,
            Integer authLevel, String manufacturerNumber, Integer rentalPeriod, byte isCustomerOwned,
            String wasChangedDate, String categoryId, String info, String defaultBinId) {
        this.productId = productId;
        this.productNumber = productNumber;
        this.productDescription = productDescription;
        this.manufacturer = manufacturer;
        this.make = make;
        this.model = model;
        this.replacementCost = replacementCost;
        this.picture = picture;
        this.authLevel = authLevel;
        this.manufacturerNumber = manufacturerNumber;
        this.rentalPeriod = rentalPeriod;
        this.isCustomerOwned = isCustomerOwned;
        this.wasChangedDate = wasChangedDate;
        this.categoryId = categoryId;
        this.info = info;
        this.defaultBinId = defaultBinId;
    }

    @Generated(hash = 392697125)
    public RentalProduct() {
    }

    @Override
    public double getPrice() {
        return replacementCost;
    }

    public String getProductId() {
        return this.productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductNumber() {
        return this.productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public String getProductDescription() {
        return this.productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getMake() {
        return this.make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getReplacementCost() {
        return this.replacementCost;
    }

    public void setReplacementCost(double replacementCost) {
        this.replacementCost = replacementCost;
    }

    public String getPicture() {
        return this.picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getAuthLevel() {
        return this.authLevel;
    }

    public void setAuthLevel(Integer authLevel) {
        this.authLevel = authLevel;
    }

    public String getManufacturerNumber() {
        return this.manufacturerNumber;
    }

    public void setManufacturerNumber(String manufacturerNumber) {
        this.manufacturerNumber = manufacturerNumber;
    }

    public Integer getRentalPeriod() {
        return this.rentalPeriod;
    }

    public void setRentalPeriod(Integer rentalPeriod) {
        this.rentalPeriod = rentalPeriod;
    }

    public byte getIsCustomerOwned() {
        return this.isCustomerOwned;
    }

    public void setIsCustomerOwned(byte isCustomerOwned) {
        this.isCustomerOwned = isCustomerOwned;
    }

    public String getWasChangedDate() {
        return this.wasChangedDate;
    }

    public void setWasChangedDate(String wasChangedDate) {
        this.wasChangedDate = wasChangedDate;
    }

    public String getCategoryId() {
        return this.categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getInfo() {
        return this.info;
    }

    @Override
    public int getQuantity() {
        return productItems.size();
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getDefaultBinId() {
        return this.defaultBinId;
    }

    public void setDefaultBinId(String defaultBinId) {
        this.defaultBinId = defaultBinId;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 663413155)
    public List<RentalProductItem> getProductItems() {
        if (productItems == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            RentalProductItemDao targetDao = daoSession.getRentalProductItemDao();
            List<RentalProductItem> productItemsNew = targetDao
                    ._queryRentalProduct_ProductItems(productId);
            synchronized (this) {
                if (productItems == null) {
                    productItems = productItemsNew;
                }
            }
        }
        return productItems;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 646922753)
    public synchronized void resetProductItems() {
        productItems = null;
    }

    @Override
    public Object getPrimaryKey() {
        return getProductId();
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RentalProduct that = (RentalProduct) o;

        return productId.equals(that.productId);
    }

    @Override
    public int hashCode() {
        return productId.hashCode();
    }

    @Override
    public ProductBinInformation getBinInformation() {
        productBins = new ProductBinInformation();
        for (RentalProductItem rentalProductItem : getProductItems()) {
            BinNumber containerBin = rentalProductItem.getContainerBin();
            if (containerBin == null) {
                continue;
            }
            if (TextUtils.isEmpty(containerBin.getBinNum())) {
                containerBin.setBinNum("_");
            }
            productBins.addBinQuantity(containerBin.getBinNum(), 1);
        }
        return productBins;
    }

    @Override
    public String toString() {
        return String.format("[%s] %s", productNumber, productDescription);
    }

    /** To-one relationship, resolved on first access. */
    @Generated(hash = 1462154943)
    public BinNumber getDefaultBin() {
        String __key = this.defaultBinId;
        if (defaultBin__resolvedKey == null || defaultBin__resolvedKey != __key) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            BinNumberDao targetDao = daoSession.getBinNumberDao();
            BinNumber defaultBinNew = targetDao.load(__key);
            synchronized (this) {
                defaultBin = defaultBinNew;
                defaultBin__resolvedKey = __key;
            }
        }
        return defaultBin;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1838501398)
    public void setDefaultBin(BinNumber defaultBin) {
        synchronized (this) {
            this.defaultBin = defaultBin;
            defaultBinId = defaultBin == null ? null : defaultBin.getBinId();
            defaultBin__resolvedKey = defaultBinId;
        }
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1869830968)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getRentalProductDao() : null;
    }
}
