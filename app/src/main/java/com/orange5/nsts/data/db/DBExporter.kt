package com.orange5.nsts.data.db

import com.orange5.nsts.util.format.DateFormatter
import timber.log.Timber
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class DBExporter(
        private val dbFile: File,
        private val exportFile: File) {

    fun exportDB() {
        try {
            val dbName = "NSTS_DB_${DateFormatter.currentTimeDbSimpleFormat()}.db"
            val exportDirectory = File(exportFile, dbName)
            if (exportDirectory.exists().not()) exportDirectory.parentFile.mkdirs()
            val inputStream = FileInputStream(dbFile)
            val outputStream = FileOutputStream(exportDirectory)

            inputStream.use {
                it.copyTo(outputStream)
            }
            outputStream.use {
                it.flush()
            }
        } catch (t: Throwable) {
            Timber.e("DB export failed (see exception)")
            throw RuntimeException(t)
        }
    }
}