package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.util.Date;

/*
FOREIGN KEY(`ConsumableProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
FOREIGN KEY(`RentalUUN`) REFERENCES `RentalProductItem`(`UniqueUnitNumber`),
PRIMARY KEY(`HistoryId`),
FOREIGN KEY(`ToBinId`) REFERENCES `BinNumber`(`BinId`)
*/
@Entity(nameInDb = "ProductReceivingLog")
public class ProductReceivingLog implements DatabaseEntity {

    @Id
    @SerializedName("HistoryId")
    @Property(nameInDb = "HistoryId")
    private String historyId;

    @SerializedName("ConsumableProductId")
    @Property(nameInDb = "ConsumableProductId")
    private String consumableProductId;

    @SerializedName("RentalUUN")
    @Property(nameInDb = "RentalUUN")
    private String rentalUUN;

    @SerializedName("Quantity")
    @Property(nameInDb = "Quantity")
    private Integer quantity ;

    @SerializedName("ToBinId")
    @Property(nameInDb = "ToBinId")
    private String toBinId;

    @SerializedName("AddDate")
    @Property(nameInDb = "AddDate")
    private Date addDate;

    @Generated(hash = 447688671)
    public ProductReceivingLog(String historyId, String consumableProductId,
                               String rentalUUN, Integer quantity, String toBinId, Date addDate) {
        this.historyId = historyId;
        this.consumableProductId = consumableProductId;
        this.rentalUUN = rentalUUN;
        this.quantity = quantity;
        this.toBinId = toBinId;
        this.addDate = addDate;
    }

    @Generated(hash = 233031268)
    public ProductReceivingLog() {
    }

    public String getHistoryId() {
        return this.historyId;
    }

    public void setHistoryId(String historyId) {
        this.historyId = historyId;
    }

    public String getConsumableProductId() {
        return this.consumableProductId;
    }

    public void setConsumableProductId(String consumableProductId) {
        this.consumableProductId = consumableProductId;
    }

    public String getRentalUUN() {
        return this.rentalUUN;
    }

    public void setRentalUUN(String rentalUUN) {
        this.rentalUUN = rentalUUN;
    }

    public Integer getQuantity() {
        return this.quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getToBinId() {
        return this.toBinId;
    }

    public void setToBinId(String toBinId) {
        this.toBinId = toBinId;
    }

    public Date getAddDate() {
        return this.addDate;
    }

    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }

    @Override
    public Object getPrimaryKey() {
        return getHistoryId();
    }
}
