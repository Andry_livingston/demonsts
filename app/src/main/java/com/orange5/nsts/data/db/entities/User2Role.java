package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

@Entity(nameInDb = "User2Role")
public class User2Role implements DatabaseEntity {

    @Id
    @SerializedName("UserId")
    @Property(nameInDb = "UserId")
    private String userId;

    @SerializedName("RoleId")
    @Property(nameInDb = "RoleId")
    private String roleId;

    @Generated(hash = 823693593)
    public User2Role(String userId, String roleId) {
        this.userId = userId;
        this.roleId = roleId;
    }

    @Generated(hash = 139834226)
    public User2Role() {
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoleId() {
        return this.roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    @Override
    public Object getPrimaryKey() {
        return getUserId();
    }
}
