package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
PRIMARY KEY(`FieldTypeId`)
*/
@Entity(nameInDb = "CustomerCustomFieldsTypes")
public class CustomerCustomFieldsTypes implements DatabaseEntity {

    @Id
    @SerializedName("FieldTypeId")
    @Property(nameInDb = "FieldTypeId")
    private String fieldTypeId;

    @SerializedName("FieldName")
    @Property(nameInDb = "FieldName")
    private String fieldName;

    @SerializedName("FOrder")
    @Property(nameInDb = "FOrder")
    private Integer fOrder ;

    @Generated(hash = 2133193039)
    public CustomerCustomFieldsTypes(String fieldTypeId, String fieldName,
                                     Integer fOrder) {
        this.fieldTypeId = fieldTypeId;
        this.fieldName = fieldName;
        this.fOrder = fOrder;
    }

    @Generated(hash = 554964354)
    public CustomerCustomFieldsTypes() {
    }

    public String getFieldTypeId() {
        return this.fieldTypeId;
    }

    public void setFieldTypeId(String fieldTypeId) {
        this.fieldTypeId = fieldTypeId;
    }

    public String getFieldName() {
        return this.fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public Integer getFOrder() {
        return this.fOrder;
    }

    public void setFOrder(Integer fOrder) {
        this.fOrder = fOrder;
    }

    @Override
    public Object getPrimaryKey() {
        return getFieldTypeId();
    }
}
