package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

/*
FOREIGN KEY(`ResolvedStatusId`) REFERENCES `DiscrepanciesResolvedStatus`(`ResolvedStatusId`),
FOREIGN KEY(`ConsumableProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
PRIMARY KEY(`DiscrepanciesID`),
FOREIGN KEY(`UpdatedByUserId`) REFERENCES `User`(`UserId`),
FOREIGN KEY(`DiscrepancyTypeId`) REFERENCES `DiscrepanciesType`(`TypeId`),
FOREIGN KEY(`CreatedByUserId`) REFERENCES `User`(`UserId`),
FOREIGN KEY(`RentalProductId`) REFERENCES `RentalProduct`(`ProductId`)
 */
@Entity(nameInDb = "Discrepancies")
public class Discrepancies implements DatabaseEntity {

    @Id
    @SerializedName("DiscrepanciesID")
    @Property(nameInDb = "DiscrepanciesID")
    private String discrepanciesID;

    @SerializedName("DescripancyDescription")
    @Property(nameInDb = "DescripancyDescription")
    private String descripancyDescription;

    @SerializedName("DiscrepancyTypeId")
    @Property(nameInDb = "DiscrepancyTypeId")
    private Integer discrepancyTypeId;

    @SerializedName("AddingDate")
    @Property(nameInDb = "AddingDate")
    private String addingDate;

    @SerializedName("ConsumableProductId")
    @Property(nameInDb = "ConsumableProductId")
    private String consumableProductId;

    @SerializedName("ConsumableQuantity")
    @Property(nameInDb = "ConsumableQuantity")
    private Integer consumableQuantity;

    @SerializedName("RentalProductId")
    @Property(nameInDb = "RentalProductId")
    private String rentalProductId;

    @SerializedName("RentalUniqueUnitNumber")
    @Property(nameInDb = "RentalUniqueUnitNumber")
    private String rentalUniqueUnitNumber;

    @SerializedName("Comment")
    @Property(nameInDb = "Comment")
    private String comment;

    @SerializedName("IsResolved")
    @Property(nameInDb = "IsResolved")
    private byte isResolved;

    @SerializedName("ResolvedStatusId")
    @Property(nameInDb = "ResolvedStatusId")
    private Integer resolvedStatusId;

    @SerializedName("ResolvedComment")
    @Property(nameInDb = "ResolvedComment")
    private String resolvedComment;

    @SerializedName("ResolvedDate")
    @Property(nameInDb = "ResolvedDate")
    private String resolvedDate;

    @SerializedName("UPDescription")
    @Property(nameInDb = "UPDescription")
    private String upDescription;

    @SerializedName("CreatedByUserId")
    @Property(nameInDb = "CreatedByUserId")
    private String createdByUserId;

    @SerializedName("UpdatedByUserId")
    @Property(nameInDb = "UpdatedByUserId")
    private String updatedByUserId;

    @Generated(hash = 433392852)
    public Discrepancies(String discrepanciesID, String descripancyDescription, Integer discrepancyTypeId,
            String addingDate, String consumableProductId, Integer consumableQuantity, String rentalProductId,
            String rentalUniqueUnitNumber, String comment, byte isResolved, Integer resolvedStatusId,
            String resolvedComment, String resolvedDate, String upDescription, String createdByUserId,
            String updatedByUserId) {
        this.discrepanciesID = discrepanciesID;
        this.descripancyDescription = descripancyDescription;
        this.discrepancyTypeId = discrepancyTypeId;
        this.addingDate = addingDate;
        this.consumableProductId = consumableProductId;
        this.consumableQuantity = consumableQuantity;
        this.rentalProductId = rentalProductId;
        this.rentalUniqueUnitNumber = rentalUniqueUnitNumber;
        this.comment = comment;
        this.isResolved = isResolved;
        this.resolvedStatusId = resolvedStatusId;
        this.resolvedComment = resolvedComment;
        this.resolvedDate = resolvedDate;
        this.upDescription = upDescription;
        this.createdByUserId = createdByUserId;
        this.updatedByUserId = updatedByUserId;
    }

    @Generated(hash = 1813730865)
    public Discrepancies() {
    }

    public String getDiscrepanciesID() {
        return this.discrepanciesID;
    }

    public void setDiscrepanciesID(String discrepanciesID) {
        this.discrepanciesID = discrepanciesID;
    }

    public String getDescripancyDescription() {
        return this.descripancyDescription;
    }

    public void setDescripancyDescription(String descripancyDescription) {
        this.descripancyDescription = descripancyDescription;
    }

    public Integer getDiscrepancyTypeId() {
        return this.discrepancyTypeId;
    }

    public void setDiscrepancyTypeId(Integer discrepancyTypeId) {
        this.discrepancyTypeId = discrepancyTypeId;
    }

    public String getAddingDate() {
        return this.addingDate;
    }

    public void setAddingDate(String addingDate) {
        this.addingDate = addingDate;
    }

    public String getConsumableProductId() {
        return this.consumableProductId;
    }

    public void setConsumableProductId(String consumableProductId) {
        this.consumableProductId = consumableProductId;
    }

    public Integer getConsumableQuantity() {
        return this.consumableQuantity;
    }

    public void setConsumableQuantity(Integer consumableQuantity) {
        this.consumableQuantity = consumableQuantity;
    }

    public String getRentalProductId() {
        return this.rentalProductId;
    }

    public void setRentalProductId(String rentalProductId) {
        this.rentalProductId = rentalProductId;
    }

    public String getRentalUniqueUnitNumber() {
        return this.rentalUniqueUnitNumber;
    }

    public void setRentalUniqueUnitNumber(String rentalUniqueUnitNumber) {
        this.rentalUniqueUnitNumber = rentalUniqueUnitNumber;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public byte getIsResolved() {
        return this.isResolved;
    }

    public void setIsResolved(byte isResolved) {
        this.isResolved = isResolved;
    }

    public Integer getResolvedStatusId() {
        return this.resolvedStatusId;
    }

    public void setResolvedStatusId(Integer resolvedStatusId) {
        this.resolvedStatusId = resolvedStatusId;
    }

    public String getResolvedComment() {
        return this.resolvedComment;
    }

    public void setResolvedComment(String resolvedComment) {
        this.resolvedComment = resolvedComment;
    }

    public String getResolvedDate() {
        return this.resolvedDate;
    }

    public void setResolvedDate(String resolvedDate) {
        this.resolvedDate = resolvedDate;
    }

    public String getUpDescription() {
        return this.upDescription;
    }

    public void setUpDescription(String upDescription) {
        this.upDescription = upDescription;
    }

    public String getCreatedByUserId() {
        return this.createdByUserId;
    }

    public void setCreatedByUserId(String createdByUserId) {
        this.createdByUserId = createdByUserId;
    }

    public String getUpdatedByUserId() {
        return this.updatedByUserId;
    }

    public void setUpdatedByUserId(String updatedByUserId) {
        this.updatedByUserId = updatedByUserId;
    }

    @Override
    public Object getPrimaryKey() {
        return getDiscrepanciesID();
    }
}
