package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
/*
  PRIMARY KEY(`AccessoryId`)
*/
@Entity(nameInDb = "AccessoryCrossReference")
public class AccessoryCrossReference implements DatabaseEntity {

    @Id
    @SerializedName("AccessoryId")
    @Property(nameInDb = "AccessoryId")
    private String accessoryId;

    @SerializedName("ProductId1")
    @Property(nameInDb = "ProductId1")
    private String productId1;

    @SerializedName("ProductId2")
    @Property(nameInDb = "ProductId2")
    private String productId2;

    @SerializedName("TypeReference")
    @Property(nameInDb = "TypeReference")
    private Integer typeReference;

    @Generated(hash = 1667739501)
    public AccessoryCrossReference(String accessoryId, String productId1,
                                   String productId2, Integer typeReference) {
        this.accessoryId = accessoryId;
        this.productId1 = productId1;
        this.productId2 = productId2;
        this.typeReference = typeReference;
    }

    @Generated(hash = 1068247965)
    public AccessoryCrossReference() {
    }

    public String getAccessoryId() {
        return this.accessoryId;
    }

    public void setAccessoryId(String accessoryId) {
        this.accessoryId = accessoryId;
    }

    public String getProductId1() {
        return this.productId1;
    }

    public void setProductId1(String productId1) {
        this.productId1 = productId1;
    }

    public String getProductId2() {
        return this.productId2;
    }

    public void setProductId2(String productId2) {
        this.productId2 = productId2;
    }

    public Integer getTypeReference() {
        return this.typeReference;
    }

    public void setTypeReference(Integer typeReference) {
        this.typeReference = typeReference;
    }

    @Override
    public Object getPrimaryKey() {
        return getAccessoryId();
    }
}
