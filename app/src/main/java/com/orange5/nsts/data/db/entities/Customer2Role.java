package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
  PRIMARY KEY(`CustomerId`,`CustomerRoleId`),
  FOREIGN KEY(`CustomerId`) REFERENCES `Customer`(`CustomerId`),
  FOREIGN KEY(`CustomerRoleId`) REFERENCES `CustomerRole`(`CustomerRoleId`)
*/
@Entity(nameInDb = "Customer2Role")
public class Customer2Role implements DatabaseEntity {

    @Id
    @SerializedName("CustomerId")
    @Property(nameInDb = "CustomerId")
    private String customerId;

    @SerializedName("CustomerRoleId")
    @Property(nameInDb = "CustomerRoleId")
    private String customerRoleId;

    @Generated(hash = 1315363538)
    public Customer2Role(String customerId, String customerRoleId) {
        this.customerId = customerId;
        this.customerRoleId = customerRoleId;
    }

    @Generated(hash = 1460324008)
    public Customer2Role() {
    }

    public String getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerRoleId() {
        return this.customerRoleId;
    }

    public void setCustomerRoleId(String customerRoleId) {
        this.customerRoleId = customerRoleId;
    }

    @Override
    public Object getPrimaryKey() {
        return getCustomerId();
    }
}
