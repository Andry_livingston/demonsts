package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;
import com.orange5.nsts.data.db.dao.CustomerDao;
import com.orange5.nsts.data.db.dao.DaoSession;
import com.orange5.nsts.data.db.dao.OrderDao;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.ToMany;

import java.util.List;

//    FOREIGN KEY("Status") REFERENCES "CustomerStatus"("StatusId"),
//    PRIMARY KEY("CustomerId"),
//    FOREIGN KEY("ReportsToCustomerId") REFERENCES "Customer"("CustomerId"),
//    FOREIGN KEY("CustomerPositionId") REFERENCES "CustomerPosition"("CustomerPositionId")

@Entity(nameInDb = "Customer")
public class Customer implements DatabaseEntity {

    private transient String toStringCached;

    public String toString() {
        if (toStringCached != null) {
            return toStringCached;
        }
        return toStringCached = String.format("%s %s %s", getLName(), getFName(), badgeNumber);
    }

    @ToMany(referencedJoinProperty = "creatorCustomerId")
    private List<Order> orders;

    @Id()
    @SerializedName("CustomerId")
    @Property(nameInDb = "CustomerId")
    private String customerId;

    @SerializedName("BadgeNumber")
    @Property(nameInDb = "BadgeNumber")
    private String badgeNumber;

    @SerializedName("FName")
    @Property(nameInDb = "FName")
    private String fName;

    @SerializedName("LName")
    @Property(nameInDb = "LName")
    private String lName;

    @SerializedName("Title")
    @Property(nameInDb = "Title")
    private String title;

    @SerializedName("SecurityLevel")
    @Property(nameInDb = "SecurityLevel")
    private Integer securityLevel;

    @SerializedName("BuyingLevel")
    @Property(nameInDb = "BuyingLevel")
    private Integer buyingLevel;

    @SerializedName("RentalLevel")
    @Property(nameInDb = "RentalLevel")
    private Integer rentalLevel;

    @SerializedName("Status")
    @Property(nameInDb = "Status")
    private Integer status;

    @SerializedName("ApproveCode")
    @Property(nameInDb = "ApproveCode")
    private String approvedCode;

    @SerializedName("Email")
    @Property(nameInDb = "Email")
    private String email;

    @SerializedName("Phone")
    @Property(nameInDb = "Phone")
    private String phone;

    @SerializedName("MaxProductPrice")
    @Property(nameInDb = "MaxProductPrice")
    private Double maxProductPrice;

    @SerializedName("MaxOrderPrice")
    @Property(nameInDb = "MaxOrderPrice")
    private Double maxOrderPrice;

    @SerializedName("MaxReplacementCost")
    @Property(nameInDb = "MaxReplacementCost")
    private Double maxReplacementCost;

    @SerializedName("CanNotBuy")
    @Property(nameInDb = "CanNotBuy")
    private Boolean canNotBuy;

    @SerializedName("CanNotRent")
    @Property(nameInDb = "CanNotRent")
    private Boolean canNotRent;

    @SerializedName("AuthLevel")
    @Property(nameInDb = "AuthLevel")
    private Integer authLevel;

    @SerializedName("CustomerPositionId")
    @Property(nameInDb = "CustomerPositionId")
    private String customerPositionId;

    @SerializedName("ReportsToCustomerId")
    @Property(nameInDb = "ReportsToCustomerId")
    private String reportsToCustomerId;

    @SerializedName("WasChangedDate")
    @Property(nameInDb = "WasChangedDate")
    private String wasChangedDate;

    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /**
     * Used for active entity operations.
     */
    @Generated(hash = 1697251196)
    private transient CustomerDao myDao;

    @Generated(hash = 555399249)
    public Customer(String customerId, String badgeNumber, String fName, String lName, String title,
                    Integer securityLevel, Integer buyingLevel, Integer rentalLevel, Integer status,
                    String approvedCode, String email, String phone, Double maxProductPrice,
                    Double maxOrderPrice, Double maxReplacementCost, Boolean canNotBuy, Boolean canNotRent,
                    Integer authLevel, String customerPositionId, String reportsToCustomerId,
                    String wasChangedDate) {
        this.customerId = customerId;
        this.badgeNumber = badgeNumber;
        this.fName = fName;
        this.lName = lName;
        this.title = title;
        this.securityLevel = securityLevel;
        this.buyingLevel = buyingLevel;
        this.rentalLevel = rentalLevel;
        this.status = status;
        this.approvedCode = approvedCode;
        this.email = email;
        this.phone = phone;
        this.maxProductPrice = maxProductPrice;
        this.maxOrderPrice = maxOrderPrice;
        this.maxReplacementCost = maxReplacementCost;
        this.canNotBuy = canNotBuy;
        this.canNotRent = canNotRent;
        this.authLevel = authLevel;
        this.customerPositionId = customerPositionId;
        this.reportsToCustomerId = reportsToCustomerId;
        this.wasChangedDate = wasChangedDate;
    }

    @Generated(hash = 60841032)
    public Customer() {
    }

    public String getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getBadgeNumber() {
        return this.badgeNumber;
    }

    public void setBadgeNumber(String badgeNumber) {
        this.badgeNumber = badgeNumber;
    }

    public String getFName() {
        return this.fName;
    }

    public void setFName(String fName) {
        this.fName = fName;
    }

    public String getLName() {
        return this.lName;
    }

    public void setLName(String lName) {
        this.lName = lName;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getSecurityLevel() {
        return this.securityLevel;
    }

    public void setSecurityLevel(Integer securityLevel) {
        this.securityLevel = securityLevel;
    }

    public Integer getBuyingLevel() {
        return this.buyingLevel;
    }

    public void setBuyingLevel(Integer buyingLevel) {
        this.buyingLevel = buyingLevel;
    }

    public Integer getRentalLevel() {
        return this.rentalLevel;
    }

    public void setRentalLevel(Integer rentalLevel) {
        this.rentalLevel = rentalLevel;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getApprovedCode() {
        return this.approvedCode;
    }

    public void setApprovedCode(String approvedCode) {
        this.approvedCode = approvedCode;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Double getMaxProductPrice() {
        return this.maxProductPrice;
    }

    public void setMaxProductPrice(Double maxProductPrice) {
        this.maxProductPrice = maxProductPrice;
    }

    public Double getMaxOrderPrice() {
        return this.maxOrderPrice;
    }

    public void setMaxOrderPrice(Double maxOrderPrice) {
        this.maxOrderPrice = maxOrderPrice;
    }

    public Double getMaxReplacementCost() {
        return this.maxReplacementCost;
    }

    public void setMaxReplacementCost(Double maxReplacementCost) {
        this.maxReplacementCost = maxReplacementCost;
    }

    public Boolean getCanNotBuy() {
        return this.canNotBuy;
    }

    public void setCanNotBuy(Boolean canNotBuy) {
        this.canNotBuy = canNotBuy;
    }

    public Boolean getCanNotRent() {
        return this.canNotRent;
    }

    public void setCanNotRent(Boolean canNotRent) {
        this.canNotRent = canNotRent;
    }

    public Integer getAuthLevel() {
        return this.authLevel;
    }

    public void setAuthLevel(Integer authLevel) {
        this.authLevel = authLevel;
    }

    public String getCustomerPositionId() {
        return this.customerPositionId;
    }

    public void setCustomerPositionId(String customerPositionId) {
        this.customerPositionId = customerPositionId;
    }

    public String getReportsToCustomerId() {
        return this.reportsToCustomerId;
    }

    public void setReportsToCustomerId(String reportsToCustomerId) {
        this.reportsToCustomerId = reportsToCustomerId;
    }

    public String getWasChangedDate() {
        return this.wasChangedDate;
    }

    public void setWasChangedDate(String wasChangedDate) {
        this.wasChangedDate = wasChangedDate;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 242226189)
    public List<Order> getOrders() {
        if (orders == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            OrderDao targetDao = daoSession.getOrderDao();
            List<Order> ordersNew = targetDao._queryCustomer_Orders(customerId);
            synchronized (this) {
                if (orders == null) {
                    orders = ordersNew;
                }
            }
        }
        return orders;
    }

    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 1446109810)
    public synchronized void resetOrders() {
        orders = null;
    }

    @Override
    public Object getPrimaryKey() {
        return getCustomerId();
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj instanceof Customer && ((Customer) obj).customerId.equals(customerId);
    }

    public String getToStringCached() {
        return this.toStringCached;
    }

    public void setToStringCached(String toStringCached) {
        this.toStringCached = toStringCached;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 462117449)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getCustomerDao() : null;
    }
}
