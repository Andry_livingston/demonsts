package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
PRIMARY KEY(`ColumnName`)
*/
@Entity(nameInDb = "DisplayColumnName")
public class DisplayColumnName implements DatabaseEntity {

    @Id
    @SerializedName("ColumnName")
    @Property(nameInDb = "ColumnName")
    private String columnName;

    @SerializedName("DisplayName")
    @Property(nameInDb = "DisplayName")
    private String displayName;

    @Generated(hash = 1410223897)
    public DisplayColumnName(String columnName, String displayName) {
        this.columnName = columnName;
        this.displayName = displayName;
    }

    @Generated(hash = 1102273114)
    public DisplayColumnName() {
    }

    public String getColumnName() {
        return this.columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public Object getPrimaryKey() {
        return getColumnName();
    }
}
