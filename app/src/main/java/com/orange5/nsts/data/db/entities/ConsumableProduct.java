package com.orange5.nsts.data.db.entities;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.app.App;
import com.orange5.nsts.data.db.base.OrderProduct;
import com.orange5.nsts.data.db.dao.BinNumberDao;
import com.orange5.nsts.data.db.dao.ConsumableBinDao;
import com.orange5.nsts.data.db.dao.ConsumableProductDao;
import com.orange5.nsts.data.db.dao.DaoSession;
import com.orange5.nsts.service.bin.ProductBinInformation;
import com.orange5.nsts.util.sql.SQLDebugger;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.ToOne;
import org.greenrobot.greendao.annotation.Transient;
import org.greenrobot.greendao.query.QueryBuilder;

import java.util.Date;
import java.util.List;

/*
  PRIMARY KEY(`ProductId`),
  FOREIGN KEY(`DefaultBinId`) REFERENCES `BinNumber`(`BinId`),
  FOREIGN KEY(`CategoryId`) REFERENCES `Category`(`CategoryID`)
*/
@Entity(nameInDb = "consumableProduct")
public class ConsumableProduct implements OrderProduct {

    public ConsumableBin getConsumableBin(BinNumber binNumber) {
        return daoSession.getConsumableBinDao()
                .queryBuilder()
                .where(ConsumableBinDao.Properties.ProductId.eq(productId),
                        ConsumableBinDao.Properties.BinId.eq(binNumber.getBinId()))
                .unique();
    }

    @Transient
    private transient boolean filteredOut = false;

    @Override
    public void filterOut() {
        filteredOut = true;
    }

    @Override
    public boolean isFilteredOut() {
        return filteredOut;
    }

    @Transient
    private ProductBinInformation productBins = null;

    @Id
    @SerializedName("ProductId")
    @Property(nameInDb = "ProductId")
    private String productId;

    @SerializedName("ProductNumber")
    @Property(nameInDb = "ProductNumber")
    private String productNumber;

    @SerializedName("ProductDescription")
    @Property(nameInDb = "ProductDescription")
    private String productDescription;

    @SerializedName("ManufacturerNumber")
    @Property(nameInDb = "ManufacturerNumber")
    private String manufacturerNumber;

    @SerializedName("MaterialCategory")
    @Property(nameInDb = "MaterialCategory")
    private String materialCategory;

    @SerializedName("Make")
    @Property(nameInDb = "Make")
    private String make;

    @SerializedName("Model")
    @Property(nameInDb = "Model")
    private String model;

    @SerializedName("UnitOfMeasure")
    @Property(nameInDb = "UnitOfMeasure")
    private String unitOfMeasure;

    @SerializedName("Price")
    @Property(nameInDb = "Price")
    private double price;//float

    @SerializedName("Picture")
    @Property(nameInDb = "Picture")
    private String picture;

    @SerializedName("AuthLevel")
    @Property(nameInDb = "AuthLevel")
    private Integer authLevel;

    @SerializedName("isCustomerOwned")
    @Property(nameInDb = "isCustomerOwned")
    private byte isCustomerOwned;

    @SerializedName("WasChangedDate")
    @Property(nameInDb = "WasChangedDate")
    private Date wasChangedDate;

    @SerializedName("DefaultBinId")
    @Property(nameInDb = "DefaultBinId")
    private String defaultBinId;

    @SerializedName("CategoryId")
    @Property(nameInDb = "CategoryId")
    private String categoryId;

    @SerializedName("Info")
    @Property(nameInDb = "Info")
    private String info;

    @ToOne(joinProperty = "defaultBinId")
    private BinNumber defaultBin;

    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /**
     * Used for active entity operations.
     */
    @Generated(hash = 1486532799)
    private transient ConsumableProductDao myDao;

    @Generated(hash = 1741317775)
    private transient String defaultBin__resolvedKey;

    @Generated(hash = 1651412839)
    public ConsumableProduct(String productId, String productNumber,
                             String productDescription, String manufacturerNumber,
                             String materialCategory, String make, String model,
                             String unitOfMeasure, double price, String picture, Integer authLevel,
                             byte isCustomerOwned, Date wasChangedDate, String defaultBinId,
                             String categoryId, String info) {
        this.productId = productId;
        this.productNumber = productNumber;
        this.productDescription = productDescription;
        this.manufacturerNumber = manufacturerNumber;
        this.materialCategory = materialCategory;
        this.make = make;
        this.model = model;
        this.unitOfMeasure = unitOfMeasure;
        this.price = price;
        this.picture = picture;
        this.authLevel = authLevel;
        this.isCustomerOwned = isCustomerOwned;
        this.wasChangedDate = wasChangedDate;
        this.defaultBinId = defaultBinId;
        this.categoryId = categoryId;
        this.info = info;
    }

    @Generated(hash = 753788325)
    public ConsumableProduct() {
    }

    @Override
    public String getProductId() {
        return this.productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Override
    public String getProductNumber() {
        return this.productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    @Override
    public String getProductDescription() {
        return this.productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    @Override
    public String getManufacturerNumber() {
        return this.manufacturerNumber;
    }

    public void setManufacturerNumber(String manufacturerNumber) {
        this.manufacturerNumber = manufacturerNumber;
    }

    public String getMaterialCategory() {
        return this.materialCategory;
    }

    public void setMaterialCategory(String materialCategory) {
        this.materialCategory = materialCategory;
    }

    @Override
    public String getMake() {
        return this.make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getUnitOfMeasure() {
        return this.unitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPicture() {
        return this.picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getAuthLevel() {
        return this.authLevel;
    }

    public void setAuthLevel(Integer authLevel) {
        this.authLevel = authLevel;
    }

    public byte getIsCustomerOwned() {
        return this.isCustomerOwned;
    }

    public void setIsCustomerOwned(byte isCustomerOwned) {
        this.isCustomerOwned = isCustomerOwned;
    }

    public Date getWasChangedDate() {
        return this.wasChangedDate;
    }

    public void setWasChangedDate(Date wasChangedDate) {
        this.wasChangedDate = wasChangedDate;
    }

    public String getDefaultBinId() {
        return this.defaultBinId;
    }

    public void setDefaultBinId(String defaultBinId) {
        this.defaultBinId = defaultBinId;
    }

    @Override
    public String getCategoryId() {
        return this.categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String getInfo() {
        return this.info;
    }

    @Override
    public int getQuantity() {
        return 0;
    }

    @Override
    public ProductBinInformation getBinInformation() {
        productBins = new ProductBinInformation();
        QueryBuilder<ConsumableBin> queryBuilder = App.getInstance().getDaoSession().getConsumableBinDao()
                .queryBuilder().where(ConsumableBinDao.Properties.ProductId.eq(getProductId()),
                        ConsumableBinDao.Properties.Quantity.gt(0));

        List<ConsumableBin> consumableBins = SQLDebugger.list(queryBuilder);

        for (ConsumableBin consumableBin : consumableBins) {
            if (consumableBin == null) {
                continue;
            }
            String binNum = consumableBin.getBinNumber().getBinNum();
            if (TextUtils.isEmpty(binNum)) {
                binNum = "_";
            }
            if (consumableBin.getBinNumber().getBinTypeId() == 0) {
                productBins.addBinQuantity(binNum, consumableBin.getQuantity());
            }
        }
        return productBins;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return String.format("[%s] %s", productNumber, productDescription);
    }

    /**
     * To-one relationship, resolved on first access.
     */
    @Generated(hash = 1462154943)
    public BinNumber getDefaultBin() {
        String __key = this.defaultBinId;
        if (defaultBin__resolvedKey == null || defaultBin__resolvedKey != __key) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            BinNumberDao targetDao = daoSession.getBinNumberDao();
            BinNumber defaultBinNew = targetDao.load(__key);
            synchronized (this) {
                defaultBin = defaultBinNew;
                defaultBin__resolvedKey = __key;
            }
        }
        return defaultBin;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 1838501398)
    public void setDefaultBin(BinNumber defaultBin) {
        synchronized (this) {
            this.defaultBin = defaultBin;
            defaultBinId = defaultBin == null ? null : defaultBin.getBinId();
            defaultBin__resolvedKey = defaultBinId;
        }
    }

    @Override
    public Object getPrimaryKey() {
        return getProductId();
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConsumableProduct product = (ConsumableProduct) o;

        return productId.equals(product.productId);
    }

    @Override
    public int hashCode() {
        return productId.hashCode();
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 991244985)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getConsumableProductDao() : null;
    }
}
