package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

/*
  FOREIGN KEY(`ShipingNoticeId`) REFERENCES `ShippingNotice`(`ShipingNoticeId`),
  PRIMARY KEY(`ShipingNoticeId`),
  FOREIGN KEY(`UpdatedByUserId`) REFERENCES `User`(`UserId`),
  FOREIGN KEY(`CreatedByUserId`) REFERENCES `User`(`UserId`)
  */
@Entity(nameInDb = "ShippingNotice")
public class ShippingNotice implements DatabaseEntity {

    @Id
    @SerializedName("ShipingNoticeId")
    @Property(nameInDb = "ShipingNoticeId")
    private String shippingNoticeId;

    @SerializedName("TransferNumber")
    @Property(nameInDb = "TransferNumber")
    private String transferNumber;

    @SerializedName("ConsumableProductNumber")
    @Property(nameInDb = "ConsumableProductNumber")
    private String consumableProductNumber;

    @SerializedName("ConsumableQuantity")
    @Property(nameInDb = "ConsumableQuantity")
    private Integer consumableQuantity;

    @SerializedName("ConsumableQuantityStart")
    @Property(nameInDb = "ConsumableQuantityStart")
    private Integer consumableQuantityStart;

    @SerializedName("RentalProductNumber")
    @Property(nameInDb = "RentalProductNumber")
    private String rentalProductNumber;

    @SerializedName("RentalUniqueUnitNumber")
    @Property(nameInDb = "RentalUniqueUnitNumber")
    private String rentalUniqueUnitNumber;

    @SerializedName("ShippingDate")
    @Property(nameInDb = "ShippingDate")
    private String shippingDate;

    @SerializedName("isProcessed")
    @Property(nameInDb = "isProcessed")
    private byte isProcessed;//bit

    @SerializedName("CreatedByUserId")
    @Property(nameInDb = "CreatedByUserId")
    private String createdByUserId;

    @SerializedName("UpdatedByUserId")
    @Property(nameInDb = "UpdatedByUserId")
    private String updatedByUserId;

    @Generated(hash = 860841717)
    public ShippingNotice(String shippingNoticeId, String transferNumber,
            String consumableProductNumber, Integer consumableQuantity, Integer consumableQuantityStart,
            String rentalProductNumber, String rentalUniqueUnitNumber, String shippingDate,
            byte isProcessed, String createdByUserId, String updatedByUserId) {
        this.shippingNoticeId = shippingNoticeId;
        this.transferNumber = transferNumber;
        this.consumableProductNumber = consumableProductNumber;
        this.consumableQuantity = consumableQuantity;
        this.consumableQuantityStart = consumableQuantityStart;
        this.rentalProductNumber = rentalProductNumber;
        this.rentalUniqueUnitNumber = rentalUniqueUnitNumber;
        this.shippingDate = shippingDate;
        this.isProcessed = isProcessed;
        this.createdByUserId = createdByUserId;
        this.updatedByUserId = updatedByUserId;
    }

    @Generated(hash = 62408916)
    public ShippingNotice() {
    }

    public String getShippingNoticeId() {
        return this.shippingNoticeId;
    }

    public void setShippingNoticeId(String shippingNoticeId) {
        this.shippingNoticeId = shippingNoticeId;
    }

    public String getTransferNumber() {
        return this.transferNumber;
    }

    public void setTransferNumber(String transferNumber) {
        this.transferNumber = transferNumber;
    }

    public String getConsumableProductNumber() {
        return this.consumableProductNumber;
    }

    public void setConsumableProductNumber(String consumableProductNumber) {
        this.consumableProductNumber = consumableProductNumber;
    }

    public Integer getConsumableQuantity() {
        return this.consumableQuantity;
    }

    public void setConsumableQuantity(Integer consumableQuantity) {
        this.consumableQuantity = consumableQuantity;
    }

    public Integer getConsumableQuantityStart() {
        return this.consumableQuantityStart;
    }

    public void setConsumableQuantityStart(Integer consumableQuantityStart) {
        this.consumableQuantityStart = consumableQuantityStart;
    }

    public String getRentalProductNumber() {
        return this.rentalProductNumber;
    }

    public void setRentalProductNumber(String rentalProductNumber) {
        this.rentalProductNumber = rentalProductNumber;
    }

    public String getRentalUniqueUnitNumber() {
        return this.rentalUniqueUnitNumber;
    }

    public void setRentalUniqueUnitNumber(String rentalUniqueUnitNumber) {
        this.rentalUniqueUnitNumber = rentalUniqueUnitNumber;
    }

    public String getShippingDate() {
        return this.shippingDate;
    }

    public void setShippingDate(String shippingDate) {
        this.shippingDate = shippingDate;
    }

    public byte getIsProcessed() {
        return this.isProcessed;
    }

    public void setIsProcessed(byte isProcessed) {
        this.isProcessed = isProcessed;
    }

    public boolean isProcessed() {
        return this.isProcessed != 0;
    }

    public void isProcessed(boolean isProcessed) {
        this.isProcessed = (byte) (isProcessed ? 1 : 0);
    }

    public String getCreatedByUserId() {
        return this.createdByUserId;
    }

    public void setCreatedByUserId(String createdByUserId) {
        this.createdByUserId = createdByUserId;
    }

    public String getUpdatedByUserId() {
        return this.updatedByUserId;
    }

    public void setUpdatedByUserId(String updatedByUserId) {
        this.updatedByUserId = updatedByUserId;
    }

    @Override
    public Object getPrimaryKey() {
        return getShippingNoticeId();
    }
}
