package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.util.Date;
/*
  FOREIGN KEY(`OrderId`) REFERENCES `Order`(`OrderId`),
  PRIMARY KEY(`OrderConsumableReturnId`),
  FOREIGN KEY(`ReturnedCustomerId`) REFERENCES `Customer`(`CustomerId`)
*/
@Entity(nameInDb = "OrderConsumableReturn")
public class OrderConsumableReturn implements DatabaseEntity {

    @Id
    @SerializedName("OrderConsumableReturnId")
    @Property(nameInDb = "OrderConsumableReturnId")
    private String orderConsumableReturnId;

    @SerializedName("ReturnedDate")
    @Property(nameInDb = "ReturnedDate")
    private Date returnedDate;

    @SerializedName("SignImage")
    @Property(nameInDb = "SignImage")
    private byte[] signImage;

    @SerializedName("ReturnedCustomerId")
    @Property(nameInDb = "ReturnedCustomerId")
    private String returnedCustomerId;

    @SerializedName("OrderId")
    @Property(nameInDb = "OrderId")
    private String orderId;

    @Generated(hash = 1916717048)
    public OrderConsumableReturn(String orderConsumableReturnId, Date returnedDate,
                                 byte[] signImage, String returnedCustomerId, String orderId) {
        this.orderConsumableReturnId = orderConsumableReturnId;
        this.returnedDate = returnedDate;
        this.signImage = signImage;
        this.returnedCustomerId = returnedCustomerId;
        this.orderId = orderId;
    }

    @Generated(hash = 1923945905)
    public OrderConsumableReturn() {
    }

    public String getOrderConsumableReturnId() {
        return this.orderConsumableReturnId;
    }

    public void setOrderConsumableReturnId(String orderConsumableReturnId) {
        this.orderConsumableReturnId = orderConsumableReturnId;
    }

    public Date getReturnedDate() {
        return this.returnedDate;
    }

    public void setReturnedDate(Date returnedDate) {
        this.returnedDate = returnedDate;
    }

    public byte[] getSignImage() {
        return this.signImage;
    }

    public void setSignImage(byte[] signImage) {
        this.signImage = signImage;
    }

    public String getReturnedCustomerId() {
        return this.returnedCustomerId;
    }

    public void setReturnedCustomerId(String returnedCustomerId) {
        this.returnedCustomerId = returnedCustomerId;
    }

    public String getOrderId() {
        return this.orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public Object getPrimaryKey() {
        return getOrderConsumableReturnId();
    }
}
