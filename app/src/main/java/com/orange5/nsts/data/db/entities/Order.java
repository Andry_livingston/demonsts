package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;
import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.data.db.dao.CustomerDao;
import com.orange5.nsts.data.db.dao.DaoSession;
import com.orange5.nsts.data.db.dao.OrderConsumableItemDao;
import com.orange5.nsts.data.db.dao.OrderDao;
import com.orange5.nsts.data.db.dao.OrderRentalItemDao;
import com.orange5.nsts.util.db.GUIDFactory;
import com.orange5.nsts.util.format.DateFormatter;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Keep;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.ToOne;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.ToIntFunction;

@Entity(nameInDb = "Order")
public class Order implements DatabaseEntity {

    @Override
    public Object getPrimaryKey() {
        return getOrderId();
    }

    @Override
    public DateTime getDateTime() {
        return DateFormatter.convertToDateTime(getOrderDate());
    }

    @ToMany(referencedJoinProperty = "orderId")
    private List<OrderConsumableItem> consumableItems;

    @ToMany(referencedJoinProperty = "orderId")
    private List<OrderRentalItem> rentalItems;

    @ToOne(joinProperty = "creatorCustomerId")
    private Customer customer;

    @ToOne(joinProperty = "whoPickedOrderCustomerId")
    private Customer pickCustomer;

    @Id()
    @SerializedName("OrderId")
    @Property(nameInDb = "OrderId")
    private String orderId = GUIDFactory.newUUID();

    @SerializedName("CreatorCustomerId")
    @Property(nameInDb = "CreatorCustomerId")
    private String creatorCustomerId;

    @SerializedName("OrderDate")
    @Property(nameInDb = "OrderDate")
    private String orderDate;

    @SerializedName("OrderCompleteDate")
    @Property(nameInDb = "OrderCompleteDate")
    private String orderCompleteDate;

    @SerializedName("Details")
    @Property(nameInDb = "Details")
    private String details;

    @SerializedName("SignImage")
    @Property(nameInDb = "SignImage")
    private byte[] signImage;

    @SerializedName("ReceiptText")
    @Property(nameInDb = "ReceiptText")
    private String receiptText;

    @SerializedName("OrderStatusId")
    @Property(nameInDb = "OrderStatusId")
    private Integer orderStatusId;

    @SerializedName("WhoPickedOrderCustomerId")
    @Property(nameInDb = "WhoPickedOrderCustomerId")
    private String whoPickedOrderCustomerId;

    @SerializedName("LimitOverwriteComment")
    @Property(nameInDb = "LimitOverwriteComment")
    private String limitOverwriteComment;

    @SerializedName("OrderSum")
    @Property(nameInDb = "OrderSum")
    private double orderSum;

    @SerializedName("OrderNumber")
    @Property(nameInDb = "OrderNumber")
    private String orderNumber;

    @SerializedName("ApprovedByCustomerId")
    @Property(nameInDb = "ApprovedByCustomerId")
    private String approvedByCustomerId;

    @SerializedName("CreatedByUserId")
    @Property(nameInDb = "CreatedByUserId")
    private String createdByUserId;

    @SerializedName("WasChangedDate")
    @Property(nameInDb = "WasChangedDate")
    private String wasChangedDate;

    @SerializedName("AccountingCode")
    @Property(nameInDb = "AccountingCode")
    private String accountingCode;

    @SerializedName("WebOrderNumber")
    @Property(nameInDb = "WebOrderNumber")
    private String webOrderNumber;

    @SerializedName("DeliveryDate")
    @Property(nameInDb = "DeliveryDate")
    private String deliveryDate;

    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /**
     * Used for active entity operations.
     */
    @Generated(hash = 949219203)
    private transient OrderDao myDao;


    @Generated(hash = 746181009)
    public Order(String orderId, String creatorCustomerId, String orderDate, String orderCompleteDate,
                 String details, byte[] signImage, String receiptText, Integer orderStatusId,
                 String whoPickedOrderCustomerId, String limitOverwriteComment, double orderSum,
                 String orderNumber, String approvedByCustomerId, String createdByUserId,
                 String wasChangedDate, String accountingCode, String webOrderNumber, String deliveryDate) {
        this.orderId = orderId;
        this.creatorCustomerId = creatorCustomerId;
        this.orderDate = orderDate;
        this.orderCompleteDate = orderCompleteDate;
        this.details = details;
        this.signImage = signImage;
        this.receiptText = receiptText;
        this.orderStatusId = orderStatusId;
        this.whoPickedOrderCustomerId = whoPickedOrderCustomerId;
        this.limitOverwriteComment = limitOverwriteComment;
        this.orderSum = orderSum;
        this.orderNumber = orderNumber;
        this.approvedByCustomerId = approvedByCustomerId;
        this.createdByUserId = createdByUserId;
        this.wasChangedDate = wasChangedDate;
        this.accountingCode = accountingCode;
        this.webOrderNumber = webOrderNumber;
        this.deliveryDate = deliveryDate;
    }

    @Generated(hash = 1105174599)
    public Order() {
    }

    public List<OrderItem> getAllItems() {
        List<OrderItem> orderProducts = new ArrayList<>();
        orderProducts.addAll(getConsumableItems());
        orderProducts.addAll(getRentalItems());
        return orderProducts;
    }

    public boolean isEmptyOrder() {
        return getSelectedItemCount() == 0;
    }

    public int getSelectedItemCount() {
        int consumableQuantity = this.getConsumableItems()
                .stream()
                .mapToInt(new ToIntFunction<OrderConsumableItem>() {
                    @Override
                    public int applyAsInt(OrderConsumableItem value) {
                        return value.getQuantity();
                    }
                })
                .sum();
        return consumableQuantity + this.getRentalItems().size();
    }

    public String getOrderId() {
        return this.orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getCreatorCustomerId() {
        return this.creatorCustomerId;
    }

    public void setCreatorCustomerId(String creatorCustomerId) {
        this.creatorCustomerId = creatorCustomerId;
    }

    public String getOrderDate() {
        return this.orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderCompleteDate() {
        return this.orderCompleteDate;
    }

    public void setOrderCompleteDate(String orderCompleteDate) {
        this.orderCompleteDate = orderCompleteDate;
    }

    public String getDetails() {
        return this.details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public byte[] getSignImage() {
        return this.signImage;
    }

    public void setSignImage(byte[] signImage) {
        this.signImage = signImage;
    }

    public String getReceiptText() {
        return this.receiptText;
    }

    public void setReceiptText(String receiptText) {
        this.receiptText = receiptText;
    }

    public Integer getOrderStatusId() {
        return this.orderStatusId;
    }

    public void setOrderStatusId(Integer orderStatusId) {
        this.orderStatusId = orderStatusId;
    }

    public String getWhoPickedOrderCustomerId() {
        return this.whoPickedOrderCustomerId;
    }

    public void setWhoPickedOrderCustomerId(String whoPickedOrderCustomerId) {
        this.whoPickedOrderCustomerId = whoPickedOrderCustomerId;
    }

    public String getLimitOverwriteComment() {
        return this.limitOverwriteComment;
    }

    public void setLimitOverwriteComment(String limitOverwriteComment) {
        this.limitOverwriteComment = limitOverwriteComment;
    }

    public Double getOrderSum() {
        return this.orderSum;
    }

    public void setOrderSum(Double orderSum) {
        this.orderSum = orderSum;
    }

    public String getOrderNumber() {
        return this.orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getApprovedByCustomerId() {
        return this.approvedByCustomerId;
    }

    public void setApprovedByCustomerId(String approvedByCustomerId) {
        this.approvedByCustomerId = approvedByCustomerId;
    }

    public String getCreatedByUserId() {
        return this.createdByUserId;
    }

    public void setCreatedByUserId(String createdByUserId) {
        this.createdByUserId = createdByUserId;
    }

    public String getWasChangedDate() {
        return this.wasChangedDate;
    }

    public void setWasChangedDate(String wasChangedDate) {
        this.wasChangedDate = wasChangedDate;
    }

    public String getAccountingCode() {
        return this.accountingCode;
    }

    public void setAccountingCode(String accountingCode) {
        this.accountingCode = accountingCode;
    }

    public String getWebOrderNumber() {
        return this.webOrderNumber;
    }

    public void setWebOrderNumber(String webOrderNumber) {
        this.webOrderNumber = webOrderNumber;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    @Generated(hash = 372597967)
    private transient String customer__resolvedKey;

    @Generated(hash = 792390349)
    private transient String pickCustomer__resolvedKey;

    /**
     * To-one relationship, resolved on first access.
     */
    @Generated(hash = 131872011)
    public Customer getCustomer() {
        String __key = this.creatorCustomerId;
        if (customer__resolvedKey == null || customer__resolvedKey != __key) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            CustomerDao targetDao = daoSession.getCustomerDao();
            Customer customerNew = targetDao.load(__key);
            synchronized (this) {
                customer = customerNew;
                customer__resolvedKey = __key;
            }
        }
        return customer;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 1982128108)
    public void setCustomer(Customer customer) {
        synchronized (this) {
            this.customer = customer;
            creatorCustomerId = customer == null ? null : customer.getCustomerId();
            customer__resolvedKey = creatorCustomerId;
        }
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    @Keep
    public void update() {
        setWasChangedDate(DateFormatter.currentTimeDb());
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 511825088)
    public List<OrderConsumableItem> getConsumableItems() {
        if (consumableItems == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            OrderConsumableItemDao targetDao = daoSession.getOrderConsumableItemDao();
            List<OrderConsumableItem> consumableItemsNew = targetDao
                    ._queryOrder_ConsumableItems(orderId);
            synchronized (this) {
                if (consumableItems == null) {
                    consumableItems = consumableItemsNew;
                }
            }
        }
        return consumableItems;
    }


    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 739560262)
    public synchronized void resetConsumableItems() {
        consumableItems = null;
    }


    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 870884136)
    public List<OrderRentalItem> getRentalItems() {
        if (rentalItems == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            OrderRentalItemDao targetDao = daoSession.getOrderRentalItemDao();
            List<OrderRentalItem> rentalItemsNew = targetDao._queryOrder_RentalItems(orderId);
            synchronized (this) {
                if (rentalItems == null) {
                    rentalItems = rentalItemsNew;
                }
            }
        }
        return rentalItems;
    }


    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 12507250)
    public synchronized void resetRentalItems() {
        rentalItems = null;
    }


    /**
     * To-one relationship, resolved on first access.
     */
    @Generated(hash = 707801521)
    public Customer getPickCustomer() {
        String __key = this.whoPickedOrderCustomerId;
        if (pickCustomer__resolvedKey == null || pickCustomer__resolvedKey != __key) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            CustomerDao targetDao = daoSession.getCustomerDao();
            Customer pickCustomerNew = targetDao.load(__key);
            synchronized (this) {
                pickCustomer = pickCustomerNew;
                pickCustomer__resolvedKey = __key;
            }
        }
        return pickCustomer;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 1740787921)
    public void setPickCustomer(Customer pickCustomer) {
        synchronized (this) {
            this.pickCustomer = pickCustomer;
            whoPickedOrderCustomerId = pickCustomer == null ? null : pickCustomer.getCustomerId();
            pickCustomer__resolvedKey = whoPickedOrderCustomerId;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (!Objects.equals(orderId, order.orderId)) return false;
        return Objects.equals(orderNumber, order.orderNumber);
    }

    @Override
    public int hashCode() {
        int result = orderId != null ? orderId.hashCode() : 0;
        result = 31 * result + (orderNumber != null ? orderNumber.hashCode() : 0);
        return result;
    }

    public void setOrderSum(double orderSum) {
        this.orderSum = orderSum;
    }

    public boolean isEmpty() {
        return getConsumableItems().isEmpty() && getRentalItems().isEmpty();
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 965731666)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getOrderDao() : null;
    }
}
