package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
PRIMARY KEY(`CustomerRoleId`)
*/
@Entity(nameInDb = "CustomerRole")
public class CustomerRole implements DatabaseEntity {

    @Id
    @SerializedName("CustomerRoleId")
    @Property(nameInDb = "CustomerRoleId")
    private String customerRoleId;

    @SerializedName("RoleName")
    @Property(nameInDb = "RoleName")
    private String roleName;

    @Generated(hash = 995565131)
    public CustomerRole(String customerRoleId, String roleName) {
        this.customerRoleId = customerRoleId;
        this.roleName = roleName;
    }

    @Generated(hash = 392426772)
    public CustomerRole() {
    }

    public String getCustomerRoleId() {
        return this.customerRoleId;
    }

    public void setCustomerRoleId(String customerRoleId) {
        this.customerRoleId = customerRoleId;
    }

    public String getRoleName() {
        return this.roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Override
    public Object getPrimaryKey() {
        return getCustomerRoleId();
    }
}
