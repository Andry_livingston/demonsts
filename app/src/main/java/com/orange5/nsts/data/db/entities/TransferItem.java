package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
  FOREIGN KEY(`FromBinId`) REFERENCES `BinNumber`(`BinId`),
  FOREIGN KEY(`TransferId`) REFERENCES `Transfer`(`TransferId`),
  PRIMARY KEY(`TransferItemId`),
  FOREIGN KEY(`ConsumableProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
  FOREIGN KEY(`RentalProductId`) REFERENCES `RentalProduct`(`ProductId`)
*/
@Entity(nameInDb = "TransferItem")
public class TransferItem implements DatabaseEntity {

    @SerializedName("TransferId")
    @Property(nameInDb = "TransferId")
    private String transferId;

    @SerializedName("TransferItemId")
    @Property(nameInDb = "TransferItemId")
    private String transferItemId;

    @SerializedName("ConsumableProductId")
    @Property(nameInDb = "ConsumableProductId")
    private String consumableProductId;

    @SerializedName("ConsumableQuantity")
    @Property(nameInDb = "ConsumableQuantity")
    private int consumableQuantity;

    @SerializedName("RentalProductId")
    @Property(nameInDb = "RentalProductId")
    private String rentalProductId;

    @SerializedName("RentalUniqueUnitNumber")
    @Property(nameInDb = "RentalUniqueUnitNumber")
    private String rentalUniqueUnitNumber;

    @SerializedName("FromBinId")
    @Property(nameInDb = "FromBinId")
    private String fromBinId;

    @Generated(hash = 1257299547)
    public TransferItem(String transferId, String transferItemId,
                        String consumableProductId, int consumableQuantity,
                        String rentalProductId, String rentalUniqueUnitNumber,
                        String fromBinId) {
        this.transferId = transferId;
        this.transferItemId = transferItemId;
        this.consumableProductId = consumableProductId;
        this.consumableQuantity = consumableQuantity;
        this.rentalProductId = rentalProductId;
        this.rentalUniqueUnitNumber = rentalUniqueUnitNumber;
        this.fromBinId = fromBinId;
    }

    @Generated(hash = 542022475)
    public TransferItem() {
    }

    public String getTransferId() {
        return this.transferId;
    }

    public void setTransferId(String transferId) {
        this.transferId = transferId;
    }

    public String getTransferItemId() {
        return this.transferItemId;
    }

    public void setTransferItemId(String transferItemId) {
        this.transferItemId = transferItemId;
    }

    public String getConsumableProductId() {
        return this.consumableProductId;
    }

    public void setConsumableProductId(String consumableProductId) {
        this.consumableProductId = consumableProductId;
    }

    public int getConsumableQuantity() {
        return this.consumableQuantity;
    }

    public void setConsumableQuantity(int consumableQuantity) {
        this.consumableQuantity = consumableQuantity;
    }

    public String getRentalProductId() {
        return this.rentalProductId;
    }

    public void setRentalProductId(String rentalProductId) {
        this.rentalProductId = rentalProductId;
    }

    public String getRentalUniqueUnitNumber() {
        return this.rentalUniqueUnitNumber;
    }

    public void setRentalUniqueUnitNumber(String rentalUniqueUnitNumber) {
        this.rentalUniqueUnitNumber = rentalUniqueUnitNumber;
    }

    public String getFromBinId() {
        return this.fromBinId;
    }

    public void setFromBinId(String fromBinId) {
        this.fromBinId = fromBinId;
    }

    @Override
    public Object getPrimaryKey() {
        return null;
    }
}
