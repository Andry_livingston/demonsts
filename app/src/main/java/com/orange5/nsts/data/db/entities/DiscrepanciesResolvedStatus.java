package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
PRIMARY KEY(`ResolvedStatusId`)
*/
@Entity(nameInDb = "DiscrepanciesResolvedStatus")
public class DiscrepanciesResolvedStatus implements DatabaseEntity {

    @Id
    @SerializedName("ResolvedStatusId")
    @Property(nameInDb = "ResolvedStatusId")
    private long resolvedStatusId;

    @SerializedName("ResolvedStatusName")
    @Property(nameInDb = "ResolvedStatusName")
    private String resolvedStatusName;

    @SerializedName("SelectBin")
    @Property(nameInDb = "SelectBin")
    private String selectBin;

    @SerializedName("HideBinSelector")
    @Property(nameInDb = "HideBinSelector")
    private Byte hideBinSelector;

    @Generated(hash = 534780132)
    public DiscrepanciesResolvedStatus(long resolvedStatusId, String resolvedStatusName, String selectBin,
            Byte hideBinSelector) {
        this.resolvedStatusId = resolvedStatusId;
        this.resolvedStatusName = resolvedStatusName;
        this.selectBin = selectBin;
        this.hideBinSelector = hideBinSelector;
    }

    @Generated(hash = 401661673)
    public DiscrepanciesResolvedStatus() {
    }

    public long getResolvedStatusId() {
        return this.resolvedStatusId;
    }

    public void setResolvedStatusId(long resolvedStatusId) {
        this.resolvedStatusId = resolvedStatusId;
    }

    public String getResolvedStatusName() {
        return this.resolvedStatusName;
    }

    public void setResolvedStatusName(String resolvedStatusName) {
        this.resolvedStatusName = resolvedStatusName;
    }

    public String getSelectBin() {
        return this.selectBin;
    }

    public void setSelectBin(String selectBin) {
        this.selectBin = selectBin;
    }

    public Byte getHideBinSelector() {
        return this.hideBinSelector;
    }

    public void setHideBinSelector(Byte hideBinSelector) {
        this.hideBinSelector = hideBinSelector;
    }

    @Override
    public Object getPrimaryKey() {
        return getResolvedStatusId();
    }
}
