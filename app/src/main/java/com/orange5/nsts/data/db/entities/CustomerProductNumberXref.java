package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
/*
  PRIMARY KEY(`CustomerProductNumber`),
  FOREIGN KEY(`ProductId`) REFERENCES `ConsumableProduct`(`ProductId`)
*/
@Entity(nameInDb = "CustomerProductNumberXref")
public class CustomerProductNumberXref implements DatabaseEntity {

    @Id
    @SerializedName("CustomerProductNumber")
    @Property(nameInDb = "CustomerProductNumber")
    private String customerProductNumber;

    @SerializedName("ProductId")
    @Property(nameInDb = "ProductId")
    private String productId;

    @Generated(hash = 1639807858)
    public CustomerProductNumberXref(String customerProductNumber,
                                     String productId) {
        this.customerProductNumber = customerProductNumber;
        this.productId = productId;
    }

    @Generated(hash = 1097555503)
    public CustomerProductNumberXref() {
    }

    public String getCustomerProductNumber() {
        return this.customerProductNumber;
    }

    public void setCustomerProductNumber(String customerProductNumber) {
        this.customerProductNumber = customerProductNumber;
    }

    public String getProductId() {
        return this.productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Override
    public Object getPrimaryKey() {
        return getCustomerProductNumber();
    }
}
