package com.orange5.nsts.data.db.entities;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.data.db.dao.CustomerDao;
import com.orange5.nsts.data.db.dao.DaoSession;
import com.orange5.nsts.data.db.dao.OrderDao;
import com.orange5.nsts.data.db.dao.OrderRentalItemDao;
import com.orange5.nsts.data.db.dao.RentalProductDao;
import com.orange5.nsts.service.rental.RentalItemService;
import com.orange5.nsts.util.db.GUIDFactory;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.ToOne;

/*
  FOREIGN KEY(`RentalReturnId`) REFERENCES `RentalReturn`(`RentalReturnId`),
  FOREIGN KEY(`ProductId`) REFERENCES `RentalProduct`(`ProductId`),
  FOREIGN KEY(`OrderId`) REFERENCES `Order`(`OrderId`),
  PRIMARY KEY(`OrderItemId`)
*/
@Entity(nameInDb = "OrderRentalItem")
public class OrderRentalItem implements OrderItem {

    private transient int aggregatedQuantity = 1;

    @Nullable
    public BinNumber getScannedBin() {
        return scannedBin;
    }

    public void setScannedBin(BinNumber scannedBin) {
        this.scannedBin = scannedBin;
    }

    private transient BinNumber scannedBin;

    public Integer getQuantity() {
        return aggregatedQuantity;
    }

    public void setAggregatedQuantity(int aggregatedQuantity) {
        this.aggregatedQuantity = aggregatedQuantity;
    }

    public void increaseAggregatedQuantity() {
        this.aggregatedQuantity++;
    }

    @ToOne(joinProperty = "orderId")
    private Order order;

    @ToOne(joinProperty = "returnCustomerId")
    private Customer returnedByCustomer;

    @ToOne(joinProperty = "productId")
    private RentalProduct rentalProduct;

    @Id
    @SerializedName("OrderItemId")
    @Property(nameInDb = "OrderItemId")
    private String orderItemId = GUIDFactory.newUUID();

    @SerializedName("OrderId")
    @Property(nameInDb = "OrderId")
    private String orderId;

    @SerializedName("UniqueUnitNumber")
    @Property(nameInDb = "UniqueUnitNumber")
    private String uniqueUnitNumber;

    @SerializedName("ProductNumber")
    @Property(nameInDb = "ProductNumber")
    private String productNumber;

    @SerializedName("StartDate")
    @Property(nameInDb = "StartDate")
    private String startDate;

    @SerializedName("EndDate")
    @Property(nameInDb = "EndDate")
    private String endDate;

    @SerializedName("RentPrice")
    @Property(nameInDb = "RentPrice")
    private double rentPrice;

    @SerializedName("ReturnDate")
    @Property(nameInDb = "ReturnDate")
    private String returnDate;

    @SerializedName("ReturnCustomerId")
    @Property(nameInDb = "ReturnCustomerId")
    private String returnCustomerId;

    @SerializedName("Comments")
    @Property(nameInDb = "Comments")
    private String comments;

    @SerializedName("DueDate")
    @Property(nameInDb = "DueDate")
    private String dueDate;

    @SerializedName("RentalPeriod")
    @Property(nameInDb = "RentalPeriod")
    private Integer rentalPeriod;

    @SerializedName("ProductId")
    @Property(nameInDb = "ProductId")
    private String productId;

    @SerializedName("RentalReturnNotes")
    @Property(nameInDb = "RentalReturnNotes")
    private String rentalReturnNotes;

    @SerializedName("WasDamaged")
    @Property(nameInDb = "WasDamaged")
    private boolean wasDamaged;

    @SerializedName("RentalReturnId")
    @Property(nameInDb = "RentalReturnId")
    private String rentalReturnId;

    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /**
     * Used for active entity operations.
     */
    @Generated(hash = 834435890)
    private transient OrderRentalItemDao myDao;


    @Generated(hash = 432308918)
    public OrderRentalItem() {
    }


    @Generated(hash = 560451154)
    public OrderRentalItem(String orderItemId, String orderId, String uniqueUnitNumber, String productNumber, String startDate,
                           String endDate, double rentPrice, String returnDate, String returnCustomerId, String comments, String dueDate,
                           Integer rentalPeriod, String productId, String rentalReturnNotes, boolean wasDamaged, String rentalReturnId) {
        this.orderItemId = orderItemId;
        this.orderId = orderId;
        this.uniqueUnitNumber = uniqueUnitNumber;
        this.productNumber = productNumber;
        this.startDate = startDate;
        this.endDate = endDate;
        this.rentPrice = rentPrice;
        this.returnDate = returnDate;
        this.returnCustomerId = returnCustomerId;
        this.comments = comments;
        this.dueDate = dueDate;
        this.rentalPeriod = rentalPeriod;
        this.productId = productId;
        this.rentalReturnNotes = rentalReturnNotes;
        this.wasDamaged = wasDamaged;
        this.rentalReturnId = rentalReturnId;
    }

    @Generated(hash = 1063247591)
    private transient String order__resolvedKey;

    @Generated(hash = 1355849290)
    private transient String rentalProduct__resolvedKey;

    @Generated(hash = 232782270)
    private transient String returnedByCustomer__resolvedKey;

    @Override
    public Integer getPicked() {
        return ((int) RentalItemService.getPickedCount(RentalItemService.getAllSimilarRentalItems(this)));
    }

    @Override
    public String toString() {
        return getProductNumber();
    }

    @Override
    public Double getPrice() {
        return rentPrice;
    }

    public String getOrderItemId() {
        return this.orderItemId;
    }

    public void setOrderItemId(String orderItemId) {
        this.orderItemId = orderItemId;
    }

    public String getOrderId() {
        return this.orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUniqueUnitNumber() {
        return this.uniqueUnitNumber;
    }

    public void setUniqueUnitNumber(String uniqueUnitNumber) {
        this.uniqueUnitNumber = uniqueUnitNumber;
    }

    public String getProductNumber() {
        return this.productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public String getStartDate() {
        return this.startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return this.endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public double getRentPrice() {
        return this.rentPrice;
    }

    public void setRentPrice(double rentPrice) {
        this.rentPrice = rentPrice;
    }

    public String getReturnDate() {
        return this.returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getReturnCustomerId() {
        return this.returnCustomerId;
    }

    public void setReturnCustomerId(String returnCustomerId) {
        this.returnCustomerId = returnCustomerId;
    }

    public String getComments() {
        return this.comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getDueDate() {
        return this.dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public Integer getRentalPeriod() {
        return this.rentalPeriod;
    }

    public void setRentalPeriod(Integer rentalPeriod) {
        this.rentalPeriod = rentalPeriod;
    }

    @Override
    public String getProductId() {
        return this.productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Nullable
    public String getRentalReturnNotes() {
        return this.rentalReturnNotes;
    }

    public void setRentalReturnNotes(String rentalReturnNotes) {
        this.rentalReturnNotes = rentalReturnNotes;
    }


    public String getRentalReturnId() {
        return this.rentalReturnId;
    }

    public void setRentalReturnId(String rentalReturnId) {
        this.rentalReturnId = rentalReturnId;
    }

    /**
     * To-one relationship, resolved on first access.
     */
    @Generated(hash = 1955925347)
    public Order getOrder() {
        String __key = this.orderId;
        if (order__resolvedKey == null || order__resolvedKey != __key) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            OrderDao targetDao = daoSession.getOrderDao();
            Order orderNew = targetDao.load(__key);
            synchronized (this) {
                order = orderNew;
                order__resolvedKey = __key;
            }
        }
        return order;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 659365769)
    public void setOrder(Order order) {
        synchronized (this) {
            this.order = order;
            orderId = order == null ? null : order.getOrderId();
            order__resolvedKey = orderId;
        }
    }

    /**
     * To-one relationship, resolved on first access.
     */
    @Generated(hash = 1429405704)
    public RentalProduct getRentalProduct() {
        String __key = this.productId;
        if (rentalProduct__resolvedKey == null || rentalProduct__resolvedKey != __key) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            RentalProductDao targetDao = daoSession.getRentalProductDao();
            RentalProduct rentalProductNew = targetDao.load(__key);
            synchronized (this) {
                rentalProduct = rentalProductNew;
                rentalProduct__resolvedKey = __key;
            }
        }
        return rentalProduct;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 249590766)
    public void setRentalProduct(RentalProduct rentalProduct) {
        synchronized (this) {
            this.rentalProduct = rentalProduct;
            productId = rentalProduct == null ? null : rentalProduct.getProductId();
            rentalProduct__resolvedKey = productId;
        }
    }

    @Override
    public Object getPrimaryKey() {
        return getOrderItemId();
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof OrderRentalItem && ((OrderRentalItem) obj).getOrderItemId().equals(orderItemId);
    }

    public boolean getWasDamaged() {
        return this.wasDamaged;
    }


    public void setWasDamaged(boolean wasDamaged) {
        this.wasDamaged = wasDamaged;
    }


    /**
     * To-one relationship, resolved on first access.
     */
    @Generated(hash = 1292422765)
    public Customer getReturnedByCustomer() {
        String __key = this.returnCustomerId;
        if (returnedByCustomer__resolvedKey == null || returnedByCustomer__resolvedKey != __key) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            CustomerDao targetDao = daoSession.getCustomerDao();
            Customer returnedByCustomerNew = targetDao.load(__key);
            synchronized (this) {
                returnedByCustomer = returnedByCustomerNew;
                returnedByCustomer__resolvedKey = __key;
            }
        }
        return returnedByCustomer;
    }


    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 1680244522)
    public void setReturnedByCustomer(Customer returnedByCustomer) {
        synchronized (this) {
            this.returnedByCustomer = returnedByCustomer;
            returnCustomerId = returnedByCustomer == null ? null : returnedByCustomer.getCustomerId();
            returnedByCustomer__resolvedKey = returnCustomerId;
        }
    }


    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 910251718)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getOrderRentalItemDao() : null;
    }
}
