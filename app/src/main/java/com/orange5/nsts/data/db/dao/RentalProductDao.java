package com.orange5.nsts.data.db.dao;

import java.util.List;
import java.util.ArrayList;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.SqlUtils;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

import com.orange5.nsts.data.db.entities.BinNumber;

import com.orange5.nsts.data.db.entities.RentalProduct;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "rentalProduct".
*/
public class RentalProductDao extends AbstractDao<RentalProduct, String> {

    public static final String TABLENAME = "rentalProduct";

    /**
     * Properties of entity RentalProduct.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property ProductId = new Property(0, String.class, "productId", true, "ProductId");
        public final static Property ProductNumber = new Property(1, String.class, "productNumber", false, "ProductNumber");
        public final static Property ProductDescription = new Property(2, String.class, "productDescription", false, "ProductDescription");
        public final static Property Manufacturer = new Property(3, String.class, "manufacturer", false, "Manufacturer");
        public final static Property Make = new Property(4, String.class, "make", false, "Make");
        public final static Property Model = new Property(5, String.class, "model", false, "Model");
        public final static Property ReplacementCost = new Property(6, double.class, "replacementCost", false, "ReplacementCost");
        public final static Property Picture = new Property(7, String.class, "picture", false, "Picture");
        public final static Property AuthLevel = new Property(8, Integer.class, "authLevel", false, "AuthLevel");
        public final static Property ManufacturerNumber = new Property(9, String.class, "manufacturerNumber", false, "ManufacturerNumber");
        public final static Property RentalPeriod = new Property(10, Integer.class, "rentalPeriod", false, "RentalPeriod");
        public final static Property IsCustomerOwned = new Property(11, byte.class, "isCustomerOwned", false, "isCustomerOwned");
        public final static Property WasChangedDate = new Property(12, String.class, "wasChangedDate", false, "WasChangedDate");
        public final static Property CategoryId = new Property(13, String.class, "categoryId", false, "CategoryId");
        public final static Property Info = new Property(14, String.class, "info", false, "Info");
        public final static Property DefaultBinId = new Property(15, String.class, "defaultBinId", false, "DefaultBinId");
    }

    private DaoSession daoSession;


    public RentalProductDao(DaoConfig config) {
        super(config);
    }
    
    public RentalProductDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"rentalProduct\" (" + //
                "\"ProductId\" TEXT PRIMARY KEY NOT NULL ," + // 0: productId
                "\"ProductNumber\" TEXT," + // 1: productNumber
                "\"ProductDescription\" TEXT," + // 2: productDescription
                "\"Manufacturer\" TEXT," + // 3: manufacturer
                "\"Make\" TEXT," + // 4: make
                "\"Model\" TEXT," + // 5: model
                "\"ReplacementCost\" REAL NOT NULL ," + // 6: replacementCost
                "\"Picture\" TEXT," + // 7: picture
                "\"AuthLevel\" INTEGER," + // 8: authLevel
                "\"ManufacturerNumber\" TEXT," + // 9: manufacturerNumber
                "\"RentalPeriod\" INTEGER," + // 10: rentalPeriod
                "\"isCustomerOwned\" INTEGER NOT NULL ," + // 11: isCustomerOwned
                "\"WasChangedDate\" TEXT," + // 12: wasChangedDate
                "\"CategoryId\" TEXT," + // 13: categoryId
                "\"Info\" TEXT," + // 14: info
                "\"DefaultBinId\" TEXT);"); // 15: defaultBinId
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"rentalProduct\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, RentalProduct entity) {
        stmt.clearBindings();
 
        String productId = entity.getProductId();
        if (productId != null) {
            stmt.bindString(1, productId);
        }
 
        String productNumber = entity.getProductNumber();
        if (productNumber != null) {
            stmt.bindString(2, productNumber);
        }
 
        String productDescription = entity.getProductDescription();
        if (productDescription != null) {
            stmt.bindString(3, productDescription);
        }
 
        String manufacturer = entity.getManufacturer();
        if (manufacturer != null) {
            stmt.bindString(4, manufacturer);
        }
 
        String make = entity.getMake();
        if (make != null) {
            stmt.bindString(5, make);
        }
 
        String model = entity.getModel();
        if (model != null) {
            stmt.bindString(6, model);
        }
        stmt.bindDouble(7, entity.getReplacementCost());
 
        String picture = entity.getPicture();
        if (picture != null) {
            stmt.bindString(8, picture);
        }
 
        Integer authLevel = entity.getAuthLevel();
        if (authLevel != null) {
            stmt.bindLong(9, authLevel);
        }
 
        String manufacturerNumber = entity.getManufacturerNumber();
        if (manufacturerNumber != null) {
            stmt.bindString(10, manufacturerNumber);
        }
 
        Integer rentalPeriod = entity.getRentalPeriod();
        if (rentalPeriod != null) {
            stmt.bindLong(11, rentalPeriod);
        }
        stmt.bindLong(12, entity.getIsCustomerOwned());
 
        String wasChangedDate = entity.getWasChangedDate();
        if (wasChangedDate != null) {
            stmt.bindString(13, wasChangedDate);
        }
 
        String categoryId = entity.getCategoryId();
        if (categoryId != null) {
            stmt.bindString(14, categoryId);
        }
 
        String info = entity.getInfo();
        if (info != null) {
            stmt.bindString(15, info);
        }
 
        String defaultBinId = entity.getDefaultBinId();
        if (defaultBinId != null) {
            stmt.bindString(16, defaultBinId);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, RentalProduct entity) {
        stmt.clearBindings();
 
        String productId = entity.getProductId();
        if (productId != null) {
            stmt.bindString(1, productId);
        }
 
        String productNumber = entity.getProductNumber();
        if (productNumber != null) {
            stmt.bindString(2, productNumber);
        }
 
        String productDescription = entity.getProductDescription();
        if (productDescription != null) {
            stmt.bindString(3, productDescription);
        }
 
        String manufacturer = entity.getManufacturer();
        if (manufacturer != null) {
            stmt.bindString(4, manufacturer);
        }
 
        String make = entity.getMake();
        if (make != null) {
            stmt.bindString(5, make);
        }
 
        String model = entity.getModel();
        if (model != null) {
            stmt.bindString(6, model);
        }
        stmt.bindDouble(7, entity.getReplacementCost());
 
        String picture = entity.getPicture();
        if (picture != null) {
            stmt.bindString(8, picture);
        }
 
        Integer authLevel = entity.getAuthLevel();
        if (authLevel != null) {
            stmt.bindLong(9, authLevel);
        }
 
        String manufacturerNumber = entity.getManufacturerNumber();
        if (manufacturerNumber != null) {
            stmt.bindString(10, manufacturerNumber);
        }
 
        Integer rentalPeriod = entity.getRentalPeriod();
        if (rentalPeriod != null) {
            stmt.bindLong(11, rentalPeriod);
        }
        stmt.bindLong(12, entity.getIsCustomerOwned());
 
        String wasChangedDate = entity.getWasChangedDate();
        if (wasChangedDate != null) {
            stmt.bindString(13, wasChangedDate);
        }
 
        String categoryId = entity.getCategoryId();
        if (categoryId != null) {
            stmt.bindString(14, categoryId);
        }
 
        String info = entity.getInfo();
        if (info != null) {
            stmt.bindString(15, info);
        }
 
        String defaultBinId = entity.getDefaultBinId();
        if (defaultBinId != null) {
            stmt.bindString(16, defaultBinId);
        }
    }

    @Override
    protected final void attachEntity(RentalProduct entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    @Override
    public String readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0);
    }    

    @Override
    public RentalProduct readEntity(Cursor cursor, int offset) {
        RentalProduct entity = new RentalProduct( //
            cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0), // productId
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // productNumber
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // productDescription
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // manufacturer
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // make
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // model
            cursor.getDouble(offset + 6), // replacementCost
            cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7), // picture
            cursor.isNull(offset + 8) ? null : cursor.getInt(offset + 8), // authLevel
            cursor.isNull(offset + 9) ? null : cursor.getString(offset + 9), // manufacturerNumber
            cursor.isNull(offset + 10) ? null : cursor.getInt(offset + 10), // rentalPeriod
            (byte) cursor.getShort(offset + 11), // isCustomerOwned
            cursor.isNull(offset + 12) ? null : cursor.getString(offset + 12), // wasChangedDate
            cursor.isNull(offset + 13) ? null : cursor.getString(offset + 13), // categoryId
            cursor.isNull(offset + 14) ? null : cursor.getString(offset + 14), // info
            cursor.isNull(offset + 15) ? null : cursor.getString(offset + 15) // defaultBinId
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, RentalProduct entity, int offset) {
        entity.setProductId(cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0));
        entity.setProductNumber(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setProductDescription(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setManufacturer(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setMake(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setModel(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setReplacementCost(cursor.getDouble(offset + 6));
        entity.setPicture(cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7));
        entity.setAuthLevel(cursor.isNull(offset + 8) ? null : cursor.getInt(offset + 8));
        entity.setManufacturerNumber(cursor.isNull(offset + 9) ? null : cursor.getString(offset + 9));
        entity.setRentalPeriod(cursor.isNull(offset + 10) ? null : cursor.getInt(offset + 10));
        entity.setIsCustomerOwned((byte) cursor.getShort(offset + 11));
        entity.setWasChangedDate(cursor.isNull(offset + 12) ? null : cursor.getString(offset + 12));
        entity.setCategoryId(cursor.isNull(offset + 13) ? null : cursor.getString(offset + 13));
        entity.setInfo(cursor.isNull(offset + 14) ? null : cursor.getString(offset + 14));
        entity.setDefaultBinId(cursor.isNull(offset + 15) ? null : cursor.getString(offset + 15));
     }
    
    @Override
    protected final String updateKeyAfterInsert(RentalProduct entity, long rowId) {
        return entity.getProductId();
    }
    
    @Override
    public String getKey(RentalProduct entity) {
        if(entity != null) {
            return entity.getProductId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(RentalProduct entity) {
        return entity.getProductId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
    private String selectDeep;

    protected String getSelectDeep() {
        if (selectDeep == null) {
            StringBuilder builder = new StringBuilder("SELECT ");
            SqlUtils.appendColumns(builder, "T", getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T0", daoSession.getBinNumberDao().getAllColumns());
            builder.append(" FROM rentalProduct T");
            builder.append(" LEFT JOIN BinNumber T0 ON T.\"DefaultBinId\"=T0.\"BinId\"");
            builder.append(' ');
            selectDeep = builder.toString();
        }
        return selectDeep;
    }
    
    protected RentalProduct loadCurrentDeep(Cursor cursor, boolean lock) {
        RentalProduct entity = loadCurrent(cursor, 0, lock);
        int offset = getAllColumns().length;

        BinNumber defaultBin = loadCurrentOther(daoSession.getBinNumberDao(), cursor, offset);
        entity.setDefaultBin(defaultBin);

        return entity;    
    }

    public RentalProduct loadDeep(Long key) {
        assertSinglePk();
        if (key == null) {
            return null;
        }

        StringBuilder builder = new StringBuilder(getSelectDeep());
        builder.append("WHERE ");
        SqlUtils.appendColumnsEqValue(builder, "T", getPkColumns());
        String sql = builder.toString();
        
        String[] keyArray = new String[] { key.toString() };
        Cursor cursor = db.rawQuery(sql, keyArray);
        
        try {
            boolean available = cursor.moveToFirst();
            if (!available) {
                return null;
            } else if (!cursor.isLast()) {
                throw new IllegalStateException("Expected unique result, but count was " + cursor.getCount());
            }
            return loadCurrentDeep(cursor, true);
        } finally {
            cursor.close();
        }
    }
    
    /** Reads all available rows from the given cursor and returns a list of new ImageTO objects. */
    public List<RentalProduct> loadAllDeepFromCursor(Cursor cursor) {
        int count = cursor.getCount();
        List<RentalProduct> list = new ArrayList<RentalProduct>(count);
        
        if (cursor.moveToFirst()) {
            if (identityScope != null) {
                identityScope.lock();
                identityScope.reserveRoom(count);
            }
            try {
                do {
                    list.add(loadCurrentDeep(cursor, false));
                } while (cursor.moveToNext());
            } finally {
                if (identityScope != null) {
                    identityScope.unlock();
                }
            }
        }
        return list;
    }
    
    protected List<RentalProduct> loadDeepAllAndCloseCursor(Cursor cursor) {
        try {
            return loadAllDeepFromCursor(cursor);
        } finally {
            cursor.close();
        }
    }
    

    /** A raw-style query where you can pass any WHERE clause and arguments. */
    public List<RentalProduct> queryDeep(String where, String... selectionArg) {
        Cursor cursor = db.rawQuery(getSelectDeep() + where, selectionArg);
        return loadDeepAllAndCloseCursor(cursor);
    }
 
}
