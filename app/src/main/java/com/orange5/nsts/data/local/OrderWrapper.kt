package com.orange5.nsts.data.local

import com.orange5.nsts.data.db.entities.ConsumableProduct
import com.orange5.nsts.data.db.entities.Order
import com.orange5.nsts.data.db.entities.OrderConsumableItem
import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.data.db.entities.RentalProduct
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

class OrderWrapper(val order: Order,
                   val rentalItems: MutableList<OrderRentalItem> = mutableListOf(),
                   val consumableItems: MutableList<OrderConsumableItem> = mutableListOf()) {

    val orderItems
        get() = getAggregatedRentalItems() + consumableItems
    val selectedItemCount: Int = orderItems.size
    val emptyOrder
        get() = rentalItems.isEmpty() && consumableItems.isEmpty()

    val orderSum: Double
        get() {
            val rentalsSum = rentalItems.sumByDouble { it.price }
            val consumablesSum = consumableItems.sumByDouble { it.price * it.quantity }
            return (rentalsSum + consumablesSum).round()
        }

    fun setRentalItems(product: RentalProduct, quantity: Int) {
        val rentals = rentalItems.filter { it.productId == product.productId }
        if (rentals.size < quantity) addRentalProduct(product, quantity - rentals.size)
        else repeat(rentals.size - quantity) {
            removeRentalItems(rentals[it])
        }
    }

    fun setConsumableItems(product: ConsumableProduct, quantity: Int, hasBackorder: Boolean) {
        val consumable = consumableItems.find { it.productId == product.productId }
        if (consumable != null) {
            consumable.quantity = quantity
            consumable.isBackorder(hasBackorder)
            updateConsumable(consumable)
        } else {
            addConsumable(product, quantity, hasBackorder)
        }
    }

    fun getRentalOrNull(item: RentalProduct) = rentalItems.find { it.productId == item.productId }

    fun getConsumableOrNull(item: ConsumableProduct) = consumableItems.find { it.productId == item.productId }

    fun removeRentalItems(item: OrderRentalItem) = rentalItems.remove(item)

    fun removeRentalItems(productNumber: String, quantity: Int) {
        var count = quantity
        rentalItems.removeIf {
            if (productNumber == it.productNumber && count != 0) {
                count--
                true
            } else {
                false
            }
        }
    }

    fun removeRentalItems(productNumber: String) {
        rentalItems.removeAll { productNumber == it.productNumber }
    }

    fun removeAllRental() {
        rentalItems.clear()
    }

    fun updateConsumable(item: OrderConsumableItem) {
        val consumable = consumableItems.find { it.orderItemId == item.orderItemId }
        if (consumable != null) {
            consumable.quantity = item.quantity
            consumable.picked = item.picked
            consumable.isBackOrder = item.isBackOrder
        } else {
            consumableItems.add(item)
        }
    }

    fun removeAllConsumables() {
        consumableItems.clear()
    }

    fun removeConsumableItems(vararg item: OrderConsumableItem) {
        consumableItems.removeAll(item)
    }

    private fun addRentalProduct(product: RentalProduct, quantity: Int) {
        repeat(quantity) {
            val rentalItem = OrderRentalItem()
            rentalItem.orderId = order.orderId
            rentalItem.productId = product.productId
            rentalItem.uniqueUnitNumber = null
            rentalItem.productNumber = product.productNumber
            rentalItem.rentPrice = product.price
            rentalItem.rentalPeriod = product.rentalPeriod
            rentalItem.returnCustomerId = order.creatorCustomerId
            addRentalItem(rentalItem)
        }
    }

    private fun addRentalItem(vararg item: OrderRentalItem) {
        rentalItems.addAll(item)
    }

    private fun addConsumable(product: ConsumableProduct, quantity: Int, hasBackorder: Boolean) {
        val consumable = OrderConsumableItem()
        consumable.quantity = quantity
        consumable.isBackorder(hasBackorder)
        consumable.price = product.price
        consumable.orderId = order.orderId
        consumable.productId = product.productId

        consumableItems.add(consumable)
    }

    private fun getAggregatedRentalItems(): List<OrderRentalItem> {
        val uniqueItems: MutableMap<String, OrderRentalItem?> = LinkedHashMap()
        rentalItems.forEach { item ->
            val productNumber = item.productNumber
            if (uniqueItems.containsKey(productNumber)) {
                uniqueItems[productNumber]!!.increaseAggregatedQuantity()
                return@forEach
            }
            item.setAggregatedQuantity(1)
            uniqueItems[productNumber] = item
        }
        return ArrayList(uniqueItems.values).filterNotNull()
    }

    fun getRentalDiff(db: List<OrderRentalItem>): RentalDiff {
        val diff = RentalDiff()
        val set = rentalItems.map { it.productId }.toSet()
        set.forEach { id ->
            val list1 = rentalItems.filter { it.productId == id }
            val list2 = db.filter { it.productId == id }
            if (list1.size > list2.size) {
                diff.toInsert.addAll(list1 - list2)
            } else if (list1.size < list2.size) {
                diff.toDelete.addAll(list2 - list1)
            }
        }
        return diff
    }

    fun getConsumableDiff(db: List<OrderConsumableItem>): ConsumableDiff {
        val diff = ConsumableDiff()
        consumableItems.forEach { consumable ->
            val item = db.find { it.productId == consumable.productId }
            if (item != null) diff.toUpdate.add(consumable)
            else diff.toInsert.add(consumable)
        }
        return diff
    }

    companion object {
        @JvmStatic
        fun from(order: Order) = OrderWrapper(order, order.rentalItems, order.consumableItems)
    }

    private fun Double.round() = BigDecimal(this).setScale(2, RoundingMode.HALF_EVEN).toDouble()

}

data class ConsumableDiff(val toInsert: MutableList<OrderConsumableItem> = mutableListOf(),
                          val toUpdate: MutableList<OrderConsumableItem> = mutableListOf())

data class RentalDiff(val toInsert: MutableList<OrderRentalItem> = mutableListOf(),
                      val toDelete: MutableList<OrderRentalItem> = mutableListOf())