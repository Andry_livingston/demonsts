package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

@Entity(nameInDb = "TransactionType")
public class TransactionType implements DatabaseEntity {

    @Id
    @SerializedName("TransactionTypeId")
    @Property(nameInDb = "TransactionTypeId")
    private int transactionTypeId;

    @SerializedName("TransactionTypeName")
    @Property(nameInDb = "TransactionTypeName")
    private String transactionTypeName;

    @Generated(hash = 271042288)
    public TransactionType(int transactionTypeId, String transactionTypeName) {
        this.transactionTypeId = transactionTypeId;
        this.transactionTypeName = transactionTypeName;
    }

    @Generated(hash = 900797809)
    public TransactionType() {
    }

    public int getTransactionTypeId() {
        return this.transactionTypeId;
    }

    public void setTransactionTypeId(int transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }

    public String getTransactionTypeName() {
        return this.transactionTypeName;
    }

    public void setTransactionTypeName(String transactionTypeName) {
        this.transactionTypeName = transactionTypeName;
    }

    @Override
    public Object getPrimaryKey() {
        return getTransactionTypeId();
    }
}
