package com.orange5.nsts.data.db.entities;

import com.google.gson.annotations.SerializedName;
import com.orange5.nsts.data.db.base.DatabaseEntity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

@Entity(nameInDb = "User")
public class User implements DatabaseEntity {

    @Id
    @SerializedName("UserId")
    @Property(nameInDb = "UserId")
    private String userId;

    @SerializedName("UserName")
    @Property(nameInDb = "UserName")
    private String userName;

    @SerializedName("Password")
    @Property(nameInDb = "Password")
    private String password;

    @SerializedName("FirstName")
    @Property(nameInDb = "FirstName")
    private String firstName;

    @SerializedName("LastName")
    @Property(nameInDb = "LastName")
    private String lastName;

    @SerializedName("Email")
    @Property(nameInDb = "Email")
    private String email;

    @SerializedName("Phone")
    @Property(nameInDb = "Phone")
    private String phone;

    @Generated(hash = 422237552)
    public User(String userId, String userName, String password, String firstName,
                String lastName, String email, String phone) {
        this.userId = userId;
        this.userName = userName;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
    }

    @Generated(hash = 586692638)
    public User() {
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public Object getPrimaryKey() {
        return getUserId();
    }
}
