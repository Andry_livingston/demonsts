package com.orange5.nsts.data.db.base;

import com.orange5.nsts.data.db.dao.DaoSession;

import org.joda.time.DateTime;

public interface DatabaseEntity {

    Object getPrimaryKey();

    default DateTime getDateTime() {
        throw new IllegalStateException("implement this method in " + getClass() + " before using it");
    }

    default void delete() {
    }

    default void __setDaoSession(DaoSession daoSession) {
    }
}
