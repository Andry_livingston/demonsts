package com.orange5.nsts.sync.converter

import com.google.gson.Gson
import com.orange5.nsts.sync.DataObject
import com.orange5.nsts.sync.Parameter
import com.orange5.nsts.sync.QueryType
import com.orange5.nsts.sync.local.LoggedSqlQuery
import com.orange5.nsts.sync.local.ServerSqlUtils
import com.orange5.nsts.util.SignatureManager
import com.orange5.nsts.util.format.DateFormatter
import org.joda.time.DateTime
import timber.log.Timber
import java.util.*

class ToServerConverter(private val gson: Gson) : Converter() {

    fun convert(query: LoggedSqlQuery): String {
        val sql = when (query.queryType) {
            QueryType.DELETE -> convertDelete(query)
            QueryType.UPDATE -> convertUpdate(query)
            QueryType.INSERT -> convertInsert(query)
            else -> {
                Timber.d("Query is null")
                ""
            }
        }
        val parameters = fillValuesArgsFromRaw(query)
        val dataObject = DataObject(sql, parameters, query.queryType.title)
        val json = gson.toJson(dataObject)
        return json.stringToBase64()
    }

    private fun convertInsert(query: LoggedSqlQuery): String {
        var sql = ServerSqlUtils.createSqlInsert(query.tableData)
        query.args
                .forEachIndexed { i, it ->
                    var newValue = "NULL"
                    if (it != null) newValue = "@$i"
                    sql = sql.replaceFirst("?", newValue)
                }
        return sql
    }

    private fun convertUpdate(query: LoggedSqlQuery): String {
        getQueryFields(query)
        val sql = ServerSqlUtils.createSqlUpdate(query.tableData)
        return addParameterName(sql, query.args)
    }

    private fun convertDelete(query: LoggedSqlQuery): String {
        val sql = ServerSqlUtils.createSqlDelete(query.tableData)
        return addParameterName(sql, query.args)
    }

    private fun fillValuesArgsFromRaw(query: LoggedSqlQuery): List<Parameter> {
        val parameters = mutableListOf<Parameter>()
        for ((index, value) in query.args.withIndex()) {
            if (value != null) {
                parameters += getParameters(value, index)
            }
        }
        return parameters
    }

    private fun addParameterName(rawSql: String, parameters: Array<Any?>): String {
        var sql = rawSql
        parameters
                .forEachIndexed { i, it ->
                    if (it != null) {
                        sql = sql.replaceFirst("?", "@$i")
                    }
                }
        return sql
    }

    private fun getQueryFields(query: LoggedSqlQuery): LoggedSqlQuery {
        val updateColumns = mutableListOf<String>()
        query.args
                .dropLast(1)
                .forEachIndexed { i, it ->
                    if (it != null) {
                        updateColumns += query.tableData.columns[i]
                    }
                }
        query.tableData.columns = updateColumns.toTypedArray()
        return query
    }

    private fun getParameters(value: Any, index: Int) = when (value) {
        is Long ->
            if (value > 946684800) mapParameters(index, "\"${DateFormatter.fromTimeStamp(value)}\"", "DateTime", "DateTime2")
            else mapParameters(index, value, "Int32", "Int")
        is Int, is Short -> mapParameters(index, value, "Int32", "Int")
        is Double, is Float -> mapParameters(index, value, "Double", "Float")
        is Boolean -> mapParameters(index, value, "Boolean", "Bit")
        is ByteArray -> mapParameters(index,

                "\"${SignatureManager.bytesToBase64(value)}\"",
                "Byte[]", "VarBinary")
        is DateTime -> mapParameters(index, "\"$value\"", "DateTime", "DateTime2")
        is String -> when {
            value.toString().isGuid() -> mapParameters(index, "\"$value\"", "Guid", "UniqueIdentifier")
            value.toString().isDate() -> mapParameters(index, "\"$value\"", "DateTime", "DateTime2")
            else -> mapParameters(index, "\"$value\"", "String", "NVarChar")
        }
        else -> mapParameters(index, "\"$value\"", "String", "NVarChar")
    }

    private fun mapParameters(index: Int, value: Any, valueType: String, sqlType: String) =
            Parameter(parameterName = "@$index",
                    value = "$value",
                    sqlDbType = sqlType,
                    valueType = "System.$valueType, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")
}

private fun String.isGuid() =
        try {
            UUID.fromString(this)
            true
        } catch (t: Throwable) {
            false
        }

private fun String.isDate() =
        try {
            DateFormatter.getDateTime(this)
            true
        } catch (t: Throwable) {
            false
        }