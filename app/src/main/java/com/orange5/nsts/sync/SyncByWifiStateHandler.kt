package com.orange5.nsts.sync

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities.NET_CAPABILITY_INTERNET
import android.net.NetworkCapabilities.TRANSPORT_WIFI
import android.net.NetworkRequest
import com.orange5.nsts.app.AppLifecycleListener
import com.orange5.nsts.sync.service.SyncService
import com.orange5.nsts.ui.base.BaseService

class SyncByWifiStateHandler private constructor(
        private val context: Context,
        private val callback: AppLifecycleListener) {

    init {
        registerNetworkCallback()
    }

    private fun registerNetworkCallback() {
        val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
        manager?.let {
            val request = getNetworkRequest()
            val callback = object : ConnectivityManager.NetworkCallback() {
                override fun onAvailable(network: Network?) {
                    handleSyncEvent()
                }
            }
            manager.registerNetworkCallback(request, callback)
        }
    }

    private fun handleSyncEvent() {
        val activity = callback.activity ?: return
        val startState = BaseService.ServiceRunner.getServiceStartState(activity) ?: return
        if (startState == BaseService.ServiceRunner.StartState.SUCCESS) {
            SyncService.start(activity)
        }
    }

    private fun getNetworkRequest() = NetworkRequest.Builder()
            .addCapability(NET_CAPABILITY_INTERNET)
            .addTransportType(TRANSPORT_WIFI)
            .build()

    companion object {
        fun registerSyncByWifiHandler(context: Context,
                                      callback: AppLifecycleListener) = SyncByWifiStateHandler(context, callback)
    }
}
