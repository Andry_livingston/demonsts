package com.orange5.nsts.sync.local;

import android.database.sqlite.SQLiteStatement;

import com.orange5.nsts.app.App;
import com.orange5.nsts.data.db.base.DatabaseEntity;
import com.orange5.nsts.sync.QueryType;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.internal.SqlUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import timber.log.Timber;

import static com.orange5.nsts.sync.local.LoggedSqlQuery.TableData;

public final class LogRequests {

    public static LoggedSqlQuery logUpdateQuery(AbstractDao<?, ?> dao, DatabaseEntity entity) {
        String sql = SqlUtils.createSqlUpdate(dao.getTablename(), dao.getAllColumns(), dao.getPkColumns());
        LoggedSqlQuery query = getLogViaReflection(QueryType.UPDATE, dao, entity, sql);
        List<Object> args = new ArrayList<>(Arrays.asList(query.getArgs()));
        Object key = entity.getPrimaryKey();
        if (key instanceof Long) {
            args.add(key);
        } else if (key == null) {
            throw new DaoException("Cannot update entity, key is null");
        } else {
            args.add(key.toString());
        }
        query.setArgs(args.toArray());
        return query;
    }

    public static LoggedSqlQuery logInsertQuery(AbstractDao<?, ?> dao, DatabaseEntity entity) {
        String sql = SqlUtils.createSqlInsert("INSERT INTO ", dao.getTablename(), dao.getAllColumns());
        return getLogViaReflection(QueryType.INSERT, dao, entity, sql);
    }

    public static LoggedSqlQuery logDeleteQuery(AbstractDao<?, ?> dao, DatabaseEntity entity) {
        String sql = SqlUtils.createSqlDelete(dao.getTablename(), dao.getPkColumns());
        SQLiteStatement stmt = App.getInstance().getRawDb().compileStatement(sql);
        Object key = entity.getPrimaryKey();
        if (key instanceof Long) {
            stmt.bindLong(1, (Long) key);
        } else if (key == null) {
            throw new DaoException("Cannot ic_delete entity, key is null");
        } else {
            stmt.bindString(1, key.toString());
        }
        return new LoggedSqlQuery(QueryType.DELETE, sql, mapTableData(dao), key);
    }

    private static LoggedSqlQuery getLogViaReflection(QueryType queryType,
                                                      AbstractDao<?, ?> dao,
                                                      DatabaseEntity entity,
                                                      String sql) {
        LoggedSqlQuery loggedRequest = null;
        SQLiteStatement newUpdateStatement = App.getInstance().getRawDb().compileStatement(sql);
        try {
            Method bindValues = dao.getClass().getDeclaredMethod("bindValues", SQLiteStatement.class, entity.getClass());
            bindValues.setAccessible(true);
            try {
                Field field = SQLiteStatement.class.getSuperclass().getDeclaredField("mBindArgs");
                field.setAccessible(true);
                bindValues.invoke(dao, newUpdateStatement, entity);
                Object[] args = (Object[]) field.get(newUpdateStatement);
                loggedRequest = new LoggedSqlQuery(queryType, sql, mapTableData(dao), args);
            } catch (IllegalAccessException | InvocationTargetException | NoSuchFieldException e) {
                Timber.d(e);
            }
        } catch (NoSuchMethodException e) {
            Timber.d(e);
        }
        return loggedRequest;
    }

    private static TableData mapTableData(AbstractDao<?, ?> dao) {
        return new TableData(dao.getTablename(),
                dao.getAllColumns(),
                dao.getPkColumns());
    }


}
