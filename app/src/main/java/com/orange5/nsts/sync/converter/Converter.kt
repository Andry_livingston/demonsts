package com.orange5.nsts.sync.converter

import android.util.Base64

abstract class Converter {

    protected fun String.base64ToString() =
            try {
                String(Base64.decode(this, Base64.DEFAULT))
            } catch (e: IllegalArgumentException) {
                ""
            }

    protected fun String.stringToBase64() =
            try {
                val bytes = this.toByteArray()
                Base64.encodeToString(bytes, Base64.DEFAULT)
            } catch (e: IllegalArgumentException) {
                ""
            }
}