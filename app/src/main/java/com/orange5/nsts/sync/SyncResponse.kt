package com.orange5.nsts.sync

import com.google.gson.annotations.SerializedName
import com.orange5.nsts.data.db.entities.SyncTransferObject


data class SyncResponse(@SerializedName("TransferObjectUpdatedIds")
                        val transferObjectUpdatedIds: Array<String>? = null,
                        @SerializedName("SyncTransferObjects")
                        var syncTransferObjects: List<SyncTransferObject> = emptyList(),
                        @SerializedName("IsFailedTransaction")
                        var isFailedTransaction: Boolean = false,
                        @SerializedName("Version")
                        val version: String = "",
                        @SerializedName("DevicePrefix")
                        val devicePrefix: String = "",
                        @SerializedName("LastSyncTime")
                        val lastSyncTime: String = "") {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SyncResponse

        if (transferObjectUpdatedIds != null) {
            if (other.transferObjectUpdatedIds == null) return false
            if (!transferObjectUpdatedIds.contentEquals(other.transferObjectUpdatedIds)) return false
        } else if (other.transferObjectUpdatedIds != null) return false
        if (syncTransferObjects != other.syncTransferObjects) return false
        if (isFailedTransaction != other.isFailedTransaction) return false
        if (version != other.version) return false
        if (devicePrefix != other.devicePrefix) return false
        if (lastSyncTime != other.lastSyncTime) return false

        return true
    }

    override fun hashCode(): Int {
        var result = transferObjectUpdatedIds?.contentHashCode() ?: 0
        result = 31 * result + syncTransferObjects.hashCode()
        result = 31 * result + isFailedTransaction.hashCode()
        result = 31 * result + version.hashCode()
        result = 31 * result + devicePrefix.hashCode()
        result = 31 * result + lastSyncTime.hashCode()
        return result
    }

}

data class DataObject(@SerializedName("SqlQuery")
                      var sqlQuery: String = "",
                      @SerializedName("Parameters")
                      val parameters: List<Parameter> = emptyList(),
                      @Transient
                      var queryType: String = "")

data class Parameter(@SerializedName("CompareInfo")
                     val compareInfo: String = "None",
                     @SerializedName("Direction")
                     val direction: String = "Input",
                     @SerializedName("IsNullable")
                     val isNullable: Boolean = true,
                     @SerializedName("LocaleId")
                     val localeId: Int = 0,
                     @SerializedName("Offset")
                     val offset: Int = 0,
                     @SerializedName("ParameterName")
                     val parameterName: String = "",
                     @SerializedName("Precision")
                     val precision: Int = 0,
                     @SerializedName("Scale")
                     val scale: Int = 0,
                     @SerializedName("Size")
                     val size: Int = 0,
                     @SerializedName("SourceColumn")
                     val sourceColumn: String = "",
                     @SerializedName("SourceColumnNullMapping")
                     val sourceColumnNullMapping: Boolean = false,
                     @SerializedName("SourceVersion")
                     val sourceVersion: String = "Current",
                     @SerializedName("SqlDbType")
                     val sqlDbType: String = "",
                     @SerializedName("TypeName")
                     val typeName: String = "",
                     @SerializedName("UdtTypeName")
                     val udtTypeName: String = "",
                     @SerializedName("Value")
                     val value: Any = "",
                     @SerializedName("ValueType")
                     val valueType: String = "",
                     @SerializedName("XmlSchemaCollectionDatabase")
                     val xmlSchemaCollectionDatabase: String = "",
                     @SerializedName("XmlSchemaCollectionName")
                     val xmlSchemaCollectionName: String = "",
                     @SerializedName("XmlSchemaCollectionOwningSchema")
                     val xmlSchemaCollectionOwningSchema: String = "")