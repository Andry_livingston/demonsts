package com.orange5.nsts.sync.local;

import com.orange5.nsts.sync.QueryType;

public class LoggedSqlQuery {

    private final QueryType queryType;
    private final String sql;
    private Object[] args;
    private final TableData tableData;

    public LoggedSqlQuery(QueryType queryType, String sql, TableData tableData, Object... args) {
        this.queryType = queryType;
        this.sql = sql;
        this.tableData = tableData;
        this.args = args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

    public QueryType getQueryType() {
        return queryType;
    }

    public String getSql() {
        return sql;
    }

    public Object[] getArgs() {
        return args;
    }

    public TableData getTableData() {
        return tableData;
    }

    public static class TableData {
        private final String tableName;
        private String[] columns;
        private final String[] pkColumns;

        public TableData(String tableName, String[] columns, String[] pkColumns) {
            this.tableName = tableName;
            this.columns = columns;
            this.pkColumns = pkColumns;
        }

        public void setColumns(String[] columns) {
            this.columns = columns;
        }

        public String getTableName() {
            return tableName;
        }

        public String[] getColumns() {
            return columns;
        }

        public String[] getPkColumns() {
            return pkColumns;
        }
    }

}
