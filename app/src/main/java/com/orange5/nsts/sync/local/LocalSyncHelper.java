package com.orange5.nsts.sync.local;

import com.orange5.nsts.data.db.base.DatabaseEntity;
import com.orange5.nsts.data.db.entities.SyncTransferObject;
import com.orange5.nsts.service.base.SyncObjectService;
import com.orange5.nsts.sync.QueryType;
import com.orange5.nsts.sync.converter.ToServerConverter;
import com.orange5.nsts.util.db.GUIDFactory;
import com.orange5.nsts.util.format.DateFormatter;

import org.greenrobot.greendao.AbstractDao;

import java.nio.charset.StandardCharsets;
import java.util.Date;

import com.orange5.nsts.util.StringUtils;

public class LocalSyncHelper {

    private final AbstractDao dao;
    private final ToServerConverter converter;
    private final SyncObjectService service;

    public LocalSyncHelper(AbstractDao dao, ToServerConverter converter, SyncObjectService service) {
        this.dao = dao;
        this.converter = converter;
        this.service = service;
    }

    public void insert(DatabaseEntity entity) {
        LoggedSqlQuery request = LogRequests.logInsertQuery(dao, entity);
        String dataObject = converter.convert(request);
        SyncTransferObject object = mapSyncObject(request.getQueryType(),
                dao.getTablename(),
                dataObject,
                null,
                entity.getPrimaryKey().toString());
        service.saveInDataBase(object);
    }

    public void update(DatabaseEntity entity) {
        LoggedSqlQuery request = LogRequests.logUpdateQuery(dao, entity);
        String dataObject = converter.convert(request);
        SyncTransferObject object = mapSyncObject(request.getQueryType(),
                dao.getTablename(),
                dataObject,
                StringUtils.arrayToString(request.getTableData().getColumns()),
                entity.getPrimaryKey().toString());
        service.saveInDataBase(object);
    }

    public void delete(DatabaseEntity entity) {
        LoggedSqlQuery request = LogRequests.logDeleteQuery(dao, entity);
        String dataObject = converter.convert(request);
        SyncTransferObject object = mapSyncObject(request.getQueryType(),
                dao.getTablename(),
                dataObject,
                null,
                entity.getPrimaryKey().toString());
        service.saveInDataBase(object);
    }

    private SyncTransferObject mapSyncObject(QueryType queryType,
                                             String tableName,
                                             String dataObject,
                                             String queryFields,
                                             String primaryKeyValue) {
        SyncTransferObject object = new SyncTransferObject();
        String date = DateFormatter.DB_DATE_T.format(new Date());
        object.setDateTimeSaved(date);
        object.setTypeData("OSS.DAL.SerializedSqlCommand");
        object.setAction(" ");
        object.setIsMine(true);
        object.setQueryType(queryType.getTitle());
        object.setQueryTable(tableName);
        object.setQueryKey(getQueryKey(dao.getPkProperty().name, primaryKeyValue));
        if (queryFields != null && queryType == QueryType.UPDATE) {
            object.setQueryFields(queryFields);
        } else {
            object.setQueryFields(",");
        }
        long lastIncrementNumber = service.getLastIncrementNumber();
        object.setIncrementNumber(lastIncrementNumber + 1);
        object.setData(dataObject.getBytes(StandardCharsets.UTF_8));
        object.setTransferObjectId(GUIDFactory.syncObjectUUID((lastIncrementNumber
                + date
                + System.currentTimeMillis()).getBytes()));
        return object;
    }

    private String getQueryKey(String pk, String pkValue) {
        if (StringUtils.isEmpty(pk) || StringUtils.isEmpty(pkValue)) {
            return "";
        }
        return StringUtils.capitalize(pk) + "=" + pkValue;
    }
}
