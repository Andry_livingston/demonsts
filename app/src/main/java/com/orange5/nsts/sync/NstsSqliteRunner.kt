package com.orange5.nsts.sync

import android.database.sqlite.SQLiteDatabase
import timber.log.Timber

class NstsSqliteRunner(private val rawDataBase: SQLiteDatabase) {

    fun runQuery(rawQuery: String) =
            try {
                rawDataBase.execSQL(rawQuery)
                true
            } catch (e: Throwable) {
                Timber.e(e)
                false
            }
}