package com.orange5.nsts.sync.converter

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.orange5.nsts.data.db.entities.SyncTransferObject
import com.orange5.nsts.sync.DataObject
import com.orange5.nsts.sync.Parameter
import com.orange5.nsts.sync.QueryType
import com.orange5.nsts.sync.SyncResponse
import timber.log.Timber
import java.nio.charset.Charset

class FromServerConverter(private val gson: Gson) : Converter() {

    /*  Запросы вида:
      INSERT [dbo].[Order]([OrderId], [CreatorCustomerId], [OrderDate], [OrderCompleteDate], [Details], [SignImage], [ReceiptText], [OrderStatusId], [WhoPickedOrderCustomerId], [LimitOverwriteComment], [OrderSum], [OrderNumber], [ApprovedByCustomerId], [CreatedByUserId], [WasChangedDate], [AccountingCode], [WebOrderNumber], [DeliveryDate])
      VALUES (              @0,         @1,                   @2, NULL, NULL, NULL, NULL, @3, NULL, @4, @5, @6, NULL, @7, NULL, NULL, NULL, @8)

      Параметры:
      ### @0=2a1787e3-be95-481b-bcab-70c0a5a0976e@1=396476@2=2/4/2019 4:40:44 PM
      @3=2@4=@5=40.14
      @6=PC0000001
      @7=0533cfe3-34f2-427e-97e1-e8dc02807068@8=2/4/2019 4:40:44 PM

      INSERT INTO "Order" ("OrderId","CreatorCustomerId","OrderDate","OrderCompleteDate","Details","SignImage","ReceiptText","OrderStatusId","WhoPickedOrderCustomerId","LimitOverwriteComment","OrderSum","OrderNumber","ApprovedByCustomerId","CreatedByUserId","WasChangedDate","AccountingCode","WebOrderNumber","DeliveryDate") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
      INSERT [dbo].[Order] ([OrderId] ,[CreatorCustomerId] ,[OrderDate] ,[OrderCompleteDate] ,[Details] ,[SignImage] ,[ReceiptText] ,[OrderStatusId] ,[WhoPickedOrderCustomerId] ,[LimitOverwriteComment] ,[OrderSum] ,[OrderNumber] ,[ApprovedByCustomerId] ,[CreatedByUserId] ,[WasChangedDate] ,[AccountingCode] ,[WebOrderNumber] ,[DeliveryDate])
      VALUES (@0,@1,@2,NULL,NULL,NULL,NULL,@7,NULL,NULL,@10,@11,NULL,@13,NULL,NULL,@16,NULL)
        @0 @1 @2 null null null null @7 null null @10 @11 null @13 null null @16 null

       UPDATE [dbo].[Order] SET ]OrderId]=? ,[CreatorCustomerId]=? ,[OrderDate]=? ,[OrderCompleteDate]=? ,[Details]=? ,[SignImage]=? ,[ReceiptText]=? ,[OrderStatusId]=? ,[WhoPickedOrderCustomerId]=? ,[LimitOverwriteComment]=? ,[OrderSum]=? ,[OrderNumber]=? ,[ApprovedByCustomerId]=? ,[CreatedByUserId]=? ,[WasChangedDate]=? ,[AccountingCode]=? ,[WebOrderNumber]=? ,[DeliveryDate]=? WHERE ]Order].]OrderId]=? VALUES (@0,@1,@2,NULL,NULL,NULL,NULL,@7,NULL,NULL,@10,@11,NULL,@13,NULL,NULL,@16,NULL,NULL)
UPDATE [dbo].[Order] SET [OrderId]=@02682fd3-a0a3-4bba-8a4d-0afacfb0324a,[CreatorCustomerId]=@394940,[OrderDate]=@2019-04-23 22:35:37.031,[OrderStatusId]=@1,[OrderSum]=@0.0,[OrderNumber]=@Android0000004,[CreatedByUserId]=@0533cfe3-34f2-427e-97e1-e8dc02807068,[WebOrderNumber]=@ WHERE [Order].[OrderId]=@02682fd3-a0a3-4bba-8a4d-0afacfb0324a
        UPDATE [dbo].[ConsumableBin]SET [Quantity] = @0 WHERE ([ConsumableBinId] = @1)
        UPDATE "Orders" SET "OrderId"=?,"CreatorCustomerId"=?,"OrderDate"=?,"OrderCompleteDate"=?,"Details"=?,"SignImage"=?,"ReceiptText"=?,"OrderStatusId"=?,"WhoPickedOrderCustomerId"=?,"LimitOverwriteComment"=?,"OrderSum"=?,"OrderNumber"=?,"ApprovedByCustomerId"=?,"CreatedByUserId"=?,"WasChangedDate"=?,"AccountingCode"=?,"WebOrderNumber"=? WHERE "Orders"."OrderId"=?

  */

    fun convert(response: SyncResponse): SyncResponse {
        response.syncTransferObjects
                .map {
                    val parsed = parseDataObjects(it)
                    if (parsed != null) {
                        it.dataObject = parsed
                    } else {
                        it.dataObject = DataObject()
                    }
                    return@map it
                }.map {
                    val query = parseRawSqlQuery(it.dataObject)
                    if (query.isNotEmpty()) {
                        it.dataObject.sqlQuery = query
                    }
                    return@map it
                }
        return response
    }

    private fun parseDataObjects(syncTransferObject: SyncTransferObject): DataObject? =
            syncTransferObject.let {
                val dataObject = try {
                    gson.fromJson((it.data.toString(Charset.defaultCharset())).base64ToString(), DataObject::class.java)
                } catch (e: JsonSyntaxException) {
                    Timber.e(e)
                    return null
                }
                dataObject.queryType = it.queryType
                dataObject
            }

    private fun parseRawSqlQuery(dataObject: DataObject): String {
        var query = dataObject.sqlQuery
        dataObject.parameters.forEach {
            query = query.replaceFirst(it.parameterName, handleParameter(it))
        }
        return convertToRawQuery(dataObject.queryType, query)
    }

    private fun handleParameter(parameter: Parameter): String =
            when (parameter.value.toString()) {
                "true" -> "1"
                "false" -> "0"
                else -> parameter.value.toString()
            }

    private fun convertToRawQuery(queryType: String, rawQuery: String): String {
        val type = TYPES_FOR_SERVER[queryType] ?: return ""
        return rawQuery
                .replace(type.serverType, type.androidType)
                .replace(DBO_SYMBOL, "")
                .replace("[", "\"")
                .replace("]", "\"")
    }

    private companion object {
        const val DBO_SYMBOL = "[dbo]."
        val TYPES_FOR_SERVER = mapOf(
                "INSERT" to QueryType.INSERT,
                "UPDATE" to QueryType.UPDATE,
                "DELETE" to QueryType.DELETE)
    }
}