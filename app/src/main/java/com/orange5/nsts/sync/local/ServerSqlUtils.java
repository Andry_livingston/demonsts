/*
 * Copyright (C) 2011-2016 Markus Junginger, greenrobot (http://greenrobot.org)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.orange5.nsts.sync.local;

import com.orange5.nsts.sync.QueryType;

import org.greenrobot.greendao.Property;

import static com.orange5.nsts.sync.local.LoggedSqlQuery.TableData;

public class ServerSqlUtils {
    private final static char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    public static StringBuilder appendProperty(StringBuilder builder, String tablePrefix, Property property) {
        if (tablePrefix != null) {
            builder.append(tablePrefix).append('.');
        }
        builder.append('[').append(property.columnName).append(']');
        return builder;
    }

    private static StringBuilder appendColumn(StringBuilder builder, String column) {
        builder.append('[').append(column).append(']');
        return builder;
    }

    private static StringBuilder appendColumnWithBrackets(StringBuilder builder, String column) {
        builder.append("([").append(column).append("]");
        return builder;
    }

    private static StringBuilder appendColumn(StringBuilder builder, String tableAlias, String column) {
        builder.append(tableAlias).append(".[").append(column).append(']');
        return builder;
    }

    public static StringBuilder appendColumns(StringBuilder builder, String tableAlias, String[] columns) {
        int length = columns.length;
        for (int i = 0; i < length; i++) {
            appendColumn(builder, tableAlias, columns[i]);
            if (i < length - 1) {
                builder.append(',');
            }
        }
        return builder;
    }

    private static StringBuilder appendColumns(StringBuilder builder, String[] columns) {
        int length = columns.length;
        for (int i = 0; i < length; i++) {
            builder.append('[').append(columns[i]).append(']');
            if (i < length - 1) {
                builder.append(',');
            }
        }
        return builder;
    }

    private static StringBuilder appendPlaceholders(StringBuilder builder, int count) {
        for (int i = 0; i < count; i++) {
            if (i < count - 1) {
                builder.append("?,");
            } else {
                builder.append('?');
            }
        }
        return builder;
    }

    private static StringBuilder appendColumnsEqualPlaceholders(StringBuilder builder, String[] columns) {
        for (int i = 0; i < columns.length; i++) {
            appendColumn(builder, columns[i]).append("=?");
            if (i < columns.length - 1) {
                builder.append(',');
            }
        }
        return builder;
    }

    private static StringBuilder appendColumnsEqValue(StringBuilder builder, String tableAlias, String[] columns) {
        for (int i = 0; i < columns.length; i++) {
            appendColumn(builder, tableAlias, columns[i]).append("=?");
            if (i < columns.length - 1) {
                builder.append(',');
            }
        }
        return builder;
    }

    private static StringBuilder appendColumnsEqValue(StringBuilder builder, String[] columns) {
        for (int i = 0; i < columns.length; i++) {
            appendColumnWithBrackets(builder, columns[i]).append("=?)");
            if (i < columns.length - 1) {
                builder.append(',');
            }
        }
        return builder;
    }

    public static String createSqlInsert(TableData tableData) {
        StringBuilder builder = new StringBuilder(QueryType.INSERT.getServerType());
        builder.append('[').append(tableData.getTableName()).append(']').append(" (");
        appendColumns(builder, tableData.getColumns());
        builder.append(") VALUES (");
        appendPlaceholders(builder, tableData.getColumns().length);
        builder.append(')');
        return builder.toString();
    }

    /**
     * Remember: SQLite does not support joins nor table alias for DELETE.
     */
    public static String createSqlDelete(TableData tableData) {
        String quotedTableName = '[' + tableData.getTableName() + ']';
        StringBuilder builder = new StringBuilder(QueryType.DELETE.getServerType());
        builder.append(quotedTableName);
        if (tableData.getPkColumns() != null && tableData.getPkColumns().length > 0) {
            builder.append(" WHERE ");
            appendColumnsEqValue(builder, tableData.getPkColumns());
        }
        return builder.toString();
    }

    public static String createSqlUpdate(TableData tableData) {
        String quotedTableName = '[' + tableData.getTableName() + ']';
        StringBuilder builder = new StringBuilder(QueryType.UPDATE.getServerType());
        builder.append(quotedTableName).append(" SET ");
        appendColumnsEqualPlaceholders(builder, tableData.getColumns());
        builder.append(" WHERE ");
        appendColumnsEqValue(builder, quotedTableName, tableData.getPkColumns());
        return builder.toString();
    }

    public static String escapeBlobArgument(byte[] bytes) {
        return "X'" + toHex(bytes) + '\'';
    }

    private static String toHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int i = 0; i < bytes.length; i++) {
            int byteValue = bytes[i] & 0xFF;
            hexChars[i * 2] = HEX_ARRAY[byteValue >>> 4];
            hexChars[i * 2 + 1] = HEX_ARRAY[byteValue & 0x0F];
        }
        return new String(hexChars);
    }
}
