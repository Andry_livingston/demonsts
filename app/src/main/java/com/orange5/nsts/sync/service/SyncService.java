package com.orange5.nsts.sync.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.orange5.nsts.R;
import com.orange5.nsts.api.sync.SyncClient;
import com.orange5.nsts.app.modules.ApiModule;
import com.orange5.nsts.receivers.SyncResultReceiver;
import com.orange5.nsts.sync.SyncManager;
import com.orange5.nsts.sync.SyncResponse;
import com.orange5.nsts.ui.base.BaseService;
import com.orange5.nsts.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.schedulers.Schedulers;

public class SyncService extends BaseService {

    @Inject
    SyncManager manager;
    @Inject
    ApiModule apiModule;

    private static final String NOTIFICATION_CHANNEL_ID = "NSTS_9991_ID";
    private static final String NOTIFICATION_CHANNEL_NAME = "NSTS_CHANNEL";
    public static final int NOTIFICATION_ID = 9991;
    private SyncClient client;
    volatile boolean areServerQueriesEmpty = true;

    public static void start(Context context) {
        Intent intent = new Intent(context, SyncService.class);
        if (Build.VERSION.SDK_INT >= 26) {
            context.startForegroundService(intent);
        } else {
            context.startService(intent);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        client = apiModule.syncClient();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Notification notification = createNotification();
        if (notification != null) {
            startForeground(NOTIFICATION_ID, notification);
            syncDataWithServer();
        } else {
            stopSelf();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void syncDataWithServer() {
        SyncResponse response = manager.processSyncToServer(null);
        sendSyncRequest(response);
    }

    private void sendSyncRequest(SyncResponse response) {
        addDisposable(rxSingleCreate(() -> client.sendData(response))
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleSyncResponse,
                        this::onError));
    }

    private void confirmExecutedQueries(String[] uuids) {
        addDisposable(rxSingleCreate(() -> client.confirmDataS(uuids))
                .subscribeOn(Schedulers.io())
                .subscribe(s -> syncDataWithServer(), this::onError));
    }

    private void handleSyncResponse(SyncResponse syncResponse) {
        String[] ids = syncResponse.getTransferObjectUpdatedIds();
        if (CollectionUtils.isEmpty(syncResponse.getSyncTransferObjects()) && (ids == null || ids.length <= 0)) {
            onFinishSync();
            return;
        }

        SyncResponse converted = manager.processSyncFromServer(syncResponse);

        if (converted == null) {
            notifyUI("Fail to sync data", areServerQueriesEmpty);
        } else {
            List<String> executed = new ArrayList<>();
            syncResponse.getSyncTransferObjects().forEach(it -> {
                if (it.getExecuted() != null) {
                    executed.add(it.getTransferObjectId());
                }
            });
            if (CollectionUtils.isNotEmpty(executed)) {
                areServerQueriesEmpty = false;
                confirmExecutedQueries(executed.toArray(new String[0]));
            } else {
                syncDataWithServer();
            }
        }
    }

    private Notification createNotification() {
        Notification notification = null;
        Notification.Builder builder = getNotificationBuilder();
        if (builder != null) {
            notification = builder.setContentTitle(getText(R.string.sync_title))
                    .setContentText(getText(R.string.notification_message))
                    .setSmallIcon(R.drawable.ic_sync_white_24dp)
                    .build();
        }
        return notification;
    }

    private Notification.Builder getNotificationBuilder() {
        Notification.Builder builder = null;

        if (Build.VERSION.SDK_INT >= 26) {

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            NotificationChannel notificationChannel = new NotificationChannel(
                    NOTIFICATION_CHANNEL_ID,
                    NOTIFICATION_CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_LOW);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(notificationChannel);
                builder = new Notification.Builder(this, NOTIFICATION_CHANNEL_ID);
            }
        } else {
            builder = new Notification
                    .Builder(this)
                    .setPriority(Notification.PRIORITY_LOW);
        }

        return builder;
    }

    private void onError(Throwable throwable) {
        notifyUI(() -> broadcastService.sendLocalBroadcast(
                SyncResultReceiver.newSyncResultIntent(throwable.getMessage(), true)));
    }

    private void notifyUI(String errorMessage, boolean areServerQueriesEmpty) {
        notifyUI(() -> broadcastService.sendLocalBroadcast(SyncResultReceiver.newSyncResultIntent(errorMessage,
                areServerQueriesEmpty)));
    }

    private void notifyUI(Runnable runnable) {
        runnable.run();
        stopSelf();
    }

    private void onFinishSync() {
        stopForeground(STOP_FOREGROUND_REMOVE);
        notifyUI(null, areServerQueriesEmpty);
    }
}
