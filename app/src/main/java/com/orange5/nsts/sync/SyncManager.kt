package com.orange5.nsts.sync

import com.orange5.nsts.BuildConfig
import com.orange5.nsts.data.db.entities.SyncTransferObject
import com.orange5.nsts.service.base.SyncObjectService
import com.orange5.nsts.service.parameters.ParametersService.Companion.ANDROID_PREFIX
import com.orange5.nsts.sync.converter.FromServerConverter
import com.orange5.nsts.util.format.DateFormatter
import timber.log.Timber
import java.util.*

class SyncManager private constructor(private val converter: FromServerConverter,
                                      private val sqliteRunner: NstsSqliteRunner,
                                      private val syncService: SyncObjectService) {

    fun processSyncFromServer(initial: SyncResponse): SyncResponse? {
        var converted: SyncResponse? = null
        if (initial.isFailedTransaction.not()) {
            syncService.markSyncObjectAsExecuted(initial.transferObjectUpdatedIds)
            converted = converter.convert(initial)
            runQueries(converted)
            Timber.e("${converted.isFailedTransaction}")
            //todo last sync date
        }
        return converted
    }

    fun processSyncToServer(serverResponse: SyncResponse? = null): SyncResponse {
        val syncObjects = getSyncObjectsFromDb()
        return mapSyncResponse(syncObjects, serverResponse)
    }

    private fun mapSyncResponse(syncObjects: List<SyncTransferObject>, serverResponse: SyncResponse?) =
            SyncResponse(
                    transferObjectUpdatedIds = null,
                    isFailedTransaction = serverResponse?.isFailedTransaction ?: false,
                    syncTransferObjects = syncObjects,
                    version = serverResponse?.version ?: BuildConfig.VERSION_CODE.toString(),
                    devicePrefix = ANDROID_PREFIX,
                    lastSyncTime = DateFormatter.DB_DATE_T.format(Date())
            )

    private fun getSyncObjectsFromDb(): List<SyncTransferObject> {
        Timber.e(this.toString())
        syncService.objects.forEach { Timber.d(it.transferObjectId) }
        return syncService.objects
    }

    private fun runQueries(converted: SyncResponse) {
        for (transferObject in converted.syncTransferObjects) {
            if (!sqliteRunner.runQuery(transferObject.dataObject.sqlQuery)) {
                converted.isFailedTransaction = true
            } else {
                transferObject.executed = DateFormatter.currentTimeDb()
            }
        }
    }

    companion object {
        @JvmStatic
        fun instance(converter: FromServerConverter, sqliteRunner: NstsSqliteRunner, syncObjectService: SyncObjectService) =
                SyncManager(converter, sqliteRunner, syncObjectService)
    }
}
