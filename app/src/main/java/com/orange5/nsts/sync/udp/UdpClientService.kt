package com.orange5.nsts.sync.udp

import com.orange5.nsts.receivers.BroadcastService
import com.orange5.nsts.receivers.UdpResultReceiver.Companion.newUdpResultIntent
import io.reactivex.Completable
import io.reactivex.CompletableEmitter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.nio.charset.Charset

class UdpClientService(private val broadcastService: BroadcastService) {

    private val TAG = UdpClientService::class.java.simpleName

    private var shouldRestartSocket = true
    private var socket: DatagramSocket? = null
    private val disposable = CompositeDisposable()


    fun onStart() {
        shouldRestartSocket = true
        disposable.add(rxCompletableCreate { runDataSocket() }
                .subscribeOn(Schedulers.io())
                .subscribe({ logD("Started Listening") }, ::onError))
    }

    fun onStop() {
        shouldRestartSocket = false
        disposable.clear()
        socket?.close()
    }

    private fun runDataSocket() {
        val port = 6000
        while (shouldRestartSocket) {
            listenAndWaitAndThrowIntent(port)
        }
    }

    private fun listenAndWaitAndThrowIntent(port: Int) {
        val buffer = ByteArray(512)
        if (socket == null || socket!!.isClosed) {
            socket = DatagramSocket(port)
            socket?.broadcast = true
            socket?.reuseAddress = true
        }

        val packet = DatagramPacket(buffer, buffer.size)

        logD("Waiting for UDP broadcast")

        socket?.receive(packet)

        val senderIP = packet.address.hostAddress
        val message = packet.data.toString(Charset.defaultCharset()).trim()

        logD("Got UDB broadcast from $senderIP, message: $message")

        notifyUI(senderIP, message)
        socket?.close()
    }

    private fun onError(t: Throwable) {
        logE("no longer listening for UDP broadcasts cause of error ${t.message}")
      /*  broadcastService.sendLocalBroadcast(
                newUdpResultIntent(t.message, "", ""))*/
    }

    private fun notifyUI(senderIp: String, data: String) {
        broadcastService.sendLocalBroadcast(
                newUdpResultIntent(null, senderIp, data))
    }

    private fun rxCompletableCreate(action: () -> Unit): Completable {
        return Completable.create { emitter: CompletableEmitter ->
            try {
                action()
                if (!emitter.isDisposed) emitter.onComplete()
            } catch (t: Throwable) {
                if (!emitter.isDisposed) emitter.onError(t)
            }
        }
    }

    private fun logD(message: String) {
        Timber.tag(TAG).d(message)
    }

    private fun logE(message: String) {
        Timber.tag(TAG).e(message)
    }
}