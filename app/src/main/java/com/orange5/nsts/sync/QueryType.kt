package com.orange5.nsts.sync

enum class QueryType(val title: String,
                     val serverType: String,
                     val androidType: String) {

    UPDATE("UPDATE", "UPDATE [dbo].", "UPDATE"),
    INSERT("INSERT", "INSERT [dbo].", "INSERT OR REPLACE INTO"),
    DELETE("DELETE", "DELETE [dbo].", "DELETE FROM")
}