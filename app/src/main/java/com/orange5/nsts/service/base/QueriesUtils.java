package com.orange5.nsts.service.base;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;

public class QueriesUtils {

    private static final int MINIMUM_SEARCH_QUERY_LENGTH = 1;

    public static boolean validQuery(String query) {
        return !TextUtils.isEmpty(query) && query.length() >= MINIMUM_SEARCH_QUERY_LENGTH;
    }

    public static String formatSearchString(String search) {
        if (search.length() == 0 || search.indexOf('%') == 0 && search.lastIndexOf('%') == search.length() - 1) {
            return search;
        }
        return "%" + search + "%";
    }

    public static List<String> explodeToKeywords(String query) {
        List<String> result = new ArrayList<>();
        String[] strings = query.trim().split("\\s+");
        for (String keyword : strings) {
            result.add(formatSearchString(keyword));
        }
        return result;
    }

    public static String addWildcards(String query) {
        return "%" + query.trim().replaceAll("\\s+", "%") + "%";
    }

    public static List<String> getAllPermutations(String query) {
        if (!validQuery(query)) {
            return Collections.emptyList();
        }
        String[] split = query.trim().split("\\s+");
        List<String> result = new ArrayList<>();
        StringJoiner joiner = new StringJoiner("%", "%", "%");
        for (String s : split) {
            joiner.add(s);
        }
        result.add(joiner.toString());

        if (split.length == 1) {
            return result;
        }

        joiner = new StringJoiner("%", "%", "%");
        for (int i = split.length - 1; i >= 0; i--) {
            String s = split[i];
            joiner.add(s);
        }
        result.add(joiner.toString());


        return result;
    }
}
