package com.orange5.nsts.service.rental;

import com.orange5.nsts.data.db.dao.RentalProductItemDao;
import com.orange5.nsts.data.db.entities.RentalProductItem;
import com.orange5.nsts.service.base.SyncingService;
import com.orange5.nsts.sync.local.LocalSyncHelper;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import static com.orange5.nsts.di.modules.SyncModule.RENTAL_PRODUCT_ITEM_SERVICE;

public class RentalProductItemService extends SyncingService<RentalProductItemDao, RentalProductItem> {

    @Inject
    @Named(RENTAL_PRODUCT_ITEM_SERVICE)
    LocalSyncHelper helper;

    @Inject
    public RentalProductItemService(RentalProductItemDao dao) {
        super(dao);
    }

    @Override
    public void insert(RentalProductItem entity) {
        super.insert(entity);
    }

    @Override
    public void update(RentalProductItem entity) {
        super.update(entity);
        dao.refresh(entity);
    }

    @Override
    public void delete(RentalProductItem entity) {
        super.delete(entity);
    }

    @Override
    protected LocalSyncHelper getSyncHelper() {
        return helper;
    }

    public RentalProductItem loadItemByUUN(String number) {
        return  dao
                .queryBuilder()
                .where(RentalProductItemDao.Properties.UniqueUnitNumber.eq(number))
                .unique();
    }

    public List<RentalProductItem> loadCheckedOutItems() {
        return dao
                .queryBuilder()
                .where(RentalProductItemDao.Properties.Status.eq(RentalStatus.CheckedOut))
                .list();
    }

    public List<RentalProductItem> loadByBinId(String binId) {
        return dao.queryBuilder().where(RentalProductItemDao.Properties.BinId.eq(binId)).list();
    }

    public List<RentalProductItem> loadRevisionItems(String binId) {
        return dao.queryBuilder().where(RentalProductItemDao.Properties.BinId.eq(binId),
                RentalProductItemDao.Properties.Status.eq(RentalStatus.Inspection.getId())).list();
    }
}
