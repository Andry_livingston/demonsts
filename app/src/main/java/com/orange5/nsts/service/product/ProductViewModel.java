package com.orange5.nsts.service.product;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.SpannableString;

import com.orange5.nsts.R;
import com.orange5.nsts.data.db.base.OrderProduct;
import com.orange5.nsts.data.db.entities.ConsumableProduct;
import com.orange5.nsts.service.bin.ProductBinInformation;
import com.orange5.nsts.ui.base.BaseViewHolder;
import com.orange5.nsts.util.format.CurrencyFormatter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringJoiner;

import com.orange5.nsts.util.Spannable;

public class ProductViewModel extends BaseViewHolder {
    private final int redColor;
    private final int blackColor;
    private final int grayColor;
    private final int greenColor;
    private Integer selectedQuantity;
    private ProductBinInformation binInformation;

    private OrderProduct currentProduct;
    private Context context;

    public ProductViewModel(Context context, OrderProduct product) {
        this.context = context;
        this.currentProduct = product;
        selectedQuantity = 0;
        binInformation = currentProduct.getBinInformation();
        redColor = context.getColor(R.color.errorRed);
        blackColor = context.getColor(R.color.black);
        grayColor = context.getColor(R.color.darkGray);
        greenColor = context.getColor(R.color.okGreen);
    }

    public String getFormattedPrice() {
        return CurrencyFormatter.Companion.formatPrice(currentProduct.getPrice());
    }

    public String getFormattedQuantity() {
        return "Qty: -1";
    }

    public SpannableString getProductDescription(String highlight) {
        return highlightBackground(currentProduct.getProductDescription(), highlight);
    }

    public SpannableString getProductNumber(String highlight) {
        return highlightBackground(currentProduct.getProductNumber(), highlight);
    }

    @SuppressLint("DefaultLocale")
    public SpannableString getAvailability() {
        int totalQuantity = 0;
        List<String> strings = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : binInformation.getCachedProductBinCount().entrySet()) {
            Integer binQuantity = entry.getValue();
            totalQuantity += binQuantity;
            String binQuantityText = String.format(Locale.getDefault(),
                    "%s(%d)", entry.getKey(), binQuantity);
            strings.add(binQuantityText);
        }

        String prefix;

        if (selectedQuantity == 0) {
            prefix = String.format("Available: %d ", totalQuantity);
        } else {
            prefix = String.format("Selected: %d / Available: %d ", selectedQuantity, totalQuantity);
        }

        StringJoiner stringJoiner = new StringJoiner(", ",
                prefix,
                "");
        strings.forEach(stringJoiner::add);
        String text = stringJoiner.toString();

        if (selectedQuantity == 0) {
            if (totalQuantity == 0) {
                return Spannable.highlightTextColor(text, grayColor, text);
            }
            return Spannable.highlightTextColor(text, blackColor, text);
        }
        return Spannable.highlightTextColor(text, greenColor, text);
    }
}
