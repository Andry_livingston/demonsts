package com.orange5.nsts.service.base

import androidx.annotation.CallSuper
import com.orange5.nsts.data.db.base.DatabaseEntity
import org.greenrobot.greendao.AbstractDao

abstract class DataBaseService<Dao : AbstractDao<Entity, *>, Entity : DatabaseEntity>(@JvmField protected val dao: Dao) {

    @CallSuper
    open fun initInsert(entities: List<Entity>) =
            if (entities.isNotEmpty()) {
                dao.deleteAll()
                dao.insertInTx(entities)
                entitySize()
            } else 0

    private fun entitySize() = dao.loadAll().size
}