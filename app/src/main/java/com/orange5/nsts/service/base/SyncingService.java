package com.orange5.nsts.service.base;

import androidx.annotation.CallSuper;

import com.orange5.nsts.data.db.base.DatabaseEntity;
import com.orange5.nsts.sync.local.LocalSyncHelper;

import org.greenrobot.greendao.AbstractDao;

public abstract class SyncingService<Dao extends AbstractDao<Entity, ?>, Entity extends DatabaseEntity> extends DataBaseService<Dao, Entity> {

    public SyncingService(Dao dao) {
        super(dao);
    }

    protected abstract LocalSyncHelper getSyncHelper();

    @CallSuper
    protected void insert(Entity entity) {
        if (entity == null) {
            throw new IllegalArgumentException("entity can't be null");
        }
        dao.insert(entity);
        getSyncHelper().insert(entity);
    }

    @CallSuper
    protected void update(Entity entity) {
        if (entity == null) {
            throw new IllegalArgumentException("entity can't be null");
        }
        dao.update(entity);
        getSyncHelper().update(entity);
    }

    @CallSuper
    protected void delete(Entity entity) {
        if (entity == null) {
            throw new IllegalArgumentException("entity can't be null");
        }
        dao.delete(entity);
        getSyncHelper().delete(entity);
    }
}