package com.orange5.nsts.service.rentalReturn;

import com.orange5.nsts.data.db.dao.RentalReturnDao;
import com.orange5.nsts.data.db.entities.RentalReturn;
import com.orange5.nsts.service.base.SyncingService;
import com.orange5.nsts.sync.local.LocalSyncHelper;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import static com.orange5.nsts.di.modules.SyncModule.RENTAL_RETURN_SERVICE;


public class RentalReturnService extends SyncingService<RentalReturnDao, RentalReturn> {

    @Inject
    @Named(RENTAL_RETURN_SERVICE)
    LocalSyncHelper helper;

    @Inject
    public RentalReturnService(RentalReturnDao dao) {
        super(dao);
    }

    @Override
    protected LocalSyncHelper getSyncHelper() {
        return helper;
    }

    public List<RentalReturn> getRentals(String query) {
        return dao
                .queryBuilder()
                .orderDesc(RentalReturnDao.Properties.StartDate)
                .list();
    }

    public RentalReturn getRentalById(String id) {
       return dao.load(id);
    }

    public void insert(RentalReturn rentalReturn) {
        super.insert(rentalReturn);
    }

    public void delete(RentalReturn rentalReturn) {
        super.delete(rentalReturn);
    }

    public void update(RentalReturn rentalReturn) {
        super.update(rentalReturn);
    }

}