package com.orange5.nsts.service.user

import com.orange5.nsts.data.db.dao.User2RoleDao
import com.orange5.nsts.data.db.entities.User2Role
import javax.inject.Inject

class UserRoleService @Inject constructor(private val user2RoleDao: User2RoleDao) {

    fun getUserRoleIdByUserId(userId: String): String {
        val role: User2Role? = user2RoleDao.queryBuilder().where(User2RoleDao.Properties.UserId.eq(userId)).unique()
        return role?.roleId ?: ""
    }
}