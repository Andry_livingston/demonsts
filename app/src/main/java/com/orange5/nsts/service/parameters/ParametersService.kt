package com.orange5.nsts.service.parameters

import com.orange5.nsts.data.db.dao.ParametersDao
import com.orange5.nsts.data.db.entities.Parameters
import com.orange5.nsts.service.base.DataBaseService
import javax.inject.Inject

class ParametersService @Inject constructor(dao: ParametersDao) : DataBaseService<ParametersDao, Parameters>(dao) {

    val orderPrefix: String
        get() = dao
                .queryBuilder()
                .where(ParametersDao.Properties.ParameterName.eq(DEVICE_PREFIX))
                .uniqueOrThrow().parameterValue

    override fun initInsert(entities: List<Parameters>): Int {
        val mapped = entities.map {
            if (it.parameterName == DEVICE_PREFIX) {
                it.parameterValue = ANDROID_PREFIX
                it
            } else it
        }
        return super.initInsert(mapped)
    }

    companion object {
        private const val DEVICE_PREFIX = "DevicePrefix"
        const val ANDROID_PREFIX = "Z"
    }
}