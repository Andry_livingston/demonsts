package com.orange5.nsts.service.base

class InitLoadException(cause: String) : RuntimeException(cause)