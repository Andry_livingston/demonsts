package com.orange5.nsts.service.discrepancy

import com.orange5.nsts.data.db.dao.DiscrepanciesTypeDao
import com.orange5.nsts.data.db.entities.DiscrepanciesType
import com.orange5.nsts.service.base.DataBaseService
import javax.inject.Inject

class DiscrepanciesTypeService @Inject constructor(discrepanciesTypeDao: DiscrepanciesTypeDao) :
        DataBaseService<DiscrepanciesTypeDao, DiscrepanciesType>(discrepanciesTypeDao) {

    fun loadAll(): List<DiscrepanciesType> = dao.loadAll()
}