package com.orange5.nsts.service.accountingcode

import com.orange5.nsts.data.db.dao.AccountingCodeDao
import com.orange5.nsts.data.db.entities.AccountingCode
import com.orange5.nsts.service.base.DataBaseService
import javax.inject.Inject

class AccountingCodeService @Inject constructor(dao: AccountingCodeDao) : DataBaseService<AccountingCodeDao, AccountingCode>(dao) {

    fun loadAll(): MutableList<AccountingCode> = dao.queryBuilder().orderAsc(AccountingCodeDao.Properties.AccountingCodeName).list()
}