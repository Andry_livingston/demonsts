package com.orange5.nsts.service.shipping;

import com.orange5.nsts.data.db.dao.ProductReceivingLogDao;
import com.orange5.nsts.data.db.entities.ProductReceivingLog;
import com.orange5.nsts.service.base.SyncingService;
import com.orange5.nsts.sync.local.LocalSyncHelper;

import javax.inject.Inject;
import javax.inject.Named;

import static com.orange5.nsts.di.modules.SyncModule.RECEIVING_LOG_SERVICE;


public class ReceivingLogService extends SyncingService<ProductReceivingLogDao, ProductReceivingLog> {

    @Inject
    @Named(RECEIVING_LOG_SERVICE)
    LocalSyncHelper helper;

    @Inject
    public ReceivingLogService(ProductReceivingLogDao dao) {
        super(dao);
    }

    @Override
    protected LocalSyncHelper getSyncHelper() {
        return helper;
    }

    public void insert(ProductReceivingLog receivingLog) {
        super.insert(receivingLog);
    }

    public void delete(ProductReceivingLog receivingLog) {
        super.delete(receivingLog);
    }

    public void update(ProductReceivingLog receivingLog) {
        super.update(receivingLog);
    }

}