package com.orange5.nsts.service.order;


import com.orange5.nsts.R;
import com.orange5.nsts.data.db.entities.Order;

public enum OrderStatus {
    remoteOrderRequest(0, R.string.order_status_remote_order_request, R.color.color_open_order_status),
    open(1, R.string.order_status_order_open, R.color.color_open_order_status), //save for later
    pickUp(2, R.string.order_status_order_pick_up, R.color.color_pick_ticket_status), //pick ticket created
    readyNotShipped(3, R.string.order_status_order_ready_not_shipped, R.color.color_open_order_status),
    suspended(4, R.string.order_status_order_suspended, R.color.orderSuspended),
    notShipped(5, R.string.order_status_order_not_shipped, R.color.color_open_order_status),
    confirmed(6, R.string.order_status_order_confirmed, R.color.color_open_order_status),
    closed(7, R.string.order_status_order_closed, R.color.orderClosed),
    active(8, R.string.order_status_order_closed, R.color.orderClosed);

    private final int id;
    private final int name;
    private final int highlightColor;

    OrderStatus(int id, int name, int highlightColor) {
        this.id = id;
        this.name = name;
        this.highlightColor = highlightColor;
    }

    public static OrderStatus byIndex(int index) {
        return values()[index];
    }

    public static OrderStatus of(Order currentOrder) {
        return byIndex(currentOrder.getOrderStatusId());
    }

    public int getId() {
        return id;
    }

    public int getName() {
        return name;
    }

    public int getHighlightColor() {
        return highlightColor;
    }

    public boolean matches(Order order) {
        return order.getOrderStatusId().equals(id);
    }
}