package com.orange5.nsts.service.order;

import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.data.db.entities.Order;
import com.orange5.nsts.data.db.entities.OrderConsumableItem;
import com.orange5.nsts.data.db.entities.OrderRentalItem;

public class OrderTotalsUtils {

    private final Order order;

    public OrderTotalsUtils(Order order) {
        this.order = order;
    }

    public int getTotalItemsSize() {
        return getConsumableSize() + getRentalSize();
    }

    public int getPickedSize() {
        return getConsumablePicked() + getRentalPicked();
    }

    public int getUnpickedSize() {
        int total = 0;
        if (getPickedSize() <= getTotalItemsSize()) {
            total = getTotalItemsSize() - getPickedSize();
        }
        return total;
    }

    public int getRentalSize() {
        int total = 0;
        order.resetRentalItems();
        for (OrderItem item : order.getAllItems()) {
            if (item instanceof OrderRentalItem) {
                total++;
            }
        }
        return total;
    }

    public int getConsumableSize() {
        int total = 0;
        order.resetRentalItems();
        for (OrderItem item : order.getAllItems()) {
            if (item instanceof OrderConsumableItem) {
                total += item.asConsumable().getQuantity();
            }
        }
        return total;
    }

    public int getRentalPicked() {
        int total = 0;
        order.resetRentalItems();
        for (OrderRentalItem item : order.getRentalItems()) {
            total += item.getUniqueUnitNumber() != null ? 1 : 0;
        }
        return total;
    }

    public int getUnpickedRental() {
        int total = 0;
        if (getRentalPicked() <= getRentalSize()) {
            total = getRentalSize() - getRentalPicked();
        }
        return total;
    }

    public int getConsumablePicked() {
        int total = 0;
        order.resetConsumableItems();
        for (OrderConsumableItem item : order.getConsumableItems()) {
            total += item.getPicked();
        }
        return total;
    }

    public int getUnpickedConsumable() {
        int total = 0;
        if (getConsumablePicked() <= getConsumableSize()) {
            total = getConsumableSize() - getConsumablePicked();
        }
        return total;
    }
}
