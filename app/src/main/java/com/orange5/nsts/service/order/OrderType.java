package com.orange5.nsts.service.order;

public enum OrderType {
    FULL_ORDER, QUICK_ORDER
}