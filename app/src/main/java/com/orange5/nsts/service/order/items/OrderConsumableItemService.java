package com.orange5.nsts.service.order.items;

import com.orange5.nsts.data.db.dao.OrderConsumableItemDao;
import com.orange5.nsts.data.db.entities.OrderConsumableItem;
import com.orange5.nsts.service.base.SyncingService;
import com.orange5.nsts.sync.local.LocalSyncHelper;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import static com.orange5.nsts.di.modules.SyncModule.ORDER_CONSUMABLE_ITEM_SERVICE;

public class OrderConsumableItemService extends SyncingService<OrderConsumableItemDao, OrderConsumableItem> {

    @Inject
    @Named(ORDER_CONSUMABLE_ITEM_SERVICE)
    LocalSyncHelper helper;

    @Inject
    public OrderConsumableItemService(OrderConsumableItemDao dao) {
        super(dao);
    }

    @Override
    protected LocalSyncHelper getSyncHelper() {
        return helper;
    }

    public void saveAll(List<OrderConsumableItem> items) {
        items.forEach(this::save);
    }

    public void save(OrderConsumableItem item) {
        if (ifExists(item)) {
            update(item);
        } else {
            insert(item);
        }
    }

    public void insertAll(List<OrderConsumableItem> items) {
        items.forEach(this::insert);
    }

    public void updateAll(List<OrderConsumableItem> items) {
        items.forEach(this::update);
    }

    public void insert(OrderConsumableItem item) {
        super.insert(item);
    }

    public void update(OrderConsumableItem item) {
        super.update(item);
    }

    public void delete(OrderConsumableItem item) {
        super.delete(item);
    }

    public boolean ifExists(OrderConsumableItem item) {
        return dao
                .queryBuilder()
                .where(OrderConsumableItemDao.Properties.OrderItemId.eq(item.getOrderItemId()))
                .unique() != null;
    }

}
