package com.orange5.nsts.service.product.async;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DatabaseAsyncExecutor {

    private static final ExecutorService service = Executors.newSingleThreadExecutor();

    public static void execute(Runnable runnable) {
        service.execute(runnable);
    }
}
