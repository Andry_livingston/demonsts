package com.orange5.nsts.service.base

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.orange5.nsts.api.load.InitLoadWrapper
import com.orange5.nsts.data.db.entities.AccessoryCrossReference
import com.orange5.nsts.data.db.entities.AccountingCode
import com.orange5.nsts.data.db.entities.AppState
import com.orange5.nsts.data.db.entities.BinNumber
import com.orange5.nsts.data.db.entities.BinType
import com.orange5.nsts.data.db.entities.Category
import com.orange5.nsts.data.db.entities.ConsumableAlternateProductNumberXref
import com.orange5.nsts.data.db.entities.ConsumableBin
import com.orange5.nsts.data.db.entities.ConsumableProduct
import com.orange5.nsts.data.db.entities.ConsumableProductCustomFields
import com.orange5.nsts.data.db.entities.Customer
import com.orange5.nsts.data.db.entities.CustomerCustomFields
import com.orange5.nsts.data.db.entities.CustomerCustomFieldsTypes
import com.orange5.nsts.data.db.entities.CustomerPosition
import com.orange5.nsts.data.db.entities.CustomerProductNumberXref
import com.orange5.nsts.data.db.entities.CustomerRole
import com.orange5.nsts.data.db.entities.CustomerStatus
import com.orange5.nsts.data.db.entities.Discrepancies
import com.orange5.nsts.data.db.entities.DiscrepanciesResolvedStatus
import com.orange5.nsts.data.db.entities.DiscrepanciesType
import com.orange5.nsts.data.db.entities.DisplayColumnName
import com.orange5.nsts.data.db.entities.InventoryAdjustment
import com.orange5.nsts.data.db.entities.InventoryAdjustmentReasonCode
import com.orange5.nsts.data.db.entities.Order
import com.orange5.nsts.data.db.entities.OrderConsumableItem
import com.orange5.nsts.data.db.entities.OrderConsumableReturn
import com.orange5.nsts.data.db.entities.OrderConsumableReturnItemByCustomer
import com.orange5.nsts.data.db.entities.OrderRentalItem
import com.orange5.nsts.data.db.entities.OrderStatus
import com.orange5.nsts.data.db.entities.Parameters
import com.orange5.nsts.data.db.entities.PickTicket
import com.orange5.nsts.data.db.entities.RentalProduct
import com.orange5.nsts.data.db.entities.RentalProductCustomFields
import com.orange5.nsts.data.db.entities.RentalProductItem
import com.orange5.nsts.data.db.entities.RentalProductStatus
import com.orange5.nsts.data.db.entities.RentalProductStatusHistory
import com.orange5.nsts.data.db.entities.RentalReturn
import com.orange5.nsts.data.db.entities.Role
import com.orange5.nsts.data.db.entities.ShippingNotice
import com.orange5.nsts.data.db.entities.Tag
import com.orange5.nsts.data.db.entities.TransactionCustomFields
import com.orange5.nsts.data.db.entities.TransactionHistory
import com.orange5.nsts.data.db.entities.TransactionType
import com.orange5.nsts.data.db.entities.Transfer
import com.orange5.nsts.data.db.entities.TransferCarrier
import com.orange5.nsts.data.db.entities.TransferItem
import com.orange5.nsts.data.db.entities.TransferStatus
import com.orange5.nsts.data.db.entities.User
import com.orange5.nsts.service.AccessoryCrossReferenceService
import com.orange5.nsts.service.AppStateService
import com.orange5.nsts.service.BinTypeService
import com.orange5.nsts.service.CategoryService
import com.orange5.nsts.service.ConsumableAlternateProductNumberXrefService
import com.orange5.nsts.service.ConsumableProduct2TagService
import com.orange5.nsts.service.ConsumableProductBinHistoryService
import com.orange5.nsts.service.ConsumableProductCustomFieldsService
import com.orange5.nsts.service.Customer2RoleService
import com.orange5.nsts.service.CustomerCustomFieldsService
import com.orange5.nsts.service.CustomerCustomFieldsTypesService
import com.orange5.nsts.service.CustomerPositionService
import com.orange5.nsts.service.CustomerProductNumberXrefService
import com.orange5.nsts.service.CustomerRoleService
import com.orange5.nsts.service.CustomerStatusService
import com.orange5.nsts.service.DiscrepanciesResolvedStatusService
import com.orange5.nsts.service.DisplayColumnNameService
import com.orange5.nsts.service.ErrorService
import com.orange5.nsts.service.InventoryAdjustmentReasonCodeService
import com.orange5.nsts.service.InventoryAdjustmentService
import com.orange5.nsts.service.OrderConsumableReturnItemByCustomerService
import com.orange5.nsts.service.OrderConsumableReturnService
import com.orange5.nsts.service.OrderStatusService
import com.orange5.nsts.service.PickTicketService
import com.orange5.nsts.service.RentalProduct2TagService
import com.orange5.nsts.service.RentalProductBinHistoryService
import com.orange5.nsts.service.RentalProductCustomFieldsService
import com.orange5.nsts.service.RentalProductStatusHistoryService
import com.orange5.nsts.service.RentalProductStatusService
import com.orange5.nsts.service.Tag2CustomerRoleService
import com.orange5.nsts.service.TagService
import com.orange5.nsts.service.TransactionCustomFieldsService
import com.orange5.nsts.service.TransactionHistoryService
import com.orange5.nsts.service.TransactionTypeService
import com.orange5.nsts.service.TransferCarrierService
import com.orange5.nsts.service.TransferItemService
import com.orange5.nsts.service.TransferService
import com.orange5.nsts.service.TransferStatusService
import com.orange5.nsts.service.accountingcode.AccountingCodeService
import com.orange5.nsts.service.bin.BinService
import com.orange5.nsts.service.bin.ConsumableBinService
import com.orange5.nsts.service.consumable.ConsumableProductService
import com.orange5.nsts.service.customer.CustomerService
import com.orange5.nsts.service.discrepancy.DiscrepanciesService
import com.orange5.nsts.service.discrepancy.DiscrepanciesTypeService
import com.orange5.nsts.service.order.OrderService
import com.orange5.nsts.service.order.items.OrderConsumableItemService
import com.orange5.nsts.service.order.items.OrderRentalItemService
import com.orange5.nsts.service.parameters.ParametersService
import com.orange5.nsts.service.rental.RentalProductItemService
import com.orange5.nsts.service.rental.RentalProductService
import com.orange5.nsts.service.rentalReturn.RentalReturnService
import com.orange5.nsts.service.shipping.ReceivingLogService
import com.orange5.nsts.service.shipping.ShippingNoticeService
import com.orange5.nsts.service.user.RoleService
import com.orange5.nsts.service.user.UserService
import com.orange5.nsts.sync.InitLoadQueries
import com.orange5.nsts.sync.InitLoadQueries.ACCESSORY_CROSS_REFERENCE
import com.orange5.nsts.sync.InitLoadQueries.ACCOUNTING_CODE
import com.orange5.nsts.sync.InitLoadQueries.APP_STATE
import com.orange5.nsts.sync.InitLoadQueries.BIN_NUMBER
import com.orange5.nsts.sync.InitLoadQueries.BIN_TYPE
import com.orange5.nsts.sync.InitLoadQueries.CATEGORY
import com.orange5.nsts.sync.InitLoadQueries.CONSUMABLE_ALTERNATE_PRODUCT_NUMBER_XREF
import com.orange5.nsts.sync.InitLoadQueries.CONSUMABLE_BIN
import com.orange5.nsts.sync.InitLoadQueries.CONSUMABLE_PRODUCT
import com.orange5.nsts.sync.InitLoadQueries.CONSUMABLE_PRODUCT_CUSTOM_FIELD
import com.orange5.nsts.sync.InitLoadQueries.CUSTOMER
import com.orange5.nsts.sync.InitLoadQueries.CUSTOMER_CUSTOM_FIELD
import com.orange5.nsts.sync.InitLoadQueries.CUSTOMER_CUSTOM_FIELDS_TYPE
import com.orange5.nsts.sync.InitLoadQueries.CUSTOMER_POSITION
import com.orange5.nsts.sync.InitLoadQueries.CUSTOMER_PRODUCT_NUMBER_XREF
import com.orange5.nsts.sync.InitLoadQueries.CUSTOMER_ROLE
import com.orange5.nsts.sync.InitLoadQueries.CUSTOMER_STATUS
import com.orange5.nsts.sync.InitLoadQueries.DISCREPANCIES_RESOLVED_STATUS
import com.orange5.nsts.sync.InitLoadQueries.DISCREPANCIES_TYPE
import com.orange5.nsts.sync.InitLoadQueries.DISCREPANCY
import com.orange5.nsts.sync.InitLoadQueries.DISPLAY_COLUMN_NAME
import com.orange5.nsts.sync.InitLoadQueries.INVENTORY_ADJUSTMENT
import com.orange5.nsts.sync.InitLoadQueries.INVENTORY_ADJUSTMENT_REASON_CODE
import com.orange5.nsts.sync.InitLoadQueries.ORDER
import com.orange5.nsts.sync.InitLoadQueries.ORDER_CONSUMABLE_ITEM
import com.orange5.nsts.sync.InitLoadQueries.ORDER_CONSUMABLE_RETURN
import com.orange5.nsts.sync.InitLoadQueries.ORDER_CONSUMABLE_RETURN_ITEM_BY_CUSTOMER
import com.orange5.nsts.sync.InitLoadQueries.ORDER_RENTAL_ITEM
import com.orange5.nsts.sync.InitLoadQueries.ORDER_STATUS
import com.orange5.nsts.sync.InitLoadQueries.PARAMETER
import com.orange5.nsts.sync.InitLoadQueries.PICK_TICKETS
import com.orange5.nsts.sync.InitLoadQueries.RENTAL_PRODUCT
import com.orange5.nsts.sync.InitLoadQueries.RENTAL_PRODUCT_CUSTOM_FIELDS
import com.orange5.nsts.sync.InitLoadQueries.RENTAL_PRODUCT_ITEM
import com.orange5.nsts.sync.InitLoadQueries.RENTAL_PRODUCT_STATUS
import com.orange5.nsts.sync.InitLoadQueries.RENTAL_PRODUCT_STATUS_HISTORY
import com.orange5.nsts.sync.InitLoadQueries.RENTAL_RETURN
import com.orange5.nsts.sync.InitLoadQueries.ROLE
import com.orange5.nsts.sync.InitLoadQueries.SHIPPING_NOTICE
import com.orange5.nsts.sync.InitLoadQueries.TAG
import com.orange5.nsts.sync.InitLoadQueries.TRANSACTION_CUSTOM_FIELDS
import com.orange5.nsts.sync.InitLoadQueries.TRANSACTION_HISTORY
import com.orange5.nsts.sync.InitLoadQueries.TRANSACTION_TYPE
import com.orange5.nsts.sync.InitLoadQueries.TRANSFER
import com.orange5.nsts.sync.InitLoadQueries.TRANSFER_CARRIER
import com.orange5.nsts.sync.InitLoadQueries.TRANSFER_ITEM
import com.orange5.nsts.sync.InitLoadQueries.TRANSFER_STATUS
import com.orange5.nsts.sync.InitLoadQueries.USER
import timber.log.Timber
import javax.inject.Inject

class InitInsertFactory @Inject constructor(
        private val gson: Gson,
        private val accountingCodeService: AccountingCodeService,
        private val binService: BinService,
        private val consumableBinService: ConsumableBinService,
        private val consumableProductService: ConsumableProductService,
        private val customerService: CustomerService,
        private val orderConsumableItemService: OrderConsumableItemService,
        private val orderRentalItemService: OrderRentalItemService,
        private val orderService: OrderService,
        private val parametersService: ParametersService,
        private val rentalProductItemService: RentalProductItemService,
        private val rentalProductService: RentalProductService,
        private val rentalReturnService: RentalReturnService,
        private val userService: UserService,
        private val accessoryCrossReferenceService: AccessoryCrossReferenceService,
        private val appStateService: AppStateService,
        private val binTypeService: BinTypeService,
        private val categoryService: CategoryService,
        private val consumableAlternateProductNumberXrefService: ConsumableAlternateProductNumberXrefService,
        private val consumableProductBinHistoryService: ConsumableProductBinHistoryService,
        private val consumableProductCustomFieldsService: ConsumableProductCustomFieldsService,
        private val consumableProduct2TagService: ConsumableProduct2TagService,
        private val customer2RoleService: Customer2RoleService,
        private val roleService: RoleService,
        private val customerCustomFieldsService: CustomerCustomFieldsService,
        private val customerCustomFieldsTypesService: CustomerCustomFieldsTypesService,
        private val customerPositionService: CustomerPositionService,
        private val customerProductNumberXrefService: CustomerProductNumberXrefService,
        private val customerRoleService: CustomerRoleService,
        private val customerStatusService: CustomerStatusService,
        private val discrepanciesService: DiscrepanciesService,
        private val discrepanciesResolvedStatusService: DiscrepanciesResolvedStatusService,
        private val discrepanciesTypeService: DiscrepanciesTypeService,
        private val displayColumnNameService: DisplayColumnNameService,
        private val errorService: ErrorService,
        private val inventoryAdjustmentService: InventoryAdjustmentService,
        private val inventoryAdjustmentReasonCodeService: InventoryAdjustmentReasonCodeService,
        private val orderConsumableReturnService: OrderConsumableReturnService,
        private val orderConsumableReturnItemByCustomerService: OrderConsumableReturnItemByCustomerService,
        private val orderStatusService: OrderStatusService,
        private val pickTicketService: PickTicketService,
        private val productReceivingLogService: ReceivingLogService,
        private val rentalProduct2TagService: RentalProduct2TagService,
        private val rentalProductBinHistoryService: RentalProductBinHistoryService,
        private val rentalProductCustomFieldsService: RentalProductCustomFieldsService,
        private val rentalProductStatusService: RentalProductStatusService,
        private val rentalProductStatusHistoryService: RentalProductStatusHistoryService,
        private val shippingNoticeService: ShippingNoticeService,
        private val tag2CustomerRoleService: Tag2CustomerRoleService,
        private val tagService: TagService,
        private val transactionCustomFieldsService: TransactionCustomFieldsService,
        private val transactionHistoryService: TransactionHistoryService,
        private val transactionTypeService: TransactionTypeService,
        private val transferCarrierService: TransferCarrierService,
        private val transferService: TransferService,
        private val transferItemService: TransferItemService,
        private val transferStatusService: TransferStatusService
) {

    fun uploadData(loadQueries: InitLoadQueries, entity: InitLoadWrapper) {
        val insertedCount = runInsert(loadQueries, entity.entities)
        if (insertedCount != entity.entityCount) throw InitLoadException("Not all entities was inserted for ${loadQueries
                .entity}. Expected ${entity.entityCount} - inserted $insertedCount")
        else Timber.d("Init Load for ${loadQueries.entity} table - passed")
    }

    private fun runInsert(loadQueries: InitLoadQueries, entity: List<JsonElement>) =
            when (loadQueries) {
                ACCOUNTING_CODE -> accountingCodeService.initInsert(entity.parse(AccountingCode::class.java))
                BIN_NUMBER -> binService.initInsert(entity.parse(BinNumber::class.java))
                CONSUMABLE_BIN -> consumableBinService.initInsert(entity.parse(ConsumableBin::class.java))
                CONSUMABLE_PRODUCT -> consumableProductService.initInsert(entity.parse(ConsumableProduct::class.java))
                CUSTOMER -> customerService.initInsert(entity.parse(Customer::class.java))
                ORDER_CONSUMABLE_ITEM -> orderConsumableItemService.initInsert(entity.parse(OrderConsumableItem::class.java))
                ORDER_RENTAL_ITEM -> orderRentalItemService.initInsert(entity.parse(OrderRentalItem::class.java))
                ORDER -> orderService.initInsert(entity.parse(Order::class.java))
                PARAMETER -> parametersService.initInsert(entity.parse(Parameters::class.java))
                RENTAL_PRODUCT_ITEM -> rentalProductItemService.initInsert(entity.parse(RentalProductItem::class.java))
                RENTAL_PRODUCT -> rentalProductService.initInsert(entity.parse(RentalProduct::class.java))
                RENTAL_RETURN -> rentalReturnService.initInsert(entity.parse(RentalReturn::class.java))
                USER -> userService.initInsert(entity.parse(User::class.java))
                ACCESSORY_CROSS_REFERENCE -> accessoryCrossReferenceService.initInsert(entity.parse(AccessoryCrossReference::class.java))
                APP_STATE -> appStateService.initInsert(entity.parse(AppState::class.java))
                BIN_TYPE -> binTypeService.initInsert(entity.parse(BinType::class.java))
                CATEGORY -> categoryService.initInsert(entity.parse(Category::class.java))
                CONSUMABLE_ALTERNATE_PRODUCT_NUMBER_XREF -> consumableAlternateProductNumberXrefService.initInsert(entity.parse(ConsumableAlternateProductNumberXref::class.java))
                CONSUMABLE_PRODUCT_CUSTOM_FIELD -> consumableProductCustomFieldsService.initInsert(entity.parse(ConsumableProductCustomFields::class.java))
                CUSTOMER_ROLE -> customerRoleService.initInsert(entity.parse(CustomerRole::class.java))
                CUSTOMER_CUSTOM_FIELD -> customerCustomFieldsService.initInsert(entity.parse(CustomerCustomFields::class.java))
                CUSTOMER_CUSTOM_FIELDS_TYPE -> customerCustomFieldsTypesService.initInsert(entity.parse(CustomerCustomFieldsTypes::class.java))
                CUSTOMER_POSITION -> customerPositionService.initInsert(entity.parse(CustomerPosition::class.java))
                CUSTOMER_PRODUCT_NUMBER_XREF -> customerProductNumberXrefService.initInsert(entity.parse(CustomerProductNumberXref::class.java))
                CUSTOMER_STATUS -> customerStatusService.initInsert(entity.parse(CustomerStatus::class.java))
                DISCREPANCY -> discrepanciesService.initInsert(entity.parse(Discrepancies::class.java))
                DISCREPANCIES_RESOLVED_STATUS -> discrepanciesResolvedStatusService.initInsert(entity.parse(DiscrepanciesResolvedStatus::class.java))
                DISCREPANCIES_TYPE -> discrepanciesTypeService.initInsert(entity.parse(DiscrepanciesType::class.java))
                DISPLAY_COLUMN_NAME -> displayColumnNameService.initInsert(entity.parse(DisplayColumnName::class.java))
                INVENTORY_ADJUSTMENT -> inventoryAdjustmentService.initInsert(entity.parse(InventoryAdjustment::class.java))
                INVENTORY_ADJUSTMENT_REASON_CODE -> inventoryAdjustmentReasonCodeService.initInsert(entity.parse(InventoryAdjustmentReasonCode::class.java))
                ORDER_CONSUMABLE_RETURN -> orderConsumableReturnService.initInsert(entity.parse(OrderConsumableReturn::class.java))
                ORDER_CONSUMABLE_RETURN_ITEM_BY_CUSTOMER -> orderConsumableReturnItemByCustomerService.initInsert(entity.parse(OrderConsumableReturnItemByCustomer::class.java))
                ORDER_STATUS -> orderStatusService.initInsert(entity.parse(OrderStatus::class.java))
                PICK_TICKETS -> pickTicketService.initInsert(entity.parse(PickTicket::class.java))
                RENTAL_PRODUCT_CUSTOM_FIELDS -> rentalProductCustomFieldsService.initInsert(entity.parse(RentalProductCustomFields::class.java))
                RENTAL_PRODUCT_STATUS -> rentalProductStatusService.initInsert(entity.parse(RentalProductStatus::class.java))
                RENTAL_PRODUCT_STATUS_HISTORY -> rentalProductStatusHistoryService.initInsert(entity.parse(RentalProductStatusHistory::class.java))
                ROLE -> roleService.initInsert(entity.parse(Role::class.java))
                SHIPPING_NOTICE -> shippingNoticeService.initInsert(entity.parse(ShippingNotice::class.java))
                TAG -> tagService.initInsert(entity.parse(Tag::class.java))
                TRANSACTION_CUSTOM_FIELDS -> transactionCustomFieldsService.initInsert(entity.parse(TransactionCustomFields::class.java))
                TRANSACTION_HISTORY -> transactionHistoryService.initInsert(entity.parse(TransactionHistory::class.java))
                TRANSACTION_TYPE -> transactionTypeService.initInsert(entity.parse(TransactionType::class.java))
                TRANSFER_CARRIER -> transferCarrierService.initInsert(entity.parse(TransferCarrier::class.java))
                TRANSFER -> transferService.initInsert(entity.parse(Transfer::class.java))
                TRANSFER_ITEM -> transferItemService.initInsert(entity.parse(TransferItem::class.java))
                TRANSFER_STATUS -> transferStatusService.initInsert(entity.parse(TransferStatus::class.java))
            }

    private fun <T> List<JsonElement>.parse(clazz: Class<T>) = this.map { gson.fromJson<T>(it, clazz) }
}


/*   CUSTOMER_CUSTOM_FIELD -> consumableProduct2TagService.initInsert(elements.parse(ConsumableProduct2Tag::class.java))
   CUSTOMER_CUSTOM_FIELD -> customer2RoleService.initInsert(elements.parse(Customer2Role::class.java))
   CUSTOMER_CUSTOM_FIELD -> rentalProduct2TagService.initInsert(elements.parse(RentalProduct2Tag::class.java))
   TAG -> tag2CustomerRoleService.initInsert(elements.parse(Tag2CustomerRole::class.java))
   RENTAL_PRODUCT_BIN_HISTORY -> rentalProductBinHistoryService.initInsert(entity.parse(RentalProductBinHistory::class.java))
   CONSUMABLE_PRODUCT_BIN_HISTORY -> consumableProductBinHistoryService.initInsert(entity.parse
 (ConsumableProductBinHistory::class.java))

   */
