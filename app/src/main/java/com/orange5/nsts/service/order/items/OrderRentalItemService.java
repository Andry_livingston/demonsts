package com.orange5.nsts.service.order.items;

import com.orange5.nsts.data.db.dao.OrderRentalItemDao;
import com.orange5.nsts.data.db.dao.RentalProductItemDao;
import com.orange5.nsts.data.db.entities.OrderRentalItem;
import com.orange5.nsts.data.db.entities.RentalProductItem;
import com.orange5.nsts.service.base.SyncingService;
import com.orange5.nsts.service.rental.RentalStatus;
import com.orange5.nsts.sync.local.LocalSyncHelper;

import org.greenrobot.greendao.query.QueryBuilder;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import static com.orange5.nsts.di.modules.SyncModule.ORDER_RENTAL_ITEM_SERVICE;

public class OrderRentalItemService extends SyncingService<OrderRentalItemDao, OrderRentalItem> {

    @Inject
    @Named(ORDER_RENTAL_ITEM_SERVICE)
    LocalSyncHelper helper;

    @Inject
    public OrderRentalItemService(OrderRentalItemDao dao) {
        super(dao);
    }

    @Override
    protected LocalSyncHelper getSyncHelper() {
        return helper;
    }

    public void saveAll(List<OrderRentalItem> items) {
        items.forEach(this::save);
    }

    public void save(OrderRentalItem item) {
        if (ifExists(item)) {
            update(item);
        } else {
            insert(item);
        }
    }

    public void insert(OrderRentalItem item) {
        if (ifExists(item)) {
            update(item);
        } else {
            super.insert(item);
        }
    }

    public void update(OrderRentalItem item) {
        super.update(item);
    }

    public void delete(OrderRentalItem item) {
        super.delete(item);
    }

    public void insertAll(List<OrderRentalItem> rentalItems) {
        rentalItems.forEach(this::insert);
    }

    public void deleteAll(List<OrderRentalItem> rentalItems) {
        rentalItems.forEach(this::delete);
    }

    public List<OrderRentalItem> loadAll() {
        return dao.loadAll();
    }

    private boolean ifExists(@NotNull OrderRentalItem item) {
        return loadItemById(item.getOrderItemId()) != null;
    }

    public List<OrderRentalItem> getItemsByReturnId(String id) {
        return dao
                .queryBuilder()
                .where(OrderRentalItemDao.Properties.RentalReturnId.eq(id))
                .list();
    }

    public OrderRentalItem loadItemById(String id) {
        return dao
                .queryBuilder()
                .where(OrderRentalItemDao.Properties.OrderItemId.eq(id))
                .unique();
    }

    public OrderRentalItem loadItemByUUN(String uun) {
        return dao
                .queryBuilder()
                .where(OrderRentalItemDao.Properties.UniqueUnitNumber.eq(uun))
                .unique();
    }

    public List<OrderRentalItem> loadByStatus(RentalStatus status) {
        QueryBuilder<OrderRentalItem> queryBuilder = dao
                .queryBuilder();
        queryBuilder.where(OrderRentalItemDao.Properties.UniqueUnitNumber.isNotNull(),
                OrderRentalItemDao.Properties.ProductId.isNotNull(),
                OrderRentalItemDao.Properties.RentalReturnId.isNull()) //TODO quick fix check if needed
                .distinct()
                .join(OrderRentalItemDao.Properties.UniqueUnitNumber, RentalProductItem.class,
                        RentalProductItemDao.Properties.UniqueUnitNumber)
                .where(RentalProductItemDao.Properties.Status.eq(status.getId()));

        return queryBuilder.orderDesc(OrderRentalItemDao.Properties.UniqueUnitNumber).list();
    }

    public List<OrderRentalItem> loadByStatusAndBin(RentalStatus status, String binId) {
        QueryBuilder<OrderRentalItem> queryBuilder = dao.queryBuilder();
        queryBuilder.where(OrderRentalItemDao.Properties.UniqueUnitNumber.isNotNull(),
                OrderRentalItemDao.Properties.ProductId.isNotNull())
                .distinct()
                .join(OrderRentalItemDao.Properties.UniqueUnitNumber, RentalProductItem.class, RentalProductItemDao.Properties.UniqueUnitNumber)
                .where(RentalProductItemDao.Properties.Status.eq(status.getId()),
                        RentalProductItemDao.Properties.BinId.eq(binId));
        List<OrderRentalItem> items = queryBuilder.orderAsc(OrderRentalItemDao.Properties.UniqueUnitNumber).list();
        List<OrderRentalItem> lastItems = new ArrayList<>();
        List<String> uuns = new ArrayList<>();
        items.forEach(it -> {
            if (!uuns.contains(it.getUniqueUnitNumber())) {
                uuns.add(it.getUniqueUnitNumber());
                lastItems.add(it);
            }
        });
        return lastItems;
    }
}
