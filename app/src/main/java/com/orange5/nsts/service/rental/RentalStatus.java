package com.orange5.nsts.service.rental;

import com.orange5.nsts.data.db.entities.RentalProductItem;

public enum RentalStatus {
    Available(1, "Available"),
    CheckedOut(2, "Checked out"),
    Lost(3, "Lost"),
    Scrapped(4, "Scrapped"),
    Allocated(5, "Allocated"),
    InRepair(6, "In Repair"),
    Inspection(7, "Inspection"),
    Damaged(8, "Damaged");

    private Integer id;
    private String name;

    RentalStatus(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean matches(RentalProductItem item) {
        return item != null && id.equals(item.getStatus());
    }

    public void set(RentalProductItem item) {
        item.setStatus(id);
    }
}
