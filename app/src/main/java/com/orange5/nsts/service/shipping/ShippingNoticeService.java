package com.orange5.nsts.service.shipping;

import com.orange5.nsts.data.db.dao.ShippingNoticeDao;
import com.orange5.nsts.data.db.entities.ShippingNotice;
import com.orange5.nsts.service.base.SyncingService;
import com.orange5.nsts.sync.local.LocalSyncHelper;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Named;

import static com.orange5.nsts.di.modules.SyncModule.SHIPPING_NOTICE_SERVICE;


public class ShippingNoticeService extends SyncingService<ShippingNoticeDao, ShippingNotice> {

    @Inject
    @Named(SHIPPING_NOTICE_SERVICE)
    LocalSyncHelper helper;

    @Inject
    public ShippingNoticeService(ShippingNoticeDao dao) {
        super(dao);
    }

    @Override
    protected LocalSyncHelper getSyncHelper() {
        return helper;
    }

    public List<ShippingNotice> getUnprocessedNotices() {
        return dao
                .queryBuilder()
                .orderDesc(ShippingNoticeDao.Properties.ShippingDate)
                .list().stream().filter(this::isNotProcessed).collect(Collectors.toList());
    }

    public List<ShippingNotice> byTransferNumber(String number) {
        return dao.queryBuilder()
                .where(ShippingNoticeDao.Properties.TransferNumber.eq(number))
                .list()
                .stream().filter(this::isNotProcessed).collect(Collectors.toList());
    }

    public ShippingNotice byId(String id) {
        return dao.queryBuilder().where(ShippingNoticeDao.Properties.ShippingNoticeId.eq(id)).unique();
    }

    public void insert(ShippingNotice shippingNotice) {
        super.insert(shippingNotice);
    }

    public void delete(ShippingNotice shippingNotice) {
        super.delete(shippingNotice);
    }

    public void update(ShippingNotice shippingNotice) {
        super.update(shippingNotice);
    }

    private boolean isNotProcessed(ShippingNotice notice) {
        boolean isProcessed;
        boolean isRental = notice.getRentalUniqueUnitNumber() != null;
        if (isRental) isProcessed = notice.isProcessed();
        else isProcessed = notice.isProcessed() || notice.getConsumableQuantity() == 0;
        return !isProcessed;
    }
}