package com.orange5.nsts.service.product

import com.orange5.nsts.data.preferencies.RawSharedPreferences

interface SearchFilter {
    var shouldShowRentals: Boolean
    var shouldShowConsumables: Boolean
    var shouldShowZeroQuantity: Boolean
    var query: String
}

open class ProductSearchFilter(private val preferences: RawSharedPreferences) : SearchFilter {

    override var shouldShowRentals: Boolean
        get() = preferences.shouldShowRentals
        set(value) {
            preferences.shouldShowRentals = value
        }
    override var shouldShowConsumables: Boolean
        get() = preferences.shouldShowConsumables
        set(value) {
            preferences.shouldShowConsumables = value
        }
    override var shouldShowZeroQuantity: Boolean
        get() = preferences.shouldShowZeroQuantity
        set(value) {
            preferences.shouldShowZeroQuantity = value
        }
    override var query: String = ""

}

class ExtrasProductSearchFilter : SearchFilter {
    override var shouldShowRentals = false
    override var shouldShowConsumables = true
    override var shouldShowZeroQuantity = true
    override var query: String = ""
}

