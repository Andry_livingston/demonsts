package com.orange5.nsts.service.order;

import com.orange5.nsts.data.db.dao.OrderDao;
import com.orange5.nsts.data.db.dao.OrderDao.Properties;
import com.orange5.nsts.data.db.entities.Customer;
import com.orange5.nsts.data.db.entities.Order;
import com.orange5.nsts.service.base.QueriesUtils;
import com.orange5.nsts.service.base.SyncingService;
import com.orange5.nsts.sync.local.LocalSyncHelper;

import org.greenrobot.greendao.query.QueryBuilder;
import org.greenrobot.greendao.query.WhereCondition;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import static com.orange5.nsts.di.modules.SyncModule.ORDER_SERVICE;
import static com.orange5.nsts.service.base.QueriesUtils.validQuery;

public class OrderService extends SyncingService<OrderDao, Order> {

    public static final int ORDERS_COUNT_LIMIT = 300;
    @Inject
    @Named(ORDER_SERVICE)
    LocalSyncHelper helper;

    @Inject
    public OrderService(OrderDao orderDao) {
        super(orderDao);
    }

    public List<Order> getOrders(String query) {
        QueryBuilder<Order> queryBuilder = dao.queryBuilder();
        if (validQuery(query)) addSearchQuery(queryBuilder, query);

        queryBuilder.where(Properties.OrderStatusId.notEq(OrderStatus.closed.getId())).orderDesc(Properties.OrderDate);

        List<Order> result = queryBuilder.list();
        int size = result.size();
        if (size < ORDERS_COUNT_LIMIT) result.addAll(getClosedOrders(ORDERS_COUNT_LIMIT - size, query));
        return result;
    }

    public Order getOrderById(String id) {
        Order order = dao.load(id);
        order.resetConsumableItems();
        order.resetRentalItems();
        return order;
    }

    public Order getLastOrder(String orderPrefix) {
        return dao
                .queryBuilder()
                .where(OrderDao.Properties.OrderNumber.like(orderPrefix + "%"))
                .orderDesc(OrderDao.Properties.OrderDate)
                .limit(1)
                .unique();
    }

    public void delete(Order order) {
        super.delete(order);
    }

    public void insert(Order order) {
        super.insert(order);
    }

    public void update(Order order) {
        super.update(order);
    }

    public void resetConsumable(Order order) {
        order.resetConsumableItems();
    }

    public void refresh(Order order) {
        dao.refresh(order);
    }

    @Override
    protected LocalSyncHelper getSyncHelper() {
        return helper;
    }

    private List<Order> getClosedOrders(int limit, String query) {
        QueryBuilder<Order> queryBuilder = dao.queryBuilder();
        if (validQuery(query)) addSearchQuery(queryBuilder, query);
        return queryBuilder
                .where(Properties.OrderStatusId.eq(OrderStatus.closed.getId()))
                .limit(limit)
                .orderDesc(Properties.OrderDate)
                .list();
    }

    private void addSearchQuery(QueryBuilder<Order> builder, String query) {
        query = QueriesUtils.addWildcards(query);
        builder.join(Properties.CreatorCustomerId, Customer.class)
                .whereOr(
                        new WhereCondition.StringCondition("J1.\"FName\"||' '||' '||J1.\"LName\" like ?", query),
                        new WhereCondition.StringCondition("OrderNumber like ?", query)
                );
    }
}
