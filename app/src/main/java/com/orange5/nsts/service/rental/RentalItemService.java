package com.orange5.nsts.service.rental;

import com.orange5.nsts.data.db.entities.BinNumber;
import com.orange5.nsts.data.db.entities.OrderRentalItem;
import com.orange5.nsts.data.db.entities.RentalProduct;
import com.orange5.nsts.data.db.entities.RentalProductItem;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RentalItemService {

    public static List<OrderRentalItem> getPickedItems(List<OrderRentalItem> rentalItems) {
        return rentalItems.stream()
                .filter(orderRentalItem -> orderRentalItem.getUniqueUnitNumber() == null)
                .collect(Collectors.toList());
    }

    public static List<AggregatedRentalItem> getAggregatedRentalsList(List<OrderRentalItem> rentalItems) {
        List<AggregatedRentalItem> result = new ArrayList<>();
        boolean found;
        for (OrderRentalItem selectedRentalItem : rentalItems) {
            found = false;
            for (AggregatedRentalItem existing : result) {
                if (existing.accepts(selectedRentalItem)) {
                    existing.add(selectedRentalItem);
                    found = true;
                    break;
                }
            }
            if (!found) {
                result.add(new AggregatedRentalItem(selectedRentalItem));
            }
        }
        return result;
    }

    public static List<OrderRentalItem> getAllRentalProductItems(RentalProduct rentalProduct, List<OrderRentalItem> rentalItems) {
        return rentalItems.stream()
                .filter(item -> item.getProductNumber().equals(rentalProduct.getProductNumber()))
                .collect(Collectors.toList());
    }


    public static List<OrderRentalItem> getAllSimilarRentalItems(OrderRentalItem orderRentalItem) {
        return orderRentalItem.getOrder()
                .getRentalItems()
                .stream()
                .filter(item -> item.getProductNumber().equals(orderRentalItem.getProductNumber()))
                .collect(Collectors.toList());
    }

    public static long getPickedCount(List<OrderRentalItem> rentalItems) {
        return getPickedItems(rentalItems).size();
    }

    public static Map<BinNumber, List<String>> getAllUniqueNumbers(OrderRentalItem item) {
        Map<BinNumber, List<String>> result = new LinkedHashMap<>();
        item.getRentalProduct().getProductItems().forEach(rentalProductItem -> {
            BinNumber key = rentalProductItem.getContainerBin();
            result.put(key, new ArrayList<>());
        });

        item.getRentalProduct().getProductItems().forEach(rentalProductItem -> {
            BinNumber key = rentalProductItem.getContainerBin();
            if (key == null) {
                return;
            }
            String uniqueUnitNumber = rentalProductItem.getUniqueUnitNumber();
            if (uniqueUnitNumber == null || uniqueUnitNumber.isEmpty()) {
                return;
            }
            result.get(key).add(uniqueUnitNumber);
        });
        return result;
    }

    public static Map<BinNumber, List<String>> getAllUniqueNumbers(RentalProduct product) {
        Map<BinNumber, List<String>> result = new LinkedHashMap<>();
        product.getProductItems().forEach(rentalProductItem -> {
            BinNumber key = rentalProductItem.getContainerBin();
            result.put(key, new ArrayList<>());
        });

        product.getProductItems().forEach(rentalProductItem -> {
            BinNumber key = rentalProductItem.getContainerBin();
            if (key == null) {
                return;
            }
            String uniqueUnitNumber = rentalProductItem.getUniqueUnitNumber();
            if (uniqueUnitNumber == null || uniqueUnitNumber.isEmpty()) {
                return;
            }
            result.get(key).add(uniqueUnitNumber);
        });
        return result;
    }

    public static List<RentalProductItem> getAllRentalItems(RentalProduct product) {
        List<RentalProductItem> result;
        result = product.getProductItems();
        return result;
    }
}
