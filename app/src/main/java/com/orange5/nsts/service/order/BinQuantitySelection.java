package com.orange5.nsts.service.order;

import java.util.Locale;

public class BinQuantitySelection {
    private final String binNumber;
    private Integer selectedQuantity;
    private Integer maxQuantity;

    public BinQuantitySelection(String binNumber, Integer maxQuantity) {
        this.binNumber = binNumber;
        this.maxQuantity = maxQuantity;
        selectedQuantity = 0;
    }
    public BinQuantitySelection(String binNumber, Integer maxQuantity, Integer selectedQuantity) {
        this.binNumber = binNumber;
        this.maxQuantity = maxQuantity;
        this.selectedQuantity = selectedQuantity;
    }

    @Override
    public String toString() {
        return String.format(Locale.getDefault(), "%s ( %d )", binNumber, selectedQuantity);
    }

    public String getBinNumber() {
        return binNumber;
    }

    public Integer getMaxQuantity() {
        return maxQuantity;
    }

    public Integer getSelectedQuantity() {
        return selectedQuantity;
    }

    public BinQuantitySelection setSelectedQuantity(Integer selectedQuantity) {
        this.selectedQuantity = selectedQuantity;
        return this;
    }

    public BinQuantitySelection setMaxQuantity(Integer maxQuantity) {
        this.maxQuantity = maxQuantity;
        return this;
    }
}
