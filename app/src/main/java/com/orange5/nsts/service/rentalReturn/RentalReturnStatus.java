package com.orange5.nsts.service.rentalReturn;

import com.orange5.nsts.data.db.entities.RentalReturn;
import com.orange5.nsts.util.Colors;

public enum RentalReturnStatus {
    open("Open", Colors.LIGHT_GREEN),
    processed("Processed", Colors.GRAY);


    private String name;
    private int color;

    RentalReturnStatus(String name, int color) {
        this.name = name;
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public static RentalReturnStatus of(RentalReturn rentalReturn) {
        if (rentalReturn.getCompleteDate() == null) {
            return open;
        }
        return processed;
    }
}
