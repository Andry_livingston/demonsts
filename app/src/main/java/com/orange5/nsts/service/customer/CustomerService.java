package com.orange5.nsts.service.customer;

import com.orange5.nsts.data.db.dao.CustomerDao;
import com.orange5.nsts.data.db.entities.Customer;
import com.orange5.nsts.service.base.DataBaseService;

import org.jetbrains.annotations.NotNull;

import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

public class CustomerService extends DataBaseService<CustomerDao, Customer> {

    @Inject
    public CustomerService(CustomerDao dao) {
        super(dao);
    }

    public List<Customer> loadAll() {
        List<Customer> list = dao.queryBuilder()
                .orderAsc(CustomerDao.Properties.LName).list();
        list.forEach(customer -> customer.setLName(customer.getLName() == null ? "" : customer.getLName().trim()));
        list.sort(Comparator.comparing(Customer::getLName));
        return list;
    }

    public Customer byBadgeNumber(String scannedBarcode) {
        return dao.queryBuilder()
                .where(CustomerDao.Properties.BadgeNumber.eq(scannedBarcode))
                .unique();
    }

    public Customer loadById(@NotNull String customerId) {
        return dao.queryBuilder()
                .where(CustomerDao.Properties.CustomerId.eq(customerId))
                .unique();
    }
}
