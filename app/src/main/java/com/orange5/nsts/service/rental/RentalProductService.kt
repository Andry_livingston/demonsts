package com.orange5.nsts.service.rental

import com.orange5.nsts.data.db.dao.RentalProductDao
import com.orange5.nsts.data.db.entities.RentalProduct
import com.orange5.nsts.service.base.DataBaseService
import javax.inject.Inject

class RentalProductService @Inject constructor(dao: RentalProductDao) : DataBaseService<RentalProductDao, RentalProduct>(dao) {

    fun byProductNumber(productNumber: String): RentalProduct? = dao
            .queryBuilder()
            .where(RentalProductDao.Properties.ProductNumber.eq(productNumber))
            .unique()

    fun byProductId(id: String): RentalProduct? = dao
            .queryBuilder()
            .where(RentalProductDao.Properties.ProductId.eq(id))
            .unique()

}