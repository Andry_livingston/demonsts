package com.orange5.nsts.service.base;

import com.orange5.nsts.data.db.dao.SyncTransferObjectDao;
import com.orange5.nsts.data.db.entities.SyncTransferObject;
import com.orange5.nsts.util.CollectionUtils;
import com.orange5.nsts.util.format.DateFormatter;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class SyncObjectService {

    private static final int ITEMS_QUANTITY = 10;
    private final SyncTransferObjectDao syncDao;

    public SyncObjectService(SyncTransferObjectDao syncDao) {
        this.syncDao = syncDao;
    }

    public void saveInDataBase(SyncTransferObject object) {
        long key = object.getIncrementNumber();
        syncDao.insert(object);
        if (object.getIncrementNumber() != key) object.setIncrementNumber(key);
        syncDao.refresh(object);
    }

    public void deleteAll() {
        syncDao.deleteAll();
    }

    public boolean isAllDataSynced() {
       List<SyncTransferObject> list =
               syncDao.queryBuilder()
                       .where(SyncTransferObjectDao.Properties.IsMine.eq(true), SyncTransferObjectDao.Properties.Executed.isNull())
                       .list();
       return list.size() == 0;
    }

    public void update(SyncTransferObject object) {
        syncDao.update(object);
        syncDao.refresh(object);
    }

    public void delete(SyncTransferObject object) {
        syncDao.delete(object);
        syncDao.refresh(object);
    }

    public List<SyncTransferObject> getObjects() {
        QueryBuilder<SyncTransferObject> builder = syncDao.queryBuilder();
        return builder.where(SyncTransferObjectDao.Properties.IsMine.eq(true), SyncTransferObjectDao.Properties.Executed.isNull())
                .orderAsc(SyncTransferObjectDao.Properties.DateTimeSaved)
                .orderAsc(SyncTransferObjectDao.Properties.IncrementNumber)
                .limit(ITEMS_QUANTITY)
                .list();
    }

    public void markSyncObjectAsExecuted(String[] ids) {
        if (ids != null && ids.length > 0) {
            List<SyncTransferObject> objects = new ArrayList<>();
            for (String id : ids) {
                SyncTransferObject object = syncDao.queryBuilder()
                        .where(SyncTransferObjectDao.Properties.TransferObjectId.eq(id))
                        .unique();
                if (object != null) {
                    objects.add(object);
                }
            }
            if (CollectionUtils.isNotEmpty(objects)) {
                objects.forEach(it -> it.setExecuted(DateFormatter.currentTimeDb()));
                syncDao.updateInTx(objects);
            }
        }
    }

    public long getLastIncrementNumber() {
        long max;
        List<SyncTransferObject> items = syncDao.loadAll();
        if (CollectionUtils.isEmpty(items)) {
            max = 0;
        } else {
            try {
                max = items.stream()
                        .max(Comparator.comparing(SyncTransferObject::getIncrementNumber))
                        .get().getIncrementNumber();
            } catch (NoSuchElementException e) {
                max = items.size();
            }
        }
        return max;
    }
}