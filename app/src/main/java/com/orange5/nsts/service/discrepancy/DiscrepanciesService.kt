package com.orange5.nsts.service.discrepancy

import com.orange5.nsts.data.db.dao.DiscrepanciesDao
import com.orange5.nsts.data.db.entities.Discrepancies
import com.orange5.nsts.di.modules.SyncModule.DISCREPANCIES_SERVICE
import com.orange5.nsts.service.base.SyncingService
import com.orange5.nsts.sync.local.LocalSyncHelper
import javax.inject.Inject
import javax.inject.Named

class DiscrepanciesService @Inject constructor(dao: DiscrepanciesDao) : SyncingService<DiscrepanciesDao, Discrepancies>(dao) {

    @Inject
    @Named(DISCREPANCIES_SERVICE)
    lateinit var localSyncHelper: LocalSyncHelper

    override fun getSyncHelper(): LocalSyncHelper = localSyncHelper

    fun loadNonResolved(): List<Discrepancies> = dao.queryBuilder()
            .whereOr(DiscrepanciesDao.Properties.IsResolved.eq(FALSE), DiscrepanciesDao.Properties.IsResolved.isNull)
            .orderDesc(DiscrepanciesDao.Properties.AddingDate).list()

    fun insertAll(items: List<Discrepancies>) {
        items.forEach(::insert)
    }

    companion object {
        const val FALSE = 0
    }
}