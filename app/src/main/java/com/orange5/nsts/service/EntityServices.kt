package com.orange5.nsts.service

import com.orange5.nsts.data.db.dao.AccessoryCrossReferenceDao
import com.orange5.nsts.data.db.dao.AppStateDao
import com.orange5.nsts.data.db.dao.BinTypeDao
import com.orange5.nsts.data.db.dao.CategoryDao
import com.orange5.nsts.data.db.dao.ConsumableAlternateProductNumberXrefDao
import com.orange5.nsts.data.db.dao.ConsumableProduct2TagDao
import com.orange5.nsts.data.db.dao.ConsumableProductBinHistoryDao
import com.orange5.nsts.data.db.dao.ConsumableProductCustomFieldsDao
import com.orange5.nsts.data.db.dao.Customer2RoleDao
import com.orange5.nsts.data.db.dao.CustomerCustomFieldsDao
import com.orange5.nsts.data.db.dao.CustomerCustomFieldsTypesDao
import com.orange5.nsts.data.db.dao.CustomerPositionDao
import com.orange5.nsts.data.db.dao.CustomerProductNumberXrefDao
import com.orange5.nsts.data.db.dao.CustomerRoleDao
import com.orange5.nsts.data.db.dao.CustomerStatusDao
import com.orange5.nsts.data.db.dao.DiscrepanciesDao
import com.orange5.nsts.data.db.dao.DiscrepanciesResolvedStatusDao
import com.orange5.nsts.data.db.dao.DiscrepanciesTypeDao
import com.orange5.nsts.data.db.dao.DisplayColumnNameDao
import com.orange5.nsts.data.db.dao.ErrorDao
import com.orange5.nsts.data.db.dao.InventoryAdjustmentDao
import com.orange5.nsts.data.db.dao.InventoryAdjustmentReasonCodeDao
import com.orange5.nsts.data.db.dao.OrderConsumableReturnDao
import com.orange5.nsts.data.db.dao.OrderConsumableReturnItemByCustomerDao
import com.orange5.nsts.data.db.dao.OrderStatusDao
import com.orange5.nsts.data.db.dao.PickTicketDao
import com.orange5.nsts.data.db.dao.ProductReceivingLogDao
import com.orange5.nsts.data.db.dao.RentalProduct2TagDao
import com.orange5.nsts.data.db.dao.RentalProductBinHistoryDao
import com.orange5.nsts.data.db.dao.RentalProductCustomFieldsDao
import com.orange5.nsts.data.db.dao.RentalProductStatusDao
import com.orange5.nsts.data.db.dao.RentalProductStatusHistoryDao
import com.orange5.nsts.data.db.dao.ShippingNoticeDao
import com.orange5.nsts.data.db.dao.Tag2CustomerRoleDao
import com.orange5.nsts.data.db.dao.TagDao
import com.orange5.nsts.data.db.dao.TransactionCustomFieldsDao
import com.orange5.nsts.data.db.dao.TransactionHistoryDao
import com.orange5.nsts.data.db.dao.TransactionTypeDao
import com.orange5.nsts.data.db.dao.TransferCarrierDao
import com.orange5.nsts.data.db.dao.TransferDao
import com.orange5.nsts.data.db.dao.TransferItemDao
import com.orange5.nsts.data.db.dao.TransferStatusDao
import com.orange5.nsts.data.db.entities.AccessoryCrossReference
import com.orange5.nsts.data.db.entities.AppState
import com.orange5.nsts.data.db.entities.BinType
import com.orange5.nsts.data.db.entities.Category
import com.orange5.nsts.data.db.entities.ConsumableAlternateProductNumberXref
import com.orange5.nsts.data.db.entities.ConsumableProduct2Tag
import com.orange5.nsts.data.db.entities.ConsumableProductBinHistory
import com.orange5.nsts.data.db.entities.ConsumableProductCustomFields
import com.orange5.nsts.data.db.entities.Customer2Role
import com.orange5.nsts.data.db.entities.CustomerCustomFields
import com.orange5.nsts.data.db.entities.CustomerCustomFieldsTypes
import com.orange5.nsts.data.db.entities.CustomerPosition
import com.orange5.nsts.data.db.entities.CustomerProductNumberXref
import com.orange5.nsts.data.db.entities.CustomerRole
import com.orange5.nsts.data.db.entities.CustomerStatus
import com.orange5.nsts.data.db.entities.Discrepancies
import com.orange5.nsts.data.db.entities.DiscrepanciesResolvedStatus
import com.orange5.nsts.data.db.entities.DiscrepanciesType
import com.orange5.nsts.data.db.entities.DisplayColumnName
import com.orange5.nsts.data.db.entities.Error
import com.orange5.nsts.data.db.entities.InventoryAdjustment
import com.orange5.nsts.data.db.entities.InventoryAdjustmentReasonCode
import com.orange5.nsts.data.db.entities.OrderConsumableReturn
import com.orange5.nsts.data.db.entities.OrderConsumableReturnItemByCustomer
import com.orange5.nsts.data.db.entities.OrderStatus
import com.orange5.nsts.data.db.entities.PickTicket
import com.orange5.nsts.data.db.entities.ProductReceivingLog
import com.orange5.nsts.data.db.entities.RentalProduct2Tag
import com.orange5.nsts.data.db.entities.RentalProductBinHistory
import com.orange5.nsts.data.db.entities.RentalProductCustomFields
import com.orange5.nsts.data.db.entities.RentalProductStatus
import com.orange5.nsts.data.db.entities.RentalProductStatusHistory
import com.orange5.nsts.data.db.entities.ShippingNotice
import com.orange5.nsts.data.db.entities.Tag
import com.orange5.nsts.data.db.entities.Tag2CustomerRole
import com.orange5.nsts.data.db.entities.TransactionCustomFields
import com.orange5.nsts.data.db.entities.TransactionHistory
import com.orange5.nsts.data.db.entities.TransactionType
import com.orange5.nsts.data.db.entities.Transfer
import com.orange5.nsts.data.db.entities.TransferCarrier
import com.orange5.nsts.data.db.entities.TransferItem
import com.orange5.nsts.data.db.entities.TransferStatus
import com.orange5.nsts.service.base.DataBaseService
import javax.inject.Inject

class AccessoryCrossReferenceService @Inject constructor(dao: AccessoryCrossReferenceDao) : DataBaseService<AccessoryCrossReferenceDao, AccessoryCrossReference>(dao)

class AppStateService @Inject constructor(dao: AppStateDao) : DataBaseService<AppStateDao, AppState>(dao)

class BinTypeService @Inject constructor(dao: BinTypeDao) : DataBaseService<BinTypeDao, BinType>(dao)

class CategoryService @Inject constructor(dao: CategoryDao) : DataBaseService<CategoryDao, Category>(dao)

class ConsumableAlternateProductNumberXrefService @Inject constructor(dao: ConsumableAlternateProductNumberXrefDao) : DataBaseService<ConsumableAlternateProductNumberXrefDao, ConsumableAlternateProductNumberXref>(dao)

class ConsumableProduct2TagService @Inject constructor(dao: ConsumableProduct2TagDao) : DataBaseService<ConsumableProduct2TagDao, ConsumableProduct2Tag>(dao)

class ConsumableProductBinHistoryService @Inject constructor(dao: ConsumableProductBinHistoryDao) : DataBaseService<ConsumableProductBinHistoryDao, ConsumableProductBinHistory>(dao)

class ConsumableProductCustomFieldsService @Inject constructor(dao: ConsumableProductCustomFieldsDao) : DataBaseService<ConsumableProductCustomFieldsDao, ConsumableProductCustomFields>(dao)

class Customer2RoleService @Inject constructor(dao: Customer2RoleDao) : DataBaseService<Customer2RoleDao, Customer2Role>(dao)

class CustomerCustomFieldsService @Inject constructor(dao: CustomerCustomFieldsDao) : DataBaseService<CustomerCustomFieldsDao, CustomerCustomFields>(dao)

class CustomerCustomFieldsTypesService @Inject constructor(dao: CustomerCustomFieldsTypesDao) : DataBaseService<CustomerCustomFieldsTypesDao, CustomerCustomFieldsTypes>(dao)

class CustomerPositionService @Inject constructor(dao: CustomerPositionDao) : DataBaseService<CustomerPositionDao, CustomerPosition>(dao)

class CustomerProductNumberXrefService @Inject constructor(dao: CustomerProductNumberXrefDao) : DataBaseService<CustomerProductNumberXrefDao, CustomerProductNumberXref>(dao)

class CustomerRoleService @Inject constructor(dao: CustomerRoleDao) : DataBaseService<CustomerRoleDao, CustomerRole>(dao)

class CustomerStatusService @Inject constructor(dao: CustomerStatusDao) : DataBaseService<CustomerStatusDao, CustomerStatus>(dao)

class DiscrepanciesResolvedStatusService @Inject constructor(dao: DiscrepanciesResolvedStatusDao) : DataBaseService<DiscrepanciesResolvedStatusDao, DiscrepanciesResolvedStatus>(dao)

class DisplayColumnNameService @Inject constructor(dao: DisplayColumnNameDao) : DataBaseService<DisplayColumnNameDao, DisplayColumnName>(dao)

class ErrorService @Inject constructor(dao: ErrorDao) : DataBaseService<ErrorDao, Error>(dao)

class InventoryAdjustmentService @Inject constructor(dao: InventoryAdjustmentDao) : DataBaseService<InventoryAdjustmentDao, InventoryAdjustment>(dao)

class InventoryAdjustmentReasonCodeService @Inject constructor(dao: InventoryAdjustmentReasonCodeDao) : DataBaseService<InventoryAdjustmentReasonCodeDao, InventoryAdjustmentReasonCode>(dao)

class OrderConsumableReturnService @Inject constructor(dao: OrderConsumableReturnDao) : DataBaseService<OrderConsumableReturnDao, OrderConsumableReturn>(dao)

class OrderConsumableReturnItemByCustomerService @Inject constructor(dao: OrderConsumableReturnItemByCustomerDao) : DataBaseService<OrderConsumableReturnItemByCustomerDao, OrderConsumableReturnItemByCustomer>(dao)

class OrderStatusService @Inject constructor(dao: OrderStatusDao) : DataBaseService<OrderStatusDao, OrderStatus>(dao)

class PickTicketService @Inject constructor(dao: PickTicketDao) : DataBaseService<PickTicketDao, PickTicket>(dao)

class RentalProduct2TagService @Inject constructor(dao: RentalProduct2TagDao) : DataBaseService<RentalProduct2TagDao, RentalProduct2Tag>(dao)

class RentalProductBinHistoryService @Inject constructor(dao: RentalProductBinHistoryDao) : DataBaseService<RentalProductBinHistoryDao, RentalProductBinHistory>(dao)

class RentalProductCustomFieldsService @Inject constructor(dao: RentalProductCustomFieldsDao) : DataBaseService<RentalProductCustomFieldsDao, RentalProductCustomFields>(dao)

class RentalProductStatusService @Inject constructor(dao: RentalProductStatusDao) : DataBaseService<RentalProductStatusDao, RentalProductStatus>(dao)

class RentalProductStatusHistoryService @Inject constructor(dao: RentalProductStatusHistoryDao) : DataBaseService<RentalProductStatusHistoryDao, RentalProductStatusHistory>(dao)

class Tag2CustomerRoleService @Inject constructor(dao: Tag2CustomerRoleDao) : DataBaseService<Tag2CustomerRoleDao, Tag2CustomerRole>(dao)

class TagService @Inject constructor(dao: TagDao) : DataBaseService<TagDao, Tag>(dao)

class TransactionCustomFieldsService @Inject constructor(dao: TransactionCustomFieldsDao) : DataBaseService<TransactionCustomFieldsDao, TransactionCustomFields>(dao)

class TransactionHistoryService @Inject constructor(dao: TransactionHistoryDao) : DataBaseService<TransactionHistoryDao, TransactionHistory>(dao)

class TransactionTypeService @Inject constructor(dao: TransactionTypeDao) : DataBaseService<TransactionTypeDao, TransactionType>(dao)

class TransferCarrierService @Inject constructor(dao: TransferCarrierDao) : DataBaseService<TransferCarrierDao, TransferCarrier>(dao)

class TransferService @Inject constructor(dao: TransferDao) : DataBaseService<TransferDao, Transfer>(dao)

class TransferItemService @Inject constructor(dao: TransferItemDao) : DataBaseService<TransferItemDao, TransferItem>(dao)

class TransferStatusService @Inject constructor(dao: TransferStatusDao) : DataBaseService<TransferStatusDao, TransferStatus>(dao)