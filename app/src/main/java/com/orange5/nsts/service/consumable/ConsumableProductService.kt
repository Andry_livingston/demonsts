package com.orange5.nsts.service.consumable

import com.orange5.nsts.data.db.dao.ConsumableProductDao
import com.orange5.nsts.data.db.dao.ConsumableProductDao.Properties.ProductId
import com.orange5.nsts.data.db.dao.ConsumableProductDao.Properties.ProductNumber
import com.orange5.nsts.data.db.entities.ConsumableProduct
import com.orange5.nsts.service.base.DataBaseService
import javax.inject.Inject

class ConsumableProductService @Inject constructor(dao: ConsumableProductDao)
    : DataBaseService<ConsumableProductDao, ConsumableProduct>(dao) {

    fun byProductNumber(number: String): ConsumableProduct? = dao.queryBuilder().where(ProductNumber.eq(number)).unique()

    fun byProductId(id: String): ConsumableProduct? = dao.queryBuilder().where(ProductId.eq(id)).unique()
}