package com.orange5.nsts.service.product;

import android.os.Handler;

import com.orange5.nsts.app.App;
import com.orange5.nsts.data.db.base.OrderProduct;
import com.orange5.nsts.data.db.dao.ConsumableBinDao;
import com.orange5.nsts.data.db.dao.ConsumableProductDao;
import com.orange5.nsts.data.db.dao.RentalProductDao;
import com.orange5.nsts.data.db.dao.RentalProductItemDao;
import com.orange5.nsts.data.db.entities.ConsumableBin;
import com.orange5.nsts.data.db.entities.ConsumableProduct;
import com.orange5.nsts.data.db.entities.RentalProduct;
import com.orange5.nsts.data.db.entities.RentalProductItem;
import com.orange5.nsts.service.product.async.DatabaseAsyncExecutor;

import org.greenrobot.greendao.query.LazyList;
import org.greenrobot.greendao.query.QueryBuilder;
import org.greenrobot.greendao.query.WhereCondition;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import timber.log.Timber;

import static com.orange5.nsts.service.base.QueriesUtils.getAllPermutations;
import static com.orange5.nsts.service.base.QueriesUtils.validQuery;

public class ProductService {

    private static ProductService instance = new ProductService();
    private Set<Closeable> closeables = new LinkedHashSet<>();

    public static ProductService get() {
        if (instance == null) {
            instance = new ProductService();
        }
        return instance;
    }

    private ProductService() {
    }

    private void closePreviousLazyLists() {
        closeables.removeIf(closeable -> {
            try {
                closeable.close();
            } catch (IOException e) {
                Timber.e(e);
                return false;
            }
            return true;
        });
    }

    public void searchProductsQuickOrder(SearchFilter filter,
                                         Consumer<List<OrderProduct>> resultConsumer) {
        closePreviousLazyLists();
        Handler uiThreadHandler = new Handler();
        DatabaseAsyncExecutor.execute(() -> {
            List<RentalProductItem> rentalProductsItem =
                    filter.getShouldShowRentals() ? loadRentalsItemLazy(filter) :
                            Collections.emptyList();
            List<ConsumableProduct> consumableProducts =
                    filter.getShouldShowConsumables() ? loadConsumablesLazy(filter) :
                            Collections.emptyList();
            List<OrderProduct> products = new ArrayList<>(rentalProductsItem);
            products.addAll(consumableProducts);

            uiThreadHandler.post(() -> resultConsumer.accept(products));
        });
    }

    public void searchProductsFullOrder(SearchFilter filter,
                                        Consumer<List<OrderProduct>> resultConsumer) {
        closePreviousLazyLists();
        Handler uiThreadHandler = new Handler();
        DatabaseAsyncExecutor.execute(() -> {
            List<RentalProduct> rentalProducts =
                    filter.getShouldShowRentals() ? loadRentalsLazy(filter) :
                            Collections.emptyList();
            List<ConsumableProduct> consumableProducts =
                    filter.getShouldShowConsumables() ? loadConsumablesLazy(filter) :
                            Collections.emptyList();
            List<OrderProduct> products = new ArrayList<>(rentalProducts);
            products.addAll(consumableProducts);
            uiThreadHandler.post(() -> resultConsumer.accept(products));
        });
    }

    public List<OrderProduct> loadAllConsumablesLazy() {
        QueryBuilder<ConsumableProduct> queryBuilder = App.getInstance().getDaoSession()
                .getConsumableProductDao().queryBuilder();
        queryBuilder.orderAsc(ConsumableProductDao.Properties.ProductNumber);

        LazyList<ConsumableProduct> consumableProducts = queryBuilder.listLazy();
        closeables.add(consumableProducts);
        return new ArrayList<>(consumableProducts);
    }

    public List<OrderProduct> loadAllRentalsLazy() {
        QueryBuilder<RentalProduct> queryBuilder = App.getInstance().getDaoSession()
                .getRentalProductDao().queryBuilder();
        queryBuilder.orderAsc(RentalProductDao.Properties.ProductNumber);
        queryBuilder.distinct();
        queryBuilder.join(RentalProductDao.Properties.ProductId,
                RentalProductItem.class, RentalProductItemDao.Properties.ProductId)
                .where(RentalProductItemDao.Properties.BinId.isNotNull());
        LazyList<RentalProduct> rentalProducts = queryBuilder.listLazy();
        closeables.add(rentalProducts);
        return new ArrayList<>(rentalProducts);
    }

    private List<ConsumableProduct> loadConsumablesLazy(SearchFilter filter) {
        QueryBuilder<ConsumableProduct> queryBuilder = App.getInstance().getDaoSession()
                .getConsumableProductDao().queryBuilder();
        queryBuilder.orderAsc(ConsumableProductDao.Properties.ProductNumber);

        if (!filter.getShouldShowZeroQuantity()) {
            queryBuilder.distinct();
            queryBuilder.join(ConsumableProductDao.Properties.ProductId,
                    ConsumableBin.class, ConsumableBinDao.Properties.ProductId)
                    .where(ConsumableBinDao.Properties.Quantity.gt(0));
        }
        if (filter.getQuery().length() > 0 ) {
            queryBuilder
                    .whereOr(getWhereConditionForColumn(filter,
                            ConsumableProductDao.Properties.ProductDescription.columnName),
                            getWhereConditionForColumn(filter,
                                    ConsumableProductDao.Properties.ProductNumber.columnName));
        }
        LazyList<ConsumableProduct> consumableProducts = queryBuilder.listLazy();
        closeables.add(consumableProducts);
        return consumableProducts;
    }

    private WhereCondition getWhereConditionForColumn(SearchFilter filter, String columnName) {
        String query = filter.getQuery();
        WhereCondition whereCondition = null;
        if (validQuery(query)) {
            StringBuilder conditionStringBuilder = new StringBuilder();
            conditionStringBuilder.append("(");
            Iterator<String> iterator = getAllPermutations(query).iterator();
            while (iterator.hasNext()) {
                String permutation = iterator.next();
                conditionStringBuilder.append(columnName)
                        .append(" like '")
                        .append(permutation)
                        .append("' ");
                conditionStringBuilder.append(iterator.hasNext() ? " or " : ")");
            }

            whereCondition = new WhereCondition.StringCondition(conditionStringBuilder.toString());
        }
        return whereCondition;
    }

    private List<RentalProduct> loadRentalsLazy(SearchFilter filter) {
        QueryBuilder<RentalProduct> queryBuilder = App.getInstance().getDaoSession()
                .getRentalProductDao().queryBuilder();
        queryBuilder.orderAsc(RentalProductDao.Properties.ProductNumber);
        queryBuilder.distinct();
        queryBuilder.join(RentalProductDao.Properties.ProductId,
                RentalProductItem.class, RentalProductItemDao.Properties.ProductId)
                .where(RentalProductItemDao.Properties.BinId.isNotNull());

        if (filter.getQuery().length() > 0) {
            queryBuilder.whereOr(getWhereConditionForColumn(filter,RentalProductDao.Properties.ProductDescription.columnName),
                    getWhereConditionForColumn(filter,RentalProductDao.Properties.ProductNumber.columnName));
        }
        LazyList<RentalProduct> rentalProducts = queryBuilder.listLazy();
        closeables.add(rentalProducts);
        return rentalProducts;
    }

    private List<RentalProductItem> loadRentalsItemLazy(SearchFilter filter) {
        QueryBuilder<RentalProductItem> queryBuilder = App.getInstance().getDaoSession()
                .getRentalProductItemDao().queryBuilder();

        queryBuilder
                .distinct()
                .orderAsc(RentalProductItemDao.Properties.ProductId)
                .join(RentalProductItemDao.Properties.ProductId,
                        RentalProduct.class, RentalProductDao.Properties.ProductId);

        if (filter.getQuery().length() > 0) {
            /*TODO: add search - .join(RentalProductItemDao.Properties.BinId, BinNumber.class, BinNumberDao.Properties.BinId)
                    .where(BinNumberDao.Properties.BinNum.notEq(RECEIVING));*/
            queryBuilder
                    .whereOr(getWhereConditionForColumn(filter,RentalProductDao.Properties.ProductDescription.columnName),
                    getWhereConditionForColumn(filter,RentalProductDao.Properties.ProductNumber.columnName),
                    queryBuilder.and(RentalProductItemDao.Properties.UniqueUnitNumber.like("%" + filter.getQuery() + "%"),
                            RentalProductItemDao.Properties.BinId.isNotNull()));
        }
        if (!filter.getShouldShowZeroQuantity()) {
            queryBuilder.where(RentalProductItemDao.Properties.Status.eq(RentalProductItem.STATUS_RENTAL_ITEM));
        }
        closeables.add(queryBuilder.listLazy());
        return queryBuilder.distinct().listLazy();
    }
}

