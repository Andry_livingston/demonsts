package com.orange5.nsts.service.user

import com.orange5.nsts.data.db.dao.RoleDao
import com.orange5.nsts.data.db.entities.Role
import com.orange5.nsts.service.base.DataBaseService
import javax.inject.Inject

class RoleService @Inject constructor(dao: RoleDao) : DataBaseService<RoleDao, Role>(dao) {

    fun getUserRoleByRoleId(roleId: String): String =
            if (roleId.isNotEmpty()) {
                dao.queryBuilder()
                        .where(RoleDao.Properties.Id.eq(roleId))
                        .unique()?.name ?: ROLE_USER
            } else ROLE_USER

    companion object {
        const val ROLE_ADMINISTRATOR = "Administrator"
        const val ROLE_USER = "User"
        const val ROLE_SUPERVISOR = "Supervisor"
    }
}