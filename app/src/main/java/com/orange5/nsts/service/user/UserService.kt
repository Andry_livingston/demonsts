package com.orange5.nsts.service.user

import com.orange5.nsts.data.db.dao.UserDao
import com.orange5.nsts.data.db.entities.User
import com.orange5.nsts.service.base.DataBaseService
import javax.inject.Inject

class UserService @Inject constructor(userDao: UserDao) : DataBaseService<UserDao, User>(userDao) {

    fun getUserByUserName(userName: String): User? = dao
            .queryRawCreate("WHERE UserName = \'$userName\' COLLATE NOCASE")
            .unique()

}
