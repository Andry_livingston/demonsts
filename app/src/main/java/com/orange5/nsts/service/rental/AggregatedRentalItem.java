package com.orange5.nsts.service.rental;

import com.orange5.nsts.app.App;
import com.orange5.nsts.data.db.base.OrderItem;
import com.orange5.nsts.data.db.dao.DaoSession;
import com.orange5.nsts.data.db.entities.BinNumber;
import com.orange5.nsts.data.db.entities.OrderRentalItem;
import com.orange5.nsts.data.db.entities.RentalProduct;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AggregatedRentalItem implements OrderItem {
    private final List<OrderRentalItem> rentalItems;
    private BinNumber scannedBin;


    public AggregatedRentalItem(OrderRentalItem firstItem) {
        rentalItems = new ArrayList<>();
        rentalItems.add(firstItem);
    }

    public void add(OrderRentalItem rentalItem) {
        if (!accepts(rentalItem)) {
            throw new IllegalArgumentException("Different product passed");
        }
        rentalItems.add(rentalItem);
    }

    public boolean accepts(OrderRentalItem rentalItem) {
        return rentalItem.getProductNumber().equals(getDefault().getProductNumber());
    }

    public boolean accepts(RentalProduct rentalProduct) {
        return rentalProduct.getProductNumber().equals(getDefault().getProductNumber());
    }

    public void remove(int count) {
        if (count < 0 || count > rentalItems.size()) {
            throw new IllegalStateException("Count must be within 0.." + rentalItems.size());
        }
        while (count-- > 0) {
            rentalItems.remove(0);
        }
    }

    @Override
    public String getProductNumber() {
        return getDefault().getProductNumber();
    }

    public OrderRentalItem getDefault() {
        return rentalItems.get(0);
    }

    @Override
    public Double getPrice() {
        return getDefault().getRentPrice();
    }

    @Override
    public String getProductId() {
        return getDefault().getProductId();
    }

    @Override
    public Integer getQuantity() {
        return rentalItems.size();
    }

    @Override
    public boolean isOversell() {
        return false;
    }

    @Override
    public Integer getPicked() {
        return Math.toIntExact(rentalItems.stream()
                .filter(orderRentalItem -> orderRentalItem.getUniqueUnitNumber() != null)
                .count());
    }

    @Nullable
    @Override
    public BinNumber getScannedBin() {
        return scannedBin;
    }

    @Override
    public void setScannedBin(BinNumber bin) {
        this.scannedBin = bin;
    }

    public OrderRentalItem getFirstUnpicked() {
        return rentalItems.stream()
                .filter(rentalItem -> rentalItem.getUniqueUnitNumber() == null)
                .findFirst()
                .orElse(null);
    }

    public List<String> getSelectedUniqueNumbers() {
        return rentalItems.stream().filter(rentalItem -> rentalItem.getUniqueUnitNumber() != null)
                .map(OrderRentalItem::getUniqueUnitNumber).collect(Collectors.toList());
    }

    public List<OrderRentalItem> getPickedItems() {
        return rentalItems.stream()
                .filter(orderRentalItem -> orderRentalItem.getUniqueUnitNumber() != null)
                .collect(Collectors.toList());
    }

    public List<OrderRentalItem> getUnpickedItems() {
        return rentalItems.stream()
                .filter(orderRentalItem -> orderRentalItem.getUniqueUnitNumber() == null)
                .collect(Collectors.toList());
    }

    public RentalProduct getRentalProduct() {
        getDefault().__setDaoSession(App.getInstance().getDaoSession());
        return getDefault().getRentalProduct();
    }


    public boolean allPicked() {
        return getPicked().equals(getQuantity());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof AggregatedRentalItem) {
            AggregatedRentalItem item = cast(obj);
            return item.getProductId().equals(getProductId());
        }
        return false;
    }

    @Override
    public Object getPrimaryKey() {
        return null;
    }

    @Override
    public void __setDaoSession(DaoSession daoSession) {
        rentalItems.forEach(item -> item.__setDaoSession(daoSession));
    }

    private static <T> T cast(Object object) {
        return (T) object;
    }
}
