select B.BinNum as BinBarcode, C.Quantity as BinQuantity, P.ProductDescription, P.ProductNumber as ProductBarcode
from ConsumableBin C
join BinNumber B on C.BinId = B.BinId
join ConsumableProduct P on P.ProductId = C.ProductId 
where BinQuantity > 0
order by binBarcode