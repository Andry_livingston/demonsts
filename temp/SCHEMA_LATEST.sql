BEGIN TRANSACTION;
DROP TABLE IF EXISTS `User2Role`;
CREATE TABLE IF NOT EXISTS `User2Role` (
	`UserId`	text NOT NULL,
	`RoleId`	text NOT NULL,
	FOREIGN KEY(`RoleId`) REFERENCES `Role`(`Id`),
	FOREIGN KEY(`UserId`) REFERENCES `User`(`UserId`),
	PRIMARY KEY(`UserId`,`RoleId`)
);
DROP TABLE IF EXISTS `User`;
CREATE TABLE IF NOT EXISTS `User` (
	`UserId`	text NOT NULL,
	`UserName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	`Password`	text ( 50 ) COLLATE NOCASE,
	`FirstName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	`LastName`	text ( 50 ) COLLATE NOCASE,
	`Email`	text ( 250 ) NOT NULL COLLATE NOCASE,
	`Phone`	text ( 50 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`UserId`)
);
DROP TABLE IF EXISTS `TransferStatus`;
CREATE TABLE IF NOT EXISTS `TransferStatus` (
	`StatusId`	integer NOT NULL,
	`StatusName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`StatusId`)
);
DROP TABLE IF EXISTS `TransferItem`;
CREATE TABLE IF NOT EXISTS `TransferItem` (
	`TransferItemId`	text NOT NULL,
	`TransferId`	text,
	`ConsumableProductId`	text,
	`ConsumableQuantity`	integer,
	`RentalProductId`	text,
	`RentalUniqueUnitNumber`	text ( 35 ) COLLATE NOCASE,
	`FromBinId`	text,
	FOREIGN KEY(`ConsumableProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
	FOREIGN KEY(`FromBinId`) REFERENCES `BinNumber`(`BinId`),
	FOREIGN KEY(`RentalProductId`) REFERENCES `RentalProduct`(`ProductId`),
	FOREIGN KEY(`TransferId`) REFERENCES `Transfer`(`TransferId`),
	PRIMARY KEY(`TransferItemId`)
);
DROP TABLE IF EXISTS `TransferCarrier`;
CREATE TABLE IF NOT EXISTS `TransferCarrier` (
	`CarrierId`	integer NOT NULL,
	`CarrierName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`CarrierId`)
);
DROP TABLE IF EXISTS `Transfer`;
CREATE TABLE IF NOT EXISTS `Transfer` (
	`TransferId`	text NOT NULL,
	`TransferNumber`	text ( 50 ) COLLATE NOCASE,
	`StatusId`	integer,
	`OpennedDate`	datetime,
	`ShippedDate`	datetime,
	`CarrierId`	integer,
	`TrackingNumber`	text ( 50 ) COLLATE NOCASE,
	`CreatedByUserId`	text,
	FOREIGN KEY(`CarrierId`) REFERENCES `TransferCarrier`(`CarrierId`),
	FOREIGN KEY(`CreatedByUserId`) REFERENCES `User`(`UserId`),
	FOREIGN KEY(`StatusId`) REFERENCES `TransferStatus`(`StatusId`),
	PRIMARY KEY(`TransferId`)
);
DROP TABLE IF EXISTS `TransactionType`;
CREATE TABLE IF NOT EXISTS `TransactionType` (
	`TransactionTypeId`	integer NOT NULL,
	`TransactionTypeName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`TransactionTypeId`)
);
DROP TABLE IF EXISTS `TransactionHistory`;
CREATE TABLE IF NOT EXISTS `TransactionHistory` (
	`TransactionId`	text NOT NULL,
	`Device`	char ( 10 ) COLLATE NOCASE,
	`CustomerId`	text ( 35 ) COLLATE NOCASE,
	`TransactionDate`	datetime,
	`Details`	text ( 2147483647 ) COLLATE NOCASE,
	`TransactionType`	integer,
	`ConsumableProductId`	text,
	`Quantity`	integer,
	`RentalProductId`	text,
	`UniqueUnitNumber`	text ( 35 ) COLLATE NOCASE,
	`SignImage`	blob ( 2147483647 ),
	`ReceiptText`	text ( 2147483647 ) COLLATE NOCASE,
	`OrderId`	text,
	FOREIGN KEY(`TransactionType`) REFERENCES `TransactionType`(`TransactionTypeId`),
	FOREIGN KEY(`RentalProductId`) REFERENCES `RentalProduct`(`ProductId`),
	PRIMARY KEY(`TransactionId`),
	FOREIGN KEY(`CustomerId`) REFERENCES `Customer`(`CustomerId`),
	FOREIGN KEY(`ConsumableProductId`) REFERENCES `ConsumableProduct`(`ProductId`)
);
DROP TABLE IF EXISTS `TransactionCustomFields`;
CREATE TABLE IF NOT EXISTS `TransactionCustomFields` (
	`CustomFieldId`	text NOT NULL,
	`TransactionId`	text NOT NULL,
	`FieldName`	text ( 50 ) COLLATE NOCASE,
	`FieldValue`	text ( 2147483647 ) COLLATE NOCASE,
	FOREIGN KEY(`TransactionId`) REFERENCES `TransactionHistory`(`TransactionId`),
	PRIMARY KEY(`CustomFieldId`)
);
DROP TABLE IF EXISTS `Tag2CustomerRole`;
CREATE TABLE IF NOT EXISTS `Tag2CustomerRole` (
	`TagId`	text NOT NULL,
	`CustomerRoleId`	text NOT NULL,
	FOREIGN KEY(`CustomerRoleId`) REFERENCES `CustomerRole`(`CustomerRoleId`),
	PRIMARY KEY(`TagId`,`CustomerRoleId`),
	FOREIGN KEY(`TagId`) REFERENCES `Tag`(`TagId`)
);
DROP TABLE IF EXISTS `Tag`;
CREATE TABLE IF NOT EXISTS `Tag` (
	`TagId`	text NOT NULL,
	`TagName`	text ( 50 ) COLLATE NOCASE,
	PRIMARY KEY(`TagId`)
);
DROP TABLE IF EXISTS `SyncTransferObject`;
CREATE TABLE IF NOT EXISTS `SyncTransferObject` (
	`TransferObjectId`	text NOT NULL,
	`DateTimeSaved`	datetime,
	`TypeData`	text ( 500 ) COLLATE NOCASE,
	`Action`	char ( 1 ) COLLATE NOCASE,
	`IsMine`	bit,
	`Executed`	datetime,
	`DataObject`	blob ( 2147483647 ),
	`QueryType`	text ( 10 ) COLLATE NOCASE,
	`QueryTable`	text ( 100 ) COLLATE NOCASE,
	`QueryKey`	text ( 100 ) COLLATE NOCASE,
	`QueryFields`	text ( 1000 ) COLLATE NOCASE,
	`IncrementNumber`	integer NOT NULL PRIMARY KEY AUTOINCREMENT
);
DROP TABLE IF EXISTS `ShippingNotice`;
CREATE TABLE IF NOT EXISTS `ShippingNotice` (
	`ShipingNoticeId`	text NOT NULL,
	`TransferNumber`	text ( 35 ) COLLATE NOCASE,
	`ConsumableProductNumber`	text ( 35 ) COLLATE NOCASE,
	`ConsumableQuantity`	integer,
	`ConsumableQuantityStart`	integer,
	`RentalProductNumber`	text ( 35 ) COLLATE NOCASE,
	`RentalUniqueUnitNumber`	text ( 35 ) COLLATE NOCASE,
	`ShippingDate`	datetime NOT NULL,
	`isProcessed`	bit,
	`CreatedByUserId`	text,
	`UpdatedByUserId`	text,
	FOREIGN KEY(`CreatedByUserId`) REFERENCES `User`(`UserId`),
	PRIMARY KEY(`ShipingNoticeId`),
	FOREIGN KEY(`ShipingNoticeId`) REFERENCES `ShippingNotice`(`ShipingNoticeId`),
	FOREIGN KEY(`UpdatedByUserId`) REFERENCES `User`(`UserId`)
);
DROP TABLE IF EXISTS `Role`;
CREATE TABLE IF NOT EXISTS `Role` (
	`Id`	text NOT NULL,
	`Name`	text ( 50 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`Id`)
);
DROP TABLE IF EXISTS `RentalReturn`;
CREATE TABLE IF NOT EXISTS `RentalReturn` (
	`RentalReturnId`	text NOT NULL,
	`ReturnNumber`	text ( 50 ) COLLATE NOCASE,
	`StartDate`	datetime,
	`CompleteDate`	datetime,
	`SignImage`	blob ( 2147483647 ),
	PRIMARY KEY(`RentalReturnId`)
);
DROP TABLE IF EXISTS `RentalProductStatusHistory`;
CREATE TABLE IF NOT EXISTS `RentalProductStatusHistory` (
	`HistoryId`	text NOT NULL,
	`UniqueUnitNumber`	text ( 35 ) COLLATE NOCASE,
	`StatusId`	smallint,
	`AddDate`	datetime,
	`Comment`	text ( 2147483647 ) COLLATE NOCASE,
	FOREIGN KEY(`StatusId`) REFERENCES `RentalProductStatus`(`StatusId`),
	PRIMARY KEY(`HistoryId`)
);
DROP TABLE IF EXISTS `RentalProductStatus`;
CREATE TABLE IF NOT EXISTS `RentalProductStatus` (
	`StatusId`	smallint NOT NULL,
	`StatusName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`StatusId`)
);
DROP TABLE IF EXISTS `RentalProductItem`;
CREATE TABLE IF NOT EXISTS `RentalProductItem` (
	`UniqueUnitNumber`	text ( 35 ) NOT NULL COLLATE NOCASE,
	`ProductId`	text,
	`Status`	smallint,
	`Allocated`	bit,
	`BinId`	text,
	`CertDate`	datetime,
	`CertDuration`	integer,
	`СontainerID`	text ( 50 ) COLLATE NOCASE,
	`Remarks`	text ( 2147483647 ) COLLATE NOCASE,
	FOREIGN KEY(`BinId`) REFERENCES `BinNumber`(`BinId`),
	FOREIGN KEY(`ProductId`) REFERENCES `RentalProduct`(`ProductId`),
	PRIMARY KEY(`UniqueUnitNumber`),
	FOREIGN KEY(`Status`) REFERENCES `RentalProductStatus`(`StatusId`)
);
DROP TABLE IF EXISTS `RentalProductCustomFields`;
CREATE TABLE IF NOT EXISTS `RentalProductCustomFields` (
	`CustomFieldId`	text NOT NULL,
	`ProductId`	text NOT NULL,
	`FieldName`	text ( 50 ) COLLATE NOCASE,
	`FieldValue`	text ( 2147483647 ) COLLATE NOCASE,
	PRIMARY KEY(`CustomFieldId`),
	FOREIGN KEY(`ProductId`) REFERENCES `RentalProduct`(`ProductId`)
);
DROP TABLE IF EXISTS `RentalProductBinHistory`;
CREATE TABLE IF NOT EXISTS `RentalProductBinHistory` (
	`HistoryId`	text NOT NULL,
	`UniqueUnitNumber`	text ( 35 ) COLLATE NOCASE,
	`BinId`	text,
	`AddDate`	datetime,
	FOREIGN KEY(`BinId`) REFERENCES `BinNumber`(`BinId`),
	FOREIGN KEY(`UniqueUnitNumber`) REFERENCES `RentalProductItem`(`UniqueUnitNumber`),
	PRIMARY KEY(`HistoryId`)
);
DROP TABLE IF EXISTS `RentalProduct2Tag`;
CREATE TABLE IF NOT EXISTS `RentalProduct2Tag` (
	`ProductID`	text NOT NULL,
	`TagId`	text NOT NULL,
	FOREIGN KEY(`ProductID`) REFERENCES `RentalProduct`(`ProductId`),
	FOREIGN KEY(`TagId`) REFERENCES `Tag`(`TagId`),
	PRIMARY KEY(`ProductID`,`TagId`)
);
DROP TABLE IF EXISTS `RentalProduct`;
CREATE TABLE IF NOT EXISTS `RentalProduct` (
	`ProductId`	text NOT NULL,
	`ProductNumber`	text ( 35 ) NOT NULL COLLATE NOCASE,
	`ProductDescription`	text ( 250 ) COLLATE NOCASE,
	`Manufacturer`	text ( 250 ) COLLATE NOCASE,
	`Make`	text ( 250 ) COLLATE NOCASE,
	`Model`	text ( 250 ) COLLATE NOCASE,
	`ReplacementCost`	float,
	`Picture`	text ( 255 ) COLLATE NOCASE,
	`AuthLevel`	integer NOT NULL DEFAULT 0,
	`ManufacturerNumber`	text ( 35 ) COLLATE NOCASE,
	`RentalPeriod`	integer,
	`isCustomerOwned`	bit,
	`WasChangedDate`	datetime,
	`CategoryId`	text,
	`Info`	text ( 50 ) COLLATE NOCASE,
	`DefaultBinId`	text,
	PRIMARY KEY(`ProductId`),
	FOREIGN KEY(`DefaultBinId`) REFERENCES `BinNumber`(`BinId`)
);
DROP TABLE IF EXISTS `ProductReceivingLog`;
CREATE TABLE IF NOT EXISTS `ProductReceivingLog` (
	`HistoryId`	text NOT NULL,
	`ConsumableProductId`	text,
	`RentalUUN`	text ( 35 ) COLLATE NOCASE,
	`Quantity`	integer,
	`ToBinId`	text,
	`AddDate`	datetime,
	FOREIGN KEY(`ToBinId`) REFERENCES `BinNumber`(`BinId`),
	PRIMARY KEY(`HistoryId`),
	FOREIGN KEY(`ConsumableProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
	FOREIGN KEY(`RentalUUN`) REFERENCES `RentalProductItem`(`UniqueUnitNumber`)
);
DROP TABLE IF EXISTS `PickTicket`;
CREATE TABLE IF NOT EXISTS `PickTicket` (
	`PickTicketId`	text NOT NULL,
	`OrderId`	text,
	`ProductId`	text,
	`IsRental`	bit,
	`BinId`	text,
	`Quantity`	integer,
	`Picked`	integer,
	FOREIGN KEY(`OrderId`) REFERENCES `Orders`(`OrderId`),
	PRIMARY KEY(`PickTicketId`)
);
DROP TABLE IF EXISTS `Parameters`;
CREATE TABLE IF NOT EXISTS `Parameters` (
	`ParameterName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	`ParameterValue`	text ( 50 ) COLLATE NOCASE,
	`ParameterTitle`	text ( 50 ) COLLATE NOCASE,
	`OrderNum`	integer,
	PRIMARY KEY(`ParameterName`)
);
DROP TABLE IF EXISTS `OrderStatus`;
CREATE TABLE IF NOT EXISTS `OrderStatus` (
	`OrderStatusId`	integer NOT NULL,
	`OrderStatusName`	text ( 50 ) COLLATE NOCASE,
	PRIMARY KEY(`OrderStatusId`)
);
DROP TABLE IF EXISTS `OrderRentalItem`;
CREATE TABLE IF NOT EXISTS `OrderRentalItem` (
	`OrderItemId`	text NOT NULL,
	`OrderId`	text,
	`UniqueUnitNumber`	text ( 35 ) COLLATE NOCASE,
	`ProductNumber`	text ( 35 ) COLLATE NOCASE,
	`StartDate`	datetime,
	`EndDate`	datetime,
	`RentPrice`	float,
	`ReturnDate`	datetime,
	`ReturnCustomerId`	text ( 35 ) COLLATE NOCASE,
	`Comments`	text ( 2147483647 ) COLLATE NOCASE,
	`DueDate`	datetime,
	`RentalPeriod`	integer,
	`ProductId`	text,
	`RentalReturnNotes`	text ( 2147483647 ) COLLATE NOCASE,
	`WasDamaged`	bit,
	`RentalReturnId`	text,
	FOREIGN KEY(`RentalReturnId`) REFERENCES `RentalReturn`(`RentalReturnId`),
	PRIMARY KEY(`OrderItemId`),
	FOREIGN KEY(`OrderId`) REFERENCES `Orders`(`OrderId`),
	FOREIGN KEY(`ProductId`) REFERENCES `RentalProduct`(`ProductId`)
);
DROP TABLE IF EXISTS `OrderConsumableReturnItemByCustomer`;
CREATE TABLE IF NOT EXISTS `OrderConsumableReturnItemByCustomer` (
	`OrderConsumableReturnItemByCustomerId`	text NOT NULL,
	`OrderItemId`	text NOT NULL,
	`Quantity`	integer NOT NULL,
	`OrderConsumableReturnId`	text,
	FOREIGN KEY(`OrderItemId`) REFERENCES `OrderConsumableItem`(`OrderItemId`),
	FOREIGN KEY(`OrderConsumableReturnId`) REFERENCES `OrderConsumableReturn`(`OrderConsumableReturnId`),
	PRIMARY KEY(`OrderConsumableReturnItemByCustomerId`)
);
DROP TABLE IF EXISTS `OrderConsumableReturn`;
CREATE TABLE IF NOT EXISTS `OrderConsumableReturn` (
	`OrderConsumableReturnId`	text NOT NULL,
	`ReturnedDate`	datetime,
	`SignImage`	blob ( 2147483647 ),
	`ReturnedCustomerId`	text ( 35 ) COLLATE NOCASE,
	`OrderId`	text,
	PRIMARY KEY(`OrderConsumableReturnId`),
	FOREIGN KEY(`ReturnedCustomerId`) REFERENCES `Customer`(`CustomerId`),
	FOREIGN KEY(`OrderId`) REFERENCES `Orders`(`OrderId`)
);
DROP TABLE IF EXISTS `OrderConsumableItem`;
CREATE TABLE IF NOT EXISTS `OrderConsumableItem` (
	`OrderItemId`	text NOT NULL,
	`OrderId`	text,
	`ProductId`	text,
	`Quantity`	integer,
	`Picked`	integer,
	`Price`	float,
	FOREIGN KEY(`ProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
	PRIMARY KEY(`OrderItemId`),
	FOREIGN KEY(`OrderId`) REFERENCES `Orders`(`OrderId`)
);
DROP TABLE IF EXISTS `Orders`;
CREATE TABLE IF NOT EXISTS `Orders` (
	`OrderId`	text NOT NULL,
	`CreatorCustomerId`	text ( 35 ) COLLATE NOCASE,
	`OrderDate`	datetime,
	`OrderCompleteDate`	datetime,
	`Details`	text ( 2147483647 ) COLLATE NOCASE,
	`SignImage`	blob ( 2147483647 ),
	`ReceiptText`	text ( 2147483647 ) COLLATE NOCASE,
	`OrderStatusId`	integer,
	`WhoPickedOrderCustomerId`	text ( 35 ) COLLATE NOCASE,
	`LimitOverwriteComment`	text ( 2147483647 ) COLLATE NOCASE,
	`OrderSum`	float,
	`OrderNumber`	text ( 50 ) COLLATE NOCASE,
	`ApprovedByCustomerId`	text ( 35 ) COLLATE NOCASE,
	`CreatedByUserId`	text,
	`WasChangedDate`	datetime,
	`AccountingCode`	text ( 50 ) COLLATE NOCASE,
	`WebOrderNumber`	text ( 50 ) COLLATE NOCASE,
	PRIMARY KEY(`OrderId`),
	FOREIGN KEY(`ApprovedByCustomerId`) REFERENCES `Customer`(`CustomerId`),
	FOREIGN KEY(`OrderStatusId`) REFERENCES `OrderStatus`(`OrderStatusId`),
	FOREIGN KEY(`CreatedByUserId`) REFERENCES `User`(`UserId`)
);
DROP TABLE IF EXISTS `InventoryAdjustmentReasonCode`;
CREATE TABLE IF NOT EXISTS `InventoryAdjustmentReasonCode` (
	`ReasonCodeId`	text NOT NULL,
	`ReasonCodeValue`	text ( 50 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`ReasonCodeId`)
);
DROP TABLE IF EXISTS `InventoryAdjustment`;
CREATE TABLE IF NOT EXISTS `InventoryAdjustment` (
	`InventoryAdjustmentId`	text NOT NULL,
	`ConsumableProductId`	text,
	`OldQnty`	integer NOT NULL,
	`NewQnty`	integer NOT NULL,
	`ReasonCodeId`	text,
	`ChangedByUserId`	text,
	`Notes`	text ( 2147483647 ) COLLATE NOCASE,
	`AddDate`	datetime NOT NULL DEFAULT (CURRENT_TIMESTAMP),
	FOREIGN KEY(`ChangedByUserId`) REFERENCES `User`(`UserId`),
	FOREIGN KEY(`ConsumableProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
	FOREIGN KEY(`ReasonCodeId`) REFERENCES `InventoryAdjustmentReasonCode`(`ReasonCodeId`),
	PRIMARY KEY(`InventoryAdjustmentId`)
);
DROP TABLE IF EXISTS `Error`;
CREATE TABLE IF NOT EXISTS `Error` (
	`ErrorId`	text NOT NULL,
	`ErrorType`	text ( 50 ) COLLATE NOCASE,
	`ErrorMessage`	text NOT NULL COLLATE NOCASE,
	`ErrorDate`	datetime NOT NULL DEFAULT (CURRENT_TIMESTAMP),
	PRIMARY KEY(`ErrorId`)
);
DROP TABLE IF EXISTS `DisplayColumnName`;
CREATE TABLE IF NOT EXISTS `DisplayColumnName` (
	`ColumnName`	text ( 250 ) NOT NULL COLLATE NOCASE,
	`DisplayName`	text ( 250 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`ColumnName`)
);
DROP TABLE IF EXISTS `DiscrepanciesType`;
CREATE TABLE IF NOT EXISTS `DiscrepanciesType` (
	`TypeId`	integer NOT NULL,
	`TypeName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	`IsNegative`	bit NOT NULL,
	`ResolutionType`	text ( 200 ) COLLATE NOCASE,
	PRIMARY KEY(`TypeId`)
);
DROP TABLE IF EXISTS `DiscrepanciesResolvedStatus`;
CREATE TABLE IF NOT EXISTS `DiscrepanciesResolvedStatus` (
	`ResolvedStatusId`	integer NOT NULL,
	`ResolvedStatusName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	`SelectBin`	text ( 50 ) NOT NULL COLLATE NOCASE,
	`HideBinSelector`	bit NOT NULL,
	PRIMARY KEY(`ResolvedStatusId`)
);
DROP TABLE IF EXISTS `Discrepancies`;
CREATE TABLE IF NOT EXISTS `Discrepancies` (
	`DiscrepanciesID`	text NOT NULL,
	`DescripancyDescription`	text ( 500 ) COLLATE NOCASE,
	`DiscrepancyTypeId`	integer,
	`AddingDate`	datetime NOT NULL,
	`ConsumableProductId`	text,
	`ConsumableQuantity`	integer,
	`RentalProductId`	text,
	`RentalUniqueUnitNumber`	text ( 35 ) COLLATE NOCASE,
	`Comment`	text ( 2147483647 ) COLLATE NOCASE,
	`IsResolved`	bit,
	`ResolvedStatusId`	integer,
	`ResolvedComment`	text ( 2147483647 ) COLLATE NOCASE,
	`ResolvedDate`	datetime,
	`UPDescription`	text ( 500 ) COLLATE NOCASE,
	`CreatedByUserId`	text,
	`UpdatedByUserId`	text,
	FOREIGN KEY(`ConsumableProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
	FOREIGN KEY(`UpdatedByUserId`) REFERENCES `User`(`UserId`),
	FOREIGN KEY(`DiscrepancyTypeId`) REFERENCES `DiscrepanciesType`(`TypeId`),
	FOREIGN KEY(`ResolvedStatusId`) REFERENCES `DiscrepanciesResolvedStatus`(`ResolvedStatusId`),
	PRIMARY KEY(`DiscrepanciesID`),
	FOREIGN KEY(`CreatedByUserId`) REFERENCES `User`(`UserId`),
	FOREIGN KEY(`RentalProductId`) REFERENCES `RentalProduct`(`ProductId`)
);
DROP TABLE IF EXISTS `CustomerStatus`;
CREATE TABLE IF NOT EXISTS `CustomerStatus` (
	`StatusId`	smallint NOT NULL,
	`StatusName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`StatusId`)
);
DROP TABLE IF EXISTS `CustomerRole`;
CREATE TABLE IF NOT EXISTS `CustomerRole` (
	`CustomerRoleId`	text NOT NULL,
	`RoleName`	text ( 50 ) COLLATE NOCASE,
	PRIMARY KEY(`CustomerRoleId`)
);
DROP TABLE IF EXISTS `CustomerProductNumberXref`;
CREATE TABLE IF NOT EXISTS `CustomerProductNumberXref` (
	`CustomerProductNumber`	text ( 35 ) NOT NULL COLLATE NOCASE,
	`ProductId`	text NOT NULL,
	FOREIGN KEY(`ProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
	PRIMARY KEY(`CustomerProductNumber`)
);
DROP TABLE IF EXISTS `CustomerPosition`;
CREATE TABLE IF NOT EXISTS `CustomerPosition` (
	`CustomerPositionId`	text NOT NULL,
	`PositionName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	`PositionLevel`	smallint,
	PRIMARY KEY(`CustomerPositionId`)
);
DROP TABLE IF EXISTS `CustomerCustomFieldsTypes`;
CREATE TABLE IF NOT EXISTS `CustomerCustomFieldsTypes` (
	`FieldTypeId`	text NOT NULL,
	`FieldName`	text ( 50 ) COLLATE NOCASE,
	`FOrder`	integer,
	PRIMARY KEY(`FieldTypeId`)
);
DROP TABLE IF EXISTS `CustomerCustomFields`;
CREATE TABLE IF NOT EXISTS `CustomerCustomFields` (
	`CustomFieldId`	text NOT NULL,
	`CustomerId`	text ( 35 ) NOT NULL COLLATE NOCASE,
	`CustomFieldTypeId`	text,
	`FieldValue`	text ( 2147483647 ) COLLATE NOCASE,
	FOREIGN KEY(`CustomerId`) REFERENCES `Customer`(`CustomerId`),
	FOREIGN KEY(`CustomFieldTypeId`) REFERENCES `CustomerCustomFieldsTypes`(`FieldTypeId`),
	PRIMARY KEY(`CustomFieldId`)
);
DROP TABLE IF EXISTS `Customer2Role`;
CREATE TABLE IF NOT EXISTS `Customer2Role` (
	`CustomerId`	text ( 35 ) NOT NULL COLLATE NOCASE,
	`CustomerRoleId`	text NOT NULL,
	FOREIGN KEY(`CustomerRoleId`) REFERENCES `CustomerRole`(`CustomerRoleId`),
	FOREIGN KEY(`CustomerId`) REFERENCES `Customer`(`CustomerId`),
	PRIMARY KEY(`CustomerId`,`CustomerRoleId`)
);
DROP TABLE IF EXISTS `Customer`;
CREATE TABLE IF NOT EXISTS `Customer` (
	`CustomerId`	text ( 35 ) NOT NULL COLLATE NOCASE,
	`BadgeNumber`	text ( 35 ) COLLATE NOCASE,
	`FName`	text ( 50 ) COLLATE NOCASE,
	`LName`	text ( 50 ) COLLATE NOCASE,
	`Title`	text ( 50 ) COLLATE NOCASE,
	`SecurityLevel`	smallint,
	`BuyingLevel`	smallint,
	`RentalLevel`	smallint,
	`Status`	smallint,
	`ApproveCode`	text ( 50 ) COLLATE NOCASE,
	`Email`	text ( 255 ) COLLATE NOCASE,
	`Phone`	text ( 50 ) COLLATE NOCASE,
	`MaxProductPrice`	float,
	`MaxOrderPrice`	float,
	`MaxReplacementCost`	float,
	`CanNotBuy`	bit DEFAULT 0,
	`CanNotRent`	bit DEFAULT 0,
	`AuthLevel`	integer NOT NULL DEFAULT 0,
	`CustomerPositionId`	text,
	`ReportsToCustomerId`	text ( 35 ) COLLATE NOCASE,
	`WasChangedDate`	datetime,
	FOREIGN KEY(`ReportsToCustomerId`) REFERENCES `Customer`(`CustomerId`),
	FOREIGN KEY(`Status`) REFERENCES `CustomerStatus`(`StatusId`),
	PRIMARY KEY(`CustomerId`),
	FOREIGN KEY(`CustomerPositionId`) REFERENCES `CustomerPosition`(`CustomerPositionId`)
);
DROP TABLE IF EXISTS `ConsumableProductCustomFields`;
CREATE TABLE IF NOT EXISTS `ConsumableProductCustomFields` (
	`CustomFieldId`	text NOT NULL,
	`ProductId`	text NOT NULL,
	`FieldName`	text ( 50 ) COLLATE NOCASE,
	`FieldVaule`	text ( 2147483647 ) COLLATE NOCASE,
	PRIMARY KEY(`CustomFieldId`),
	FOREIGN KEY(`ProductId`) REFERENCES `ConsumableProduct`(`ProductId`)
);
DROP TABLE IF EXISTS `ConsumableProductBinHistory`;
CREATE TABLE IF NOT EXISTS `ConsumableProductBinHistory` (
	`HistoryId`	text NOT NULL,
	`ProductId`	text,
	`Quantity`	integer,
	`ToBinId`	text,
	`AddDate`	datetime,
	FOREIGN KEY(`ToBinId`) REFERENCES `BinNumber`(`BinId`),
	PRIMARY KEY(`HistoryId`),
	FOREIGN KEY(`ProductId`) REFERENCES `ConsumableProduct`(`ProductId`)
);
DROP TABLE IF EXISTS `ConsumableProduct2Tag`;
CREATE TABLE IF NOT EXISTS `ConsumableProduct2Tag` (
	`ProductID`	text NOT NULL,
	`TagId`	text NOT NULL,
	FOREIGN KEY(`TagId`) REFERENCES `Tag`(`TagId`),
	PRIMARY KEY(`ProductID`,`TagId`),
	FOREIGN KEY(`ProductID`) REFERENCES `ConsumableProduct`(`ProductId`)
);
DROP TABLE IF EXISTS `ConsumableProduct`;
CREATE TABLE IF NOT EXISTS `ConsumableProduct` (
	`ProductId`	text NOT NULL,
	`ProductNumber`	text ( 35 ) NOT NULL COLLATE NOCASE,
	`ProductDescription`	text ( 250 ) NOT NULL COLLATE NOCASE,
	`ManufacturerNumber`	text ( 35 ) COLLATE NOCASE,
	`MaterialCategory`	text ( 50 ) COLLATE NOCASE,
	`Make`	text ( 50 ) COLLATE NOCASE,
	`Model`	text ( 50 ) COLLATE NOCASE,
	`UnitOfMeasure`	text ( 10 ) COLLATE NOCASE,
	`Price`	float,
	`Picture`	text ( 255 ) COLLATE NOCASE,
	`AuthLevel`	integer NOT NULL DEFAULT 0,
	`isCustomerOwned`	bit,
	`WasChangedDate`	datetime,
	`DefaultBinId`	text,
	`CategoryId`	text,
	`Info`	text ( 50 ) COLLATE NOCASE,
	FOREIGN KEY(`DefaultBinId`) REFERENCES `BinNumber`(`BinId`),
	PRIMARY KEY(`ProductId`),
	FOREIGN KEY(`CategoryId`) REFERENCES `Category`(`CategoryID`)
);
DROP TABLE IF EXISTS `ConsumableBin`;
CREATE TABLE IF NOT EXISTS `ConsumableBin` (
	`ConsumableBinId`	text NOT NULL,
	`ProductId`	text NOT NULL,
	`Quantity`	integer NOT NULL,
	`AddingDate`	datetime,
	`Allocated`	integer,
	`BinId`	text,
	FOREIGN KEY(`ProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
	FOREIGN KEY(`BinId`) REFERENCES `BinNumber`(`BinId`),
	PRIMARY KEY(`ConsumableBinId`)
);
DROP TABLE IF EXISTS `ConsumableAlternateProductNumberXref`;
CREATE TABLE IF NOT EXISTS `ConsumableAlternateProductNumberXref` (
	`AlternateNumber`	text ( 35 ) NOT NULL COLLATE NOCASE,
	`ProductId`	text NOT NULL,
	PRIMARY KEY(`AlternateNumber`),
	FOREIGN KEY(`ProductId`) REFERENCES `ConsumableProduct`(`ProductId`)
);
DROP TABLE IF EXISTS `Category`;
CREATE TABLE IF NOT EXISTS `Category` (
	`CategoryID`	text NOT NULL,
	`CategoryName`	text ( 250 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`CategoryID`)
);
DROP TABLE IF EXISTS `BinType`;
CREATE TABLE IF NOT EXISTS `BinType` (
	`BinTypeId`	smallint NOT NULL,
	`BinTypeName`	text ( 20 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`BinTypeId`)
);
DROP TABLE IF EXISTS `BinNumber`;
CREATE TABLE IF NOT EXISTS `BinNumber` (
	`BinId`	text NOT NULL,
	`BinNum`	text ( 35 ) COLLATE NOCASE,
	`BinTypeId`	smallint,
	`OrdNumber`	integer,
	PRIMARY KEY(`BinId`),
	FOREIGN KEY(`BinTypeId`) REFERENCES `BinType`(`BinTypeId`)
);
DROP TABLE IF EXISTS `AppState`;
CREATE TABLE IF NOT EXISTS `AppState` (
	`AppName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	`LastSyncDate`	datetime,
	PRIMARY KEY(`AppName`)
);
DROP TABLE IF EXISTS `AccountingCode`;
CREATE TABLE IF NOT EXISTS `AccountingCode` (
	`AccountingCodeId`	text NOT NULL,
	`AccountingCodeName`	text ( 50 ) COLLATE NOCASE,
	PRIMARY KEY(`AccountingCodeId`)
);
DROP TABLE IF EXISTS `AccessoryCrossReference`;
CREATE TABLE IF NOT EXISTS `AccessoryCrossReference` (
	`AccessoryId`	text NOT NULL,
	`ProductId1`	text NOT NULL,
	`ProductId2`	text NOT NULL,
	`TypeReference`	integer,
	PRIMARY KEY(`AccessoryId`)
);
DROP INDEX IF EXISTS `Customer_IX_Customer_ApproveCode`;
CREATE UNIQUE INDEX IF NOT EXISTS `Customer_IX_Customer_ApproveCode` ON `Customer` (
	`ApproveCode`	DESC
);
COMMIT;
