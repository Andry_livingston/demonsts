BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `User2Role` (
	`UserId`	text NOT NULL,
	`RoleId`	text NOT NULL,
	FOREIGN KEY(`RoleId`) REFERENCES `Role`(`Id`),
	PRIMARY KEY(`UserId`,`RoleId`),
	FOREIGN KEY(`UserId`) REFERENCES `User`(`UserId`)
);
CREATE TABLE IF NOT EXISTS `User` (
	`UserId`	text NOT NULL,
	`UserName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	`Password`	text ( 50 ) COLLATE NOCASE,
	`FirstName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	`LastName`	text ( 50 ) COLLATE NOCASE,
	`Email`	text ( 250 ) NOT NULL COLLATE NOCASE,
	`Phone`	text ( 50 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`UserId`)
);
CREATE TABLE IF NOT EXISTS `TransferStatus` (
	`StatusId`	integer NOT NULL,
	`StatusName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`StatusId`)
);
CREATE TABLE IF NOT EXISTS `TransferItem` (
	`TransferItemId`	text NOT NULL,
	`TransferId`	text,
	`ConsumableProductId`	text,
	`ConsumableQuantity`	integer,
	`RentalProductId`	text,
	`RentalUniqueUnitNumber`	text ( 35 ) COLLATE NOCASE,
	`FromBinId`	text,
	FOREIGN KEY(`RentalProductId`) REFERENCES `RentalProduct`(`ProductId`),
	PRIMARY KEY(`TransferItemId`),
	FOREIGN KEY(`TransferId`) REFERENCES `Transfer`(`TransferId`),
	FOREIGN KEY(`FromBinId`) REFERENCES `BinNumber`(`BinId`),
	FOREIGN KEY(`ConsumableProductId`) REFERENCES `ConsumableProduct`(`ProductId`)
);
CREATE TABLE IF NOT EXISTS `TransferCarrier` (
	`CarrierId`	integer NOT NULL,
	`CarrierName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`CarrierId`)
);
CREATE TABLE IF NOT EXISTS `Transfer` (
	`TransferId`	text NOT NULL,
	`TransferNumber`	text ( 50 ) COLLATE NOCASE,
	`StatusId`	integer,
	`OpennedDate`	datetime,
	`ShippedDate`	datetime,
	`CarrierId`	integer,
	`TrackingNumber`	text ( 50 ) COLLATE NOCASE,
	`CreatedByUserId`	text,
	FOREIGN KEY(`CarrierId`) REFERENCES `TransferCarrier`(`CarrierId`),
	PRIMARY KEY(`TransferId`),
	FOREIGN KEY(`StatusId`) REFERENCES `TransferStatus`(`StatusId`),
	FOREIGN KEY(`CreatedByUserId`) REFERENCES `User`(`UserId`)
);
CREATE TABLE IF NOT EXISTS `TransactionType` (
	`TransactionTypeId`	integer NOT NULL,
	`TransactionTypeName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`TransactionTypeId`)
);
CREATE TABLE IF NOT EXISTS `TransactionHistory` (
	`TransactionId`	text NOT NULL,
	`Device`	char ( 10 ) COLLATE NOCASE,
	`CustomerId`	text ( 35 ) COLLATE NOCASE,
	`TransactionDate`	datetime,
	`Details`	text ( 2147483647 ) COLLATE NOCASE,
	`TransactionType`	integer,
	`ConsumableProductId`	text,
	`Quantity`	integer,
	`RentalProductId`	text,
	`UniqueUnitNumber`	text ( 35 ) COLLATE NOCASE,
	`SignImage`	blob ( 2147483647 ),
	`ReceiptText`	text ( 2147483647 ) COLLATE NOCASE,
	`OrderId`	text,
	FOREIGN KEY(`TransactionType`) REFERENCES `TransactionType`(`TransactionTypeId`),
	FOREIGN KEY(`RentalProductId`) REFERENCES `RentalProduct`(`ProductId`),
	FOREIGN KEY(`ConsumableProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
	FOREIGN KEY(`CustomerId`) REFERENCES `Customer`(`CustomerId`),
	PRIMARY KEY(`TransactionId`)
);
CREATE TABLE IF NOT EXISTS `TransactionCustomFields` (
	`CustomFieldId`	text NOT NULL,
	`TransactionId`	text NOT NULL,
	`FieldName`	text ( 50 ) COLLATE NOCASE,
	`FieldValue`	text ( 2147483647 ) COLLATE NOCASE,
	PRIMARY KEY(`CustomFieldId`),
	FOREIGN KEY(`TransactionId`) REFERENCES `TransactionHistory`(`TransactionId`)
);
CREATE TABLE IF NOT EXISTS `Tag2CustomerRole` (
	`TagId`	text NOT NULL,
	`CustomerRoleId`	text NOT NULL,
	FOREIGN KEY(`TagId`) REFERENCES `Tag`(`TagId`),
	FOREIGN KEY(`CustomerRoleId`) REFERENCES `CustomerRole`(`CustomerRoleId`),
	PRIMARY KEY(`TagId`,`CustomerRoleId`)
);
CREATE TABLE IF NOT EXISTS `Tag` (
	`TagId`	text NOT NULL,
	`TagName`	text ( 50 ) COLLATE NOCASE,
	PRIMARY KEY(`TagId`)
);
CREATE TABLE IF NOT EXISTS `SyncTransferObject` (
	`TransferObjectId`	text NOT NULL,
	`DateTimeSaved`	datetime,
	`TypeData`	text ( 500 ) COLLATE NOCASE,
	`Action`	char ( 1 ) COLLATE NOCASE,
	`IsMine`	bit,
	`Executed`	datetime,
	`DataObject`	blob ( 2147483647 ),
	`QueryType`	text ( 10 ) COLLATE NOCASE,
	`QueryTable`	text ( 100 ) COLLATE NOCASE,
	`QueryKey`	text ( 100 ) COLLATE NOCASE,
	`QueryFields`	text ( 1000 ) COLLATE NOCASE,
	`IncrementNumber`	integer NOT NULL PRIMARY KEY AUTOINCREMENT
);
CREATE TABLE IF NOT EXISTS `ShippingNotice` (
	`ShipingNoticeId`	text NOT NULL,
	`TransferNumber`	text ( 35 ) COLLATE NOCASE,
	`ConsumableProductNumber`	text ( 35 ) COLLATE NOCASE,
	`ConsumableQuantity`	integer,
	`ConsumableQuantityStart`	integer,
	`RentalProductNumber`	text ( 35 ) COLLATE NOCASE,
	`RentalUniqueUnitNumber`	text ( 35 ) COLLATE NOCASE,
	`ShippingDate`	datetime NOT NULL,
	`isProcessed`	bit,
	`CreatedByUserId`	text,
	`UpdatedByUserId`	text,
	PRIMARY KEY(`ShipingNoticeId`),
	FOREIGN KEY(`ShipingNoticeId`) REFERENCES `ShippingNotice`(`ShipingNoticeId`),
	FOREIGN KEY(`UpdatedByUserId`) REFERENCES `User`(`UserId`),
	FOREIGN KEY(`CreatedByUserId`) REFERENCES `User`(`UserId`)
);
CREATE TABLE IF NOT EXISTS `Role` (
	`Id`	text NOT NULL,
	`Name`	text ( 50 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`Id`)
);
CREATE TABLE IF NOT EXISTS `RentalReturn` (
	`RentalReturnId`	text NOT NULL,
	`ReturnNumber`	text ( 50 ) COLLATE NOCASE,
	`StartDate`	datetime,
	`CompleteDate`	datetime,
	`SignImage`	blob ( 2147483647 ),
	PRIMARY KEY(`RentalReturnId`)
);
CREATE TABLE IF NOT EXISTS `RentalProductStatusHistory` (
	`HistoryId`	text NOT NULL,
	`UniqueUnitNumber`	text ( 35 ) COLLATE NOCASE,
	`StatusId`	smallint,
	`AddDate`	datetime,
	`Comment`	text ( 2147483647 ) COLLATE NOCASE,
	PRIMARY KEY(`HistoryId`),
	FOREIGN KEY(`StatusId`) REFERENCES `RentalProductStatus`(`StatusId`)
);
CREATE TABLE IF NOT EXISTS `RentalProductStatus` (
	`StatusId`	smallint NOT NULL,
	`StatusName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`StatusId`)
);
CREATE TABLE IF NOT EXISTS `RentalProductItem` (
	`UniqueUnitNumber`	text ( 35 ) NOT NULL COLLATE NOCASE,
	`ProductId`	text,
	`Status`	smallint,
	`Allocated`	bit,
	`BinId`	text,
	`CertDate`	datetime,
	`CertDuration`	integer,
	`СontainerID`	text ( 50 ) COLLATE NOCASE,
	`Remarks`	text ( 2147483647 ) COLLATE NOCASE,
	FOREIGN KEY(`Status`) REFERENCES `RentalProductStatus`(`StatusId`),
	PRIMARY KEY(`UniqueUnitNumber`),
	FOREIGN KEY(`BinId`) REFERENCES `BinNumber`(`BinId`),
	FOREIGN KEY(`ProductId`) REFERENCES `RentalProduct`(`ProductId`)
);
CREATE TABLE IF NOT EXISTS `RentalProductCustomFields` (
	`CustomFieldId`	text NOT NULL,
	`ProductId`	text NOT NULL,
	`FieldName`	text ( 50 ) COLLATE NOCASE,
	`FieldValue`	text ( 2147483647 ) COLLATE NOCASE,
	PRIMARY KEY(`CustomFieldId`),
	FOREIGN KEY(`ProductId`) REFERENCES `RentalProduct`(`ProductId`)
);
CREATE TABLE IF NOT EXISTS `RentalProductBinHistory` (
	`HistoryId`	text NOT NULL,
	`UniqueUnitNumber`	text ( 35 ) COLLATE NOCASE,
	`BinId`	text,
	`AddDate`	datetime,
	FOREIGN KEY(`UniqueUnitNumber`) REFERENCES `RentalProductItem`(`UniqueUnitNumber`),
	FOREIGN KEY(`BinId`) REFERENCES `BinNumber`(`BinId`),
	PRIMARY KEY(`HistoryId`)
);
CREATE TABLE IF NOT EXISTS `RentalProduct2Tag` (
	`ProductID`	text NOT NULL,
	`TagId`	text NOT NULL,
	FOREIGN KEY(`ProductID`) REFERENCES `RentalProduct`(`ProductId`),
	FOREIGN KEY(`TagId`) REFERENCES `Tag`(`TagId`),
	PRIMARY KEY(`ProductID`,`TagId`)
);
CREATE TABLE IF NOT EXISTS `RentalProduct` (
	`ProductId`	text NOT NULL,
	`ProductNumber`	text ( 35 ) NOT NULL COLLATE NOCASE,
	`ProductDescription`	text ( 250 ) COLLATE NOCASE,
	`Manufacturer`	text ( 250 ) COLLATE NOCASE,
	`Make`	text ( 250 ) COLLATE NOCASE,
	`Model`	text ( 250 ) COLLATE NOCASE,
	`ReplacementCost`	float,
	`Picture`	text ( 255 ) COLLATE NOCASE,
	`AuthLevel`	integer NOT NULL DEFAULT 0,
	`ManufacturerNumber`	text ( 35 ) COLLATE NOCASE,
	`RentalPeriod`	integer,
	`isCustomerOwned`	bit,
	`WasChangedDate`	datetime,
	`CategoryId`	text,
	`Info`	text ( 50 ) COLLATE NOCASE,
	`DefaultBinId`	text,
	FOREIGN KEY(`DefaultBinId`) REFERENCES `BinNumber`(`BinId`),
	PRIMARY KEY(`ProductId`)
);
CREATE TABLE IF NOT EXISTS `ProductReceivingLog` (
	`HistoryId`	text NOT NULL,
	`ConsumableProductId`	text,
	`RentalUUN`	text ( 35 ) COLLATE NOCASE,
	`Quantity`	integer,
	`ToBinId`	text,
	`AddDate`	datetime,
	FOREIGN KEY(`RentalUUN`) REFERENCES `RentalProductItem`(`UniqueUnitNumber`),
	FOREIGN KEY(`ConsumableProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
	PRIMARY KEY(`HistoryId`),
	FOREIGN KEY(`ToBinId`) REFERENCES `BinNumber`(`BinId`)
);
CREATE TABLE IF NOT EXISTS `PickTicket` (
	`PickTicketId`	text NOT NULL,
	`OrderId`	text,
	`ProductId`	text,
	`IsRental`	bit,
	`BinId`	text,
	`Quantity`	integer,
	`Picked`	integer,
	PRIMARY KEY(`PickTicketId`),
	FOREIGN KEY(`OrderId`) REFERENCES `Order`(`OrderId`)
);
CREATE TABLE IF NOT EXISTS `Parameters` (
	`ParameterName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	`ParameterValue`	text ( 50 ) COLLATE NOCASE,
	`ParameterTitle`	text ( 50 ) COLLATE NOCASE,
	`OrderNum`	integer,
	PRIMARY KEY(`ParameterName`)
);
CREATE TABLE IF NOT EXISTS `OrderStatus` (
	`OrderStatusId`	integer NOT NULL,
	`OrderStatusName`	text ( 50 ) COLLATE NOCASE,
	PRIMARY KEY(`OrderStatusId`)
);
CREATE TABLE IF NOT EXISTS `OrderRentalItem` (
	`OrderItemId`	text NOT NULL,
	`OrderId`	text,
	`UniqueUnitNumber`	text ( 35 ) COLLATE NOCASE,
	`ProductNumber`	text ( 35 ) COLLATE NOCASE,
	`StartDate`	datetime,
	`EndDate`	datetime,
	`RentPrice`	float,
	`ReturnDate`	datetime,
	`ReturnCustomerId`	text ( 35 ) COLLATE NOCASE,
	`Comments`	text ( 2147483647 ) COLLATE NOCASE,
	`DueDate`	datetime,
	`RentalPeriod`	integer,
	`ProductId`	text,
	`RentalReturnNotes`	text ( 2147483647 ) COLLATE NOCASE,
	`WasDamaged`	bit,
	`RentalReturnId`	text,
	FOREIGN KEY(`OrderId`) REFERENCES `Order`(`OrderId`),
	PRIMARY KEY(`OrderItemId`),
	FOREIGN KEY(`ProductId`) REFERENCES `RentalProduct`(`ProductId`),
	FOREIGN KEY(`RentalReturnId`) REFERENCES `RentalReturn`(`RentalReturnId`)
);
CREATE TABLE IF NOT EXISTS `OrderConsumableReturnItemByCustomer` (
	`OrderConsumableReturnItemByCustomerId`	text NOT NULL,
	`OrderItemId`	text NOT NULL,
	`Quantity`	integer NOT NULL,
	`OrderConsumableReturnId`	text,
	FOREIGN KEY(`OrderItemId`) REFERENCES `OrderConsumableItem`(`OrderItemId`),
	PRIMARY KEY(`OrderConsumableReturnItemByCustomerId`),
	FOREIGN KEY(`OrderConsumableReturnId`) REFERENCES `OrderConsumableReturn`(`OrderConsumableReturnId`)
);
CREATE TABLE IF NOT EXISTS `OrderConsumableReturn` (
	`OrderConsumableReturnId`	text NOT NULL,
	`ReturnedDate`	datetime,
	`SignImage`	blob ( 2147483647 ),
	`ReturnedCustomerId`	text ( 35 ) COLLATE NOCASE,
	`OrderId`	text,
	FOREIGN KEY(`OrderId`) REFERENCES `Order`(`OrderId`),
	FOREIGN KEY(`ReturnedCustomerId`) REFERENCES `Customer`(`CustomerId`),
	PRIMARY KEY(`OrderConsumableReturnId`)
);
CREATE TABLE IF NOT EXISTS `OrderConsumableItem` (
	`OrderItemId`	text NOT NULL,
	`OrderId`	text,
	`ProductId`	text,
	`Quantity`	integer,
	`Picked`	integer,
	`Price`	float,
	FOREIGN KEY(`ProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
	PRIMARY KEY(`OrderItemId`),
	FOREIGN KEY(`OrderId`) REFERENCES `Order`(`OrderId`)
);
CREATE TABLE IF NOT EXISTS `Order` (
	`OrderId`	text NOT NULL,
	`CreatorCustomerId`	text ( 35 ) COLLATE NOCASE,
	`OrderDate`	datetime,
	`OrderCompleteDate`	datetime,
	`Details`	text ( 2147483647 ) COLLATE NOCASE,
	`SignImage`	blob ( 2147483647 ),
	`ReceiptText`	text ( 2147483647 ) COLLATE NOCASE,
	`OrderStatusId`	integer,
	`WhoPickedOrderCustomerId`	text ( 35 ) COLLATE NOCASE,
	`LimitOverwriteComment`	text ( 2147483647 ) COLLATE NOCASE,
	`OrderSum`	float,
	`OrderNumber`	text ( 50 ) COLLATE NOCASE,
	`ApprovedByCustomerId`	text ( 35 ) COLLATE NOCASE,
	`CreatedByUserId`	text,
	`WasChangedDate`	datetime,
	`AccountingCode`	text ( 50 ) COLLATE NOCASE,
	`WebOrderNumber`	text ( 50 ) COLLATE NOCASE,
	PRIMARY KEY(`OrderId`),
	FOREIGN KEY(`ApprovedByCustomerId`) REFERENCES `Customer`(`CustomerId`),
	FOREIGN KEY(`OrderStatusId`) REFERENCES `OrderStatus`(`OrderStatusId`),
	FOREIGN KEY(`CreatedByUserId`) REFERENCES `User`(`UserId`)
);
CREATE TABLE IF NOT EXISTS `InventoryAdjustmentReasonCode` (
	`ReasonCodeId`	text NOT NULL,
	`ReasonCodeValue`	text ( 50 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`ReasonCodeId`)
);
CREATE TABLE IF NOT EXISTS `InventoryAdjustment` (
	`InventoryAdjustmentId`	text NOT NULL,
	`ConsumableProductId`	text,
	`OldQnty`	integer NOT NULL,
	`NewQnty`	integer NOT NULL,
	`ReasonCodeId`	text,
	`ChangedByUserId`	text,
	`Notes`	text ( 2147483647 ) COLLATE NOCASE,
	`AddDate`	datetime NOT NULL DEFAULT (CURRENT_TIMESTAMP),
	FOREIGN KEY(`ChangedByUserId`) REFERENCES `User`(`UserId`),
	FOREIGN KEY(`ConsumableProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
	FOREIGN KEY(`ReasonCodeId`) REFERENCES `InventoryAdjustmentReasonCode`(`ReasonCodeId`),
	PRIMARY KEY(`InventoryAdjustmentId`)
);
CREATE TABLE IF NOT EXISTS `Error` (
	`ErrorId`	text NOT NULL,
	`ErrorType`	text ( 50 ) COLLATE NOCASE,
	`ErrorMessage`	text NOT NULL COLLATE NOCASE,
	`ErrorDate`	datetime NOT NULL DEFAULT (CURRENT_TIMESTAMP),
	PRIMARY KEY(`ErrorId`)
);
CREATE TABLE IF NOT EXISTS `DisplayColumnName` (
	`ColumnName`	text ( 250 ) NOT NULL COLLATE NOCASE,
	`DisplayName`	text ( 250 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`ColumnName`)
);
CREATE TABLE IF NOT EXISTS `DiscrepanciesType` (
	`TypeId`	integer NOT NULL,
	`TypeName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	`IsNegative`	bit NOT NULL,
	`ResolutionType`	text ( 200 ) COLLATE NOCASE,
	PRIMARY KEY(`TypeId`)
);
CREATE TABLE IF NOT EXISTS `DiscrepanciesResolvedStatus` (
	`ResolvedStatusId`	integer NOT NULL,
	`ResolvedStatusName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	`SelectBin`	text ( 50 ) NOT NULL COLLATE NOCASE,
	`HideBinSelector`	bit NOT NULL,
	PRIMARY KEY(`ResolvedStatusId`)
);
CREATE TABLE IF NOT EXISTS `Discrepancies` (
	`DiscrepanciesID`	text NOT NULL,
	`DescripancyDescription`	text ( 500 ) COLLATE NOCASE,
	`DiscrepancyTypeId`	integer,
	`AddingDate`	datetime NOT NULL,
	`ConsumableProductId`	text,
	`ConsumableQuantity`	integer,
	`RentalProductId`	text,
	`RentalUniqueUnitNumber`	text ( 35 ) COLLATE NOCASE,
	`Comment`	text ( 2147483647 ) COLLATE NOCASE,
	`IsResolved`	bit,
	`ResolvedStatusId`	integer,
	`ResolvedComment`	text ( 2147483647 ) COLLATE NOCASE,
	`ResolvedDate`	datetime,
	`UPDescription`	text ( 500 ) COLLATE NOCASE,
	`CreatedByUserId`	text,
	`UpdatedByUserId`	text,
	FOREIGN KEY(`ConsumableProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
	PRIMARY KEY(`DiscrepanciesID`),
	FOREIGN KEY(`DiscrepancyTypeId`) REFERENCES `DiscrepanciesType`(`TypeId`),
	FOREIGN KEY(`CreatedByUserId`) REFERENCES `User`(`UserId`),
	FOREIGN KEY(`UpdatedByUserId`) REFERENCES `User`(`UserId`),
	FOREIGN KEY(`ResolvedStatusId`) REFERENCES `DiscrepanciesResolvedStatus`(`ResolvedStatusId`),
	FOREIGN KEY(`RentalProductId`) REFERENCES `RentalProduct`(`ProductId`)
);
CREATE TABLE IF NOT EXISTS `CustomerStatus` (
	`StatusId`	smallint NOT NULL,
	`StatusName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`StatusId`)
);
CREATE TABLE IF NOT EXISTS `CustomerRole` (
	`CustomerRoleId`	text NOT NULL,
	`RoleName`	text ( 50 ) COLLATE NOCASE,
	PRIMARY KEY(`CustomerRoleId`)
);
CREATE TABLE IF NOT EXISTS `CustomerProductNumberXref` (
	`CustomerProductNumber`	text ( 35 ) NOT NULL COLLATE NOCASE,
	`ProductId`	text NOT NULL,
	FOREIGN KEY(`ProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
	PRIMARY KEY(`CustomerProductNumber`)
);
CREATE TABLE IF NOT EXISTS `CustomerPosition` (
	`CustomerPositionId`	text NOT NULL,
	`PositionName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	`PositionLevel`	smallint,
	PRIMARY KEY(`CustomerPositionId`)
);
CREATE TABLE IF NOT EXISTS `CustomerCustomFieldsTypes` (
	`FieldTypeId`	text NOT NULL,
	`FieldName`	text ( 50 ) COLLATE NOCASE,
	`FOrder`	integer,
	PRIMARY KEY(`FieldTypeId`)
);
CREATE TABLE IF NOT EXISTS `CustomerCustomFields` (
	`CustomFieldId`	text NOT NULL,
	`CustomerId`	text ( 35 ) NOT NULL COLLATE NOCASE,
	`CustomFieldTypeId`	text,
	`FieldValue`	text ( 2147483647 ) COLLATE NOCASE,
	FOREIGN KEY(`CustomerId`) REFERENCES `Customer`(`CustomerId`),
	PRIMARY KEY(`CustomFieldId`),
	FOREIGN KEY(`CustomFieldTypeId`) REFERENCES `CustomerCustomFieldsTypes`(`FieldTypeId`)
);
CREATE TABLE IF NOT EXISTS `Customer2Role` (
	`CustomerId`	text ( 35 ) NOT NULL COLLATE NOCASE,
	`CustomerRoleId`	text NOT NULL,
	FOREIGN KEY(`CustomerId`) REFERENCES `Customer`(`CustomerId`),
	FOREIGN KEY(`CustomerRoleId`) REFERENCES `CustomerRole`(`CustomerRoleId`),
	PRIMARY KEY(`CustomerId`,`CustomerRoleId`)
);
CREATE TABLE IF NOT EXISTS `Customer` (
	`CustomerId`	text ( 35 ) NOT NULL COLLATE NOCASE,
	`BadgeNumber`	text ( 35 ) COLLATE NOCASE,
	`FName`	text ( 50 ) COLLATE NOCASE,
	`LName`	text ( 50 ) COLLATE NOCASE,
	`Title`	text ( 50 ) COLLATE NOCASE,
	`SecurityLevel`	smallint,
	`BuyingLevel`	smallint,
	`RentalLevel`	smallint,
	`Status`	smallint,
	`ApproveCode`	text ( 50 ) COLLATE NOCASE,
	`Email`	text ( 255 ) COLLATE NOCASE,
	`Phone`	text ( 50 ) COLLATE NOCASE,
	`MaxProductPrice`	float,
	`MaxOrderPrice`	float,
	`MaxReplacementCost`	float,
	`CanNotBuy`	bit DEFAULT 0,
	`CanNotRent`	bit DEFAULT 0,
	`AuthLevel`	integer NOT NULL DEFAULT 0,
	`CustomerPositionId`	text,
	`ReportsToCustomerId`	text ( 35 ) COLLATE NOCASE,
	`WasChangedDate`	datetime,
	FOREIGN KEY(`Status`) REFERENCES `CustomerStatus`(`StatusId`),
	PRIMARY KEY(`CustomerId`),
	FOREIGN KEY(`CustomerPositionId`) REFERENCES `CustomerPosition`(`CustomerPositionId`),
	FOREIGN KEY(`ReportsToCustomerId`) REFERENCES `Customer`(`CustomerId`)
);
CREATE TABLE IF NOT EXISTS `ConsumableProductCustomFields` (
	`CustomFieldId`	text NOT NULL,
	`ProductId`	text NOT NULL,
	`FieldName`	text ( 50 ) COLLATE NOCASE,
	`FieldVaule`	text ( 2147483647 ) COLLATE NOCASE,
	FOREIGN KEY(`ProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
	PRIMARY KEY(`CustomFieldId`)
);
CREATE TABLE IF NOT EXISTS `ConsumableProductBinHistory` (
	`HistoryId`	text NOT NULL,
	`ProductId`	text,
	`Quantity`	integer,
	`ToBinId`	text,
	`AddDate`	datetime,
	PRIMARY KEY(`HistoryId`),
	FOREIGN KEY(`ProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
	FOREIGN KEY(`ToBinId`) REFERENCES `BinNumber`(`BinId`)
);
CREATE TABLE IF NOT EXISTS `ConsumableProduct2Tag` (
	`ProductID`	text NOT NULL,
	`TagId`	text NOT NULL,
	FOREIGN KEY(`ProductID`) REFERENCES `ConsumableProduct`(`ProductId`),
	PRIMARY KEY(`ProductID`,`TagId`),
	FOREIGN KEY(`TagId`) REFERENCES `Tag`(`TagId`)
);
CREATE TABLE IF NOT EXISTS `ConsumableProduct` (
	`ProductId`	text NOT NULL,
	`ProductNumber`	text ( 35 ) NOT NULL COLLATE NOCASE,
	`ProductDescription`	text ( 250 ) NOT NULL COLLATE NOCASE,
	`ManufacturerNumber`	text ( 35 ) COLLATE NOCASE,
	`MaterialCategory`	text ( 50 ) COLLATE NOCASE,
	`Make`	text ( 50 ) COLLATE NOCASE,
	`Model`	text ( 50 ) COLLATE NOCASE,
	`UnitOfMeasure`	text ( 10 ) COLLATE NOCASE,
	`Price`	float,
	`Picture`	text ( 255 ) COLLATE NOCASE,
	`AuthLevel`	integer NOT NULL DEFAULT 0,
	`isCustomerOwned`	bit,
	`WasChangedDate`	datetime,
	`DefaultBinId`	text,
	`CategoryId`	text,
	`Info`	text ( 50 ) COLLATE NOCASE,
	FOREIGN KEY(`DefaultBinId`) REFERENCES `BinNumber`(`BinId`),
	FOREIGN KEY(`CategoryId`) REFERENCES `Category`(`CategoryID`),
	PRIMARY KEY(`ProductId`)
);
CREATE TABLE IF NOT EXISTS `ConsumableBin` (
	`ConsumableBinId`	text NOT NULL,
	`ProductId`	text NOT NULL,
	`Quantity`	integer NOT NULL,
	`AddingDate`	datetime,
	`Allocated`	integer,
	`BinId`	text,
	FOREIGN KEY(`ProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
	PRIMARY KEY(`ConsumableBinId`),
	FOREIGN KEY(`BinId`) REFERENCES `BinNumber`(`BinId`)
);
CREATE TABLE IF NOT EXISTS `ConsumableAlternateProductNumberXref` (
	`AlternateNumber`	text ( 35 ) NOT NULL COLLATE NOCASE,
	`ProductId`	text NOT NULL,
	FOREIGN KEY(`ProductId`) REFERENCES `ConsumableProduct`(`ProductId`),
	PRIMARY KEY(`AlternateNumber`)
);
CREATE TABLE IF NOT EXISTS `Category` (
	`CategoryID`	text NOT NULL,
	`CategoryName`	text ( 250 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`CategoryID`)
);
CREATE TABLE IF NOT EXISTS `BinType` (
	`BinTypeId`	smallint NOT NULL,
	`BinTypeName`	text ( 20 ) NOT NULL COLLATE NOCASE,
	PRIMARY KEY(`BinTypeId`)
);
CREATE TABLE IF NOT EXISTS `BinNumber` (
	`BinId`	text NOT NULL,
	`BinNum`	text ( 35 ) COLLATE NOCASE,
	`BinTypeId`	smallint,
	`OrdNumber`	integer,
	PRIMARY KEY(`BinId`),
	FOREIGN KEY(`BinTypeId`) REFERENCES `BinType`(`BinTypeId`)
);
CREATE TABLE IF NOT EXISTS `AppState` (
	`AppName`	text ( 50 ) NOT NULL COLLATE NOCASE,
	`LastSyncDate`	datetime,
	PRIMARY KEY(`AppName`)
);
CREATE TABLE IF NOT EXISTS `AccountingCode` (
	`AccountingCodeId`	text NOT NULL,
	`AccountingCodeName`	text ( 50 ) COLLATE NOCASE,
	PRIMARY KEY(`AccountingCodeId`)
);
CREATE TABLE IF NOT EXISTS `AccessoryCrossReference` (
	`AccessoryId`	text NOT NULL,
	`ProductId1`	text NOT NULL,
	`ProductId2`	text NOT NULL,
	`TypeReference`	integer,
	PRIMARY KEY(`AccessoryId`)
);
CREATE UNIQUE INDEX IF NOT EXISTS `Customer_IX_Customer_ApproveCode` ON `Customer` (
	`ApproveCode`	DESC
);
COMMIT;
